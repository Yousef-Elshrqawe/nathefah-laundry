<?php

namespace App\Exports;

use App\Models\Order\order;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class OrderInvoiceExport implements FromCollection  , WithMapping, WithHeadings
{

    protected $ids;

    public function __construct($ids)
    {
        $this->ids = $ids;

    }

    public function collection()
    {
        return order::whereIn('id', $this->ids)->get();
    }

    public function headings(): array
    {
        $sumPriceBeforeDiscount = Order::whereIn('id', $this->ids)->where('price_after_discount', null)->sum('price_before_discount');
        $sumPriceAfterDiscount = Order::whereIn('id', $this->ids)->where('price_after_discount', '!=', null)->sum('price_after_discount');
        $price = $sumPriceBeforeDiscount + $sumPriceAfterDiscount;
        return [
            'Order number',
            'Customer name',
            'Customer Phone',
            'Branch  name',
            'Payment type',
            'payment method',
            'Delivery fees',
            'Taxes',
            'Discount value',
            'order price',
            'Created at',
            'Total Price Orders : ' . $price,

        ];
    }

    public function map($order): array
    {
     if($order->pymenttype_id == 1) {$payment_type = 'Wallet';}
        elseif($order->pymenttype_id == 2){$payment_type = 'Cash';}
        else{$payment_type = 'Visa';}

        if ($order->price_after_discount != null) {$price = $order->price_after_discount;}
        else {$price = $order->price_before_discount;}
        return [
            $order->id,
            $order->customer_name,
            $order->customer_phone,
            $order->branch->username,
            $payment_type,
            // $order->paymenttype->name,
            $order->delivery_fees,
            $order->taxes,
            $order->discount_value,
            $price,
            $order->created_at,
        ];
    }
}
