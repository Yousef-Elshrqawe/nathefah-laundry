<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\DiscountRepositoryInterface;
use App\Repositories\DiscountRepository;

class DiscountRepositiryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(DiscountRepositoryInterface::class, DiscountRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
