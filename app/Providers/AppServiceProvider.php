<?php

namespace App\Providers;

use App\Interfaces\PackageRepositoryInterface;
use App\Interfaces\PeriodRepositoryInterface;
use App\Interfaces\SubscriptionRepositoryInterface;
use App\Interfaces\SystemRepositoryInterface;
use App\Interfaces\TransactionRepositoryInterface;

use App\Repositories\PackageRepository;
use App\Repositories\PeriodRepository;
use App\Repositories\SubscriptionRepository;
use App\Repositories\SystemRepository;
use App\Repositories\TransactionRepository;




use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Pagination\Paginator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(
            PackageRepositoryInterface::class,
            PackageRepository::class
        );

        $this->app->bind(
            PeriodRepositoryInterface::class,
            PeriodRepository::class
        );

        $this->app->bind(
            SubscriptionRepositoryInterface::class,
            SubscriptionRepository::class
        );

        $this->app->bind(
            SystemRepositoryInterface::class,
            SystemRepository::class
        );

        $this->app->bind(
            TransactionRepositoryInterface::class,
            TransactionRepository::class
        );

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        // Schema::defaultStringLength(191);
        Paginator::useBootstrap();
    }
}
