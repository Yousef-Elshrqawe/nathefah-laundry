<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\DriverRepositoyInterface;
use App\Repositories\DriverRepository;

class DriverProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(DriverRepositoyInterface::class, DriverRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
