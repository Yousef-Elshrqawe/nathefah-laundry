<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\WalletRepositoryInterface;
use App\Repositories\WalletRepository;

class WalletProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(WalletRepositoryInterface::class, WalletRepository::class);

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
