<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Interfaces\Indeliveryorderinterface;
use App\Repositories\IndeliveryorderRepository;

class IndeliveryOrderProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
        $this->app->bind(Indeliveryorderinterface::class, IndeliveryorderRepository::class);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
