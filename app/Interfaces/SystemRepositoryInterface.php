<?php

namespace App\Interfaces;

interface SystemRepositoryInterface
{
    public function increasebalance($balance);
    public function decreasebalance($balance);
}
