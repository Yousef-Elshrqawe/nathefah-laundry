<?php


namespace App\Interfaces;


interface SubscriptionRepositoryInterface
{
    public function subscripe($request,$laundry_id);

    public function checkbranchcount($laundry_id);
}
