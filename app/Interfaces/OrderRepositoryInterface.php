<?php

namespace App\Interfaces;

interface OrderRepositoryInterface
{
    // we use it when customer make order
    public function getservices($request,$lang);
    // we use it to filter laundries when user enter services he need
    public function selectlaundry($request,$lang);
    // we use it when customer chose laundry when he creates order
    public function chooselaundry($request);

    //user use it when chose laundry from top laundries
    public function laundryservices($branch_id,$lang);

    public function getcategoryitems($category_id,$service_id,$branch_id,$lang);

    // get additional service inside item
    public function itemdetailes($itemid,$lang);

    public function submitorder($request);

    public function reciveorder($request);

    public function unasignedorder($request);

    public function orderinfo($order_id,$lang,$code);


    //this function generate code for every order to recive order
    public function generatecode($order_id);

    // this function get code of the order
    public function getcode($status,$order_id);

    // this request get delivery status of order (pick_up_laundry/drop_of_laundry/pick_up_home/drop_of_home)
    public function orderdeliverystatus($order_id,$lang);

    public function ordersummary($request);
    // this function get new orders in laundry abblication that created by user
    public function neworders($request);

    // this function display order info of new order
    public function neworderinfo($request);


    public function acceptorder($order_id,$branch_id,$branch_user_id);


    public function cancelorder($request,$order_id);

    // we use this function to recive order with code by(customer / driver / laundry)
    public function reciveorderbycode($order_id,$code);
    public function finishorder($id,$branchid);


    // we use this function to check count of order in delivery
    public function check_delivery_order($branchid);
    public function reciveorder_from_customer($order);


    // in this function customer recive order from laundry
    public function reciveorder_from_laundry($order,$orderstatus);

    public function transformmonytopoints($mony);


    public function checkorder($request,$branchid);


    public function rejectorder($request,$branch_id);
    // this function to get price of older that user should by
    // if user use discount will return price after discount
    // if user don't user discount return price pefore discount
    public function orderprice($order);

    public function addordertaxes($order,$branchid,$price_before_discount);


    public function getprice($order);

    /*









    */

}
