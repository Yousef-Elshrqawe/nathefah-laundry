<?php

namespace App\Interfaces;

interface DiscountRepositoryInterface
{
   public function creatediscount($request);
   // for user abblication
   public function getdiscount($request);
   public function usediscount($order_id,$code , $lang);
   //for dashboard  get [previous - avilable] discount
   public function getdiscounts($type);


   //check if order hasdiscount
   public function orderdiscount($order_id);

   // we use this function when edit order have discount  $order refere to new order table
   public function order_edit_discount($order,$discount_id);
}
