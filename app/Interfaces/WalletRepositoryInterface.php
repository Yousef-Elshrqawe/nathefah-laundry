<?php

namespace App\Interfaces;

interface WalletRepositoryInterface
{
    // we use this function to get point inside wallet of user
    public function getpallence($user_id);
    // we use this function when user pay by wallet
    public function increesebrnachwallet($branchid,$amount);
    // we use this function when user pay by wallet
    public function decreaseuserwallet($amount,$user_id);
    // in this function increase user wallet we use it when branch reject order
    public function increeseuserwallet($user_id,$amount);
}
