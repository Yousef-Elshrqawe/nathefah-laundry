<?php


namespace App\Interfaces;


interface PeriodRepositoryInterface
{
    public function index();
    public function store($request);
    public function update($request);
    public function destroy($period_id);
    public function getPeriods($lang);
}
