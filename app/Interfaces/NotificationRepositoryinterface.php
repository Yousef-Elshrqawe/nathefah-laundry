<?php

namespace App\Interfaces;

interface NotificationRepositoryinterface
{
   // this function to create notification type in tables()
   public function createnotificationsetting($type,$id);
   // this function store devices token
   public function createdevicetoken($type,$id,$device_token);
   public function getnotificationsetting($id,$lang);
   public function updatenotificationsetting($id,$status);

   public function sendnotification($type,$id,$title,$body,$notificationtype,$key=null,$data=null);
   // save notification table
   public function createnotification($type,$id,$title,$body);
   public function getnotification($type,$id,$lang);

   public function delete_devicetoken($device_token,$type,$id=null);
}
