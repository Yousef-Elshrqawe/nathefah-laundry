<?php

namespace App\Interfaces;

interface BranchRepositoryInterface
{
  public function getopentime($branchid);
  // this function check if phone is exist in data base or not
  public function checkphone($request);
  public function getlaundries($lat,$long);
  public function branchrate($rate);
  public function distance($lat1, $lon1, $lat2, $lon2, $unit);
  public function checkbranchservices();
  public function get_paymet_methods($branch_id,$lang);


}
