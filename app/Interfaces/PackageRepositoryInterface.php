<?php


namespace App\Interfaces;


interface PackageRepositoryInterface
{
    public function index();
    public function store($request);
    public function update($request);
    public function destroy($package_id);
    public function getPackage($period_id);
}
