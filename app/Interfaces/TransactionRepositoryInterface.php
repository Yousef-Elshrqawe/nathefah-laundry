<?php

namespace App\Interfaces;

interface TransactionRepositoryInterface
{

    // use this for branch
    public function createtransaction($order,$price);
    // use this for system
    public function  create_system_transaction($order,$price);



    public function create_externance_transaction($order,$price);

    public function create_externance_system_transaction($order,$price);

    // this function use when user cancel order to  decrease blance of branch

    // $type refer to driver make eny acction in this order or not
    public function decrese_brnach_balance($order,$amount,$delivery_action);


    // we use it when laundry recive order to customer
    public function activetransaction($order);

    // we use it when  customer cancel order after branch assign order to driver
    public function deletetransaction($order);




}
