<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Subscription\LaundryPackage;
use App\Models\Laundry\Laundry;
use Carbon\Carbon;


class subscription extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'supscription:end';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command for description ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $LaundryPackages=LaundryPackage::get();
        foreach($LaundryPackages as $LaundryPackage){
              if($LaundryPackage->end_date <= now()){
                $laundry=Laundry::find($LaundryPackage->laundry_id);

                $laundry->status=false;
                $laundry->save();
             }
        }
        return 0;
    }
}
