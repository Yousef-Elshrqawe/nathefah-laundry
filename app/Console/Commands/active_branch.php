<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\closeingday\Closeingday;
use App\Models\Laundry\{branch,branchcloseingday};
use Carbon\Carbon;

class active_branch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'branch:active';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command to open closing branchs when the current day not holiday in this branch
          نقوم باستخدام هذا الكومند لاعاده فتح المغاسل المغلقه عند انتهاء ايام العطله
     ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::now();
        $today = $today->format('l');
        $branchs=branch::where('status','closed')->chunk(300, function ($branchs) use($today) {
            foreach($branchs as $branch){
                $branchclosingdaies_id=branchcloseingday::where('branch_id',$branch->id)->pluck('closeingday_id')->all();
                $branchclosingdaies=Closeingday::find($branchclosingdaies_id);
                $status='closed';
                foreach($branchclosingdaies as $closeday){
                    if($closeday->name==$today){
                        $status='closed';
                    }else{
                        $status='open';
                    }
                }
                $branch->update([
                    'status'=>$status
                  ]);
            }
        });

        return 0;
    }
}
