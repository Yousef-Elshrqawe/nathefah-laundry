<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\closeingday\Closeingday;
use App\Models\Laundry\{branch,branchcloseingday};
use Carbon\Carbon;


class offlineday extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'offline:day';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'this command check  offline days of branchs to ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $today = Carbon::now();
        $day = $today->format('l');
        $closingdaies=Closeingday::get();
        foreach ($closingdaies as $key => $closingday) {
          if($closingday->name==$day){
            $branchs_id=branchcloseingday::where('closeingday_id',$closingday->id)->pluck('branch_id')->all();
            $branchs=branch::wherein('id',$branchs_id)->chunk(250, function($chunk) {
                foreach ($chunk as $branch) {
                    $branch->update([
                        'status'=>'closed'
                      ]);
                }
            });
          }
        }
        return 0;
    }
}
