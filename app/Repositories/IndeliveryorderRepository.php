<?php

namespace App\Repositories;
use App\Interfaces\Indeliveryorderinterface;
use App\Models\Laundry\branch;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Str;


class IndeliveryorderRepository implements Indeliveryorderinterface
{
    public $orders_id=[];
    public function driverorder($orders,$lang,$drivers){
        $this->orders_id=$orders;
        $services=DB::table('order_detailes')
        ->select('servicetranslations.name','order_id','order_detailes.service_id')
        ->join('servicetranslations','servicetranslations.service_id','=','order_detailes.service_id')
        ->selectRaw('sum(quantity) as quantity')
        ->wherein('order_detailes.order_id',$this->orders_id)
        ->where('order_detailes.additionalservice_id','=',null)
        ->where('servicetranslations.locale',$lang)
        ->groupBy('order_detailes.order_id')
        ->groupBy('order_detailes.service_id')
        ->groupBy('servicetranslations.service_id')
        ->groupBy('servicetranslations.name')
        ->get();
        $additionalservices=DB::table('order_detailes')
        ->select('order_id','order_detailes.service_id','order_detailes.additionalservice_id','additionalservicetranslations.name')
        ->join('additionalservicetranslations','additionalservicetranslations.additionalservice_id','=','order_detailes.additionalservice_id')
        ->selectRaw('sum(quantity) as quantity')
        ->wherein('order_detailes.order_id',$this->orders_id)
        ->where('order_detailes.additionalservice_id','!=',null)
        ->where('additionalservicetranslations.locale',$lang)
        ->groupBy('additionalservicetranslations.name')
        ->groupBy('order_detailes.order_id')
        ->groupBy('order_detailes.service_id')
        ->groupBy('order_detailes.additionalservice_id')
        ->get();
        $argents=db::table('argent')->wherein('order_id',$this->orders_id)->get();

        // but additional service inside service
        foreach($services as $service){
            $service->additionalservice=[];
            foreach($additionalservices as $key=>$additionalservice){
                if($service->order_id == $additionalservice->order_id && $service->service_id == $additionalservice->service_id){
                    array_push($service->additionalservice,$additionalservice);
                }
            }
            foreach($argents as $argent){
                $service->argent=0;
                if($service->service_id==$argent->service_id&&$service->order_id == $argent->order_id){
                    $service->argent=$argent->quantity;
                }
            }
         }
         foreach($drivers as $driver){
            $driver->order=[];
             foreach($services as $service){
                if($service->order_id==$driver->order_id){
                     array_push($driver->order,$service);
                }
             }
             unset($driver->order_id);
         }
         return $drivers;
    }
}
