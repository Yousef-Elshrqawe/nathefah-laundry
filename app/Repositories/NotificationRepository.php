<?php

namespace App\Repositories;

use App\Interfaces\NotificationRepositoryinterface;
use App\Services\GoogleAccessTokenService;
use App\Models\Notification\
{branchnotifytype,
    Usernotifytype,
    drivernotifytype,
    BranchDeviceToken,
    UserDeviceToken
,
    DriverDeviceToken,
    BranchNotification,
    UserNotification,
    DriverNotification,
    usernotificationtranslation,
    branchnotificationtranslation,
    drivernotificationtranslation
};
use GuzzleHttp\Client;
use App\Models\User;
use App\Models\Laundry\Branchuser;
use DB;
use Kreait\Firebase\Factory;
use Kreait\Firebase\Messaging\CloudMessage;
use Kreait\Firebase\Messaging\Notification;
use Kreait\Firebase\Messaging\Messaging;
use GuzzleHttp\Exception\RequestException; // إذا كنت تستخدم Guzzle كعميل HTTP
use Exception;

class NotificationRepository implements NotificationRepositoryinterface
{

    protected $messaging;
    public $googleAccessToken = '';
    protected static $firebaseApiUrl = 'https://fcm.googleapis.com/v1/projects/nathefa-ce6db/messages:send';

    public function __construct(GoogleAccessTokenService $googleAccessTokenService)
    {
        $this->googleAccessToken = $googleAccessTokenService->getAccessToken(); // == $sourceKey
    }

    public function createnotificationsetting($type, $id)
    {
        if ($type == 'branch') {
            for ($i = 1; $i < 7; $i++) {
                branchnotifytype::create([
                    'user_branch_id' => $id,
                    'notificationtype_id' => $i
                ]);
            }
            return true;
        } elseif ($type == 'user') {
            for ($i = 1; $i <= 7; $i++) {
                if ($i == 3 || $i == 5 || $i == 8 || $i == 9 || $i == 10) {
                    Usernotifytype::create([
                        'user_id' => $id,
                        'notificationtype_id' => $i
                    ]);
                }
            }
            return true;
        } elseif ($type == 'driver') {
            for ($i = 1; $i <= 4; $i++) {
                drivernotifytype::create([
                    'driver_id' => $id,
                    'notificationtype_id' => $i
                ]);
            }
            return true;
        }
    }

    public function getnotificationsetting($id, $lang)
    {
        $notifications = DB::table('usernotifytype')
            ->select(
                'usernotifytype.id',
                'usernotifytype.status',
                'notificationtypetranslations.name',
                'notificationtypes.created_at'
            )
            ->join('notificationtypes', 'usernotifytype.notificationtype_id', '=', 'notificationtypes.id')
            ->join('notificationtypetranslations', 'notificationtypetranslations.notificationtype_id', '=', 'notificationtypes.id')
            ->where('notificationtypetranslations.locale', $lang)
            ->where('usernotifytype.user_id', $id)
            ->get();

        return $notifications;
    }


    public function updatenotificationsetting($id, $status)
    {
        $Usernotifytype = Usernotifytype::find($id);
        $Usernotifytype->update([
            'status' => $status
        ]);
        return true;
    }

    public function createdevicetoken($type, $id, $device_token)
    {
        if ($type == 'user') {
            $userdevicetoken = UserDeviceToken::where(['user_id' => $id, 'device_token' => $device_token])->first();
            if ($userdevicetoken != null) {
                $userdevicetoken->delete();
            }


            UserDeviceToken::create([
                'user_id' => $id,
                'device_token' => $device_token
            ]);
            return true;
        }
        if ($type == 'branch') {
            $userdevicetoken = BranchDeviceToken::where(['branch_user_id' => $id, 'device_token' => $device_token])->first();

            if ($userdevicetoken != null) {
                $userdevicetoken->delete();
            }


            BranchDeviceToken::create([

                'branch_user_id' => $id,
                'device_token' => $device_token
            ]);

            return true;
        }
        if ($type == 'driver') {
            $userdevicetoken = DriverDeviceToken::where(['driver_id' => $id, 'device_token' => $device_token])->first();

            if ($userdevicetoken != null) {

                $userdevicetoken->delete();
            }




            DriverDeviceToken::create([

                'driver_id' => $id,
                'device_token' => $device_token
            ]);
            return true;

            return false;
        }

    }



    public function sendnotification($type, $id, $title, $body, $notificationtype, $key = null, $data = null)
    {

        $firebaseApiUrl = self::$firebaseApiUrl;
        $header = [
            'Authorization' => 'Bearer ' . $this->googleAccessToken,
            'Content-Type' => 'application/json',
            'content_available' => true,
        ];
        $client = new Client(['headers' => $header]);
        $datafire = [
            'title' => $title,
            'body' => $body,
            isset($data['order_id']) ? 'order_id' : null,
            isset($data['driver_name']) ? 'driver_name' : null
        ];


        if ($type == 'user') {
            $order_id = null;
            if ($data != null) {
                $order_id = $data['order_id'];
            }
            $ids = Usernotifytype::where('user_id', $id)->where('notificationtype_id', $notificationtype)->where('status', '1')->pluck('user_id')->all();

            $userdevicetokens = UserDeviceToken::where('user_id', $id)->pluck('device_token')->all();
            if (empty($userdevicetokens)) {

            }

            foreach ($userdevicetokens as $token) {

                $payload = [
                    'message' => [
                        'token' => $token,
                        'notification' => [
                            'title' => $title,
                            'body' => $body,
                        ],
                        "android" => [
                            "priority" => "high",
                            "notification" => [
                                // "click_action" => "TOP_STORY_ACTIVITY",
                                "image" => "https://foo.bar/pizza-monster.png",
                                "sound" => "default"
                            ],
                            "data" => $datafire
                        ],
                        "apns" => [
                            "headers" => [
                                "apns-priority" => "10",
                            ],
                            "payload" => [
                                "aps" => [
                                    "mutable-content" => 1,
                                    "content-available" => 1,
                                    "sound" => "default"
                                ]
                            ],
                            "fcm_options" => [
                                "image" => "https://foo.bar/pizza-monster.png",
                            ]
                        ],
                        'data' => $datafire
                    ]
                ];


                try {
                    $response = $client->post($firebaseApiUrl, ['json' => $payload]);

                } catch (Exception $e) {

                }

            }

        }
        if ($type == 'branch') {

            $ids = branchnotifytype::where('user_branch_id', $id)->where('notificationtype_id', $notificationtype)->where('status', '1')->pluck('user_branch_id')->all();

            $userdevicetokens = BranchDeviceToken::where('branch_user_id', $id)->pluck('device_token')->all();
            if (empty($userdevicetokens)) {

            }
            $driver_name = null;
            if ($data != null && isset($data['driver_name'])) {
                $driver_name = $data['driver_name'];
            }


            foreach ($userdevicetokens as $token) {

                $payload = [
                    'message' => [
                        'token' => $token,
                        'notification' => [
                            'title' => $title,
                            'body' => $body,
                        ],
                        "android" => [
                            "priority" => "high",
                            "notification" => [
                                // "click_action" => "TOP_STORY_ACTIVITY",
                                "image" => "https://foo.bar/pizza-monster.png",
                                "sound" => "default"
                            ],
                            "data" => $datafire
                        ],
                        "apns" => [
                            "headers" => [
                                "apns-priority" => "10",
                            ],
                            "payload" => [
                                "aps" => [
                                    "mutable-content" => 1,
                                    "content-available" => 1,
                                    "sound" => "default"
                                ]
                            ],
                            "fcm_options" => [
                                "image" => "https://foo.bar/pizza-monster.png",
                            ]
                        ],
                        'data' => $datafire
                    ]
                ];

                // إرسال الطلب إلى Firebase
                try {
                    $response = $client->post($firebaseApiUrl, ['json' => $payload]);
//                    $statusCode = $response->getStatusCode();
//                    $body = json_decode($response->getBody(), true);
//


                  /*  // التحقق من النجاح أو الفشل
                    if ($statusCode == 200 ||  $statusCode == '200' ) {
                        echo true;
                    } else {
                        echo "Failed to send notification. Status code: $statusCode";
                    }*/
                } catch (Exception $e) {


                }

            }

        }
        if ($type == 'driver') {
            $ids = drivernotifytype::where('driver_id', $id)->where('notificationtype_id', $notificationtype)->where('status', '1')->pluck('driver_id')->all();
//                if (empty($ids)) {
//                    return true;
//                }
            $userdevicetokens = DriverDeviceToken::where('driver_id', $id)->pluck('device_token')->all();
            if (empty($userdevicetokens)) {

            }
            $driver_name = null;
            if ($data != null && isset($data['driver_name'])) {
                $driver_name = $data['driver_name'];
            }

            foreach ($userdevicetokens as $token) {

                $payload = [
                    'message' => [
                        'token' => $token,
                        'notification' => [
                            'title' => $title,
                            'body' => $body,
                        ],
                        "android" => [
                            "priority" => "high",
                            "notification" => [
                                // "click_action" => "TOP_STORY_ACTIVITY",
                                "image" => "https://foo.bar/pizza-monster.png",
                                "sound" => "default"
                            ],
                            "data" => $datafire
                        ],
                        "apns" => [
                            "headers" => [
                                "apns-priority" => "10",
                            ],
                            "payload" => [
                                "aps" => [
                                    "mutable-content" => 1,
                                    "content-available" => 1,
                                    "sound" => "default"
                                ]
                            ],
                            "fcm_options" => [
                                "image" => "https://foo.bar/pizza-monster.png",
                            ]
                        ],
                        'data' => $datafire
                    ]
                ];


                try {
                    $response = $client->post($firebaseApiUrl, ['json' => $payload]);
                } catch (Exception $e) {


                }

            }

        }


    }


    public
    function createnotification($type, $id, $titles, $bodys)
    {
        if ($type == 'user') {
            $usernotification = UserNotification::create([
                'user_id' => $id,
            ]);
            foreach ($titles as $key => $title) {
                usernotificationtranslation::create([
                    'usernotification_id' => $usernotification->id,
                    'title' => $title,
                    'body' => $bodys[$key],
                    'locale' => $key
                ]);
            }
        }
        if ($type == 'branch') {
            $branchnotification = BranchNotification::create([
                'branch_id' => $id,
            ]);
            foreach ($titles as $key => $title) {
                branchnotificationtranslation::create([
                    'branch_notify_id' => $branchnotification->id,
                    'title' => $title,
                    'body' => $bodys[$key],
                    'locale' => $key
                ]);
            }
        }
        if ($type == 'driver') {
            $drivernotification = DriverNotification::create([
                'driver_id' => $id,
            ]);
            foreach ($titles as $key => $title) {
                drivernotificationtranslation::create([
                    'driver_notify_id' => $drivernotification->id,
                    'title' => $title,
                    'body' => $bodys[$key],
                    'locale' => $key
                ]);
            }
        }
        return true;
    }

    public
    function getnotification($type, $id, $lang)
    {
        if ($type == 'user') {
            $notifications = DB::table('usernotification')
                ->join('usernotificationtranslations', 'usernotificationtranslations.usernotification_id', '=', 'usernotification.id')
                ->where('usernotification.user_id', $id)
                ->where('locale', $lang)
                ->orderby('usernotification.id', 'desc')
                ->get();
            // $notifications=UserNotification::where('user_id',$id)->orderby('created_at','desc')->get()->take(100);
        }
        if ($type == 'branch') {
            $notifications = DB::table('branchnotification')
                ->join('branchnotificationtranslations', 'branchnotificationtranslations.branch_notify_id', '=', 'branchnotification.id')
                ->where('branchnotification.branch_id', $id)
                ->where('locale', $lang)
                ->orderby('branchnotification.id', 'desc')
                ->get();
        }
        if ($type == 'driver') {
            $notifications = DB::table('drivernotification')
                ->join('usernotificationtranslations', 'drivernotificationtranslations.driver_notify_id', '=', 'drivernotification.id')
                ->where('drivernotification.driver_id', $id)
                ->where('locale', $lang)
                ->orderby('drivernotification.id', 'desc')
                ->get();
        }
        return $notifications;
    }

    public
    function delete_devicetoken($device_token, $type, $id = null)
    {
        if ($type == 'branch') {
            $user_id = 'branch_user_id';
            $userdevicetoken = BranchDeviceToken::where(['device_token' => $device_token, 'branch_user_id' => $id])->first();
        } elseif ($type == 'user') {
            $user_id = 'user_id';
            $userdevicetoken = UserDeviceToken::where(['device_token' => $device_token, 'user_id' => $id])->first();
        } elseif ($type == 'driver') {
            $user_id = 'driver_id';
            $userdevicetoken = DriverDeviceToken::where(['device_token' => $device_token, 'driver_id' => $id])->first();
        }
        if ($userdevicetoken != null)
            $userdevicetoken->delete();
        return true;
    }
}

