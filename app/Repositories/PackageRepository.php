<?php


namespace App\Repositories;


use App\Interfaces\PackageRepositoryInterface;
use App\Models\Subscription\Package;
use Illuminate\Support\Collection;


class PackageRepository implements PackageRepositoryInterface
{

    protected $model;

    public function __construct()
    {
        $this->model = new Package();
    }//End __construct Function


    public function index()
    {
        // TODO: Implement index() method.
        return $this->model::with('period')->get();
    }

    public function store($request)
    {
        // TODO: Implement store() method.
       return  $this->model::create($request->all());
    }


    public function update($request)
    {
        // TODO: Implement update() method.
        $package = $this->model::findOrFail($request->id);

       return $package->update($request->all());
    }


    public function destroy($package_id)
    {
        // TODO: Implement destroy() method.
        return  $this->model::findOrFail($package_id)->delete();
    }

    public function getPackage($period_id , $package_id = null)
    {
        // TODO: Implement getPackage() method.
        if ($package_id != null) {
            $packages=$this->model::where('period_id',$period_id)->where('id', '!=', $package_id)->orderBy('max_branch','asc')->get();
        } else {
            $packages=$this->model::where('period_id',$period_id)->orderBy('max_branch','asc')->get();
        }
        $new_packages=[];
        foreach($packages as  $key=>$package){
            $new_packages[]=$package;
        }
        return $new_packages;
    }

}
