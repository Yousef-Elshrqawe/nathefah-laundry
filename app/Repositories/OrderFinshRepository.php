<?php

namespace App\Repositories;

use App\Interfaces\OrderRepositoryInterface;
use App\Models\Discount\Discount;
use App\Models\Discount\UserDiscount;
use App\Models\Notification\drivernotifytype;
use App\Repositories\WalletRepository;
use Illuminate\Support\Facades\App;
use App\Interfaces\{NotificationRepositoryinterface,
    OrderFinshRepositoryInterface,
    TransactionRepositoryInterface,
    SystemRepositoryInterface};
use App\Interfaces\BranchRepositoryInterface;
use App\Models\Laundry\branchservice;
use App\Models\laundryservice\Service;
use App\Models\Laundry\Branchitem;
use App\Models\Laundry\BranchitemTranslation;
use App\Models\Laundry\{branch, Laundry};
use App\Http\Resources\editservice\categoryresource;
use App\Http\Resources\editservice\serviceresource;
use App\Models\laundryservice\Additionalservice;
use App\Models\Order\OrderDriveryStatus;
use App\Models\Order\order;
use App\Http\Resources\editservice\branchitem as branchitemresource;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Order\orderdetailes;
use App\Models\laundryservice\Argent;
use App\Models\User\Adress;
use App\Traits\queries\orders;
use App\Traits\queries\serviceTrait;
use App\Models\Order\delivery_type;
use App\Models\Order\payment_method;
use App\Models\Logs\Accept_order;
use App\Models\Logs\Reject_order;
use App\Models\Balance\Balance;
use App\Models\Driver\Driver;
use App\Models\System\Systeminfo;
use App\Traits\response;
use App\Models\Laundry\Branchuser;
use Carbon\Carbon;
use App\Models\User;
use Validator;
use Auth;


class OrderFinshRepository implements OrderFinshRepositoryInterface
{

    use orders, serviceTrait, response;

    public $service_ids = [];

    public function __construct(NotificationRepositoryinterface $NotificationRepository
        , WalletRepository                                      $WalletRepository, BranchRepositoryInterface $BranchRepository
        , TransactionRepositoryInterface                        $TransactionRepository
        , SystemRepositoryInterface                             $SystemRepository
        , OrderRepositoryInterface                              $OrderRepository
    )
    {
        $this->NotificationRepository = $NotificationRepository;
        $this->WalletRepository = $WalletRepository;
        $this->BranchRepository = $BranchRepository;
        $this->TransactionRepository = $TransactionRepository;
        $this->SystemRepository = $SystemRepository;
        $this->OrderRepository = $OrderRepository;
    }

    // this function get the services in the system
    public function getservices($request, $lang)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $services = Service::select('id', 'img')->get();
        return $services;
    }

    // this function get laundry that provide service that choosen by user
    public function selectlaundry($request, $lang)
    {
        $lat = $request->lat;
        $long = $request->long;
        $branchs = DB::table("branchs")->
        select('branchs.id as id', 'branchs.username', 'laundries.name', 'laundries.logo', 'branchs.argent'
            , 'branchs_rate.rate as rate', 'branchs.visa'
            , 'branchs.cash', 'branchs.delivery_status', 'branchs.lat', 'branchs.long'
            , 'laundries.door_to_door_delivery_fees as delivery_fees', 'laundries.one_way_delivery_fees'
            , 'laundries.taxes', 'laundries.taxamount'
            , DB::raw("round(6371 * acos(cos(radians(" . $lat . "))
        * cos(radians(branchs.lat))
        * cos(radians(branchs.long) - radians(" . $long . "))
        + sin(radians(" . $lat . "))
        * sin(radians(branchs.lat))),1) AS distance"))
            ->join('laundries', 'laundries.id', '=', 'branchs.laundry_id')
            ->join('brnachservices', 'brnachservices.branch_id', '=', 'branchs.id')
            ->leftjoin('branchs_rate', 'branchs_rate.branch_id', '=', 'branchs.id')
            ->wherein('brnachservices.service_id', $request->services)
            ->where('laundries.status', 1)
            ->where('branchs.status', 'open')
            ->groupBy("branchs.id")
            ->groupBy("branchs.lat")
            ->groupBy("branchs.long")
            ->groupBy("branchs.visa")
            ->groupBy("branchs.cash")
            ->groupBy("branchs.argent")
            ->groupBy("laundries.door_to_door_delivery_fees")
            ->groupBy("laundries.one_way_delivery_fees")
            ->groupBy("laundries.taxes")
            ->groupBy("laundries.taxamount")
            ->groupBy("branchs.username")
            ->groupBy("branchs_rate.rate")
            ->groupby("branchs.delivery_status")
            ->groupBy("laundries.name")
            ->groupBy("laundries.logo")
            ->orderby('distance', 'ASC')
            ->paginate(30);
        foreach ($branchs as $branch) {
            if ($branch->logo == null)
                $branch->logo = asset('uploads/laundry/logos/default/images.jpg');
            $branch->logo = asset('uploads/laundry/logos/' . $branch->logo);
        }
        return $branchs;
    }

    public function laundryservices($branch_id, $lang)
    {
        $services = DB::table('services')
            ->select('servicetranslations.name', 'services.id')
            ->join('brnachservices', 'brnachservices.service_id', '=', 'services.id')
            ->join('servicetranslations', 'servicetranslations.service_id', 'services.id')
            ->where('brnachservices.branch_id', $branch_id)
            ->where('servicetranslations.locale', $lang)
            ->where('brnachservices.status', 'on')
            ->get();
        return $services;
    }

    public function chooselaundry($request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $branch_id = $request->branch_id;
        $branch = branch::find($branch_id);
        $data = [];
        $branchservices = branchservice::select('service_id')
            ->wherein('service_id', $request->services)
            ->where('branch_id', $branch_id)->where('status', 'on')->get();
        foreach ($branchservices as $branchservice) {
            array_push($this->service_ids, $branchservice->service_id);
        }
//         $services = Service::wherein('id', $this->service_ids)->select('id')->with('categories')->get()->makehidden(['created_at', 'updated_at']);
        //هاتلي  الاقسام اللي موجوده في الخدمات اللي انا اخترتها الي  ليها ايتم فقط
        $services = Service::wherein('id', $this->service_ids)->with(['categories' => function ($q) use ($branch_id) {
            $q->whereHas('branchitems', function ($q) use ($branch_id) {
                $q->where('branch_id', $branch_id);
            });
        }])->get();
        $data['services'] = $services;
        $data['branchargent'] = $branch->argent;
        return $data;
    }

    // this function get item inside specify category
    public function getcategoryitems($category_id, $service_id, $branch_id, $lang)
    {
        $brnchitem = Branchitem::whereHas('branchitemprice', function ($q) use ($service_id, $branch_id, $category_id) {
            $q->where('service_id', $service_id)->where('branch_id', $branch_id)->where('category_id', $category_id);
        })->with(['branchitemprice' => function ($q) use ($service_id, $branch_id, $category_id) {
            $q->where('service_id', $service_id)->where('branch_id', $branch_id)->where('category_id', $category_id)->get();
        }])->get();
        return ['status' => true, 'message' => 'get all pranch item price successfully',
            'data' => ['branchitem' => branchitemresource::collection($brnchitem)]
        ];
    }

    // we use this function to get the additional serviece that can be done in this item
    public function itemdetailes($item_id, $lang)
    {
        $itemadditionalservice = Additionalservice::select('id')->whereHas('branchadditionalservice', function (Builder $query) use ($item_id) {
            $query->where('branchitem_id', $item_id)->where('status', 'on');
        })->with(['itemprices' => function ($q) use ($item_id) {
            $q->select('additionalservice_id', 'price', 'id as item_price_id')->where('branchitem_id', $item_id)->get();
        }])->get();
        return $itemadditionalservice;
    }

    public function submitorder($request)
    {
        $branchid = $request->branch_id;
        $price_before_discount = 0;
        try {
            DB::beginTransaction();
            $urgent = false;
            foreach ($request->serviceprices as $serviceprice) {
                if ($serviceprice['argent'] != 0) {
                    $branch = branch::find($request->branch_id);
                    if ($branch->argent == 0)
                        return 'urgentfalse';
                    $urgent = true;
                    break;
                }
            }
            $user = Auth::guard('user_api')->user();
            $currentaddress = Adress::where(['curent' => 1, 'user_id' => $user->id])->first();
            if ($currentaddress == null)
                return 'curentaddressfalse';
            $customer_location = $currentaddress->building . ' ' . $currentaddress->street . ' ' . $currentaddress->city . ' ' . $currentaddress->district;
            $order = order::create([
                'branch_id' => $branchid,
                'customer_name' => $user->name,
                'customer_phone' => $user->phone,
                'country_code' => $user->country_code,
                'customer_location' => $customer_location,
                'order_by' => 'customer',
                'user_id' => $user->id,
                'lat' => $currentaddress->lat,
                'long' => $currentaddress->long,
                'urgent' => $urgent
            ]);
            foreach ($request->serviceprices as $serviceprice) {
                $price = $serviceprice['quantity'] * $serviceprice['price'];
                orderdetailes::create([
                    'order_id' => $order->id,
                    'branchitem_id' => $serviceprice['branchitem_id'],
                    'price' => $price,
                    'service_id' => $serviceprice['service_id'],
                    'quantity' => $serviceprice['quantity'],
                    'note' => $serviceprice['note']
                ]);
                $price_before_discount += $price;
                // this condation to add agent
                if ($serviceprice['argent'] != 0) {
                    $argentprice = Branchitem::select('argentprice')->where('id', $serviceprice['branchitem_id'])->first();
                    $argentprice = $serviceprice['argent'] * $argentprice->argentprice;
                    Argent::create([
                        'order_id' => $order->id,
                        'price' => $argentprice,
                        'quantity' => $serviceprice['argent'],
                        'service_id' => $serviceprice['service_id'],
                        'branchitem_id' => $serviceprice['branchitem_id'],
                    ]);
                    $price_before_discount += $argentprice;

                    // لو الوردر تم  امسح  الدسكونت اللي اتعمل عليه
                    /*     if ($order) {
                             $discount = Discount::where('type', 'new-user')->first();
                             if ($discount != null) {
                                 $userDiscount = UserDiscount::where('user_id', Auth::guard('user_api')->user()->id)->where('discount_id', $discount->id)->first();
                                 if ($userDiscount != null) {
                                     $userDiscount->delete();
                                 }
                             }
                         }*/
                }
            }
            foreach ($request->additionalservices as $additionalservice) {
                $price = $additionalservice['quantity'] * $additionalservice['price'];
                orderdetailes::create([
                    'order_id' => $order->id,
                    'branchitem_id' => $additionalservice['branchitem_id'],
                    'service_id' => $additionalservice['service_id'],
                    'price' => $price,
                    'additionalservice_id' => $additionalservice['additionalservice_id'],
                    'quantity' => $additionalservice['quantity']
                ]);
                $price_before_discount += $price;
            }

            $this->addordertaxes($order, $branchid, $price_before_discount);
            DB::commit();
            return $order->id;
        } catch (\Exception $ex) {
            DB::rollback();
            return false;
        }
    }

    // in this function user or laundery recive order from driver by scaning qr code
    public function reciveorder($request)
    {
        $order_id = $request->order_id;
        $confirm_type = $request->confirm_type;
        //start transaction
        //  try {
        DB::beginTransaction();
        $orderstatus = OrderDriveryStatus::where('order_id', $order_id)->latest('id')->first();
        $order = order::find($order_id);
        if ($order == null) {
            return false;
        }
        // if order delivery type home drop of laundry recive order from customer not driver
        if ($order->delivery_type_id == 3 && $orderstatus->order_status == 'pick_up_laundry')
            $data = $this->reciveorder_from_customer($order);
        // here customer recive order from laundry after finish when delivery type self delivery
        if ($order->delivery_type_id == 4 && $orderstatus->order_status == 'pick_up_laundry' || $order->delivery_type_id == 2 && $orderstatus->order_status == 'pick_up_laundry')
            $data = $this->reciveorder_from_laundry($order, $orderstatus);
        $driver_id = $order->driver_id;
        if ($confirm_type == 'drop_of_laundry') {
            $order_delivery_status = true;
            // dd($order);
            if ($orderstatus->order_status == 'drop_of_laundry') {
                if ($order->delivery_type_id == 4 || $order->delivery_type_id == 2)
                    $order_delivery_status = false;

                $orderstatus->update([
                    'confirmation' => true
                ]);
                OrderDriveryStatus::create([
                    'order_id' => $order_id,
                    'driver_id' => null,
                    'order_status' => 'pick_up_laundry',
                    'code' => rand(1000, 9999)
                ]);
                $driver_id = $order->driver_id;


                $order->update([
                    'driver_id' => null,
                    'delivery_status' => 'no_progress',
                    'progress' => 'inprogress',
                    'order_delivery_status' => $order_delivery_status,
                    'start_tracking' => false
                ]);
                // send notification to driver to remove qr code page
                $drivernotifytype = drivernotifytype::where('driver_id', $driver_id)->where('notificationtype_id', 3)->where('status', 1)->first();
                if ($drivernotifytype) {
                    $this->NotificationRepository->sendnotification('driver', $driver_id, 'recive order', 'recive order', 3, 5, ['order_id' => $order_id]);
                }

            }
            // the customer make order not laundry
            if ($order->user_id != null) {
                $user = User::find($order->user_id);
                // in case driver deliver order to laundry
                if ($order->delivery_type_id != 2) {
                    $title = 'تسليم الطلب ';
                    $body = 'تم تسليم طلبك من المغسله';
                    if ($user->lang == 'en') {
                        $title = 'order is recived ';
                        $body = 'your order is recived to laundry';
                    }
                    $titles = ['ar' => 'تسليم جديد', 'en' => 'order is recived'];
                    $bodys = ['ar' => 'تم تسليم الطلب الي المغسله', 'en' => 'your order is recived to laundry'];
                    $driver_name = Driver::select('name')->find($order->driver_id);
                    $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '3')->where('status', '1')->first();
                    if ($usernotifytype) {
                        $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 3, ['order_id' => $order_id]);
                        $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
                    }
                } // here customer create order self delivery and will deliver his order to laundry
                elseif ($order->delivery_type_id == 2) {
                    $title = 'recive order';
                    $body = 'recive order ';
                    $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 5, ['order_id' => $order_id]);
                }
            }

            $data['status'] = true;
            $data['message'] = 'laundry recived order successfully';
        } elseif ($confirm_type == 'drop_of_home') {
            // here driver give customer his order
            if ($orderstatus->order_status == 'drop_of_home') {
                // validation to make sure this request use only one time
                $this->checkreciveorder($orderstatus);
                $orderstatus->update([
                    'confirmation' => true
                ]);
                $order = order::find($order_id);
                $order->update([
                    'progress' => 'completed',
                    'delivery_status' => 'completed',
                    'drop_date' => now()->format('Y-m-d H:i:s'),
                    'order_time' => round((strtotime(Carbon::now()) - strtotime($order->created_at)) / 3600)
                ]);
                $price = $this->getprice($order);
                // this table for branch
                if ($order->pymenttype_id == 1 || $order->pymenttype_id == 3)
                    $this->TransactionRepository->activetransaction($order);

            }
            // send notification to driver to remove qr code from page of driver application and go to home
            $this->NotificationRepository->sendnotification('driver', $order->driver_id, 'recive order', 'recive order', 3, 5, ['order_id' => $order_id]);

            $data['status'] = true;
            $data['message'] = 'user recived order successfully';
            $data['data']['driver_id'] = $order->driver_id;
        }
        DB::commit();
        return $data;
        // } catch (\Exception $e) {
        //     DB::rollback();
        // }
    }

    public function getprice($order)
    {
        // check if order use discount if discount created by admin the branch balance don't effected
        if ($order->discounttype == null) {
            $price = $order->price_before_discount;
        }
        if ($order->discounttype == 'Admin') {
            $price = $order->price_before_discount;
        }
        if ($order->discounttype == 'branch') {
            $price = $order->price_after_discount;
        }
        return $price;
    }

    public function checkreciveorder($orderstatus)
    {
        if ($orderstatus->confirmation == 1) {
            $data['status'] = true;
            $data['message'] = 'user already recived order successfully';
            return $data;
        }
    }


    // in this function  laundery recive order from customer by scaning qr code
    public function reciveorder_from_customer($order)
    {
        $user_id = $order->user_id;
        $order_id = $order->id;
        $order = $order->update([
            'progress' => 'inprogress',
            'order_delivery_status' => true
        ]);

        // send notification to customer to remove qr c ode from page of customer application and go to home
        $this->NotificationRepository->sendnotification('user', $user_id, 'recive order', 'recive order', 3, 5, ['order_id' => $order_id]);

        $data['status'] = true;
        $data['message'] = 'laundry recived order successfully';
        return $data;
    }

    // in this function customer recive order from laundry
    public function reciveorder_from_laundry($order, $orderstatus)
    {

        // validation to make sure this request use only one time
        $this->checkreciveorder($orderstatus);
        $order->update([
            'progress' => 'completed',
            'payed' => true
        ]);

        // send notification to laundry to remove qr code from page of laundry application and go to home
        $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
        foreach ($Branchusers as $Branchuser) {
            if ($Branchuser->lang == 'ar') {
                $title = 'استلام طلب';
                $body = 'استلم العميل الطلب';
            }
            if ($Branchuser->lang == 'en') {
                $title = 'recive order';
                $body = 'customer recive order';
            }
            $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 3, 5, ['order_id' => $order->id]);
        }

        $price = $this->getprice($order);
        // if($order->pymenttype_id==1&&$order->pymenttype_id==3)
        // $this->createBalance($order,$price);
        if ($order->pymenttype_id == 1 || $order->pymenttype_id == 3)
            $this->TransactionRepository->activetransaction($order);

        $data['status'] = true;
        $data['message'] = 'customer recived order successfully';
        return $data;

    }

    public function unasignedorder($request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $orders = DB::table('orders')
            ->select('orders.id', 'orders.customer_name', 'orders.progress')
            ->where(['confirmation' => 'accepted', 'branch_id' => $branch_id, 'driver_id' => null])
            ->where('order_delivery_status', true)
            ->where('orders.progress', '!=', 'inprogress')
            ->wherein('delivery_type_id', [1, 4, 3])
            ->where('orders.order_delivery_status', true)
            ->orderby('orders.id', 'desc')
            ->get();
        // get service and put it under order
        $orders = $this->orderwithservice($orders, $lang);
        $data['status'] = true;
        $data['message'] = "get inprogress order successfully";
        $data['data']['orders'] = $orders;
        return $data;
    }

    // we use this function when recive order to know order info of it in branch application
    public function orderinfo($order_id, $lang, $code = null)
    {
        if ($code != null) {
            $OrderDriveryStatus = OrderDriveryStatus::where(['code' => $code, 'confirmation' => false])->latest()->first();
            if ($OrderDriveryStatus == null) {
                return $this->response(false, 'some thing is wrong');
            }
            $order_id = $OrderDriveryStatus->order_id;
        }
        $order = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('orders', 'orders.id', '=', 'order_detailes.order_id')
            ->join('order_delivery_status', 'order_detailes.order_id', '=', 'order_delivery_status.order_id')
            ->join('delivery_types', 'delivery_types.id', '=', 'orders.delivery_type_id')
            ->join('delivery_type_translations', 'delivery_type_translations.delivery_type_id', '=', 'delivery_types.id')
            ->select('orders.id', 'orders.customer_name', 'orders.customer_location as pick_up_location', 'delivery_type_translations.name as delivery_type', 'orders.driver_id', 'orders.taxes', 'orders.progress',
                'orders.discounttype', 'orders.price_before_discount', 'orders.price_after_discount', 'orders.delivery_fees', 'orders.payed', 'orders.rate', 'orders.delivery_type_id', 'order_delivery_status.order_status'
                , 'orders.start_tracking'
            )
            // ->selectRaw('sum(order_detailes.price) as price')
            ->groupBy('orders.id')
            ->groupBy('orders.customer_name')
            ->groupBy('orders.customer_location')
            ->groupBy('delivery_type_translations.name')  //

            ->groupBy('orders.discounttype')
            ->groupBy('orders.price_before_discount')
            ->groupBy('orders.price_after_discount')
            ->groupBy('orders.delivery_type_id')
            ->groupBy('orders.delivery_fees')
            ->groupBy('orders.driver_id')
            ->groupBy('orders.taxes')
            ->groupBy('orders.progress')
            ->groupBy('orders.payed')
            ->groupBy('orders.start_tracking')
            ->groupBy('order_delivery_status.order_status')
            ->groupBy('orders.rate')
            ->where('delivery_type_translations.locale', $lang)
            ->first();
        if ($order == null) {
            return response()->json(['status' => false, 'message' => 'this order not found'], 416);
        }
        // $orderargentprice=DB::table('orders')->where('orders.id',$order_id)
        // ->leftjoin('argent','orders.id','=','argent.order_id')
        // ->selectRaw('sum(argent.price) as argentprice')
        // ->groupBy('argent.order_id')
        // ->first();
        $order->price = $this->orderprice($order);
        // this query get services with count of item in it
        $services = $this->serive($order_id, null, $lang);
        // this query get items with count of item in it
        $items = $this->items($order_id, null, $lang);
        // this query to get additional service
        $additionals = $this->additionals($order_id, null, $lang);
        $argents = db::table('argent')->where('order_id', $order_id)->get();
        // but additional service in the item
        foreach ($items as $key => $item) {
            $item->additonalservice = [];
            foreach ($additionals as $additional) {
                if ($item->item_id == $additional->item_id && $item->service_id == $additional->service_id) {
                    array_push($item->additonalservice, $additional);
                }
            }
            //but argent inside item
            foreach ($argents as $argent) {
                $item->argent = 0;
                if ($item->item_id == $argent->branchitem_id) {
                    $item->argent = $argent->quantity;
                }
            }
        }
        //but argent inside item
        foreach ($services as $key => $service) {
            $service->item = [];
            foreach ($items as $item) {
                if ($item->service_id == $service->service_id) {
                    array_push($service->item, $item);
                }
            }
        }

        if ($order->payed == 1) {
            $order->payed = true;
        } else {
            $order->payed = false;
        }

        $OrderDetailes = OrderDetailes::where('order_id', $order_id)->get();

        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data']['order'] = $order;
        $data['data']['serives'] = $services;
        $data['data']['items'] = $items;
        $data['data']['additionals'] = $additionals;
        $data['data']['subtotals'] = $OrderDetailes->where('additionalservice_id', '==', null)->sum('price');
        $data['data']['orderDetails'] = OrderDetailes::where('order_id', $order_id)->get();
        $data['data']['orderargentprice'] = $argents->sum('price');
        //order fees
        $data['data']['price'] = $order->price;
        //other fees
        $data['data']['delivery_fees'] = $order->delivery_fees;
        $data['data']['taxes'] = $order->taxes;
        //total fees
        $data['data']['total'] = $order->price;
        $data['data']['price_before_discount'] = $order->price_before_discount + $order->delivery_fees;
        $data['data']['price_after_discount'] = $order->price_after_discount + $order->delivery_fees;
        //additionalPrice
        $data['data']['additionalPrice'] = $OrderDetailes->where('additionalservice_id', '!=', null)->sum('price');


        return $data;
    }

    // this request get delivery status of order (pick_up_laundry/drop_of_laundry/pick_up_home/drop_of_home)
    public function orderdeliverystatus($order_id, $lang)
    {
        $order = DB::table('orders')
            ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
            ->join('delivery_type_translations', 'delivery_type_translations.delivery_type_id', '=', 'orders.delivery_type_id')
            ->select('order_status', 'orders.progress', 'orders.confirmation', 'orders.delivery_status', 'orders.driver_id', 'orders.delivery_type_id', 'delivery_type_translations.name', 'orders.branch_id', 'order_delivery_status.order_status'
                , 'order_delivery_status.order_status', 'order_delivery_status.navigation', 'orders.start_tracking')
            ->where('order_delivery_status.confirmation', false)
            ->where('orders.confirmation', '!=', 'refused')
            ->where('orders.id', $order_id)
            ->where('delivery_type_translations.locale', $lang)
            ->first();

        // start check qr code
        $order->view_qrcode = false;
        // driver recive order from customer delivery_type (by delivery)
        if ($order->delivery_type_id == 1) {
            if ($order->order_status == 'pick_up_home' && $order->start_tracking == true)
                $order->view_qrcode = true;
        }
        // laundry recive order from customer delivery_type (self delivery)
        if ($order->delivery_type_id == 2) {
            if ($order->order_status == 'drop_of_laundry')
                $order->view_qrcode = true;
        }
        // laundry recive order from customer delivery_type (home drop of)
        if ($order->delivery_type_id == 3) {
            if ($order->order_status == 'pick_up_laundry' && $order->progress == 'no_progress')
                $order->view_qrcode = true;
        }
        // laundry recive order from customer delivery_type (self drop of)
        if ($order->delivery_type_id == 4) {
            if ($order->order_status == 'pick_up_home' && $order->start_tracking == true)
                $order->view_qrcode = true;
        }
        // end check qr code

        // after user recive hid order
        if ($order == null) {
            // self delivery
            $order = DB::table('orders')
                ->select('order_status', 'orders.progress', 'orders.confirmation', 'orders.delivery_status', 'orders.driver_id', 'order_delivery_status.order_status'
                    , 'delivery_type_translations.name', 'orders.branch_id', 'orders.start_tracking')
                ->join('delivery_type_translations', 'delivery_type_translations.delivery_type_id', '=', 'orders.delivery_type_id')
                ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
                ->where('orders.delivery_type_id', 2)
                ->where('orders.confirmation', '!=', 'refused')
                ->where('orders.checked', true)
                ->where('orders.id', $order_id)
                ->where('delivery_type_translations.locale', $lang)
                ->first();
            if ($order == null)
                return false;
            if ($order->confirmation == 'pending' && $order->progress == 'no_progress') {
                $order->progress = 'waiting for laundry confirmation';
                $order->status = 0;
            }
            if ($order->confirmation == 'accepted') {
                $order->progress = 'accepted';
                $order->status = 1;
            }

            // start check qr code
            $order->view_qrcode = false;
            // driver recive order from customer delivery_type (by delivery)
            if ($order->delivery_type_id == 1) {
                if ($order->order_status == 'pick_up_home' && $order->start_tracking == true)
                    $order->view_qrcode = true;
            }
            // laundry recive order from customer delivery_type (self delivery)
            if ($order->delivery_type_id == 2) {
                if ($order->order_status == 'drop_of_laundry')
                    $order->view_qrcode = true;
            }
            // laundry recive order from customer delivery_type (home drop of)
            if ($order->delivery_type_id == 3) {
                if ($order->order_status == 'pick_up_laundry' && $order->progress == 'no_progress')
                    $order->view_qrcode = true;
            }
            // laundry recive order from customer delivery_type (self drop of)
            if ($order->delivery_type_id == 4) {
                if ($order->order_status == 'pick_up_home' && $order->start_tracking == true)
                    $order->view_qrcode = true;
            }
            // end check qr code

            return $order;
        }

        //  start tracking of order
        if ($order->confirmation == 'pending' && $order->progress == 'no_progress') {
            $order->progress = 'waiting for laundry confirmation';
            $order->status = 0;
        }
        // by delivery
        if ($order->delivery_type_id == 1) {

            if ($order->confirmation == 'accepted' && $order->progress == 'no_progress' && $order->delivery_status == 'no_progress') {
                $order->progress = 'waiting for driver confirmation';
                $order->status = 1;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'no_progress' && $order->delivery_status == 'inprogress') {
                $order->progress = 'waiting for driver go to you';
                $order->status = 2;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'indelivery' && $order->order_status == 'drop_of_laundry') {

                $order->progress = 'waiting for driver go to laundry';
                $order->status = 3;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'finished' && $order->delivery_status == 'no_progress') {
                $order->progress = 'waiting for driver confirmation';
                $order->status = 1;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'finished' && $order->order_status == 'pick_up_laundry') {
                $order->progress = 'waiting for driver go to laundry';
                $order->status = 3;
            }

            if ($order->confirmation == 'accepted' && $order->delivery_status == 'inprogress' && $order->order_status == 'drop_of_home') {
                $order->progress = 'waiting for driver go to you';
                $order->status = 2;

            }

        } // self delivery
        elseif ($order->delivery_type_id == 2) {
            if ($order->confirmation == 'accepted' && $order->progress == 'no_progress' && $order->delivery_type_id == 2) {
                $order->progress = 'order accepted go to laundry';
                $order->status = 5;
            }
            if ($order->confirmation == 'accepted' && $order->progress == 'finished' && $order->delivery_type_id == 2) {
                $order->progress = 'order finished go to laundry to recive order';
                $order->status = 6;
            }
        } // home drop of
        elseif ($order->delivery_type_id == 3) {
            if ($order->confirmation == 'accepted' && $order->progress == 'no_progress') {
                $order->progress = 'order accepted go to laundry';
                $order->status = 5;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'finished' && $order->delivery_status == 'no_progress') {
                $order->progress = 'waiting for driver confirmation';
                $order->status = 1;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'finished' && $order->order_status == 'pick_up_laundry') {
                $order->progress = 'waiting for driver go to laundry';
                $order->status = 3;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'indelivery' && $order->delivery_status == 'inprogress' && $order->order_status == 'drop_of_home') {
                $order->progress = 'waiting for driver go to you';
                $order->status = 2;
            }

        } // self drop of
        elseif ($order->delivery_type_id == 4) {
            if ($order->confirmation == 'accepted' && $order->progress == 'no_progress' && $order->delivery_status == 'no_progress') {
                $order->progress = 'waiting for driver confirmation';
                $order->status = 1;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'no_progress' && $order->delivery_status == 'inprogress') {
                $order->progress = 'waiting for driver go to you';
                $order->status = 2;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'indelivery' && $order->order_status == 'drop_of_laundry') {
                $order->progress = 'waiting for driver go to laundry';
                $order->status = 3;
            }

            if ($order->confirmation == 'accepted' && $order->progress == 'finished') {
                $order->progress = 'order finished go to laundry to recive order';
                $order->status = 6;
            }

        }
        // end tracking of order


        // get data of delivery man if order in delivery
        if ($order->delivery_status == 'inprogress') {
            $driver = DB::table('drivers')
                ->leftjoin('drivers_rate', 'drivers_rate.driver_id', '=', 'drivers.id')
                ->select('name', 'img', 'country_code', 'phone', 'drivers.rate')
                ->distinct()
                ->where('drivers.id', $order->driver_id)
                ->first();
            if ($driver != null) {
                if ($driver->rate == null) {
                    $driver->rate = 0;
                }
                if ($driver->img == null) {
                    $driver->img = asset("/uploads/driver/img/default.png");
                } else {
                    $driver->img = asset("/uploads/driver/img/" . $driver->img);
                }
            }
            $order->driver = $driver;
        }


        return $order;
    }

    // this order summary for customer application this function is the same function in App\Http\Controllers\branch\orderController  (order info)
    public function ordersummary($request)
    {
        $lang = $request->header('lang');
        $order_id = $request->order_id;
        App::setLocale($lang);
        $deliverytype = delivery_type::select('id')->get()->makehidden('translations');
        $paymentmethods = payment_method::select('id')->get()->makehidden('translations');
        $orderdetailes = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'order_detailes.service_id')
            ->selectRaw('sum(price) as total')
            ->selectRaw('sum(quantity) as quantity')
            ->selectRaw('servicetranslations.name')
            ->where('order_detailes.order_id', $order_id)
            ->where('servicetranslations.locale', 'en')
            ->where('order_detailes.additionalservice_id', '=', null)
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();
        $order = order::find($order_id);
        $argentprice = Argent::where('order_id', $order_id)->sum('price');
        $additionalservice = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('additionalservicetranslations', 'additionalservicetranslations.additionalservice_id', '=', 'order_detailes.additionalservice_id')
            ->where('additionalservicetranslations.locale', $lang)
            ->selectRaw('additionalservicetranslations.name')
            ->selectRaw('sum(price) as price')
            ->selectRaw('quantity')
            ->where('order_detailes.additionalservice_id', '!=', null)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_detailes.quantity')
            ->get();
        $totaladditionalprice = 0;
        foreach ($additionalservice as $addtional) {
            $totaladditionalprice += $addtional->price;
        }

        $paymentmethods = $this->BranchRepository->get_paymet_methods($order->branch_id, $lang);

        $data['status'] = true;
        $data['message'] = "return order info succeffuly";
        $data['data']['deliverytype'] = $deliverytype;
        $data['data']['paymentmethods'] = $paymentmethods;
        $data['data']['orderdetailes'] = $orderdetailes;
        $data['data']['argentprice'] = $argentprice;
        $data['data']['delivery_fees'] = $order->delivery_fees;
        $data['data']['additionalservices'] = $additionalservice;
        $data['data']['totaladditionalprice'] = $totaladditionalprice;
        $data['data']['price_after_discount'] = $order->price_after_discount;
        $data['data']['price_before_discount'] = $order->price_before_discount;
        $data['data']['paymentmethods'] = $paymentmethods;
        $data['data']['taxes'] = $order->taxes;
        return $data;
    }

    // this function get new orders in laundry abblication that created by user
    public $orders_id = [];
    public $items = [];

    public function neworders($request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $orders = DB::table('orders')->select('orders.id as order_id', 'users.name as customer_name', 'customer_location', 'delivery_type_translations.name as order_status', 'discounttype', 'price_before_discount', 'price_after_discount', 'delivery_fees', 'orders.payed')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->join('order_detailes', 'orders.id', '=', 'order_detailes.order_id')
            ->join('delivery_types', 'delivery_types.id', '=', 'orders.delivery_type_id')
            ->join('delivery_type_translations', 'delivery_type_translations.delivery_type_id', '=', 'delivery_types.id')
            //->selectRaw('sum(order_detailes.price) as fees')

            ->where('checked', 1)->where('confirmation', 'pending')
            ->where('delivery_type_translations.locale', $lang)
            ->where('orders.branch_id', $branch_id)
            ->groupBy('orders.id')  //
            ->groupBy('orders.payed')
            ->groupBy('users.name')
            ->groupBy('orders.discounttype')
            ->groupBy('orders.price_before_discount')
            ->groupBy('orders.price_after_discount')
            ->groupBy('orders.delivery_fees')
            ->groupBy('delivery_type_translations.name')
            ->groupBy('orders.customer_location')
            ->orderBy('orders.id', 'desc')
            ->get();

        foreach ($orders as $order) {
            $order->fees = round($this->orderprice($order), 5);
            array_push($this->orders_id, $order->order_id);
            array_push($this->items, $this->items($order->order_id, null, $lang));
        }
        $orders = $this->orderserives($orders, $lang);
        return $orders;
    }

    // this is show order information from customer
    public function neworderinfo($request)
    {
        $order_id = $request->order_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('orders', 'orders.id', '=', 'order_detailes.order_id')
            ->join('users', 'orders.user_id', '=', 'users.id')
            ->select('orders.id as order_id', 'users.name as customer_name', 'orders.customer_location')
            ->selectRaw('sum(order_detailes.price) as fees')
            ->groupBy('orders.id')
            ->groupBy('users.name')
            ->groupBy('orders.customer_location')
            ->first();
        if ($order == null) {
            return false;
        }
        $orderargentprice = DB::table('orders')->where('orders.id', $order_id)
            ->leftjoin('argent', 'orders.id', '=', 'argent.order_id')
            ->selectRaw('sum(argent.price) as argentprice')
            ->groupBy('argent.order_id')
            ->first();
        $order->fees = $order->fees + $orderargentprice->argentprice;
        // this query get services with count of item in it
        $services = $this->serive($order_id, null, $lang);
        // this query get items with count of item in it
        $items = $this->items($order_id, null, $lang);
        // this query to get additional service
        $additionals = $this->additionals($order_id, null, $lang);
        $argents = db::table('argent')->where('order_id', $order_id)->get();
        // but additional service in the item
        foreach ($items as $key => $item) {
            $item->additonalservice = [];
            foreach ($additionals as $additional) {
                if ($item->item_id == $additional->item_id && $item->service_id == $additional->service_id) {
                    array_push($item->additonalservice, $additional);
                }
            }
            //but argent inside item
            foreach ($argents as $argent) {
                $item->argent = 0;
                if ($item->item_id == $argent->branchitem_id) {
                    $item->argent = $argent->quantity;
                }
            }
        }
        //but argent inside item
        foreach ($services as $key => $service) {
            $service->item = [];
            foreach ($items as $item) {
                if ($item->service_id == $service->service_id) {
                    array_push($service->item, $item);
                }
            }
        }
        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data']['order'] = $order;
        $data['data']['serives'] = $services;
        return $data;
    }

    // accept order in branch from user
    public function acceptorder($order_id, $branch_id, $branch_user_id)
    {
        //  try{
        DB::beginTransaction();
        //$branch_user_id=Auth::guard('branchuser-api')->user()->id;
        $order = order::where(['id' => $order_id, 'branch_id' => $branch_id])->first();
        if ($order == null)
            return false;

        if ($order->confirmation == 'accepted')
            return ['message' => App::getlocale() == 'ar' ? 'الطلب مقبول بالفعل' : 'order is already accepted'];

        if ($order->confirmation == 'refused')
            return ['message' => App::getlocale() == 'ar' ? 'الطلب مرفوض' : 'order is refused'];

        $order->update([
            'confirmation' => 'accepted'
        ]);
        Accept_order::create([
            'order_id' => $order->id,
            'branch_id' => $order->branch_id,
            'branch_user_id' => $branch_user_id
        ]);


        /* if ($order->pymenttype_id == 1) {
             $walletamount = $this->WalletRepository->getpallence($order->user_id);
             if ($walletamount < $order->price_before_discount) {
                 return $this->response(false, App::getlocale() == 'ar' ? 'رصيدك غير كافي' : 'you have not enough mony in wallet');
             }
             //$this->WalletRepository->increesebrnachwallet($order->branch_id,$usediscount);
             $this->WalletRepository->decreaseuserwallet($order->price_before_discount, $order->user_id);
         }

         // create balance transaction
         if ($order->pymenttype_id == 1 || $order->pymenttype_id == 3) {
             $price = $this->getprice($order);
             $this->TransactionRepository->createtransaction($order, $price);
         }*/


        $urgents = Argent::where('order_id', $order->id)->get();
//        \Mail::to($order->user->email)->send(new \App\Mail\InvoiceMail($order, $urgents));
        $user = User::find($order->user_id);
        $title = 'تم قبول طلبك ';
        $body = 'تم قبول طلبك وجاري تنفيذه';
        if ($user->lang == 'en') {
            $title = 'Order Accepted';
            $body = 'Laundry has accepted your order';
        }
        $titles = ['ar' => 'تم قبول طلبك', 'en' => 'Order Accepted'];
        if ($order->delivery_type_id != 3) {
            $bodys = ['ar' => 'تم قبول طلبك وجاري تنفيذه', 'en' => 'Laundry has accepted your order'];
        } else {
            $bodys = ['ar' => 'تم قبول طلبك  من فضبك توجه الي المغسله لتسليم الطلب', 'en' => 'your order is accepted please go to laundry '];
        }
        $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 0, ['order_id' => $order_id]);
        $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
        DB::commit();
        return true;
        //     }catch(\Exception $ex){
        //         DB::rollback();
        //         return false;
        // }
    }

    public function cancelorder($request, $branchid, $fort_id = null)
    {
        $order_id = $request->order_id;
        $user = Auth::guard('user_api')->user();
        $order = order::where('id', $order_id)->where('id', $order_id)->where('user_id', $user->id)->first();
        if ($order == null) {
            return false;
        } else {
            // محتاجين نرجع فلوس  العميل  للطريقه الي دفع بيها
            if ($order->pymenttype_id == 1) {
                $this->WalletRepository->increeseuserwallet($order->price_before_discount, $user->id);
            }
            //  visa or master card
            if ($order->pymenttype_id == 3) {
                error_reporting(E_ALL);
                ini_set('display_errors', '1');
                $access_code = 'ECLmBCXFjMVcehgasQ8d';
                $merchant_identifier = '00f1ca9c';
                $sha_request_phrase = '46PXPeVmlpRrScEAVgVV9U)]';
                $sha_type = 'sha256';
                $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';

                $amount = $order->price_before_discount;
                $currency = 'SAR';

                $shaString = '';
                $arrData = array(
                    'command' => 'REFUND',
                    'access_code' => $access_code,
                    'merchant_identifier' => $merchant_identifier,
                    'amount' => $amount * 100,
                    'currency' => 'SAR',
                    'language' => $request->header('lang'),
                    'fort_id' => $fort_id,
                    'order_description' => 'iPhone 14 pro  max',
                );
                ksort($arrData);
                foreach ($arrData as $key => $value) {
                    $shaString .= "$key=$value";
                }

                $shaString = $sha_request_phrase . $shaString . $sha_request_phrase;
                $signature = hash($sha_type, $shaString);


                $arrDataSignature = array(
                    'command' => 'REFUND',
                    'access_code' => $access_code,
                    'merchant_identifier' => $merchant_identifier,
                    'amount' => $amount * 100,
                    'currency' => 'SAR',
                    'language' => $request->header('lang'),
                    'signature' => $signature,
                    'fort_id' => $fort_id,
                    'order_description' => 'iPhone 14 pro  max',
                );

                $ch = curl_init($url);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($arrDataSignature));
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'Content-Length: ' . strlen(json_encode($arrDataSignature))));
                $result = curl_exec($ch);
                curl_close($ch);
                $result = json_decode($result);
                return $result;


            }

            $order->delete();
            return true;
        }
    }

    /*  public function cancelorder($request, $branchid)
      {
          $order_id = $request->order_id;
          $user = Auth::guard('user_api')->user();
          $order = order::where('id', $order_id)->where('id', $order_id)->where('user_id', $user->id)->first();
          if ($order == null) {
              return false;
          } else {
              $order->delete();
              return true;
          }
      }*/

    public function generatecode($order_id)
    {
        $orderstatus = OrderDriveryStatus::where(['confirmation' => false, 'order_id' => $order_id])->first();
        $orderstatus->update([
            'code' => rand(1000, 9999),
        ]);
        return true;
    }

    public function getcode($status, $order_id)
    {
        $orderstatus = OrderDriveryStatus::where(['confirmation' => false, 'order_status' => $status, 'order_id' => $order_id])->first();
        if ($orderstatus == null)
            return false;
        return $orderstatus->code;
    }

    public function reciveorderbycode($order_id, $code)
    {
        $orderstatus = OrderDriveryStatus::where(['confirmation' => false, 'order_id' => $order_id])->first();
        //$orderstatus

    }

    public function finishorder($id, $branchid)
    {
        $order = Order::where(['id' => $id, 'branch_id' => $branchid])->first();
        if ($order == null)
            return false;
        $order->update([
            'progress' => 'finished',
            'driver_id' => null
        ]);
        $amount = $order->price_after_discount != null ? $order->price_after_discount : $order->price_before_discount;
        $this->OrderRepository->transformmonytopoints($amount);
        if ($order->delivery_type_id == 4 || $order->delivery_type_id == 2) {
            $user = User::find($order->user_id);
            if ($user != null) {
                $title = 'تم انهاء طلبك ';
                $body = 'من فضلك توجه الي المغسله لاستلام طلبك';
                if ($user->lang == 'en') {
                    $title = 'order is finished';
                    $body = 'please go to laundry to recive your order';
                }
                $titles = ['ar' => 'تم انهاء الطلب', 'en' => 'order is finished'];
                $bodys = ['ar' => 'من فضلك توجه الي المغسله لاستلام الطلب', 'en' => 'please go to laundry to recive your order'];
                $driver_name = Driver::select('name')->find($order->driver_id);
                $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 3, ['order_id' => $order->id]);
                $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
            }
        }
        return true;
    }

    public function check_delivery_order($branch_id)
    {
        $deliveryorder = order::where(['branch_id' => $branch_id, 'checked' => true, 'confirmation' => 'accepted'])
            ->where('progress', '!=', 'completed')->wherein('delivery_type_id', [2, 3])
            ->count();
        return $deliveryorder;
    }

    public function transformmonytopoints($mony)
    {
        $user = Auth::guard('user_api')->user();
        $user->update([
            'points' => $user->points + $mony
        ]);
    }

    // تنقيص النقط من المستخدم
    public function decreasepoints($points, $user_id = null)
    {
        $user = User::find($user_id);
        $user->update([
            'points' => $user->points - $points
        ]);
    }


    // use to check out order for branch
    public function checkorder($request, $branchid)
    {
        //dd($request->all());
        $order_id = $request->order_id;
        $order_delivery_status = true;
        $order = order::where('id', $order_id)->where('branch_id', $branchid)->first();
        if ($order == null) {
            $data['status'] = false;
            $data['message'] = 'order not found';
            return response()->json($data, 405);
        } else {
            if ($order->checked == true) {
                return $this->response(false, 'this order alerdy checked', $data = null, 422);
            }
            // if order found
            DB::beginTransaction();
            // delvivery consisit of threemain type(self delivery - one way delivery - by delivery)


            if ($request->delivery_fees != null)
                $this->add_delivery_fees($order, round((int)$request->delivery_fees, 0, PHP_ROUND_HALF_DOWN));

            if ($request->delivery_type == 'bydelivery') {
                $delivery_type_id = 1;
                $order_status = 'pick_up_home';
            } elseif ($request->delivery_type == 'on_way_delivery') {
                //start validate way of delivery
                $validator = Validator::make($request->all(), [
                    'way_delivery' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                        'message' => $validator->messages()->first()
                    ], 422);
                }

                //  end validate way of delivery
                if ($request->way_delivery == 'home_drop_of') {
                    $delivery_type_id = 3;
                    $order_delivery_status = false;
                    $order_status = 'drop_of_laundry';
                } elseif ($request->way_delivery == 'self_drop_of') {
                    $delivery_type_id = 4;
                    $order_status = 'pick_up_home';
                } else {
                    return response()->json(['message' => 'the way delivery input is false'], 422);
                }
            } elseif ($request->delivery_type == 'self_delivery') {
                $order_delivery_status = false;
                $delivery_type_id = 2;
                $order_status = null;
                OrderDriveryStatus::create([
                    'order_id' => $order->id,
                    'driver_id' => null,
                    'order_status' => 'drop_of_laundry',
                    'code' => rand(999, 9999),
                ]);
            }
            // we will remove this from here
            $order->update([
                'day' => $request->day,
                'from' => $request->from,
                'to' => $request->to,
                'delivery_type_id' => $delivery_type_id,
                'order_delivery_status' => $order_delivery_status,
                'checked' => true
            ]);
            if ($order->delivery_type_id == 1 || $order->delivery_type_id == 3 || $order->delivery_type_id == 4) {
                OrderDriveryStatus::create([
                    'order_id' => $order->id,
                    'driver_id' => null,
                    'order_status' => $order_status,
                    'code' => rand(1000, 9999),
                ]);
            }


            $user = User::where('phone', $order->customer_phone)->first();
            if ($user != null)
                $order->update(['user_id' => $user->id]);

            DB::commit();
            $data['status'] = true;
            $data['message'] = 'order checled  succefully';
            return response()->json($data);
        }
    }

    // we use this function to reject order
    public function rejectorder($request, $branch_user_id)
    {

        $order = Order::find($request->order_id);
        if ($order == null)
            return response()->json(['status' => false, 'message' => 'this order not found'], 401);


        if ($order->confirmation == 'accepted')
            return ['message' => 'order is already accepted'];

        if ($order->confirmation == 'refused')
            return ['message' => 'order is already refused'];


        $order->update([
            'confirmation' => 'refused'
        ]);
        $amount = $order->price_before_discount;
        if ($order->discounttype != null)
            $amount = $order->price_after_discount;
        $this->WalletRepository->increeseuserwallet($order->user_id, $amount);
        $this->SystemRepository->decreasebalance($amount);
        $user = User::find($order->user_id);
        $title = 'تم رفض طلبك ';
        $body = 'تم رفض طلبك  ';
        if ($user->lang == 'en') {
            $title = 'your order is rejected';
            $body = 'your order is rejected and will done';
        }
        $titles = ['ar' => 'تم رفض طلبك ', 'en' => 'your order is rejected'];
        $bodys = ['ar' => 'تم رفض طلبك ', 'en' => 'your order is rejected and will done'];
        $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 1, ['order_id' => $request->order_id]);
        $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
        $reject_order = Reject_order::create([
            'order_id' => $order->id,
            'branch_id' => $order->branch_id,
            'branch_user_id' => $branch_user_id
        ]);

        return true;

    }

    public function add_delivery_fees($order, $delivery_fees)
    {
        $price_after_discount = null;
        if ($order->discounttype != null)
            $price_after_descount = $order->price_after_discount + $delivery_fees;
        $order->update([
            'delivery_fees' => $delivery_fees,
            'price_before_discount' => $order->price_before_discount + $delivery_fees,
            'price_after_discount' => $price_after_discount
        ]);
        return true;
    }

    // start cancelation order
    public function ordercancelation($order, $request)
    {
        switch ($order->progress) {
            case 'inprogress':
                return false;
                break;
            case 'no_progress':
                // driver  accept order yet
                if ($order->delivery_status == 'inprogress') {
                    if ($order->payed == 0) {
                        $this->WalletRepository->decreaseuserwallet($order->delivery_fees, $order->user_id);
                    } else {
                        $amount = $this->orderprice($order) - $order->delivery_fees;
                        // return money for user
                        $this->WalletRepository->increeseuserwallet($order->user_id, $amount);
                        // decrese balance balance of system
                        $this->SystemRepository->decreasebalance($amount);
                        // decrease balance of branch
                        $this->TransactionRepository->decrese_brnach_balance($order, $amount, true);
                        // create_externance_transaction for branch
                        $this->TransactionRepository->create_externance_transaction($order, $amount);
                        // create_externance_transaction for system
                        $this->TransactionRepository->create_externance_system_transaction($order, $amount);
                    }
                } else {
                    // driver didn't accept order yet
                    if ($order->payed == true) {
                        // return money for user
                        $this->WalletRepository->increeseuserwallet($order->user_id, $this->orderprice($order));
                        // decrese balance balance of system
                        $this->SystemRepository->decreasebalance($this->orderprice($order));
                        // decrease balance of branch
                        $this->TransactionRepository->decrese_brnach_balance($order, $this->orderprice($order), false);
                        // create_externance_transaction for branch
                        $this->TransactionRepository->create_externance_transaction($order, $this->orderprice($order));
                        // create_externance_transaction for system
                        $this->TransactionRepository->create_externance_system_transaction($order, $this->orderprice($order));
                    }
                }
                break;
        }
        $order->update([
            'checked' => false,
            'canceled' => true
        ]);

        return true;
    }

    public function orderprice($order)
    {
        if ($order->discounttype == null) {
            return $order->price_before_discount;
        } else {
            return $order->price_after_discount;
        }
    }
    // end cancelation order

    // start calculate taxes
    function addordertaxes($order, $branchid, $price_before_discount)
    {
        $branch = branch::select('laundry_id')->find($branchid);
        $laundry = Laundry::select('taxes', 'taxamount')->find($branch->laundry->id);

        if ($order->discount_type == 1) {
            $price_before_discount = $price_before_discount - ($price_before_discount * $order->discount_value / 100);
        } else {
            $price_before_discount = $price_before_discount - $order->discount_value;
        }

        if ($laundry->taxes == true) {
            $taxes = $this->calculatetaxes($price_before_discount, $laundry->taxamount) - $price_before_discount;
            $price_before_discount = $this->calculatetaxes($price_before_discount, $laundry->taxamount);
            $order->update([
                'price_before_discount' => $price_before_discount,
                'taxes' => $taxes
            ]);
        } else {
            $order->update([
                'price_before_discount' => $price_before_discount,
            ]);
        }
        return true;
    }

    function calculatetaxes($price, $taxamount)
    {
        $tax = $price * ($taxamount / 100);
        $price = $price + $tax;
        return $price;
    }

    // end calculate taxes
}
