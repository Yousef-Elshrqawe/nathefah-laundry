<?php

namespace App\Repositories;
use App\Interfaces\BranchRepositoryInterface;
use App\Models\Laundry\{branch,Branchuser,branchservice};
use App\Models\laundryservice\{branchAdditionalservice};
use Illuminate\Support\Facades\DB;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Auth;
use App\Traits\otp;

use Illuminate\Support\Str;


class BranchRepository implements BranchRepositoryInterface
{
    use otp;
    public function getopentime($branchid){
        $opentime=branch::select('open_time','closed_time')->find($branchid);
        if($opentime==null)
        return false;
        return $opentime;
    }
    public function checkphone($request){
       $country_code=$request->country_code;
       $phone=$request->phone;
       $branchuser=Branchuser::where(['phone'=>$phone,'country_code'=>$country_code])->first();
       if($branchuser==null){
        return false;
       }
       $branchuser->update([
           'otp'=> rand(1000,9999),
       ]);

        $message = $this->sendSmsMessage($request, 'branchUser', __('auth.Your Nathefah verification code is') . ' ' . $branchuser->otp , $branchuser->otp);

        return $branchuser;
    }

    public function getlaundries($lat,$long){
        $branchs=branch::select('id','username','laundry_id')->whereHas('branchservices',function($q)use($request){
            $q->wherein('service_id',$request->services);
          })->with('laundry',function($q){
            $q->select('name','id')->get();
          })->get();
    }
    public function branchrate($rate){
        if($rate==null){
            $rate='No Rate';
        }elseif($rate>4||$rate<5){
            $rate='very good';
        }
        elseif($rate>3||$rate<4){
            $rate='good';
        }
        elseif($rate>2||$rate<3){
            $rate='acceptable';
        }
        elseif($rate>1||$rate<2){
            $rate='bad';
        }
        elseif($rate>0||$rate<1){
            $rate='very bad';
        }
        return $rate;
    }

  public  function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
          return 0;
        }
        else {
          $theta = $lon1 - $lon2;
          $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
          $dist = acos($dist);
          $dist = rad2deg($dist);
          $miles = $dist * 60 * 1.1515;
          $unit = strtoupper($unit);

          if ($unit == "K") {
            return ($miles * 1.609344);
          } else if ($unit == "N") {
            return ($miles * 0.8684);
          } else {
            return $miles;
          }
        }
    } // end of distance

    public function checkbranchservices(){
      //  $branch=
    }


     public function get_paymet_methods($branch_id,$lang){
        $paymentmethods=DB::table('branch_payments')
        ->join('payment_method_translations','payment_method_translations.payment_method_id','=','branch_payments.payment_method_id')
        ->join('payment_methods','payment_methods.id','=','branch_payments.payment_method_id')
        ->select('branch_payments.id','payment_method_translations.name','branch_payments.status','payment_method_translations.name'
        ,'payment_methods.img','branch_payments.payment_method_id','payment_methods.payment_key')
        ->where('payment_method_translations.locale',$lang)
        ->where('branch_payments.branch_id',$branch_id)
        ->get();

        foreach($paymentmethods as $payment){
            $payment->img=asset('payment_imgs/'.$payment->img);
         }

         return $paymentmethods;

    }





    // rate of older
    // $laundries=DB::table('laundries')
    // ->join('branchs','branchs.laundry_id','=','laundries.id')
    // ->select('laundries.name')
    // ->selectRaw('avg(branchs.rate) as rate')
    // ->groupBy('laundries.name')
    // ->orderBy('rate','desc')
    // ->get();
}
