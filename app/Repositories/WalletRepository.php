<?php

namespace App\Repositories;
use App\Interfaces\WalletRepositoryInterface;
use App\Models\Wallet\{user_wallet,user_free_wallet,Walletlogs,branch_wallet};
use Illuminate\Support\Facades\DB;
use App\Traits\response;
use Carbon\Carbon;
use Validator;
use Auth;

class WalletRepository implements WalletRepositoryInterface
{

   public function getfreewallet(){
        $user_id=Auth::guard('user_api')->user()->id?? 0;
        $user_free_wallets=user_free_wallet::select('amount','id')
        ->where('end_date','>=',Carbon::now()->toDateTimeString())
        ->where('user_id',$user_id)
        ->where('amount','!=',0)
        ->orderBy('end_date','asc')
        ->get();
        return $user_free_wallets;
   }
   public function getpallence($user_id){
        // $user_id=auth()->guard('user_api')->user()->id;
        // $user_id=Auth::guard('user_api')->user()->id;
        // return $user_id;
        $wallet=user_wallet::where('user_id',$user_id)->first();

        $user_free_wallets=$this->getfreewallet();
        $amount=$wallet->amount;
        if(!$user_free_wallets->isEmpty()){
           foreach($user_free_wallets as $user_free_wallet){
              $amount+=$user_free_wallet->amount;
           }
        }
        return $amount;
   }
   public function increesebrnachwallet($branch_id,$amount){
     $branch_wallet=branch_wallet::where('branch_id',$branch_id)->first();
     if($branch_wallet!=null){
        $amount=$branch_wallet->amount+$amount;
        $branch_wallet->update([
            'amount'=>$amount
        ]);
     }
     return true;
   }
   public function decreaseuserwallet($amount,$user_id){
        $dss=user_free_wallet::where('user_id',$user_id)->get();
        // $user_id=Auth::guard('user_api')->user()->id;
        $wallet=user_wallet::where('user_id',$user_id)->first();
        $user_free_wallets=$this->getfreewallet();
        foreach($user_free_wallets as $user_free_wallet){
            if($user_free_wallet->amount<$amount){
                $amount=$amount-($user_free_wallet->amount);
                $user_free_wallet->update([
                    'amount'=>0
                  ]);
            }
            if($user_free_wallet->amount>$amount){
                $user_free_wallet->update([
                    'amount'=>$user_free_wallet->amount-$amount
                ]);
                return true;
            }
            if($user_free_wallet->amount==$amount){
                $user_free_wallet->update([
                  'amount'=>0
                ]);
                return true;
              }
        }
        $wallet->update([
            'amount'=>$wallet->amount-$amount
        ]);
        return true;
   }
   public function increeseuserwallet($user_id,$amount){
    $user_wallet=user_wallet::where('user_id',$user_id)->first();
    if($user_wallet!=null){
       $amount=$user_wallet->amount+$amount;
       $user_wallet->update([
           'amount'=>$amount
       ]);
    }
    return true;
   }
}
