<?php


namespace App\Repositories;


use App\Interfaces\SubscriptionRepositoryInterface;
use App\Models\Subscription\LaundryPackage;
use App\Models\Subscription\Period;
use App\Models\Subscription\Package;
use App\Models\Balance\Balance;
use App\Models\SystemTransaction;
use App\Models\System\Systeminfo;
use Carbon\Carbon;


class SubscriptionRepository implements SubscriptionRepositoryInterface
{
   public function subscripe($request,$laundry_id){
    $package=Package::find($request->package_id);
    $packagesprice=Package::where('min_branch','<',$package->min_branch)
    ->where('period_id',$package->period_id)
    ->selectRaw('sum(price) as price')
    ->GroupBy('period_id')
    ->first();
    $end_date = Carbon::now();
    if($packagesprice!=null)
    $packagesprice=$packagesprice->price;
    if($packagesprice==null)
    $packagesprice=0;
    $price=$package->price+$packagesprice;

    switch ($package->period->duration_unit) {
        case 'day':
            $end_date->addDays($package->period->duration_value);
            break;
        case 'week':
            $end_date->addWeeks($package->period->duration_value);
            break;
        case 'month':
            $end_date->addMonths($package->period->duration_value);
            break;
        case 'year':
            $end_date->addYears($package->period->duration_value);
            break;
    }
    LaundryPackage::create([
      'laundry_id'=>$laundry_id,
      'package_id'=>$request->package_id,
      'price'=>$price,
      'start_date'=>Carbon::now(),
      'end_date'=>$end_date
    ]);
    $Systeminfo= Systeminfo::latest()->first();
    $Systeminfo->balance=$Systeminfo->balance+$price;
    $Systeminfo->save();
    $transaction=SystemTransaction::create([
        'amount'=>$price,
        //'branch_id'=>$order->branch_id,
        'laundry_id'=>$laundry_id,
        'system_balance'=>$Systeminfo->balance,

        'type'=>'balance',
        'transaction'=>'enternance',
        'transaction_number'=>$request->fort_id,
        'transaction_type'=>'subscription',

    ]);
   }

   public function checkbranchcount($laundry_id){
     $laundrypackage=LaundryPackage::with('package')->where('laundry_id',$laundry_id)->first();

     $max_branch=$laundrypackage->package->first()->max_branch;

     return $max_branch;
   }


}
