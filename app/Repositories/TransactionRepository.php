<?php

namespace App\Repositories;
use App\Interfaces\TransactionRepositoryInterface;
use App\Models\Laundry\branch;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Discount\{BranchDiscount,UserDiscount,Discount};
use App\Models\Order\{order,OrderDiscount};
use Illuminate\Support\Facades\DB;
use App\Models\System\Systeminfo;
use App\Models\{ApplicationRates, Laundry\Branchuser, Transaction, SystemTransaction};
use Auth;

class TransactionRepository implements TransactionRepositoryInterface
{

   // الترانزاكشن بتعبر عن  عمليات البيع والشراء المرتبطه بالرصيد والتي تمت عن طريق الدفع بالمحفظه او الفيزا
   // create transaction of branch

   public function createtransaction($order,$price){



        // add Transaction to system
        $Systeminfo= Systeminfo::latest()->first();

       $application_percentage=ApplicationRates::latest()->first();
       $application_percentage=$price*($application_percentage->rate/100);
       $old_price= $price -  $application_percentage ;

        // calcualte  Transaction for branch
        $branch=branch::find($order->branch_id);
        $branch->pending_balance=$branch->pending_balance+$old_price;
        $branch->save();


        $fort_id=null;
        if($order->fort_id!=null);
        $fort_id=$order->fort_id;

        $type='cache';
        if($order->pymenttype_id==1||$order->pymenttype_id==3)
        $type='balance';

        $payment_type='';
        if ($order->pymenttype_id==3) {
            $payment_type='visa';
        }else{
            $payment_type='wallet';
        }

        $Transaction=Transaction::create([
            // هنا يتم حساب نسبه التطبيق
            'amount'=>$old_price,
            'order_id'=>$order->id,
            'branch_id'=>$order->branch_id,

             // هنا  بيتم عرض الرصيد اللي المفوض هيبقي معاه بعد ميخلص الطلب كامل
            'branch_balance'=>/*$branch->balance+*/$branch->pending_balance,
            'system_balance'=>$Systeminfo->balance,
            'application_percentage'=>$application_percentage,

            'type'=>$type,
            'transaction'=>'enternance',
            'transaction_number'=>$fort_id,
            'transaction_type'  =>'order',
            'payment_type'      =>$payment_type
        ]);

   }


   // create transaction of main system in this function the balance is increase
   public function create_system_transaction($order,$price){

            $Systeminfo= Systeminfo::latest()->first();
            $Systeminfo->balance=$Systeminfo->balance+$price*(98/100);
            $Systeminfo->save();

            $fort_id=null;
            if($order->fort_id!=null);
            $fort_id=$order->fort_id;

            $type='cache';
            if($order->pymenttype_id==1||$order->pymenttype_id==3)
            $type='balance';


            $payment_type='';
            if ($order->pymenttype_id==3) {
                $payment_type='visa';
            }else{
                $payment_type='wallet';
            }

            $Transaction=SystemTransaction::create([
                // هنا يتم حساب نسبه التطبيق
                'amount'=>$price*(98/100),
                'order_id'=>$order->id,
                'branch_id'=>$order->branch_id,
                //'branch_balance'=>$branch->balance+$branch->pending_balance,
                'system_balance'=>$Systeminfo->balance,
                'type'=>$type,
                'transaction'=>'enternance',
                'transaction_number'=>$fort_id,
                'transaction_type'  =>'order',
                'payment_type'      =>$payment_type
            ]);

   }


   public function create_externance_transaction($order,$price){

         // add Transaction to system
         $Systeminfo= Systeminfo::latest()->first();

         // calcualte  Transaction for branch
         $branch=branch::find($order->branch_id);
         $branch->pending_balance=$branch->pending_balance-$price*(98/100);
         $branch->save();


         $fort_id=null;
         if($order->fort_id!=null);
         $fort_id=$order->fort_id;

         $type='cache';
         if($order->pymenttype_id==1||$order->pymenttype_id==3)
         $type='balance';


         $Transaction=Transaction::create([
            // هنا يتم حساب نسبه التطبيق
            'amount'=>$price*(98/100),
            'order_id'=>$order->id,
            'branch_id'=>$order->branch_id,

             // هنا  بيتم عرض الرصيد اللي المفوض هيبقي معاه بعد ميخلص الطلب كامل
            'branch_balance'=>$branch->balance+$branch->pending_balance,
            'system_balance'=>$Systeminfo->balance,

            'type'=>$type,
            'transaction'=>'externance',
            'transaction_number'=>$fort_id,
            'transaction_type'  =>'order',
        ]);



   }

   public function create_externance_system_transaction($order,$price){

    $Systeminfo= Systeminfo::latest()->first();

    $fort_id=null;
    if($order->fort_id!=null);
    $fort_id=$order->fort_id;

    $type='cache';
    if($order->pymenttype_id==1||$order->pymenttype_id==3)
    $type='balance';

    $Transaction=SystemTransaction::create([
        // هنا يتم حساب نسبه التطبيق
        'amount'=>$price*(98/100),
        'order_id'=>$order->id,
        'branch_id'=>$order->branch_id,

        //'branch_balance'=>$branch->balance+$branch->pending_balance,

        'system_balance'=>$Systeminfo->balance,

        'type'=>$type,
        'transaction'=>'externance',
        'transaction_number'=>$fort_id,
        'transaction_type'  =>'order',
    ]);
   }






   // in this function active transaction when customer recive his order from branch of laundry
   public function activetransaction($order){
        $Transaction=Transaction::where('order_id',$order->id)->first();
       if ($Transaction != null){
           $Transaction->pending=false;
           $Transaction->save();
           $branch=branch::find($order->branch_id);
           $branch->pending_balance=$branch->pending_balance - $Transaction->amount;
           $branch->balance= $branch->balance + $Transaction->amount;
           $branch->save();
       }



    /*   $branch=branch::find($order->branch_id);
       $branch->pending_balance=$branch->pending_balance-$Transaction->amount;
       $branch->balance= $branch->balance + $Transaction->amount;
       $branch->save();
       if ($Transaction != null){
           $Transaction->pending=false;
           $Transaction->save();
       }*/
       //price_after_discount
   /*    if ($order->price_after_discount != null){
           $price = $order->price_after_discount;
       }else{
           $price = $order->total_price;
       }*/


   }



   public function decrese_brnach_balance($order,$amount,$delivery_action){

        if($order->acceptation='accepted'){
            $branch=branch::find($order->branch_id);
            $branch->pending_balance=$branch->pending_balance-($amount*(98/100));
            // check in driver make eny action in this order
            if($delivery_action==true){
                $Transaction=Transaction::where('order_id',$order->id)->first();
                // here we add delivery fees to
                $branch->balance= $branch->balance + $Transaction->amount-($amount*(98/100));
            }
        }

        return true;
   }




   public function deletetransaction($order){
     $Transaction=Transaction::where('order_id',$order->id)->first();

     $branch=branch::find($order->branch_id);
     $branch->pending_balance=$branch->pending_balance-$Transaction->amount;
     $branch->balance= $branch->balance - $Transaction->amount;

     $systemtrans=SystemTransaction::where('order_id',$order->id)->first();
     $systemtrans->balance=$systemtrans->balance-$systemtrans->balance;
     $systemtrans->save();

     $branch->save();
     $Transaction->delete();
     return true;
   }

   public function branchTransaction($branchid,$price){
        $branch=branch::find($branchid);
   }
}
