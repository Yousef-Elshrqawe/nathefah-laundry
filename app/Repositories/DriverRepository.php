<?php

namespace App\Repositories;
use App\Interfaces\DriverRepositoyInterface;
use Illuminate\Support\Facades\DB;
use Auth;

class DriverRepository implements DriverRepositoyInterface
{
   public function getimg($img){
     if($img==null){
        return $img=asset("/uploads/driver/img/default.png");
     }
     return asset("/uploads/driver/img/".$img);
   }
}
