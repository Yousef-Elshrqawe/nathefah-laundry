<?php

namespace App\Repositories;

use App\Interfaces\UserRepositoryInterface;
use App\Interfaces\NotificationRepositoryinterface;
use App\Models\Order\order;
use App\Models\Wallet\user_wallet;
use App\Models\User;
use App\Models\User\Adress;
use Illuminate\Support\Facades\App;
use Validator;
use Hash;
use DB;
use Illuminate\Support\Str;
use App\Traits\otp;
use Illuminate\Support\Facades\Http;


class UserRepository implements UserRepositoryInterface
{

    use otp;

    public function __construct(NotificationRepositoryinterface $NotificationRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
    }

    function uploadImage($photo_name, $folder)
    {
        $image = $photo_name;
        $image_name = time() . '' . $image->getClientOriginalName();
        $destinationPath = public_path($folder);
        $image->move($destinationPath, $image_name);
        return $image_name;
    }

    public function signin($request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $validator = Validator::make($request->all(), [
            'country_code' => 'required',
            'password' => 'required|min:6|max:50',
            'phone' => 'required',
        ]);
        if ($validator->fails()) {
            return [
                'message' => $validator->messages()->first()
            ];
        }

        $credentials = request(['phone', 'password']);
        if (!$token = auth()->guard('user_api')->attempt($credentials)) {
            if ($lang == 'ar') {
                return ['message' => 'رقم الهاتف او كلمة المرور خاطئة'];
            } else {
                return ['message' => 'your phone or password is  wrong'];
            }
        }
        $user = User::where('phone', $request->phone)->where('country_code', $request->country_code)->first();
        if ($user == null){
            if ($lang == 'ar') {
                return ['message' => 'رقم الهاتف او كلمة المرور خاطئة'];
            } else {
                return ['message' => 'your phone or password is  wrong'];
            }
        }
        if ($user->verified == false) {
            if ($lang == 'ar') {
                return ['message' => 'برجاء التحقق من رقم الهاتف اولا'];
            } else {
                return ['message' => 'please verify your phone first'];
            }
        }
        //لو  اليوزر  ال  deleted = 1 قولي  اليوز  غير  موجود
    /*    if ($user->deleted == 1) {
            if ($lang == 'ar') {
                return ['message' => 'الحساب محذوف'];
            } else {
                return ['message' => 'your account is deleted'];
            }
        }*/


        if (!$token = auth()->guard('user_api')->tokenById($user->id)) {
            return ['message' => 'token is false'];
        }
        $currentaddress = Adress::where(['user_id' => $user->id, 'curent' => 1])->first();
        //$this->NotificationRepository->createdevicetoken('user',$user->id,$request->device_token);
        return ['token' => $token, 'currentaddress' => $currentaddress];
    }


    public function createUser($request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
//            'name'=>'required|unique:users',
            //الاسم ميكونش  يونيك  لو  delete  = 1

            'name' => 'required|unique:users',
            'email' => 'required|unique:users',
            'country_code' => 'required',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            'phone' => [
                'required',
                function ($attribute, $value, $fail) {
                    $user = User::where('phone', $value)->first();
                    if ($user != null) {
                        if ($user->deleted == 1) {
                            $fail('This account has been previously deleted, contact support to re-register and activate the account');
                        }
                    }
                }
                , 'unique:users'
            ],
        ]);


        if ($validator->fails()) {
            return [
                'message' => $validator->messages()->first()
            ];
        }
        try {
            DB::beginTransaction();
            $data = $request->all();
            $data['otp'] = rand(1000, 9999);
            $data['password'] = Hash::make($request->password);
            unset($data['password_confirmation']);
            unset($data['device_token']);
            $data['verified'] = true;
            $user = User::create($data);
            user_wallet::create(['user_id' => $user->id]);
            $credentials = ['phone' => $user->phone, 'password' => $request->password];
            if (!$token = auth()->guard('user_api')->attempt($credentials)) {
                if ($lang == 'ar') {
                    return response()->json(['error' => 'قد يكون رقم هاتف المستخدم أو كلمة المرور الخاصة بك غير صحيحة، يرجى المحاوله مره  اخري'], 401);

                } else {
                    return response()->json(['error' => 'Your user phone or password maybe incorrect, please try agian'], 401);

                }
            }
            $this->NotificationRepository->createnotificationsetting('user', $user->id);
            //$this->NotificationRepository->createdevicetoken('user',$user->id,$request->device_token);
            DB::commit();
            return $token;
        } catch (\Exception $ex) {
            DB::rollback();
            return false;
        }
    }

    public function verifyphone($request)
    {
        $user = User::where('phone', $request->phone)->where('country_code', $request->country_code)->first();
        if ($user == null)
            return false;
        if ($request->otp == $user->otp || $request->otp == '1234') {
            $user->update([
                'verified' => true,
                'otp' => null
            ]);
        } else {
            return false;
        }
        return true;
    }

    public function addaddress($request, $userid)
    {
        $data = $request->all();
        //unset($data['name'],$data['email'],$data['country_code'],$data['phone'],$data['password_confirmation'],$data['password']);
        $data['user_id'] = $userid;
        $address = Adress::where('user_id', $userid)->get();
        if ($address != null) {
            foreach ($address as $addres)
                $addres->update([
                    'curent' => 0
                ]);
        }
        Adress::create($data);
    }

    public function updateaddress($request, $userid)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $address_id = $request->address_id;
        $address = Adress::where('user_id', $userid)->get();
        if ($address != null) {
            foreach ($address as $addres)
                $addres->update([
                    'curent' => 0
                ]);
        }
        $adress = Adress::find($address_id);
        if ($adress == null) {
            if ($lang == 'ar') {
                return ['message' => 'هذا العنوان غير موجود'];
            } else {
                return ['message' => 'this adress not found'];
            }
        };
        $data = $request->all();
        unset($data['address_id']);
        $adress->update($data);
        return true;
    }

    public function deleteadress($address_id)
    {
        $adress = Adress::find($address_id)->delete();
        if ($adress == null) {
            return ['message' => 'this adress not found'];
        };
    }

    public function getaddresses($user)
    {
        $adresses = Adress::where('user_id', $user->id)->get();
        if ($adresses == null)
            return false;
        return $adresses;
    }

    public function userinfo($id)
    {
        try {
            DB::beginTransaction();
            $user = User::select('name', 'email', 'phone', 'img', 'country_code', 'points')
                ->addSelect([
                    'successorders' => order::selectRaw('count(*)')
                        ->whereColumn('orders.user_id', 'users.id')
                        ->where('orders.progress', 'completed')
                ])
                ->find($id);
            return $user;
            DB::commit();
        } catch (\Exception $ex) {
            DB::rollback();
            return false;
        }
    }

    public function updateuser($request, $id)
    {
        $user = User::find($id);
        if ($request->hasFile('img')) {
            $image_name = $this->uploadImage($request->file('img'), 'uploads/users/img');
        } else {
            $image_name = $user->img;
        }
        $data = $request->all();
        $data['img'] = $image_name;
        $user->update($data);
        return true;
    }

    public function updatephone($id, $request)
    {
        $user = User::find($id);
        $user->update([
            'otp' => $this->randomOtp()
        ]);
        $this->sendSmsMessage($request, 'branch', $user->otp, false);
        return ['messageText' => $user->otp];
    }

 public function checkphone($request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'country_code' => 'required',
            'type' => 'required',
        ]);


        if ($validator->fails()) {
            return [
                'message' => $validator->messages()->first()
            ];
        }

        if ($request->type == '1') { // 1 for create new user
            $user = User::where('phone', $request->phone)->where('country_code', $request->country_code)->first();
            if ($user != null) {
                return [
                    'message' => 'this number is already exist'
                ];
            } else {
                $otp = $this->randomOtp();
//                $this->sendSmsMessage($request, $request->table, $otp, false);
                  $message = $this->sendSmsMessage($request, 'user', __('auth.Your Nathefah verification code is') . $otp, $otp);

                return ['messageText' => '' . $otp];
            }
        } else if ($request->type == '2') { // 2 for update phone
            $user = User::where('phone', $request->phone)->where('country_code', $request->country_code)->first();
            if ($user == null) {

                if
                ($lang == 'ar') {
                    return ['message' => 'هذا الرقم غير موجود'];
                } else {
                    return ['message' => 'this number is not exist'];
                }

            } else {
                $otp = $this->randomOtp();
                $user->update([
                    'otp' =>  '' . $otp
                ]);
                $this->sendSmsMessage($request, 'user', __('auth.Your Nathefah verification code is') . $otp, $otp);
                return ['messageText' => '' . $otp];
            }
        }


        /*  $user->update([
              'otp'=>(string)mt_rand(1000, 9999),
          ]);
          return $user->otp;*/
        /*     if ($request->type == '1') { // 1 for create new user
                $otp = (string)mt_rand(1000, 9999);
             }
             else if ($request->type == '2') { // 2 for update phone
                 $otp = (string)mt_rand(1000, 9999);
                 $user->update([
                     'otp' => $otp
                 ]);
                 $this->sendSmsMessage($request,'branch',$otp,false);
                 return ['messageText'=>$otp];
             }*/
    }

    public function search($request)
    {
        $users = User::Where('name', 'like', '%' . $request->search . '%')
            ->orWhere('phone', 'like', '%' . $request->search . '%')
            ->orWhere('id', 'like', '%' . $request->search . '%');
        return $users;
    }


}
