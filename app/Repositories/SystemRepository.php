<?php
namespace App\Repositories;
use App\Interfaces\SystemRepositoryInterface;
use App\Models\System\Systeminfo;

use DB;
use Illuminate\Support\Str;
class SystemRepository implements SystemRepositoryInterface
{
  public function increasebalance($balance){
    $balance= Systeminfo::latest()->first();
    $balance->balance=$balance->balance+$balance;
    $balance->save();
    return true;
  }


  // this balance refer to balance of all branches
  public function decreasebalance($amount){
    $balance= Systeminfo::latest()->first();
    $balance->balance=$balance->balance-($amount*(98/100));
    $balance->save();
    return true;
  }

}
