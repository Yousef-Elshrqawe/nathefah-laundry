<?php


namespace App\Repositories;


use App\Interfaces\PeriodRepositoryInterface;
use App\Models\Subscription\SubscriptionPackage;
use App\Models\Subscription\{Period, PeriodTranslation};
use Illuminate\Http\Request;
use App\Models\Subscription\Package;
use Illuminate\Support\Facades\DB;


class PeriodRepository implements PeriodRepositoryInterface
{

    protected $model;

    public function __construct()
    {
        $this->model = new Period();
    }//End __construct Function


    public function index()
    {
        // TODO: Implement index() method.
        return $this->model::get();
    }

    public function store($request)
    {
        // TODO: Implement store() method.

        return $this->model::create($request->all());

        // return  $this->model::create([
        //     'name'               => $request->name,
        //     'duration_value'     => $request->duration_value,
        //     'duration_unit'      => $request->duration_unit,
        // ]);
    }


    public function update($request)
    {
        // TODO: Implement update() method.
        $period = $this->model::with('BeriodTranslation')->findOrFail($request->id);
        $request = Request::createFromGlobals();
        return $period->update($request->all());
    }


    public function destroy($period_id)
    {
        // TODO: Implement destroy() method.
        return $this->model::findOrFail($period_id)->delete();
    }

    /*   public function getPeriods($lang)
       {
           // TODO: Implement getPeriods() method.
           return  DB::table('periods')
                   ->join('packages','packages.period_id','=','periods.id')
                   ->join('periodtranslations','periodtranslations.period_id','=','periods.id')
                   ->select('periods.id','periods.duration_value','duration_unit','min_branch','max_branch','packages.id as package_id','periodtranslations.name')
                   ->selectRaw('max(price) as price')
                   ->where('min_branch',1)
                   ->groupBy('periods.id')->groupBy('price')->groupBy('periods.duration_value')->groupBy('periodtranslations.name')
                   ->groupBy('duration_unit')->groupBy('duration_unit')->groupBy('max_branch')->groupBy('min_branch')
                   ->groupBy('packages.id')
                   ->where('periodtranslations.locale',$lang)
                   ->get();


           return $this->model::addSelect([
                   'min_branch' => Package::selectRaw('min(min_branch)')
                       ->whereColumn('periods.id', 'packages.period_id')
               ])
               ->addSelect([
                   'max_branch' => Package::selectRaw('max(max_branch)')
                       ->whereColumn('periods.id', 'packages.period_id')
               ])
               ->addSelect([
                   'price' => Package::selectRaw('max(price)')
                       ->whereColumn('periods.id', 'packages.period_id')
               ])  >get();




       }*/

    public function getPeriods($lang)
    {
        // TODO: Implement getPeriods() method.
        $rows = DB::table('periods')
            ->join('packages', 'packages.period_id', '=', 'periods.id')
            ->join('periodtranslations', 'periodtranslations.period_id', '=', 'periods.id')
            ->select('periods.id', 'periods.duration_value', 'duration_unit', 'min_branch', 'max_branch', 'packages.id as package_id', 'periodtranslations.name')
            ->selectRaw('max(price) as price')
            ->where('min_branch', 1)
//            ->where('max_branch','>',1)
            // هاتلي  اقل  max_branch لل باكج اللي مدته اكبر
            ->selectRaw('min(packages.max_branch) as max_branch')
            ->groupBy('periods.id')->groupBy('price')->groupBy('periods.duration_value')->groupBy('periodtranslations.name')
            ->groupBy('duration_unit')->groupBy('duration_unit')->groupBy('max_branch')->groupBy('min_branch')
            ->groupBy('packages.id')
            ->where('periodtranslations.locale', $lang)
            ->orderBy('packages.price', 'asc');

        return $rows->take($rows->count() +1  )->get();
    }


}
