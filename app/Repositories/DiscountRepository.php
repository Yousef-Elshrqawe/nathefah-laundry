<?php

namespace App\Repositories;
use App\Interfaces\DiscountRepositoryInterface;
use App\Models\Laundry\branch;
use Carbon\Carbon;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\Discount\{BranchDiscount,UserDiscount,Discount};
use App\Models\Order\{order,OrderDiscount};
use Illuminate\Support\Facades\DB;
use Auth;

class DiscountRepository implements DiscountRepositoryInterface
{

    public function creatediscount($request){
        $data=$request->all();
        $code=Str::random(6);
        $data['code']=$code;
        $discount=Discount::create($data);
        $users=User::get();
        $branchs=branch::get();
        foreach($users as $user){
            UserDiscount::create([
               'user_id'=>$user->id,
               'discount_id'=>$discount->id
            ]);
        }
        foreach($branchs as $branch){
            BranchDiscount::create([
                'branch_id'=>$branch->id,
                'laundry_id'=>$branch->laundry_id,
                'discount_id'=>$discount->id
            ]);
        }
    }
    public function getdiscount($request){
        $user_id=Auth::guard('user_api')->user()->id;
        $discounts=DB::table('discounts')
        ->select('amount','percent','code')
        ->join('user_discounts','user_discounts.discount_id','=','discounts.id')
        ->where('count','>',0)
        ->where('enddate','>=',Carbon::now()->toDateTimeString())
        ->where('user_discounts.user_id',$user_id)
        ->groupBy('code')->groupBy('amount')->groupBy('percent')
        ->get();
        return $discounts;
    }

    public function usediscount($order_id,$code , $lang)
    {
        app()->setLocale($lang);

      $user_id=Auth::guard('user_api')->user()->id;
      $order=order::find($order_id);
      $discount=Discount::where('code',$code)->first();
      if($discount==null)
      return ['message'=> trans('response.this discount not available') ];
      $userdiscount=UserDiscount::where('user_id',$user_id)->where('discount_id',$discount->id)->first();
      if($userdiscount==null)
          return ['message'=> trans('response.this discount not available') ];
        if($userdiscount->count<=0){
        return ['message'=>trans('response.you already used this discount')];
      }

      if($discount->percent!=0){

        if($order->price_before_discount<=$discount->min_order_price)
          return [
              'message' => __('response.min_order_price', ['min_order' => $discount->min_order_price])
          ];

          //         $price_after_discount=$order->price_before_discount-($order->price_before_discount*($discount->percent/100));
          //متعملش  خصم علي  الدليفري
          $price = $order->price_before_discount - $order->delivery_fees;
          $price_after_discount = $price - ($price * ($discount->percent / 100));
          $price_after_discount+= $order->delivery_fees;

      }else{
        if($order->price_before_discount<=$discount->min_order_price && $discount->amount > $order->price_before_discount)
            return [
                'message' => __('response.min_order_price', ['min_order' => $discount->min_order_price])
            ];
        $price_after_discount=$order->price_before_discount-$discount->amount;
      }
        $this->discountlogs($order_id,$discount->id);
        return $price_after_discount;
    }


    public function getdiscounts($type){
        if($type=='avilable'){
           $discounts=Discount::where('enddate','>=',Carbon::now()->toDateTimeString())
              ->paginate(5);
        }
        if($type=='previous'){
            $discounts=Discount::where('enddate','<=',Carbon::now()->toDateTimeString())
                ->paginate(5);
        }
        return $discounts;
    }


    public function order_edit_discount($order,$discount_id){
        $discount=Discount::where('id',$discount_id)->first();
        if($discount->enddate<=Carbon::now()->toDateTimeString()){
           return false;
        }
        if($discount->percent!=0){

            if($order->price_before_discount<=$discount->min_order_price)
                return [
                    'message' => __('response.min_order_price', ['min_order' => $discount->min_order_price])
                ];
             $price_after_discount=$order->price_before_discount-($order->price_before_discount*($discount->percent/100));
          }else{
            if($order->price_before_discount<=$discount->min_order_price)
                return [
                    'message' => __('response.min_order_price', ['min_order' => $discount->min_order_price])
                ];
            $price_after_discount=$order->price_before_discount-$discount->amount;
          }
          $order->price_after_discount=$price_after_discount;
          $order->save();
          return true;
    }


    public function discountlogs($order_id,$discount_id){
        OrderDiscount::create([
           'order_id'=>$order_id,
           'discount_id'=>$discount_id
        ]);
        return true;
    }

    public function orderdiscount($order_id){
         $orderdiscount=OrderDiscount::where('order_id',$order_id)->first();
         if($orderdiscount!=null)
         return $orderdiscount;
         return false;
    }
}
