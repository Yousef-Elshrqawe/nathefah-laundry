<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class SendSmsJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $phone;
    protected $messageText;
    public function __construct($phone, $messageText)
    {
        $this->phone = $phone;
        $this->messageText = $messageText;
    }


    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
   /*     require_once('vendor/autoload.php');

        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', 'https://apis.deewan.sa/sms/v1/messages', [
            'body' => '{
            "senderName":"Nathefah",
            "messageType":"text",
            "messageText":"'.$this->messageText.'",
            "recipients":"'.$this->phone.'"
            }',



            'headers' => [
                'Authorization' => 'Bearer ' . env('SMS_TOKEN'), // Add your token or headers here
                'accept' => 'application/json',
                'content-type' => 'application/json',
            ],
        ]);*/



        $response = Http::withHeaders([
            'Authorization' => 'Bearer ' . env('SMS_TOKEN'), // Add your token or headers here
            'accept' => 'application/json',
            'content-type' => 'application/json',
        ])->post('https://apis.deewan.sa/sms/v1/messages', [

            'senderName'     => 'Nathefah',
            'messageType'    =>'text',
            'messageText'    => $this->messageText,
            'recipients'     => $this->phone
        ]);
        $data['messageText']= $this->messageText;
        $response =json_decode($response);
        return [
            'status'         =>true,
            'message'        =>'send message successfully',
            'data'           =>$data,
            'messageText'    => $this->messageText,
            'smsresponse'    => $response
        ];
    }
}
