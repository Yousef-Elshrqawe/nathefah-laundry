<?php

namespace App\Jobs;

use App\Models\Driver\Driver;
use App\Models\Laundry\Branchuser;
use App\Models\User;
use App\Repositories\NotificationRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class SendNotificationJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $type;
    protected $userId;
    protected $title;
    protected $body;
    protected $titlesData;
    protected $bodysData;


    /**
     * Create a new job instance.
     *
     * @param string $type
     * @param string $userId
     * @param string $title
     * @param string $body
     * @param array $titlesData
     * @param array $bodysData
     */


    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($type, $userId, $title, $body, $titlesData, $bodysData)
    {
        $this->type = $type;
        $this->userId = $userId;
        $this->title = $title;
        $this->body = $body;
        $this->titlesData = $titlesData;
        $this->bodysData = $bodysData;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(NotificationRepository $NotificationRepository)
    {
        $recipients = $this->getRecipientsByType();

        foreach ($recipients as $recipient) {
            $NotificationRepository->sendnotification($this->type, $recipient->id, $this->title, $this->body, 1);
            $NotificationRepository->createnotification($this->type, $recipient->id, $this->titlesData, $this->bodysData);
        }
    }


    /**
     * Get recipients based on the type.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    private function getRecipientsByType()
    {
        switch ($this->type) {
            case 'user':
                return $this->userId == 'all' ? User::all() : User::where('id', $this->userId)->get();
            case 'driver':
                return $this->userId == 'all' ? Driver::all() : Driver::where('id', $this->userId)->get();
            case 'branch':
                return $this->userId == 'all' ? Branchuser::all() : Branchuser::where('id', $this->userId)->get();
            default:
                throw new \InvalidArgumentException('Invalid notification type');
        }
    }

    public function onQueue()
    {
        // إذا كانت الإشعار للمستخدم فقط، اجعل له أولوية عالية.
        if ($this->type === 'user' && $this->userId != 'all') {
            return 'high-priority'; // يمكنك تحديد اسم الـ Queue هنا.
        }

        if ($this->type === 'driver' && $this->userId != 'all') {
            return 'high-priority'; // يمكنك تحديد اسم الـ Queue هنا.
        }

        if ($this->type === 'branch' && $this->userId != 'all') {
            return 'high-priority'; // يمكنك تحديد اسم الـ Queue هنا.
        }

        return 'default'; // باقي الإشعارات تكون في الـ Queue الافتراضي.
    }
}
