<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use App\Models\Discount\{Discount,BranchDiscount,UserDiscount};
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use App\Interfaces\NotificationRepositoryinterface;


class assigneduserdiscount implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public $data;
    public $id;
    public function __construct($data,$id,$NotificationRepository)
    {
        //
        $this->data=$data;
        $this->id=$id;
        $this->NotificationRepository=$NotificationRepository;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
        foreach($this->data as $user){
            if($user!=null){

            //     $title='قسيمه جديده';
            //     $body='لقد حصلت علي قسيمه جديده';
            //    if($user->lang=='en'){
            //     $title='new voucher';
            //     $body='your have recive new voucher';
            //     }
            //     $titles=['ar'=>'قسيمه جديده','en'=>'new voucher'];
            //     $bodys=['ar'=>'لقد حصلت علي قسيمه جديده','en'=>'you have recive new voucher'];
            //     $this->NotificationRepository->sendnotification('user',$user->id,$title,$body,3);
            //     $this->NotificationRepository->createnotification('user',$user->id,$titles,$bodys);
             $data[]=[
                'user_id'=>$user->id,
                'count'=>1,
                'discount_id'=>$this->id
              ];
            }
        }
        UserDiscount::insert($data);
    }
}
