<?php

namespace App\Models\Discount;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchDiscount extends Model
{
    use HasFactory;
    protected $table='branch_discounts';
    public $guarded=[];
}
