<?php

namespace App\Models\Discount;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class UserDiscount extends Model
{
    use HasFactory;
    protected $table='user_discounts';
    public $guarded=[];
    public function user(){
        return $this->belongsTo(User::class);
    }
}
