<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionBranch extends Model
{
    use HasFactory;

    protected $fillable = [
        'branch_id',
        'paid',
        'remaining',
        'current_balance',
        'branch_balance',
    ];
}
