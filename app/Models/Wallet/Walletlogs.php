<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Walletlogs extends Model
{
    use HasFactory;
    protected $table='wallet_logs';
    protected $guarded=[];
}
