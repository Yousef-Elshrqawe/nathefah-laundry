<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laundry_wallet extends Model
{
    use HasFactory;
    protected $table='laundry_wallets';
    public $guarded=[];
}
