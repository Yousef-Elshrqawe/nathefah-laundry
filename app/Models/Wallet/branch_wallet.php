<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class branch_wallet extends Model
{
    use HasFactory;
    protected $table='branch_wallets';
    public $guarded=[];
}
