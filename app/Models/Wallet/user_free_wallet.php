<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_free_wallet extends Model
{
    use HasFactory;
    protected $table='user_free_wallets';
    public $guarded=[];
}
