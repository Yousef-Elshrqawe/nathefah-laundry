<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class laundry_free_wallet extends Model
{
    use HasFactory;
    protected $table='laundry_free_walets';
    public $guarded=[];
}
