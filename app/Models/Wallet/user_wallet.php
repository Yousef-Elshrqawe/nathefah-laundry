<?php

namespace App\Models\Wallet;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class user_wallet extends Model
{
    use HasFactory;
    protected $table='user_wallets';
    public $guarded=[];
}
