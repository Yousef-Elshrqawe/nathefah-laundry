<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionFeedback extends Model
{
    use HasFactory;
    protected $table='transaction_feedback';
    protected $guarded=[];

}
