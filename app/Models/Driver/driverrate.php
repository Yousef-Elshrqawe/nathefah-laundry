<?php

namespace App\Models\Driver;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class driverrate extends Model
{
    use HasFactory;
    protected $table='drivers_rate';
    public $guarded=[];
}
