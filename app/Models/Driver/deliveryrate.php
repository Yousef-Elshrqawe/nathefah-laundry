<?php

namespace App\Models\Driver;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class deliveryrate extends Model
{
    use HasFactory;
    protected $table='delivery_rate';
    public $guarded=[];
}
