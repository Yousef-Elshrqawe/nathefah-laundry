<?php

namespace App\Models\Driver;

use App\Models\Laundry\branch;
use App\Models\Order\{order,OrderDriveryStatus};
use DriversRate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Session;

class Driver extends Authenticatable implements JWTSubject
{
    use HasFactory;
    public $guarded=[];
/*    public function getImgAttribute($value)
    {
         if($value==null)
         return asset("/uploads/driver/img/default.png");
         return asset("/uploads/driver/img/".$value);
    }*/
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    public function rate()
    {
        return $this->hasOne(driverrate::class,'driver_id');
    }
    public function orders(){
        return $this->hasMany(Order::class , 'driver_id' , 'id');
    }

    /**
     * Completed Orders
     */
    public function orders1(){
        return $this->hasMany(Order::class , 'driver_id' , 'id')
                ->where('delivery_status','=', "completed");
    }
    public function driverOrdersInProgress(){
        // return Order::where('delivery_status' ,'inprogress')
        //     ->where('driver_id' , $this['id'] )->count();
        // return OrderDriveryStatus::groupBy('order_id')->select('order_id', DB::raw('count(*) as total'))
        //     ->where('confirmation' ,true)->where('driver_id' , $this['id'] )->count();
        return OrderDriveryStatus::where('confirmation' ,true)
            ->where('driver_id' , $this['id'] )->distinct('order_id')->count();
    }
    public function driverOrdersCompleted(){
        return OrderDriveryStatus::where('confirmation' ,true)->wherein('order_status',['drop_of_laundry','drop_of_home'])
            ->when(Session::get('driverlistdate') != null , function ($q){
                return $q->where('created_at','>',Session::get('driverlistdate'));
            })->where('driver_id' , $this['id'] )
            ->count();
    }
    public function driverRates_count(){
        $total_rate = driverrate::where( 'driver_id' , $this['id'] )->get()->avg('rate');
        return number_format( $total_rate, 1 );
    }

    public function delivery_status()
    {
        return $this->hasMany(OrderDriveryStatus::class , 'driver_id' , 'id');
    }

    public function in_progress_orders()
    {
        return $this->hasMany(Order::class , 'driver_id' , 'id');
    }

    //branch
    public function branch()
    {
        return $this->belongsTo(Branch::class , 'branch_id' , 'id');
    }
}
