<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderEditLogs extends Model
{
    use HasFactory;
    protected $table='order_edit_logs';
    protected $guarded=[];

}
