<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Translatable;

class payment_method extends Model
{

    use HasFactory;
    protected $guarded=[];
        public function getImgAttribute($value)
    {
        return asset('payment_imgs/'.$value);
    }

    //translatable
    public $translatedAttributes = ['name'];
    public $translationModel = payment_methodTranslation::class;

    public function translations()
    {
        return $this->hasMany(payment_methodTranslation::class);
    }


}
