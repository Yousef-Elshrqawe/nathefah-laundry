<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\laundryservice\Service;
use App\Models\laundryservice\Category;
use App\Models\laundryservice\Additionalservice;
use App\Models\Laundry\Branchitem;

class orderdetailes extends Model
{
    use HasFactory;
    protected $table='order_detailes';
    protected $guarded=[];
    public function service()
    {
        return $this->belongsTo(Service::class);
    }


    public function additionalservice()
    {
        return $this->belongsTo(Additionalservice::class);
    }
    public function Branchitem(){
        return $this->belongsTo(Branchitem::class);
    }
}
