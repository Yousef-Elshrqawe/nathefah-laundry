<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rejectreason extends Model
{
    use HasFactory;
    protected $table='rejected_reasons';
    protected $guarded=[];
}
