<?php

namespace App\Models\Order;

use App\Models\Laundry\branch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\laundryservice\Argent;
use App\Models\Driver\Driver;

class order extends Model
{

    use HasFactory;

    protected $guarded=[];
    public function getProgressAttribute($value){
        return __('progress.'.$value);
    }

    public function getOriginalProgressAttribute() {
        return $this->attributes['progress'];
    }
    public function orderdetailes(){
      return  $this->hasMany(orderdetailes::class);
    }
    public function urgents(){
        return $this->hasMany(Argent::class);
    }
    public function deliverytype(){
        return $this->belongsTo(delivery_type::class,'delivery_type_id');
    }
    public function paymenttype(){
        return $this->belongsTo(payment_method::class,'payment_method_id');
    }






    public function driver(){
        return $this->belongsTo(Driver::class,'driver_id');
    }
     //OrderDriveryStatus
    public function  OrderDriveryStatus(){
        return $this->hasMany(OrderDriveryStatus::class)->latest();
    }
    public function branch(){
        return $this->belongsTo(branch::class,'branch_id');
    }
}
