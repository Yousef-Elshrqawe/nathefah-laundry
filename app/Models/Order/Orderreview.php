<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
class Orderreview extends Model
{
    use HasFactory;
    protected $table='order_reviews';
    public $guarded=[];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
}
