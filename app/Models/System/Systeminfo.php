<?php

namespace App\Models\System;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Systeminfo extends Model
{
    use HasFactory;
    protected $table='systeminfo';
    protected $guarded=[];
}
