<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DriverDeviceToken extends Model
{
    use HasFactory;
    protected $table='driverdevicestoken';
    protected $guarded=[];
}
