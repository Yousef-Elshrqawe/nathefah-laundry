<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class drivernotificationtranslation extends Model
{
    use HasFactory;
    protected $table='drivernotificationtranslations';
    protected $guarded=[];
}
