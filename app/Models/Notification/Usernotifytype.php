<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usernotifytype extends Model
{
    use HasFactory;
    protected $table='usernotifytype';
    protected $guarded=[];
}
