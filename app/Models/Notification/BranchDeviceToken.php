<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchDeviceToken extends Model
{
    use HasFactory;
    protected $table='branchdevicestoken';
    protected $guarded=[];
}
