<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class branchnotificationtranslation extends Model
{
    use HasFactory;
    protected $table='branchnotificationtranslations';
    protected $guarded=[];
}
