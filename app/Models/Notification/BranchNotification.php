<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use  App\Models\Notification\branchnotificationtranslation;
class BranchNotification extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $table='branchnotification';
    public function translations(){
        return $this->hasMany(branchnotificationtranslation::class,'branch_notify_id');
    }
}
