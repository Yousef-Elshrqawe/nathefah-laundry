<?php

namespace App\Models\Notification;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class usernotificationtranslation extends Model
{
    use HasFactory;
    protected $table='usernotificationtranslations';
    protected $guarded=[];
}
