<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Laundry\{branch,Laundry};
use App\Models\Order\{order};

class SystemTransaction extends Model
{
    use HasFactory;
    protected $guarded=[];




    public function branch(){
        return $this->belongsTo(branch::class);
    }
    public function laundry(){
        return $this->belongsTo(Laundry::class,'laundry_id');
    }
    public function order(){
        return $this->belongsTo(Order::class);
    }
}
