<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TermsCondition extends Model
{
    use HasFactory;

    protected $fillable = [
        'terms_conditions_ar',
        'terms_conditions_en',
        'privacy_policy_ar',
        'privacy_policy_en',
    ];


    public function getTermsCondition($lang)
    {
        $lang = app()->getLocale();
        if ($lang == 'ar') {
            return $this->terms_conditions_ar;
        } else {
            return $this->terms_conditions_en;
        }
    }

    public function getPrivacyPolicy($lang)
    {
        $lang = app()->getLocale();
        if ($lang == 'ar') {
            return $this->privacy_policy_ar;
        } else {
            return $this->privacy_policy_en;
        }
    }


}
