<?php

namespace App\Models\Laundry;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BranchPayment extends Model
{
    use HasFactory;
    protected $table='branch_payments';
    protected $guarded=[];
    
        public function payment_method(){
        return $this->belongsTo('App\Models\Laundry\PaymentMethod','payment_method_id');
    }
}
