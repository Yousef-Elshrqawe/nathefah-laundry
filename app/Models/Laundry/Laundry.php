<?php

namespace App\Models\Laundry;

use App\Models\Subscription\LaundryPackage;
use App\Models\Subscription\Package;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;


class Laundry extends Authenticatable implements JWTSubject
{
    use HasFactory;
    //use Translatable;
    protected $guarded=[];
    protected $table="laundries";
    //public $translatedAttributes = ['name'];
    protected $hidden = [
        'password',
    ];

    public function getLogoNameAttribute($value)
    {
        return $this->logo;
    }

    public function getLogoAttribute($value)
    {
         if($value==null)
         return asset('uploads/laundry/logos/default/images.jpg');
         return asset('uploads/laundry/logos/'.$value);
    }


    public function getCompanyregisterAttribute($value)
    {
         if($value==null)
         return asset('uploads/branches/logos/default/images.jpg');
         return asset('uploads/laundry/company_register/'.$value);
    }
    public function getTaxcardAttribute($value)
    {
         if($value==null)
         return asset('uploads/branches/logos/default/images.jpg');
         return asset('uploads/laundry/tax_card/'.$value);
    }
    public function branchitem()
    {
        return $this->hasMany(Categoryservice::class);
    }
    public function branchs()
    {
        return $this->hasMany(branch::class);
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function package()
    {
        return $this->hasOne(LaundryPackage::class,'laundry_id');
    }
}
