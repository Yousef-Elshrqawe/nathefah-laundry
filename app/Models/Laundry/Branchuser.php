<?php

namespace App\Models\Laundry;

use App\Models\Notification\branchnotifytype;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Traits\HasRoles;
use Tymon\JWTAuth\Contracts\JWTSubject;
class Branchuser extends Authenticatable implements JWTSubject
{
    use HasFactory,HasRoles;
    protected $table='branchusers';
    protected $guarded=[];
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function notifications()
    {
        return $this->hasMany(branchnotifytype::class,'user_branch_id');
    }


    /**
     * @param $value
     * Hash Password
     */
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }


}
