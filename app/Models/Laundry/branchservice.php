<?php

namespace App\Models\Laundry;

use App\Models\laundryservice\Service;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class branchservice extends Model
{
    use HasFactory;
    protected $table="brnachservices";
    protected $guarded=[];

    //service
    public function service()
    {
        return $this->belongsTo(Service::class,'service_id');
    }



}
