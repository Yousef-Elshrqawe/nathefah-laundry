<?php

namespace App\Models\Laundry;

use App\Models\Driver\Driver;
use App\Models\Order\order;
use App\Models\Schedule;
use App\Models\TransactionBranch;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\closeingday\Closeingday;

// use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
// use Astrotomic\Translatable\Translatable;

class branch extends Authenticatable implements JWTSubject
{
    use HasFactory;
    protected $guarded=[];
    protected $table="branchs";
    public $translatedAttributes = ['name'];



    public function closingdayes(){
        return $this->belongsToMany(Closeingday::class,'branch_closingdaies');
    }
    public function branchservices(){
        return $this->hasMany(branchservice::class,'branch_id');
    }
    //branchusers
    public function branchusers(){
        return $this->hasMany(Branchuser::class,'branch_id');
    }

    public function laundry(){
        return $this->belongsTo(Laundry::class,'laundry_id');
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    public function getJWTCustomClaims()
    {
        return [];
    }
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function users(){
        return $this->hasMany(Branchuser::class,'branch_id');
    }

    public function drivers()
    {
        return $this->hasMany(Driver::class,'branch_id');
    }

    public function orders()
    {
        return $this->hasMany(order::class,'branch_id');
    }
    public function setPasswordAttribute($value) {
        $this->attributes['password'] = Hash::make($value);
    }

    public function schedules()
    {
        return $this->morphMany(Schedule::class, 'scheduleable');
    }

    public function transactionBranches()
    {
        return $this->hasMany(TransactionBranch::class, 'branch_id');
    }

}
