<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DiscountFirstOrder extends Model
{
    use HasFactory;
    public $table='discount_first_order_users';
    protected $guarded=[];

}
