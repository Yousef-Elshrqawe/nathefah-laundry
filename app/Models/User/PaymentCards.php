<?php

namespace App\Models\User;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentCards extends Model
{
    use HasFactory;
    protected $table='userpaymentcards';
    protected $guarded=[];
}
