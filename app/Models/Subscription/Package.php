<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class Package extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    public $translatedAttributes = ['name','description'];
    protected $guarded=[];
    protected $hidden=['pivot','translations'];

    protected $fillable = [
        'name',
        'description',
        'min_branch',
        'max_branch',
        'price',
        'period_id'
    ];

    public function period(){
        return $this->belongsTo(Period::class,'period_id');
    }
}
