<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriodTranslation extends Model
{
    use HasFactory;
    protected $table='periodtranslations';
    protected $guarded=[];
}
