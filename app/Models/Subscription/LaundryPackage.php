<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LaundryPackage extends Model
{
    use HasFactory;

    protected $fillable = [
        'laundry_id',
        'package_id',
        'start_date',
        'end_date',
        'price'
    ];

    public function package()
    {
        return $this->belongsTo(Package::class,'package_id');
    }
}
