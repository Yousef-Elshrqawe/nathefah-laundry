<?php

namespace App\Models\Subscription;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class Period extends Model implements TranslatableContract
{
    use HasFactory,Translatable;

    public $translatedAttributes = ['name'];
    protected $hidden=['pivot','translations'];
    protected $guarded=[];

    // protected $fillable = [
    //     'name',
    //     'duration_value',
    //     'duration_unit',
    // ];


    public function packages(){
        return $this->hasMany(Package::class,'period_id');
    }

    // هاتلي  اول  باكدج ب اقل سعر
    public function minPackage(){
        return $this->hasOne(Package::class,'period_id')->orderBy('max_branch','asc');
    }

    public function BeriodTranslation(){
        return $this->hasMany(PeriodTranslation::class);
    }
}
