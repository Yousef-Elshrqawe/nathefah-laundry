<?php

namespace App\Models\laundryservice;

use App\Models\Laundry\Branchitem;
use App\Models\Laundry\branchservice;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;

class Service extends Model implements TranslatableContract
{
    use HasFactory,Translatable;
    protected $guarded=[];
    public $translatedAttributes = ['name'];
    protected $hidden = ['pivot','translations'];
    //setitemprice
    public function getImgAttribute($value)
    {
       return asset('uploads/service/'.$value);
    }
    public function categoryservices()
    {
        return $this->hasMany(Categoryservice::class);
    }
    public function categories(){
        return $this->belongsToMany(Category::class);
    }

    public function serviceprice(){
        return $this->hasMany(Serviceitemprice::class);
    }

    public function branchservice(){
        return $this->hasMany(branchservice::class,'service_id');
    }
    //branchitem
    public function branchitem(){
        return $this->hasMany(Branchitem::class);
    }

    //servicetranslations
    public function servicetranslations(){
        return $this->hasMany(ServiceTranslation::class);
    }
}
