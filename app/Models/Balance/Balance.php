<?php

namespace App\Models\Balance;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    use HasFactory;
    protected $table='branchbalance';
    protected $guarded=[];

    // public function setCreatedAtAttribute($value) {
    //     return $this->date('h:m a', strtotime($value));
    // }


    public function branch(){
        return $this->belongsTo(branch::class);
    }
    public function laundry(){
        return $this->belongsTo(Laundry::class,'laundry_id');
    }

}
