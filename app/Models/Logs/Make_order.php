<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Make_order extends Model
{
    use HasFactory;
    protected $table='make_order_logs';
    public $guarded=[];
}
