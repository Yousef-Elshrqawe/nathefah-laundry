<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reject_order extends Model
{
    use HasFactory;
    protected $table='reject_order_logs';
    public $guarded=[];
}
