<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accept_order extends Model
{
    use HasFactory;
    protected $table='accept_order_logs';
    public $guarded=[];
}
