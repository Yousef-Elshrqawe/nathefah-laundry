<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
class Deliveryfees extends Model
{
    use HasFactory;
    protected $table='delivery_fees';
    protected $guarded=[];
}
