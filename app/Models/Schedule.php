<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedule extends Model
{
    use HasFactory;

    protected $fillable = [
        'schedulable_id', 'schedulable_type', 'day_id', 'start_time', 'end_time' , 'is_active'
    ];

    public function scheduleable()
    {
        return $this->morphTo();
    }

    public function day()
    {
        return $this->belongsTo(Day::class);
    }
}
