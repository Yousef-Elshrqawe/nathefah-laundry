<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Tymon\JWTAuth\Contracts\JWTSubject;
use App\Models\User\Adress;
use App\Models\Wallet\{user_wallet,user_free_wallet};


class User  extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;


    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    // ];
    public function adress(){
        return $this->hasMany(Adress::class,'user_id');
    }

    use HasFactory;
    public $guarded=[];
    public function getImgAttribute($value)
    {
         if($value==null)
         return asset('uploads/users/img/default/default.png');
         return asset('uploads/users/img/'.$value);
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function userwallet(){
        return $this->hasOne(user_wallet::class);
    }
    public function userfreewallet(){
        return $this->hasMany(user_free_wallet::class);
    }

}
