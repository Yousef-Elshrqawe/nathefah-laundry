<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Laundry\{branch,Laundry};

class Transaction extends Model
{
    use HasFactory;
    protected $guarded=[];
    protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
    ];
    public function branch(){
        return $this->belongsTo(branch::class);
    }
    public function laundry(){
        return $this->belongsTo(Laundry::class,'laundry_id');
    }
    // public function getAmountAttribute($value)
    // {
    //     return round($this->value, 2);
    // }
    // public function getBalanceAttribute($value)
    // {
    //     return round($this->value, 2);
    // }

    public function getCreatedAtAttribute($value)
    {
//        return Carbon::parse($value)->format('Y-m-d H:i:s');
        return Carbon::parse($value)->addHours(3)->format('Y-m-d H:i:s');
    }
}
