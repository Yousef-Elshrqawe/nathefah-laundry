<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserTerms extends Model
{
    use HasFactory;
    //table name
    protected $table = 'user_terms';

    protected $fillable = [
        'user_id',
        'terms_condition_id',
        'is_read',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function termsCondition()
    {
        return $this->belongsTo(TermsCondition::class);
    }

}
