<?php

namespace App\Services;

use Google\Client;
use Exception;

class GoogleAccessTokenService
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client();
        $serviceAccountPath = base_path(env('FIREBASE_CREDENTIALS'));
        $this->client->setAuthConfig($serviceAccountPath);
        $this->client->addScope('https://www.googleapis.com/auth/firebase.messaging');
    }

    public function getAccessToken()
    {
        $accessToken = $this->client->fetchAccessTokenWithAssertion();

        if (isset($accessToken['access_token'])) {
            return $accessToken['access_token'];
        } else {
            throw new Exception('Failed to get access token');
        }
    }

} // end of GoogleAccessTokenService
