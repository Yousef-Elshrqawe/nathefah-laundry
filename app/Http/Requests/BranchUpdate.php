<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string|unique:branchs,username,' . $this->id,
            'phone' => ['required', 'string', 'size:9', 'regex:/^(05|5)[0-9]{8}$/', 'unique:branchs,phone,' . $this->id],
            'argent' => 'required|numeric',
            'laundry_id' => 'required|numeric|exists:laundries,id',
            'status' => 'required|in:open,closed',
            'open_time' => 'nullable|string',
            'closed_time' => 'nullable|string',
            'password' => 'nullable|string|min:6',
            'password_confirmation' => 'nullable|string|same:password',
            'lat' => 'nullable',
            'long' => 'nullable',
        ];
    }


    public function messages()
    {
        return [

            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ];
    }

}
