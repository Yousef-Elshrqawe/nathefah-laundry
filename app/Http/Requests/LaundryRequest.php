<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LaundryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'                        => 'required|unique:laundries',
//            'phone'                       => 'required|unique:laundries',
            'phone' => 'required|regex:/^5[0-9]{8}$/|unique:laundries',
            'email'                       => 'required|unique:laundries',
            'branch_number'               => 'required|in:one,many',
            'laundry_logo'                => 'required',
            'tax_card'                    => 'nullable',
            'tax_number'                  => 'nullable',
            'company_register'            => 'required',
            'password'                    => 'required',
            'package'                     => 'required',
            'period'                      => 'required',
        ];
    }
    public function messages()
    {
        return [

            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ];
    }
}
