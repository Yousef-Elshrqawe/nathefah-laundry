<?php

namespace App\Http\Requests;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class CreateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:users',
            'email' => 'required|unique:users',
            'country_code' => 'required',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            'phone' => [
                'required',
                function ($attribute, $value, $fail) {
                    $user = User::where('phone', $value)->first();
                    if ($user != null) {
                        if ($user->deleted == 1) {
                            $fail('This account has been previously deleted, contact support to re-register and activate the account');
                        }
                    }
                }
                , 'unique:users'
            ],
        ];


    }


    // هاتلي لغه التطبيق


    public function messages()
    {
       $lang  = App::setLocale($this->header('lang'));
      if ($lang == 'ar') {
                return [
                    'name.required' => 'الاسم مطلوب',
                    'email.required' => 'البريد الالكتروني مطلوب',
                    'country_code.required' => 'كود الدولة مطلوب',
                    'password.required' => 'كلمة المرور مطلوبة',
                    'password.min' => 'كلمة المرور يجب ان تكون اكبر من 6 احرف',
                    'password.max' => 'كلمة المرور يجب ان تكون اقل من 50 حرف',
                    'password_confirmation.required' => 'تأكيد كلمة المرور مطلوب',
                    'password_confirmation.min' => 'تأكيد كلمة المرور يجب ان تكون اكبر من 6 احرف',
                    'password_confirmation.max' => 'تأكيد كلمة المرور يجب ان تكون اقل من 50 حرف',
                    'phone.required' => 'رقم الهاتف مطلوب',
                    'phone.unique' => 'رقم الهاتف مستخدم مسبقا',
                ];
            } else {
                return [
                    'name.required' => 'Name is required',
                    'email.required' => 'Email is required',
                    'country_code.required' => 'Country code is required',
                    'password.required' => 'Password is required',
                    'password.min' => 'Password must be at least 6 characters',
                    'password.max' => 'Password must be less than 50 characters',
                    'password_confirmation.required' => 'Password confirmation is required',
                    'password_confirmation.min' => 'Password confirmation must be at least 6 characters',
                    'password_confirmation.max' => 'Password confirmation must be less than 50 characters',
                    'phone.required' => 'Phone number is required',
                    'phone.unique' => 'Phone number is already used',
                ];
            }

    }


    //fail

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['status' => false, 'message' => $validator->errors()->first()], 401));
    }
}
