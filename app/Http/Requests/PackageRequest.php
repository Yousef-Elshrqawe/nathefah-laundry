<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          //  'name'               => ['required'],
          //  'description'        => ['required'],
            'min_branch'         => ['required'],
            'max_branch'         => ['required'],
            'period_id'         => ['required'],
            'price'              => ['required','numeric']
        ];
    }

    public function messages()
    {
        return [
           //  'name.required'                     => 'Name package is required',
           //  'description.required'              => 'Description package is required',
            'min_branch.required'               => 'Number of min branches is required',
            'max_branch.required'               => 'Number of max branches is required',
            'period_id.required'                => 'Period package is required',
            'price.required'                    => 'Price package is required',
        ];
    }
}
