<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DriverRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

       $currentURL = \Route::current()->getName();

        //Store
        if($currentURL == "branch.store.driver"){
            $unique = 'required|unique:drivers';
            $branch_id = 'nullable';
        }

        //Update
        if($currentURL == "branch.edit.driver"){
            $unique    = 'required';
            $branch_id = 'nullable';
        }

        if($currentURL == "laundry.store.drivers")
        {
            $unique    = 'unique:drivers';
            $branch_id = 'required';
        }

        return [
            'name'         => ['required'],
            'country_code' => ['nullable'],
            'phone'        => ['required',$unique ,'numeric' , 'regex:/^0?5[0-9]{8}$/'],
            'email'        => ['email','required',$unique],
            'branch_id'    => [$branch_id]
        ];
    }

    public function messages()
    {
        return [
            'name.required'              => 'Name is required',
            'email.required'             => 'Email is required',
            'email.email'                => 'Invalid email address',
            'email.unique'               => 'Email is already exist',
            'phone.required'             => 'Phone number is required',
            'phone.unique'               => 'Phone is already exist',
            'phone.regex'                => 'The phone number must be in the format 5xxxxxxxx',
            'country_code.required'      => 'Country Code is required',
            'branch_id.required'         => 'Branch is required',
        ];
    }


//    protected function failedValidation(Validator $validator)
//    {
//        throw new HttpResponseException(response()->json([
//            'status'  => false,
//            'message' => $validator->errors()->first(),
//            'data'    => null,
//        ], 422));
//    }

}
