<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AdminUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:admins,email'.($this->id ? ",$this->id" : ''),
            'username' => 'required|string|min:3|unique:admins,username'.($this->id ? ",$this->id" : ''),
            'password' => 'nullable|string|min:6',
            'password_confirmation' => 'nullable|string|same:password',
        ];
    }

}
