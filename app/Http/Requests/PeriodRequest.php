<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PeriodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
    //        'name'               => ['required'],
            'duration_value'     => ['required'],
            'duration_unit'      => ['required'],
        ];
    }

    public function messages()
    {
        return [
       //     'name.required'                     => 'Name package is required',
            'duration_value.required'           => 'Duration value package is required',
            'duration_unit.required'            => 'Duration unit package is required',
        ];
    }
}
