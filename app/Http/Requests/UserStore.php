<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:users,email',
//           'phone' => 'required|string|unique:users,phone',
            'phone' => 'required|regex:/^5[0-9]{8}$/|unique:users,phone',
            'img' => 'nullable|image',
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|string|same:password',
        ];
    }

    public function messages()
    {
        return [

            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ];
    }

}
