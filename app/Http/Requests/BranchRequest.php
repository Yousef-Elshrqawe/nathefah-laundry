<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch_name'         => ['required','unique:branchs,username'],
            'branch_country_code' => ['required'],
            'branch_phone' => [
                'required',
                'regex:/^(05|5)[0-9]{8}$/',
                'unique:branchs,phone'
            ],
            'user_phone'               => ['required','unique:branchusers,phone']
          //  'email'        => ['email','required','unique:branchs,email']
        ];
    }

    public function messages()
    {
        return [
            'branch_name.required'          => 'User name is required',
            'branch_name.unique'          => 'User name is already exist',
            'email.required'             => 'Email is required',
            'email.email'                => 'Invalid email address',
            'email.unique'               => 'Email is already exist',
            'branch_phone.required'             => 'Phone number is required',
            'branch_phone.unique'               => 'Phone is already exist',
            'branch_country_code.required'      => 'Country Code is required',
            'user_phone.required'               => 'User phone is required',
            'user_phone.unique'                 => 'User phone is already exist',
            'branch_phone.regex' => 'The phone number must be in the format 5xxxxxxxx',

        ];
    }
}
