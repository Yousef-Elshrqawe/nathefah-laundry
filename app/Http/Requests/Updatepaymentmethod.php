<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Updatepaymentmethod extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'payment_type' =>['required'],
            'status'     => ['required'],
        ];
    }
    public function messages()
    {
        return [
            'payment_type'             =>'payment type is required',
            'status.required'          => 'payment method  is required',
        ];
    }
}
