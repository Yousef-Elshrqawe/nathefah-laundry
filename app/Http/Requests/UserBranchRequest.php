<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username'     => ['required'],
            'country_code' => ['required'],
            'phone'        => ['required','unique:branchusers'],
            'email'        => ['email','required','unique:branchusers'],
            'role'          =>['required'],
            'password'     =>['required','confirmed'],
            'password_confirmation' =>['required']
        ];
    }

    public function messages()
    {
        return [
            'username.required'          => 'User name is required',
            'email.required'             => 'Email is required',
            'email.email'                => 'Invalid email address',
            'email.unique'               => 'Email is already exist',
            'phone.required'             => 'Phone number is required',
            'phone.unique'               => 'Phone is already exist',
            'country_code.required'      => 'Country Code is required',
        ];
    }

}
