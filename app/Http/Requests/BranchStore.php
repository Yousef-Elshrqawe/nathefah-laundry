<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BranchStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|string|unique:branchs,username',
//            'phone' => 'required|string|unique:branchs,phone',
            'phone' => [
                'required',
                'regex:/^(05|5)[0-9]{8}$/',
                'unique:branchs,phone'
            ],
            'argent' => 'required|numeric',
            'laundry_id' => 'required|numeric|exists:laundries,id',
            'status' => 'required|in:open,closed',
        /*    'open_time' => 'required|string',
            'closed_time' => 'required|string',*/
            'password' => 'required|string|min:6',
            'password_confirmation' => 'required|string|same:password',
            'lat'=>'required',
            'long'=>'required',
        ];
    }
    public function messages()
    {
        return [

            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ];
    }

}
