<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|min:3',
            'email' => 'required|email|unique:users,email' . ($this->id ? ",$this->id" : ''),
            'password' => 'nullable|string|min:6',
            'password_confirmation' => 'nullable|string|same:password',
//            'phone' => 'required|string|unique:users,phone' . ($this->id ? ",$this->id" : ''),
            'phone' => 'required|regex:/^5[0-9]{8}$/|unique:users,phone' . ($this->id ? ",$this->id" : ''),
            'img' => 'nullable|image',
        ];
    }

    public function messages()
    {
        return [

            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ];
    }

}
