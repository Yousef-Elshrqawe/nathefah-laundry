<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PeriodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'          => $this->id,
            'name'        => $this->name,
            /*'min_branch'  => $this->min_branch,
            'max_branch'  => $this->max_branch,
            'price'       => $this->price*/
            'duration_value' => $this->duration_value,
            'duration_unit'  => $this->duration_unit,
            // محتاج اول  باكدج وتكون  الاقل  سعر
            'packages' =>   new PackageResource($this->minPackage),
        ];
    }
}
