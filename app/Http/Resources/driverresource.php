<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class driverresource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'            =>$this->id,
            'phone'         =>$this->phone,
            'name'          =>$this->name,
            'img'           =>$this->img,
            'country_code'  =>$this->country_code,
            'orderscount'   =>$this->orderscount,
            'rate'          =>$this->rate,
        ];
    }
}
