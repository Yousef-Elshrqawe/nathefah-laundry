<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Models\Laundry\branch;
use App\Models\Laundry\branchcloseingday;
use App\Models\Laundry\Branchuser;
use Illuminate\Http\Request;
use App\Traits\response;
use Illuminate\Validation\Rule;
use Validator;
use Auth;
use App;
use Hash;
use App\Traits\otp;



class ProfileController extends Controller
{
    //
    use response,otp;
    public function edit(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $branchinfo=branch::with('closingdayes','laundry')->select('id','username','phone','lat','long','open_time','closed_time','address','country_code','laundry_id')
        ->find($branch_id);
        $data['branchinfo']=$branchinfo;
        return $this->response(true,'return branch ingo successfully',$data);
    }
    public function update(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $branch=branch::find($branch_id);

        $validator =Validator::make($request->all(), [
        'username'=>'required|unique:branchs,username,'.$branch_id,
        'lat'=>'required',
        'long'=>'required',
        'address'=>'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['message'=>$validator->messages()->first()],403);
        }
        DB::transaction(function()use(&$branch,$request)
        {
                $branch->update([
                'username'=>$request->username,
                'lat'=>$request->lat,
                'long'=>$request->long,
                'address'=>$request->address,

                ]);
                branchcloseingday::where('branch_id',$branch->id)->delete();
        });

        $branch_user = Branchuser::where('branch_id',$branch->id)->first();
        $branch_user->update([
            'username'=>$request->username,
        ]);
        return $this->response(true,'branch info updated successfully');
    }
    public function updatepassword(Request $request){
        $user_id=Auth::guard('branchuser-api')->user()->id;
        $lang=$request->header('lang');
        App::setLocale($lang);

        $Branchuser=Branchuser::find($user_id);
        if(!Hash::check($request->old_password, $Branchuser->password)) {
            return $this->response(false, trans('passwords.The specified password does not match the old password'),null,403);
        }
        $validator =Validator::make($request->all(), [
            'password'=> 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->messages()->first()],403);
            }
            $Branchuser->update([
                'password' => $request->password,
             ]);
        return $this->response(true,trans('passwords.branch password updated successfully'));
    }
    public function editphone(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $branch=branch::find($branch_id);
        if($request->phone!=$branch->phone){
            $branch->update([
                'otp'=>(string)mt_rand(1000, 9999),
            ]);
            $this->sendSmsMessage($request,'branch',$branch->otp,false);
            $data['messageText']=$branch->otp;
            return $this->response(true,trans('passwords.please send otp in the next request with phone number'),$data);
        }else{
            return $this->response(false,trans('passwords.this number is already exist'));
        }
    }
    public function checkotp(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $branch=branch::find($branch_id);
        if($branch->otp==$request->otp)
        return $this->response(true,'go to nexrt request');

        return $this->response(false,'otp is wrong',null,407);
    }
    public function updatephone(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);



        $branch=branch::find($branch_id);

        $existing = DB::table('branchs')
            ->where('phone', $request->phone)
            ->where('id', '!=', $branch->id)
            ->first();

        if ($existing) {
            return $this->response(false, trans('validation.phone_already_taken'),null,403);

        }

        if($branch->otp==$request->otp && $branch->phone==$request->oldphone){
            $branch->update([
              'phone'=>$request->phone
            ]);

            $branchUser = Branchuser::where('branch_id',$branch->id)->first();
            $branchUser->update([
                'phone'=>$request->phone
            ]);

        }else{
            return $this->response(false, trans('passwords.otp is wrong or phone'));
        }
        return $this->response(true,      trans('passwords.phone updated successfully'));
    }
    public  function changelang(Request $request){
        $lang=$request->header('lang');
        $user=Auth::guard('branchuser-api')->user();
        App::setLocale($lang);
        $user->update([
            'lang'=>$lang
        ]);
        return $this->response(true,'update language successfully your lang become '.$lang );
   }
   //delete branch
    public function deleteBranch(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = branch::find($branch_id);
        $branch->update([
            'deleted' => "1" ,
        ]);
        Auth::guard('branchuser-api')->logout();
        return $this->response(true, 'branch deleted successfully');
    }
}
