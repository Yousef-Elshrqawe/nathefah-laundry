<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use App\Interfaces\DriverRepositoyInterface;
use App\Interfaces\NotificationRepositoryinterface;
use App\Models\Notification\drivernotifytype;
use Illuminate\Http\Request;
use App\Models\Driver\Driver;
use App\Models\Order\{order, OrderDriveryStatus};
use App\Traits\response;
use Illuminate\Support\Facades\DB;
use Validator;
use Auth;

class driverController extends Controller
{
    //
    use response;

    public function __construct(DriverRepositoyInterface $DriverRepository, NotificationRepositoryinterface $NotificationRepository)
    {
        $this->DriverRepository = $DriverRepository;
        $this->NotificationRepository = $NotificationRepository;
    }

    // get online driver
    public function avilabledriver(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $avilabledriver = Driver::select('id', 'name', 'phone', 'country_code', 'img', 'classification')
            ->where('deleted', '0')
            ->where('branch_id', $branch_id)
            ->where('status', 'online')->get();
        foreach ($avilabledriver as $driver) {
            $driver->img = $this->DriverRepository->getimg($driver->img);
        }
        $data['avilabledriver'] = $avilabledriver;
        return $this->response(true, 'get avilable driver successfully', $data);
    }

    // get offline driver
    public function offlinedriver(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $avilabledriver = Driver::select('id', 'name', 'phone', 'country_code', 'img', 'classification')
            ->where('deleted', '0')
            ->where('branch_id', $branch_id)
            ->where('status', 'offline')->get();
        foreach ($avilabledriver as $driver) {
            $driver->img = $this->DriverRepository->getimg($driver->img);
        }
        $data['avilabledriver'] = $avilabledriver;
        return $this->response(true, 'get offline driver successfully', $data);
    }

    public function alldriver(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $avilabledriver = Driver::select('id', 'name', 'status', 'phone', 'country_code', 'classification', 'deleted')
            ->where('deleted', '0')
            ->where('branch_id', $branch_id)
            ->get();
        foreach ($avilabledriver as $driver) {
            $driver->img = $driver->img
                ? asset("/uploads/driver/img/" . $driver->img)
                : asset("/uploads/driver/img/default.png");
        }
        $data['avilabledriver'] = $avilabledriver;
        return $this->response(true, 'get all driver successfully', $data);
    }

    public function assignorder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'driver_id' => 'required',
            'order_id' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }

        $order_id = $request->order_id;
        $driver_id = $request->driver_id;
        try {
            DB::beginTransaction();
            order::findorfail($order_id)->update([
                'driver_id' => $driver_id
            ]);
            $driver = Driver::find($driver_id);
            $orderdeliverystatus = OrderDriveryStatus::where('order_id', $order_id)->where('confirmation', false)->first();
            $orderdeliverystatus->update([
                'driver_id' => $driver_id,
            ]);
            $title = 'طلب جديد';
            $body = 'لقد استلمت طلب جديد الان';
            if ($driver->lang == 'en') {
                $title = 'new order';
                $body = 'you recive new order from laundry now';
            }
            $titles = ['ar' => 'طلب جددي', 'en' => 'new order'];
            $bodys = ['ar' => 'new order', 'en' => 'you recive new order from laundry now'];
            $drivernotifytype = drivernotifytype::where('driver_id', $driver_id)->where('notificationtype_id', 3)->where('status', 1)->first();
            if ($drivernotifytype) {
                $this->NotificationRepository->sendnotification('driver', $driver_id, $title, $body, 1, null, null);
                $this->NotificationRepository->createnotification('driver', $driver_id, $titles, $bodys);
            }
            DB::commit();
            return $this->response(true, 'assign order successfully');
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->response(false, 'some thing wrong ', null, 408);
        }

    }

    public function assignorders(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'orders' => 'required',
            'driver_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $driver_id = $request->driver_id;
        try {
            DB::beginTransaction();
            foreach ($request->orders as $order) {
                order::findorfail($order)->update([
                    'driver_id' => $driver_id
                ]);
                $orderdeliverystatus = OrderDriveryStatus::where('order_id', $order)->where('confirmation', false)->first();
                $orderdeliverystatus->update([
                    'driver_id' => $driver_id,
                ]);
            }

            $driver = Driver::find($driver_id);
            $drivernotifytype = drivernotifytype::where('driver_id', $driver_id)->where('notificationtype_id', 1)->where('status', 1)->first();

            if ($drivernotifytype) {

                $title = 'طلب جديد';
                $body = 'لقد استلمت طلب جديد الان';
                if ($driver->lang == 'en') {
                    $title = 'new order';
                    $body = 'you recive new order from laundry now';
                }
                $titles = ['ar' => 'طلب جددي', 'en' => 'new order'];
                $bodys = ['ar' => 'new order', 'en' => 'you recive new order from laundry now'];
                    $this->NotificationRepository->sendnotification('driver', $driver_id, $title, $body, 1, null, null);
                    $this->NotificationRepository->createnotification('driver', $driver_id, $titles, $bodys);
            }

            DB::commit();
            return $this->response(true, 'assign order successfully');
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->response(false, 'some thing wrong ', null, 408);
        }
    }

    public function changedriver(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
            'driver_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()->first()], 403);
        }
        try {
            DB::beginTransaction();
            $order_id = $request->order_id;
            $order = order::find($order_id);
            $order->update([
                'driver_id' => $request->driver_id,
                'delivery_status' => null
            ]);
            DB::commit();
            return response()->json(['status' => true, 'message' => 'driver change successfuly']);
        } catch (\Exception $ex) {
            DB::rollback();
            return response()->json(['status' => false, 'message' => 'some thing wrong'], 401);
        }
    }

    public function filterdriver(Request $request)
    {
        // الحصول على branch_id للمستخدم الحالي
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;

        // إعداد الفلاتر الأساسية لحالة السائقين
        $status = ['online', 'offline'];
        if ($request->status === 'online') {
            $status = ['online'];
        } elseif ($request->status === 'offline') {
            $status = ['offline'];
        }

        // إعداد اتجاه الترتيب بناءً على الخيار الأبجدي
        $order = $request->alphabetic ? 'asc' : 'desc';

        // بناء الاستعلام الأساسي
        $query = DB::table('drivers')
            ->where('drivers.branch_id', $branch_id)
            ->whereIn('drivers.status', $status);

        // إذا كان فلتر التقييم مفعل، ننضم إلى جدول drivers_rate ونحدد الحقول المطلوبة مع تجميع النتائج
        if ($request->rated) {
            $query = $query->join('drivers_rate', 'drivers_rate.driver_id', '=', 'drivers.id')
                ->select(
                    'drivers.name',
                    'drivers.id',
                    'drivers.status',
                    'drivers.phone',
                    'drivers.country_code',
                    'drivers.img'
                )
                ->groupBy(
                    'drivers.id',
                    'drivers.name',
                    'drivers.status',
                    'drivers.phone',
                    'drivers.country_code',
                    'drivers.img'
                );
        } else {
            // في حالة عدم استخدام فلتر التقييم، نحدد الحقول مباشرة
            $query = $query->select('name', 'id', 'status', 'phone', 'country_code', 'img');
        }

        // ترتيب النتائج بناءً على اسم السائق
        $query = $query->orderBy('name', $order);

        // تنفيذ الاستعلام والحصول على النتائج
        $drivers = $query->get();

        // تحديث رابط الصورة لكل سائق باستخدام الـ DriverRepository
        foreach ($drivers as $driver) {
            $driver->img = $this->DriverRepository->getimg($driver->img);
        }

        $data['drivers'] = $drivers;
        return $this->response(true, 'get drivers successfully', $data);
    }


    public function search(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;

        $drivers = DB::table('drivers')
            ->select('name', 'drivers.id', 'drivers.status', 'phone', 'country_code', 'img')
            ->where('branch_id', $branch_id)
            ->where(function ($query) use ($request) {
                if (!empty($request->name)) {
                    $query->where('name', 'like', "%{$request->name}%");
                }
                if (!empty($request->phone)) {
                    $query->where('phone', 'like', "%{$request->phone}%");
                }
                if (!empty($request->status)) {
                    $query->whereIn('status', (array)$request->status);
                }
            })
            ->get();

        foreach ($drivers as $driver) {
            $driver->img = $driver->img
                ? asset("/uploads/driver/img/" . $driver->img)
                : asset("/uploads/driver/img/default.png");
        }

        $data['drivers'] = $drivers;
        return $this->response(true, 'get driver successfully', $data);
    }

}
