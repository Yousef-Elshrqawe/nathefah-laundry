<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Order\order;
use App\Traits\response;
use App\Traits\queries\serviceTrait;
use App\Models\Transaction;
use Auth;
use App;
use Carbon\Carbon;

class HomeController extends Controller
{
    //
    use response,serviceTrait;
    public function getbalance(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);

        $branch_balance=DB::table('branchs')->select('balance','pending_balance')->where('id',$branch_id)->first();


        $Balancetoday=0;

        $enternancebalance=Transaction::selectRaw('sum(amount) as amount')
        ->whereDate('created_at',Carbon::now())->where('transaction','enternance')
        ->where('branch_id',$branch_id)
        ->GroupBy('transaction')->first();

        $externalbalance=Transaction::selectRaw('sum(amount) as amount')
        ->whereDate('created_at',Carbon::now())->where('transaction','externance')
        ->where('branch_id',$branch_id)
        ->GroupBy('transaction')->first();

        if($enternancebalance==null){
            $enternancebalance==0;
        }else{
            $enternancebalance=$enternancebalance->amount;
        }
        if($externalbalance==null){
            $enternancebalance==0;
        }else{
            $externalbalance=$externalbalance->amount;
        }

        $Balancetoday=$enternancebalance-$externalbalance;

        $data['Balance']=$branch_balance->balance;
        $data['pending_balance']=$branch_balance->pending_balance;
        $data['Balancetoday']=$Balancetoday;


        return $this->response(true,'get balance success',$data);
    }
    public function branchinfo(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $branch=DB::table('branchs')
        ->join('laundries','laundries.id','=','branchs.laundry_id')
        ->select('branchs.status as status','laundries.name as name','laundries.logo as logo','branchs.username','branchs.delivery_status','branchs.cash','branchs.visa')
        ->where('branchs.id',$branch_id)
        ->first();
        if($branch->logo==null){
            $branch->logo=asset("/uploads/branches/logos/default/images.jpg");
        }else{
            $branch->logo=asset("uploads/laundry/logos/".$branch->logo);
        }
        $data['branch']=$branch;
        return $this->response(true,'get branch info successfully',$data);
    }
    public $orders_id=[];
    public $items=[];
    public function brenchreview(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        $orders=DB::table('orders')
        ->join('order_reviews','order_reviews.order_id','=','orders.id')
        ->select('customer_name','order_reviews.desc as comment','order_reviews.rate','orders.created_at','orders.id as order_id','order_reviews.created_at as review_time')
        ->where('orders.rate',true)
        ->where('orders.branch_id',$branch_id)
        ->orderBy('orders.created_at','desc')
        ->paginate(10);
         foreach($orders as $order){
             //$order->rate=$this->BranchRepository->branchrate($order->rate);
             $order->created_at=date('Y-m-d', strtotime($order->created_at));
             $order->time=date('h:m a', strtotime($order->created_at));
         }
         foreach($orders as $order){
            array_push($this->orders_id,$order->order_id);
            array_push($this->items,$this->items($order->order_id,null,$lang));
            }
        $orders=$this->orderserives($orders,$lang);
        return $orders;
    }
    public function latestreview(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        $orders=DB::table('orders')
        ->join('order_reviews','order_reviews.order_id','=','orders.id')
        ->select('customer_name','order_reviews.desc as comment','order_reviews.rate','orders.created_at','orders.id as order_id','order_reviews.created_at as review_time')
        ->where('orders.rate',true)
        ->where('orders.branch_id',$branch_id)
        ->get()
        ->take(6);
         foreach($orders as $order){
             //$order->rate=$this->BranchRepository->branchrate($order->rate);
             $order->created_at=date('Y-m-d', strtotime($order->created_at));
             $order->time=date('h:m a', strtotime($order->created_at));
         }
         foreach($orders as $order){
            array_push($this->orders_id,$order->order_id);
            array_push($this->items,$this->items($order->order_id,null,$lang));
            }
        $orders=$this->orderserives($orders,$lang);
        $data['latest_order']=$orders;
        return $this->response(true,'get latest order successfult',$data);
        return $orders;
    }

    public  function balancetransaction(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $balance=Transaction::select('branch_balance as balance','amount','transaction','order_id','created_at', 'updated_at','pending')
        ->where('branch_id',$branch_id)
        ->orderBy('created_at','desc')
        ->get();
        foreach($balance as $transaction){
              $transaction->amount=round($transaction->amount,1);
              $transaction->balance=round($transaction->balance,1);
        }
        $data['balance']=$balance;
        return $this->response(true,'get balance success',$data);
    }



}

        // $Balance=DB::table('orders')
        // ->join('order_detailes','order_detailes.order_id','=','orders.id')
        // ->selectRaw('sum(order_detailes.price) as price')
        // ->where('branch_id',$branch_id)
        // ->where('checked',true)
        // ->where('confirmation','accepted')
        // ->groupBy('orders.branch_id')
        // ->first();
        // $argentpalance=DB::table('orders')
        // ->join('argent','argent.order_id','=','orders.id')
        // ->selectRaw('sum(argent.price) as price')
        // ->where('orders.branch_id',$branch_id)
        // ->where('checked',true)
        // ->where('confirmation','accepted')
        // ->groupBy('orders.branch_id')
        // ->first();
        // $Balancetoday=DB::table('orders')
        // ->join('order_detailes','order_detailes.order_id','=','orders.id')
        // ->selectRaw('sum(order_detailes.price) as price')
        // ->where('branch_id',$branch_id)
        // ->where('checked',true)
        // ->where('confirmation','accepted')
        // ->whereDate('orders.created_at', Carbon::today())
        // ->groupBy('orders.branch_id')
        // ->groupBy('orders.created_at')
        // ->first();
        // $argentpalancetoday=DB::table('orders')
        // ->join('argent','argent.order_id','=','orders.id')
        // ->selectRaw('sum(argent.price) as price')
        // ->where('orders.branch_id',$branch_id)
        // ->where('checked',true)
        // ->where('confirmation','accepted')
        // ->groupBy('orders.created_at')
        // ->groupBy('orders.branch_id')
        // ->whereDate('orders.created_at', Carbon::today())
        // ->first();
       // $Balance->price+=$argentpalance->price;
        // if($Balance==null){
        //     $data['Balance']=['price'=>0];
        // }else{
        //     $data['Balance']=$Balance;
        // }
        // if($Balancetoday==null){
        //     $data['Balancetoday']=0;
        // }else{
        //     $argentpalancetoday=0;
        //     if($argentpalancetoday!=null)
        //     $argentpalancetoday=$argentpalancetoday->price;
        //     $Balancetoday->price+=$argentpalancetoday;
        //     $data['Balancetoday']=$Balancetoday->price;
        // }
