<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use App\Models\Day;
use App\Models\Laundry\branchservice;
use App\Traits\FilterDateBranchTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Laundry\branch;
use App;
use Auth;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;
use Validator;


class ExpectedTimeController extends Controller
{
    use FilterDateBranchTrait;

    //index
    public function index($branch_id = null)
    {
        $branch_id = Auth::guard('branch')->user()->branch_id;
        $branch = Branch::find($branch_id);

        $rows = branchservice::where('branch_id', $branch->id)->get();

        return view('branch.expected_time.index', compact('rows'));
    }


    //edit
    public function edit($id, $branch_id = null)
    {

        $branch_id = Auth::guard('branch')->user()->branch_id;
        $branch = Branch::find($branch_id);
        $row = branchservice::where('id', $id)->first();
        return view('branch.expected_time.edit', compact('row', 'branch'));


    }

    //update
    public function update(Request $request, $id, $branch_id = null)
    {
        $branch_id = Auth::guard('branch')->user()->branch_id;
        $branch = Branch::find($branch_id);
        $row = branchservice::where('id', $id)->first();
        $validator = Validator::make($request->all(), [
            'expected_time' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $row->update([
            'expected_time' => $request->expected_time,
        ]);

        Alert::success('Success', 'Expected time updated successfully');
        return Redirect::route('branch.expected.time');
    }


}
