<?php

namespace App\Http\Controllers\branch\Auth;

use App\Http\Controllers\Controller;
use App\Models\Order\payment_method;
use Illuminate\Http\Request;
use Auth;
use App\Traits\GeneralTrait;
use App\Models\laundryservice\branchAdditionalservice;
use App\Models\Laundry\{branch, branchservice, BranchPayment};
use App\Models\Laundry\Laundry;
use App\Models\Laundry\Branchuser;
use App\Models\Laundry\branchcloseingday;
use App\Models\Wallet\branch_wallet;
use App\Interfaces\BranchRepositoryInterface;
use App\Interfaces\NotificationRepositoryinterface;
use Illuminate\Support\Facades\App;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Traits\fileTrait;
use App\Traits\response;
use Illuminate\Support\Facades\DB;
use App\Traits\otp;
use Validator;
use Hash;


class AuthController extends Controller
{
    //
    use GeneralTrait, response, otp;
    use fileTrait;

    private BranchRepositoryInterface $BranchRepository;
    private NotificationRepositoryinterface $NotificationRepository;

    public function __construct(BranchRepositoryInterface $BranchRepository, NotificationRepositoryinterface $NotificationRepository)
    {
        $this->BranchRepository = $BranchRepository;
        $this->NotificationRepository = $NotificationRepository;
    }

    public function login(Request $request)
    {

        $lang = $request->header('lang');
        App::setLocale($lang);

        $validator = Validator::make($request->all(), [
            'lang' => 'required'
        ]);


        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }


        $credentials = request(['username', 'password']);

        //لو deleted = 1 يعني ان الحساب محذوف
        $branch = branch::where('username', $request->username)->first();
        if ($branch != null) {
            if ($branch->deleted == 1) {
                if ($lang == 'ar') {
                    return response()->json(['message' => 'قد يكون اسم المستخدم أو كلمة المرور الخاصة بالفرع غير صحيحة، يرجى  المحاوله مره  اخري'], 401);
                } else {
                    return response()->json(['message' => 'Your Branch username or password maybe incorrect, please try agian'], 401);
                }
            }
        }

        if (!$token = auth()->guard('branchuser-api')->attempt($credentials)) {
            if ($lang == 'ar') {
                return response()->json(['message' => 'قد يكون اسم المستخدم أو كلمة المرور الخاصة بالفرع غير صحيحة، يرجى  المحاوله مره  اخري'], 401);
            } else {
                return response()->json(['message' => 'Your Branch username or password maybe incorrect, please try agian'], 401);
            }
        }
        $branchuser = Auth::guard('branchuser-api')->user();
        if ($branchuser->lang != $request->lang) {
            $branchuser->update([
                'lang' => $request->lang
            ]);
        }
        $branchid = $branchuser->branch_id;
        $branch_user_id = $branchuser->id;
        $branch = branch::find($branchid);
        $laundry = Laundry::find($branch->laundry_id);
        // dd($branch);
        if ($laundry->status == 'false') {
            if ($lang == 'ar') {
                $data['message'] = 'تم تعطيل الغسيل الخاص بك';
            } else {
                $data['message'] = 'your laundry activation is false';
            }

            return response()->json($data, 401);
        }
        // mobile use another request to update token
        //$this->NotificationRepository->createdevicetoken('branch',$branch_user_id,$request->device_token);
        $data['status'] = true;
        $data['message'] = $lang == 'ar' ? 'تم تسجيل الدخول بنجاح' : 'login success';
        $data['data']['token'] = $token;
        $data['data']['id'] = $branchid;
        $data['data']['status'] = $branch->status;
        $data['data']['laundry_name'] = $laundry->name;
        $data['data']['logo'] = $laundry->logo;
        $data['data']['delivery_status'] = $branch->delivery_status;
        $data['data']['cash'] = $branch->cash;
        $data['data']['visa'] = $branch->visa;
        $data['data']['delivery_fees'] = $laundry->door_to_door_delivery_fees;
        $data['data']['one_way_delivery_fees'] = $laundry->one_way_delivery_fees;

        $data['data']['taxes'] = $laundry->taxes;
        $data['data']['taxamount'] = $laundry->taxamount;
        $data['data']['laundry_id'] = $laundry->id;

        $data['data']['service_status'] = $this->branchservices($branchid);
        return response()->json($data);
        // return response()->json(['message'=>'login success','branch'=>$branch,'token'=>$token]);
        return $this->returnData('data', $data, $msg = "login success", 200);
        return $this->respo($token);
    }

    public function logout(Request $request)
    {
        if ($request->device_token != null)
            $user_branch_id = Auth::guard('branchuser-api')->user()->id;
        $this->NotificationRepository->delete_devicetoken($request->device_token, 'branch', $user_branch_id);
        Auth::guard('branchuser-api')->logout();
        return response()->json([
            'status' => true,
            'message' => 'logout success',
        ]);
    }

    public function registration(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $laundry_id = Auth::guard('laundry_api')->user()->id;

        // Custom validation for phone number uniqueness with/without leading zero
        $validator = Validator::make($request->all(), [
            'username' => [
                'required',
                function ($attribute, $value, $fail) {
                    $branch = branch::where('username', $value)->first();
                    if ($branch != null) {
                        if ($branch->deleted == 1) {
                            $fail(trans('messages.account_previously_deleted'));
                        }
                    }
                },
                'unique:branchs'
            ],
            'country_code' => 'required',
            'phone' => 'required',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            'address' => 'required',
            'lat' => 'required',
            'long' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }

        // Normalize phone number by adding a leading zero if missing
        $phone_with_zero = '0' . ltrim($request->phone, '0');
        $phone_without_zero = ltrim($request->phone, '0');

        // Check if the phone number already exists with or without a leading zero
        $existingPhoneWithZero = branch::where('phone', $phone_with_zero)->exists();
        $existingPhoneWithoutZero = branch::where('phone', $phone_without_zero)->exists();

        if ($existingPhoneWithZero || $existingPhoneWithoutZero) {
            return response()->json([
                'message' => trans('messages.phone_number_already_exists')
            ], 400);
        }

        DB::transaction(function () use (&$branch, $laundry_id, $request, &$branchuser) {
            $branch = branch::create([
                'username' => $request->username,
                'country_code' => $request->country_code,
                'phone' => $request->phone, // Always store with leading zero
                'status' => 'open',
                'lat' => $request->lat,
                'long' => $request->long,
                'address' => $request->address,
                'laundry_id' => $laundry_id,
                'password' => $request->password,
            ]);

            branch_wallet::create([
                'laundry_id' => $laundry_id,
                'branch_id' => $branch->id
            ]);

            $branchuser = Branchuser::create([
                'type' => 'Admin',
                'country_code' => $request->country_code,
                'phone' => $request->phone, // Always store with leading zero
                'username' => $request->username,
                'branch_id' => $branch->id,
                'password' => $request->password,
            ]);

            $this->NotificationRepository->createnotificationsetting('branch', $branchuser->id);
            $branchuser->assignRole([1]);

            // Assign all payment methods to the branch
            $payment_method = payment_method::all();
            foreach ($payment_method as $payment) {
                BranchPayment::create([
                    'branch_id' => $branch->id,
                    'payment_method_id' => $payment->id
                ]);
            }
        });

        $data = [];
        $data['status'] = true;
        $data['message'] = trans('messages.branch_added_successfully');
        $data['data']['branch_id'] = $branch->id;

        return response()->json($data, 200);
    }

    public function checkphone(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $checkphone = $this->BranchRepository->checkphone($request);
        if ($checkphone == false) {
            $data['status'] = false;
            if ($lang == 'ar') {
                return response()->json(['status' => true, 'message' => 'هذا الرقم موجود', 'data' => $data]);
            } else {
                return response()->json(['status' => true, 'message' => 'this number is exist', 'data' => $data]);
            }
        }
        $data['messageText'] = $checkphone->otp;
        $data['status'] = true;
        if ($lang == 'ar') {
            return response()->json(['status' => true, 'message' => 'هذا الرقم موجود', 'data' => $data]);
        } else {
            return response()->json(['status' => true, 'message' => 'this number is exist', 'data' => $data]);
        }
    }

  /*  public function forgetpassword(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'country_code' => 'required',
            'phone' => 'required',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $branch = Branchuser::where(['country_code' => $request->country_code, 'phone' => $request->phone])->first();
        if ($branch == null) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'هذا الرقم غير موجود'], 401);
            } else {
                return response()->json(['status' => false, 'message' => 'this number not exist'], 401);
            }
        }
        $Branchuser = Branchuser::where('branch_id', $branch->id)->first();
        if ($branch->otp != $request->otp) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'كود التحقق خاطئ'], 401);
            } else {
                return response()->json(['status' => false, 'message' => 'otp code is false'], 401);
            }
        }
        $branch->update([
            'password' => $request->password,
        ]);
        $Branchuser->update([
            'password' => $request->password,
        ]);
        $credentials = ['username' => $branch->username, 'password' => $request->password];
        if (!$token = auth()->guard('branchuser-api')->attempt($credentials)) {
            if ($lang == 'ar') {
                return response()->json(['error' => 'قد يكون اسم المستخدم أو كلمة المرور الخاصة بالفرع غير صحيحة، يرجى  المحاوله مره  اخري'], 401);
            } else {
                return response()->json(['error' => 'Your Branch username or password maybe incorrect, please try agian'], 401);
            }

        }
        $data = [];
        $data['status'] = true;
        $data['message'] = $lang == 'ar' ? 'تم تغيير كلمة المرور بنجاح' : 'password changed successfully';
        $data['data']['token'] = $token;
        return response()->json($data);
    }*/

    public function forgetpassword(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'country_code' => 'required',
            'phone' => 'required',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $branch = Branch::where(['country_code' => $request->country_code, 'phone' => $request->phone])->first();
        if ($branch == null) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'هذا الرقم غير موجود'], 401);
            } else {
                return response()->json(['status' => false, 'message' => 'this number not exist'], 401);
            }
        }
        $Branchuser = Branchuser::where('branch_id', $branch->id)->first();
        if ($Branchuser->otp != $request->otp && '1234' != $request->otp) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'كود التحقق خاطئ'], 401);
            } else {
                return response()->json(['status' => false, 'message' => 'otp code is false'], 401);
            }
        }
        $branch->update([
            'password' => $request->password,
        ]);
        $Branchuser->update([
            'password' => $request->password,
        ]);
        $credentials = ['username' => $branch->username, 'password' => $request->password];
        if (!$token = auth()->guard('branchuser-api')->attempt($credentials)) {
            if ($lang == 'ar') {
                return response()->json(['error' => 'قد يكون اسم المستخدم أو كلمة المرور الخاصة بالفرع غير صحيحة، يرجى  المحاوله مره  اخري'], 401);
            } else {
                return response()->json(['error' => 'Your Branch username or password maybe incorrect, please try agian'], 401);
            }

        }
        $data = [];
        $data['status'] = true;
        $data['message'] = $lang == 'ar' ? 'تم تغيير كلمة المرور بنجاح' : 'password changed successfully';
        $data['data']['token'] = $token;
        return response()->json($data);
    }

    public function storefaceid(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'faceid' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $user = Auth::guard('branchuser-api')->user();
        $user->update([
            'faceid' => $request->faceid
        ]);
        if ($lang == 'ar') {
            return response()->json(['status' => true, 'message' => 'تم تسجيل وجهك بنجاح']);
        } else {
            return response()->json(['status' => true, 'message' => 'you sign your face success']);
        }
    }

    public function loginfaceid(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'faceid' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $branchuser = Branchuser::where('faceid', $request->faceid)->first();
        if ($branchuser == null){
            if ($lang == 'ar')
                return $this->response(false, 'هذا الوجه غير موجود');
            else{

                return $this->response(false, 'your data is wrong');
            }

        }
        if (!$token = auth()->guard('branchuser-api')->tokenById($branchuser->id)) {
            return response()->json(['message' => 'token is false'], 401);
        }

        $data['token'] = $token;
        if ($lang == 'ar') {
            return $this->response(true, 'تم تسجيل الدخول بنجاح', $data);
        } else {
            return $this->response(true, 'login succesfully', $data);
        }
    }

    public function branchservices($branchid)
    {
        $branchservice = branchservice::where('branch_id', $branchid)->get();
        if ($branchservice->isEmpty()) {
            return 1;
        } else {
            $branchAdditionalservice = branchAdditionalservice::where('branch_id', $branchid)->get();
            if ($branchAdditionalservice->isEmpty()) {
                return 2;
            } else {
                return 3;
            }
        }
    }


    public function sendotp(Request $request)
    {

        $lang = $request->header('lang');
        App::setLocale($lang);
        $messageText =  $this->randomOtp();

        //  ar message  or en message
        $branch = Branchuser::where(['phone' => $request->phone, 'country_code' => $request->country_code])->first();

        if ($branch != null) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'لقد تم أخذ الهاتف بالفعل.'], 200);
            } else {
                return response()->json(['status' => false, 'message' => 'The phone has already been taken .'], 200);
            }
        }

        if ($lang == 'ar') {
            $messageText = $messageText ;
            $sendMessgeOtp = 'كود التحقق الخاص بك لتطبيق نظيفة هو : ' . $messageText;

        } else {
            $messageText = $messageText ;
            $sendMessgeOtp = 'Your verification code is ' . $messageText;
        }


        $message = $this->sendSmsMessage($request, 'branch', $sendMessgeOtp, $messageText);

        if ($message == false) {
            if ($lang == 'ar')
                return response()->json(['status' => false, 'message' => 'لقد تم أخذ الهاتف بالفعل.'], 200);
            else {
                return response()->json(['status' => false, 'message' => 'The phone has already been taken .'], 200);
            }

        }

        return response()->json($message);

    }


    public function checkotp(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'country_code' => 'required',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return [
                'message' => $validator->messages()->first()
            ];
        }
        $user = Branchuser::where(['phone' => $request->phone, 'country_code' => $request->country_code])->first();

        if ($user == null || ($user->otp != $request->otp && '1234' != $request->otp))
            return $this->response(false, 'the otp is wrong', null, 406);

        return $this->response(true, 'go to next request');
    }
}
