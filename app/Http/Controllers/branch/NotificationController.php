<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Traits\response;
use Illuminate\Http\Request;
use App\Models\Notification\branchnotifytype;
use App\Interfaces\NotificationRepositoryinterface;
use Auth;
use App;

class NotificationController extends Controller
{
    //
    use response;
    private NotificationRepositoryinterface $NotificationRepository;
    public function __construct(NotificationRepositoryinterface $NotificationRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
    }
    public function getnotificationsetting(Request $request){
        $user_branch_id=Auth::guard('branchuser-api')->user()->id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $notifications=DB::table('notificationtypes')
        ->join('notificationtypetranslations','notificationtypetranslations.notificationtype_id','=','notificationtypes.id')
        ->join('branchnotifytype','branchnotifytype.notificationtype_id','=','notificationtypes.id')
        ->select('branchnotifytype.id as id','notificationtypetranslations.name','branchnotifytype.status' ,'notificationtypes.id as notification_id' , 'notificationtypes.created_at')
        ->where('locale',$lang)
        ->where('user_branch_id',$user_branch_id)
      // ->groupBy('notificationtypetranslations.notificationtype_id')
        ->get();
        $data['data']['notifications']=$notifications;
        return $this->response(true,'get avilable driver successfully',$data);
    }
    public function updatenotification(Request $request){
        $notification_id=$request->notification_id;
        $notification= branchnotifytype::find($notification_id);
        if($notification->status==true){
            $notification->update([
                'status'=>false
            ]);
        }else{
            $notification->update([
                'status'=>true
            ]);
        }
        return $this->response(true,'notification updated successfully');
    }
    public function updatetoken(Request $request){
        $user_id=Auth::guard('branchuser-api')->user()->id;
        $this->NotificationRepository->createdevicetoken('branch',$user_id,$request->device_token);
        return $this->response(true,'fcm token updated successfuly');
    }
    public function getnotification(Request $request){
        $lang=$request->header('lang');
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $notifications=$this->NotificationRepository->getnotification('branch',$branch_id,$lang);
        $data['notification']=$notifications;
        return $this->response(true,'get notification success',$data);
    }
}
