<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\{UpdateDeliveyStatus,Updatepaymentmethod};
use App\Interfaces\BranchRepositoryInterface;

use Illuminate\Support\Facades\DB;
use App\Interfaces\OrderRepositoryInterface;
use App\Models\Laundry\branch;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Models\Laundry\BranchPayment;
use Carbon\Carbon;
use Auth;
use App;

class SettingController extends Controller
{
    //
    use response;
    public function __construct(OrderRepositoryInterface $OrderRepository,BranchRepositoryInterface $BranchRepository)
    {
        $this->OrderRepository   =$OrderRepository;
        $this->BranchRepository  =$BranchRepository;
    }
    public function getsetting(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $branch=branch::select('delivery_status','cash','visa')->find($branch_id);
        $lang=$request->header('lang');
        App::setLocale($lang);

        $data['setting']=[['name'=> __('admin.cash'),'status'=>$branch->cash],['name'=> __('admin.delivery_status'),'status'=>$branch->delivery_status],['name'=> __('admin.visa'),'status'=>$branch->visa]];
        return $this->response(true,'get setting success',$data);
    }
    public function updatestatus(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $branch=branch::find($branch_id);
        if($branch->status=='open'){
            $branch->update([
                'status'=>'closed'
              ]);
        }else{
            $branch->update([
                'status'=>'open'
              ]);
        }
        return $this->response(true,'update pranch status successfully');
    }
    public function avilabledayes(){
        $date=[];
         for($i=0;$i<7;$i++){
            array_push($date, date('d-m-y', strtotime("+$i days")));
          }
        $data['date']=$date;
        return $this->response(true,'avilable dates',$data);
    }

    public function update_delivery_status(Request $request ,UpdateDeliveyStatus $UpdateDeliveyStatus){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        // here user turn of delivery so we check first that he has no order in delivery
        if($request->delivery_status==false){
            $deliveryorder=$this->OrderRepository->check_delivery_order($branch_id);
            if($deliveryorder>0)
            return $this->response(false,'you have in delivery order',null,409);
        }
        branch::find($branch_id)->update(['delivery_status'=>$request->delivery_status]);
        return $this->response(true,'branch on delivery updated successfulu');
    }
    public function update_payment_type(Request $request,Updatepaymentmethod $Updatepaymentmethod){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        branch::find($branch_id)->update([$request->payment_type=>$request->status]);
        return $this->response(true,'payment updated successfuly');
    }

    public function getpaymentmethod(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);

        $paymentmethods=$this->BranchRepository->get_paymet_methods($branch_id,$lang);



         $data  ['payment_methods']=$paymentmethods;
        return $this->response(true,'get methods successfult',$data);
    }


     public function update_payment_method(Request $request){

        $lang=$request->header('lang');
        App::setLocale($lang);
        $paymentmethods=BranchPayment::find($request->payment_id);
        $paymentmethods->update(['status'=>$request->status]);
        return $this->response(true,'update payment method successfuly');
     }

}
