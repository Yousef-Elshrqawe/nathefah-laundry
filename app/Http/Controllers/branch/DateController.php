<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use App\Models\Day;
use App\Traits\FilterDateBranchTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Laundry\branch;
use App;
use Auth;
use Illuminate\Support\Facades\Redirect;
use RealRashid\SweetAlert\Facades\Alert;
use Validator;


class DateController extends Controller
{
    use FilterDateBranchTrait;

    //index
    public function index($branch_id = null)
    {

        if ($branch_id) {
            $branch = Branch::find($branch_id);
        } else {
            $branch_id_branch = Auth::guard('branch')->user()->branch_id;
            $branch = Branch::find($branch_id_branch);
        }
        $rows = Day::all();
//        $countTime = $branch->schedules
        if ($branch_id != null) {
            return view('Admin.branch.dates.index', compact('rows', 'branch'));
        } else {
            return view('branch.dates.index', compact('rows', 'branch'));
        }

    }

    //show
    public function show($id, $branch_id = null)
    {

        if ($branch_id) {
            $branch = Branch::find($branch_id);
        } else {
            $branch_id_branch = Auth::guard('branch')->user()->branch_id;
            $branch = Branch::find($branch_id_branch);
        }
        $day = Day::find($id);
        $schedules = $branch->schedules()->where('scheduleable_id', $branch->id)->where('scheduleable_type', 'App\Models\Laundry\branch')->where('day_id', $id)->get();

        if ($branch_id) {
            return view('Admin.branch.dates.show', compact('schedules', 'branch', 'day'));
        } else {
            return view('branch.dates.show', compact('schedules', 'branch', 'day'));
        }

    }

    //store
    public function store(Request $request, $branch_id = null)
    {

        if ($branch_id) {
            $branch = Branch::find($branch_id);
        } else {
            $branch_id_branch = Auth::guard('branch')->user()->branch_id;
            $branch = Branch::find($branch_id_branch);
        }
        $validator = Validator::make($request->all(), [
            'day_id' => 'required|exists:days,id',
            'start_time' => 'required',
            'end_time' => 'required|after:start_time',
            'is_active' => 'required|in:0,1',
        ]);


        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $start_time_24 = $request->start_time;
        $end_time_24 = $request->end_time;
        $existingSchedule = $branch->schedules()
            ->where('day_id', $request->day_id)
            ->where(function ($query) use ($start_time_24, $end_time_24) {
                $query->whereBetween('start_time', [$start_time_24, $end_time_24])
                    ->orWhereBetween('end_time', [$start_time_24, $end_time_24])
                    ->orWhere(function ($query) use ($start_time_24, $end_time_24) {
                        $query->where('start_time', '<=', $start_time_24)
                            ->where('end_time', '>=', $end_time_24);
                    });
            })
            ->exists();


        if ($existingSchedule == true) {
            Alert::error('error', 'This time slot is already taken for this day.');
            return Redirect::back();
        }

        $branch->schedules()->create([
            'day_id' => $request->day_id,
            'start_time' => $start_time_24,
            'end_time' => $end_time_24,
            'is_active' => $request->is_active,
        ]);

        Alert::success('Success', 'Schedule added successfully');
        return Redirect::back();
    }

    //edit
    public function edit($id, $branch_id = null)
    {
        if ($branch_id) {
            $branch = Branch::find($branch_id);
        } else {
            $branch_id_branch = Auth::guard('branch')->user()->branch_id;
            $branch = Branch::find($branch_id_branch);
        }
        $row = $branch->schedules()->where('id', $id)->first();
        if ($branch_id) {
            return view('Admin.branch.dates.edit', compact('row', 'branch'));
        } else {
            return view('branch.dates.edit', compact('row', 'branch'));
        }

    }

    //update
    public function update(Request $request, $id, $branch_id = null)
    {
        if ($branch_id) {
            $branch = Branch::find($branch_id);
        } else {
            $branch_id_branch = Auth::guard('branch')->user()->branch_id;
            $branch = Branch::find($branch_id_branch);
        }
        $validator = Validator::make($request->all(), [
            'day_id' => 'required|exists:days,id',
            'start_time' => 'required',
            'end_time' => 'required|after:start_time',
            'is_active' => 'required|in:0,1',
        ]);

        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }

        $start_time_24 = $request->start_time;
        $end_time_24 = $request->end_time;
        $existingSchedule = $branch->schedules()
            ->where('day_id', $request->day_id)
            ->where(function ($query) use ($start_time_24, $end_time_24) {
                $query->whereBetween('start_time', [$start_time_24, $end_time_24])
                    ->orWhereBetween('end_time', [$start_time_24, $end_time_24])
                    ->orWhere(function ($query) use ($start_time_24, $end_time_24) {
                        $query->where('start_time', '<=', $start_time_24)
                            ->where('end_time', '>=', $end_time_24);
                    });
            })
            ->where('id', '!=', $id)
            ->exists();

        if ($existingSchedule) {
            Alert::error('error', 'This time slot is already taken for this day.');
            return Redirect::back();
        }

       $schedules = $branch->schedules()->where('id', $id)->update([
            'start_time' => $start_time_24,
            'end_time' => $end_time_24,
            'is_active' => $request->is_active,
        ]);


        Alert::success('Success', 'Schedule updated successfully');

        if ($branch_id) {
            return Redirect::route('branch.dates.show', [ $request->day_id, $branch_id]);
        } else {
            return Redirect::route('branch.dates.show', [ $request->day_id]);
        }

    }

    //delete
    public function delete($id, $branch_id = null)
    {
        if ($branch_id) {
            $branch = Branch::find($branch_id);
        } else {
            $branch_id_branch = Auth::guard('branch')->user()->branch_id;
            $branch = Branch::find($branch_id_branch);
        }
        $branch->schedules()->where('id', $id)->delete();
        Alert::success('Success', 'Schedule deleted successfully');
        return Redirect::back();
    }
}
