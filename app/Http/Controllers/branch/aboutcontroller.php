<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\response;
use App;
class aboutcontroller extends Controller
{
    //
    use response;
    public function index(Request $request){
        $lang=$request->header('lang');
        App::setLocale($lang);
        $about=DB::table('abouttranslations')->select('content')->where('locale',$lang)->first();
        return $this->response(true,'get data succeesfult',$about);
    }
}
