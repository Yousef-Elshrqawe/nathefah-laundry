<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use App\Interfaces\SystemRepositoryInterface;
use App\Models\Wallet\user_wallet;
use App\Repositories\WalletRepository;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Laundry\branchservice;
use App\Models\laundryservice\Service;
use App\Models\laundryservice\branchAdditionalservice;
use App\Models\Laundry\Branchitem;
use App\Models\Laundry\BranchitemTranslation;
use App\Models\laundryservice\Additionalservice;
use App\Models\laundryservice\Argent;
use Illuminate\Database\Eloquent\Builder;
use App\Models\Order\delivery_type;
use App\Models\Order\payment_method;
use App\Models\Driver\Driver;
use Illuminate\Support\Facades\DB;
use App\Models\Order\order;
use App\Models\Order\orderdetailes;
use App\Models\laundryservice\Serviceitemprice;
use App\Interfaces\OrderRepositoryInterface;
use App\Interfaces\BranchRepositoryInterface;
use App\Models\Order\OrderDriveryStatus;
use App\Traits\queries\serviceTrait;
use App\Traits\response;
use App\Traits\queries\orders;
use App\Models\Laundry\{branch, Laundry};
use App\Models\Order\Rejectreason;
use App\Models\Logs\Make_order;
use App\Models\Logs\Accept_order;
use App\Models\Logs\Reject_order;
use App\Interfaces\NotificationRepositoryinterface;
use App\Http\Resources\driverresource;
use App\Models\User;
use Illuminate\Support\Facades\Http;
use Validator;
use Auth;
use App;


class OrderController extends Controller
{
    use serviceTrait, orders, response, App\Traits\sms, App\Traits\otp;

    /*   private $access_code = 'ECLmBCXFjMVcehgasQ8d';
       private $merchant_identifier = '00f1ca9c';
       private $sha_request_phrase = '46PXPeVmlpRrScEAVgVV9U)]';
       private $sha_type = 'sha256';
       private $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';*/


    private $access_code = 'U1xDaJUuiyE8hA2OAE70';
    private $merchant_identifier = 'EIIWLjkk';
    private $sha_request_phrase = '15KHEd88OBA6MoXh4BtVD9$&';
    private $sha_type = 'sha256';
    private $url = 'https://paymentservices.payfort.com/FortAPI/paymentApi';
    private $urlStc = 'https://checkout.payfort.com/FortAPI/paymentPage';


    private OrderRepositoryInterface $OrderRepository;
    private SystemRepositoryInterface $SystemRepository;

    public function __construct(
        OrderRepositoryInterface        $OrderRepository
        , BranchRepositoryInterface     $BranchRepository,
        NotificationRepositoryinterface $NotificationRepository,
        WalletRepository                $WalletRepository,
        SystemRepositoryInterface       $SystemRepository
    )
    {
        $this->OrderRepository = $OrderRepository;
        $this->BranchRepository = $BranchRepository;
        $this->NotificationRepository = $NotificationRepository;
        $this->WalletRepository = $WalletRepository;
        $this->SystemRepository = $SystemRepository;
    }

    #Reigon[this is create ordder cycle]
    public $service_ids = [];

    public function getservice(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        //$services= $this->OrderRepository->selectlaundry($branch_id,$lang);
        // get services of branch
        $services = $this->OrderRepository->laundryservices($branch_id, $lang);
        $services_id = [];
        foreach ($services as $key => $value) {
            $services_id[$key] = $value->id;
        }

        $services = Service::select('id')->with('categories')->wherein('id', $services_id)->get();

        $branch = branch::select('argent')->find($branch_id);

        $data['services'] = $services;
        $data['branchargent'] = $branch->argent;
        return $this->response(true, 'get services succefully', $data);
    }

    public function itemdetailes(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $item_id = $request->item_id;
        $itemadditionalservice = Additionalservice::select('id')->whereHas('branchadditionalservice', function (Builder $query) use ($item_id) {
            $query->where('branchitem_id', $item_id)->where('status', 'on');
        })->with(['itemprices' => function ($q) use ($item_id) {
            $q->select('additionalservice_id', 'price', 'id as item_price_id')->where('branchitem_id', $item_id)->get();
        }])->get();
        $data['status'] = true;
        $data['message'] = "return avavilable additional service of this item";
        $data['data']['additionalservices'] = $itemadditionalservice;
        return response()->json($data);
    }

    public function submitorder(Request $request)
    {
        try {
            $branchid = Auth::guard('branchuser-api')->user()->branch_id;
            $branch_user_id = Auth::guard('branchuser-api')->user()->id;
            $branch = branch::find($branchid);
            DB::transaction(function () use (&$order, $request, $branchid, $branch_user_id) {
                $urgent = false;
                foreach ($request->serviceprices as $serviceprice) {
                    if ($serviceprice['argent'] != 0) {
                        $urgent = true;
                        break;
                    }
                }
                $order = order::create([
                    'branch_id' => $branchid,
                    'customer_name' => $request->customer_name,
                    'country_code' => $request->country_code,
                    'customer_phone' => $request->customer_phone,
                    'customer_location' => $request->customer_location,
                    'order_by' => 'laundry',
                    'lat' => $request->lat,
                    'long' => $request->long,
                    'confirmation' => 'accepted',
                    'urgent' => $urgent
                ]);

                $user = User::where('phone', $order->customer_phone)->first();
                if ($user != null)
                    $order->update(['user_id' => $user->id]);
                // else{
                //     $user = User::create(['phone' => $order->customer_phone, ]);
                //     $order->update(['user_id' => $user->id]);
                // }

                $price_before_discount = 0;
                foreach ($request->serviceprices as $serviceprice) {
                    $price = $serviceprice['quantity'] * $serviceprice['price'];
                    orderdetailes::create([
                        'order_id' => $order->id,
                        'branchitem_id' => $serviceprice['branchitem_id'],
                        'price' => $price,
                        'service_id' => $serviceprice['service_id'],
                        'quantity' => $serviceprice['quantity'],
                        'note' => $serviceprice['note']

                    ]);
                    $price_before_discount += $price;
                    // this condation to add agent
                    if ($serviceprice['argent'] != 0) {
                        $argentprice = Branchitem::select('argentprice')->where('id', $serviceprice['branchitem_id'])->first();
                        $argentprice = $serviceprice['argent'] * $argentprice->argentprice;
                        Argent::create([
                            'order_id' => $order->id,
                            'price' => $argentprice,
                            'quantity' => $serviceprice['argent'],
                            'service_id' => $serviceprice['service_id'],
                            'branchitem_id' => $serviceprice['branchitem_id'],
                        ]);
                        $price_before_discount += $argentprice;
                    }
                }
                foreach ($request->additionalservices as $additionalservice) {
                    $price = $additionalservice['quantity'] * $additionalservice['price'];
                    orderdetailes::create([
                        'order_id' => $order->id,
                        'branchitem_id' => $additionalservice['branchitem_id'],
                        'service_id' => $additionalservice['service_id'],
                        'price' => $price,
                        'additionalservice_id' => $additionalservice['additionalservice_id'],
                        'quantity' => $additionalservice['quantity']
                    ]);
                    $price_before_discount += $price;
                }
                // calculate taxes
                $this->OrderRepository->addordertaxes($order, $branchid, $price_before_discount);


                // logs of order to know how make this order
                Make_order::create([
                    'order_id' => $order->id,
                    'branch_id' => $branchid,
                    'branch_user_id' => $branch_user_id
                ]);
            });
        } catch (Throwable $e) {
            report($e);
            return false;
        }
        $data['status'] = true;
        $data['message'] = 'order added succefully';
        $data['data']['order'] = $order->id;
        $data['data']['distance'] = $this->BranchRepository->distance($request->lat, $request->long, $branch->lat, $branch->long, 'k');
        return response()->json($data);
    }

    public function cancelorder(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order_id = $request->order_id;
        $branchid = Auth::guard('branchuser-api')->user()->branch_id;
        $order = order::where('id', $order_id)->where('branch_id', $branchid)->first();
        $user = User::where('id', $order->user_id)->first();
        if ($order == null) {
            $data['status'] = false;
            $data['message'] = 'order not found';
            return response()->json($data, 405);
        } else {

            $amount = $order->price_after_discount != null ? $order->price_after_discount : $order->price_before_discount;


            $user->update([
                'points' => $user->points - $amount
            ]);

            //لو دافع  ب  المحفظة هيتم ارجاع المبلغ للمحفظة
            if ($order->pymenttype_id == 1) {
                $wallet = user_wallet::where('user_id', $order->user_id)->first();
                $wallet->update([
                    'amount' => $wallet->amount + $amount
                ]);
            }

            if ($order->pymenttype_id == 3) {
                $shaString = '';
                $arrData = array(
                    'command' => 'REFUND',
                    'access_code' => $this->access_code,
                    'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                    'amount' => $order->price_before_discount * 100,
                    'currency' => 'SAR',
                    'language' => 'en',
                    'fort_id' => $order->fort_id,
                    'order_description' => 'order cancelation',
                );

                ksort($arrData);
                foreach ($arrData as $key => $value) {
                    $shaString .= "$key=$value";
                }
                $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
                $signature = hash($this->sha_type, $shaString);

                $response = Http::post($this->url, [
                    'command' => 'REFUND',
                    'access_code' => $this->access_code,
                    'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                    'amount' => $order->price_before_discount * 100,
                    'currency' => 'SAR',
                    'language' => 'en',
                    'fort_id' => $order->fort_id,
                    "signature" => $signature,
                    'order_description' => 'order cancelation',
                ]);
                $response = $response->json()['response_message'];
                if ($response == 'Success') {
                    $order->orderdetailes()->delete();
                    //urgents
                    $urgents = Argent::where('order_id', $order->id)->get();
                    foreach ($urgents as $urgent) {
                        $urgent->delete();
                    }
                    //deliverytype
                    $order->OrderDriveryStatus()->delete();
                    $order->delete();
                    $user = User::find($order->user_id);
                    $this->sendMessage($user->country_code . $user->phone, 'We apologize! Your order no. ' . $order->id . 'has been cancelled by laundry' . $order->branch->username);
                    return $this->response(true, $lang == 'ar' ? 'تم الغاء الطلب بنجاح' : 'order cancel succefully');
                } else {
                    return $this->response(false, $lang == 'ar' ? 'حدث خطأ ما' : 'some thing is wrong');
                }
            }


            $user = User::find($order->user_id);
            $title = ' الغاء الطلب';
            $body = 'نعتذر ! تم إلغاء طلبكم رقم ' . $order->id . ' من  المغسله ' . $order->branch->username;
            if ($user->lang == 'en') {
                $title = 'Order cancelation';
                $body = 'We apologize! Your order no. ' . $order->id . 'has been cancelled by laundry' . $order->branch->username;
            }

            $country_code = $user->country_code;
            $phone = $user->phone;
            $phonenumber = $country_code . $phone;

            \App\Jobs\SendSmsJob::dispatch($phonenumber, $body);

            $order->orderdetailes()->delete();
            //urgents
            $urgents = Argent::where('order_id', $order->id)->get();
            foreach ($urgents as $urgent) {
                $urgent->delete();
            }
            //deliverytype
            $order->OrderDriveryStatus()->delete();
            $order->delete();
            $data['status'] = true;
            $data['message'] = 'order cancel succefully';
            return response()->json($data);
        }
    }

    public function orderinfo(Request $request)
    {
        $lang = $request->header('lang');
        $order_id = $request->order_id;
        App::setLocale($lang);
        $deliverytype = delivery_type::select('id')->get()->makehidden('translations');
        $paymentmethods = payment_method::select('id')->get()->makehidden('translations');

        $orderdetailes = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'order_detailes.service_id')
            ->selectRaw('sum(price) as total')
            ->selectRaw('sum(quantity) as quantity')
            ->selectRaw('servicetranslations.name')
            ->where('order_detailes.order_id', $order_id)
            ->where('servicetranslations.locale', $lang)
            ->where('order_detailes.additionalservice_id', '=', null)
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();
        $totalorderdetailesprice = 0;
        foreach ($orderdetailes as $orderdetaile) {
            $totalorderdetailesprice += $orderdetaile->total;
        }
        $additionalservice = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('additionalservicetranslations', 'additionalservicetranslations.additionalservice_id', '=', 'order_detailes.additionalservice_id')
            ->where('additionalservicetranslations.locale', $lang)
            ->selectRaw('additionalservicetranslations.name')
            ->selectRaw('sum(price) as price')
            ->selectRaw('quantity')
            ->where('order_detailes.additionalservice_id', '!=', null)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_detailes.quantity')
            ->get();
        $totaladditionalprice = 0;
        foreach ($additionalservice as $addtional) {
            $totaladditionalprice += $addtional->price;
        }
        $argentprice = Argent::where('order_id', $order_id)->sum('price');
        $order = Order::find($order_id);
        $data['status'] = true;
        $data['message'] = "return order info succeffuly";
        $data['data']['deliverytype'] = $deliverytype;
        $data['data']['paymentmethods'] = $paymentmethods;
        $data['data']['orderdetailes'] = $orderdetailes;
        $data['data']['argentprice'] = $argentprice;
        $data['data']['additionalservices'] = $additionalservice;
        $data['data']['totaladditionalprice'] = $totaladditionalprice;
        $data['data']['totalserviceprice'] = $totalorderdetailesprice;
        $data['data']['delivery_fees'] = $order->delivery_fees;
        $data['data']['taxes'] = $order->taxes;
        // $data['price_after_discount']=$order->price_after_discount;
        // $data['price_before_discount']=$order->price_before_discount;
        return response()->json($data);
    }

    public function getopentime(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $opentime = $this->BranchRepository->getopentime($branch_id);
        if ($opentime == false)
            return $this->response(false, 'some thing is wrong', $data = null, 403);
        return $this->response(true, 'opening hours get successfuly', $opentime);
    }

    public function checkorder(Request $request)
    {
        $order_id = $request->order_id;
        $order_delivery_status = true;
        $branchid = Auth::guard('branchuser-api')->user()->branch_id;
        $order = order::where('id', $order_id)->where('branch_id', $branchid)->first();
        if ($order == null) {
            $data['status'] = false;
            $data['message'] = 'order not found';
            return response()->json($data, 405);
        } else {
            if ($order->checked == true) {
                return $this->response(false, 'this order alerdy checked', $data = null, 403);
            }
            // if order found
            DB::beginTransaction();
            // delvivery consisit of threemain type(self delivery - one way delivery - by delivery)
            if ($request->delivery_type == 'bydelivery') {
                $delivery_type_id = 1;
                $order_status = 'pick_up_home';
            } elseif ($request->delivery_type == 'on_way_delivery') {
                //start validate way of delivery

                $validator = Validator::make($request->all(), [
                    'way_delivery' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                        'message' => $validator->messages()->first()
                    ], 403);
                }

                //  end validate way of delivery
                if ($request->way_delivery == 'home_drop_of') {
                    $delivery_type_id = 3;
                    $order_delivery_status = false;
                    $order_status = 'pick_up_laundry';
                } elseif ($request->way_delivery == 'self_drop_of') {
                    $delivery_type_id = 4;
                    $order_status = 'pick_up_home';
                } else {
                    return response()->json(['message' => 'the way delivery input is false'], 403);
                }
            } elseif ($request->delivery_type == 'self_delivery') {
                $order_delivery_status = false;
                $delivery_type_id = 2;
                $order_status = null;
                $user = User::where('phone', $order->customer_phone)->first();
                if ($user != null)
                    $order->update(['user_id' => $user->id]);

                OrderDriveryStatus::create([
                    'order_id' => $order->id,
                    'driver_id' => null,
                    'order_status' => 'drop_of_laundry',
                    'code' => rand(999, 9999),
                ]);

            }
            // we will remove this from here
            $order->update([
                'day' => $request->day,
                'from' => date("H:i:s", strtotime($request->from)),
                'to' => date("H:i:s", strtotime($request->to )),
                'delivery_type_id' => $delivery_type_id,
                'order_delivery_status' => $order_delivery_status,
                'checked' => true
            ]);
            if ($order->delivery_type_id == 1 || $order->delivery_type_id == 3 || $order->delivery_type_id == 4) {
                OrderDriveryStatus::create([
                    'order_id' => $order->id,
                    'driver_id' => null,
                    'order_status' => $order_status,
                    'code' => rand(999, 9999),
                ]);
            }
            DB::commit();
            $data['status'] = true;
            $data['message'] = 'order checled  succefully';
            return response()->json($data);
        }
    }
    #EndReigon
    ###################################################################################################################
    ###################################################################################################################
    ###################################################################################################################
    ###################################################################################################################
    #Reigon[this is show order cycle]
    public function inprogressorder(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $orders = DB::table('orders')
            ->select('orders.id', 'orders.customer_name', 'delivery_type_id')
            ->where('orders.progress', 'inprogress')
            ->where('checked', true)
            ->where('confirmation', 'accepted')
            ->where('branch_id', $branch_id)
            ->orderby('created_at', 'desc')
            ->get();
        // get service and put it under order
        $orders = $this->orderwithservice($orders, $lang);
        $data['status'] = true;
        $data['message'] = "get inprogress order successfully";
        $data['data']['orders'] = $orders;
        return response()->json($data);
    }

    public $orders_id = [];

    public function completedorder(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $orders = DB::table('orders')
            ->select('orders.id', 'orders.customer_name')
            ->whereIn('orders.progress', ['completed', 'finished'])
            ->where('checked', true)
            ->where('branch_id', $branch_id)
            ->paginate(10);
        // get service and put it under order
        $orders = $this->orderwithservice($orders, $lang);
        $data['status'] = true;
        $data['message'] = 'get completed order successfully';
        $data['data']['orders'] = $orders;
        return response()->json($orders);
    }

    public $drivers_id = [];

    public function indeliveryorder(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;

        // get driver that have order
        // $drivers=DB::table('drivers')
        // ->select('drivers.id as driver_id','drivers.name','orders.id as order_id')
        // ->where('branch_id',$branch_id)
        // ->join('orders','drivers.id','=','orders.driver_id')
        // ->where('delivery_status','inprogress')
        // ->wherein('orders.id',function($q){
        //     $q->from('orders')->groupBy('orders.driver_id')->selectRaw('MAX(orders.id)')
        //     ->distinct()->get();
        // })->get();
        $drivers = DB::table('drivers')
            ->select('drivers.id as driver_id', 'drivers.name', 'drivers.img', 'drivers.phone', 'drivers.country_code', 'drivers.rate')
            ->join('orders', 'drivers.id', '=', 'orders.driver_id')
            ->where('orders.delivery_status', 'inprogress')
            //->where('orders.delivery','indelivery')
            ->distinct()
            ->where('orders.branch_id', $branch_id)
            ->get();
        // get orders of drivers
        foreach ($drivers as $driver) {
            array_push($this->drivers_id, $driver->driver_id);
        }
        $orders = [];
        foreach ($drivers as $driver) {
            $order = DB::table('orders')->where('orders.driver_id', $driver->driver_id)
                ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
                ->select('orders.id as order_id', 'orders.driver_id', 'order_delivery_status.order_status as order_status')
                ->where('orders.delivery_status', 'inprogress')
                ->where('checked', true)
                ->where('order_delivery_status.confirmation', false)
                ->where('orders.branch_id', $branch_id)
                ->latest('orders.id')
                ->first();
            if ($order != null) {
                array_push($orders, $order);
            }
        }
        //return response()->json($orders);
        $orderscount = Order::select('driver_id', 'id as order_id')->where('branch_id', $branch_id)->where('delivery_status', 'inprogress')
            ->groupBy('orders.driver_id')
            ->groupBy('orders.id')
            ->get();
        //get orders id
        foreach ($orders as $order) {
            array_push($this->orders_id, $order->order_id);
        }
        $services = DB::table('order_detailes')
            ->select('servicetranslations.name', 'order_id', 'order_detailes.service_id')
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'order_detailes.service_id')
            ->selectRaw('sum(quantity) as quantity')
            ->wherein('order_detailes.order_id', $this->orders_id)
            ->where('order_detailes.additionalservice_id', '=', null)
            ->where('servicetranslations.locale', $lang)
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.service_id')
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();
        $additionalservices = DB::table('order_detailes')
            ->select('order_id', 'order_detailes.service_id', 'order_detailes.additionalservice_id', 'additionalservicetranslations.name')
            ->join('additionalservicetranslations', 'additionalservicetranslations.additionalservice_id', '=', 'order_detailes.additionalservice_id')
            ->selectRaw('sum(quantity) as quantity')
            ->wherein('order_detailes.order_id', $this->orders_id)
            ->where('order_detailes.additionalservice_id', '!=', null)
            ->where('additionalservicetranslations.locale', $lang)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.service_id')
            ->groupBy('order_detailes.additionalservice_id')
            ->get();
        // but additional service inside service
        $argents = db::table('argent')->wherein('order_id', $this->orders_id)->get();
        foreach ($services as $service) {
            $service->additionalservice = [];
            foreach ($additionalservices as $key => $additionalservice) {
                if ($service->order_id == $additionalservice->order_id && $service->service_id == $additionalservice->service_id) {
                    array_push($service->additionalservice, $additionalservice);
                }
            }
            foreach ($argents as $argent) {
                $service->argent = 0;
                if ($service->service_id == $argent->service_id && $service->order_id == $argent->order_id) {
                    $service->argent = $argent->quantity;
                }
            }
        }
        foreach ($orders as $order) {
            $order->services = [];
            foreach ($services as $service) {
                if ($order->order_id == $service->order_id) {
                    array_push($order->services, $service);
                }
            }
        }
        foreach ($drivers as $driver) {
            $driver->ordercount = 0;
            $driver->distance = 5;
            foreach ($orderscount as $count) {
                if ($count->driver_id == $driver->driver_id) {
                    $driver->ordercount += 1;
                }
            }
        }
        foreach ($drivers as $driver) {
            $driver->orderstatus = '';
            $driver->order = [];
            foreach ($orders as $order) {
                if ($order->driver_id == $driver->driver_id) {
                    $driver->orderstatus = $order->order_status;
                    foreach ($services as $service) {
                        if ($order->order_id == $service->order_id) {
                            array_push($driver->order, $service);
                        }
                    }
                }
            }
            if ($driver->img == null) {
                $driver->img = asset("/uploads/driver/img/default.png");
            } else {
                $driver->img = asset("/uploads/driver/img/" . $driver->img);
            }

        }
        $data['status'] = true;
        $data['message'] = 'get completed order successfully';
        $data['data']['drivers'] = $drivers;
        return response()->json($data);
    }

    public function moreorder(Request $request)
    {
        $driver_id = $request->driver_id;
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $driver = Driver::select('id', 'name', 'img', 'phone', 'country_code', 'rate')->find($driver_id);
        $driver = new driverresource($driver);
        $orders = DB::table('orders')
            ->select('orders.id as order_id', 'orders.driver_id', 'order_delivery_status.order_status as order_status')
            ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
            ->where('orders.branch_id', $branch_id)
            ->where('drivers.id', $driver_id)
            ->where('order_delivery_status.confirmation', false)
            ->join('drivers', 'drivers.id', '=', 'orders.driver_id')
            ->where('delivery_status', 'inprogress')
            ->latest('orders.id')->distinct()
            ->get();
        // get orders of drivers
        $orderscount = Order::select('driver_id', 'id as order_id')
            ->where('driver_id', $driver_id)
            ->where('branch_id', $branch_id)->where('delivery_status', 'inprogress')
            ->groupBy('orders.driver_id')
            ->groupBy('orders.id')
            ->get();
        //get orders id
        foreach ($orders as $order) {
            array_push($this->orders_id, $order->order_id);
        }
        $services = DB::table('order_detailes')
            ->select('servicetranslations.name', 'order_id', 'order_detailes.service_id')
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'order_detailes.service_id')
            ->selectRaw('sum(quantity) as quantity')
            ->wherein('order_detailes.order_id', $this->orders_id)
            ->where('order_detailes.additionalservice_id', '=', null)
            ->where('servicetranslations.locale', $lang)
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.service_id')
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();

        $additionalservices = DB::table('order_detailes')
            ->select('order_id', 'order_detailes.service_id', 'order_detailes.additionalservice_id', 'additionalservicetranslations.name')
            ->join('additionalservicetranslations', 'additionalservicetranslations.additionalservice_id', '=', 'order_detailes.additionalservice_id')
            ->selectRaw('sum(quantity) as quantity')
            ->wherein('order_detailes.order_id', $this->orders_id)
            ->where('order_detailes.additionalservice_id', '!=', null)
            ->where('additionalservicetranslations.locale', $lang)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.service_id')
            ->groupBy('order_detailes.additionalservice_id')
            ->get();

        $argents = db::table('argent')->wherein('order_id', $this->orders_id)->get();
        foreach ($services as $service) {
            foreach ($argents as $argent) {
                $service->argent = 0;
                if ($service->service_id == $argent->service_id && $service->order_id == $argent->order_id) {
                    $service->argent = $argent->quantity;
                }
            }
        }
        // but additional service inside service
        foreach ($services as $service) {
            $service->additionalservice = [];
            foreach ($additionalservices as $additionalservice) {
                if ($service->order_id == $additionalservice->order_id && $service->service_id == $additionalservice->service_id) {
                    array_push($service->additionalservice, $additionalservice);
                }
            }
        }
        // but services inside drivers
        $driver->orderscount = 0;
        foreach ($orderscount as $ordercount) {
            if ($driver->id == $ordercount->driver_id) {
                $driver->orderscount += 1;
            }
        }
        foreach ($orders as $order) {
            $order->services = [];
            foreach ($services as $service) {
                if ($service->order_id == $order->order_id) {
                    array_push($order->services, $service);
                }
            }
        }
        $data['status'] = true;
        $data['message'] = 'get completed order successfully';
        $data['data']['driver'] = $driver;
        $data['data']['orders'] = $orders;
        return response()->json($data);
    }

    public function ordersummary(Request $request)
    {
        $order_id = $request->order_id;
//        $driver_id = DB::table('orders')->select('orders.driver_id')->where('orders.id', $order_id)->first()->driver_id;
        $driver_id = Order::select('driver_id')->where('id', $order_id)->first()->driver_id;
        if ($driver_id == null)
            $driver_id = null;
        $driver = Driver::select('name', 'img', 'classification', 'id', 'country_code', 'phone', 'rate')->find($driver_id);
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order = DB::table('orders')
            ->select('orders.*')
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
            ->where('order_delivery_status.driver_id', $driver_id)->latest('order_delivery_status.id')
            //->where('order_delivery_status.confirmation',false)
            ->where('orders.id', $order_id)
            ->where('order_detailes.order_id', $order_id)
            ->selectRaw('orders.created_at')
            ->selectRaw('orders.updated_at')
            ->selectRaw('order_delivery_status.order_status')
            //->selectRaw('sum(order_detailes.price) as price')
            ->groupBy('orders.id')
            ->groupBy('orders.delivery_fees')
            ->groupBy('orders.customer_location')
            ->groupBy('orders.created_at')
            ->groupBy('orders.delivery_status')
            ->groupBy('orders.customer_name')
            ->groupBy('orders.taxes')
            ->groupBy('orders.discounttype')
            ->groupBy('orders.price_before_discount')
            ->groupBy('orders.price_after_discount')
            ->groupBy('order_delivery_status.order_id')
            ->groupBy('order_delivery_status.id')
            ->groupBy('order_delivery_status.order_status')
            ->orderBy('order_delivery_status.created_at', 'desc')
            ->first();


        if ($order == null)
            return $this->response(false, 'you have no access for this order', null, 401);


        $order->created_at = date('Y-m-d', strtotime($order->created_at));
        /*$order->time = date('h:m a',  $order->created_at);*/
        //خرجلي الوقت فقط من $order->created_at
        $order->time = Carbon::parse( $order->updated_at)->format('h:i A');
        // $orderargentprice=DB::table('order_detailes')->where('order_detailes.order_id',$order_id)
        // ->join('orders','orders.id','=','order_detailes.order_id')
        // ->leftjoin('argent','orders.id','=','argent.order_id')
        // ->selectRaw('sum(argent.price) as argentprice')
        // ->groupBy('argent.id')
        // ->first();
        $order->price = $this->OrderRepository->orderprice($order);
        // this query get services with count of item in it
        $services = $this->serive($order_id, $driver_id, $lang);
        // this query get items with count of item in it
        $items = $this->items($order_id, $driver_id, $lang);

        // this query to get additional service
        $additionals = $this->additionals($order_id, $driver_id, $lang);
        $argents = db::table('argent')->where('order_id', $order_id)->get();
        // but additional service in the item
        foreach ($items as $key => $item) {
            $item->additonalservice = [];
            foreach ($additionals as $additional) {
                if ($item->item_id == $additional->item_id && $item->service_id == $additional->service_id) {
                    array_push($item->additonalservice, $additional);
                }
            }
            //but argent inside item
            $item->argent = 0;
            foreach ($argents as $argent) {
                if ($item->item_id == $argent->branchitem_id) {
                    //$item->argent=true;
                    $item->argent = $argent->quantity;
                }
            }
        }
        //but argent inside item
        foreach ($services as $key => $service) {
            $service->item = [];
            foreach ($items as $item) {
                if ($item->service_id == $service->service_id) {
                    array_push($service->item, $item);
                }
            }
        }


        // checnge driver
        $changedriver = false;
        if ($order->delivery_status == 'no_progress' && $order->driver_id != null)
            $changedriver = true;

        $order->changedriver = $changedriver;

        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data']['driver'] = $driver;
        $data['data']['order'] = $order;
        $data['data']['serives'] = $services;
        return response()->json($data);

    }

    public function serachorder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'search_type' => 'required',
            'key' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json(['message' => $validator->messages()->first()], 403);
        }

        //dd($request->all());
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $type = gettype($request->key);
        //dd($type);
        // order is [completed  or inprogress]
        $search_type = $request->search_type;
        ($type == 'integer') ? $serach_key = 'id' : $serach_key = 'customer_name';
        if ($type == 'integer') {
            $orders = DB::table('orders')
                ->select('orders.id', 'orders.customer_name')
                ->where('orders.progress', $search_type)
                ->where('branch_id', $branch_id)
                ->where('id', $request->key)
                ->orwhere('customer_phone', 'like', "%{$request->key}%")
                ->latest()
                ->get();
        }
        if ($type == 'string') {

            $orders = DB::table('orders')
                ->select('orders.id', 'orders.customer_name')
                ->where('orders.progress', $search_type)
                ->where('branch_id', $branch_id)
                ->where('customer_name', 'like', "%{$request->key}%")
                ->orwhere('customer_phone', 'like', "%{$request->key}%")
                ->latest()
                ->get();
        }

        $orders = $this->orderwithservice($orders, $lang);
        if ($orders->isEmpty()) {
            return $this->response(false, 'this order not found');
        }
        $data['orders'] = $orders;
        return $this->response(true, 'return orders success', $data);
    }

    public function unasignedorder(Request $request)
    {
        $orders = $this->OrderRepository->unasignedorder($request);
        return $orders;
    }

    // pick up order from delivery man
    public function reciveorderinfo(Request $request)
    {
        $code = null;
        if (isset($request->code) != null)
            $code = $request->code;
        $order_id = $request->order_id;
        $lang = $request->header('lang');
        $orders = $this->OrderRepository->orderinfo($order_id, $lang, $code);
        return $orders;
    }
    #EndReigon
    #Reigon[this is confirm order cycle]
    public function getcode(Request $request)
    {
        $order_id = $request->order_id;
        $status = 'pick_up_laundry';
        // $order=order::find($order_id);
        // if($order->delivery_type_id==4){
        //   $status='drop_of_laundry';
        // }
        $code = $this->OrderRepository->getcode($status, $order_id);
        if ($code == false)
            return $this->response(false, 'you have no accsess for this order');
        $data['order_code'] = $code;
        return $this->response(true, 'get order code succsessfuly', $data);
    }

    //recive order from delivery pick up
    public function reciveorder(Request $request)
    {
        $data = $this->OrderRepository->reciveorder($request);
        //   if ($data==null){
        //     return $this->response(false,'some thing wrong');
        //   }
        return $this->response($data['status'], $data['message']);
    }

    // new order
    public function getneworder(Request $request)
    {
        $orders = $this->OrderRepository->neworders($request);
        $data['orders'] = $orders;
        return $this->response(true, 'get new orders successfuly', $data);
    }

    public function neworderinfo(Request $request)
    {
        $data = $this->OrderRepository->neworderinfo($request);
        if ($data == false)
            return response()->json(['status' => false, 'message' => 'this order not found'], 401);
        return $data;
    }

    // accept order from customerdriver accept order
    public function acceptorder(Request $request)
    {
        $branch_user = Auth::guard('branchuser-api')->user();
        $order = $this->OrderRepository->acceptorder($request->order_id, $branch_user->branch_id, $branch_user->id);
        if ($order == 'accepted')
            return $this->response(true, 'order already accepted');

        if ($order == 'refused')
            return $this->response(false, 'order already rejected successfully', null, 407);

        if ($order == false)
            return $this->response(false, 'order some thing is wrong', null, 407);


        return response()->json(['status' => true, 'message' => 'order accepted successfully']);
    }

    // reject order  use to atomatic reject after specify time in mobile application
    public function rejectorder(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $branch_user_id = Auth::guard('branchuser-api')->user()->id;

        // return $branch_user_id;
        // try{
        // DB::beginTransaction();
        $order = Order::find($request->order_id);
        $user = User::find($order->user_id);

        if ($order == null)
            return $this->response(false, $lang == 'ar' ? 'هذا الطلب غير موجود' : 'this order not found');

        if ($order->confirmation == 'accepted')
            return $this->response(true, $lang == 'ar' ? 'الطلب مقبول بالفعل' : 'order is already accepted');

        if ($order->confirmation == 'refused')
            return $this->response(true, $lang == 'ar' ? 'الطلب مرفوض بالفعل' : 'order is already rejected');


        $user = User::find($order->user_id);
        $title = 'تم رفض الطلب';
        $body = 'نعتذر ! تم رفض طلبكم رقم ' . $order->id . 'من المغسلة' . $order->branch->username;
        if (app()->getLocale() == 'en') {
            $title = 'your order is rejected';
            $body = 'We apologize! Your order no ' . $order->id . ' has been rejected by laundry ' . $order->branch->username;
        }
        $country_code = $user->country_code;
        $phone = $user->phone;
        $phoneNotification = $country_code . $phone;

        $this->sendMessage($user->country_code . $user->phone, $body);


        $amount = $order->price_after_discount != null ? $order->price_after_discount : $order->price_before_discount;

        if ($order->pymenttype_id == 1) {
            $wallet = user_wallet::where('user_id', $user->id)->first();
            $wallet->update([

                'amount' => $wallet->amount + $amount
            ]);
        }

        if ($order->pymenttype_id == 3) {
            $shaString = '';
            $arrData = array(
                'command' => 'REFUND',
                'access_code' => $this->access_code,
                'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                'amount' => $order->price_before_discount * 100,
                'currency' => 'SAR',
                'language' => 'en',
                'fort_id' => $order->fort_id,
                'order_description' => 'order cancelation',
            );

            ksort($arrData);
            foreach ($arrData as $key => $value) {
                $shaString .= "$key=$value";
            }
            $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
            $signature = hash($this->sha_type, $shaString);

            $response = Http::post($this->url, [
                'command' => 'REFUND',
                'access_code' => $this->access_code,
                'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                'amount' => $order->price_before_discount * 100,
                'currency' => 'SAR',
                'language' => 'en',
                'fort_id' => $order->fort_id,
                "signature" => $signature,
                'order_description' => 'order cancelation',
            ]);
            $response = $response->json()['response_message'];
            if ($response != 'Success') {
                return $this->response(false, $lang == 'ar' ? 'خطأ في عملية الدفع' : 'payment transaction error');
            }
        }

        $order->update([
            'confirmation' => 'refused'
        ]);


        $titles = ['ar' => 'تم رفض الطلب', 'en' => 'your order is rejected'];
        $bodys = ['ar' => 'نعتذر ! تم رفض طلبكم رقم ' . $order->id . 'من المغسلة' . $order->branch->username, 'en' => 'We apologize! Your order no ' . $order->id . ' has been rejected by laundry ' . $order->branch->username];
        $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '3')->where('status', '1')->first();
        if ($usernotifytype) {
            $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 1, ['order_id' => $request->order_id]);
            $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
        }
        $reject_order = Reject_order::create([
            'order_id' => $order->id,
            'branch_id' => $order->branch_id,
            'branch_user_id' => $branch_user_id
        ]);

        // DB::commit();
        return response()->json(['status' => true, 'message' => $lang == 'ar' ? 'تم رفض الطلب' : 'order has been rejected']);
        // } catch (\Exception $e) {
        // DB::rollback();
        return $this->response(false, __('response.wrong'), null, 407);
        // }
    }

    public function rejectreason(Request $request)
    {
        $branch_user_id = Auth::guard('branchuser-api')->user()->id;
        $order = Order::find($request->order_id);
        $user = User::find($order->user_id);
        if ($order == null)
            return response()->json(['status' => false, 'message' => 'this order not found'], 401);

        $validator = Validator::make($request->all(), [
            'order_id' => 'required',
            'desc' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        try {
            DB::beginTransaction();
            $order = Order::find($request->order_id);
            if ($order == null)
                return $this->response(false, 'this order not found');

            if ($order->confirmation == 'accepted')
                return $this->response(true, 'order is already accepted');

            if ($order->confirmation == 'refused')
                return $this->response(true, 'order is already rejected');


            $title = 'تم رفض طلبك ';
            $body = 'نعتذر ! تم رفض طلبكم رقم ' . $order->id . 'من المغسلة' . $order->branch->username;
            if (app()->getLocale() == 'en') {
                $title = 'your order is rejected';
                $body = 'We apologize! Your order no ' . $order->id . ' has been rejected by laundry ' . $order->branch->username;
            }

            $this->sendMessage($user->country_code . $user->phone, $body);

            if ($order->pymenttype_id == 3) {
                $shaString = '';
                $arrData = array(
                    'command' => 'REFUND',
                    'access_code' => $this->access_code,
                    'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                    'amount' => $order->price_before_discount * 100,
                    'currency' => 'SAR',
                    'language' => 'en',
                    'fort_id' => $order->fort_id,
                    'order_description' => 'order cancelation',
                );

                ksort($arrData);
                foreach ($arrData as $key => $value) {
                    $shaString .= "$key=$value";
                }
                $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
                $signature = hash($this->sha_type, $shaString);

                $response = Http::post($this->url, [
                    'command' => 'REFUND',
                    'access_code' => $this->access_code,
                    'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                    'amount' => $order->price_before_discount * 100,
                    'currency' => 'SAR',
                    'language' => 'en',
                    'fort_id' => $order->fort_id,
                    "signature" => $signature,
                    'order_description' => 'order cancelation',
                ]);
                $response = $response->json()['response_message'];
                if ($response != 'Success') {
                    return $this->response(false, 'payment transaction error');
                }
            }


            $order->update([
                'confirmation' => 'refused'
            ]);


            $user = User::find($order->user_id);
            $amount = $order->price_after_discount != null ? $order->price_after_discount : $order->price_before_discount;

            if ($order->pymenttype_id == 1) {
                $wallet = user_wallet::where('user_id', $user->id)->first();
                $wallet->update([

                    'amount' => $wallet->amount + $amount
                ]);
            }


            $titles = ['ar' => 'تم رفض طلبك ', 'en' => 'your order is rejected'];
            $bodys = ['ar' => 'نعتذر ! تم رفض طلبكم رقم ' . $order->id . 'من المغسلة' . $order->branch->username, 'en' => 'We apologize! Your order no ' . $order->id . ' has been rejected by laundry ' . $order->branch->username];
            $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '3')->where('status', '1')->first();
            if ($usernotifytype) {
                $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 1, ['order_id' => $request->order_id]);
                $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
            }
            $reject_order = Reject_order::create([
                'order_id' => $order->id,
                'branch_id' => $order->branch_id,
                'branch_user_id' => $branch_user_id
            ]);
            Rejectreason::create([
                'order_id' => $request->order_id,
                'desc' => $request->desc
            ]);
            DB::commit();
            return $this->response(true, 'reject reason added successfully');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->response(false, 'some thing wrong');
        }


    }

    public function orderdeliverystatus(Request $request)
    {
        $order_id = $request->order_id;
        $lang = $request->header('lang');
        $orderdeliveryinfo = $this->OrderRepository->orderdeliverystatus($order_id, $lang);
        //$orderdeliveryinfo=Arr::except($orderdeliveryinfo,['confirmation','delivery_status']);
        $data['order_delivery_status'] = $orderdeliveryinfo;
        return $this->response(true, 'get order delivey status successfuly', $orderdeliveryinfo);
    }

    // finish order cycle
    public function finishorder(Request $request)
    {
        DB::beginTransaction();
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $order = $this->OrderRepository->finishorder($request->order_id, $branch_id);
        DB::commit();
        if ($order == true) {
            return $this->response(true, 'order finished successfuly');
//            $this->OrderRepository->transformmonytopoints($request->order_id);
        }
        return $this->response(false, 'you have no access', null, 401);
    }
    #endReigon
    #Reigon[filter order]
    public function filterorder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'order_type' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $orderType = $request->order_type;
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $orders = Order::select('id', 'customer_name', 'delivery_type_id')
            ->when($request->urgent == true, function ($q) {
                return $q->where('urgent', 1);
            })
            ->when($request->top_rated == true, function ($q) {
                return $q->where('rate', 1);
            })
            ->when($request->self_delivery == true, function ($q) {
                return $q->where('delivery_type_id', 1);
            })
            ->when($request->bydelivery == true, function ($q) {
                return $q->wherein('delivery_type_id', [2, 3]);
            })
            ->when($request->sort_piked_date == true, function ($q) {
                return $q->orderby('picked_date', 'desc');
            })
            ->when($request->sort_drop_date == true, function ($q) {
                return $q->orderby('drop_date', 'desc');
            })
            ->when($request->alphabetic == true, function ($q) {
                return $q->orderby('customer_name', 'asc');
            })
            ->where('orders.progress', $orderType)
            ->where('checked', true)
            ->where('branch_id', $branch_id)
            ->paginate(20);
        // because self_delivery_type_is same column in this case user choose delivery order and self delivery so we will get all
        if ($request->has('bydelivery') && $request->self_delivery) {
            $orders = Order::select('id', 'customer_name')
                ->when($request->top_rated == true, function ($q) {
                    return $q->where('rate', 1);
                })
                ->when($request->urgent == true, function ($q) {
                    return $q->where('urgent', 1);
                })
                ->when($request->sort_piked_date == true, function ($q) {
                    return $q->orderby('picked_date', 'desc');
                })
                ->when($request->sort_drop_date == true, function ($q) {
                    return $q->orderby('drop_date', 'desc');
                })
                ->when($request->alphabetic == true, function ($q) {
                    return $q->orderby('customer_name', 'asc');
                })
                ->where('orders.progress', $orderType)
                ->where('checked', true)
                ->where('branch_id', $branch_id)
                ->paginate(20);
        }
        $orders = $this->orderwithservice($orders, $lang);
        return $orders;
        // return response()->json($orders);
    }
    #Endreigon
}
