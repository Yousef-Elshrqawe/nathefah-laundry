<?php

namespace App\Http\Controllers\branch;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Order\order;
use App\Traits\response;
use App\Traits\queries\serviceTrait;
use App\Traits\queries\orders;
use App\Interfaces\Indeliveryorderinterface;
use App\Repositories\IndeliveryorderRepository;
use Auth;
use App;

class indeliveryorderController extends Controller
{
    //
    public function __construct(Indeliveryorderinterface $Indeliveryorder)
    {
        $this->Indeliveryorder = $Indeliveryorder;
    }
    use response,serviceTrait,orders;
    public $orders_id=[];
    public function driverorder(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $drivers=DB::table('drivers')
        ->join('orders','drivers.id','orders.driver_id')
        ->join('order_delivery_status','order_delivery_status.order_id','=','orders.id')
        ->select('orders.driver_id','drivers.name','orders.id as order_id')
        ->where('delivery_status','inprogress')
        ->where('orders.branch_id',$branch_id)
        ->where('order_delivery_status.order_status','pick_up_laundry')
        ->where('order_delivery_status.confirmation',false)
        ->orderBy('orders.id','desc')
        ->get();

        foreach($drivers as $driver){
            $driver->ordercount=0;
            foreach($drivers as $iterationdriver){
               if($driver->driver_id==$iterationdriver->driver_id){
                 $driver->ordercount+=1;
               }
            }
        }
        foreach($drivers as $driver){
            array_push($this->orders_id,$driver->order_id);
        }
        $drivers=$this->Indeliveryorder->driverorder($this->orders_id,$lang,$drivers);
        $data['drivers']=$drivers;
        return $this->response(true,'get dirvers with orders success',$data);
    }
    public function customerorder(Request $request){
        // this get order that customer will send it to laundry
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        $orders=DB::table('orders')
        ->join('order_delivery_status','order_delivery_status.order_id','=','orders.id')
        ->select('orders.id','orders.customer_name')
        ->where('order_delivery_status.order_status','pick_up_laundry')
        ->where('order_delivery_status.confirmation',false)
        ->wherein('orders.delivery_type_id',[4,2])
        ->where('orders.progress','!=','completed')
        ->where('orders.branch_id',$branch_id)
        ->orderBy('orders.id','desc')
        ->get();
        $orders=$this->orderwithservice($orders,$lang);
        $data['orders']=$orders;
        return $this->response(true,'get balance success',$data);
    }
    public function forcustomersearch(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        if($request->name!=null){
            $orders=DB::table('orders')
            ->join('order_delivery_status','order_delivery_status.order_id','=','orders.id')
           // ->join('users','users.id','=','orders.user_id')
            ->select('orders.id','orders.customer_name')
            ->where('order_delivery_status.order_status','pick_up_laundry')
            ->where('orders.delivery_type_id',3)
            ->where('orders.customer_name','like',"%{$request->name}%")
            ->where('orders.branch_id',$branch_id)
            ->get();
            $orders=$this->orderwithservice($orders,$lang);
            $data['orders']=$orders;
            return $this->response(true,'get balance success',$data);
        }
        if($request->phone!=null){
            $orders=DB::table('orders')
            ->join('order_delivery_status','order_delivery_status.order_id','=','orders.id')
            ->join('users','users.id','=','orders.user_id')
            ->select('orders.id','orders.customer_name')
            ->where('order_delivery_status.order_status','pick_up_laundry')
            ->where('orders.delivery_type_id',3)
            ->where('users.phone','=',$request->phone)
            ->get();
            $orders=$this->orderwithservice($orders,$lang);
            $data['orders']=$orders;
            return $this->response(true,'get balance success',$data);
        }
    }
    public function searchdriverorder(Request $request){
        $branch_id=Auth::guard('branchuser-api')->user()->branch_id;
        $lang=$request->header('lang');
        App::setLocale($lang);
        if($request->name!=null){
            $drivers=DB::table('drivers')
            ->join('orders','drivers.id','orders.driver_id')
            ->select('orders.driver_id','drivers.name','orders.id as order_id')
            ->where('delivery_status','inprogress')
            ->where('orders.branch_id',$branch_id)
            ->where('orders.customer_name','like',"%{$request->name}%")
            ->get();
            foreach($drivers as $driver){
                //$driver->ordercount=0;
                $orderscount=DB::table('drivers')
                ->join('orders','drivers.id','orders.driver_id')
                ->selectRaw('count(orders.driver_id) as order_count')
                ->where('delivery_status','inprogress')
                ->where('orders.branch_id',$branch_id)
                ->where('orders.driver_id','=',$driver->driver_id)
                ->first();
                $driver->ordercount=$orderscount->order_count;
                array_push($this->orders_id,$driver->order_id);
            }
            $drivers=$this->Indeliveryorder->driverorder($this->orders_id,$lang,$drivers);
        }
        if($request->phone!=null){
            $drivers=DB::table('drivers')
            ->join('orders','drivers.id','orders.driver_id')
            ->join('users','users.id','=','orders.user_id')
            ->select('orders.driver_id','drivers.name','orders.id as order_id')
            ->where('delivery_status','inprogress')
            ->where('orders.branch_id',$branch_id)
            ->where('users.phone','=',$request->phone)
            ->get();
            foreach($drivers as $driver){
                //$driver->ordercount=0;
                $orderscount=DB::table('drivers')
                ->join('orders','drivers.id','orders.driver_id')
                ->selectRaw('count(orders.driver_id) as order_count')
                ->where('delivery_status','inprogress')
                ->where('orders.branch_id',$branch_id)
                ->where('orders.driver_id','=',$driver->driver_id)
                ->first();
                $driver->ordercount=$orderscount->order_count;
                array_push($this->orders_id,$driver->order_id);
            }
            $drivers=$this->Indeliveryorder->driverorder($this->orders_id,$lang,$drivers);
        }
        $data['drivers']=$drivers;
        return $this->response(true,'get dirvers with orders success',$data);
    }
}
