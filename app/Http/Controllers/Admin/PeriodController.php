<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PeriodRequest;
use App\Models\Subscription\Period;
use Illuminate\Http\Request;

use App\Http\Resources\PeriodResource;
use App\Interfaces\PeriodRepositoryInterface;
use App\Traits\GeneralTrait;
use Exception;
use RealRashid\SweetAlert\Facades\Alert;
use App\Traits\response;

class PeriodController extends Controller
{
    use GeneralTrait;
    use response;
    protected $modelRepository;

    public function __construct(PeriodRepositoryInterface $modelRepository)
    {
        $this->modelRepository = $modelRepository;
    }

    public function index()
    {
        $periods = $this->modelRepository->index();
        return view('Admin.periods.index',compact('periods'));
    }

    public function store(PeriodRequest $request)
    {

       try {
            $this->modelRepository->store($request);

            Alert::success('success', 'Period created successfully');

            return redirect()->back();

        } catch (Exception $e) {
            return redirect()->withErrors($e);
        }
    }

    public function update(PeriodRequest $request)
    {
        // try {
            $this->modelRepository->update($request);

            Alert::success('success', 'Period updated successfully');

            return redirect()->back();

        // } catch (Exception $e) {
        //     return redirect()->withErrors($e);
        // }
    }

    public function destroy($package_id)
    {
        try {
            $this->modelRepository->destroy($package_id);

            Alert::success('success', 'Period deleted successfully');

            return redirect()->back();


        } catch (Exception $e) {
            return redirect()->withErrors($e);
        }
    }

    public function getPeriods(Request $request)
    {
        $lang=$request->header('lang');
//        $periods = $this->modelRepository->getPeriods($lang);
        $periods =  Period::with('packages')->get();
        return $this->response(true , 'get periods success' ,  ['periods' => PeriodResource::collection($periods)] , 200);
       /*$data['periods']=$periods;
       return $this->response(true,'get periods success',$data);*/
    }
}
