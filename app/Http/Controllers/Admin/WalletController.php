<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Wallet\{user_wallet,user_free_wallet};
use App\Interfaces\UserRepositoryInterface;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\User;
use Carbon\Carbon;

class WalletController extends Controller
{
    //


    public function __construct(UserRepositoryInterface $UserRepository ,NotificationRepositoryinterface $NotificationRepository)
    {
        $this->UserRepository = $UserRepository;
        $this->NotificationRepository=$NotificationRepository;
    }

    public function index()
    {
        $users = User::with([
            'userwallet',
            'userfreewallet' => function($q){
                // ملاحظة: لا داعي لاستخدام ->get() هنا؛
                // لأن with() سيجلب البيانات تلقائياً
                $q->selectRaw('user_id, SUM(amount) as amount')
                    ->where('end_date', '>=', Carbon::now()->toDateTimeString())
                    ->groupBy('user_id');
            }
        ])->paginate(50);

        foreach ($users as $user) {
            // إذا لم توجد userwallet أو amount بداخلها، سيرجع 0 بدلاً من حدوث خطأ
            $walletAmount = optional($user->userwallet)->amount ?? 0;

            // إذا لم توجد سجلات في userfreewallet أو لم يكن بها amount، سيرجع 0
            $freeWalletAmount = optional($user->userfreewallet->first())->amount ?? 0;

            // إجمالي المحفظة
            $user->totalwallet = $walletAmount + $freeWalletAmount;
        }

        return view('Admin.wallet.index', compact('users'));
    }


    public function addfreewallet(Request $request)
    {
        $request->validate([
            'userid'   => 'required',
            'amount'   => 'required',
            'end_date' => 'required',
        ]);

        try {
            DB::beginTransaction();

            // إنشاء سجل جديد في user_free_wallet
            user_free_wallet::create([
                'user_id' => $request->userid,
                'end_date' => $request->end_date,
                'amount'  => $request->amount,
            ]);

            // جلب المستخدم من قاعدة البيانات
            $user = User::find($request->userid);

            // تحضير نصوص الإشعار بلغتين (ar, en)
            // يمكنك ضبط النصوص حسب ما يناسبك
            $titles = [
                'ar' => 'المحفظة',
                'en' => 'Wallet',
            ];
            $bodys = [
                'ar' => 'لقد حصلت على مبلغ بقيمة ' . $request->amount . ' ينتهي بتاريخ ' . $request->end_date,
                'en' => 'You have received an amount of ' . $request->amount . ' which will end on ' . $request->end_date,
            ];

            // نحدّد لغة المستخدم، وإن لم تكن موجودة نعتمد العربية أو الإنجليزية (كما تفضّل)
            $userLang = $user->lang ?? 'ar';
            // يمكن تغييره إلى 'en' إذا أردت أن تكون الإنجليزية هي الافتراضية

            // نحدّد العنوان والنص بناءً على لغة المستخدم
            $title = ($userLang === 'en') ? $titles['en'] : $titles['ar'];
            $body  = ($userLang === 'en') ? $bodys['en']  : $bodys['ar'];

            // فحص ما إذا كان المستخدم مفعلًا لاستقبال هذا النوع من الإشعارات (notificationtype_id = 3)
            $usernotifytype = DB::table('usernotifytype')
                ->where('user_id', $user->id)
                ->where('notificationtype_id', '3')
                ->where('status', '1')
                ->first();

            if ($usernotifytype) {
                // إرسال الإشعار الفوري
                $this->NotificationRepository->sendnotification('user', $user->id, $title, $body, 10);

                // إنشاء سجل الإشعار في قاعدة البيانات
                $this->NotificationRepository->createnotification('user', $user->id, $titles, $bodys);
            }

            DB::commit();

            Alert::success('success', 'Add free discount success');
            return redirect()->route('Admin.user.wallet');
        } catch (\Exception $e) {
            DB::rollback();
            // يمكنك عرض الخطأ أو تسجيله
            Alert::error('Error', $e->getMessage());
            return redirect()->route('Admin.user.wallet');
        }
    }


    public function search(Request $request){
        $users= $this->UserRepository->search($request);
        $users=$users->with(['userwallet','userfreewallet'=>function($q){
        $q->selectRaw('sum(amount) as amount')
        ->selectRaw('user_id')
        ->where('end_date','>=',Carbon::now()->toDateTimeString())
        ->groupBy('user_id')
        ->get();
        }])->paginate();
        foreach ($users as $user) {
            $walletAmount    = optional($user->userwallet)->amount ?? 0;
            $freeWalletAmount = optional($user->userfreewallet->first())->amount ?? 0;
            $user->totalwallet = $walletAmount + $freeWalletAmount;
        }

        return view('Admin.wallet.index',compact('users'));
    }
}
