<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Models\laundryservice\Category;
use App\Models\laundryservice\CategoryTranslation;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories = Category::with('categorytranslate')
                        ->latest()
                        ->paginate(10);
        return view('Admin.categories.index', compact('categories'));
    }

    public function store(CategoryRequest $request)
    {
        $langs = config('translatable.locales');

        $category = Category::create();

        foreach ($langs as $locale) {
            CategoryTranslation::create([
                'category_id' => $category->id,
                'locale'      => $locale,
                'name'        => $request->$locale['name'],
            ]);
        } //end of for each

        return redirect()->back()->with(['success' => __('admin.added_successfully')]);
    }

    public function update(CategoryRequest $request)
    {
        $category = Category::findOrFail($request->id);

        foreach (config('translatable.locales') as $locale) {
            CategoryTranslation::where('locale', $locale)
                ->where('category_id', $category->id)
                ->update([
                    'name' => $request->$locale['name']
                ]);
        }

        return redirect()->back()->with(['success' => __('admin.updated_successfully')]);
    }

    public function destroy($id)
    {
        $category = Category::findOrFail($id)->delete();
        $translation = CategoryTranslation::where('category_id',$id)->delete();
        return redirect()->back()->with(['success' => __('admin.delete_successfully')]);
    }
}
