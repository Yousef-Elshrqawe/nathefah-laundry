<?php

namespace App\Http\Controllers\Admin;

use App\Exports\OrderInvoiceExport;
use App\Http\Controllers\Admin\abstractions\OrderAbstractions;
use App\Models\User;
use App\Models\Order\{order, Orderreview, orderdetailes, OrderDriveryStatus, payment_method};
use App\Models\laundryservice\{Service, Argent, Serviceitemprice};
use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use App\Interfaces\OrderRepositoryInterface;
use App\Models\Day;
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use App\Traits\GeneralTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use PaymentMethods;

class OrderController extends OrderAbstractions
{

    use GeneralTrait;

    public $OrderRepository;
    public $NotificationRepository;

    public function __construct(OrderRepositoryInterface $OrderRepository, NotificationRepositoryinterface $NotificationRepository)
    {
        $this->OrderRepository = $OrderRepository;
        $this->NotificationRepository = $NotificationRepository;
    }

    public function index(Request $request, $type, $date, $reviewdate)
    {


        $orderdate = $date;
        $datereview = $reviewdate;

        if ($date != 'all') {
            $date = \Carbon\Carbon::today()->subDays($date);
        }
        if ($reviewdate != 'all') {
            $reviewdate = \Carbon\Carbon::today()->subDays($reviewdate);
        }
        $laundryname = '';
        $branchname = '';



        $laundries = Laundry::get();
        $users = User::latest()->get();
        if (!$request->branch_id) {
            if($request->laundry_id){
                $branch = Branch::select('username', 'laundry_id', 'id')->where('laundry_id', $request->laundry_id)->get();
                $branchs = Branch::select('username', 'id')->where('laundry_id', $request->laundry_id)->get();
            }else{
                $branch = Branch::select('username', 'laundry_id', 'id')->get();
                $branchs = Branch::select('username', 'id')->get();
            }
            $laundryname = 'All';
            $branch_id = $branch->pluck('id')->toArray();


            if ($branch != null){
                $branchname = count($branch) > 0 ? $branch[0]->username : 'All Branches';
            }
        } else {
            $branch = Branch::select('username', 'laundry_id', 'id')->findorfail($request->branch_id);
            $laundryname = Laundry::select('name')->find($branch->laundry_id)->name;
            $branch_id = $request->branch_id;

            $branchs = Branch::select('username', 'id')->where('laundry_id', $branch->laundry_id)->get();
            if ($branch != null){
                $branchname = $branch->username;
            }
        }
        if ($branch == null){
            return redirect()->back();
        }














        $user_id = $request->user_id;
      /*  if($request->user_id){
            $orders = $orders->where('user_id', $user_id);
        }*/

        if ($type == 'indelivery') {
            $orders = $this->inDeliveryOrder($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'inprogress') {
            $orders = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'inprogress', $request);
        }
        if ($type == 'completed') {
            $orders = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'completed', $request);
        }
        if ($type == 'unassigned') {
            $orders = $this->unassigned($branch_id, $orderdate, $date, $request);

        }
        if ($type == 'new') {
            $orders = $this->newOrder($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'all') {
            $orders = $this->allOrders($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'one_way_delivery') {
            $orders = $this->one_way_delivery($branch_id, $orderdate, $date, $request);
        }

        // count of orders
        $all = $this->allOrders($branch_id, $orderdate, $date, $request)->total();

        $completed = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'completed', $request)->total();

        $inprogresscount = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'inprogress', $request)->total();

        $indelivery = $this->inDeliveryOrder($branch_id, $orderdate, $date, $request)->total();

        $unassigned = $this->unassigned($branch_id, $orderdate, $date, $request)->total();

        $neworder = $this->newOrder($branch_id, $orderdate, $date, $request)->total();

        $onewayorders = $this->one_way_delivery($branch_id, $orderdate, $date, $request)->total();
        // end count of orders

        $services = $this->getServices($branch_id);

        $reports = $this->reports($branch_id, $reviewdate);


        $latestorders = $this->latestOrders($branch_id, '');

        $porttype = 'Admin';


        //user_id


//        dd($orders);

        return view('Admin.orders.index', compact('type', 'orders', 'services', 'porttype', 'branch_id', 'branchs', 'users' ,
            'reports', 'latestorders', 'indelivery', 'all', 'inprogresscount', 'completed', 'orderdate', 'datereview', 'unassigned', 'neworder', 'onewayorders', 'laundries', 'branchname', 'laundryname'));
    }
    //create
    public function create()
    {
        $laundries = Laundry::get();
        return view('Admin.orders.create', compact( 'laundries'));
    }

    //store
    public function store(Request $request)
    {
        //validate
        $this->validate($request, [
            'branch_id' => 'required',
            'laundry_id' => 'required',
        ]);


        $services = $this->getServices($request->branch_id);
        $branchId = $request->branch_id;
        $laundry = Laundry::find($request->laundry_id);
        $porttype='branch';
        $isadmin = 1;
        $branch = Branch::find($request->branch_id);
        $crate_admin = 1;

        return view('Admin.orders.next_create', compact('services', 'porttype', 'laundry', 'isadmin','branchId' ,'crate_admin' ,'branch'));
    }

    public function confirmorder(Request $request)
    {
        if ($request->isadmin = 1) {
            $branchid = $request->branch_id;
        } else {
            $branchid = Auth::guard('branch')->user()->branch_id;
        }
        $this->OrderRepository->checkorder($request, $branchid);

        // $order= Order::find($order_id)->update(['checked'=>true]);
        Alert::success('success', 'order confirmed successfuly');


        return redirect()->back();


    }

    public function completed(Request $request)
    {
//        dd($request->all());
        $rows = Order::where('progress', 'completed');

        /* if ($request->laundry == 'All' && $request->branch == 'All') {
             $rows;
         } elseif ($request->laundry != 'All' && $request->branch == 'All') {
             $branchs = Branch::where('laundry_id', $request->laundry)->get();
 //            $rows = Order::where('progress', 'completed')->whereIn('branch_id', $branchs->pluck('id'))->latest()->paginate(50);
             $rows->whereIn('branch_id', $branchs->pluck('id'));
         } elseif ($request->laundry != 'All' && $request->branch != 'All') {
 //            $rows = Order::where('progress', 'completed')->where('branch_id', $request->branch)->latest()->paginate(50);
             $rows->where('branch_id', $request->branch);
         }

         //date = All  date = 30 date = 60  date = 90

         if ($request->date == 'All') {
             $rows;
         } elseif ($request->date == '1') {
             $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(1));
         } elseif ($request->date == '7') {
             $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(7));
         } elseif ($request->date == '30') {
             $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(30));
         } elseif ($request->date == '60') {
             $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(60));
         } elseif ($request->date == '90') {
             $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(90));
         }*/

        $rows = $rows->latest()->paginate(50);
//        dd($rows);

        $laundries = Laundry::latest()->get();
        $branchs = Branch::latest()->get();
        $payment_methods = payment_method::get();
//        $rows = Order::where('progress','completed')->latest()->paginate(50);
        return view('Admin.orders.completed', compact('rows', 'laundries', 'branchs','payment_methods'));
    }

    //filter orders
    public function filterOrders(Request $request)
    {


        $rows = Order::where('progress', 'completed');
        /*      if ($request->pymenttype_id == 'All') {
                   $rows;
               } else {
                   $rows->where('pymenttype_id', $request->pymenttype_id);
               }*/



        if ($request->laundry == 'All' && $request->branch == 'All') {
            $rows;
        } elseif ($request->laundry != 'All' && $request->branch == 'All' ) {
            $branchs = Branch::where('laundry_id', $request->laundry)->get();
            $rows->whereIn('branch_id', $branchs->pluck('id'));
        } elseif ($request->laundry != 'All' && $request->branch != 'All') {
            $rows->where('branch_id', $request->branch);
        }


        //filter by pymenttype_id 1= Wallet 2= Cash 3 = Visa all
        if ($request->pymenttype_id == 'All') {
            $rows;
        } else {
            $rows->where('pymenttype_id', $request->pymenttype_id);
        }

        if($request->pymenttype_id == 3){
            if($request->payment_method && $request->payment_method != 'All'){
                $rows->where('payment_method_id', $request->payment_method);
            }
        }

        if ($request->date == 'All') {
            $rows;
        } elseif ($request->date == '1') {
            $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays());
        } elseif ($request->date == '7') {
            $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(7));
        } elseif ($request->date == '30') {
            $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(30));
        } elseif ($request->date == '60') {
            $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(60));
        } elseif ($request->date == '90') {
            $rows->where('created_at', '>=', \Carbon\Carbon::today()->subDays(90));
        }


        $rows = $rows->latest()->paginate(50);

        $laundries = Laundry::latest()->get();
        $branchs = Branch::latest()->get();
        $pymenttypes = DB::table('pymenttype')->latest()->get();
        $payment_methods = payment_method::get();
//        $rows = Order::where('progress','completed')->latest()->paginate(50);
        return view('Admin.orders.completed', compact('rows', 'laundries', 'branchs','pymenttypes','payment_methods'));


    }



    public
    function getBransh($id)
    {
        $subCategories = branch::where('laundry_id', $id)->latest()->get();
        return response()->json($subCategories);
    }

    // export  invoices orders to excel
    public function exportInvoices($ids)
    {
        $ids = explode(',', $ids);
        $orders = Excel::download(new OrderInvoiceExport($ids), 'invoices-orders.xlsx');
        return $orders;
    }


    public
    function viewall(Request $request)
    {


        $laundryname = '';
        $branchname = '';
        $laundries = Laundry::where('status', 1)->get();
        if (!$request->branch_id) {
            $branch = Branch::select('username', 'laundry_id', 'id')->where('laundry_id', $laundries[0]->id)->first();
            $laundryname = Laundry::select('name')->find($laundries[0]->id)->name;
            if ($branch != null)
                $branchname = $branch->username;
        } else {
            $branch = Branch::select('username', 'laundry_id', 'id')->findorfail($request->branch_id);
            $laundryname = Laundry::select('name')->find($branch->laundry_id)->name;
            if ($branch != null)
                $branchname = $branch->username;
        }
        if ($branch == null)
            return redirect()->back();
        $branch_id = $request->branch_id ? $request->branch_id : $branch->id;

        $orders = $this->viewAllOrders($branch_id, $request);

        $laundries = Laundry::get();


        return view('Admin.orders.view_all', compact('orders', 'laundries', 'laundryname', 'branchname'));
    }

    public
    function getBranch(Request $request)
    {
        $branches = $this->Branches($request->laundry_id);
        return $this->returnData('data', $branches, 'All branches by laundry id', 200);
    }


    public
    function view($id)
    {
        $order = Order::with(['driver', 'paymenttype', 'deliverytype', 'orderdetailes' => function ($q) {
            $q->with('service', 'Branchitem', 'additionalservice')->get();
        }])->find($id);
        $urgents = Argent::where('order_id', $id)->get();
        return view('branch.Order.view_order', compact('order', 'urgents'));
    }
}
