<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class AuthController extends Controller
{
    //
    public function loginview(){
        return view('Admin.login');
    }


    public function login(Request $request){
        $request->validate([
          'username'=>'required',
           'password'=>'required'
        ]);
        $credentials = $request->only('username', 'password');
        if (Auth::guard('Admin')->attempt($credentials)) {
            return redirect()->route('Admin.deashboard');
        }
        return redirect()->route('Admin.login')->with('error','Login details are not valid');
    }

    public function logout(){
        Auth::guard('Admin')->logout();
        return redirect()->route('Admin.login');
    }
}
