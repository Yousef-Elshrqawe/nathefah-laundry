<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Admin\Admin;
use Auth;
use Hash;
use Session;
class SettingController extends Controller
{
    //
    public function index(){
        $user=Auth::guard('Admin')->user();
        $users=Admin::get();
        return view('Admin.setting.index',compact('user','users'));
     }
     public function update(Request $request){

         $user=Auth::guard('Admin')->user();
         if($request->password==null){
            $user->update($request->except('_token'));
            Alert::success('success', 'Information update successfuly');
         }else{
             $request->validate([
                'password'=> 'required|min:6|max:50|confirmed',
                'password_confirmation' => 'required|max:50|min:6',
             ]);
             $user->update([
                'password' => Hash::make($request->password),
             ]);
             Alert::success('success', 'password update successfuly');
         }
         return redirect()->back();
         //dd('hallo');

     }

     public function storeuser(Request $request){
        $request->validate([
          'name'=>'required|unique:admins',
          'email'=>'required|unique:admins',
          'username'=>'required|unique:admins',
          'password'=> 'required|min:6|max:50|confirmed',
          'password_confirmation' => 'required|max:50|min:6',
        ]);
        Admin::create([
            'name'=>$request->name,
            'email'=>$request->email,
            'username'=>$request->username,
            'password' => Hash::make($request->password),
        ]);
        Alert::success('success','Add user succsessfuly');
        Session::flash('add_user', true);

        return redirect()->back();
     }
}
