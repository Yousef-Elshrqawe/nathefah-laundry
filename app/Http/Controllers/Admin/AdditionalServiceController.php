<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\laundryservice\AdditionalCategoryService;
use App\Models\laundryservice\Additionalservice;
use App\Models\laundryservice\AdditionalserviceTranslation;
use App\Models\laundryservice\Category;
use App\Models\laundryservice\Categoryservice;
use App\Models\laundryservice\Service;
use App\Models\laundryservice\ServiceTranslation;
use App\Traits\fileTrait;
use App\Traits\GeneralTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use RealRashid\SweetAlert\Facades\Alert;


class AdditionalServiceController extends Controller
{
    use fileTrait,GeneralTrait;
    //
    public function index(Request $request)
    {
        $services = Additionalservice::with('categoryaditionalservices')
                    ->when($request->service_search_input, function ($q) use ($request) {
                         return $q->whereTranslationLike('name', '%' . $request->service_search_input . '%');
                      })->orderBy('id', 'DESC')
                     ->paginate(10);

        $categories = Category::with('categoryservice')->get();


        return view('Admin.additional_services.index', compact(['services','categories']));
    } // end of index


    public function store(Request $request){
        // $request->validate([
        //    'ar.name' => 'required',
        //    'en.name' => 'required',
        //    'image' => 'required|image',
        // ]);
        // Service::create([
        //    'code'=>$request->code,
        // ]);
        // return redirect('Super-admin/discount/avilable');

        $langs = config('translatable.locales');

        //validation
        $rules = [];
        foreach ($langs as $locale) {
            $rules += [$locale . '.name' => 'required|unique:additionalservicetranslations,name',];
        }


        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        //end of validation

        //insert
        $service = Additionalservice::create();

        foreach ($langs as $locale) {
            $service_trans = new AdditionalserviceTranslation();
            $service_trans->additionalservice_id = $service->id;
            $service_trans->locale = $locale;
            $service_trans->name = $request->$locale['name'];
            $service_trans->save();
        } //end of for each

        Alert::success('success', __('admin.added_successfully'));

        return redirect()->back()->with(['success' => __('admin.added_successfully')]);
    } // end of


    public function edit($id)
    {
        $service = Additionalservice::find($id);
        if (!$service)
            return redirect()->route('Admin.services')->with(['error' => 'هذه الخدمة غير موجوده']);

        return view('Admin.additional_services.edit', compact(['service']));
    } // end of edit



    public function update(Request $request, $id)
    {
        $service = Additionalservice::find($id);

        if (!$service)
            return redirect()->route('Admin.services')->with(['error' => 'هذه الخدمة غير موجوده']);

        $request->validate([
            'ar.name' => 'required',
            'en.name' => 'required',
        ]);

        //Update translations
        foreach (config('translatable.locales') as $locale) {
            AdditionalserviceTranslation::where('locale', $locale)
                ->where('additionalservice_id', $service->id)
                ->update([
                    'name' => $request->$locale['name']
                ]);
        }
        Alert::success('success', __('admin.updated_successfully'));

        return redirect()->back()->with(['success' => __('admin.updated_successfully')]);
    } // end of update


    public function assignCategoryToService(Request $request)
    {
        $service_id   = $request->service_id;
        $category_ids = $request->category_ids;

        //Delete old checked first
        AdditionalCategoryService::where('additionalservice_id',$service_id)->delete();

        foreach ($category_ids as $category_id)
        {
            AdditionalCategoryService::create([
                'additionalservice_id'  => $service_id,
                'category_id' => $category_id,
            ]);
        }

        return redirect()->back()->with(['success' => 'Assign category successfully']);
    }

    public function getServiceCategory(Request $request)
    {
        $categories = AdditionalCategoryService::where('additionalservice_id',$request->id)->get();



        return $this->returnData('data',$categories,'Categories Service',200);
    }
}
