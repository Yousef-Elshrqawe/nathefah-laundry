<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\DriverStore;

use App\Http\Requests\DriverUpdate;
use App\Models\Driver\Driver;
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;

class DriverController extends Controller
{
    //
    public function index(Request $request)
    {
        $rows = Driver::latest()->paginate(10);
        $branches = branch::latest()->get();
        $laundries = Laundry::latest()->get();

        return view('Admin.driver.index', compact(['rows', 'branches' , 'laundries']));
    } // end of index

    //getBranchesAjax
    public function getBranchesAjax(Request $request , $id)
    {
        $branches = branch::where('laundry_id', $id)->get();
        return response()->json($branches);
    } // end of getBranchesAjax

    public function store(DriverStore $request)
    {
        $data = $request->except(['password_confirmation', 'next' , 'laundry_id']);
        $data['password'] = bcrypt($request->password);
        $driver = Driver::create($data + ['country_code' => '+966']);
        Alert::success('Success', 'Driver Created Successfully');
        return Redirect::back();
    } // end of store


    public function edit($id)
    {
        $row = Driver::findOrFail($id);
        $branches = branch::latest()->get();
        $laundries = Laundry::latest()->get();
        return view('Admin.driver.edit', compact(['row', 'branches' , 'laundries']));
    } // end of edit


    public function update(DriverUpdate $request, $id)
    {
        $row = Driver::findOrFail($id);
        $data = $request->except(['password_confirmation', 'next' , 'laundry_id']);
        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data['password'] = $row->password;
        }
        $row->update($data);
        Alert::success('Success', 'Driver Updated Successfully');
        return Redirect::route('admin.drivers');
    } // end of update


    public function destroy($id)
    {
        $row = Driver::findOrFail($id);
        $row->delete();
        Alert::success('Success', 'Driver Deleted Successfully');
        return Redirect::back();
    } // end of destroy


}
