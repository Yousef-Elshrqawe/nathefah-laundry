<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ForceUpdate;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class UpdateAppsController extends Controller
{

    public function index(Request $request)
    {
        $rows = ForceUpdate::orderBy('id', 'desc')->get();
        return view('Admin.update_apps.index', compact(['rows']));
    } // end of index




    public function edit($id)
    {
        $row = ForceUpdate::findOrFail($id);
        return view('Admin.update_apps.edit', compact(['row']));
    } // end of edit



    public function update(Request $request, $id)
    {
        $row = ForceUpdate::findOrFail($id);

        $request->validate([
            'version' => 'required',
            'is_force_update' => 'required|in:0,1',
            ]);

        //Update
        $row->update($request->all());
        Alert::success('Success', 'App Updated Successfully');
        return  Redirect::route('admin.update.apps');
    } // end of update




} // end of ItemController
