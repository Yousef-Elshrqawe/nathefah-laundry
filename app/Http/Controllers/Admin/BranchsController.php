<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStore;
use App\Http\Requests\AdminUpdate;
use App\Http\Requests\BranchStore;
use App\Http\Requests\BranchUpdate;
use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;
use App\Models\Admin\Admin;

use App\Models\Laundry\branch;
use App\Models\Laundry\BranchPayment;
use App\Models\Laundry\Branchuser;
use App\Models\Laundry\Laundry;
use App\Models\Order\payment_method;
use App\Models\TransactionBranch;
use App\Models\User;
use App\Models\Wallet\branch_wallet;
use Illuminate\Support\Facades\DB;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class BranchsController extends Controller
{

    public function index(Request $request)
    {
        $rows = branch::latest()->paginate(10);
        $laundries = Laundry::latest()->get();

        return view('Admin.branch.index', compact(['rows', 'laundries']));
    } // end of index


    public function store(BranchStore $request)
    {
        $data = $request->except(['password_confirmation', 'next' ]);

        DB::beginTransaction();
       $branch =  branch::create($data + ['country_code' => "+966"]);
        $branchuser =   $branch->branchusers()->create([
            'username' => $request->username,
            'country_code' => "+966", // "+966
            'phone' => $request->phone,
            'type' => 'Admin',
            'password' => bcrypt($request->password),
        ]);

        branch_wallet::create([
            'laundry_id' => $request->laundry_id,
            'branch_id' => $branch->id
        ]);
        $branchuser->assignRole([1]);
        $payment_method = payment_method::all();
        foreach ($payment_method as $payment) {
            BranchPayment::create([
                'branch_id' => $branch->id,
                'payment_method_id' => $payment->id
            ]);

        }
        DB::commit();

        Alert::success('Success', 'Branch Added Successfully');
        return Redirect::back();
    } // end of store


    public function edit($id)
    {

        $row = branch::findOrFail($id);
        $laundries = Laundry::latest()->get();
        return view('Admin.branch.edit', compact(['row', 'laundries']));
    } // end of edit



    public function update(BranchUpdate $request, $id)
    {
        $row = branch::findOrFail($id);
        $data = $request->except(['password_confirmation', 'next' ]);

        // dd($data);
        $row->update($data);
         $row->branchusers()->update([
            'username' => $request->username,
            'phone' => $request->phone,
            'type' => 'Admin',
            'password' => bcrypt($request->password),

        ]);


        if (branch_wallet::where('branch_id', $row->id)->exists()) {
            branch_wallet::where('branch_id', $row->id)->update([
                'laundry_id' => $request->laundry_id,
                'branch_id' => $row->id
            ]);
        }
        else {
            branch_wallet::create([
                'laundry_id' => $request->laundry_id,
                'branch_id' => $row->id
            ]);
        }

        $branchuser =  Branchuser::where('branch_id', $row->id)->first();
        $branchuser->assignRole([1]);


        Alert::success('Success', 'Branch Updated Successfully');
        return Redirect::back();
    } // end of update


    public function destroy($id)
    {
        $row = branch::findOrFail($id);
        $row->orders()->delete();
        $row->branchusers()->delete();
        $row->delete();
        Alert::success('Success', 'Branch Deleted Successfully');
        return Redirect::back();
    } // end of destroy


    public function payment($id)
    {
        $branch = branch::findOrFail($id);
        $rows = TransactionBranch::where('branch_id', $id)->latest()->paginate(10);
        return view('Admin.branch.payments.index', compact('rows', 'branch'));
    } // end of payment

    //paymentstore
    public function paymentstore(Request $request)
    {
        $branch = branch::findOrFail($request->branch_id);
        $data = $request->validate([
            'branch_id' => 'required|exists:branchs,id',
            'paid' => 'required|numeric|min:1|max:'.$branch->balance,
        ]);
        $data['remaining'] = $branch->balance - $request->paid;
        $data['branch_balance'] = $branch->balance;
        $branch->update([
            'balance' => $branch->balance - $request->paid,
        ]);
        $data['current_balance'] = $branch->balance;


        TransactionBranch::create($data);
        Alert::success('Success', 'Payment Added Successfully');
        return Redirect::back();

    } // end of paymentstore


} // end of ItemController
