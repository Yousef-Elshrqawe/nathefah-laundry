<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Laundry\Laundry;
use RealRashid\SweetAlert\Facades\Alert;

class DeliveryfeesController extends Controller
{
    //
    public function index(){
        $delivery_fees=Laundry::select('door_to_door_delivery_fees','one_way_delivery_fees')->find(auth('laundry')->id());
        return view('laundry.deliveryfees.index',compact('delivery_fees'));
    }
    public function update(Request $request){
        $delivery_fees=Laundry::find(auth('laundry')->id());
        if($request->id==1){
            $delivery_fees->update(['one_way_delivery_fees'=>$request->price]);
        }else{
         $delivery_fees->update(['door_to_door_delivery_fees'=>$request->price]);
        }
        Alert::success('success', 'delivery fees update succsessfuly');
        return redirect()->back();
    }
}
