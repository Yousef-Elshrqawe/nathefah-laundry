<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Admin\abstractions\SettingAbstractions;
use App\Interfaces\{NotificationRepositoryinterface,SubscriptionRepositoryInterface};
use App\Http\Controllers\Controller;
use App\Models\Laundry\branch;
use App\Models\Laundry\Branchuser;
use App\Models\closeingday\Closeingday;
use App\Models\Order\order;
use App\Models\Balance\Balance;
use App\Models\Wallet\branch_wallet;
use App\Traits\GeneralTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Http\Requests\{BranchRequest,UserBranchRequest};
use RealRashid\SweetAlert\Facades\Alert;


class BranchController extends SettingAbstractions
{

    use GeneralTrait;

    public function __construct(NotificationRepositoryinterface $NotificationRepository ,SubscriptionRepositoryInterface $SubscriptionRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
        $this->SubscriptionRepository = $SubscriptionRepository;

    }

    public function index(Request $request)
    {
        $branches = branch::where('laundry_id',auth('laundry')->id())
            // ->addSelect([
            //     'balance' => Balance::selectRaw('sum(price)')
            //         ->whereColumn('branchbalance.branch_id', 'branchs.id')
            //          ])
            ->addSelect([
                'totalOrders' => Order::selectRaw('COUNT(*)')
                    ->whereColumn('orders.branch_id', 'branchs.id')
            ])
            ->when($request->search, function($q) use ($request) {
                $q->where('laundry_id',auth('laundry')->id())
                    ->where(function ($query) use ($request) {
                        $query->where('username', 'like', '%' . $request->search . '%')
                              ->orWhere('country_code', $request->search)
                              ->orWhere('phone', $request->search)
                              ->orWhere('status', $request->search)
                              ->orWhere('address', $request->search);
                    });
            })
            ->get();

        $branchId = count($branches) > 0 ? $branches[0]->id : "";


        if(count($branches) > 0){
            $reports = DB::table('orders')
                ->select(DB::raw(" (SELECT count(*) from orders WHERE orders.branch_id = $branchId) AllOrders"))
                ->addSelect([
                    'completedOrders' => Order::selectRaw('COUNT(*)')
                        ->where('orders.branch_id', $branchId)
                        ->where('orders.delivery_status', "completed")
                ])
                ->addSelect([
                    'inProgressOrders' => Order::selectRaw('COUNT(*)')
                        ->where('orders.branch_id', $branchId)
                        ->where('orders.delivery_status', "inprogress")
                ])
                ->first();
        }else{
            $reports = 0;
        }

//        return $reports;
        $closeingdaies = Closeingday::get();

        return view('laundry.branches.index',compact('branches','reports','closeingdaies'));
    }


    public function store(Request $request,BranchRequest $BranchRequest)
    {

        $laundry_id=auth('laundry')->id();
        $limitbranchcount=$this->SubscriptionRepository->checkbranchcount($laundry_id);
        $branchcount=branch::where('laundry_id',$laundry_id)->count();
        if($branchcount>=$limitbranchcount){
          Alert::alert('alert', 'You have exceeded the limit', 'Type');
          return redirect()->back();
        }
        DB::beginTransaction();
        //Save Branch Info
        $branch = $this->storeBranch($request);

        //Save Branch Wallet
        $this->storeBranchWallet($branch);

        //Save Admin Info For Branch
        $this->storeBranchAdmin($request,$branch);

        DB::commit();
        Alert::success('success', 'Branch added successfully');
        return redirect()->back();
    }

    public function edit(Request $request){
         $branch = branch::where('id',$request->branchId)->first();
         return $this->returnData('data',$branch,'All Data',200);
    }

    public function update(Request $request)
    {

        $branch = branch::where('id',$request->id)->first();

        $branch->update([
            'username'       => $request->branch_name,
            'country_code'   => $request->branch_country_code,
            'phone'          => $request->branch_phone,
            'status'         => $request->status,
            'lat'            => $request->lat,
            'long'           => $request->long,
            'open_time'      => $request->open_time,
            'closed_time'    => $request->closed_time,
            'address'        => $request->address,
            'password'       => Hash::make($request->branch_password),
        ]);

        return redirect()->back();
    }
    public function destroy($id)
    {
        branch::findOrFail($id)->delete();
        return redirect()->back();
    }

    public function reports(Request $request)
    {
        $brancheid = $request->branchId;

        $reports = DB::table('orders')
            ->select(DB::raw(" (SELECT count(*) from orders WHERE orders.branch_id = $brancheid) AllOrders"))
            ->addSelect([
                'completedOrders' => Order::selectRaw('COUNT(*)')
                    ->where('orders.branch_id', $brancheid)
                    ->where('orders.delivery_status', "completed")
            ])
            ->addSelect([
                'inProgressOrders' => Order::selectRaw('COUNT(*)')
                    ->where('orders.branch_id', $request)
                    ->where('orders.delivery_status', "inprogress")
            ])
            ->first();

        return $this->returnData('data',$reports,'All Data',200);
    }

    public function storeBranch($request)
    {
        return branch::create([
            'username'       => $request->branch_name,
            'country_code'   => $request->branch_country_code,
            'phone'          => $request->branch_phone,
            'status'         => $request->status,
            'lat'            => $request->lat,
            'long'           => $request->long,
            'open_time'      => $request->open_time,
            'closed_time'    => $request->closed_time,
            'address'        => $request->address,
            'laundry_id'     => auth('laundry')->id(),
            'password'       => Hash::make($request->branch_password),
        ]);
    }

    public function storeBranchWallet($branch)
    {
        branch_wallet::create([
            'laundry_id' => auth('laundry')->id(),
            'branch_id'  => $branch->id
        ]);
    }

    public function storeBranchAdmin($request,$branch)
    {

        $user = Branchuser::create([
            'type'          => 'Admin',
            'country_code'  => $request->user_country_code,
            'phone'         => $request->user_phone,
            'username'      => $request->username,
            'branch_id'     => $branch->id,
            'password'      => $request->password,
        ]);
         $this->NotificationRepository->createnotificationsetting('branch',$user->id);
         $role=Role::where('guard_name','branch')->first();
         $user->assignRole([$role->id]);
    }


    /**
     * @param Request $request
     * @param $id
     * @return object
     */
    public function updateInfo(Request $request,$id)
    {
        $this->updateBranchInfo($id,$request);

        Alert::success('success', 'Info updated successfully');
        return redirect()->back();
    }


    public function branchDetails(Request $request,$branchId)
    {
        $branchInfo    = $this->branchInfo($branchId,$request);
        $successOrders = $this->successOrders($branchId,'');
        $services_id   = $this->serviceIds($branchId);
        $services      = $this->services($branchId);

        $additionalservices_id = $this->additionalServicesIds($branchId);
        // use this in show only
        $additionalservices = $this->additionalServices($branchId);

        $latestOrders = $this->latestOrders($branchId,'');


        $todaySuccessOrders = $this->successOrders($branchId,Carbon::today());
        $todayOrders        = $this->allTodayOrders($branchId);


        $yesterdaySuccessOrders = $this->successOrders($branchId,Carbon::yesterday());
        $yesterdayOrders        = $this->allTodayOrders($branchId);

        $reports  = $this->orderReports($branchId);

        return view('laundry.branches.branch_details.index',compact(
            'branchInfo',
            'successOrders',
            'branchId',
            'services_id',
            'services',
            'additionalservices',
            'additionalservices_id',
            'latestOrders',
            'todaySuccessOrders',
            'todayOrders',
            'yesterdayOrders',
            'yesterdaySuccessOrders',
            'reports'
        ));
    }
}
