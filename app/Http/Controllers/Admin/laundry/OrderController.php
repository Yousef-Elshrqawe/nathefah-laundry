<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Admin\abstractions\OrderAbstractions;
use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use App\Interfaces\OrderRepositoryInterface;
use App\Models\Driver\Driver;
use App\Models\laundryservice\Argent;
use App\Models\Notification\drivernotifytype;
use App\Models\Order\order;
use App\Models\Order\OrderDriveryStatus;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Laundry\Branchuser;
use App\Models\Order\payment_method;
use Auth;
use Session;

class OrderController extends OrderAbstractions
{

    public $OrderRepository;
    public $NotificationRepository;

    public function __construct(OrderRepositoryInterface $OrderRepository,NotificationRepositoryinterface $NotificationRepository)
    {
        $this->OrderRepository = $OrderRepository;
        $this->NotificationRepository=$NotificationRepository;
    }

    public function index(Request $request,$type,$date,$reviewdate){
        $orderdate  =  $date;
        $datereview = $reviewdate;

        if($date!='all'){
            $date = \Carbon\Carbon::today()->subDays($date);
        }
        if($reviewdate!='all'){
            $reviewdate = \Carbon\Carbon::today()->subDays($reviewdate);
        }

        $branches = $this->Branches(auth('laundry')->id());

        if($branches->isEmpty()){
            Alert::error('no branchs', 'you have no branchs add branch first');
            return redirect()->back();
        }

        //get selected branch id
        $branch_id=$this->getBranchId($request,$branches);


        if($type == 'indelivery'){
            $orders = $this->inDeliveryOrder($branch_id,$orderdate,$date,$request);
        }

        if($type=='inprogress'){
            $orders = $this->inProgressCompletedOrder($branch_id,$orderdate,$date,'inprogress',$request);
        }
        if($type=='completed'){
            $orders = $this->inProgressCompletedOrder($branch_id,$orderdate,$date,'completed',$request);
        }
        if($type=='unassigned'){
            $orders = $this->unassigned($branch_id,$orderdate,$date,$request);
        }
        if($type=='new'){
            $orders = $this->newOrder($branch_id,$orderdate,$date,$request);
        }

        if($type=='all'){
            $orders = $this->allOrders($branch_id,$orderdate,$date,$request);
        }

        if($type=='self_delivery'){
            $orders = $this->self_delivery_orders($branch_id,$orderdate,$date,$request);
        }

        if($type=='one_way_delivery'){
            $orders =$this->one_way_delivery($branch_id,$orderdate,$date,$request);
        }

        // count of orders
        $all             = $this->allOrders($branch_id,$orderdate,$date,$request)->total();


        $completed       = $this->inProgressCompletedOrder($branch_id,$orderdate,$date,'completed',$request)->total();

        $inprogresscount = $this->inProgressCompletedOrder($branch_id,$orderdate,$date,'inprogress',$request)->total();

        $indelivery      = $this->inDeliveryOrder($branch_id,$orderdate,$date,$request)->total();

        $unassigned      = $this->unassigned($branch_id,$orderdate,$date,$request)->total();

        $neworder        = $this->newOrder($branch_id,$orderdate,$date,$request)->total();

        $onewayorders=$this->one_way_delivery($branch_id,$orderdate,$date,$request)->total();

        $self_delivery_orders=$this->self_delivery_orders($branch_id,$orderdate,$date,$request)->total();

        // end count of orders

        $services = $this->getServices($branch_id);

        $reports  = $this->reports($branch_id,$reviewdate);

        $latestorders = $this->latestOrders($branch_id,'');

        $laundry=Auth::guard('laundry')->user();

        $porttype='laundry';

        $payment_methods=payment_method::all();

        return view('laundry.orders.index',compact('type','orders','services','porttype',
            'reports','latestorders','indelivery','all','inprogresscount','completed','orderdate'
            ,'datereview','unassigned','neworder','branches','onewayorders','self_delivery_orders','laundry','branch_id','payment_methods'));
    }


    public function submit(Request $request){



        $branches = $this->Branches(auth('laundry')->id());

        $branch_id = $request->branch_id ? $request->branch_id : $branches[0]->id;

        $submit =  $this->submitOrder($request,$branch_id);

        $order     = $submit[0];
        $itemcount = $submit[1];
        $distance  = $submit[2];
        return view('branch.components.order_detailes',compact('order','itemcount','distance'));
    }

    public function confirmOrder(Request $request){


        $branches = $this->Branches(auth('laundry')->id());

        $branch_id = $request->branch_id ? $request->branch_id : $branches[0]->id;


        $this->OrderRepository->checkorder($request,$branch_id);


        Alert::success('success', 'Order confirmed successfully');
        return redirect()->back();
    }

    public function latestOrder(Request $request){
        $date = \Carbon\Carbon::today()->subDays($request->days);


        $branch_id=$this->getBranchId($request,$branches);

        $orders = $this->latestOrders($branch_id,$date);

        return view('laundry.home.index',compact('orders'));
    }

    public function viewAll(Request $request){
        $branches = $this->Branches(auth('laundry')->id());

        //get selected branch id
        $branch_id=$this->getBranchId($request,$branches);

        $orders    = $this->viewAllOrders($branch_id,$request);
        return view('laundry.orders.view',compact('orders','branches','branch_id'));
    }


    public function view($id){
        $order=Order::with(['driver','paymenttype','deliverytype','orderdetailes'=>function($q){
            $q->with('service','Branchitem','additionalservice')->get();
        }])->find($id);
        $urgents=Argent::where('order_id',$id)->get();
        //dd($urgents);
        return view('branch.Order.view_order',compact('order','urgents'));
    }

    public function finishOrder(Request $request,$id){
        $branches      = $this->Branches(auth('laundry')->id());

        //get selected branch id
        $branch_id=$this->getBranchId($request,$branches);

        $order = $this->OrderRepository->finishorder($id,$branch_id);

        if($order==true){
            Alert::success('success', 'Order finished successfully');
            return redirect()->back();
        }
    }


    public function acceptOrder(Request $request,$id){

        $branches      = $this->Branches(auth('laundry')->id());

        //get selected branch id
        $branch_id=$this->getBranchId($request,$branches);


        $branch_user_id=Branchuser::where('branch_id',$branch_id)->first()->id;

        $order =$this->OrderRepository->acceptorder($id,$branch_id,$branch_user_id);
        if($order==true){
            Alert::success('success', 'Order accepted successfully');
            return redirect()->back();
        }
    }


    public function assignOrderReview(Request $request,$order_id){
        $branches      = $this->Branches(auth('laundry')->id());

        //get selected branch id
        $branch_id=$this->getBranchId($request,$branches);

        $drivers= Driver::where('branch_id',$branch_id)->get();


        return view('laundry.drivers.assigndrivers',compact('drivers','order_id','branches','branch_id'));
    }


    public function assignDriver($driver_id,$order_id){
        order::findorfail($order_id)->update([
            'driver_id'=>$driver_id
        ]);
        $driver=Driver::find($driver_id);
        $orderdeliverystatus=OrderDriveryStatus::where('order_id',$order_id)->first();
        $orderdeliverystatus->update([
            'driver_id'=>$driver_id,
        ]);
        $title='طلب جديد';
        $body='لقد استلمت طلب جديد الان';
        if($driver->lang=='en'){
            $title='new order';
            $body='you recive new order from laundry now';
        }
        $titles=['ar'=>'طلب جددي','en'=>'new order'];
        $bodys=['ar'=>'new order','en'=>'you recive new order from laundry now'];

        $driver = Driver::find($driver_id);
        $drivernotifytype = drivernotifytype::where('driver_id', $driver_id)->where('notificationtype_id', 3)->where('status', 1)->first();

        if ($drivernotifytype) {
            $this->NotificationRepository->sendnotification('driver', $driver_id, $title, $body, 1, null, null);
            $this->NotificationRepository->createnotification('driver', $driver_id, $titles, $bodys);
        }
        Alert::success('success', 'order accepted successfuly');
        return redirect()->route('laundry.orders',['unassigned','all','all']);
    }


    public function generate_qr_code($order_id){
        $code=  $this->OrderRepository->getcode('pick_up_laundry',$order_id);
        return view('components.orders.qrcode',compact('order_id','code'));
    }

    public function receiveOrder(Request $request){
        $data=$this->OrderRepository->reciveorder($request);
        if ($data==null){
            return redirect()->back();
        }
        Alert::success('success', 'order accepted successfuly');
        return redirect()->back();
    }


    public function getBranchId($request,$branches){
        if($request->branch_id)
        Session::put('branch_id',$request->branch_id);

        $branch_id = $request->branch_id ? $request->branch_id : Session::get('branch_id');
        if(Session::get('branch_id')==null)
        $branch_id=$branches[0]->id;
        return $branch_id;
    }

    public function deliverOrder($id){
        try{
            $this->deliver($id);
            Alert::success('success', 'user recive  order success');
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

}
