<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Admin\abstractions\ServiceAbstractions;
use App\Models\Laundry\branchservice;
use App\Models\Order\order;
use App\Traits\GeneralTrait;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Session;

class ServiceController extends ServiceAbstractions
{
    use GeneralTrait;

    public function index(Request $request){



        $branches = $this->Branches(auth('laundry')->id());

        if($branches->isEmpty()){
            Alert::error('no branchs', 'you have no branchs add branch first');
            return redirect()->back();
         }


        $branch_id = $request->branch_id ? $request->branch_id : $branches[0]->id;

        $services_id = $this->serviceIds($branch_id);

        $services = $this->services($branch_id);

        $additionalservices_id = $this->additionalServicesIds($branch_id);
        // use this in show only
        $additionalservices = $this->additionalServices($branch_id);

        $argentPrice = $this->argentPrice($branch_id);

        $reports = $this->reports($branch_id);
        $cancelOrders = order::whereIN('branch_id',$branches->pluck('id')->toArray())->where('confirmation' , 'refused')->count();


        return view('laundry.services.index',compact('services','additionalservices','services_id','additionalservices_id','argentPrice','branches','reports','cancelOrders','branch_id'));
    }

    public function edit(Request $request){
        $branches = $this->Branches(auth('laundry')->id());

        if($branches->isEmpty()){
            Alert::error('no branchs', 'you have no branchs add branch first');
            return redirect()->back();
         }

         if($request->branch_id)
         Session::put('branch_id',$request->branch_id);

         $branch_id = $request->branch_id ? $request->branch_id : Session::get('branch_id');
         if(Session::get('branch_id')==null)
         $branch_id=$branches[0]->id;


        $services_id = $this->serviceIds($branch_id);

        $services    = $this->servicesByServiceId($branch_id,$services_id);

        $additionalservices = $this->additionalServices($branch_id);

        return view('laundry.services.edit',compact('services','additionalservices','branches','branch_id'));
    }

    public function update(Request $request){
        $this->updateService($request);
        Alert::success('success', 'Services updated successfully');
        return redirect()->back();
    }

    public function getItem($id){
        return $this->getServiceItem($id);
    }

    // in this function change item name
    public function editItem(Request $request){
        $this->editServiceItem($request);
        Alert::success('success', 'Item updated successfully');
        return redirect()->back();
    }

    public function addItem(Request $request){
        //dd($request->all());
        $branches  = $this->Branches(auth('laundry')->id());

        if($branches->isEmpty()){
            Alert::error('no branchs', 'you have no branchs add branch first');
            return redirect()->back();
        }

        $branch_id = $request->branch_id ? $request->branch_id : $branches[0]->id;

        $this->storeItem($request,$branch_id);

        Alert::success('success', 'New items Add successfully');
        return redirect()->back();
    }

    public function changeStatus(Request $request)
    {
        $service = $this->changeServiceStatus($request);

        return $this->returnData('data',$service,'Status','200');
    }

    public function changeItemStatus(Request $request)
    {
        $service = $this->changeItemServiceStatus($request);
        return $this->returnData('data',$service,'Status','200');
    }

    public function changeArgentPrice(Request $request)
    {
        $this->changeItemArgentPrice($request);

        Alert::success('success', 'Argent price updated successfully');
        return redirect()->back();
    }

}
