<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
class AuthController extends Controller
{
    //
    public function loginview(){
        return view('laundry.Auth.login');
    }
    public function login(Request $request) {
        // تحقق من صحة المدخلات
        $request->validate([
            'phone' => 'required',
            'password' => 'required',
        ]);

        // تنسيق رقم الهاتف لمحاولة تسجيل الدخول بحالتين
        $phone = ltrim($request->phone, '0'); // إزالة الأصفار من البداية
        $formattedPhoneWithZero = '0' . $phone; // الرقم مع الصفر في البداية
        $formattedPhoneWithoutZero = $phone; // الرقم بدون الصفر في البداية

        // إعداد بيانات المصادقة لمحاولة الاثنين
        $credentialsWithZero = [
            'phone' => $formattedPhoneWithZero,
            'password' => $request->password,
        ];

        $credentialsWithoutZero = [
            'phone' => $formattedPhoneWithoutZero,
            'password' => $request->password,
        ];

        // محاولة تسجيل الدخول باستخدام الرقم مع صفر أو بدونه
        if (Auth::guard('laundry')->attempt($credentialsWithZero) || Auth::guard('laundry')->attempt($credentialsWithoutZero)) {
            return redirect()->route('laundry.home', 7);
        }

        // في حالة فشل تسجيل الدخول
        return redirect()->route('laundry.login')->with('error', 'Login details are not valid');
    }


    public function logout(){
        Auth::guard('laundry')->logout();
        return redirect()->route('laundry.login');
    }
}
