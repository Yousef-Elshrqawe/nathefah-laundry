<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Admin\abstractions\HomeAbstraction;
use App\Models\{Transaction,SystemTransaction};

use App\Http\Controllers\Controller;
use App\Models\Laundry\branch;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Session;

class dashboardController extends HomeAbstraction
{
    //
    public function home(Request $request,$week){

        $branches = branch::where('laundry_id', auth('laundry')->id())->get();


        if ($week == 'All') {

            $date = \Carbon\Carbon::now()->subDays(365);
        }else{
            $date = \Carbon\Carbon::today()->subDays($week);
        }

        $balance = 0;
        $balanceToday = 0;
        $todayOrders = 0;
        $inProgress = 0;

        $balance = branch::selectRaw('sum(balance) as balance')
            ->groupBy('laundry_id')
            ->where('laundry_id', auth('laundry')->id())
            ->first();

        if ($balance != null) {
            $balance = round($balance->balance, 1);
        } else {
            $balance = 0;
        }

        $balanceToday = $this->todaybalance();

// Check if $branches array has elements
        if ($request->branch_id) {
            Session::put('branch_id', $request->branch_id);
        }

        $branch_id = $request->branch_id ? $request->branch_id : Session::get('branch_id');

        if (Session::get('branch_id') == null) {
            if (count($branches) > 0) { // Check if the array is not empty
                $branch_id = $branches[0]->id;
            } else {
                // Set a default branch or handle the case when there are no branches
                $branch_id = null; // or any other appropriate value
            }
        }

        $orders = null;

        if (isset($branch_id)) {
            $todayOrders = $this->orders($branch_id, 'today');
            $inProgress = $this->orders($branch_id, 'inprogress');
            $orders = $this->orderDetails($branch_id, $date, $request);
        } else {
            if (isset($branches[0]->id)) {
                $todayOrders = $this->orders($branches[0]->id, 'today');
                $inProgress = $this->orders($branches[0]->id, 'inprogress');
                $orders = $this->orderDetails($branches[0]->id, $date, $request);
            }
        }

        return view('laundry.home.index', compact('orders', 'branches', 'todayOrders', 'inProgress', 'week', 'balance', 'balanceToday', 'branch_id'));

    }


    function todaybalance(){

        $branch_ids=branch::where('laundry_id',auth('laundry')->id())->pluck('id')->all();
        $todayprice=0;
        $enternancebalance=Transaction::selectRaw('sum(amount) as price')
        ->whereDate('created_at',Carbon::now())
        ->wherein('branch_id',$branch_ids)
        ->where('transaction','enternance')
        ->GroupBy('transaction')->first();
        $externalbalance=Transaction::selectRaw('sum(amount) as price')
        ->whereDate('created_at',Carbon::now())->where('transaction','externance')
        ->wherein('branch_id',$branch_ids)
        ->GroupBy('transaction')->first();
        if($enternancebalance!=null){
            $enternancebalance=$enternancebalance->price;
         }else{
             $enternancebalance=0;
         }
         if($externalbalance!=null){
            $externalbalance=$externalbalance->price;
         }else{
             $externalbalance=0;
         }
        $todayprice=$enternancebalance-$externalbalance;
        return $todayprice;

    }
}
