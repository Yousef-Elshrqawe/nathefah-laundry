<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Controller;
use App\Models\Laundry\{Laundry,branch};
use App\Traits\fileTrait;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Models\Subscription\LaundryPackage;

class SettingController extends Controller
{
    use fileTrait;

    public function index()
    {
        $laundryInfo  = Laundry::where('id',auth('laundry')->id())->first();

        $branches  = branch::where('laundry_id',auth('laundry')->id())->get();

        $laundrypackage = LaundryPackage::where('laundry_id',auth('laundry')->id())->first();

        return view('laundry.settings.index',compact('laundryInfo','branches','laundrypackage'));
    }

    public function updateInfo(Request $request,$id)
    {
        $laundry = Laundry::findOrFail($id);

        // if($laundry->logo!=null){
        if ($request->hasFile('logo')) {
            $logo = explode('/', $laundry->logo );
            $logo = $logo[6];
            $this->deleteFile($logo,'uploads/laundry/logos/');
            $laundry->logo =  $this->MoveImage($request->logo,'uploads/laundry/logos/');
        }

        $laundry->name = $request->name;
        $laundry->phone = $request->phone;
        $laundry->country_code = $request->country_code;
        $laundry->tax_number = $request->tax_number;
        $laundry->save();

        // $laundry->update([
        //     'name'         => $request->name,
        //     'logo'         => $request->logo,
        //     'phone'        => $request->phone,
        //     'country_code' => $request->country_code,
        // ]);

        Alert::success('success', 'Laundry updated successfully');
        return redirect()->back();
    }

}
