<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Laundry\Laundry;
use Auth;
use RealRashid\SweetAlert\Facades\Alert;

class TaxesController extends Controller
{
    //
    public function index(){

        $taxes=Laundry::select('taxes','taxamount')->find(auth('laundry')->id());
        return view('laundry.taxes.index',compact('taxes'));
    }
    public function update(Request $request){

        $taxes=Laundry::find(auth('laundry')->id())->update([
            'taxamount'=>$request->taxamount
         ]);
        Alert::success('success', 'taxes updated successfuly');
        return redirect()->back();
    }

    public function updatestatus(Request $request){
        $taxes=Laundry::find(auth('laundry')->id());
        if($taxes->taxes==true)
        {
            $taxes->update(['taxes'=>false]);
            return 'false';
        }else{
            $taxes->update(['taxes'=>true]);
            return 'true';
        }
    }
}
