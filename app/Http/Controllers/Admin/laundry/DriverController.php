<?php

namespace App\Http\Controllers\Admin\laundry;

use App\Http\Controllers\Controller;
use App\Http\Requests\DriverRequest;
use App\Models\Country;
use App\Models\Driver\Driver;
use App\Models\Laundry\branch;
use App\Models\Order\order;
use App\Models\Order\{OrderDriveryStatus,CancelOrder};
use App\Traits\GeneralTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class DriverController extends Controller
{

    use GeneralTrait;
    public function index(Request $request)
    {
        $branches   = branch::where('laundry_id',auth('laundry')->id())
                        ->get();

        if($branches->isEmpty()){
            Alert::error('no branchs', 'you have no branchs add branch first');
            return redirect()->back();
        }

        $branchId   =  $branches[0]->id;

        $drivers   = Driver::with('rate')->where('branch_id',$branchId)

                        ->when($request->search, function($q) use ($request,$branchId) {
                            $q->where('branch_id',$branchId)
                                ->where(function ($query) use ($request) {
                                    $query->where('name', 'like', '%' . $request->search . '%')
                                        ->orWhere('country_code', 'like', '%' . $request->search . '%')
                                        ->orWhere('phone', 'like', '%' . $request->search . '%')
                                        ->orWhere('email', 'like', '%' . $request->search . '%')
                                        ->orWhere('status', 'like', '%' . $request->search . '%')
                                        ->orWhere('classification', 'like', '%' . $request->search . '%');
                                });
                        })
                    ->get();

        $successDriverCount=OrderDriveryStatus::where(['confirmation'=>true])
        ->wherein('order_status',['drop_of_home','drop_of_laundry'])
        ->wherein('driver_id',$drivers->pluck('id'))
        ->count();

        $deliverdOrder=Order::where('progress','completed')
        ->wherein('delivery_type_id',[1,3,4])
        ->where('branch_id',$branchId)
        ->count();

        $canceledorders=CancelOrder::where('branch_id',$branchId)->count();

        $topDriver = DB::table("drivers")
                     ->select('rate','name')
                     ->where('drivers.branch_id',$branchId)
                     ->orderBy('rate','desc')
                     ->first();
        $countries = Country::all();

        return view('laundry.drivers.index',compact('branches','topDriver','drivers','successDriverCount','deliverdOrder','canceledorders','countries'));
    }




    public function getDriversInBranch(Request $request){

        $query = Driver::with('rate')
                ->with(['delivery_status' => function($q) {
                    $q->where('confirmation', true)
                     ->wherein('order_status',['drop_of_laundry','drop_of_home']);
                }])
                ->addSelect([
                    'orderscount' => OrderDriveryStatus::selectRaw('COUNT(*)')
                     ->whereColumn('order_delivery_status.driver_id', 'drivers.id')
                     ->wherein('order_status',['drop_of_laundry','drop_of_home'])
                   //  ->where('confirmation' ,true)
                     ->distinct('order_delivery_status.order_id')
                ])
                ->where('branch_id',$request->branchId);

    //    if($request->month){

    //        $query = $query->condition();
    //    }

        $drivers = $query->get();
        return $this->returnData('data',$drivers,'Drives in branch',200);
    }

    public function getTopDriversInBranch(Request $request){


            $branchId=$request->branchId;
            $drivers   = Driver::select('id')->where('branch_id',$branchId);

            $topDriver = DB::table("drivers")
            ->select('rate','name')
            ->where('drivers.branch_id',$branchId)
            ->orderBy('rate','desc')
            ->first();


            $successDriverCount=OrderDriveryStatus::where(['confirmation'=>true])
            ->wherein('order_status',['drop_of_home','drop_of_laundry'])
            ->wherein('driver_id',$drivers->pluck('id'))
            ->count();

            $deliverdOrder=Order::where('progress','completed')
            ->wherein('delivery_type_id',[1,3,4])
            ->where('branch_id',$branchId)
            ->count();

            $canceledorders=CancelOrder::where('branch_id',$branchId)->count();

            $data['topDriver']=$topDriver;
            $data['successDriverCount']=$successDriverCount;
            $data['deliverdOrder']=$deliverdOrder;


        return $this->returnData('data',$data,'Drives in branch',200);
    }


    public function driverDetails($id)
    {
        $driver = Driver::with(['orders' => function($query){
            $query->where('delivery_status','inprogress')->latest()->limit(10);
        }])
            ->addSelect([
                'successDriverCount' => Order::selectRaw('COUNT(*)')
                    ->whereColumn('orders.driver_id', 'drivers.id')
                    ->where('orders.delivery_status', "completed")
            ])->addSelect([
                'completedOrderToday' => Order::selectRaw('COUNT(*)')
                    ->whereColumn('orders.driver_id', 'drivers.id')
                    ->where('orders.delivery_status', "completed")
                    ->whereDate('created_at', Carbon::today())
            ])->addSelect([
                'completedOrderYesterday' => Order::selectRaw('COUNT(*)')
                    ->whereColumn('orders.driver_id', 'drivers.id')
                    ->where('orders.delivery_status', "completed")
                    ->whereDate('created_at', Carbon::yesterday())
            ])->addSelect([
                'allOrderToday' => Order::selectRaw('COUNT(*)')
                    ->whereColumn('orders.driver_id', 'drivers.id')
                    ->whereDate('created_at', Carbon::today())
            ])->addSelect([
                'allOrderYesterday' => Order::selectRaw('COUNT(*)')
                    ->whereColumn('orders.driver_id', 'drivers.id')
                    ->whereDate('created_at', Carbon::yesterday())
            ])
            ->where('id',$id)
            ->first();

        $complete    = $this->differanceCompleteDays($driver->completedOrderToday,$driver->completedOrderYesterday);

        $all         = $this->differanceCompleteDays($driver->allOrderToday,$driver->allOrderYesterday);

        $latestOrder = Order::where('driver_id',$id)->latest()->limit(10)->get();

        return view('laundry.drivers.drivers-details',compact('driver','complete','all','latestOrder'));
    }

    /**
     * @param $today
     * @param $yesterday
     * @return string
     */
    public function differanceCompleteDays($today,$yesterday)
    {
        $differance = $today - $yesterday;

        if(preg_match('/^[1-9]\d*$/',$differance)) {
            $order = "+" . $differance;
        }else{
            $order = "-" . $differance;
        }
        return $order;
    }



    public function store(DriverRequest $request)
    {


  /*      $country = Country::where('code',$request->country_code)->first();
        if($country->number_length != strlen($request->phone)){
            Alert::error('error', 'The number must be '.$country->number_length.' numbers');
            return redirect()->back();
        }*/

        Driver::create($request->validated() + ['country_code' => '+966']);
        Alert::success('success', 'Driver created successfully');
        return redirect()->back();

    }

    //selectSearch

    public function selectSearch(Request $request)
    {
        $movies = [];
        if($request->has('q')) {
            $search = $request->q;
            $movies = Country::select("code")
                ->where('code', 'LIKE', "%$search%")
                ->get();

        }
        return response()->json($movies);
    }

}
