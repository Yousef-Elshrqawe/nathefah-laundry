<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{Support,Setting};
use RealRashid\SweetAlert\Facades\Alert;

class SupportController extends Controller
{
    //

    public function index(){

       $setting=Setting::select('support_phone','country_code')->first();
       $Supports=Support::paginate(50);
       return view('Admin.Support.index',compact('Supports','setting'));
    }

    public function update(Request $request){

         $setting=Setting::first();
         $setting->support_phone=$request->phone;
         $setting->country_code=$request->country_code;
         $setting->save();

         Alert::success('success',   'change support phone success');
         return redirect()->back();

    }

   

}
