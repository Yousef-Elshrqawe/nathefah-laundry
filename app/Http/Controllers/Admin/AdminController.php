<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStore;
use App\Http\Requests\AdminUpdate;
use App\Models\Admin\Admin;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class AdminController extends Controller
{

    public function index(Request $request)
    {
        $rows = Admin::latest()->paginate(10);

        return view('Admin.admin.index', compact(['rows']));
    } // end of index


    public function store(AdminStore $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($request->password);
        Admin::create($data);
        return Redirect::back();
    } // end of store


    public function edit($id)
    {
        $row = Admin::findOrFail($id);
        return view('Admin.admin.edit', compact(['row']));
    } // end of edit



    public function update(AdminUpdate $request, $id)
    {
        $row = Admin::findOrFail($id);
        $data = $request->all();
        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data['password'] = $row->password;
        }
        $row->update($data);
        Alert::success('Success', 'Admin Updated Successfully');
        return  Redirect::route('Admin.admins');
    } // end of update


    public function destroy($id)
    {
        $row = Admin::findOrFail($id);
        $row->delete();
        Alert::success('Success', 'Admin Deleted Successfully');
        return Redirect::back();
    } // end of destroy


} // end of ItemController
