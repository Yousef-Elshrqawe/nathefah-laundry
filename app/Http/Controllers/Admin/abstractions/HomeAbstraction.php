<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Http\Controllers\Controller;
use App\Models\Order\order;
use Carbon\Carbon;
use App\Models\{Transaction,SystemTransaction};
use Illuminate\Support\Facades\DB;

class HomeAbstraction extends Controller
{
    public function balancetoday($branch_id,$date)
    {
            $enternancebalance=Transaction::selectRaw('sum(amount) as price')
            ->where('branch_id',$branch_id)
            ->whereDate('created_at',now()->today())->where('transaction','enternance')
            ->GroupBy('transaction')->first();


            $externalbalance=Transaction::selectRaw('sum(amount) as price')
            ->where('branch_id',$branch_id)
            ->whereDate('created_at',now()->today())->where('transaction','externance')
            ->GroupBy('transaction')->first();

            if($enternancebalance!=null){
                $enternancebalance=$enternancebalance->price;
             }else{
                 $enternancebalance=0;
             }
             if($externalbalance!=null){
                $externalbalance=$externalbalance->price;
             }else{
                 $externalbalance=0;
             }
            $todayprice=$enternancebalance-$externalbalance;
            return $todayprice;
    }

    public function balance($branch_id,$date){
        return   DB::table('branchs')
        ->selectRaw('balance')
        ->where('id',$branch_id)
        ->first()->balance;
    }

    public function orders($branch_id,$type)
    {

       if($type == 'today'){
       $order= order::where(['branch_id'=>$branch_id,'checked'=>1])->whereDate('created_at', now()->today())->count();
       }
       if($type == 'inprogress'){
        $order= order::where(['branch_id'=>$branch_id,'checked'=>1])->where('progress','inprogress')->count();
       }

        return $order;

    }

    public function orderDetails($branch_id, $date, $request)
    {
    
        return order::where(['branch_id'=>$branch_id,'checked'=>1])
            ->where('checked',true)
            ->where('created_at','>',$date)
            ->with('deliverytype','paymenttype')
            ->when($request->search, function($q) use ($request,$branch_id,$date) {
                $q->where(['branch_id'=>$branch_id,'checked'=>1])
                    ->where('checked',true)
                    ->where('created_at','>',$date)
                    ->where(function ($query) use ($request) {
                        $query
                            //->where('status', 'like', '%' . $request->search . '%')
                            ->Where('id',$request->search)
                            //->orWhere('progress', 'like', '%' . $request->search . '%')
                            //->orWhere('delivery_status', 'like', '%' . $request->search . '%')
                            ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                            ->orWhere('customer_phone', 'like', '%' . $request->search . '%')
                            ->orWhere('customer_location', 'like', '%' . $request->search . '%');
                            //->orWhere('confirmation', 'like', '%' . $request->search . '%')
                            //->orWhere('rate', 'like', '%' . $request->search . '%');
                    });
            })
            ->latest()
            ->take(25)
            ->get();
    }
}
