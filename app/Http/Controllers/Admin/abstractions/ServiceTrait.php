<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Models\Laundry\branchservice;
use App\Models\laundryservice\Additionalservice;
use App\Models\laundryservice\branchAdditionalservice;
use App\Models\laundryservice\Service;
use App\Models\Order\order;

trait ServiceTrait
{
    public function serviceIds($branch_id)
    {
        return branchservice::select('service_id')->where('branch_id',$branch_id)->pluck('service_id')->all();
    }

    public function services($branch_id)
    {
        return Service::withTranslation()->with(['branchservice'=>function($q)use($branch_id){
            $q->where('branch_id',$branch_id)->get();
        },'categories'=>function($q)use($branch_id){
            $q->with('branchitems',function($q)use($branch_id){
                $q->with('branchitemprice','item')->where('branch_id',$branch_id)->get();
            })->get();
        },'categories.items'])
            ->get();
    }

    public function additionalServicesIds($branch_id)
    {
        return branchAdditionalservice::select('additionalservice_id')
            ->where('branch_id',$branch_id)
            ->pluck('additionalservice_id')
            ->all();
    }

    public function additionalServices($branch_id)
    {
        return Additionalservice::with(['categories'=>function($q)use($branch_id){
            $q->with('branchitems',function($q)use($branch_id){
                $q->with('itemadditionalservice')->where('branch_id',$branch_id)->get();
                $q->with('branchitemprice',function($q){
                    $q->where('additionalservice_id','!=',null)->get();
                })->where('branch_id',$branch_id)->get();
            });
        }])->get();
    }

    public function latestOrders($branch_id,$date)
    {
        return order::where(['branch_id'=>$branch_id,'checked'=>1])
            ->with('deliverytype','paymenttype')
            ->when($date, function ($query) use ($date) {
                $query->where('created_at','>',$date);
            })
            ->latest()->take(25)->get();
    }
}
