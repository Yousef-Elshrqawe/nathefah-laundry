<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Http\Controllers\Controller;
use App\Models\Laundry\branch;
use App\Models\Laundry\Branchitem;
use App\Models\Laundry\BranchitemTranslation;
use App\Models\Laundry\branchservice;
use App\Models\laundryservice\branchAdditionalservice;
use App\Models\laundryservice\ItemTranslation;
use App\Models\laundryservice\Service;
use App\Models\laundryservice\Serviceitemprice;
use App\Models\Order\order;
use Illuminate\Support\Facades\DB;

class ServiceAbstractions extends Controller
{
    use ServiceTrait;

    public function argentPrice($branch_id)
    {
       return  Branchitem::where('branch_id',$branch_id)->get();
    }

    public function reports($branch_id)
    {
        return DB::table('orders')
            ->select(DB::raw(" (SELECT count(*) from orders WHERE orders.branch_id = $branch_id And orders.progress='completed' And orders.delivery_type_id != 2) allOrders"))
            ->addSelect([
                'completedOrders' => Order::selectRaw('COUNT(*)')
                    ->where('orders.branch_id', $branch_id)
                    ->where('orders.progress', "completed")
            ])
            ->addSelect([
                'inProgressOrders' => Order::selectRaw('COUNT(*)')
                    ->where('orders.branch_id', $branch_id)
                    ->where('orders.progress', "inprogress")
            ])
            ->first();
    }


    public function servicesByServiceId($branch_id,$services_id)
    {
        return Service::withTranslation()->with('categories',function($q)use($branch_id){
            $q->with('branchitems',function($q)use($branch_id){
                $q->with('branchitemprice','item')->where('branch_id',$branch_id)->get();
            })->get();
        })->wherein('id',$services_id)->get();
    }

    public function updateService($request)
    {
        $items=[];
        $item_prices=[];
        $keys=[];
        $item_price_ids=[];
        foreach ($request->count as $key=>$item){
            if($item!=0){
                array_push($items,$item);
                array_push($keys,$key);
            }
        }
        foreach($keys as $key=>$item){
            $item_prices[$key]['item_count']=$request->count[$item];
            $item_price[$key]['id']=$request->itempriceid[$item];
            $item_price_ids[]=$request->itempriceid[$item];
            //array_push($item_prices,$request->itempriceid[$key]);
        }
        $Serviceitemprices=Serviceitemprice::wherein('id',$item_price_ids)->get();

        foreach($Serviceitemprices as $key=>$Serviceitemprice){
            $item_prices[$key]['item_count']*$Serviceitemprice->price;
            $Serviceitemprice->update([
                'price'=>$item_prices[$key]['item_count'],
            ]);
        }
    }

    public function getServiceItem($id)
    {
       return  Branchitem::with('branchitemtranslation')->find($id);
    }

    public function editServiceItem($request)
    {
        $request->validate([
            'ar.name'=>'required',
            'en.name'=>'required',
        ]);
        $Branchitem = Branchitem::with('branchitemtranslation')->find($request->id);
        $Branchitem->update($request->all());
    }

    public function storeItem($request,$branchid)
    {
        $items=[];
        $item_prices=[];
        $keys=[];
        $item_price_ids=[];
        // end create branch item
        if($request->type=='service'){
            $service_type='service_id';
        }else{
            $service_type='additionalservice_id';
        }
        foreach ($request->count as $key=>$item){
            if($item!=0){
                array_push($items,$item);
                array_push($keys,$key);
            }
        }
        DB::beginTransaction();
        foreach($keys as $key){
            $branchservice=Serviceitemprice::where(['branch_id'=>$branchid,$service_type=>$request->service_id[$key],'item_id'=>$request->item_id[$key],])->first();
            if($branchservice!=null){
                $branchservice->update([
                    'price'=>$request->count[$key],
                ]);
            }else{
                // create branch item
                $branchitem=Branchitem::where(['branch_id'=>$branchid,'item_id'=>$request->item_id[$key]])->first();
                if($branchitem==null){
                    $branchitem=Branchitem::create([
                        'branch_id'=>$branchid,
                        'category_id'=>$request->category_id[$key],
                        'item_id'=>$request->item_id[$key],
                    ]);
                    $itemtranslations=ItemTranslation::where('item_id',$request->item_id[$key])->get();
                    foreach($itemtranslations as $itemtranslation){
                        BranchitemTranslation::create([
                            'branchitem_id'=>$branchitem->id,
                            'locale'=>$itemtranslation->locale,
                            'name'=>$itemtranslation->name,
                        ]);
                    }
                }
                Serviceitemprice::create([
                    'branch_id'=>$branchid,
                    'category_id'=>$request->category_id[$key],
                    $service_type=>$request->service_id[$key],
                    'price'=>$request->count[$key],
                    'branchitem_id'=>$branchitem->id,
                    'item_id'=>$request->item_id[$key],
                ]);
                if($request->type=='service'){
                    $branchservice=branchservice::where(['branch_id'=>$branchid,'service_id'=>$request->service_id[$key]])->first();
                    if($branchservice==null){
                        branchservice::create([
                            'branch_id'=>$branchid,
                            'service_id'=>$request->service_id[$key],
                        ]);
                    }
                }else{
                    $branchAdditionalservice=  branchAdditionalservice::where(['branch_id'=>$branchid,'additionalservice_id'=>$request->service_id[$key],'branchitem_id'=>$branchitem->id])->first();
                    if($branchAdditionalservice==null){
                        branchAdditionalservice::create([
                            'branch_id'=>$branchid,
                            'additionalservice_id'=>$request->service_id[$key],
                            'branchitem_id'=>$branchitem->id,
                        ]);
                    }
                }
            }
        }
        DB::commit();
    }


    public function changeServiceStatus($request)
    {
        $service = branchservice::findOrFail($request->id);

        $service->update([
            'status' => $service->status == "on" ? "off" : "on"
        ]);

       return $service;
    }

    public function changeItemServiceStatus($request)
    {
        $service = branchAdditionalservice::findOrFail($request->id);
        $service->update([
            'status' => $service->status == "on" ? "off" : "on"
        ]);
        return $service;
    }

    public function changeItemArgentPrice($request)
    {
        Branchitem::where('id',$request->id)->update([
            'argentprice' => $request->argentprice
        ]);
    }

    public function Branches($laundry_id)
    {
        //auth('laundry')->id()
        return branch::where('laundry_id',$laundry_id)->get();
    }

    public function branchDetails($branch_id)
    {
        return branch::where('id',$branch_id)->first();
    }


    //

    public function branchservices($branch_id){
        return branchservice::where('branch_id',$branch_id)->count();
    }

    public function branchadditionalservices($branch_id){
        return branchAdditionalservice::where('branch_id',$branch_id)->GroupBy('branchitem_id')->count();
    }

}
