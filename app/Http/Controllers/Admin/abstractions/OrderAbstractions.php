<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Http\Controllers\Controller;
use App\Models\ApplicationRates;
use App\Models\Transaction;
use App\Models\Laundry\{branch,Laundry};
use App\Models\Laundry\{branchservice,Branchitem};
use App\Models\laundryservice\{Service,Argent};
use App\Models\laundryservice\Serviceitemprice;
use App\Models\Order\{order,OrderDriveryStatus,orderdetailes,Orderreview};
use App\Interfaces\BranchRepositoryInterface;
use App\Interfaces\OrderRepositoryInterface;



class OrderAbstractions extends Controller
{
    public $BranchRepository;
    public function __construct(BranchRepositoryInterface $BranchRepository,OrderRepositoryInterface $OrderRepository)
    {
        $this->BranchRepository = $BranchRepository;
        $this->OrderRepository  =$OrderRepository;
    }

    public function inDeliveryOrder($branch_id,$orderDate,$date,$request)
    {
        if(is_array($branch_id)){
            return order::with('deliverytype','OrderDriveryStatus')
            ->where('orders.delivery_status','inprogress')->wherein('branch_id',$branch_id)
                //user_id
                    ->when($request->user_id, function($q) use ($request) {
                        return $q->where('user_id',$request->user_id);
                    })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
        }
        return order::with('deliverytype','OrderDriveryStatus')
            ->where('orders.delivery_status','inprogress')
            ->where('branch_id',$branch_id)
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
    }

    public function inProgressCompletedOrder($branch_id,$orderDate,$date,$type,$request)
    {
        if(is_array($branch_id)){
            return order::with('deliverytype')
            ->where('progress',$type)
            ->wherein('branch_id',$branch_id)
                ->when($request->user_id, function($q) use ($request) {
                    return $q->where('user_id',$request->user_id);
                })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
        }

        return order::with('deliverytype')
            ->where('progress',$type)
            ->where('branch_id',$branch_id)
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
    }

    public function unassigned($branch_id,$orderDate,$date,$request)
    {
        if(is_array($branch_id)){
            return order::with('deliverytype')
            ->where(['confirmation'=>'accepted','driver_id'=>null])->wherein('branch_id',$branch_id)
            ->where('order_delivery_status',true)
            ->where('orders.progress','!=','inprogress')
            ->wherein('delivery_type_id',[1,4,3])
                ->when($request->user_id, function($q) use ($request) {
                    return $q->where('user_id',$request->user_id);
                })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->orderby('created_at','desc')
            ->paginate(25);
        }
        return order::with('deliverytype')
            ->where(['confirmation'=>'accepted','branch_id'=>$branch_id,'driver_id'=>null])
            ->where('order_delivery_status',true)
            ->where('orders.progress','!=','inprogress')
            ->wherein('delivery_type_id',[1,4,3])
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->orderby('created_at','desc')
            ->paginate(25);
    }

    public function one_way_delivery($branch_id,$orderDate,$date,$request)
    {
        if(is_array($branch_id)){
            return order::with('deliverytype')
            ->where(['confirmation'=>'accepted','driver_id'=>null])->wherein('branch_id',$branch_id)
            ->wherein('progress',['finished','no_progress'])
            ->wherein('delivery_type_id',[3,4])
                ->when($request->user_id, function($q) use ($request) {
                    return $q->where('user_id',$request->user_id);
                })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
        }
        return order::with('deliverytype')
            ->where(['confirmation'=>'accepted','branch_id'=>$branch_id,'driver_id'=>null])
            ->wherein('progress',['finished','no_progress'])
            ->wherein('delivery_type_id',[3,4])
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
    }

    public function self_delivery_orders($branch_id,$orderDate,$date,$request){
        if(is_array($branch_id)){
            return order::with('deliverytype')
            ->where(['confirmation'=>'accepted','driver_id'=>null])->wherein('branch_id',$branch_id)
            ->wherein('progress',['finished','no_progress'])
            ->wherein('delivery_type_id',[2])
                ->when($request->user_id, function($q) use ($request) {
                    return $q->where('user_id',$request->user_id);
                })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
        }
        return order::with('deliverytype')
            ->where(['confirmation'=>'accepted','branch_id'=>$branch_id,'driver_id'=>null])
            ->wherein('progress',['finished','no_progress'])
            ->wherein('delivery_type_id',[2])
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
    }


    public function newOrder($branch_id,$orderDate,$date,$request)
    {
        if(is_array($branch_id)){
            return order::with('deliverytype')->where(['confirmation'=>'pending','checked'=>true])
            ->wherein('branch_id',$branch_id)
                ->when($request->user_id, function($q) use ($request) {
                    return $q->where('user_id',$request->user_id);
                })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
        }
        return order::with('deliverytype')->where(['confirmation'=>'pending','branch_id'=>$branch_id,'checked'=>true])
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
    }

    public function allOrders($branch_id,$orderDate,$date,$request)
    {

        if(is_array($branch_id)){
            return order::with('OrderDriveryStatus')
                ->where('checked',true)->wherein('branch_id',$branch_id)
                ->when($request->user_id, function($q) use ($request) {
                    return $q->where('user_id',$request->user_id);
                })
                ->when($orderDate!='all',function($q)use($date){
                    return  $q->where('created_at','>',$date);
                })->when($request->search, function($q) use ($request,$branch_id) {
                    return
                        $q->where('id',$request->search)
                            ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                            ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
                })
                ->latest()
                ->paginate(25);
        }
        return order::with('OrderDriveryStatus')
            ->where(['branch_id'=>$branch_id,'checked'=>true])
            ->when($request->user_id, function($q) use ($request) {
                return $q->where('user_id',$request->user_id);
            })
            ->when($orderDate!='all',function($q)use($date){
                return  $q->where('created_at','>',$date);
            })
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(25);
    }

    public function latestOrders($branch_id,$date)
    {
        if(is_array($branch_id)){
            return order::where('checked',1)
            ->with('deliverytype','paymenttype')->wherein('branch_id',$branch_id)
            ->when($date, function ($query) use ($date) {
                $query->where('created_at','>',$date);
            })
            ->latest()->take(25)->get();
        }
        return order::where(['branch_id'=>$branch_id,'checked'=>1])
            ->with('deliverytype','paymenttype')
            ->when($date, function ($query) use ($date) {
                $query->where('created_at','>',$date);
            })
            ->latest()->take(25)->get();
    }


    public function rejectedorder($branchid){
        if(is_array($branchid)){
            return order::where(['checked'=>1,'confirmation'=>'refused'])->wherein('branch_id',$branchid)->get();

        }
        return order::where(['branch_id'=>$branchid,'checked'=>1,'confirmation'=>'refused'])->get();
    }


    public function serviceIds($branch_id)
    {
        return branchservice::select('service_id')->where('branch_id',$branch_id)->pluck('service_id')->all();
    }

    public function getServices($branch_id)
    {
        $services_id = $this->serviceIds($branch_id);

        return Service::withTranslation()->with(['categories'=>function($q)use($branch_id){
            $q->with('branchitems',function($q)use($branch_id){
                $q->with('branchitemprice','item')->where('branch_id',$branch_id)->get();
            })->get();
        },'categories.additionalservice'])->wherein('id',$services_id)->get();
    }

    public function reports($branch_id,$reviewDate)
    {
        return Orderreview::where('branch_id',$branch_id)
            ->when($reviewDate!='all',function($q)use($reviewDate){
                return  $q->where('created_at','>',$reviewDate);
            })
            ->latest()->with('user')->paginate(20);
    }

    public function Branches($laundry_id)
    {
        if($laundry_id=='All'){
            return branch::all();
        }
        return branch::where('laundry_id',$laundry_id)->get();
    }


    public function submitOrder($request,$branchid)
    {
        $branch=branch::find($branchid);
        //dd($branch);
        // start create main service
        $items=[];
        $item_prices=[];
        $keys=[];
        $item_price_ids=[];
        foreach ($request->count as $key=>$item){
            if($item!=0){
                array_push($items,$item);
                array_push($keys,$key);
            }
        }
        foreach($keys as $key=>$item){
            $item_prices[$key]['item_count']=$request->count[$item];
            $item_price[$key]['id']=$request->itempriceid[$item];
            $item_price_ids[]=$request->itempriceid[$item];
            //array_push($item_prices,$request->itempriceid[$key]);
        }

        $Serviceitemprices=Serviceitemprice::wherein('id',$item_price_ids)->get();
        $price=0;
        $itemcount=0;
        foreach($Serviceitemprices as $key=>$Serviceitemprice){
            $price+=$item_prices[$key]['item_count']*$Serviceitemprice->price;
            $itemcount+=$item_prices[$key]['item_count'];
        }

        //discount

        // end prepare
        $order = $this->createOrder($request,$branchid,$price);
        foreach($Serviceitemprices as $key=>$Serviceitemprice){
            $price=$item_prices[$key]['item_count']*$Serviceitemprice->price;
            $this->createOrderDetails($order,$Serviceitemprice,$item_prices[$key]);
        }
        // additionale service
        if($request->additional_ids!=null)
            $addtionalservice=$this->createadditionalservice($request,$order);
        // add urgent
        $urgent=$this->createurgent($request,$order);

        // calculate taxes
        // calculate taxes
        $this->OrderRepository->addordertaxes($order,$branchid,$order->price_before_discount);


        // calaculate distance
        $distance=0;
        if($request->lat!=null)
            $distance= $this->distance($request->lat,$request->long,$branch->lat,$request->long,'k');



        // خصم  نسبه  التطبيق  من  المغسله
        $branch = Branch::find($order->branch_id);
        $app_percentage = ApplicationRates::first();

        $branch->update([
            'balance' => $branch->balance - ($price * ($app_percentage->rate / 100)),
        ]);

        $fort_id = null;
        if ($order->fort_id != null) ;
        $fort_id = $order->fort_id;

        $Transaction = Transaction::create([
            // هنا يتم حساب نسبه التطبيق
            'amount' => ($price * ($app_percentage->rate / 100)),
            'order_id' => $order->id,
            'branch_id' => $order->branch_id,

            // هنا  بيتم عرض الرصيد اللي المفوض هيبقي معاه بعد ميخلص الطلب كامل
            'branch_balance' => $branch->balance,
            'system_balance' => ($price * ($app_percentage->rate / 100)),
            'application_percentage' => ($price * ($app_percentage->rate / 100)),

            'pending' => 0,
            'type' => 'cache',
            'transaction' => 'externance',
            'transaction_number' => $fort_id,
            'transaction_type' => 'order',
            'payment_type' => null
        ]);




        return [$order,$itemcount,$distance];

    }

    public function createOrder($request,$branchid,$price)
    {
        //$delivery_fees=$this->deliveryfees($request,$branchid);


        return  order::create([
            'branch_id'=>$branchid,
            'customer_name'=>$request->customer_name,
            'customer_phone'=>$request->customer_phone,
            'customer_location'=>$request->location,
            'order_by'=>'laundry',
            'price_before_discount'=>$price,
            'confirmation'=>'accepted',
            'discount_type'=>$request->discount_type == null ? 2 : $request->discount_type,
            'discount_value'=>$request->discount_value == null ? 0 : $request->discount_value,

            // 'driver_id'=>1,
            // 'urgent'=>$urgent
        ]);
    }

    public function createOrderDetails($order,$Serviceitemprice,$item_prices)
    {
        orderdetailes::create([
            'order_id'=>$order->id,
            'branchitem_id'=>$Serviceitemprice->branchitem_id,
            'price'=> $Serviceitemprice->price,
            'service_id'=>$Serviceitemprice->service_id,
            'quantity'=>$item_prices['item_count'],
            //'note'=>$serviceprice['note']
        ]);
    }


    public function createadditionalservice($request,$order){
        $items=[];
        $item_prices=[];
        $keys=[];
        $item_price_ids=[];
        $price=$order->price_before_discount;
        foreach($request->additionalcount as $key=>$item){
            if($item!=0){
                array_push($items,$item);
                array_push($keys,$key);
            }
        }
        foreach($keys as $key=>$item){
            $item_prices[$key]['item_count']=$request->additionalcount[$item];
            $Serviceitemprice=Serviceitemprice::find($request->additional_ids[$item]);
            $price+=$Serviceitemprice->price*$request->additionalcount[$item];
            $this->createOrderDetails($order,$Serviceitemprice,$item_prices[$key]);
        }
        $order->price_before_discount=$price;
        $order->save();
    }

    public function createurgent($request,$order){
        $items=[];
        $item_prices=[];
        $keys=[];
        $item_price_ids=[];
        $price=$order->price_before_discount;
        foreach($request->argent as $key=>$item){
            if($item!=0){
                array_push($items,$item);
                array_push($keys,$key);
            }
        }
        foreach($keys as $key=>$item){
            $branchitem= Branchitem::find($request->argent_ids[$item]);
            $price+= $branchitem->argentprice*$request->argent[$item];
            $Serviceitemprice=Serviceitemprice::find($request->itempriceid[$item]);
            Argent::create([
                'service_id'=>$Serviceitemprice->service_id,
                'branchitem_id'=>$Serviceitemprice->branchitem_id,
                'order_id'=>$order->id,
                'price'=>$branchitem->argentprice*$request->argent[$item],
                'quantity'=>$request->argent[$item]
            ]);
        }
        $order->price_before_discount=$price;
        $order->save();
    }

    public function viewAllOrders($branch_id,$request)
    {
        return order::where(['branch_id'=> $branch_id,'checked'=>1])
            ->with('deliverytype','paymenttype')
            ->when($request->search, function($q) use ($request,$branch_id) {
                return
                    $q->where('id',(int)$request->search)
                        ->orWhere('customer_name', 'like', '%' . $request->search . '%')
                        ->orWhere('customer_phone', 'like', '%' . $request->search . '%');
            })
            ->latest()
            ->paginate(50);
    }



    public function deliver($id){
        $order=order::find($id);
        $orderstatus=OrderDriveryStatus::where('order_id',$id)->latest('id')->first();
        $this->OrderRepository->reciveorder_from_laundry($order,$orderstatus);
    }



    // public function deliveryfees($request,$branchid){
    //     $laundry_id=branch::find($branchid);
    //     $laundry=Laundry::find($laundry_id);
    //     Laundry::find();
    // }

    function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        }
        else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    } // end of distance

}
