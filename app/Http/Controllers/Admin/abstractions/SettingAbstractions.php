<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Http\Controllers\Controller;
use App\Models\Laundry\branch;
use App\Models\Order\order;
use App\Models\Order\Orderreview;
use Carbon\Carbon;

class SettingAbstractions extends Controller
{
    use ServiceTrait;

    public function branchInfo($branchId,$request)
    {
        return branch::with('users')
            ->where('id',$branchId)
            ->when($request->search, function($q) use ($request,$branchId) {
                $q->with(['users'=>function($q) use ($request,$branchId) {
                    $q->where('branch_id',$branchId)
                        ->where(function ($query) use ($request) {
                            $query->where('username', 'like', '%' . $request->search . '%')
                                ->orWhere('country_code', 'like', '%' . $request->search . '%')
                                ->orWhere('phone', 'like', '%' . $request->search . '%')
                                ->orWhere('email', 'like', '%' . $request->search . '%');
                        });
                }]);
            })
            ->first();
    }

    public function successOrders($branch_id,$date)
    {
        return order::where('progress','completed')
            ->where('branch_id',$branch_id)
            ->when($date, function ($query) use ($date) {
                $query->whereDate('created_at', Carbon::today());
            })
            ->count();
    }

    public function allTodayOrders($branch_id)
    {
        return order::where(['branch_id'=>$branch_id,'checked'=>true])
            ->whereDate('created_at', Carbon::today())
            ->count();
    }

    public function updateBranchInfo($id,$request)
    {
        $branch = branch::findOrFail($id);
        $branch->update([
            'username'     => $request->username,
            'phone'        => $request->phone,
            'country_code' => $request->country_code,
            'address'      => $request->address,
            'status'       => $request->status,
            'open_time'    => $request->open_time,
            'closed_time'  => $request->closed_time,
        ]);
    }


    public function orderReports($branch_id)
    {
        $reviewDate = 0;
        return Orderreview::where('branch_id',$branch_id)
            ->when($reviewDate!='all',function($q)use($reviewDate){
                return  $q->where('created_at','>',$reviewDate);
            })->latest()->with('user')->paginate(20);
    }
}
