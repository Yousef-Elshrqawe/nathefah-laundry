<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Models\Balance\Balance;
use App\Models\System\Systeminfo;
use Carbon\Carbon;
use App\Models\{Transaction,SystemTransaction};

trait balanceAbstraction
{
    /**
     * @param $laundry
     * @param $request
     */
    function transaction($type, $date, $search_key = null) {
        $transactions = SystemTransaction::with('branch.laundry', 'laundry', 'order')
            ->orderBy('created_at', 'desc')
            ->when($type != 'All', function ($q) use ($type) {
                return $q->where('transaction_type', $type);
            })
            ->when($date != 'All', function ($q) use ($date) {
                return $q->where('created_at', '>=', Carbon::now()->subDays((int) $date));
            })
            ->when($search_key != null, function ($q) use ($search_key) {
                return $q->where(function ($query) use ($search_key) {
                    $query->where('transaction_number', $search_key)
                        ->orWhere('amount', $search_key);
                });
            });

        return $transactions;
    }

    function todaybalance(){
        $todayprice=0;
        $enternancebalance=Transaction::selectRaw('sum(amount) as price')
        ->whereDate('created_at',Carbon::now())->where('transaction','enternance')
        ->GroupBy('transaction')->first();
        $externalbalance=Transaction::selectRaw('sum(amount) as price')
        ->whereDate('created_at',Carbon::now())->where('transaction','externance')
        ->GroupBy('transaction')->first();
        if($enternancebalance!=null){
            $enternancebalance=$enternancebalance->price;
         }else{
             $enternancebalance=0;
         }
         if($externalbalance!=null){
            $externalbalance=$externalbalance->price;
         }else{
             $externalbalance=0;
         }
        $todayprice=$enternancebalance-$externalbalance;
        return $todayprice;
    }
}
