<?php


namespace App\Http\Controllers\Admin\abstractions;


use App\Models\Subscription\LaundryPackage;
use App\Models\Subscription\Package;
// use Carbon\Subscription\Carbon;
use Carbon\Carbon;

trait PackageTrait
{
    /**
     * @param $laundry
     * @param $request
     */
    public function addPackage($laundry,$request)
    {
        $end_date = Carbon::now();
        $package = Package::with('period')->where('id',$request->package)->first();


        switch ($package->period->duration_unit) {
            case 'day':
                $end_date->addDays($package->period->duration_value);
                break;
            case 'week':
                $end_date->addWeeks($package->period->duration_value);
                break;
            case 'month':
                $end_date->addMonths($package->period->duration_value);
                break;
            case 'year':
                $end_date->addYears($package->period->duration_value);
                break;
        }

        LaundryPackage::create([
            'laundry_id'  => $laundry->id,
            'package_id'  => $package->id,
            'start_date'  => Carbon::now(),
            'end_date'    => $end_date,
        ]);
    }
}
