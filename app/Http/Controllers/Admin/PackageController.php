<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PackageRequest;
use App\Http\Resources\PackageResource;
use App\Interfaces\PackageRepositoryInterface;
use App\Models\Subscription\Package;
use App\Models\Subscription\Period;
use App\Traits\GeneralTrait;
use Exception;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use App\Traits\response;


class PackageController extends Controller
{
    use GeneralTrait;
    use response;
    protected $modelRepository;

    public function __construct(PackageRepositoryInterface $modelRepository)
    {
        $this->modelRepository = $modelRepository;
    }

    public function index()
    {
        $packages = $this->modelRepository->index();
        $periods  = Period::all();
        return view('Admin.packages.index',compact('packages','periods'));
    }

    public function store(PackageRequest $request)
    {
        try {
            $this->modelRepository->store($request);

            Alert::success('success', 'Package created successfully');

            return redirect()->back();

        } catch (Exception $e) {
            return redirect()->withErrors($e);
        }
    }

    public function update(PackageRequest $request)
    {
        try {
            $this->modelRepository->update($request);

            Alert::success('success', 'Package updated successfully');

            return redirect()->back();

        } catch (Exception $e) {
            return redirect()->withErrors($e);
        }
    }

    public function destroy($package_id)
    {
        try {
            $this->modelRepository->destroy($package_id);

            Alert::success('success', 'Package deleted successfully');

            return redirect()->back();


        } catch (Exception $e) {
            return redirect()->withErrors($e);
        }
    }

    public function getPackage($period_id , $package_id = null)
    {
        $packages  = $this->modelRepository->getPackage($period_id , $package_id);
        $data['packages']=$packages;
        //return $this->returnData('data',PackageResource::collection($data),'Packages',200);
        return $this->response(true,'get packages successfully',$data);
    }

    public function getPackageAjax($period_id)
    {
        $packages = Package::where('period_id',$period_id)->get();
        return response()->json($packages);
    }

}
