<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendNotifications;
use App\Interfaces\NotificationRepositoryinterface;
use App\Jobs\SendNotificationJob;
use App\Models\Driver\Driver;
use App\Models\Laundry\Branchuser;
use App\Models\User;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class SendNotificationsController extends Controller
{

    public function __construct(NotificationRepositoryinterface $NotificationRepository)
    {$this->NotificationRepository = $NotificationRepository;}


    public function index(Request $request)
    {
        return view('Admin.sendNotifications.index');
    } // end of index

    //create
    public function create($type)
    {
        if ($type == 'user') {
            $rows = User::latest()->get();
            return view('Admin.sendNotifications.create', compact('rows' , 'type'));
        } elseif ($type == 'driver') {
            $rows = Driver::latest()->get();
            return view('Admin.sendNotifications.create', compact('rows' , 'type'));
        } elseif ($type == 'branch') {
            $rows = Branchuser::latest()->get();
            return view('Admin.sendNotifications.create', compact('rows' , 'type'));
        }
    } // end of create

/*    //store
    public function store(SendNotifications $request)
    {


        $titlesData = ['ar' => $request->title_ar, 'en' => $request->title_en];
        $bodysData = ['ar' => $request->body_ar, 'en' => $request->body_en];


        $title = app()->getLocale() == 'ar' ? $request->title_ar : $request->title_en;
        $body = app()->getLocale() == 'ar' ? $request->body_ar : $request->body_en;

        if ($request->type == 'user' && $request->user_id == 'all') {
            $users = User::all();
            foreach ($users as $user) {
                $this->NotificationRepository->sendnotification('user', $user->id, $title, $body, 1);
                $this->NotificationRepository->createnotification('user', $user->id, $titlesData, $bodysData);
            }
        } elseif ($request->type == 'user' && $request->user_id != 'all') {
            $this->NotificationRepository->sendnotification('user', $request->user_id, $title, $body, 1);
            $this->NotificationRepository->createnotification('user', $request->user_id, $titlesData, $bodysData);
        }

        if ($request->type == 'driver' && $request->user_id == 'all') {
            $drivers = Driver::all();
            foreach ($drivers as $driver) {
                $this->NotificationRepository->sendnotification('driver', $driver->id, $title, $body, 1);
                $this->NotificationRepository->createnotification('driver', $driver->id, $titlesData, $bodysData);
            }
        } elseif ($request->type == 'driver' && $request->user_id != 'all') {
            $this->NotificationRepository->sendnotification('driver', $request->user_id, $title, $body, 1);
            $this->NotificationRepository->createnotification('driver', $request->user_id, $titlesData, $bodysData);
        }


        if ($request->type == 'branch' && $request->user_id == 'all') {
            $branches = Branchuser::all();
            foreach ($branches as $branch) {
                $this->NotificationRepository->sendnotification('branch', $branch->id, $title, $body, 1);
                $this->NotificationRepository->createnotification('branch', $branch->id, $titlesData, $bodysData);
            }
        } elseif ($request->type == 'branch' && $request->user_id != 'all') {
            $this->NotificationRepository->sendnotification('branch', $request->user_id, $title, $body, 1);
            $this->NotificationRepository->createnotification('branch', $request->user_id, $titlesData, $bodysData);
        }

        Alert::success('Success', 'Notification Sent Successfully');
        return redirect()->route('Admin.send-notifications');

    } // end of store*/




    public function store(SendNotifications $request)
    {
        $titlesData = [
            'ar' => $request->title_ar,
            'en' => $request->title_en
        ];
        $bodysData = [
            'ar' => $request->body_ar,
            'en' => $request->body_en
        ];

        $this->dispatchNotificationJobs($request->type, $request->user_id, $titlesData, $bodysData);

        Alert::success('Success', 'Notification Sent Successfully');
        return redirect()->route('Admin.send-notifications');
    }


    private function dispatchNotificationJobs($type, $userId, $titlesData, $bodysData)
    {
        $recipients = $this->getRecipientsByType($type, $userId);

        foreach ($recipients as $recipient) {
            $userLang = $recipient->lang ?? app()->getLocale();

            $title = $userLang === 'ar' ? $titlesData['ar'] : $titlesData['en'];
            $body  = $userLang === 'ar' ? $bodysData['ar'] : $bodysData['en'];

            SendNotificationJob::dispatch($type, $recipient->id, $title, $body, $titlesData, $bodysData);
        }
    }



    private function getRecipientsByType($type, $userId)
    {
        switch ($type) {
            case 'user':
                return $userId == 'all' ? User::all() : User::where('id', $userId)->get();
            case 'driver':
                return $userId == 'all' ? Driver::all() : Driver::where('id', $userId)->get();
            case 'branch':
                return $userId == 'all' ? Branchuser::all() : Branchuser::where('id', $userId)->get();
            default:
                throw new \InvalidArgumentException('Invalid notification type');
        }
    }



} // end of ItemController
