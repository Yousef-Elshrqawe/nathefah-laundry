<?php

namespace App\Http\Controllers\Admin\branch;
use App\Http\Requests\DriverRequest;
use Carbon\Carbon;
use App\Models\Order\{order,OrderDriveryStatus,CancelOrder};
use App\Models\Notification\{drivernotifytype};
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Driver\Driver;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Session;
use Auth;

class DriverController extends Controller
{
    ########################### START INDEX #########################
    protected function index(Request $request,$driverlist,$driverreport){

        $branch_id = Auth::guard('branch')->user()->branch_id;

        $data['driverlist']   =  $driverlist;
        $data['driverreport'] = $driverreport;

        $driverreportdate=0;  $driverlistdate=0;
        if($driverlist!='All')
        $driverlistdate   = \Carbon\Carbon::today()->subDays($driverlist*30);
        if($driverreport!='All')
        $driverreportdate = \Carbon\Carbon::today()->subDays($driverreport*30);

        Session::put('driverlistdate',$driverlistdate);

        $data['drivers'] = Driver::where('branch_id',$branch_id)

                            ->when($request->search, function($q) use ($request,$branch_id,$driverreportdate) {
                                $q->where('branch_id',$branch_id)
                                    ->where(function ($query) use ($request) {
                                        $query->where('name', 'like', '%' . $request->search . '%')
                                            ->orWhere('country_code', 'like', '%' . $request->search . '%')
                                            ->orWhere('phone', 'like', '%' . $request->search . '%')
                                            ->orWhere('email', 'like', '%' . $request->search . '%')
                                            ->orWhere('status', 'like', '%' . $request->search . '%')
                                            ->orWhere('classification', 'like', '%' . $request->search . '%');
                                    });
                            })
                            ->get();

        $indeliverycount = order::where('branch_id',$branch_id)->where('progress','indelivery')->where('created_at','>',$driverreportdate)->count();
        $data['indeliverycount']=$indeliverycount;

        // هنا بنجيب عدد التوصيلات للطلب ممكن الطلب يتم عليه عمليتين توصيل او عمليه واحده
        $Deliveryscount=OrderDriveryStatus::where(['confirmation'=>true])
        ->wherein('order_status',['drop_of_home','drop_of_laundry'])
        ->wherein('driver_id',$data['drivers']->pluck('id'))
        ->when($driverreport!='All',function($q)use($driverreportdate){
            $q->where('created_at','>',$driverreportdate);
        })
        ->count();
        $data['Deliveryscount']=$Deliveryscount;


        $canceledorders= order::where('branch_id',$branch_id)->where('confirmation','refused')->count();
        $data['canceledorders']=$canceledorders;




        $topdriver= DB::table("drivers")
        ->leftjoin('drivers_rate','drivers_rate.driver_id','=','drivers.id')
        ->select('drivers.rate','name')
        ->where('drivers.branch_id',$branch_id)
        ->orderBy('drivers.rate','desc')
        ->orderBy('drivers_rate.order_count','desc')
        ->first();

        //
        //->select('rate','name')->where('drivers.branch_id',$branch_id)->orderBy('rate','desc')->first();
         $data['topdriver']=$topdriver;




        return view('branch.drivers.index')->with( $data );
    }
    ###########################  END INDEX  #########################



    public function store(Request $request){
        $request->validate([
           'name'=>'required',
           'country_code'=>'nullable',
            'phone' => 'required|regex:/^0?5[0-9]{8}$/|unique:drivers,phone',
           'email'=>'unique:drivers'
        ],
        [
            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ]);
        $data=$request->all();
        $branch_id=Auth::guard('branch')->user()->branch_id;
        $data['branch_id']=$branch_id;
        $data['country_code']= '+966';
        $driver= Driver::create($data);

        for($i=1;$i<=3;$i++){
            if($i!=2)
            drivernotifytype::create([
               'driver_id'=>$driver->id,
               'notificationtype_id'=>$i
            ]);
        }


        Alert::success('success', 'Driver added successfully');
        return redirect()->back();

    }

    public function update(Request $request)
    {
        $validated = $request->validate([
            'name'         => 'required',
            'email'        => 'required',
            'phone'        => 'required|regex:/^0?5[0-9]{8}$/',
        ],[
            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ]);


        $driver = Driver::findOrFail($request->id);
        $driver->update([
            'name'         => $request->name,
            'email'        => $request->email,
            'phone'        => $request->phone
        ]);

        Alert::success('success', 'Driver updated successfully');

        return redirect()->back();
    }


    public function driverDetails($id)
    {
        $driver = Driver::with(['orders' => function($query){
                             $query->where('delivery_status','inprogress')->latest()->limit(10);
                        }])
                    ->addSelect([
                        'successDriverCount' => Order::selectRaw('COUNT(*)')
                            ->whereColumn('orders.driver_id', 'drivers.id')
                            ->where('orders.delivery_status', "completed")
                    ])->addSelect([
                        'completedOrderToday' => Order::selectRaw('COUNT(*)')
                            ->whereColumn('orders.driver_id', 'drivers.id')
                            ->where('orders.delivery_status', "completed")
                            ->whereDate('created_at', Carbon::today())
                    ])->addSelect([
                        'completedOrderYesterday' => Order::selectRaw('COUNT(*)')
                            ->whereColumn('orders.driver_id', 'drivers.id')
                            ->where('orders.delivery_status', "completed")
                            ->whereDate('created_at', Carbon::yesterday())
                    ])->addSelect([
                        'allOrderToday' => Order::selectRaw('COUNT(*)')
                            ->whereColumn('orders.driver_id', 'drivers.id')
                            ->whereDate('created_at', Carbon::today())
                    ])->addSelect([
                        'allOrderYesterday' => Order::selectRaw('COUNT(*)')
                            ->whereColumn('orders.driver_id', 'drivers.id')
                            ->whereDate('created_at', Carbon::yesterday())
                    ])
                    ->where('id',$id)
                    ->first();

        $complete    = $this->differanceCompleteDays($driver->completedOrderToday,$driver->completedOrderYesterday);

        $all         = $this->differanceCompleteDays($driver->allOrderToday,$driver->allOrderYesterday);

        $latestOrder = Order::where('driver_id',$id)->latest()->limit(10)->get();

        return view('branch.drivers.drivers-details',compact('driver','complete','all','latestOrder'));
    }

    /**
     * @param $today
     * @param $yesterday
     * @return string
     */
    public function differanceCompleteDays($today,$yesterday)
    {
        $differance = $today - $yesterday;

        if(preg_match('/^[1-9]\d*$/',$differance)) {
            $order = "+" . $differance;
        }else{
            $order = "-" . $differance;
        }
        return $order;
    }
}
