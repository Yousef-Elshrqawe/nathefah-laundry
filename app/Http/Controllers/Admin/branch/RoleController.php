<?php

namespace App\Http\Controllers\Admin\branch;

use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Traits\GeneralTrait;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    use GeneralTrait;


    function __construct()
    {

//        $this->middleware('permission:branch-role-create', ['only' => ['store']]);
//        $this->middleware('permission:branch-role-edit', ['only' => ['edit','update']]);
//        $this->middleware('permission:حذف صلاحية', ['only' => ['destroy']]);

    }

    public function store(RoleRequest $request)
    {
        $role = Role::create([
            'guard_name' => 'branch',
            'name' => $request->input('name')
        ]);

        $role->syncPermissions($request->input('permissions'));

      return redirect()->back();
    }

    public function edit(Request $request)
    {
        $role         = Role::with('permissions')->where('id',$request->id)->first();
        $permissions  = Permission::where('guard_name','branch')->get();

        $response = [
            'role'        => $role,
            'permissions' => $permissions
        ];
        return $this->returnData('data',$response,'Role with permissions',200);
    }

    public function update(Request $request)
    {
        $role = Role::find($request->id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permissions'));
        return redirect()->back()->with('success','Role updated successfully');
    }


    public function destroy($id)
    {
         Role::find($id)->delete();
        return $this->returnSuccessMessage("Role deleted successfully");
    }

}
