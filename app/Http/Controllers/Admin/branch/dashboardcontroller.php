<?php

namespace App\Http\Controllers\Admin\branch;
use App\Http\Controllers\Admin\abstractions\HomeAbstraction;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Order\{order};
use \Carbon\Carbon;


class dashboardcontroller extends HomeAbstraction
{
    //
    public function home(Request $request,$week){
    $branch_id=Auth::guard('branch')->user()->branch_id;

    $date = \Carbon\Carbon::today()->subDays($week);

    $orders = $this->orderDetails($branch_id,$date,$request);

    $todayorders = $this->orders($branch_id,'today');

    $inprogress  = $this->orders($branch_id,'inprogress');


    $Balance = round($this->balance($branch_id,''),1);

    $Balancetoday= round($this->balancetoday($branch_id, Carbon::today()),1);

    return view('branch.home.index',compact('orders','todayorders','inprogress','week','Balance','Balancetoday'));
    }
}
