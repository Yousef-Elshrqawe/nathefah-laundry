<?php

namespace App\Http\Controllers\Admin\branch;

use App\Http\Controllers\Admin\abstractions\ServiceAbstractions;
use App\Models\Laundry\branch;
use App\Models\Laundry\branchservice;
use App\Models\laundryservice\branchAdditionalservice;
use App\Traits\GeneralTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use RealRashid\SweetAlert\Facades\Alert;


//rejectedorder
class ServiceController extends ServiceAbstractions
{
    use GeneralTrait;

    //
    public function index(){
        $branch_id   = Auth::guard('branch')->user()->branch_id;
        $services_id = $this->serviceIds($branch_id);

        // use this in show only
        $services = $this->services($branch_id);


         $additionalservices_id = $this->additionalServicesIds($branch_id);
         // use this in show only
         $additionalservices = $this->additionalServices($branch_id);

         $argentPrice = $this->argentPrice($branch_id);

        $reports = $this->reports($branch_id);

        $branch = branch::where('id',$branch_id)->first();

        $branchservices= $this->branchservices($branch_id);

        $additionalservicescount=$this->branchadditionalservices($branch_id);

        return view('branch.services.index',compact('services','additionalservices','services_id','additionalservices_id','argentPrice','reports','branch','branchservices','additionalservicescount'));
    }

    public function edit(){
        $branch_id   = Auth::guard('branch')->user()->branch_id;
        $services_id = $this->serviceIds($branch_id);

        $services    = $this->servicesByServiceId($branch_id,$services_id);

        $additionalservices = $this->additionalServices($branch_id);

        return view('branch.services.edit',compact('services','additionalservices'));
    }

    public function update(Request $request){
        $this->updateService($request);
        Alert::success('success', 'Services updated successfully');
        return redirect()->back();
    }

    public function getitem($id){
        return $this->getServiceItem($id);
    }

    // in this function change item name
    public function edititem(Request $request){
         $this->editServiceItem($request);
         Alert::success('success', 'Item updated successfully');
         return redirect()->back();
    }

    public function additem(Request $request){
        //dd($request->all());
        $branchuser = Auth::guard('branch')->user();
        $branchid   = $branchuser->branch_id;

        $this->storeItem($request,$branchid);

        Alert::success('success', 'New items Add successfully');
        return redirect()->back();
    }


    public function changeStatus(Request $request)
    {
        $service = $this->changeServiceStatus($request);

        return $this->returnData('data',$service,'Status','200');
    }

    public function changeItemStatus(Request $request)
    {
        $service = $this->changeItemServiceStatus($request);
        return $this->returnData('data',$service,'Status','200');
    }

    public function changeArgentPrice(Request $request)
    {
        $this->changeItemArgentPrice($request);

        Alert::success('success', 'Argent price updated successfully');
        return redirect()->back();
    }

    public function changeArgentPriceStatus(Request $request)
    {
        $branch = branch::where('id',$request->id)->first();
        $branch->update([
            'argent' => $branch->argent == 1 ? 0 : 1
        ]);

        return $this->returnData('data',$branch,'Status','200');
    }
}
