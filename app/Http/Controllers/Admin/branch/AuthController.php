<?php

namespace App\Http\Controllers\Admin\branch;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use Session;

class AuthController extends Controller
{
    //
    public function loginview(){
        return view('branch.Auth.login');
    }
    public function login(Request $request) {
        // تحقق من صحة المدخلات
        $request->validate([
            'phone' => 'required|numeric',
            'password' => 'required',
        ]);

        // تنسيق رقم الهاتف لمحاولة تسجيل الدخول بحالتين
        $phone = ltrim($request->phone, '0'); // إزالة الأصفار من البداية
        $formattedPhoneWithZero = '0' . $phone; // الرقم مع الصفر في البداية
        $formattedPhoneWithoutZero = $phone; // الرقم بدون الصفر في البداية

        // إعداد بيانات المصادقة لمحاولة الاثنين
        $credentials = [
            ['phone' => $formattedPhoneWithZero, 'password' => $request->password],
            ['phone' => $formattedPhoneWithoutZero, 'password' => $request->password],
        ];

        foreach ($credentials as $credential) {
            if (Auth::guard('branch')->attempt($credential)) {
                Session::put('username', Auth::guard('branch')->user()->username);
                return redirect()->route('branch.home', [1]);
            }
        }

        // في حالة فشل تسجيل الدخول
        return redirect()->route('branch.login')->with('error', 'Login details are not valid');
    }



    public function logout(){
        Auth::guard('branch')->logout();
        return redirect()->route('branch.login');
    }


}
