<?php

namespace App\Http\Controllers\Admin\branch;

use App\Http\Controllers\Admin\abstractions\SettingAbstractions;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserBranchRequest;
use App\Interfaces\NotificationRepositoryinterface;
use App\Models\Laundry\branch;
use App\Models\Laundry\Branchuser;
use App\Models\Notification\branchnotifytype;
use App\Models\Notification\drivernotifytype;
use App\Models\Notification\Usernotifytype;
use App\Traits\GeneralTrait;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class SettingController extends SettingAbstractions
{
    use GeneralTrait;

    /**
     * User belongs to branch
     * Get branch info
     * @return object
     */

     public function __construct(NotificationRepositoryinterface $NotificationRepository)
     {
         $this->NotificationRepository = $NotificationRepository;
     }

    public function index(Request $request)
    {
        $currentUser   =  auth('branch')->user();
        $notifications = $currentUser->notifications;
        $branchInfo    = $this->branchInfo($currentUser->branch_id,$request);
        $roles         = Role::where('guard_name','branch')
                        ->when($request->search, function($q) use ($request) {
                            $q->where('name', 'like', '%' . $request->search . '%');
                        })->get();
        $permission  = Permission::where('guard_name','branch')->get();
        return view('branch.settings.index',compact('branchInfo','currentUser','notifications','roles','permission'));
    }

    /**
     * @param Request $request
     * @param $id
     * @return object
     */
    public function updateInfo(Request $request,$id)
    {
       $this->updateBranchInfo($id,$request);

        if($request->ajax())
        {
            return $this->returnSuccessMessage('Data updated successfully',   "200",  'data', 200);
        }

        Alert::success('success', 'Info updated successfully');
        return redirect()->back();
    }

    /**
     * @param $id
     * @return object
     */
    public function restInfo($id)
    {
        $branch = branch::findOrFail($id);
        $branch->update([
            'username'     => null,
            'phone'        => null,
            'country_code' => null,
            'address'      => null,
        ]);

        return $this->returnSuccessMessage('Data rest successfully',   "200",  'data', "");

    }

    /**
     * @param UserBranchRequest $request
     * @return Object
     */
    public function storeUserInBranch(UserBranchRequest $request)
    {


        // $role = explode("-", $request->role);
        // $role_id   =  $role[0];
        // $role_name =  $role[1];

        $user =   Branchuser::create([
            'username'     => $request->username,
            'email'        => $request->email,
            'country_code' => $request->country_code,
            'phone'        => $request->phone,
            'branch_id'    => auth('branch')->user()->branch_id,
            'password'     => $request->password,
            'type'         => 'user'
        ]);

        $this->NotificationRepository->createnotificationsetting('branch',$user->id);

        $user->assignRole([$request->role]);

        Alert::success('success', 'User created successfully');
        return redirect()->back();
    }

    public function editUserInBranch(Request $request)
    {
        $user =   Branchuser::with('roles')->findOrFail($request->id);
        return $this->returnData('data',$user,'User Data',200);
    }

    public function updateUserInBranch(Request $request)
    {

        $role = explode("-", $request->role);
        $role_id   =  $role[0];
        $role_name =  $role[1];

        $user =   Branchuser::findOrFail($request->id);
        $user->update([
            'username'     => $request->username,
            'email'        => $request->email,
            'country_code' => $request->country_code,
            'phone'        => $request->phone,
            'branch_id'    => auth('branch')->user()->branch_id,
            'password'     => 123456, // Default passwords
            'type'         => $role_name
        ]);
        //$this->NotificationRepository->createnotificationsetting('branch',$branchuser->id);
        DB::table('model_has_roles')->where('model_id',$request->id)->delete();
        $user->assignRole([$role_id]);

        Alert::success('success', 'User updated successfully');
        return redirect()->back();
    }

    /**
     * @param $user_id
     * @return object
     */
    public function deleteUserInBranch($user_id)
    {
        Branchuser::findOrFail($user_id)->delete();
        return $this->returnSuccessMessage('User deleted successfully',   "200",  'data', "");
    }

    public function addDeleteNotification(Request $request)
    {
        $notification = branchnotifytype::where('user_branch_id',auth('branch')->id())->get();

        $notification[0]->status = $request->option1;
        $notification[1]->status = $request->option2;
        $notification[2]->status = $request->option3;
        $notification[3]->status = $request->option4;
        $notification[4]->status = $request->option5;

        for ($i=0; $i<=5; $i++){
            $notification[$i]->save();
        }

        return $this->returnSuccessMessage('Notification updated successfully',   "200",  'data', "");
    }

    /**
     * @return object
     */
    public function restNotifications()
    {
        branchnotifytype::where('user_branch_id',auth('branch')->id())
                            ->update(['status' => 0]);
        return $this->returnSuccessMessage('Notification rest successfully',   "200",  'data', "");

    }


    public function branchDetails()
    {
        $branchId      = auth('branch')->user()->branch_id;
        $branchInfo    = $this->branchInfo($branchId,'');
        $successOrders = $this->successOrders($branchId,'');
        $services_id   = $this->serviceIds($branchId);
        $services      = $this->services($branchId);

        $additionalservices_id = $this->additionalServicesIds($branchId);
        // use this in show only
        $additionalservices = $this->additionalServices($branchId);

        $latestOrders = $this->latestOrders($branchId,'');


        $todaySuccessOrders = $this->successOrders($branchId,Carbon::today());
        $todayOrders        = $this->allTodayOrders($branchId);


        $yesterdaySuccessOrders = $this->successOrders($branchId,Carbon::yesterday());
        $yesterdayOrders        = $this->allTodayOrders($branchId);

        $reports  = $this->orderReports($branchId);

        return view('branch.settings.branch_details.index',compact(
            'branchInfo',
            'successOrders',
            'branchId',
            'services_id',
            'services',
            'additionalservices',
            'additionalservices_id',
            'latestOrders',
            'todaySuccessOrders',
            'todayOrders',
            'yesterdayOrders',
            'yesterdaySuccessOrders',
            'reports'
        ));
    }



}
