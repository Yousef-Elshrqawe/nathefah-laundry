<?php

namespace App\Http\Controllers\Admin\branch;

use App\Http\Controllers\Admin\abstractions\OrderAbstractions;
use App\Http\Controllers\Controller;
use App\Models\Notification\drivernotifytype;
use App\Models\Transaction;
use App\QueryFilters\TransactionFilter;
use App\Models\laundryservice\{Service, Argent, Serviceitemprice};
use App\Models\Driver\Driver;
use App\Interfaces\NotificationRepositoryinterface;
use App\Interfaces\OrderRepositoryInterface;
use App\Models\Order\{order, Orderreview, orderdetailes, OrderDriveryStatus};
use App\Models\Laundry\{branchservice};
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use Illuminate\Http\Request;
use Auth;
use Alert;

class OrderController extends OrderAbstractions
{
    public $OrderRepository;
    public $NotificationRepository;

    public function __construct(OrderRepositoryInterface $OrderRepository, NotificationRepositoryinterface $NotificationRepository)
    {
        $this->OrderRepository = $OrderRepository;
        $this->NotificationRepository = $NotificationRepository;
    }

    public function index(Request $request, $type, $date, $reviewdate)
    {
        $orderdate = $date;
        $datereview = $reviewdate;

        if ($date != 'all') {
            $date = \Carbon\Carbon::today()->subDays($date);
        }
        if ($reviewdate != 'all') {
            $reviewdate = \Carbon\Carbon::today()->subDays($reviewdate);
        }
        $branch_id = Auth::guard('branch')->user()->branch_id;

        if ($type == 'indelivery') {
            $orders = $this->inDeliveryOrder($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'inprogress') {
            $orders = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'inprogress', $request);
        }
        if ($type == 'completed') {
            $orders = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'completed', $request);
        }
        if ($type == 'unassigned') {
            $orders = $this->unassigned($branch_id, $orderdate, $date, $request);

        }
        if ($type == 'new') {
            $orders = $this->newOrder($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'self_delivery') {
            $orders = $this->self_delivery_orders($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'all') {
            $orders = $this->allOrders($branch_id, $orderdate, $date, $request);
        }

        if ($type == 'one_way_delivery') {
            $orders = $this->one_way_delivery($branch_id, $orderdate, $date, $request);
        }

        // count of orders
        $all = $this->allOrders($branch_id, $orderdate, $date, $request)->total();

        $completed = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'completed', $request)->total();

        $inprogresscount = $this->inProgressCompletedOrder($branch_id, $orderdate, $date, 'inprogress', $request)->total();

        $indelivery = $this->inDeliveryOrder($branch_id, $orderdate, $date, $request)->total();

        $unassigned = $this->unassigned($branch_id, $orderdate, $date, $request)->total();

        $neworder = $this->newOrder($branch_id, $orderdate, $date, $request)->total();

        $onewayorders = $this->one_way_delivery($branch_id, $orderdate, $date, $request)->total();

        $self_delivery_orders = $this->self_delivery_orders($branch_id, $orderdate, $date, $request)->total();
        // end count of orders

        $services = $this->getServices($branch_id);

        // dd($services);

        $reports = $this->reports($branch_id, $reviewdate);

        $latestorders = $this->latestOrders($branch_id, '');

        $branch = branch::select('laundry_id')->find($branch_id);
        $laundry = Laundry::select('one_way_delivery_fees', 'door_to_door_delivery_fees')->find($branch->laundry_id);

        $porttype = 'branch';

        return view('branch.Order.index', compact('type', 'orders', 'services', 'porttype',
            'reports', 'latestorders', 'indelivery', 'all', 'inprogresscount', 'completed', 'orderdate', 'datereview', 'unassigned', 'neworder', 'onewayorders', 'self_delivery_orders', 'laundry'));
    }


    // we use ajax in this request
    public function submit(Request $request)
    {

        if ($request->branchId != 0) {
            $branchid = $request->branchId;
        } else {
            $branchuser = Auth::guard('branch')->user();
            $branchid = $branchuser->branch_id;
        }


        $submit = $this->submitOrder($request, $branchid);

        $order = $submit[0];
        $itemcount = $submit[1];
        $distance = $submit[2];

        return view('branch.components.order_detailes', compact('order', 'itemcount', 'distance'));
    }

    public function confirmorder(Request $request)
    {
        if ($request->isadmin = 1) {
            $branchid = $request->branch_id;
        } else {
            $branchid = Auth::guard('branch')->user()->branch_id;
        }
        $this->OrderRepository->checkorder($request, $branchid);

        // $order= Order::find($order_id)->update(['checked'=>true]);
        Alert::success('success', 'order confirmed successfuly');


        return redirect()->back();


    }

    public function latestorder(Request $request)
    {
        $date = \Carbon\Carbon::today()->subDays($request->days);
        $branch_id = Auth::guard('branch')->user()->branch_id;
        $orders = $this->latestOrders($branch_id, $date);
        return view('branch.home.index', compact('orders'));
    }

    public function viewall(Request $request)
    {
        $branch_id = Auth::guard('branch')->user()->branch_id;
        $orders = $this->viewAllOrders($branch_id, $request);
        return view('branch.Order.view', compact('orders'));
    }

    public function view($id)
    {
        $order = Order::with(['driver', 'paymenttype', 'deliverytype', 'orderdetailes' => function ($q) {
            $q->with('service', 'Branchitem', 'additionalservice')->get();
        }])->find($id);
        $urgents = Argent::where('order_id', $id)->get();
        //dd($urgents);
        return view('branch.Order.view_order', compact('order', 'urgents'));
    }

    public function finishorder($id)
    {
        $branchuser = Auth::guard('branch')->user();
        $branchid = $branchuser->branch_id;
        $order = $this->OrderRepository->finishorder($id, $branchid);
        if ($order == true) {
            Alert::success('success', 'order finished successfuly');
            return redirect()->back();
        }
    }

    public function acceptorder($id)
    { //
        $branch_user = Auth::guard('branch')->user();
        $order = $this->OrderRepository->acceptorder($id, $branch_user->branch_id, $branch_user->id);
        if ($order == true) {
            Alert::success('success', 'order accepted successfuly');
            return redirect()->back();
        }
    }

    public function assignorderview($order_id)
    {
        $branch_id = Auth::guard('branch')->user()->branch_id;
        $drivers = Driver::where('branch_id', $branch_id)->get();
        return view('branch.drivers.assigndrivers', compact('drivers', 'order_id'));
    }

    public function assgindriver($driver_id, $order_id)
    {
        order::findorfail($order_id)->update([
            'driver_id' => $driver_id
        ]);
        $driver = Driver::find($driver_id);
        $orderdeliverystatus = OrderDriveryStatus::where('order_id', $order_id)->where('confirmation', false)->first();
        $orderdeliverystatus->update([
            'driver_id' => $driver_id,
        ]);
        $title = 'طلب جديد';
        $body = 'لقد استلمت طلب جديد الان';
        if ($driver->lang == 'en') {
            $title = 'new order';
            $body = 'you recive new order from laundry now';
        }
        $titles = ['ar' => 'طلب جددي', 'en' => 'new order'];
        $bodys = ['ar' => 'new order', 'en' => 'you recive new order from laundry now'];
        $drivernotifytype = drivernotifytype::where('driver_id', $driver_id)->where('notificationtype_id', 3)->where('status', 1)->first();
        if ($drivernotifytype) {
            $this->NotificationRepository->sendnotification('driver', $driver_id, $title, $body, 1, null, null);
        }
        $this->NotificationRepository->createnotification('driver', $driver_id, $titles, $bodys);
        Alert::success('success', 'order accepted successfuly');
        return redirect()->route('branch.orders', ['unassigned', 'all', 'all']);
    }

    public function generate_qr_code($order_id)
    {
        $code = $this->OrderRepository->getcode('pick_up_laundry', $order_id);
        return view('components.orders.qrcode', compact('order_id', 'code'));
    }

    public function reciveorder(Request $request)
    {
        $data = $this->OrderRepository->reciveorder($request);

        if ($data == null) {
            return redirect()->back();
        }
        Alert::success('success', 'order accepted successfuly');
        return redirect()->back();
    }

    public function rejectorder(Request $request)
    {
        $order = Order::find($request->order_id);
        $branchuser = branch::find( $order->branch_id);

//        $branchuser = Auth::guard('branch')->user();
       $cancelorder = $this->OrderRepository->cancelorder($request, $branchuser->id);

       if ($cancelorder == true) {
           Alert::success('success', 'order rejected successfuly');
           return redirect()->back();
         }else {
           Alert::error('error', 'order rejected failed');
           return redirect()->back();
       }
        return redirect()->back();
    }


    public function deliverOrder($id)
    {
        try {
            $this->deliver($id);
            Alert::success('success', 'user recive  order success');
            return redirect()->back();
        } catch (\Exception $e) {
            return redirect()->back();
        }
    }

    //transactions
    public function transactions(Request $request)
    {
        $branch_id = Auth::guard('branch')->user()->branch_id;

        $query = Transaction::where('branch_id', $branch_id);

        $filter = new TransactionFilter($request);
        $transactions = $filter->apply($query)->paginate(20);
        $rows = $transactions->appends($request->all());
        return view('branch.transactions.index', compact('rows') );

    }




}
