<?php

namespace App\Http\Controllers\Admin\branch;

use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use App\Models\Notification\
    {branchnotifytype,Usernotifytype,drivernotifytype,BranchDeviceToken,UserDeviceToken
    ,DriverDeviceToken,BranchNotification,UserNotification,DriverNotification,
    usernotificationtranslation,branchnotificationtranslation,drivernotificationtranslation};
use Illuminate\Http\Request;
use Auth;

class NotificationController extends Controller
{
    public function index(Request $request){

        $branch_id = Auth::guard('branch')->user()->branch_id;

        $notifications = BranchNotification::with(['translations'=>function($q){
              $q->where('locale','en')->get();
            }])
            ->where('branch_id',$branch_id)
            ->when($request->search, function($q) use ($request,$branch_id) {
                $q->where('branch_id',$branch_id)
                    ->with(['translations'=>function($q) use ($request) {
                        $q->where('locale','en')
                        ->where(function ($query) use ($request) {
                            $query->where('title', 'like', '%' . $request->search . '%')
                                ->orWhere('body', 'like', '%' . $request->search . '%');
                        });
                    }]);
            })
            ->paginate(100);


        return view('branch.Notification.index',compact('notifications'));
    }
}
