<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\{ApplicationRates, Support, Setting};
use RealRashid\SweetAlert\Facades\Alert;

class ApplicationRatesController extends Controller
{

    //edit
    public function edit(){
        $row= ApplicationRates::first();
        return view('Admin.application_rate.edit',compact('row'));
    }


    public function update(Request $request){

         $row= ApplicationRates::first();
            if($row){
                $row->update([
                    'rate' => $request->rate
                ]);
                Alert::success('Success', 'Application Rate Updated Successfully');
                return redirect()->route('admin.application-rate.edit');
            }

            return redirect()->route('admin.application-rate.edit');

    }



}
