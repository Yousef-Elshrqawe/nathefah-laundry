<?php

namespace App\Http\Controllers\Admin;
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;

use App\Http\Controllers\Controller;
use App\Models\Order\order;
use App\Models\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\System\Systeminfo;
use Carbon\Carbon;
use App\Http\Controllers\Admin\abstractions\balanceAbstraction;

class HomeController extends Controller
{
    //
    use balanceAbstraction;
    public function index(Request $request){
        $todayorders=order::where('checked',true)->whereDate('created_at',Carbon::now())->count();

        $inprogressordercount=order::where('progress','inprogress')->count();

        $todaybalance=round($this->todaybalance(),1);
        $balance= round(Systeminfo::select('balance')->first()->balance,1);



        $LaundriesCount = Laundry::count();
        $topLaundries = Laundry::where('status',1)
            ->with(array('branchs' => function($query) {
                $query->addSelect([
                    'allOrders' => order::selectRaw('count(*)')
                        ->whereColumn('orders.id', 'branchs.id')
                ]);
            }))
            ->addSelect([
                'rate' => branch::selectRaw('avg(rate)')
                    ->whereColumn('branchs.laundry_id', 'laundries.id')
            ])
            ->orderBy('rate','desc')
            ->take(5)
            ->get();


        $badLaundries = Laundry::where('status',1)
            ->with(array('branchs' => function($query) {
                $query->addSelect([
                    'allOrders' => order::selectRaw('count(*)')
                        ->whereColumn('orders.id', 'branchs.id')
                ]);
            }))
            ->addSelect([
                'rate' => branch::selectRaw('avg(rate)')
                    ->whereColumn('branchs.laundry_id', 'laundries.id')
            ])
            ->orderBy('rate','asc')
            ->take(5)
            ->get();

        $total_order= order::count();

        // اجمالي  سعر  الاوردرات
        $price_price_before_discount = order::sum('price_before_discount');
        $price_after_discount = order::sum('price_after_discount');
        if ($price_price_before_discount == null)
            $price_price_before_discount = 0;
        if ($price_after_discount == null)
            $price_after_discount = 0;


        $total_price = $price_price_before_discount - $price_after_discount;
        $app_percentage = Transaction::sum('application_percentage');


        return view('Admin.home',compact('LaundriesCount','topLaundries','badLaundries','todayorders','inprogressordercount','balance','todaybalance'
            ,'total_order' ,'total_price' ,'app_percentage'));
    }
}
