<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdminStore;
use App\Http\Requests\AdminUpdate;
use App\Http\Requests\UserStore;
use App\Http\Requests\UserUpdate;
use App\Models\Admin\Admin;
use App\Models\User;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class UserController extends Controller
{

    public function index(Request $request)
    {
        $rows = User::latest()->paginate(10);

        return view('Admin.user.index', compact(['rows']));
    } // end of index


    public function store(UserStore $request)
    {
        $data = $request->except(['password_confirmation', 'next' ]);
        $data['password'] = bcrypt($request->password);
        User::create($data + ['verified' => '1' , 'country_code' => '+966']);
        return Redirect::back();
    } // end of store


    public function edit($id)
    {
        $row = User::findOrFail($id);
        return view('Admin.user.edit', compact(['row']));
    } // end of edit



    public function update(UserUpdate $request, $id)
    {
        $row = User::findOrFail($id);
        $data = $request->except(['password_confirmation', 'next' ]);
        if ($request->password) {
            $data['password'] = bcrypt($request->password);
        } else {
            $data['password'] = $row->password;
        }
        $row->update($data);
        Alert::success('Success', 'User Updated Successfully');
        return  Redirect::route('admin.users');
    } // end of update


    public function destroy($id)
    {
        $row = User::findOrFail($id);
        $row->delete();
        Alert::success('Success', 'User Deleted Successfully');
        return Redirect::back();
    } // end of destroy


} // end of ItemController
