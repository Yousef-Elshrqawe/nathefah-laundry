<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Order\order;
use App\Models\Transaction;
use Illuminate\Http\Request;
use App\Models\Balance\Balance;
use App\Models\System\Systeminfo;
use App\Models\SystemTransaction;
use Carbon\Carbon;
use App\Http\Controllers\Admin\abstractions\balanceAbstraction;


class BalanceController extends Controller
{
    // start index page
    use balanceAbstraction;
    public function index($type,$date){
        $transactions=$this->transaction($type,$date)->paginate(50);

        $todaybalance=round($this->todaybalance(),1);
        $balance= round(Systeminfo::select('balance')->first()->balance,1);
        $total_order= order::count();

        // اجمالي  سعر  الاوردرات
        $price_price_before_discount = order::sum('price_before_discount');
        $price_after_discount = order::sum('price_after_discount');
        if ($price_price_before_discount == null)
            $price_price_before_discount = 0;
        if ($price_after_discount == null)
            $price_after_discount = 0;


        $total_price = $price_price_before_discount - $price_after_discount;
        $app_percentage = Transaction::sum('application_percentage');

        return view('Admin.balance.index',compact('transactions','type','date','balance','todaybalance' ,'total_order' ,'total_price' ,'app_percentage'));
    }
    public function search(Request $request){
       $type=$request->type;   $date=$request->date;
       $transactions=$this->transaction($type,$date,$request->search_key)->paginate(50);
       $todaybalance=$this->todaybalance();
       $balance= round(Systeminfo::select('balance')->first()->balance,1);

        $total_order= order::count();
        $price_price_before_discount = order::sum('price_before_discount');
        $price_after_discount = order::sum('price_after_discount');
        if ($price_price_before_discount == null)
            $price_price_before_discount = 0;
        if ($price_after_discount == null)
            $price_after_discount = 0;


        $total_price = $price_price_before_discount - $price_after_discount;
        $app_percentage = Transaction::sum('application_percentage');
       return view('Admin.balance.index',compact('transactions','type','date','balance','todaybalance' ,'total_order' ,'total_price' ,'app_percentage'));
    }

    // end index page
    // start view
    public function view($id){
       $transaction=SystemTransaction::with('branch.laundry','laundry','order')->find($id);
       return $transaction;
    }

}
