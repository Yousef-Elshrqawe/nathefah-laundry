<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class ItemController extends Controller
{

    public function index(Request $request)
    {
        $items = Item::when($request->item_search_input, function ($q) use ($request) {
            return $q->whereTranslationLike('name', '%' . $request->item_search_input . '%');
        })->orderBy('id', 'DESC')->paginate(10);

        $categories = Category::get();

        return view('Admin.item.index', compact(['items', 'categories']));
    } // end of index


    public function store(Request $request)
    {
        $langs = config('translatable.locales');

        //validation
        $rules = [];
        foreach ($langs as $localee) {
            $rules += [$localee . '.name' => 'required',];
        }

        $rules += [
            // 'serial' => 'required',
            'category_id' => 'required|exists:categories,id',
        ];

        // dd($request->all());
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        //end of validation

        //insert
        // $item = Item::create($request->all());
        $item = Item::create([
            "category_id" => $request->category_id,
        ]);

        foreach ($langs as $localee) {
            $item_trans = new ItemTranslation();
            $item_trans->item_id = $item->id;
            $item_trans->locale = $localee;
            $item_trans->name = $request->$localee['name'];
            $item_trans->save();
        } //end of for each

        Alert::success('success', __('admin.added_successfully'));

        return redirect()->back()->with(['success' => __('admin.added_successfully')]);
    } // end of


    public function edit($id)
    {
        $item = Item::find($id);
        $categories = Category::get();

        if (!$item)
            return redirect()->route('Admin.items')->with(['error' => 'هذا العنصر غير موجود']);

        return view('Admin.item.edit', compact(['item', 'categories']));
    } // end of edit



    public function update(Request $request, $id)
    {
        $item = Item::find($id);
        if (!$item)
            return redirect()->route('Admin.items')->with(['error' => 'هذا العنصر غير موجود']);

        $request->validate([
            'ar.name' => 'required',
            'en.name' => 'required',
            'category_id' => 'required|exists:categories,id',
            ]);

        //Update
        $item->category_id = $request->category_id;
        $item->save();

        //Update translations
        foreach (config('translatable.locales') as $localee) {
            ItemTranslation::where('locale', $localee)
                ->where('item_id', $item->id)
                ->update([
                    'name' => $request->$localee['name']
                ]);
        }
        Alert::success('success', __('admin.updated_successfully'));

        return redirect()->back()->with(['success' => __('admin.updated_successfully')]);
    } // end of update




} // end of ItemController
