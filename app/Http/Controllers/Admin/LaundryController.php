<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Admin\abstractions\PackageTrait;
use App\Http\Controllers\Admin\abstractions\ServiceAbstractions;
use App\Http\Requests\LaundryRequest;
use App\Models\Admin\Deliveryfees;
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use App\Models\Order\order;
use App\Models\Order\Orderreview;
use App\Models\Subscription\Period;
use App\Models\Wallet\laundry_wallet;
use App\Traits\fileTrait;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Admin\abstractions\balanceAbstraction;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use RealRashid\SweetAlert\Facades\Alert;


class LaundryController extends ServiceAbstractions
{

    use fileTrait,PackageTrait,balanceAbstraction;

    public function index(Request $request, $date)
    {
        $LaundriesCount = Laundry::count();
        $activelaundries = Laundry::where('status', 1)->count();

        // استعلام الأساس بدون التصفية
        $laundriesQuery = Laundry::with([
            'branchs' => function ($query) {
                $query->addSelect([
                    'allOrders' => order::selectRaw('count(*)')
                        ->whereColumn('orders.branch_id', 'branchs.id')
                        ->where('checked', true)
                ]);
            },
            'package.package.period'
        ])->addSelect([
            'rate' => branch::selectRaw('avg(rate)')
                ->whereColumn('branchs.laundry_id', 'laundries.id')
        ]);

        // تطبيق البحث
        if ($request->search) {
            $laundriesQuery->where(function ($query) use ($request) {
                $query->where('email', 'like', '%' . $request->search . '%')
                    ->orWhere('phone', 'like', '%' . $request->search . '%')
                    ->orWhere('name', 'like', '%' . $request->search . '%');
            });
        }

        // تصفية التاريخ
        if ($date != 'All') {
            $laundriesQuery->where('created_at', '>', Carbon::now()->subDays($date));
        }

        // تنفيذ البحث مع التصفية والتقسيم إلى صفحات
        $laundries = $laundriesQuery->latest()->paginate(10);

        // تمرير البحث إلى الروابط لتفادي فقدانه عند الانتقال بين الصفحات
        $laundries->appends(['search' => $request->search, 'date' => $date]);

        // بيانات أخرى للعرض
        $periods = Period::all();
        $todaybalance = $this->todaybalance();
        $topbranch = branch::orderBy('rate', 'desc')->first();
        $toplaundry = $topbranch ? Laundry::find($topbranch->laundry_id) : null;
        $laundriesAll = Laundry::latest()->get();

        return view('Admin.laundries.index', compact(
            'LaundriesCount',
            'laundries',
            'periods',
            'todaybalance',
            'date',
            'toplaundry',
            'activelaundries',
            'laundriesAll'
        ));
    }




    public function pandinglaundry(){
        $Laundries=Laundry::where('status','false')
        ->with('branchs')
        ->orderby('created_at','desc')->paginate(20);
        return view('Admin.laundries.New_laundries',compact('Laundries'));
    }
    public function shownewlaundry($id){
        $Laundry=Laundry::with('branchs')->find($id);

        return view('Admin.laundries.show_new_laundry',compact('Laundry'));
    }
    public function Activation($id){
        $Laundry=Laundry::with('branchs')->find($id)->update([
            'status'=>'true'
        ]);
        return redirect()->route('Admin.pending.laundry');
    }

    public function laundryDetails(Request $request,$laundry_id)
    {

        $laundry  = Laundry::where('id',$laundry_id)->first();
        $branches     = $this->Branches($laundry_id);
        $branches_ids = $branches->pluck('id');

        $completedOrders = order::whereIn('branch_id',$branches_ids)
                                 ->whereIn('progress',['completed','finished'])
                                 ->count();

        $latestOrders    = order::with('branch')->select('id','delivery_status','branch_id')
                                ->whereIn('branch_id',$branches_ids)
                                ->latest()
                                ->take(10)
                                ->get();

        $order       = order::whereIn('branch_id',$branches_ids)->pluck('id');

        $orderReview = Orderreview::whereIn('order_id',$order)->count();
        $orderReviewCount = Orderreview::whereIn('order_id', $order)->count();
        $averageRating = Orderreview::whereIn('order_id', $order)->avg('rate') ?? 0;
        $fullStars = floor($averageRating);
        $halfStar = ($averageRating - $fullStars) >= 0.5;
        $emptyStars = 5 - $fullStars - ($halfStar ? 1 : 0);



        $branch_id=null;
        $branch=Branch::select('id')->where('Laundry_id',$laundry_id)->first();
        if($branch!=null)
        $branch_id=$branch['id'];
        return view('Admin.laundries.laundry_details',compact(

            'branches',
            'laundry_id',
            'laundry',
            'completedOrders',
            'latestOrders',
            'orderReview',
            'branch_id',
            'orderReviewCount',
            'fullStars',
            'halfStar',
            'emptyStars'

        ));
    }

    public function orderReports($branch_id)
    {
        return Orderreview::where('branch_id',$branch_id)
           ->latest()->with('user')->paginate(20);
    }


    public function store(LaundryRequest $request)
    {
       /* try{*/
            DB::beginTransaction();
            $logo = null;
            if($request->file('laundry_logo')){
                $logo = $this->MoveImage($request->file('laundry_logo'),'uploads/laundry/logos');
            }

            $tax_card = null;
            if ($request->file('tax_card')){
                $tax_card = $this->MoveImage($request->file('tax_card'),'uploads/laundry/tax_card');
            }

            $company_register = null;
            if ($request->file('company_register')){
                $company_register = $this->MoveImage($request->file('company_register'),'uploads/laundry/company_register');
            }

            $one_way_delivery_fees      = Deliveryfees::where('type','one way')->first()->price;

            $door_to_door_delivery_fees = Deliveryfees::where('type','door to door')->first()->price;

            $laundry = Laundry::create([
                'name'                       => $request->name,
                'phone'                      => $request->phone,
                'country_code'               => "+966" ,
                'email'                      => $request->email,
                'branch'                     => $request->branch_number,
                'logo'                       => $logo,
                'taxcard'                    => $tax_card,
                'tax_number'                 => $request->tax_number,
                'companyregister'            => $company_register,
                'password'                   => Hash::make($request->password),
                'one_way_delivery_fees'      => $one_way_delivery_fees,
                'door_to_door_delivery_fees' => $door_to_door_delivery_fees
            ]);

            laundry_wallet::create([
                'laundry_id' => $laundry->id
            ]);

            //package
            $this->addPackage($laundry,$request);

            DB::commit();
            Alert::success('success', __('admin.added_successfully'));
            return redirect()->back();

       /* }catch(\exption $e){
            return redirect()->back();
        }*/
    }


    public function laundryDetailsUpdate(Request $request,$id)
    {
        $request->validate([
            'name'  => 'required|string',
            'email' => 'required|email|unique:laundries,email,'.$id,
            'phone' => 'required|regex:/^5[0-9]{8}$/|unique:laundries,phone,'.$id,
        ],
        [
            'phone.regex' => 'The phone number must be in the format 5xxxxxxxx',
        ]);

        $laundry = Laundry::findOrFail($id);
        $laundry->update([
            'name'  => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
        ]);

        return redirect()->back()->with(['success' => __('admin.updated_successfully')]);
    }


    public function acticealundry($id){
        try{
            $Laundry=Laundry::findorfail($id);
            $Laundry->update([
              'status' => 1,
            ]);
            Alert::success('success', 'Active laundry successfuly');
            return redirect()->back();

        }catch(\Exception $ex){
           return redirect()->back();
        }
    }


    public function disapleslaundry($id){

         try{
            $Laundry=Laundry::find($id);

            $Laundry->update([
            'status' => 0,
            ]);
            Alert::success('success', 'Active laundry successfuly');
            return redirect()->back();
        }catch(\Exception $ex){
        return redirect()->back();
       }
    }

    //create branch
    public function createBranch(Request $request)
    {
        $laundriesAll = Laundry::latest()->get();
        return view('Admin.branch.create',compact('laundriesAll'));
    }


}
