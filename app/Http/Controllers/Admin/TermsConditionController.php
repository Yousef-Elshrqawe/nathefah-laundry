<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ForceUpdate;
use App\Models\TermsCondition;
use App\Models\UserTerms;
use App\Models\laundryservice\{Item, ItemTranslation, Category};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use RealRashid\SweetAlert\Facades\Alert;


class TermsConditionController extends Controller
{

    public function index(Request $request)
    {
        $rows = TermsCondition::orderBy('id', 'desc')->get();
        return view('Admin.terms_conditions.index', compact(['rows']));
    } // end of index




    public function edit($id)
    {
        $row = TermsCondition::findOrFail($id);
        return view('Admin.terms_conditions.edit', compact(['row']));
    } // end of edit



    public function update(Request $request, $id)
    {
        $row = TermsCondition::findOrFail($id);

        $request->validate([
            'terms_conditions_ar' => 'required',
            'terms_conditions_en' => 'required',
            'privacy_policy_en' => 'required',
            'privacy_policy_ar' => 'required',
            ]);

        //Update
        $row->update($request->all());

        $userTerms = UserTerms::where('is_read', 1)->get();
        foreach ($userTerms as $userTerm) {
            $userTerm->update([
                'is_read' => 0
            ]);
        }

        Alert::success('Success', 'Terms & Conditions Updated Successfully');
        return  Redirect::route('admin.terms_conditions');
    } // end of update




} // end of ItemController
