<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\PointRequest;
use App\Models\Point;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class PointController extends Controller
{

    public function index()
    {
        $points = Point::first();
        return view('Admin.points.index',compact('points'));
    }

    public function update(PointRequest $request)
    {
        $points = Point::first();

        $points->update([
            'amount' => $request->amount,
            'price'  => $request->price
        ]);

        Alert::success('success', 'Points update successful');

        return redirect()->back();
    }
}
