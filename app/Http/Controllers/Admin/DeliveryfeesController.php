<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\{Deliveryfees};
use RealRashid\SweetAlert\Facades\Alert;

class DeliveryfeesController extends Controller
{
    //
    public function index(){
        $delivery_fees=Deliveryfees::get();
        return view('Admin.deliveryfees.index',compact('delivery_fees'));
    }
    public function update(Request $request){
        Deliveryfees::find($request->id)->update(['price'=>$request->price]);
        Alert::success('success', 'delivery fees update succsessfuly');
        return redirect()->back();
    }
}
