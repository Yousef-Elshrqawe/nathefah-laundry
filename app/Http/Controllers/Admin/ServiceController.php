<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\laundryservice\Category;
use App\Models\laundryservice\Categoryservice;
use App\Traits\GeneralTrait;
use App\Models\laundryservice\{Service, ServiceTranslation};
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Traits\fileTrait;
use RealRashid\SweetAlert\Facades\Alert;


class ServiceController extends Controller
{

    use fileTrait,GeneralTrait;
    //
    // public function index(){
    //     return view('Admin.service.index');
    // } // end of index

    public function index(Request $request)
    {
        $services = Service::with('categoryservices')->when($request->service_search_input, function ($q) use ($request) {
            return $q->whereTranslationLike('name', '%' . $request->service_search_input . '%');
        })->orderBy('id', 'DESC')->paginate(10);
        $categories = Category::with('categoryservice')->get();
//        return $categories;
        return view('Admin.service.index', compact(['services','categories']));
    } // end of index


    public function store(Request $request){
        // $request->validate([
        //    'ar.name' => 'required',
        //    'en.name' => 'required',
        //    'image' => 'required|image',
        // ]);
        // Service::create([
        //    'code'=>$request->code,
        // ]);
        // return redirect('Super-admin/discount/avilable');

        $langs = config('translatable.locales');

        //validation
        $rules = [];
        foreach ($langs as $locale) {
            $rules += [$locale . '.name' => 'required|unique:servicetranslations,name',];
        }

        $rules += [
            'image' => 'required|image'
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return Redirect::back()->withErrors($validator)->withInput();
        }
        //end of validation

        $cover_name = '';
        if ($request->hasFile('image')) {
            # Upload New Image & Return its New Name
            $image_name = $this->uploadImage($request->file('image'), 'uploads/service/');
            # Save New Name in DB
            $cover_name = $image_name;
        }

        //insert
        $service = new Service();
        $service->img = $cover_name;
        $service->expected_time = $request->expected_time;
        $service->save();

        foreach ($langs as $locales) {
            $service_trans = new ServiceTranslation();
            $service_trans->service_id = $service->id;
            $service_trans->locale = $locales;
            $service_trans->name = request()->$locales['name'];
            $service_trans->save();
        } //end of for each

        Alert::success('success', __('admin.added_successfully'));

        return redirect()->back()->with(['success' => __('admin.added_successfully')]);
    } // end of


    public function edit($id)
    {
        $service = Service::find($id);
        if (!$service)
            return redirect()->route('Admin.services')->with(['error' => 'هذه الخدمة غير موجوده']);

        return view('Admin.service.edit', compact(['service']));
    } // end of edit



    public function update(Request $request, $id)
    {
        $service = Service::find($id);
        if (!$service)
            return redirect()->route('Admin.services')->with(['error' => 'هذه الخدمة غير موجوده']);

        $request->validate([
            // 'code'=>'required|unique:discounts,code,'.$request->id,
            'ar.name' => 'required',
            'en.name' => 'required',
            'expected_time' => 'required',
            'image' => 'nullable|image',
            ]);

        $service->expected_time = $request->expected_time;
        $service->save();
        $cover_name = $service->img;
        if ($request->hasFile('image')) {
            # Delete Old Image
            $this->deleteFile($cover_name, 'uploads/service/');
            # Upload New Image & Return its New Name
            $image_name = $this->uploadImage($request->file('image'), 'uploads/service/');
            # Save New Name in DB
            $cover_name = $image_name;
            // update image in db
            $service->img = $cover_name;
            $service->expected_time = $request->expected_time;
            $service->save();
        }



        //Update translations
        foreach (config('translatable.locales') as $locales) {
            ServiceTranslation::where('locale', $locales)
                ->where('service_id', $service->id)
                ->update([
                    'name' => $request->$locales['name']
                ]);
        }
        Alert::success('success',  __('admin.updated_successfully'));

        return redirect()->back()->with(['success' => __('admin.updated_successfully')]);
    } // end of update


    public function assignCategoryToService(Request $request)
    {
        $service_id   = $request->service_id;
        $category_ids = $request->category_ids;
        if(!$category_ids)
        return redirect()->back();

        //Delete old checked first
        Categoryservice::where('service_id',$service_id)->delete();

        foreach ($category_ids as $category_id)
        {
            Categoryservice::create([
                'service_id'  => $service_id,
                'category_id' => $category_id,
            ]);
        }
        Alert::success('success',   'Assign category successfully');

        return redirect()->back()->with(['success' => 'Assign category successfully']);
    }

    public function getServiceCategory(Request $request)
    {
        $categories = Categoryservice::where('service_id',$request->id)->get();
        return $this->returnData('data',$categories,'Categories Service',200);
    }

} // end of ServiceController
