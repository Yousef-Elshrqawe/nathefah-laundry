<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Interfaces\DiscountRepositoryInterface;
use App\Interfaces\NotificationRepositoryinterface;

use App\Models\Discount\{Discount,BranchDiscount,UserDiscount};
use Illuminate\Http\Request;
use App\Models\User;
use App\Jobs\assigneduserdiscount;
use RealRashid\SweetAlert\Facades\Alert;

class DiscountController extends Controller
{
    //
    public function __construct(DiscountRepositoryInterface $DiscountRepository,NotificationRepositoryinterface $NotificationRepository)
    {
        $this->DiscountRepository = $DiscountRepository;
        $this->NotificationRepository =$NotificationRepository;
    }
    public function index($type){
        $discounts=$this->DiscountRepository->getdiscounts($type);
        $count=$discounts->count();
        return view('Admin.Discount.index',compact('discounts','type'));
    }
    public function store(Request $request){
        $request->validate([
           'code'=>'required|unique:discounts',
           'percent'=>'required',
           'amount'=>'required',
           'min_order_price'=>'required',
           'start_date'=>'required',
           'enddate'=>'required',
            'type'=>'required'
        ]);
        Discount::create([
           'code'=>$request->code,
           'percent'=>$request->percent,
           'amount'=>$request->amount,
           'min_order_price'=>$request->min_order_price,
           'start_date'=>$request->start_date,
           'enddate'=>$request->enddate,
           'created_by'=>'Admin',
            'type'=> $request->type
        ]);
        Alert::success('success', 'add discount successfuly');
        return redirect()->back();
//        return redirect('Super-admin/discount/avilable');
    }
    public function delete($id){
         $discount=Discount::find($id);
         $discount->delete();
        return redirect('Super-admin/discount/avilable');
    }
    public function edit($id){
        $discount=Discount::find($id);
        return $discount;
    }
    public function update(Request $request){
        $request->validate([
            'code'=>'required|unique:discounts,code,'.$request->id,
            'percent'=>'required',
            'amount'=>'required',
            'min_order_price'=>'required',
            'start_date'=>'required',
            'enddate'=>'required',
         ]);
         $discount=Discount::find($request->id);
         $discount->update([
            'code'=>$request->code,
            'percent'=>$request->percent,
            'amount'=>$request->amount,
            'min_order_price'=>$request->min_order_price,
            'start_date'=>$request->start_date,
            'enddate'=>$request->enddate,
         ]);
                 Alert::success('success', 'update discount successfuly');

         return redirect()->back();
    }

    public function assign($id){
        $discount=Discount::find($id);
        $discountusers=UserDiscount::with('user')->where('discount_id',$id)->paginate(50);

        return view('Admin.Discount.assign',compact('discount','discountusers'));
    }
    public function assginalluser($id,Request $request){
        $discount=Discount::find($id);
        $discount->update([
            'assigned'=>true
        ]);
        User::select('id')->chunk(500,function($data)use($id){
            dispatch(new assigneduserdiscount($data,$id,$this->NotificationRepository));
        });
        Alert::success('success','wait minute for assign discount for all users');
        return redirect()->back();
    }

    public function deleteassigneduser($id){
        UserDiscount::find($id)->delete();
        return redirect()->back();
    }


    public function search($name){
      $users=User::select('id','name')->where('name','like',"%{$name}%")->get();
      return $users;
    }

    public function assginuser(Request $request){

       /* if ($request->user_id==null) {
            $users=User::select('id')->get();
            foreach ($users as $user) {
                $userdiscount=UserDiscount::where('user_id',$user->id)->where('discount_id',$request->discount_id)->first();
                if($userdiscount==null){
                    UserDiscount::create([
                       'user_id'=>$user->id,
                       'discount_id'=>$request->discount_id,
                       'count'=>1
                    ]);
                }
            }
        }*/
        if ($request->user_id==null) {
            Alert::error('error','please select user');
            return redirect()->back();
        }
        $userdiscount=UserDiscount::where('user_id',$request->user_id)->where('discount_id',$request->discount_id)->first();
        if($userdiscount==null){
            UserDiscount::create([
               'user_id'=>$request->user_id,
               'discount_id'=>$request->discount_id,
               'count'=>1
            ]);
        }
        return redirect()->back();
    }


    ########################################## discount for new user ##########################################

    //users-index
    public function newuserdiscount(){
       $discount=Discount::where('type','new-user')->first();
            return view('Admin.Discount.New-Users.index',compact('discount'));
    }

    //usersEditDiscount
    public function newuserdiscountEdit($id){
        $discount=Discount::find($id);
        return view('Admin.Discount.New-Users.edit',compact('discount'));
    }

    //usersUpdateDiscount
    public function newuserdiscountUpdate(Request $request){
        $request->validate([
            'code'=>'required|unique:discounts,code,'.$request->id,
            'percent'=>'required',
            'amount'=>'required',
            'min_order_price'=>'required',
            'start_date'=>'required',
            'enddate'=>'required',
         ]);
         $discount=Discount::find($request->id);
         $discount->update([
            'code'=>$request->code,
            'percent'=>$request->percent,
            'amount'=>$request->amount,
            'min_order_price'=>$request->min_order_price,
            'start_date'=>$request->start_date,
            'enddate'=>$request->enddate,
         ]);
        Alert::success('success', 'update discount successfuly');
        return  redirect('Super-admin/new/users/discount');
    }
}
