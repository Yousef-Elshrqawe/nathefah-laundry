<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use App\Traits\response;
use Illuminate\Http\Request;
use Auth;
use App;

class NotificationController extends Controller
{
    //
    use response;
    private NotificationRepositoryinterface $NotificationRepository;
    public function __construct(NotificationRepositoryinterface $NotificationRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
    }
    public function getnotificationsetting(Request $request){
       $user_id=Auth::guard('user_api')->user()->id;
       $lang=$request->header('lang');
       App::setLocale($lang);
       $notificationsetting=$this->NotificationRepository->getnotificationsetting($user_id,$lang);
       $data['notificationsetting']=$notificationsetting;
       return $this->response(true,'get data uccessfully',$data);
    }
    public function updatenotificationsetting(Request $request){
        $notification_id=$request->notification_id;
        $status=$request->status;
        $this->NotificationRepository->updatenotificationsetting($notification_id,$status);
        return $this->response(true,'notification update successfully');
    }
    public function updatetoken(Request $request){
         $user_id=Auth::guard('user_api')->user()->id;
         $this->NotificationRepository->createdevicetoken('user',$user_id,$request->device_token);
         return $this->response(true,'fcm token updated successfuly');
    }

    public function getnotification(Request $request){
        $user_id=Auth::guard('user_api')->user()->id;
        $lang=$request->header('lang');
        $notifications=$this->NotificationRepository->getnotification('user',$user_id,$lang);
        $data['notification']=$notifications;
        return $this->response(true,'get notification success',$data);
    }

}
