<?php
namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use App\Interfaces\UserRepositoryInterface;
use App\Models\laundryservice\Argent;
use App\Models\Order\order;
use App\Repositories\NotificationRepository;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Models\User;
use Auth;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Mail;
use Validator;
use Hash;
class ProfileController extends Controller
{
    //
    use response;
    private UserRepositoryInterface $UserRepository;
    public function __construct(UserRepositoryInterface $UserRepository , NotificationRepositoryinterface $NotificationRepository)
    {
        $this->UserRepository = $UserRepository;
        $this->NotificationRepository = $NotificationRepository;
    }
    public function edit(Request $request){
        $user_id=Auth::guard('user_api')->user()->id;
        $user= $this->UserRepository->userinfo($user_id);
        return $this->response(true,'get data success',$user);
    }
    public function editphone(Request $request){
        $user_id=Auth::guard('user_api')->user()->id;
        $user= $this->UserRepository->userinfo($user_id);
        unset($user['name']);unset($user['email']);
        return $this->response(true,'get data success',$user);
    }
    public function update(Request $request){
        $user_id=Auth::guard('user_api')->user()->id;
        $validator =Validator::make($request->all(), [
            'name'=>'required|unique:users,name,'.$user_id,
            'email'=>'required|unique:users,email,'.$user_id,
            'img'=>'image|mimes:jpeg,png,jpg,gif,svg',
          ]);
          if ($validator->fails()) {
            return $this->response(false,$validator->messages()->first(),null,403);
          }
        $user=$this->UserRepository->updateuser($request,$user_id);
        if($user==true)
        return $this->response(true,'data updated successfully',null);
        if($user==false)
        return $this->response(false,'some thing is wrong',null,403);
    }
    public function updatepassword(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $user_id=Auth::guard('user_api')->user()->id;
        if(!Hash::check($request->old_password, Auth::guard('user_api')->user()->password)) {

            if($lang == 'ar'){
                return $this->response(false, "كلمة المرور القديمة المحددة لا تتطابق مع كلمة المرور القديمة");
            }
            return $this->response(false, "The specified password does not match the old password");
        }
        $validator =Validator::make($request->all(), [
            'password'=> 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
          ]);
          if ($validator->fails()) {
            return $this->response(false,$validator->messages()->first(),null,403);
          }
        $data=$request->all();
        unset($data['password_confirmation']); unset($data['old_password']);
        $data['password']= Hash::make($request->password);
        $user=User::find($user_id);
        $user->update($data);
        // $user=$this->UserRepository->updateuser($data,$user_id);
        // if($user==true)
        if ($lang == 'ar') {
            return $this->response(true,'تم تحديث كلمة المرور بنجاح',null);
        }
        return $this->response(true,'data updated successfully',null);
        // if($user==false)
        // return $this->response(false,'some thing is wrong',null,403);
    }
    public function updatephone(Request $request){
        $user_id=Auth::guard('user_api')->user()->id;
        $validator =Validator::make($request->all(), [
            'phone'=> 'required|unique:users',
            'country_code' => 'required',
          ]);
        if ($validator->fails()) {
        return $this->response(false,$validator->messages()->first(),null,403);
        }
        $user=$this->UserRepository->updatephone($user_id,$request);
        return $this->response(true,'go to authntication request',$user);
    }
    public function verifyphone(Request $request){
        $user=Auth::guard('user_api')->user();
        $validator =Validator::make($request->all(), [
            'phone'=> 'required',
            'country_code' => 'required',
            'otp'=>'required',
          ]);
        if ($validator->fails()) {
        return $this->response(false,$validator->messages()->first(),null,403);
        }
        if($user->otp==$request->otp){
            $user->update(['phone'=>$request->phone]);
            return $this->response(true,'phone updated successfuly');
        }else{
            return $this->response(false,'this otp is false',null,403);
        }
    }
    public function getaddresses(Request $request){
        $user=Auth::guard('user_api')->user();
        $addresses= $this->UserRepository->getaddresses($user);
        if($addresses==false)
        return $this->response(false,'no address avilable',null,403);
        $data['addresses']=$addresses;
        return $this->response(true,'get addresses',$data);
    }
   public  function changelang(Request $request){
        $lang=$request->header('lang');
        $user=Auth::guard('user_api')->user();
        App::setLocale($lang);
        $user->update([
            'lang'=>$lang
        ]);
        return $this->response(true,'update language successfuly your lang become '.$lang );
   }

   //delete account
    public function deleteaccount(Request $request)
    {
        $user = Auth::guard('user_api')->user();
        $userr = User::find($user->id);
        $userr->update([
            'deleted' => "1" ,
        ]);
        Auth::guard('user_api')->logout();
        return $this->response(true, 'Account deleted successfully');
    }

    public function sendmailOrder(Request $request)
    {
        $order = Order::find($request->order_id);
        if (!$order) {
            return $this->response(false, 'order not found', null, 404);
        }
        $urgents = Argent::where('order_id', $order->id)->get();
        $user = User::find($order->user_id);
        Mail::to($request->email)->send(new \App\Mail\InvoiceMail($order, $urgents));
        return $this->response(true, 'email send successfully');
    }

    //sendnotification
    public function sendnotification(Request $request)
    {
        $userNotification = $this->NotificationRepository->sendnotification($request->type, $request->id,$request->title, $request->body, $request->notificationtype);
        return $userNotification;
    }
}
