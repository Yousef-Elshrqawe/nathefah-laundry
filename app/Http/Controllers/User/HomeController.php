<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Day;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Laundry\branch;
use Illuminate\Support\Facades\DB;
use App\Interfaces\BranchRepositoryInterface;
use App\Models\Slider;
use App\Models\Laundry\branchservice;
use App\Traits\response;
use App\Models\laundryservice\ServiceTranslation;
use App;
use Validator;
class HomeController extends Controller
{
      //
      use response;
      public function __construct(BranchRepositoryInterface $BranchRepository)
      {
          $this->BranchRepository = $BranchRepository;
      }
      public function checkrange(Request $request){
        $lat=$request->lat;
        $long=$request->lon;
        $nearst_branch=DB::table("branchs")->
        select('branchs.id as id',DB::raw("6371 * acos(cos(radians(" . $lat . "))
        * cos(radians(branchs.lat))
        * cos(radians(branchs.long) - radians(" . $long . "))
        + sin(radians(" .$lat. "))
        * sin(radians(branchs.lat))) AS distance"))
        ->where('lat','>=',$lat-0.5)->where('lat','<=',$lat+0.5)
        ->get()->min('distance');
        if($nearst_branch==null||$nearst_branch>=20){
            $data['range']=false;
            return $this->response(true,'out of range',$data);
        }
         $data['range']=true;
         return $this->response(true,'in our range 20 kilo',$data);
      }
      public function getslider(Request $request){
          $lang=$request->header('lang');
          App::setLocale($lang);
          $sliders=Slider::select('id')->get();
          $data['slider']=$sliders;
          return $this->response(true,'get sliders successfully',$data);
      }
      function sort($arr){
        $n=count($arr);
        if($n<= 1){
            return $arr;
        }
        else{
            $pivot = array();
            $pivot[0]['id'] = $arr[0]['id'];
            $pivot[0]['name'] = $arr[0]['name'];
            $pivot[0]['username'] = $arr[0]['username'];
            $pivot[0]['address'] = $arr[0]['address'];
            $pivot[0]['distance'] = $arr[0]['distance'];
            $left = array();
            $right = array();
            for($i = 1; $i < count($arr); $i++)
            {
                if($arr[$i]['distance'] < $pivot[0]['distance']){
                    $left[] = $arr[$i];
                }
                else{
                    $right[] = $arr[$i];
                }
            }
            return array_merge(self::sort($left), $pivot,self::sort($right));
        }
      }

      public $toplaundries=[];

      public function toplaundries(Request $request){
        $lat=$request->lat;
        $long=$request->lon;
          $today = Carbon::now()->format('l');
          $day = Day::where('name', $today)->first();
          $time = Carbon::now()->format('H:i:s');
        // $branches=branch::select('id','username','laundry_id','lat','long','address')->with('laundry',function($q){
        //     $q->select('name','id')->get();
        //     })->get()->take(100);
        // // but laundries in array
        // foreach($branches as $key=>$branch){
        //         $distance=$this->distance((float)$branch->lat,(float)$branch->long,(float)$lat,(float)$lon,'k');
        //         $this->toplaundries[$key]['id']=$branch->id;
        //         $this->toplaundries[$key]['name']=$branch->laundry->name;
        //         $this->toplaundries[$key]['username']=$branch->username;
        //         $this->toplaundries[$key]['address']=$branch->address;
        //         $this->toplaundries[$key]['distance']=$distance;
        // }
        // // sort array
        // $this->toplaundries=$this->sort($this->toplaundries);
        $branchs=DB::table("branchs")->
        select('branchs.id as id','laundries.name','laundries.logo','branchs.username','branchs.address','branchs_rate.rate','branchs.status'
        ,'branchs.visa','branchs.cash','branchs.delivery_status','branchs.lat','branchs.long'
        ,'laundries.door_to_door_delivery_fees as delivery_fees','laundries.one_way_delivery_fees'
        ,'laundries.taxes','laundries.taxamount'
        ,DB::raw("round(6371 * acos(cos(radians(" . $lat . "))
        * cos(radians(branchs.lat))
        * cos(radians(branchs.long) - radians(" . $long . "))
        + sin(radians(" .$lat. "))
        * sin(radians(branchs.lat))),1) AS distance"))
            ->join('schedules', function ($join) use ($day, $time) {
                $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                    ->where('schedules.scheduleable_type', '=', 'App\Models\Laundry\branch')
                    ->where('schedules.day_id', $day->id)
                    ->where('schedules.is_active', 1)
                    ->where('schedules.start_time', '<=', $time)
                    ->where('schedules.end_time', '>=', $time);
            })
        ->join('laundries','laundries.id','=','branchs.laundry_id')
        ->leftjoin('branchs_rate','branchs_rate.branch_id','=','branchs.id')
        ->join('brnachservices','brnachservices.branch_id','=','branchs.id')
        ->where('laundries.status',true)
        ->groupBy("branchs_rate.rate")
        ->groupBy("branchs.id")
        ->groupBy("branchs.lat")
        ->groupBy("branchs.visa")
        ->groupBy("branchs.cash")
        ->groupBy("branchs.delivery_status")
        ->groupBy("laundries.door_to_door_delivery_fees")
        ->groupBy("laundries.taxes")
        ->groupBy("laundries.taxamount")
        ->groupBy("laundries.one_way_delivery_fees")
        ->groupBy("branchs.long")
        ->groupBy("branchs.address")
        ->groupBy("branchs.username")
        ->groupBy("branchs.status")
        ->groupBy("laundries.name")
        ->groupBy("laundries.logo")
        ->havingRaw('distance <= 20')
        ->orderby('distance','ASC')
        ->orderby('rate','desc')
        ->get();
        //->count();
     //   dd($branchs);
        foreach($branchs as $branch){
            if($branch->logo==null){
                $branch->logo=asset("/uploads/branches/logos/default/images.jpg");
            }else{
                $branch->logo=asset("uploads/laundry/logos/".$branch->logo);
            }
            //$branch->rate=$this->BranchRepository->branchrate($branch->rate);
        }
         $data['top_laundries']=$branchs;


//         $distance=$this->BranchRepository->distance($lat,$long,$branch->lat,$branch->long,'k');
         return $this->response(true,'top laundries',$data);
        return $distance;
      }
      // in this functoin we search with [service or laundry name]
      public $services_id=[];
      public function search(Request $request){
          $today = Carbon::now()->format('l');
          $day = Day::where('name', $today)->first();
          $time = Carbon::now()->format('H:i:s');

          //dd($request->all());
        $validator =Validator::make($request->all(), [
            'search_key'=>'required',
            'lat'=>'required',
            'long'=>'required'
            ]);
            if ($validator->fails()) {
                return response()->json(['message'=>$validator->messages()->first()],403);
            }
            $lat=$request->lat;
            $long=$request->long;
        $services=DB::table('servicetranslations')->select('service_id')->where('name','like',"%{$request->search_key}%")
        ->get();
        foreach($services as $service){
            array_push($this->services_id,$service->service_id);
        }
        $branchs=DB::table("branchs")->
        select('branchs.id as id','branchs.username','laundries.name','laundries.logo','branchs_rate.rate as rate'
        ,'branchs.visa','branchs.cash','branchs.delivery_status','branchs.status'
        ,'laundries.door_to_door_delivery_fees as delivery_fees','laundries.one_way_delivery_fees'
        ,'laundries.taxes','laundries.taxamount'
        ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
        * cos(radians(branchs.lat))
        * cos(radians(branchs.long) - radians(" . $long . "))
        + sin(radians(" .$lat. "))
        * sin(radians(branchs.lat))) AS distance"))
            ->join('schedules', function ($join) use ($day, $time) {
                $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                    ->where('schedules.scheduleable_type', '=', 'App\Models\Laundry\branch')
                    ->where('schedules.day_id', $day->id)
                    ->where('schedules.is_active', 1)
                    ->where('schedules.start_time', '<=', $time)
                    ->where('schedules.end_time', '>=', $time);
            })
       ->where('lat','>=',$lat-0.5)->where('lat','<=',$lat+0.5)
       ->join('laundries','laundries.id','=','branchs.laundry_id')
       ->join('brnachservices','brnachservices.branch_id','=','branchs.id')
       ->leftjoin('branchs_rate','branchs_rate.branch_id','=','branchs.id')
       ->groupBy("branchs.id")
       ->groupBy("branchs.username")
       ->groupBy("branchs_rate.rate")
       ->groupBy("laundries.name")
       ->groupBy("branchs.visa")
       ->groupBy("branchs.status")
       ->groupBy("branchs.cash")
       ->groupBy("branchs.delivery_status")

       ->groupBy("laundries.door_to_door_delivery_fees")
       ->groupBy("laundries.one_way_delivery_fees")
       ->groupBy("laundries.taxes")
       ->groupBy("laundries.taxamount")
       ->groupBy("laundries.logo")
       ->orderby('distance','ASC')
       ->groupBy("branchs.lat")
       ->groupBy("branchs.long")
       ->orderby('rate','ASC')
       ->where('branchs.status','open')
       ->wherein('brnachservices.service_id',$this->services_id)
       ->paginate(50);
        if(!$branchs->isEmpty()){
            foreach($branchs as $branch){
                if($branch->logo==null){
                    $branch->logo=asset("/uploads/branches/logos/default/images.jpg");
                }else{
                    $branch->logo=asset("uploads/laundry/logos/".$branch->logo);
                }
               // $branch->rate=$this->BranchRepository->branchrate($branch->rate);
            }
        }
        if($branchs->isEmpty()){
            $branchs=DB::table("branchs")->
            select('branchs.id as id','branchs.username','laundries.name','laundries.logo','branchs_rate.rate as rate','branchs.status'
            ,'branchs.visa','branchs.cash','branchs.delivery_status'
            ,'laundries.door_to_door_delivery_fees as delivery_fees','laundries.one_way_delivery_fees'
             ,'laundries.taxes','laundries.taxamount'

            ,DB::raw("6371 * acos(cos(radians(" . $lat . "))
            * cos(radians(branchs.lat))
            * cos(radians(branchs.long) - radians(" . $long . "))
            + sin(radians(" .$lat. "))
            * sin(radians(branchs.lat))) AS distance"))
                ->join('schedules', function ($join) use ($day, $time) {
                    $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                        ->where('schedules.scheduleable_type', '=', 'App\Models\Laundry\branch')
                        ->where('schedules.day_id', $day->id)
                        ->where('schedules.is_active', 1)
                        ->where('schedules.start_time', '<=', $time)
                        ->where('schedules.end_time', '>=', $time);
                })
            ->join('laundries','laundries.id','=','branchs.laundry_id')
            ->leftjoin('branchs_rate','branchs_rate.branch_id','=','branchs.id')
            ->groupBy("branchs.id")
            ->groupBy("branchs.username")
            ->groupBy("branchs_rate.rate")

            ->groupBy("branchs.visa")
            ->groupBy("branchs.cash")
            ->groupBy("branchs.delivery_status")
            ->groupBy("laundries.door_to_door_delivery_fees")
            ->groupBy("laundries.one_way_delivery_fees")
            ->groupBy("branchs.status")
            ->groupBy("laundries.taxes")
            ->groupBy("laundries.taxamount")
            ->groupBy("laundries.name")
            ->groupBy("laundries.logo")
            ->groupBy("branchs.lat")
            ->groupBy("branchs.long")
            ->orderby('rate','ASC')
             ->where('laundries.name','like',"{$request->search_key}%")
             ->where('branchs.status','open')
             ->where('laundries.status',true)
             ->paginate(50);
            foreach($branchs as $branch){
                if($branch->logo==null){
                    $branch->logo=asset("/uploads/branches/logos/default/images.jpg");
                }else{
                    $branch->logo=asset("uploads/laundry/logos/".$branch->logo);
                }
               // $branch->rate=$this->BranchRepository->branchrate($branch->rate);
            }
        }
        return $branchs;
        $data['branchs']=$branchs;
        return $this->response(true,'get all pranches successfuly',$data);
      }

       public function about(){

       }


}
