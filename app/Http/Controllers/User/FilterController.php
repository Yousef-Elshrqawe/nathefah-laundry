<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Day;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Interfaces\BranchRepositoryInterface;
use App\Traits\response;
use App;

class FilterController extends Controller
{
    //
    use response;

    public function __construct(BranchRepositoryInterface $BranchRepository)
    {
        $this->BranchRepository = $BranchRepository;
    }

    public function getadditionalservice(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $additionalservices = DB::table('additionalservicetranslations')
            ->select('additionalservice_id', 'name')->where('locale', $lang)->get();
        $data['additionalservices'] = $additionalservices;
        return $this->response(true, 'get additional service successfuly', $data);
    }

    public function filterLaundry(Request $request)
    {
        // الحصول على اليوم الحالي والمواعيد
        $today = Carbon::now()->format('l');
        $day   = Day::where('name', $today)->first();
        $time  = Carbon::now()->format('H:i:s');

        // الحصول على الإحداثيات من الطلب
        $lat  = $request->lat;
        $long = $request->long;

        // إعداد خيارات الفلترة:
        // إذا كان sortname true فهذا يعني ترتيب النتائج أبجدياً بناءً على اسم المغسلة.
        $sortNameSelected = $request->sort_alphapitic ? true : false;
        // فلترة الفروع العاجلة: إذا كان urgent موجود وصحيح، نجلب فقط الفروع العاجلة.
        $urgentFilter     = $request->has('urgent') && $request->urgent ? true : false;
        // الحد الأدنى للتقييم: إذا كان top_rated true، يكون الحد 2، وإلا 0.
        $topRated         = $request->top_rated ? 2 : 0;
        // ترتيب التقييم: إذا كان top_rate_sort true نستخدم 'desc'، وإلا 'asc'.
        $topRateSort      = $request->top_rate_sort ? 'desc' : 'asc';



        // بدء بناء الاستعلام الأساسي باستخدام DB::table
        $query = DB::table('branchs')
            ->select(
                'branchs.id as id',
                'branchs.username',
                'laundries.name',
                'laundries.logo',
                'branchs_rate.rate as rate'
            )
            ->selectRaw(
                "ROUND(6371 * ACOS(COS(RADIANS(?)) * COS(RADIANS(branchs.lat)) * COS(RADIANS(branchs.long) - RADIANS(?)) + SIN(RADIANS(?)) * SIN(RADIANS(branchs.lat))), 1) AS distance",
                [$lat, $long, $lat]
            )
            // الانضمام لجداول أساسية
            ->join('laundries', 'laundries.id', '=', 'branchs.laundry_id')
            ->leftJoin('branchs_rate', 'branchs_rate.branch_id', '=', 'branchs.id')
            // الانضمام لجدول الجداول الزمنية (schedules) للتحقق من أن الفرع يعمل في الوقت الحالي
            ->join('schedules', function ($join) use ($day, $time) {
                $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                    ->where('schedules.scheduleable_type', 'App\Models\Laundry\branch')
                    ->where('schedules.day_id', $day->id)
                    ->where('schedules.is_active', 1)
                    ->where('schedules.start_time', '<=', $time)
                    ->where('schedules.end_time', '>=', $time);
            })
            // الشروط الأساسية للفرع والمغسلة
            ->where('branchs.status', 'open')
            ->where('laundries.status', true)
            ->where('branchs.deleted', 1)
            ->where('branchs.rate', '>=', $topRated)
            // فلترة جغرافية: تحديد النطاق في خطوط العرض وخطوط الطول
            ->whereBetween('branchs.lat', [$lat - 0.2, $lat + 0.2])
            ->whereBetween('branchs.long', [$long - 0.2, $long + 0.2]);

        if ($request->has('free_delivery') && $request->free_delivery ==  "true") {
            $query->where('branchs.delivery_fees', 0);
        }

        // تطبيق فلترة الفروع العاجلة إذا طلب المستخدم ذلك
        if ($urgentFilter) {
            // تأكد من استخدام اسم العمود الصحيح؛ هنا نفترض أنه "argent" كما في الكود الأصلي
            $query->where('branchs.argent', 1);
        }

        // إضافة فلترة الخدمات إذا كانت موجودة في الطلب
        if (!empty($request->services)) {
            $services = is_array($request->services) ? $request->services : [$request->services];
            $query->join('brnachservices', 'brnachservices.branch_id', '=', 'branchs.id')
                ->whereIn('brnachservices.service_id', $services);
        }

        // إضافة فلترة الخدمات الإضافية إذا كانت موجودة
        if (!empty($request->additional_service)) {
            $additionalServices = is_array($request->additional_service) ? $request->additional_service : [$request->additional_service];
            $query->join('branch_additionalservice', 'branch_additionalservice.branch_id', '=', 'branchs.id')
                ->whereIn('branch_additionalservice.additionalservice_id', $additionalServices);
        }

        // تجميع النتائج لتفادي تكرار الصفوف
        $query->groupBy('branchs.id', 'branchs.lat', 'branchs.long', 'branchs.username', 'branchs_rate.rate', 'laundries.name', 'laundries.logo');

        // فلترة النتائج بناءً على المسافة المحسوبة
        $query->havingRaw('distance <= 20');

        // ترتيب النتائج:
        if ($sortNameSelected) {
            // إذا تم اختيار الترتيب الأبجدي، نطبق ترتيب واحد فقط بناءً على اسم المغسلة (غير حساس لحالة الأحرف)
            $query->orderByRaw("LOWER(laundries.name) ASC");
        } else {
            // الترتيب الافتراضي: حسب التقييم أولاً، ثم المسافة، ثم اسم المغسلة (ترتيب تنازلي)
            $query->orderBy('branchs.rate', $topRateSort)
                ->orderBy('distance', 'ASC')
                ->orderByRaw("LOWER(laundries.name) DESC");
        }

        // تنفيذ الاستعلام مع التصفح (pagination)
        $branchs = $query->paginate(50);

        // تحديث رابط الصورة لكل فرع
        foreach ($branchs as $branch) {
            $branch->logo = $branch->logo
                ? asset("uploads/laundry/logos/" . $branch->logo)
                : asset("/uploads/branches/logos/default/images.jpg");
        }

        return $branchs;
    }




}
