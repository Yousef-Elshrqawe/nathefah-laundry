<?php

namespace App\Http\Controllers\User;

use App\Interfaces\{DiscountRepositoryInterface,
    OrderFinshRepositoryInterface,
    OrderRepositoryInterface,
    TransactionRepositoryInterface,
    BranchRepositoryInterface,
    SystemRepositoryInterface,
    NotificationRepositoryinterface
};
use Illuminate\Support\Facades\Mail;


use App\Models\Order\{OrderDiscount, payment_method, OrderEditLogs, OrderEditDetailes, ArgentEditLogs};
use App\Models\Order\{order, orderdetailes, Orderreview, OrderDriveryStatus, delivery_type};
use App\Models\Laundry\{branch, Branchitem, branchservice, Branchuser, Branchrate};
use App\Models\laundryservice\{Argent, Service, Additionalservice};
use App\Models\Discount\{BranchDiscount, UserDiscount, Discount};
use App\Models\{ApplicationRates,
    Day,
    Notification\branchnotifytype,
    Notification\drivernotifytype,
    Transaction,
    User,
    TransactionFeedback,
    Wallet\user_wallet
};
use App\Repositories\{DiscountRepository, WalletRepository};
use App\Models\Driver\{driverrate, Driver, deliveryrate};
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Models\System\Systeminfo;
use Illuminate\Http\Request;
use App\Models\User\Adress;
use App\Traits\response;
use Carbon\Carbon;
use Validator;
use App;
use Auth;
use Arr;
use PDF;

class OrderController extends Controller
{
    //
    use response;
    use App\Traits\otp;

    /*    private $access_code = 'ECLmBCXFjMVcehgasQ8d';
        private $merchant_identifier = '00f1ca9c';
        private $sha_request_phrase = '46PXPeVmlpRrScEAVgVV9U)]';
        private $sha_type = 'sha256';
        private $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';*/


    private $access_code = 'U1xDaJUuiyE8hA2OAE70';
    private $merchant_identifier = 'EIIWLjkk';
    private $sha_request_phrase = '15KHEd88OBA6MoXh4BtVD9$&';
    private $sha_type = 'sha256';
    private $url = 'https://paymentservices.payfort.com/FortAPI/paymentApi';
    private $urlStc = 'https://checkout.payfort.com/FortAPI/paymentPage';


    public function __construct(OrderRepositoryInterface       $OrderRepository, BranchRepositoryInterface $BranchRepository, DiscountRepositoryInterface $DiscountRepository
        , WalletRepository                                     $WalletRepository, NotificationRepositoryinterface $NotificationRepository
        , SystemRepositoryInterface                            $SystemRepository,
                                TransactionRepositoryInterface $TransactionRepository
    )
    {
        $this->OrderRepository = $OrderRepository;
        $this->BranchRepository = $BranchRepository;
        $this->DiscountRepository = $DiscountRepository;
        $this->WalletRepository = $WalletRepository;
        $this->NotificationRepository = $NotificationRepository;
        $this->SystemRepository = $SystemRepository;
        $this->TransactionRepository = $TransactionRepository;
    }

    #Reigon[this is show order cycle & make order]
    public function getservices(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $services = Service::select('id', 'img')->get();
        $data['services'] = $services;
        return $this->response(true, 'get services succefully', $data);
    }

    public function laundryservices(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $services = $this->OrderRepository->laundryservices($request->branch_id, $lang);
        $data['services'] = $services;
        return $this->response(true, 'get services succefully', $data);
    }

    public function selectlaundry(Request $request)
    {
        $today = Carbon::now()->format('l');
        $day = Day::where('name', $today)->first();
        $time = Carbon::now()->format('H:i:s');
        $lat = $request->lat;
        $long = $request->long;
        $branchs = DB::table("branchs")
            ->select(
                'branchs.id as id',
                'branchs.username',
                'laundries.name',
                'laundries.logo',
                'branchs.argent',
                'branchs_rate.rate as rate',
                'branchs.visa',
                'branchs.cash',
                'branchs.delivery_status',
                'branchs.lat',
                'branchs.long',
                'laundries.door_to_door_delivery_fees as delivery_fees',
                'laundries.one_way_delivery_fees',
                'laundries.taxes',
                'laundries.taxamount',
                DB::raw("ROUND(6371 * ACOS(COS(RADIANS(" . $lat . "))
            * COS(RADIANS(branchs.lat))
            * COS(RADIANS(branchs.long) - RADIANS(" . $long . "))
            + SIN(RADIANS(" . $lat . "))
            * SIN(RADIANS(branchs.lat))), 1) AS distance")
            )
            ->join('schedules', function ($join) use ($day, $time) {
                $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                    ->where('schedules.scheduleable_type', '=', 'App\Models\Laundry\branch')
                    ->where('schedules.day_id', $day->id)
                    ->where('schedules.is_active', 1)
                    ->where('schedules.start_time', '<=', $time)
                    ->where('schedules.end_time', '>=', $time);
            })
            ->join('laundries', 'laundries.id', '=', 'branchs.laundry_id')
            ->join('brnachservices', 'brnachservices.branch_id', '=', 'branchs.id')
            ->leftJoin('branchs_rate', 'branchs_rate.branch_id', '=', 'branchs.id')
            ->whereIn('brnachservices.service_id', $request->services)
            ->where('laundries.status', 1)
            ->where('branchs.status', 'open')
            ->groupBy([
                "branchs.id",
                "branchs.lat",
                "branchs.long",
                "branchs.visa",
                "branchs.cash",
                "branchs.argent",
                "laundries.door_to_door_delivery_fees",
                "laundries.one_way_delivery_fees",
                "laundries.taxes",
                "laundries.taxamount",
                "branchs.username",
                "branchs_rate.rate",
                "branchs.delivery_status",
                "laundries.name",
                "laundries.logo"
            ])
            ->havingRaw('distance <= 20')
            ->orderBy('distance', 'ASC')
            ->paginate(30);

        foreach ($branchs as $branch) {
            if ($branch->logo == null)
                $branch->logo = asset('uploads/laundry/logos/default/images.jpg');
            $branch->logo = asset('uploads/laundry/logos/' . $branch->logo);
        }
        return $branchs;

    }

    public function chooselaundry(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $branch_id = $request->branch_id;
        $branch = branch::find($branch_id);
        if ($branch == null) {
            return $this->response(false, 'branch not found', null, 404);
        }
        $data = [];
        $branchservices = branchservice::select('service_id')
            ->wherein('service_id', $request->services)
            ->where('branch_id', $branch_id)->where('status', 'on')->get();
        foreach ($branchservices as $branchservice) {
            array_push($data, $branchservice->service_id);
        }
//         $services = Service::wherein('id', $this->service_ids)->select('id')->with('categories')->get()->makehidden(['created_at', 'updated_at']);
        //هاتلي  الاقسام اللي موجوده في الخدمات اللي انا اخترتها الي  ليها ايتم فقط
        $services = Service::wherein('id', $data)->with(['categories' => function ($q) use ($branch_id) {
            $q->whereHas('branchitems', function ($q) use ($branch_id) {
                $q->where('branch_id', $branch_id);
            });
        }])->get();
        $data['services'] = $services;
        $data['branchargent'] = $branch->argent;
        return $this->response(true, 'get services succefully', $data);
    }

    public function getcategoryitems(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $categoryitems = $this->OrderRepository->getcategoryitems($request->category_id, $request->service_id, $request->branch_id, $lang);
        return $categoryitems;
    }

    public function itemdetailes(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $itemdetailes = $this->OrderRepository->itemdetailes($request->item_id, $lang);
        $data['itemdetailes'] = $itemdetailes;
        return $this->response(true, 'return item detailes successfulty', $data);
    }

    public function submitorder(Request $request)
    {
        $order = $this->OrderRepository->submitorder($request);
        if ($order == 'urgentfalse') {
            return $this->response(false, 'this branch closed urgent', null, 422);
        }
        if ($order == 'curentaddressfalse') {
            return $this->response(false, 'please select your current address first', null, 422);
        }

        return $this->response(true, 'order creates success', ['order_id' => $order]);
    }


    public function calculatDestination(Request $request)
    {
        $branch = branch::find($request->branch_id);
        $distance = $this->BranchRepository->distance($branch->lat, $branch->long, $request->lat, $request->long, 'k');
        $data['distance'] = $distance;
        return $this->response(true, 'distance calculated successfully', $data);
    }

    #Reigon[this is check out page]
    public function opentime(Request $request)
    {
        $branch_id = $request->branch_id;
        $opentime = $this->BranchRepository->getopentime($branch_id);
        if ($opentime == false)
            return $this->response(false, 'some thing is wrong', $data = null, 401);
        return $this->response(true, 'opening hours get successfuly', $opentime);
    }

    // this function get all info about order and branch after check out android devloper only use this request
    public function branchinfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch_id' => 'required',
            'order_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->response(false, $validator->messages()->first(), null, 401);
        }
        $branch_id = $request->branch_id;
        $lang = $request->header('lang');
        $order_id = $request->order_id;
        App::setLocale($lang);
        $user_id = Auth::guard('user_api')->user()->id;
        // get address of user
        $adresses = Adress::where('user_id', $user_id)->get();
        $data['address'] = $adresses;
        //start open time
        $opentime = $this->BranchRepository->getopentime($branch_id);
        if ($opentime == false)
            return $this->response(false, 'some thing is wrong', $data = null, 401);
        $data['opentime'] = $opentime;
        // end open time
        // start get date
        $date = [];
        for ($i = 0; $i < 7; $i++) {
            array_push($date, date('d-m-y', strtotime("+$i days")));
        }
        $data['date'] = $date;
        // end get date
        // start get order summary
        $deliverytype = delivery_type::select('id')->get()->makehidden('translations');
        // $paymentmethods=payment_method::select('id')->get()->makehidden('translations');
        $orderdetailes = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'order_detailes.service_id')
            ->selectRaw('sum(price) as total')
            ->selectRaw('sum(quantity) as quantity')
            ->selectRaw('servicetranslations.name')
            ->where('order_detailes.order_id', $order_id)
            ->where('servicetranslations.locale', $lang)
            ->where('order_detailes.additionalservice_id', '=', null)
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();
        $order = order::find($order_id);
        $argentprice = Argent::where('order_id', $order_id)->sum('price');
        $additionalservice = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('additionalservicetranslations', 'additionalservicetranslations.additionalservice_id', '=', 'order_detailes.additionalservice_id')
            ->where('additionalservicetranslations.locale', $lang)
            ->selectRaw('additionalservicetranslations.name')
            ->selectRaw('sum(price) as price')
            ->selectRaw('quantity')
            ->where('order_detailes.additionalservice_id', '!=', null)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_detailes.quantity')
            ->get();


        $paymentmethods = $this->BranchRepository->get_paymet_methods($branch_id, $lang);


        $totaladditionalprice = 0;
        foreach ($additionalservice as $addtional) {
            $totaladditionalprice += $addtional->price;
        }
        $total_service_price = 0;
        foreach ($orderdetailes as $orderdetail) {
            $total_service_price += $orderdetail->total;
        }
        $data['status'] = true;
        $data['message'] = "return order info succeffuly";
        $data['deliverytype'] = $deliverytype;
        $data['paymentmethods'] = $paymentmethods;
        $data['orderdetailes'] = $orderdetailes;
        $data['argentprice'] = $argentprice;
        $data['additionalservices'] = $additionalservice;
        $data['totaladditionalprice'] = $totaladditionalprice;
        $data['price_after_discount'] = $order->price_after_discount;
        $data['price_before_discount'] = $order->price_before_discount;
        $data['total_service_price'] = $total_service_price;
        $data['taxes'] = $order->taxes;

        // end get order summary
        return $this->response(true, 'opening branch info successfuly', $data);
    }


    public function ordersummary(Request $request)
    {
        $ordersummary = $this->OrderRepository->ordersummary($request);
        return $ordersummary;
    }
    // in this function user paied by three wayes(cash/visa/wallet) & three way of delivery(self_delivery | one way delivery | by delivey)
    // user can use discount in this function

    public function checkout(Request $request)
    {
        if ( $request->header('lang') == null) {
            $lang = 'ar';
        } else {
            $lang = $request->header('lang');
        }

        App::setLocale($lang);
//        try {
        $payment_method_id = null;
        if ($request->payment_method_id != null) {
            $payment_method_id = $request->payment_method_id;
        }

        if ($request->pymenttype_id == 1) {
            if ($request->merchant_reference != null) {
                $transactionfeedback = TransactionFeedback::where('fort_id', $request->fort_id)->first();
                if ($transactionfeedback == null) {
                    return $this->response(false, 'payment transaction error');
                } else {
                    $transactionfeedback->update([
                        'order_id' => $request->order_id,
                    ]);
                }
            }
        }
        $today = Carbon::now()->format('l');
        $day = Day::where('name', $today)->first();
        $time = Carbon::now()->format('H:i:s');

        $user_id = Auth::guard('user_api')->user()->id;
        $user = User::find($user_id);
        $order_id = $request->order_id;
        $branchid = $request->branch_id;
        $branchUser = Branchuser::where('branch_id', $branchid)->first();
        $branch = branch::find($branchid)->schedules()
            ->where('day_id', $day->id)
            ->where('is_active', 1)
            ->where('start_time', '<=', $time)
            ->where('end_time', '>=', $time)
            ->first();

        //هاتلي  موعيد  الفرع  انهرضه ويكون اكبر من الوقت الحالي
        if ($branch == null) {
            // ابحث عن أول جدول يبدأ بعد الوقت الحالي (الجدول القادم)
            $branchTime = branch::find($branchid)->schedules()
                ->where('day_id', $day->id)
                ->where('is_active', 1)
                ->where('start_time', '>=', $time)
                ->first();

            if ($branchTime != null) {
                if ($lang == 'ar') {
                    $start_time = $this->formatTimeArabic($branchTime->start_time);
                    $end_time   = $this->formatTimeArabic($branchTime->end_time);
                    return $this->response(false, 'تم إغلاق هذا الفرع وسيكون متاح في الفترة من ' . $start_time . ' إلى ' . $end_time, null, 400);
                } else {
                    // للغة الإنجليزية أو غيرها
                    $start_time = date("h:i A", strtotime($branchTime->start_time));
                    $end_time   = date("h:i A", strtotime($branchTime->end_time));
                    return $this->response(false, 'This branch is closed and will be available from ' . $start_time . ' to ' . $end_time, null, 400);
                }
            } else {
                return $this->response(false, 'الفرع مغلق اليوم وليس لديه مواعيد متاحة حتى الآن', null, 400);
            }
        }

        $pymenttype_id = 2;
        $order_delivery_status = true;

        $order = order::where('id', $order_id)->where('branch_id', $branchid)->first();


        if ($order == null) {
            $data['status'] = false;
            $data['message'] = 'order not found';
            return response()->json($data, 405);
        } else {
            if ($order->checked == true) {
                return $this->response(false, 'this order alerdy checked', $data = null, 422);
            }
            // if order found
            DB::beginTransaction();
            if ($request->delivery_fees != null)
                $this->OrderRepository->add_delivery_fees($order, $request->delivery_fees);
            // check order payment-type
            $payed = false;
            // 1 refer to wallet 3 refer to visa
            if ($request->pymenttype_id == 1 || $request->pymenttype_id == 3) {
                $payed = true;
            }
            // delvivery consisit of three main type(self delivery - one way delivery - by delivery)
            #Reigon[start delivery type]
            if ($request->delivery_type == 'bydelivery') {
                $delivery_type_id = 1;
                $order_status = 'pick_up_home';
            } elseif ($request->delivery_type == 'on_way_delivery') {
                //start validate way of delivery
                $validator = Validator::make($request->all(), [
                    'way_delivery' => 'required',
                ]);
                if ($validator->fails()) {
                    return response()->json([
                        'message' => $validator->messages()->first()
                    ], 422);
                }
                //  end validate way of delivery
                if ($request->way_delivery == 'home_drop_of') {
                    $delivery_type_id = 3;
                    $order_delivery_status = false;
                    $order_status = 'pick_up_laundry';
                } elseif ($request->way_delivery == 'self_drop_of') {
                    $delivery_type_id = 4;
                    $order_status = 'pick_up_home';
                } else {
                    return response()->json(['message' => 'the way delivery input is false'], 422);
                }
            } elseif ($request->delivery_type == 'self_delivery') {
                $order_delivery_status = false;
                $delivery_type_id = 2;
                $order_status = null;

                OrderDriveryStatus::create([
                    'order_id' => $order->id,
                    'driver_id' => null,
                    'order_status' => 'drop_of_laundry',
                    'code' => rand(999, 9999),
                ]);
            }
            if ($delivery_type_id == 1 || $delivery_type_id == 3 || $delivery_type_id == 4) {
                OrderDriveryStatus::create([
                    'order_id' => $order->id,
                    'driver_id' => null,
                    'order_status' => $order_status,
                    'code' => rand(999, 9999),
                ]);
            }
            #EndReigon[delivery type]
            // this table use to check delivery status of orders
            // start if user use discount
            if ($request->discount_code != null) {
                //this return the value of mony after discount
                $usediscount = $this->DiscountRepository->usediscount($order->id, $request->discount_code , $lang);
                if (is_array($usediscount)) {
                    $message = $usediscount['message'];
                    return $this->response(false, $message, null, 401);
                }
                // start if user use wallet to pay his order
                if ($request->pymenttype_id == 1) {
                    $walletamount = $this->WalletRepository->getpallence($user_id);
                    if ($walletamount < $usediscount) {
                        if ($lang == 'ar') {
                            return $this->response(false, 'لا يوجد لديك رصيد كافي في المحفظة');
                        } else {
                            return $this->response(false, 'you have not enough money in wallet');
                        }
                    }//
                    //$this->WalletRepository->increesebrnachwallet($order->branch_id,$usediscount);
                    $this->WalletRepository->decreaseuserwallet($usediscount, $user_id);
                    $discount = Discount::where('code', $request->discount_code)->first();
                    $userdiscount = UserDiscount::where('user_id', $user_id)->where('discount_id', $discount->id)->first();
                    $userdiscount->update([
                        'count' => $userdiscount->count - 1
                    ]);
                } else {
                    $discount = Discount::where('code', $request->discount_code)->first();
                    $userdiscount = UserDiscount::where('user_id', $user_id)->where('discount_id', $discount->id)->first();
                    $userdiscount->update([
                        'count' => $userdiscount->count - 1
                    ]);
                }
                //end wallet
                $order->update([
                    'day' => $request->day,
                    'from' => date("H:i:s", strtotime($request->from)),
                    'to' => date("H:i:s", strtotime($request->to)),
                    'delivery_type_id' => $delivery_type_id,
                    'price_after_discount' => $usediscount,
                    'pymenttype_id' => $request->pymenttype_id,
                    'order_delivery_status' => $order_delivery_status,
                    'payment_method_id' => $payment_method_id,
                    'payed' => $payed,
                    'checked' => true,
                    'fort_id' => $request->fort_id
                ]);
//                    $this->OrderRepository->transformmonytopoints($usediscount);
            }
            //end user has discount
            // user has no discount
            else {
                // add balance for system
                //   if($request->pymenttype_id==1||$request->pymenttype_id==3){
                //     $delivery_fees=0;
                //     if($request->delivery_fees!=null)
                //     $delivery_fees=$request->delivery_fees;
                //     $this->SystemRepository->increasebalancereciveorder($order->price_before_discount);
                //    }
                if ($request->pymenttype_id == 1) {
                    $walletamount = $this->WalletRepository->getpallence($user_id);
                    if ($walletamount < $order->price_before_discount) {
                        if ($lang == 'ar') {
                            return $this->response(false, 'لا يوجد لديك رصيد كافي في المحفظة');
                        } else {
                            return $this->response(false, 'you have not enough money in wallet');
                        }
                    }
                    //$this->WalletRepository->increesebrnachwallet($order->branch_id,$usediscount);
                    $this->WalletRepository->decreaseuserwallet($order->price_before_discount, $user_id);
                }
                //end wallet
                $order->update([
                    'day' => $request->day,
                    'from' => date("H:i:s", strtotime($request->from)),
                    'to' => date("H:i:s", strtotime($request->to)),
                    'delivery_type_id' => $delivery_type_id,
                    'order_delivery_status' => $order_delivery_status,
                    'payment_method_id' => $payment_method_id,
                    'pymenttype_id' => $request->pymenttype_id,
                    'payed' => $payed,
                    'checked' => true,
                    'fort_id' => $request->fort_id
                ]);
                // charge point
//                    $this->OrderRepository->transformmonytopoints($order->price_before_discount);
            }
            // end user has no discount
            // start send notification to branch
            $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
            $notf_s = DB::table('branchnotifytype')->where('user_branch_id', $Branchusers->first()->id)
                ->where('notificationtype_id', '1')
                ->where('status', '1')->first();

            if ($notf_s) {
                try {
                    foreach ($Branchusers as $Branchuser) {
                        if ($Branchuser->lang == 'ar') {
                            $title = 'طلب جديد';
                            $body = 'لقد استلمت طلب جديد رقم الطلب ' . $order->id;
                        }
                        if ($Branchuser->lang == 'en') {
                            $title = 'New Order';
                            $body = 'You have received a new order number ' . $order->id;
                        }
                        $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 1);
                    }
                } catch (\Exception $e) {

                }


                $titles = ['ar' => 'طلب جديد', 'en' => 'New Order'];
                $bodys = ['ar' => 'لقد استلمت طلب جديد رقم الطلب ' . $order->id, 'en' => 'You have received a new order number ' . $order->id];
                $this->NotificationRepository->createnotification('branch', $order->branch_id, $titles, $bodys);
                // end send notification to branch
            }


            // start send notification to user
            $userLanguage = $user->lang;
            if ($userLanguage == 'ar') {
                $title = 'تم ارسال طلبك الي  المغسله';
                $body = 'تم ارسال طلبك الي  المغسله' . $order->branch->username . 'وفي انتظار تاكيد الطلب من المغسله';
            } else {
                $title = 'Your order has been sent to the laundry';
                $body = 'Your order has been sent to the laundry ' . $order->branch->username . ' and waiting for the laundry to confirm the order';
            }
            try {
                $usernotifytype = DB::table('usernotifytype')->where('user_id', $user_id)->where('notificationtype_id', '1')->where('status', '1')->first();
                if ($usernotifytype) {
                    $this->NotificationRepository->sendnotification('user', $user_id, $title, $body, 1);
                }
            } catch (\Exception $e) {

            }
            /*  if ($userNotification == false) {
                  return $this->response(false, 'notification not send', null, 405);
              }*/
            // end send notification to user


            // system transaction
            if ($request->pymenttype_id == 1 || $request->pymenttype_id == 3) {
                $price = $this->OrderRepository->getprice($order);
                $this->TransactionRepository->create_system_transaction($order, $price);
            }


            $order->load(['paymenttype', 'deliverytype', 'orderdetailes' => function ($q) {
                $q->with('service', 'Branchitem', 'additionalservice')->get();
            }]);
            $urgents = Argent::where('order_id', $order->id)->get();

            Mail::to($user->email)->send(new \App\Mail\InvoiceMail($order, $urgents));

            /*$this->sendMessage($user->country_code . $user->phone, __('auth.Your request has been received successfully'));*/
            $this->sendMessage($user->country_code . $user->phone, app()->getLocale() == 'ar' ? 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username : 'Your order ' . '(' . $order->id . ')' . ' has been sent to the laundry ' . $order->branch->username);


            DB::commit();
            /*      try {
                      Mail::to($user->email)->send(new \App\Mail\InvoiceMail($order, $urgents));
                  } catch (\Exception $e) {

                      //اظهار  الخطاء

                      return $e->getMessage();
                  }*/
            #EndReigon[send email]

            /*    $country_code = $user->country_code;
                $phone = $user->phone;
                $phonen = $country_code . $phone;

                \App\Jobs\SendSmsJob::dispatch($phonen, 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username);*/


            $data['status'] = true;
            $data['message'] = 'order checked  successfully';
            return response()->json($data);
        }
//        } catch (\Exception $e) {
//            DB::rollback();
//            return $this->response(false, __('response.wrong'), null, 406);
//        }
    }

    /**
     * تحويل وقت بنمط 24 ساعة (أو 12 ساعة) إلى صيغة عربية مع صباحًا / مساءً
     *
     * @param  string  $timeValue  وقت بصيغة مثل "09:00:00" أو "23:30:00"
     * @return string
     */
    function formatTimeArabic($timeValue)
    {
        // سنعرض الساعة والدقيقة فقط مثلاً 09:30
        $formatTime = date("h:i", strtotime($timeValue));
        // لمعرفة AM أو PM
        $amOrPm = date("A", strtotime($timeValue)); // سترجع "AM" أو "PM"

        if ($amOrPm === 'AM') {
            return $formatTime . ' صباحًا';
        } else {
            return $formatTime . ' مساءً';
        }
    }





    // generate Invoice PDF

    /*    public function generateInvoice(Request $request)
        {
            $order = Order::find($request->order_id);
            $urgents = Argent::where('order_id', $order->id)->get();
            $pdf = PDF::loadView('pdf.invoice', compact('order', 'urgents'));
            $pdf->save(public_path('invoices/invoice' . $order->id . '.pdf'));
            return $pdf
        }*/


    //send mail to user
    public function sendmailOrder(Request $request)
    {
        $order = Order::find($request->order_id);
        dd($order);
        $urgents = Argent::where('order_id', $order->id)->get();
        $user = User::find($order->user_id);
        Mail::to($user->email)->send(new \App\Mail\InvoiceMail($order, $urgents));
        return $this->response(true, 'email send successfully');
    }



    #EndReigon[check out page]

    //cancel order
    public function ordercancelation(Request $request)
    {
        $lang = $request->header('lang');
        \Illuminate\Support\Facades\App::setLocale($lang);
        //try{
        DB::beginTransaction();
        $order = Order::find($request->order_id);
        $ordercancelation = $this->OrderRepository->ordercancelation($order, $request);

        $user = User::find($order->user_id);
        $this->sendMessage($user->country_code . $user->phone, __('auth.Your request has been canceled successfully'));

        DB::commit();
        if ($ordercancelation == false)
            return $this->response(false, 'order is prepared in laundry', null, 405);
        return $this->response(true, $lang == 'ar' ? 'تم الغاء الطلب بنجاح' : 'order canceled successfully');
        //} catch (\Exception $e) {
        DB::rollback();
        return $this->response(false, __('response.wrong'), null, 406);
        //}
    }


    public function cancelorder(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order_id = $request->order_id;
        $user = Auth::guard('user_api')->user();
        $order = Order::find($order_id);
        if ($order == null) {
            return $this->response(false, 'order not found', null, 404);
        }

        $amount = $order->price_after_discount != null ? $order->price_after_discount : $order->price_before_discount;

        $user = User::find($order->user_id);
        $this->sendMessage($user->country_code . $user->phone, app()->getLocale() == 'ar' ? 'تم الغاء الطلب رقم ' . $order->id . ' من  قبلكم' : 'You have cancelled your order no. ' . $order->id);

        if ($order->pymenttype_id == 1) {
            $wallet = user_wallet::where('user_id', $order->user_id)->first();
            $wallet->update([
                'amount' => $wallet->amount + $amount
            ]);
        }
        if ($order->pymenttype_id == 3) {
            $shaString = '';
            $arrData = array(
                'command' => 'REFUND',
                'access_code' => $this->access_code,
                'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $order->fort_id,
                'amount' => $order->price_before_discount * 100,
                'currency' => 'SAR',
                'language' => 'en',
                'fort_id' => $order->fort_id,
                'order_description' => 'order cancelation',
            );

            ksort($arrData);
            foreach ($arrData as $key => $value) {
                $shaString .= "$key=$value";
            }
            $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
            $signature = hash($this->sha_type, $shaString);

            $response = Http::post($this->url, [
                'command' => 'REFUND',
                'access_code' => $this->access_code,
                'merchant_identifier' => $this->merchant_identifier,
//                'merchant_reference' => $transactionFeedback->merchant_reference,
                'amount' => $order->price_before_discount * 100,
                'currency' => 'SAR',
                'language' => 'en',
                'fort_id' => $order->fort_id,
                "signature" => $signature,
                'order_description' => 'order cancelation',
            ]);

            $response = $response->json()['response_message'];
            if ($response == 'Success') {
                $order->orderdetailes()->delete();
                //urgents
                $urgents = Argent::where('order_id', $order->id)->get();
                foreach ($urgents as $urgent) {
                    $urgent->delete();
                }
                //deliverytype
                $order->OrderDriveryStatus()->delete();
                $order->delete();


                return $this->response(true, $lang == 'ar' ? 'تم الغاء الطلب بنجاح' : 'order canceled successfully');
            } else {
                return $this->response(false, $lang == 'ar' ? 'حدث خطأ' : 'error occurred', null, 401);
            }
        }

        /*        $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
                foreach ($Branchusers as $Branchuser) {
                    if ($Branchuser->lang == 'ar') {
                        $title = 'الغاء طلب';
                        $body = 'تم الغاء الطلب رقم ' . $order->id . ' من  قبلكم';
                    }
                    if ($Branchuser->lang == 'en') {
                        $title = 'order cancellation';
                        $body = 'order has been canceled number ' . $order->id . ' by user';
                    }
                    $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 1);
                }
                $titles = ['ar' => 'الغاء طلب', 'en' => 'order cancellation'];
                $bodys = ['ar' => 'تم الغاء الطلب رقم ' . $order->id . ' من قبلكم ', 'en' => 'order has been canceled number ' . $order->id . ' by user'];
                $this->NotificationRepository->createnotification('branch', $order->branch_id, $titles, $bodys);*/
        // end send notification to branch


        $order->orderdetailes()->delete();
        //urgents
        $urgents = Argent::where('order_id', $order->id)->get();
        foreach ($urgents as $urgent) {
            $urgent->delete();
        }
        //deliverytype
        $order->OrderDriveryStatus()->delete();
        $order->delete();

        return $this->response(true, $lang == 'ar' ? 'تم الغاء الطلب بنجاح' : 'order canceled successfully');
    }
    #EndReigon[this is show order cycle]

    #Reigon[edite order cycle]
    public function edit(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order = Order::find($request->order_id);
        $order_details = $order->orderdetailes;
        $branch_id = $order->branch_id;
        $lang = $request->header('lang');
        $data['order'] = $this->branchservices($branch_id, $order->id, $lang, $order_details);


        /*     $services = Service::whereHas('branchservice', function ($q) use ($branch_id) {
                 $q->where('branch_id', $branch_id);
             })->with('servicetranslations')->select('servicetranslations.name', 'services.id')
                 ->join('brnachservices', 'brnachservices.service_id', '=', 'services.id')
                 ->join('servicetranslations', 'servicetranslations.service_id', '=', 'services.id')
                 ->where('servicetranslations.locale', $lang)
                 ->where('brnachservices.status', 'on')
                 ->get();

             dd($services);*/

        $data['argent'] = branch::select('argent')->where('id', $branch_id)->first();

        return $this->response(true, 'get order success', $data);
    }
    //start get edit orde response

    // get services and categories
    public function branchservices($branch_id, $order_id, $lang, $order_details)
    {
//        $services = $this->OrderRepository->laundryservices($branch_id, $lang);

        /*$services = DB::table('services')
            ->select('servicetranslations.name', 'services.id')
            ->join('brnachservices', 'brnachservices.service_id', '=', 'services.id')
            ->join('servicetranslations', 'servicetranslations.service_id', 'services.id')
            ->where('brnachservices.branch_id', $branch_id)
            ->where('servicetranslations.locale', $lang)
            ->where('brnachservices.status', 'on')
            ->get();*/

        //محتاج اعمل انه يجيب الخدمات اللي موجوده في الطلب

        /*    $services = Service::select('servicetranslations.name', 'services.id')
                ->join('brnachservices', 'brnachservices.service_id', '=', 'services.id')
                ->join('servicetranslations', 'servicetranslations.service_id', '=', 'services.id')
                ->where('brnachservices.branch_id', $branch_id)
                ->where('servicetranslations.locale', $lang)
                ->where('brnachservices.status', 'on')
                ->get();*/

        $services = Service::whereHas('branchservice', function ($q) use ($branch_id) {
            $q->where('branch_id', $branch_id);
        })->with('servicetranslations')->select('servicetranslations.name', 'services.id')
            ->join('brnachservices', 'brnachservices.service_id', '=', 'services.id')
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'services.id')
            ->where('servicetranslations.locale', $lang)
            ->where('brnachservices.status', 'on')
            ->get();


        $services_id = [];
        foreach ($services as $key => $value) {
            $services_id[$key] = $value->id;
        }
        $services = Service::whereIn('id', $services_id)->with('categories')->get();

        foreach ($services as $service) {
            foreach ($service->categories as $category) {
                $category->branchitems = $this->categoryitems($order_id, $category->id, $service->id, $branch_id, $lang);
            }
        }


        return $services;
    }

    // get items inside category
    public function categoryitems($order_id, $category_id, $service_id, $branch_id, $lang)
    {

        $order_detailes = orderdetailes::select('branchitem_id', 'service_id', 'additionalservice_id', 'price', 'quantity')
            ->where(['order_id' => $order_id, 'service_id' => $service_id, 'additionalservice_id' => null])->get();

        $branchitems = Branchitem::whereHas('branchitemprice', function ($q) use ($service_id, $branch_id, $category_id) {
            $q->where('service_id', $service_id)->where('branch_id', $branch_id)->where('category_id', $category_id);
        })->with(['branchitemprice' => function ($q) use ($service_id, $branch_id, $category_id) {
            $q->where('service_id', $service_id)->where('branch_id', $branch_id)->where('category_id', $category_id)->get();
        }])->get();
        $detailes = ['price' => 0, 'quantity' => 0];
        foreach ($branchitems as $branchitem) {
            $branchitem->detailes = $detailes;
            foreach ($order_detailes as $detail) {
                if ($detail->branchitem_id == $branchitem->id) {
                    unset($detail->{'branchitem_id'});
                    unset($detail->{'service_id'});
                    unset($detail->{'additionalservice_id'});
                    $branchitem->detailes = $detail;
                }
            }
            unset($branchitem->{'created_at'});
            unset($branchitem->{'updated_at'});
            unset($branchitem->branchitemprice[0]->{'created_at'});
            unset($branchitem->branchitemprice[0]->{'updated_at'});
            //argent


            $branchitem->additionalservice = $this->getitemdetailes($lang, $branch_id, $branchitem->id, $order_id, $service_id);

            $argent = $this->argents($branchitem->id, $service_id, $order_id);
            if ($argent == null) {
                $detailes = ['price' => 0, 'quantity' => 0];
                $branchitem->argent = $detailes;
            } else {
                $branchitem->argent = $argent;
            }

        }
        return $branchitems;
    }

    public function argents($branchitem_id, $service_id, $order_id)
    {
        $argent = Argent::select('price', 'quantity')->where('branchitem_id', $branchitem_id)
            ->where('service_id', $service_id)->where('order_id', $order_id)->first();
        return $argent;
    }

    // in this function but additional service inside item
    public function getitemdetailes($lang, $branch_id, $branchitem_id, $order_id, $service_id)
    {

        // return additional service of item
        $itemdetailes = $this->OrderRepository->itemdetailes($branchitem_id, $lang);


        $order_detailes = orderdetailes::select('branchitem_id', 'service_id', 'additionalservice_id', 'price', 'quantity')
            ->where(['order_id' => $order_id, 'branchitem_id' => $branchitem_id])
            ->where('additionalservice_id', '!=', null)
            ->where('service_id', $service_id)
            ->get();
        $detaile = ['price' => 0, 'quantity' => 0];
        foreach ($itemdetailes as $item) {
            $item->detailes = $detaile;
            foreach ($order_detailes as $detailes) {
                if ($detailes->additionalservice_id == $item->id) {
                    unset($detailes->{'branchitem_id'});
                    unset($detailes->{'service_id'});
                    unset($detailes->{'additionalservice_id'});
                    $item->detailes = $detailes;
                }
            }
        }
        return $itemdetailes;

    }

    //end get edit orde response

    public function update(Request $request)
    {
        $price_before_discount = 0;

        DB::beginTransaction();
        $order = OrderEditLogs::create(['order_id' => $request->order_id,]);

        //delete old order detailes
        $old_order_detailes = OrderEditDetailes::where('order_id', $request->order_id)->get();
        foreach ($old_order_detailes as $old_order_detaile) {
            $old_order_detaile->delete();
        }

        //delete old argent
        $old_argents = ArgentEditLogs::where('order_id', $request->order_id)->get();
        foreach ($old_argents as $old_argent) {
            $old_argent->delete();
        }


        if (count($request->serviceprices) > 0) {
            foreach ($request->serviceprices as $serviceprice) {
                $price = $serviceprice['quantity'] * $serviceprice['price'];
                OrderEditDetailes::create([
                    'order_id' => $request->order_id,
                    'order_edit_id' => $order->id,
                    'branchitem_id' => $serviceprice['branchitem_id'],
                    'price' => $price,
                    'service_id' => $serviceprice['service_id'],
                    'quantity' => $serviceprice['quantity'],
                    'note' => $serviceprice['note']
                ]);
                $price_before_discount += $price;
                // this condation to add agent
                if ($serviceprice['argent'] != 0) {
                    $argentprice = Branchitem::select('argentprice')->where('id', $serviceprice['branchitem_id'])->first();
                    $argentprice = $serviceprice['argent'] * $argentprice->argentprice;
                    ArgentEditLogs::create([
                        'order_id' => $request->order_id,
                        'order_edit_id' => $order->id,
                        'price' => $argentprice,
                        'quantity' => $serviceprice['argent'],
                        'service_id' => $serviceprice['service_id'],
                        'branchitem_id' => $serviceprice['branchitem_id'],
                    ]);

                    $price_before_discount += $argentprice;
                }
            }
        }
        if (count($request->additionalservices) > 0) {
            foreach ($request->additionalservices as $additionalservice) {
                $price = $additionalservice['quantity'] * $additionalservice['price'];
                OrderEditDetailes::create([
                    'order_id' => $request->order_id,
                    'order_edit_id' => $order->id,
                    'branchitem_id' => $additionalservice['branchitem_id'],
                    'price' => $price,
                    'service_id' => $additionalservice['service_id'],
                    'quantity' => $additionalservice['quantity'],
                    'additionalservice_id' => $additionalservice['additionalservice_id'],
                ]);
                $price_before_discount += $price;
            }
        }

        $this->OrderRepository->addordertaxes($order, $request->branch_id, $price_before_discount);

        $discount = $this->DiscountRepository->orderdiscount($request->order_id);


        try {
            $user = User::find($order->user_id);
            $this->sendMessage($user->country_code . $user->phone, __('auth.Your request has been modified successfully'));
        } catch (\Exception $e) {

        }


        if ($discount != false) {
            $orderDiscount = OrderDiscount::where('order_id', $request->order_id)->first();
            $this->DiscountRepository->order_edit_discount($order, $orderDiscount->discount_id);
            //  return
        }

        DB::commit();

        return $this->response(true, 'you submit order success');
    }


    //  before check out order
    public function edit_order_summary(Request $request)
    {


        $order_id = $request->order_id;
        $order = OrderEditLogs::where('order_id', $order_id)->orderBy('id', 'desc')->first();
        $oldorder = order::find($order_id);
        $oldorderDetailes = OrderDetailes::where('order_id', $order_id)->sum('price');

        if ($order == null)
            return $this->response(false, 'this order not make edit', null, 422);


        $lang = $request->header('lang');
        $order_id = $request->order_id;
        App::setLocale($lang);
        $deliverytype = delivery_type::select('id')->get()->makehidden('translations');


        $orderdetailes = DB::table('order_edit_detailes')->where('order_edit_detailes.order_id', $order_id)
            ->join('servicetranslations', 'servicetranslations.service_id', '=', 'order_edit_detailes.service_id')
            ->selectRaw('sum(price) as total')
            ->selectRaw('sum(quantity) as quantity')
            ->selectRaw('servicetranslations.name')
            ->where('order_edit_detailes.order_id', $order_id)
            ->where('servicetranslations.locale', $lang)
            ->where('order_edit_detailes.additionalservice_id', '=', null)
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();


        $argentprice = ArgentEditLogs::where('order_id', $order_id)->sum('price');


        $additionalservice = DB::table('order_edit_detailes')->where('order_edit_detailes.order_id', $order_id)
            ->join('additionalservicetranslations', 'additionalservicetranslations.additionalservice_id', '=', 'order_edit_detailes.additionalservice_id')
            ->where('additionalservicetranslations.locale', $lang)
            ->selectRaw('additionalservicetranslations.name')
            ->selectRaw('sum(price) as price')
            ->selectRaw('quantity')
            ->where('order_edit_detailes.additionalservice_id', '!=', null)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_edit_detailes.quantity')
            ->get();
        $totaladditionalprice = 0;
        foreach ($additionalservice as $addtional) {
            $totaladditionalprice += $addtional->price;
        }

        // old payment
        $paymentmethods = [];
        switch ($oldorder->pymenttype_id) {
            case 1:
                $payment = 'wallet';

                break;
            case 2:
                $payment = 'cash';
                break;
            default:
                $payment = 'visa';


        }
        $paymentmethods = $this->BranchRepository->get_paymet_methods($oldorder->branch_id, $lang);


        // difference price
        if ($order->price_after_discount == null) {
            $difference_price = $order->price_before_discount - $oldorder->price_before_discount;
        } else {
            $difference_price = $order->price_after_discount - $oldorder->price_after_discount;
        }


        $totalPrice = 0;

        if ($order->price_after_discount == null) {
            $totalPrice =
                $orderdetailes->sum('total') +
                $argentprice +
                $additionalservice->sum('price') +
                $oldorder->delivery_fees +
//                $oldorder->taxes+
                $order->taxes;
        } else {
            $totalPrice =
                $oldorder->delivery_fees
//                + $oldorder->taxes
                + $order->taxes
                + $orderdetailes->sum('total')
                + $argentprice
                + $additionalservice->sum('price')
                + $difference_price;
        }


        $data['status'] = true;
        $data['message'] = "return order info succeffuly";
        $data['data']['deliverytype'] = $deliverytype;
        $data['data']['paymentmethods'] = $paymentmethods;
        $data['data']['orderdetailes'] = $orderdetailes;
        $data['data']['argentprice'] = $argentprice;
        $data['data']['delivery_fees'] = $oldorder->delivery_fees;
        $data['data']['additionalservices'] = $additionalservice;
        $data['data']['totaladditionalprice'] = $totaladditionalprice;
        $data['data']['price_after_discount'] = $order->price_after_discount != null ? $order->price_after_discount + $oldorder->delivery_fees : $order->price_after_discount;
        $data['data']['price_before_discount'] = $order->price_before_discount + $oldorder->delivery_fees;
//        $data['data']['price_before_discount'] = $orderdetailes->sum('price') + $argentprice + $additionalservice->sum('price') + $order->delivery_fees + $order->taxes + $difference_price;

        $data['data']['old_price_after_discount'] = $oldorder->price_after_discount;
        $data['data']['old_price_before_discount'] = $oldorder->price_before_discount;

        $data['data']['taxes'] = $order->taxes /* + $oldorder->taxes*/
        ;

        $data['data']['payment'] = $payment;


//        $data['data']['difference_price'] =  $totalPrice  -  $oldorder->price_before_discount -  $oldorder->price_after_discount  ;
        $data['data']['difference_price'] = $difference_price + $oldorder->delivery_fees;
        $oldorder->price_edit = $difference_price + $oldorder->delivery_fees;
        $oldorder->price_before_edit = $oldorder->price_before_discount + $oldorder->delivery_fees;
        $oldorder->price_after_edit = $order->price_after_discount != null ? $order->price_after_discount + $oldorder->delivery_fees : $order->price_after_discount;
        $oldorder->save();


        $data['data']['paymentmethods'] = $paymentmethods;

        return $data;

    }

    public function editcheckout(Request $request)
    {

        DB::beginTransaction();


        if ($request->pymenttype_id == 1) {
            if ($request->merchant_reference != null) {
                $transactionfeedback = TransactionFeedback::where('merchant_reference', $request->merchant_reference)->first();
                if ($transactionfeedback == null)
                    return $this->response(false, 'payment transaction error');
            }
        }

        $user_id = Auth::guard('user_api')->user()->id;
        $order_id = $request->order_id;
        $pymenttype_id = 2;
        $order_delivery_status = true;

        // old order
        $oldorder = order::where('id', $order_id)->first();
        $old_orderdetailes = orderdetailes::where('order_id', $order_id)->get();
        $oldargents = Argent::where('order_id', $order_id)->get();


        // new order
        $order = OrderEditLogs::where('order_id', $order_id)->orderBy('id', 'desc')->first();
        $orderdetailes = OrderEditDetailes::where('order_id', $order_id)->get();
        $argents = ArgentEditLogs::where('order_id', $order_id)->get();


        // difference price
        if ($order->price_after_discount == null) {
            $difference_price = $order->price_before_discount;
        } else {
            $difference_price = $order->price_after_discount;
        }

        if ($oldorder->price_after_discount == null) {
            $old_difference_price = $order->price_before_discount;
        } else {
            $old_difference_price = $order->price_after_discount;
        }

        if ($oldorder->pymenttype_id == 1) {

            // if new order update less old order
            if ($oldorder->price_edit < 0) {
                $this->WalletRepository->increeseuserwallet($user_id, abs($oldorder->price_edit));
                //          $this->OrderRepository->transformmonytopoints(abs($difference_price));
            }

            if ($oldorder->price_edit > 0) {
                $walletamount = $this->WalletRepository->getpallence($user_id);
                if ($walletamount < $oldorder->price_edit) {
                    return $this->response(false, 'you have not enough money in wallet');
                }
                $this->WalletRepository->decreaseuserwallet($oldorder->price_edit, $user_id);
//            $this->OrderRepository->transformmonytopoints($difference_price);
            }

        }

        // update new order detailes
        foreach ($old_orderdetailes as $old_detail) {
            $old_detail->delete();
        }
        foreach ($orderdetailes as $detaile) {
            orderdetailes::create([
                'order_id' => $detaile->order_id,
                'branchitem_id' => $detaile->branchitem_id,
                'service_id' => $detaile->service_id,
                'price' => $detaile->price,
                'additionalservice_id' => $detaile->additionalservice_id,
                'quantity' => $detaile->quantity
            ]);
        }

        // update argent
        foreach ($oldargents as $argent) {
            $argent->delete();
        }

        foreach ($argents as $argent) {
            Argent::create([
                'order_id' => $argent->order_id,
                'price' => $argent->price,
                'quantity' => $argent->quantity,
                'service_id' => $argent->service_id,
                'branchitem_id' => $argent->branchitem_id,
            ]);
        }

        if ($difference_price < 0)
            $cal = "-";
        else
            $cal = "+";


        // update order
        $oldorder->price_after_discount = $order->price_after_discount != null ? $difference_price + $oldorder->delivery_fees : $order->price_after_discount;
        $oldorder->price_before_discount = $order->price_before_discount + $oldorder->delivery_fees;
        $oldorder->taxes = $order->taxes;
        if ($order->price_after_discount == null) {
            $oldorder->discounttype = null;
            $oldorder->save();
        }

        if ($order == null) {
            $data['status'] = false;
            $data['message'] = 'order not found';
            return response()->json($data, 405);
        } else {
            // if order found
            /*  if ($request->pymenttype_id == 1) {

                  $price = $this->OrderRepository->orderprice($oldorder);
                  $walletamount = $this->WalletRepository->getpallence($user_id);
                  if ($walletamount < $order->$price) {
                      if ($lang == 'ar') {
                          return $this->response(false, 'لا يوجد لديك رصيد كافي في المحفظة');
                      } else {
                          return $this->response(false, 'you have not enough money in wallet');
                      }
                  }
                  $this->WalletRepository->decreaseuserwallet($price, $user_id);
              }*/
            //end wallet
            $fort_id = null;
            if ($request->fort_id != null) {
                $fort_id = $request->fort_id;
            }
            $oldorder->update([
                'day' => $request->day,
                'from' => date("H:i:s", strtotime($request->from)),
                'to' => date("H:i:s", strtotime($request->to)),
                'fort_id' => $fort_id
            ]);
            // charge point
            $branch = Branch::find($oldorder->branch_id);
            if ($oldorder->confirmation == 'accepted') {
                $trans = Transaction::where('order_id', $oldorder->id)->first();
                if ($trans) {
                    $branch->pending_balance = $branch->pending_balance - $trans->amount;
                    $branch->save();
                    $trans->delete();
                }
                if ($oldorder->pymenttype_id == 1 || $oldorder->pymenttype_id == 3) {
                    $price = $this->getprice($oldorder);
                    $this->TransactionRepository->createtransaction($oldorder, $price);
                }
            }


            // start send notification to branch
            $Branchusers = Branchuser::where('branch_id', $oldorder->branch_id)->get();
            $notf_s = DB::table('branchnotifytype')->where('user_branch_id', $Branchusers->first()->id)
                ->where('notificationtype_id', '1')
                ->where('status', '1')->first();

            if ($notf_s) {
                $user = Auth::guard('user_api')->user();
                try {
                    foreach ($Branchusers as $Branchuser) {
                        if ($Branchuser->lang == 'en') {
                            $title = 'edit order';
                            $body = 'user edit order' . $user->name;
                        }
                        if ($Branchuser->lang == 'ar') {
                            $title = 'تعديل الطلب ';
                            $body = $user->name . ' لقد تم تعديل طلب العميل ';
                        }

                        $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 1);
                    }
                } catch (\Exception $e) {

                }
                $titles = ['ar' => 'طلب جديد', 'en' => 'New Order'];
                $bodys = ['ar' => 'لقد استلمت طلب جديد من العميل ' . $user->name . 'رقم الطلب ' . $oldorder->id, 'en' => 'you have new order from user ' . $user->name . 'order number ' . $oldorder->id];
                $this->NotificationRepository->createnotification('branch', $oldorder->branch_id, $titles, $bodys);
                // end send notification to branch
            }
            DB::commit();
            $data['status'] = true;
            $data['message'] = 'order updated  succefully';
            return response()->json($data);
        }

    }

    public function getprice($order)
    {
        // check if order use discount if discount created by admin the branch balance don't effected
        if ($order->discounttype == null) {
            $price = $order->price_before_discount;
        }
        if ($order->discounttype == 'Admin') {
            $price = $order->price_before_discount;
        }
        if ($order->discounttype == 'branch') {
            $price = $order->price_after_discount;
        }
        return $price;
    }

    public function canceleditorder(Request $request)
    {

        DB::beginTransaction();
        $order_id = $request->order_id;

        $order = OrderEditLogs::where('order_id', $order_id)->first();
        $order->delete();

        DB::commit();

        return $this->response(true, 'cancel edit order successfuly');


    }

    #EndReigon[edit order cycle]

    #Reigon[this is recive order cycle]
    // this request for navigation
    public function orderdeliverystatus(Request $request)
    {
        $order_id = $request->order_id;
        $lang = $request->header('lang');
        $orderdeliveryinfo = $this->OrderRepository->orderdeliverystatus($order_id, $lang);
        if ($orderdeliveryinfo == false)
            return $this->response(true, 'true', null, 200);
        //$orderdeliveryinfo=Arr::except($orderdeliveryinfo,['confirmation','delivery_status']);
        $data['order_delivery_status'] = $orderdeliveryinfo;
        return $this->response(true, 'get order delivey status successfuly', $orderdeliveryinfo);
    }

    //(android mobile use this request)
    public function reciveorderinfo(Request $request)
    {
        $code = null;
        if (isset($request->code) != null)
            $code = $request->code;
        $order_id = $request->order_id;
        $lang = $request->header('lang');
        $order = $this->OrderRepository->orderinfo($order_id, $lang, $code);

        // display qr code buttom in user mobile application (android mobile use this request)
        $order['data']['order']->view_qrcode = false;
        // driver recive order from customer delivery_type (by delivery)
        if ($order['data']['order']->delivery_type_id == 1) {
            if ($order['data']['order']->order_status == 'pick_up_home' && $order['data']['order']->start_tracking == true)
                $order['data']['order']->view_qrcode = true;
        }
        // laundry recive order from customer delivery_type (self delivery)
        if ($order['data']['order']->delivery_type_id == 2) {
            if ($order['data']['order']->order_status == 'drop_of_laundry')
                $order['data']['order']->view_qrcode = true;
        }
        // laundry recive order from customer delivery_type (home drop of)
        if ($order['data']['order']->delivery_type_id == 3) {
            if ($order['data']['order']->order_status == 'pick_up_laundry' && $order['data']['order']->progress == 'no_progress')
                $order['data']['order']->view_qrcode = true;
        }
        // laundry recive order from customer delivery_type (self drop of)
        if ($order['data']['order']->delivery_type_id == 4) {
            if ($order['data']['order']->order_status == 'pick_up_home' && $order['data']['order']->start_tracking == true)
                $order['data']['order']->view_qrcode = true;
        }

        return $order;
    }

    // we use this function when user give order to driver
    public function getcode(Request $request)
    {
        $order_id = $request->order_id;
        $status = 'pick_up_home';
        $order = order::find($order_id);
        if ($order->delivery_type_id == 3) {
            $status = 'pick_up_laundry';
        } elseif ($order->delivery_type_id == 2) {
            $status = 'drop_of_laundry';
        }

        $code = $this->OrderRepository->getcode($status, $order_id);
        if ($code == false)
            return $this->response(false, 'you have no accsess for this order');
        $data['order_code'] = $code;
        return $this->response(true, 'get order code succsessfuly', $data);
    }

    // we use this function when user recive order from driver
    public function reciveorder(Request $request)
    {

        $lang = $request->header('lang');
        App::setLocale($lang);
        $order_id = $request->order_id;
        $confirm_type = $request->confirm_type;
        //start transaction
        //  try {
        DB::beginTransaction();

        $orderstatus = OrderDriveryStatus::where('order_id', $order_id)->latest('id')->first();
        $order = order::find($order_id);
        if ($order == null) {
            return $this->response(false, 'order not found', null, 404);
        }


        // if order delivery type home drop of laundry recive order from customer not driver
        if ($order->delivery_type_id == 3 && $orderstatus->order_status == 'pick_up_laundry')
            $data = $this->OrderRepository->reciveorder_from_customer($order);
        // here customer recive order from laundry after finish when delivery type self delivery
        if ($order->delivery_type_id == 4 && $orderstatus->order_status == 'pick_up_laundry' || $order->delivery_type_id == 2 && $orderstatus->order_status == 'pick_up_laundry')
            $data = $this->OrderRepository->reciveorder_from_laundry($order, $orderstatus);
        $driver_id = $order->driver_id;

        if ($confirm_type == 'drop_of_laundry') {
            $order_delivery_status = true;
            // dd($order);
            if ($orderstatus->order_status == 'drop_of_laundry') {
                if ($order->delivery_type_id == 4 || $order->delivery_type_id == 2)
                    $order_delivery_status = false;

                $orderstatus->update([
                    'confirmation' => true
                ]);
                OrderDriveryStatus::create([
                    'order_id' => $order_id,
                    'driver_id' => null,
                    'order_status' => 'pick_up_laundry',
                    'code' => rand(1000, 9999)
                ]);
                $driver_id = $order->driver_id;


                $order->update([
                    'driver_id' => null,
                    'delivery_status' => 'no_progress',
                    'progress' => 'inprogress',
                    'order_delivery_status' => $order_delivery_status,
                    'start_tracking' => false
                ]);
                // send notification to driver to remove qr code page


                try {

                    $driver = Driver::find($driver_id);
                    $drivernotifytype = drivernotifytype::where('driver_id', $driver_id)->where('notificationtype_id', 1)->where('status', 1)->first();

                    if ($drivernotifytype) {
                        $this->NotificationRepository->sendnotification('driver', $driver_id, $lang == 'ar' ? 'تم تسليم الطلب' : 'order is received', $lang == 'ar' ? 'تم تسليم الطلب الي العميل ' . $order->user->name . 'رقم الطلب ' . $order->id : 'order is received to user ' . $order->user->name . 'order number ' . $order->id, 3, 5, ['order_id' => $order_id]);

                    }
                } catch (\Exception $e) {

                }

            }
            // the customer make order not laundry
            if ($order->user_id != null) {
                $user = User::find($order->user_id);
                // in case driver deliver order to laundry
                if ($order->delivery_type_id != 2) {
                    $title = 'تسليم الطلب ';
                    $body = 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username;
                    if ($user->lang == 'en') {
                        $title = 'order is received ';
                        $body = 'your order ' . '(' . $order->id . ')' . ' is received to laundry ' . $order->branch->username;
                    }
                    $country_code = $user->country_code;
                    $phone = $user->phone;
                    $phonen = $country_code . $phone;

//                    $this->sendSms($phonen, 'تم تسليم الطلب الي المغسله');
                    /*  \App\Jobs\SendSmsJob::dispatch($phonen, 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username);*/


                    $titles = ['ar' => 'تسليم جديد', 'en' => 'order is received'];
                    $bodys = ['ar' => 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username, 'en' => 'your order ' . '(' . $order->id . ')' . ' is received to laundry ' . $order->branch->username];
                    $driver_name = Driver::select('name')->find($order->driver_id);
                    try {
                        $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '3')->where('status', '1')->first();
                        if ($usernotifytype) {
                            $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 3, ['order_id' => $order_id]);
                        }
                    } catch (\Exception $e) {

                    }
                    $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
                } // here customer create order self delivery and will deliver his order to laundry
                elseif ($order->delivery_type_id == 2) {
                    $title = $lang == 'ar' ? 'تم تسليم الطلب' : 'order is received';
                    $body = $lang == 'ar' ? 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username : 'your order ' . '(' . $order->id . ')' . ' is received to laundry ' . $order->branch->username;
                    try {
                        $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '3')->where('status', '1')->first();
                        if ($usernotifytype) {
                            $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 5, ['order_id' => $order_id]);
                        }
                    } catch (\Exception $e) {

                    }
                }
            }

            $data['status'] = true;
            $data['message'] = 'laundry recived order successfully';
        } elseif ($confirm_type == 'drop_of_home') {


            $price = $this->OrderRepository->getprice($order);
            // this table for branch
            if ($order->driver_id != null) {
                if ($order->pymenttype_id == 1 || $order->pymenttype_id == 3) {

                    $this->TransactionRepository->activetransaction($order);
                }
            }


            if ($order->pymenttype_id == 2) {

                // خصم  نسبه  التطبيق  من  المغسله
                $branch = Branch::find($order->branch_id);
                $app_percentage = ApplicationRates::first();

                $branch->update([
                    'balance' => $branch->balance - ($price * ($app_percentage->rate / 100)),
                ]);

                $fort_id = null;
                if ($order->fort_id != null) ;
                $fort_id = $order->fort_id;

                $Transaction = Transaction::create([
                    // هنا يتم حساب نسبه التطبيق
                    'amount' => ($price * ($app_percentage->rate / 100)),
                    'order_id' => $order->id,
                    'branch_id' => $order->branch_id,

                    // هنا  بيتم عرض الرصيد اللي المفوض هيبقي معاه بعد ميخلص الطلب كامل
                    'branch_balance' => $branch->balance,
                    'system_balance' => ($price * ($app_percentage->rate / 100)),
                    'application_percentage' => ($price * ($app_percentage->rate / 100)),

                    'pending' => 0,
                    'type' => 'cache',
                    'transaction' => 'externance',
                    'transaction_number' => $fort_id,
                    'transaction_type' => 'order',
                    'payment_type' => null
                ]);
            }


            // here driver give customer his order
            if ($orderstatus->order_status == 'drop_of_home') {
                // validation to make sure this request use only one time
                $this->OrderRepository->checkreciveorder($orderstatus);
                $orderstatus->update([
                    'confirmation' => true
                ]);
                $order = order::find($order_id);
                $order->update([
                    'progress' => 'completed',
                    'delivery_status' => 'completed',
                    'drop_date' => now()->format('Y-m-d H:i:s'),
                    'order_time' => round((strtotime(Carbon::now()) - strtotime($order->created_at)) / 3600)
                ]);


            }
            // send notification to driver to remove qr code from page of driver application and go to home
            try {
                $driver = Driver::find($order->driver_id);
                $drivernotifytype = drivernotifytype::where('driver_id', $order->driver_id)->where('notificationtype_id', 3)->where('status', 1)->first();

                if ($drivernotifytype) {
                    $this->NotificationRepository->sendnotification('driver', $order->driver_id, $lang == 'ar' ? 'تم تسليم الطلب' : 'order is received', $lang == 'ar' ? 'تم تسليم الطلب الي العميل ' . $order->user->name . 'رقم الطلب ' . $order->id : 'order is received to user ' . $order->user->name . 'order number ' . $order->id, 3, 5, ['order_id' => $order_id]);
                }
                $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
                $notf_s = DB::table('branchnotifytype')->where('user_branch_id', $Branchusers->first()->id)
                    ->where('notificationtype_id', '3')
                    ->where('status', '1')->first();
                if ($notf_s) {
                    $this->NotificationRepository->sendnotification('branch', $order->branch_id, $lang == 'ar' ? 'تم تسليم الطلب' : 'order is received', $lang == 'ar' ? 'تم تسليم الطلب الي العميل ' . $order->user->name . 'رقم الطلب ' . $order->id : 'order is received to user ' . $order->user->name . 'order number ' . $order->id, 3, 5, ['order_id' => $order_id]);
                }
            } catch (\Exception $e) {

            }
            $data['status'] = true;
            $data['message'] = $lang == 'ar' ? 'تم تسليم الطلب بنجاح' : 'user received order successfully';
            $data['data']['driver_id'] = $order->driver_id;
        }
        DB::commit();
        return $data;
        // } catch (\Exception $e) {
        //     DB::rollback();
        // }
    }

    public function activeorder(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $user_id = Auth::guard('user_api')->user()->id;
        $user_lang = Auth::guard('user_api')->user()->lang;
        $orders = DB::table('orders')
            ->select('laundries.name', 'orders.id as order_id', 'orders.progress', 'orders.created_at as time', 'orders.checked')
            ->selectRaw('sum(order_detailes.quantity) as quantity')
            ->join('branchs', 'orders.branch_id', '=', 'branchs.id')
            ->join('laundries', 'branchs.laundry_id', '=', 'laundries.id')
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->groupBy('orders.id')->groupBy('order_detailes.order_id')->groupBy('orders.checked')
            ->groupBy('laundries.name')->groupBy('orders.progress')
            ->groupBy('orders.created_at')
            ->where('orders.user_id', $user_id)
            ->where('orders.checked', true)
            ->where('orders.progress', '!=', 'completed')
            ->where('confirmation', '!=', 'refused')
            ->orderBy('orders.id', 'desc')
            ->get();
        if ($orders->isEmpty()) {
            $data['order'] = [];
            return $this->response(true, 'no avilable order yet', $data);
        }
        foreach ($orders as $order) {
            $diffHours = round((strtotime(Carbon::now()) - strtotime($order->time)) / 3600);
            $order->time = $diffHours;
            if ($order->progress == 'no_progress') {
                $order->progress = __('progress.Order placed');
            } else {
                $order->progress = __('progress.' . $order->progress);
            }

        }
        $data['orders'] = $orders;

//        $data['progressTra'] = __('progress.' . $orders->progress);

        return $this->response(true, 'get active order successfully', $data);
    }

    public function previousorder(Request $request)
    {
        $user_id = Auth::guard('user_api')->user()->id;
        $orders = DB::table('orders')
            ->select('laundries.name', 'orders.id as order_id', 'orders.progress', 'orders.created_at', 'orders.updated_at', 'orders.rate', 'orders.delivery_type_id')
            ->selectRaw('sum(order_detailes.quantity) as quantity')
            ->join('branchs', 'orders.branch_id', '=', 'branchs.id')
            ->join('laundries', 'branchs.laundry_id', '=', 'laundries.id')
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->groupBy('orders.id')->groupBy('order_detailes.order_id')->groupBy('orders.rate')
            ->groupBy('laundries.name')->groupBy('orders.progress')
            ->groupBy('orders.created_at')
            ->groupBy('orders.updated_at')
            ->orderby('orders.created_at', 'desc')
            ->where('orders.user_id', $user_id)
            ->where('progress', 'completed')->paginate(10);
        foreach ($orders as $order) {
            $order->time = Carbon::parse($order->updated_at)->format('h:i A');
            $order->created_at = date('Y-m-d', strtotime($order->created_at));
        }
        return $orders;
        return $this->response(true, 'get previous order successfully', $orders);
    }

    public function rateorder(Request $request)
    {
        try {
            DB::beginTransaction();
            $order_id = $request->order_id;
            $user_id = Auth::guard('user_api')->user()->id;
            $order = order::find($order_id);
            $driver = Driver::find($order->driver_id);
            $order_review = Orderreview::where(['user_id' => $user_id, 'order_id' => $order_id])->first();
            if ($order->rate == 1)
                return $this->response(true, 'you already rate this order', null, 422);
            if ($order->user_id != $user_id)
                return $this->response(false, 'you have not access for this order', null, 401);
            if ($order == null)
                return $this->response(false, 'this order not found', null, 401);
            if ($order->progress == 'inprogress' || $order->progress == null)
                return $this->response(false, 'this order not finished yet', null, 401);
            if ($order_review != null)
                return $this->response(false, 'you make review for this order', null, 401);
            $rate = (int)$request->rate;
            $delivery_rate = (int)$request->delivery_rate;
            #Reigon[make rate for this order only]
            $order->update([
                'rate' => 1
            ]);
            Orderreview::create([
                'rate' => $rate,
                'user_id' => $user_id,
                'order_id' => $order_id,
                'desc' => $request->review,
                'branch_id' => $order->branch_id,
            ]);
            deliveryrate::create([
                'rate' => $delivery_rate,
                'driver_id' => $order->driver_id,
                'user_id' => $user_id,
                'branch_id' => $order->branch_id,
                'order_id' => $order_id,
            ]);
            #ENDReigon[make rate for this order only]
            #Reigon[make total rate for branch]
            $Branchrate = Branchrate::where('branch_id', $order->branch_id)->first();
            if ($Branchrate == null) {
                $Branchrate = Branchrate::create([
                    'rate' => $rate,
                    'branch_id' => $order->branch_id,
                    'order_count' => 1,
                    'total_point' => $rate,
                ]);
            } else {
                $rate = ($rate + $Branchrate->total_point) / ($Branchrate->order_count + 1);
                $Branchrate->update([
                    'branch_id' => $order->branch_id,
                    'rate' => $rate,
                    'order_count' => $Branchrate->order_count + 1,
                    'total_point' => $Branchrate->total_point + (int)$request->rate,
                ]);
            }
            $branch = branch::find($order->branch_id);
            $branch->update([
                'rate' => $Branchrate->rate
            ]);
            #endReigon[make total rate for branch]
            #Reigon[make total rate for driver]
            $driverrate = driverrate::where(['branch_id' => $order->branch_id, 'driver_id' => $order->driver_id])->first();
            if ($driverrate == null) {
                $driverrate = driverrate::create([
                    'rate' => $delivery_rate,
                    'branch_id' => $order->branch_id,
                    'driver_id' => $order->driver_id,
                    'order_count' => 1,
                    'total_point' => $delivery_rate,
                ]);
            } else {
                $rate = ($delivery_rate + $driverrate->total_point) / ($driverrate->order_count + 1);
                $driverrate->update([
                    'branch_id' => $order->branch_id,
                    'rate' => $rate,
                    'order_count' => $driverrate->order_count + 1,
                    'total_point' => $driverrate->total_point + (int)$request->delivery_rate,
                ]);


                /*   if ($rate != null) {
                       $driver->rate = $rate;
                       $driver->save();
                   }*/

                /* $driver->rate = $rate;
                 $driver->save();*/
            }
            if ($driverrate->order_count > 25 && $driverrate->order_count >= 3) {
                $driver = Driver::find($order->driver_id);
                $driver->update(['classification', 'super']);
            }
            if ($driverrate->order_count > 25 && $driverrate->order_count >= 2) {
                $driver = Driver::find($order->driver_id);
                $driver->update(['classification', 'good']);
            }
            if ($driverrate->order_count > 25 && $driverrate->order_count <= 2) {
                $driver = Driver::find($order->driver_id);
                $driver->update(['classification', 'normal']);
            }


            #endReigon[make total rate for branch]
            DB::commit();
            return $this->response(true, 'you make review successfully');
        } catch (\Exception $ex) {
            DB::rollback();
            return $this->response(true, 'false');
        }
    }

    // we don't use this request again
    public function deliveryrate(Request $request)
    {
        $order_id = $request->order_id;
        $user_id = Auth::guard('user_api')->user()->id;
        $order = order::find($order_id);
        $delivery_review = deliveryrate::where(['user_id' => $user_id, 'order_id' => $order_id])->first();
        if ($order->user_id != $user_id)
            return $this->response(false, 'you have not access for this order', null, 401);
        if ($order == null)
            return $this->response(false, 'this order not found', null, 401);
        if ($order->progress == 'inprogress' || $order->progress == null)
            return $this->response(false, 'this order not finished yet', null, 401);
        if ($delivery_review != null)
            return $this->response(false, 'you make review for this order', null, 401);
        $rate = (int)$request->rate;

        return $this->response(true, 'you make review successfully');
    }

    // this function is old
    public function orderreview(Request $request)
    {
        $order_id = $request->order_id;
        $user_id = Auth::guard('user_api')->user()->id;
        $order = order::find($order_id);
        if ($order->user_id != $user_id)
            return $this->response(false, 'you have not access for this order', null, 401);
        if ($order == null)
            return $this->response(false, 'this order not found', null, 401);
        if ($order->progress == 'inprogress' || $order->progress == null)
            return $this->response(false, 'this order not finished yet', null, 401);
        Orderreview::create([
            'order_id' => $order_id,
            'desc' => $request->review,
            'rate' => $request->rete
        ]);
        return $this->response(true, 'you make review successfully');
    }

    #EndReigon[recice order cycle]
    //DiscountFirstOrder

    //send mail to user
    public function sendmail(Request $request)
    {
        $order = Order::find($request->order_id);
        dd($order);
        if ($order == null)
            return $this->response(false, 'order not found', null, 404);

        $urgents = Argent::where('order_id', $order->id)->get();
        $user = User::find($order->user_id);
        Mail::to($user->email)->send(new \App\Mail\InvoiceMail($order, $urgents));
        return $this->response(true, 'email send successfully');
    }

}
