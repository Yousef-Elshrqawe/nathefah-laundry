<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\App;
use App\Models\{Order\payment_method, Order\payment_methodTranslation, Point, TransactionFeedback};
use App\Repositories\WalletRepository;
use Illuminate\Http\Request;
use PaymentMethodsTrnslations;
use App\Models\Wallet\{user_wallet, user_free_wallet, Walletlogs};
use Illuminate\Support\Facades\DB;
use App\Traits\response;
use Carbon\Carbon;
use Validator;
use Auth;

class WalletController extends Controller
{
    //
    use response;

    public function __construct(WalletRepository $WalletRepository)
    {
        $this->WalletRepository = $WalletRepository;
    }

    public function getpallence(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

/*        $payMethod = payment_method::get()->makehidden('translations');
        $payMethodTrnslations = payment_methodTranslation::w*/

       $payMethod = DB::table('payment_methods')
           // id = 1 ,2 , 5 , 6
            ->join('payment_method_translations', 'payment_methods.id', '=', 'payment_method_translations.payment_method_id')
            ->where('payment_method_translations.locale', $lang)
            ->select('payment_methods.id', 'payment_methods.img' , 'payment_methods.payment_key' , 'payment_method_translations.name')
            ->get();

       // id = 1 ,2 , 5 , 6



        //paht of image
        foreach ($payMethod as $pay) {
            $pay->img = asset('payment_imgs/' . $pay->img);
        }

        $user_id = Auth::guard('user_api')->user()->id;
        $amount = $this->WalletRepository->getpallence($user_id);
        $data['amount'] = $amount;
        $data['payMethod'] = $payMethod;

        return $this->response(true, 'your amount is', $data);
    }

    // this function charge wallet of user
    public function charge(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $validator = Validator::make($request->all(), [
            'refrence_code' => 'required',
            'amount' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }

        /* if($request->merchant_reference!=null){
              $transactionfeedback = TransactionFeedback::where('merchant_reference',$request->merchant_reference)->first();
              if($transactionfeedback==null)
              return $this->response(false,'payment transaction error');
           }*/

        $user_id = Auth::guard('user_api')->user()->id;
        Walletlogs::create([
            'amount' => $request->amount,
            'user_id' => $user_id,
            'refrence_code' => $request->refrence_code,
        ]);
        $wallet = user_wallet::where('user_id', $user_id)->first();
        $amount = $wallet->amount + $request->amount;
        $wallet->update([
            'amount' => $amount
        ]);
//        return $this->response(true, 'you charge your wallet successfuly');

        if ($lang == 'ar') {
            return $this->response(true, 'تم شحن المحفظة بنجاح');
        } else {
            return $this->response(true, 'you charge your wallet successfully');
        }
    }

    public function convertPointsToWallet(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $user = Auth::guard('user_api')->user();

        //convert points to money
        $points = Point::first();
        $user_points = $user->points;
        $convert = ($user_points * $points->price) / $points->amount;

        //Add to user wallet

        $wallet = user_wallet::where('user_id', $user->id)->first();
        $wallet->update([
            'amount' => $wallet->amount + $convert,
        ]);

        //Make User Points 0
        $user->update([
            'points' => 0,
        ]);

//        return  $this->response(true,'Points added to wallet successfully');
        if ($lang == 'ar') {
            return $this->response(true, 'تمت إضافة النقاط إلى المحفظة بنجاح');
        } else {
            return $this->response(true, 'Points added to wallet successfully');
        }

    }

    public function getpoints(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $user_points = Auth::guard('user_api')->user()->points;
        $points = Point::first();
        $convert = ($user_points * $points->price) / $points->amount;
        $data['points'] = $user_points;
        $data['equality_mony'] = $convert;


//        return $this->response(true, 'get point successfuly', $data);

        if ($lang == 'ar') {
            return $this->response(true, 'تم الحصول على النقاط بنجاح', $data);
        } else {
            return $this->response(true, 'get point successfully', $data);
        }
    }
}
