<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class TermsandCondittionController extends Controller
{
    //
    public function termsindex(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        if ($lang == 'ar') {
            return view('user.termsandcondition.index-ar');
        } else {
            return view('user.termsandcondition.index');
        }
    }

    public function policyindex(Request $request)
    {

            if ($lang = $request->header('lang') == null) {
                $lang = 'ar';
            } else {
                $lang = $request->header('lang');
            }
            App::setLocale($lang);

        if ($lang == 'ar') {
            return view('user.termsandcondition.policy-ar');
        } else {
            return view('user.termsandcondition.policy');
        }
    }

}
