<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Laundry\branch;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Models\User\PaymentCards;
use App\Models\TransactionFeedback;
use Auth;

class PaymentController extends Controller
{
    //
    use response;

    ## payfort payment gateway production ##
    private $access_code = 'U1xDaJUuiyE8hA2OAE70';
    private $merchant_identifier = 'EIIWLjkk';
    private $sha_request_phrase = '15KHEd88OBA6MoXh4BtVD9$&';
    private $sha_type = 'sha256';
    private $url = 'https://paymentservices.payfort.com/FortAPI/paymentApi';
    private $urlStc = 'https://checkout.payfort.com/FortAPI/paymentPage';

    ## payfort payment gateway test ##
    /* private $access_code = 'ECLmBCXFjMVcehgasQ8d';
     private $merchant_identifier = '00f1ca9c';
     private $sha_request_phrase = '46PXPeVmlpRrScEAVgVV9U)]';
     private $sha_type = 'sha256';
     private  $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi'; */


    ## get sdk token for payfort payment gateway ##
    public function sdktoken(Request $request)
    {

        $shaString = '';
        $arrData = array(
            'access_code' => $this->access_code,
            'device_id' => $request->device_id,
            'language' => $request->header('lang'),
            'merchant_identifier' => $this->merchant_identifier,
            'service_command' => 'SDK_TOKEN',
        );

        ksort($arrData);
        foreach ($arrData as $key => $value) {
            $shaString .= "$key=$value";
        }
        $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
        $signature = hash($this->sha_type, $shaString);

        $response = Http::post($this->url, [
            'access_code' => $this->access_code,
            'device_id' => $request->device_id,
            'language' => $request->header('lang'),
            'merchant_identifier' => $this->merchant_identifier,
            'service_command' => 'SDK_TOKEN',
            "signature" => $signature
        ]);
        $response = json_decode($response);
        return $this->response(true, 'get sdktoken success', $response);
    }

    ## generate otp for payfort payment gateway ##
    public function generateotd(Request $request)
    {

        $shaString = '';
        $arrData = array(
            'access_code' => $this->access_code,
            'amount' => $request->amount,
            'command' => $request->command,
            'currency' => 'SAR',
            'customer_email' => $request->customer_email,
            'digital_wallet' => 'STCPAY',
            'language' => $request->header('lang'),
            'merchant_identifier' => $this->merchant_identifier,
            'merchant_reference' => $request->merchant_reference,
            'return_url' => $request->return_url
        );

        ksort($arrData);
        foreach ($arrData as $key => $value) {
            $shaString .= "$key=$value";
        }
        $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
        $signature = hash("sha256", $shaString);
        $data['signature'] = $signature;
        $data['access_code'] = $this->access_code;
        $data['merchant_identifier'] = $this->merchant_identifier;
        return $this->response(true, 'get sdktoken success', $data);
    }

    ## save payment card ##
    public function savepaymentcard(Request $request)
    {
        $user_id = Auth::guard('user_api')->user()->id;
        PaymentCards::create([
            'user_id' => $user_id,
            'token_name' => $request->token_name,
            'card_name' => $request->card_name,
            'type' => $request->type ?? 'visa' , // visa , master , mada
            'card_holder_name' => $request->card_holder_name,
        ]);

        return $this->response(true, 'save curd successful');
    }

    ## get user payment cards ##
    public function getusercurds()
    {
        $user = Auth::guard('user_api')->user();
        $PaymentCards = PaymentCards::where('user_id', $user->id) ->orderBy('is_favorite', 'desc')->get();
        $data['PaymentCards'] = $PaymentCards;
        return $this->response(true, 'get curdes successfully', $data);
    }

    //Add the user's preferred payment card
    public function addFavoriteCard(Request $request)
    {
        $user_id = Auth::guard('user_api')->user()->id;
        //Verify that this user has cards at all
        $PaymentCards = PaymentCards::where('user_id', $user_id)->get();
        if ($PaymentCards->count() == 0) {
            return $this->response(false, __('admin.You have no registered cards') . '!');
        }

        //Verify that the card belongs to the user
        $PaymentCards = PaymentCards::where('id', $request->card_id)->where('user_id', $user_id)->first();
        if ($PaymentCards == null) {
            return $this->response(false, __('admin.This card does not belong to you') . '!');
        }

        //Update the card to be the preferred card
        PaymentCards::where('user_id', $user_id)->update(['is_favorite' => 0]);
        PaymentCards::where('id', $request->card_id)->update(['is_favorite' => 1]);
        return $this->response(true, __('admin.The favorite card has been added successfully') . '!');
    }


    //getbranchpaymentmethod
    public function getbranchpaymentmethod(Request $request , $id)
    {
        $lang = $request->header('lang');
        $branch_id =  $id;

        $branch = Branch::where('id', $branch_id)->select('id', 'cash', 'visa')->first();
        $paymentmethods=DB::table('branch_payments')
            ->join('payment_method_translations','payment_method_translations.payment_method_id','=','branch_payments.payment_method_id')
            ->join('payment_methods','payment_methods.id','=','branch_payments.payment_method_id')
            ->select('branch_payments.id','payment_method_translations.name','branch_payments.status','payment_method_translations.name'
                ,'payment_methods.img','branch_payments.payment_method_id','payment_methods.payment_key')
            ->where('payment_method_translations.locale',$lang)
            ->where('branch_payments.branch_id',$branch_id)
            ->where('status',1)
            ->get();

        foreach($paymentmethods as $payment){
            $payment->img=asset('payment_imgs/'.$payment->img);
        }
        $data['branch'] = $branch;
        $data['paymentmethods'] = $paymentmethods;
        return $this->response(true,'get payment methods',$data);

    }



    // حذف بطاقه الدفع
    public function deleteCard(Request $request , $id)
    {
        $user_id = Auth::guard('user_api')->user()->id;
        $PaymentCards = PaymentCards::where('id', $id)->where('user_id', $user_id)->first();
        if ($PaymentCards == null) {
            return $this->response(false, __('admin.This card does not belong to you') . '!');
        }
        $PaymentCards->delete();
        return $this->response(true, __('admin.The card has been deleted successfully') . '!');
    }


    ## transaction feedback ##
    public function transactionFeedback(Request $request)
    {
//        dd($request->all());
        $TransactionFeedback = new TransactionFeedback();
        $TransactionFeedback->response = json_encode($request->all());
        $TransactionFeedback->merchant_reference = $request->merchant_reference;
        $TransactionFeedback->response_code = $request->response_code;
        $TransactionFeedback->fort_id = $request->fort_id;
        $TransactionFeedback->save();
        return $this->response(true, 'save feed back');
    }


    ## transaction  success  or fail ##
    public function successtransaction(Request $request)
    {
        $TransactionFeedback = TransactionFeedback::where('merchant_reference', $request->merchant_reference)->first();

        if ($TransactionFeedback != null) {
            if ($TransactionFeedback->response_code != null && $TransactionFeedback->response_code == '14000') {
                return view('user.payment.returnsuccess');
            } else {
                return view('user.payment.returnfailed');
            }
        } else {
            return view('user.payment.returnfailed');
        }


    }


}
