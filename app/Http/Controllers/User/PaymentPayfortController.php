<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Order\order;
use Illuminate\Support\Facades\Http;
use Illuminate\Http\Request;
use App\Traits\response;
use App\Models\User\PaymentCards;
use App\Models\TransactionFeedback;
use Auth;
class PaymentPayfortController extends Controller
{
    //
    use response;

    ## payfort payment gateway production ##
   /* private $access_code = 'U1xDaJUuiyE8hA2OAE70';
    private $merchant_identifier = 'EIIWLjkk';
    private $sha_request_phrase = '15KHEd88OBA6MoXh4BtVD9$&';
    private $sha_type = 'sha256';
    private  $url = 'https://paymentservices.payfort.com/FortAPI/paymentApi';*/

    ## payfort payment gateway test ##
     private $access_code = 'ECLmBCXFjMVcehgasQ8d';
     private $merchant_identifier = '00f1ca9c';
     private $sha_request_phrase = '46PXPeVmlpRrScEAVgVV9U)]';
     private $sha_type = 'sha256';
     private  $url = 'https://sbpaymentservices.payfort.com/FortAPI/paymentApi';




    ## get sdk token for payfort payment gateway ##
    public function sdktoken(Request $request)
    {
        $shaString = '';
        $arrData = array(
            'access_code'           => $this->access_code,
            'device_id'             => $request->device_id,
            'language'              => $request->header('lang'),
            'merchant_identifier'   => $this->merchant_identifier,
            'service_command'       => 'SDK_TOKEN',
        );

        ksort($arrData);
        foreach ($arrData as $key => $value) {
            $shaString .= "$key=$value";
        }
        $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
        $signature = hash($this->sha_type, $shaString);

        $response = Http::post($this->url, [
            'access_code'         => $this->access_code,
            'device_id'           => $request->device_id,
            'language'            => $request->header('lang'),
            'merchant_identifier' => $this->merchant_identifier,
            'service_command'     => 'SDK_TOKEN',
            "signature"           => $signature
        ]);
        $response = json_decode($response);
        return $this->response(true, 'get sdktoken success', $response);
    }

    ## generate otp for payfort payment gateway ##
    public function generateotd(Request $request)
    {
        $shaString = '';
        $arrData = array(
            'access_code'           => $this->access_code,
            'amount'                => $request->amount,
            'command'               => $request->command,
            'currency'              => 'SAR',
            'customer_email'        => $request->customer_email,
            'digital_wallet'        => 'STCPAY',
            'language'              => $request->header('lang'),
            'merchant_identifier'   => $this->merchant_identifier,
            'merchant_reference'    => $request->merchant_reference,
            'return_url'            => $request->return_url
        );

        ksort($arrData);
        foreach ($arrData as $key => $value) {
            $shaString .= "$key=$value";
        }
        $shaString = $this->sha_request_phrase . $shaString . $this->sha_request_phrase;
        $signature = hash("sha256", $shaString);
        $data['signature']           = $signature;
        $data['access_code']         = $this->access_code;
        $data['merchant_identifier'] = $this->merchant_identifier;
        return $this->response(true, 'get sdktoken success', $data);
    }



    ## transaction feedback ##
    public function transactionFeedback(Request $request)
    {

        $TransactionFeedback = new TransactionFeedback();
        $TransactionFeedback->response = json_encode($request->all());
        $TransactionFeedback->merchant_reference = $request->merchant_reference;
        $TransactionFeedback->response_code      = $request->response_code;
        $TransactionFeedback->fort_id            = $request->fort_id;
        $TransactionFeedback->save();

        //هاتلي  اخر  اوردر  للعميل وضيفي له العملية
        $order = Order::where('user_id', Auth::id())->orderBy('id', 'desc')->first();
        $order->fort_id = $request->fort_id;
        $order->save();
        return $this->response(true, 'save feed back');
    }



    ## transaction  success  or fail ##
    public function successtransaction(Request $request)
    {

        $TransactionFeedback = TransactionFeedback::where('merchant_reference', $request->merchant_reference)->first();
        if ($TransactionFeedback == null) {
            //سجلي العملية في جدول العمليات
            $TransactionFeedback = new TransactionFeedback();
            $TransactionFeedback->response = json_encode($request->all());
            $TransactionFeedback->merchant_reference = $request->merchant_reference;
            $TransactionFeedback->response_code = $request->response_code;
            $TransactionFeedback->fort_id = $request->fort_id;
            $TransactionFeedback->save();
            $order = Order::where('user_id', Auth::id())->orderBy('id', 'desc')->first();
            $order->fort_id = $request->fort_id;
            $order->save();
        }

        if ($TransactionFeedback != null) {
            if ($TransactionFeedback->response_code != null && $TransactionFeedback->response_code == '14000') {
                return view('user.payment.returnsuccess');
            } else {
                return view('user.payment.returnfailed');
            }
        } else {
            return view('user.payment.returnfailed');
        }


    }


}
