<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Day;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use App\Models\Laundry\branchservice;
use App\Interfaces\BranchRepositoryInterface;
use App\Models\Order\{order,Orderreview};
use Validator;
use App\Traits\response;
use App;
class LaundryController extends Controller
{
    //
    use response;
    private BranchRepositoryInterface $BranchRepository;
    public function __construct(BranchRepositoryInterface $BranchRepository)
    {
        $this->BranchRepository = $BranchRepository;
    }
    function sort($arr){
        $n=count($arr);
        if($n<= 1){
            return $arr;
        }
        else{
            $pivot = array();
            $pivot[0]['id'] = $arr[0]['id'];
            $pivot[0]['name'] = $arr[0]['name'];
            $pivot[0]['username'] = $arr[0]['username'];
            $pivot[0]['address'] = $arr[0]['address'];
            $pivot[0]['distance'] = $arr[0]['distance'];
            $left = array();
            $right = array();
            for($i = 1; $i < count($arr); $i++)
            {
                if($arr[$i]['distance'] < $pivot[0]['distance']){
                    $left[] = $arr[$i];
                }
                else{
                    $right[] = $arr[$i];
                }
            }
            return array_merge(self::sort($left), $pivot,self::sort($right));
        }
      }

    //end of distance
    public function laundryinfo(Request $request){
        $lang=$request->header('lang');
        App::setLocale($lang);
        $validator =Validator::make($request->all(), [
            'branch_id'=>'required',
            'lat'=>'required',
            'long'=>'required',
          ]);
          if ($validator->fails()) {
            return $this->response(false,$validator->messages()->first(),null,401);
          }
           $id=$request->branch_id;
            $branch=branch::select('id','username','laundry_id','lat','long','address','cash','visa','delivery_status','rate','status')->with('laundry',function($q){
            $q->select('id','name','logo','door_to_door_delivery_fees','one_way_delivery_fees')->get();
            })->addSelect([
                'success_order_count' => order::selectRaw('count(id)')
                    ->whereColumn('branchs.id', 'orders.branch_id')
                    ->whereIn('progress', ['completed', 'finished'])
            ])->addSelect([
                'order_reviews' => Orderreview::selectRaw('count(id)')
                    ->whereColumn('branchs.id', 'order_reviews.branch_id')
            ])->find($id);
            $branch->distance=$this->BranchRepository->distance((float)$branch->lat,(float)$branch->long,(float)$request->lat,(float)$request->long,'k');
            $brnach_services=DB::table('brnachservices')
            ->select('servicetranslations.name' ,'brnachservices.expected_time')
            ->join('servicetranslations','servicetranslations.service_id','=','brnachservices.service_id')
            ->where('servicetranslations.locale',$lang)
            ->where('brnachservices.branch_id',$id)->get();

            foreach($brnach_services as $service){
                $service=$service-> expected_time = $service-> expected_time ;
            }
           $branch->brnach_services=$brnach_services;
           $branch->laundry->rate='very good';
           $branch->delivery_fees=$branch->laundry->door_to_door_delivery_fees;
           $branch->one_way_delivery_fees=$branch->laundry->one_way_delivery_fees;
           $branch->averge_service_time=22;
           $branch->live_tracking=true;
           $data['branch_info']=$branch;
           return $this->response(true,'get branch info successfuly',$data);
    }
    public function getlaundries(Request $request){
        // in this query get branches by location
        $today = Carbon::now()->format('l');
        $day = Day::where('name', $today)->first();
        $time = Carbon::now()->format('H:i:s');
        $lat=$request->lat;
        $long=$request->long;
        $branchs=DB::table("branchs")->
        where('branchs.deleted' , 1)->
        select('branchs.id as id','branchs.username','laundries.name','laundries.logo','branchs_rate.rate as rate'
        ,'branchs.visa','branchs.cash','branchs.delivery_status'
        ,'laundries.door_to_door_delivery_fees as delivery_fees','laundries.one_way_delivery_fees'
        ,'laundries.taxes','laundries.taxamount'

        ,DB::raw("round(6371 * acos(cos(radians(" . $lat . "))
        * cos(radians(branchs.lat))
        * cos(radians(branchs.long) - radians(" . $long . "))
        + sin(radians(" .$lat. "))
        * sin(radians(branchs.lat))),1) AS distance"))
            ->join('schedules', function ($join) use ($day, $time) {
                $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                    ->where('schedules.scheduleable_type', '=', 'App\Models\Laundry\branch')
                    ->where('schedules.day_id', $day->id)
                    ->where('schedules.is_active', 1)
                    ->where('schedules.start_time', '<=', $time)
                    ->where('schedules.end_time', '>=', $time);
            })
        ->join('laundries','laundries.id','=','branchs.laundry_id')
        ->leftjoin('branchs_rate','branchs_rate.branch_id','=','branchs.id')
        ->join('brnachservices','brnachservices.branch_id','=','branchs.id')
        ->where('branchs.status','open')
        ->where('laundries.status',true)

        ->groupBy("branchs.id")
        ->groupBy("branchs.lat")
        ->groupBy("branchs.long")
        ->groupBy("branchs.username")
        ->groupBy("branchs.visa")
        ->groupBy("branchs.cash")
        ->groupBy("branchs.delivery_status")
        ->groupBy("laundries.door_to_door_delivery_fees")
        ->groupBy("laundries.one_way_delivery_fees")
        ->groupBy("laundries.taxes")
        ->groupBy("laundries.taxamount")
        ->groupBy("branchs_rate.rate")
        ->groupBy("laundries.name")
        ->groupBy("laundries.logo")
        ->havingRaw('distance <= 20')
        ->orderby('distance','ASC')
        ->paginate(5);
        foreach($branchs as $branch){
            if($branch->logo==null){
                $branch->logo=asset("/uploads/branches/logos/default/images.jpg");
            }else{
                $branch->logo=asset("uploads/laundry/logos/".$branch->logo);
            }
           // $branch->rate=$this->BranchRepository->branchrate($branch->rate);
        }

        return $branchs;

        return $this->response(true,'get all pranches successfuly',$branchs);
    }
    // search by laundry name
    public function search(Request $request){
        $today = Carbon::now()->format('l');
        $day = Day::where('name', $today)->first();
        $time = Carbon::now()->format('H:i:s');
       $lat=$request->lat;
       $long=$request->long;
       $branchs=DB::table("branchs")->
           where('branchs.deleted',1)->
       select('branchs.id as id','branchs.username','laundries.name','laundries.logo','branchs_rate.rate as rate'
       ,'branchs.visa','branchs.cash','branchs.delivery_status'
       ,'laundries.door_to_door_delivery_fees as delivery_fees','one_way_delivery_fees'
       ,'laundries.taxes','laundries.taxamount'
       ,DB::raw("round(6371 * acos(cos(radians(" . $lat . "))
       * cos(radians(branchs.lat))
       * cos(radians(branchs.long) - radians(" . $long . "))
       + sin(radians(" .$lat. "))
       * sin(radians(branchs.lat))),1) AS distance"))
           ->join('schedules', function ($join) use ($day, $time) {
               $join->on('schedules.scheduleable_id', '=', 'branchs.id')
                   ->where('schedules.scheduleable_type', '=', 'App\Models\Laundry\branch')
                   ->where('schedules.day_id', $day->id)
                   ->where('schedules.is_active', 1)
                   ->where('schedules.start_time', '<=', $time)
                   ->where('schedules.end_time', '>=', $time);
           })
       ->join('laundries','laundries.id','=','branchs.laundry_id')
       ->leftjoin('branchs_rate','branchs_rate.branch_id','=','branchs.id')
       ->where('laundries.status',true)
       ->groupBy("branchs.id")
       ->groupBy("branchs.username")
       ->groupBy("branchs_rate.rate")
       ->groupBy("branchs.visa")
       ->groupBy("branchs.cash")
       ->groupBy("branchs.delivery_status")
       ->groupBy("laundries.door_to_door_delivery_fees")
       ->groupBy("laundries.one_way_delivery_fees")
       ->groupBy("laundries.taxes")
       ->groupBy("laundries.taxamount")
       ->groupBy("laundries.name")
       ->groupBy("laundries.logo")
       ->groupBy("branchs.lat")
       ->groupBy("branchs.long")
       ->havingRaw('distance <= 20')
       ->orderby('rate','ASC')

       ->where('laundries.name','like',"{$request->laundry_name}%")
       ->where('branchs.status','open')
       //->orwhere('branchs.username','like',"{$request->laundry_name}%")
       ->paginate(100);
       foreach($branchs as $branch){
           if($branch->logo==null){
               $branch->logo=asset("/uploads/branches/logos/default/images.jpg");
           }else{
               $branch->logo=asset("uploads/laundry/logos/".$branch->logo);
           }
       }
       return $branchs;

    }
    public function laundryreviews(Request $request){
       $orders=DB::table('orders')
       ->join('order_reviews','order_reviews.order_id','=','orders.id')
       ->select('customer_name','order_reviews.desc as comment','order_reviews.rate','orders.created_at')
       ->where('orders.rate',true)
       ->where('orders.branch_id',$request->branch_id)
       ->orderBy('created_at','desc')
       ->get()
       ->take(50);
        foreach($orders as $order){
            //$order->rate=$this->BranchRepository->branchrate($order->rate);
            $order->created_at=date('Y-m-d', strtotime($order->created_at));
            $order->time=date('h:m a', strtotime($order->created_at));
        }
           $data['branch_reviews']=$orders;
           return $this->response(true,'get branch reviews successfuly',$data);
    }









}
