<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Interfaces\UserRepositoryInterface;
use App\Models\User;
use App\Models\User\Adress;
use Illuminate\Http\Request;
use App\Traits\response;
use Auth;
use Validator;
use App;

class AdressController extends Controller
{
    //
    use response;
    private UserRepositoryInterface $UserRepository;
    public function __construct(UserRepositoryInterface $UserRepository)
    {
        $this->UserRepository = $UserRepository;
    }

    public function createadress(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'lat' => 'required',
            'long' => 'required',
            'title' => 'nullable',
        ]);
        if ($validator->fails()) {
            return $this->response(false, $validator->messages()->first(), null, 401);
        }
        $user_id = Auth::guard('user_api')->user()->id;
        //If lat and long are present in the verb, the user will tell me that it already exists
        $user = User::where('id', $user_id)->first();
        $lat = number_format($request->lat, 3);
        $long = number_format($request->long, 3);

    //Do 4H on the address that I got from the user and select lat and long
        $adress = Adress::where('user_id', $user_id)->get();
        foreach ($adress as $ad) {
            $lat1 = number_format($ad->lat, 3);
            $long1 = number_format($ad->long, 3);
            if ($lat == $lat1 && $long == $long1) {
                return $this->response(false, $lang == 'ar' ? 'العنوان موجود بالفعل' : 'address already exists' , null, 406);
            }
        }

        $adress = $this->UserRepository->addaddress($request, $user_id);
        if (is_array($adress)) {
            return $this->response(false, $adress['message']);
        }
        return $this->response(true, $lang == 'ar' ? 'تم اضافة العنوان بنجاح' : 'address added successfully');
    }
    public function updateaddress(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $user_id=Auth::guard('user_api')->user()->id;
        $adress= $this->UserRepository->updateaddress($request,$user_id);
        if(is_array($adress)){
            return $this->response(false,$adress['message']);
        }
        return $this->response(true,$lang=='ar'?'تم تحديث العنوان بنجاح':'address updated successfully');
    }
    public function deleteadress(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $adress_id=$request->adress_id;
        $adress= $this->UserRepository->deleteadress($adress_id);
        if(is_array($adress)){
            return $this->response(false,$adress['message']);
         }
        return $this->response(true, $lang=='ar'?'تم حذف العنوان بنجاح':'address deleted successfully');
    }
}
