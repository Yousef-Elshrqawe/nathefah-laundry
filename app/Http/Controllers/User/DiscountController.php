<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Interfaces\DiscountRepositoryInterface;
use App\Repositories\DiscountRepository;
use App\Traits\response;

class DiscountController extends Controller
{
    //
    use response;
    public function __construct(DiscountRepositoryInterface $DiscountRepository)
    {
        $this->DiscountRepository = $DiscountRepository;
    }
    public function creatediscount(Request $request){
      $discount=$this->DiscountRepository->creatediscount($request);
    }
    public function getdiscount(Request $request){
        $discounts=$this->DiscountRepository->getdiscount($request);
        $data['discounts']=$discounts;
        return $this->response(true,'get discount success',$data);
    }
    public function usediscount(Request $request){
        //lang
        $lang = $request->header('lang');
        app()->setLocale($lang);

        $usediscount=$this->DiscountRepository->usediscount($request->order_id,$request->code , $lang);
        if(is_array($usediscount)){
            $message=$usediscount['message'];
            return $this->response(false,$message,null,409);
        }
        $data['price_after_discount']=$usediscount;
        return $this->response(true,'get price after disount successfuly',$data);
    }
}
