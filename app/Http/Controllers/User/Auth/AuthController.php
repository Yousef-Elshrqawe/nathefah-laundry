<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Requests\CreateUserRequest;
use App\Interfaces\NotificationRepositoryinterface;
use App\Interfaces\UserRepositoryInterface;
use App\Http\Controllers\Controller;
use App\Models\Laundry\Branchuser;
use App\Models\Notification\Usernotifytype;
use App\Models\TermsCondition;
use App\Models\UserTerms;
use App\Models\Wallet\user_wallet;
use App\Models\Discount\Discount;
use Illuminate\Http\Request;
use App\Models\User\Adress;
use App\Traits\response;
use App\Models\User;
use Auth;
use Hash;
use App\Traits\otp;
use Illuminate\Support\Facades\App;
use Validator;

class AuthController extends Controller
{
    //
    use response, otp;

    private UserRepositoryInterface $UserRepository;

    public function __construct(UserRepositoryInterface $UserRepository, NotificationRepositoryinterface $NotificationRepository)
    {
        $this->UserRepository = $UserRepository;
        $this->NotificationRepository = $NotificationRepository;
    }

    public function signin(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $user = $this->UserRepository->signin($request);
        if (is_array($user) && isset($user['message'])) {
            return $this->response(false, $user['message'], null, 401);
        }
        $data['token'] = $user['token'];
        $data['currentaddress'] = $user['currentaddress'];
        if ($lang == 'ar') {
            return $this->response(true, 'قمت بتسجيل الدخول بنجاح', $data);

        } else {
            return $this->response(true, 'you logged in successfully', $data);

        }
    }


    public function register(CreateUserRequest $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

           $last_id = User::latest()->first();
        if ($last_id == null) {
            $last_id = 1;
        } else {
            $last_id = $last_id->id;
        }

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'country_code' => $request->country_code,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'otp' => $this->randomOtp(),
            'verified' => true,
        ]);

        $user_wallet = user_wallet::create([
            'user_id' => $user->id,
            'amount' => 0,
        ]);

        $termsCondition = TermsCondition::first();
        $userTerms = UserTerms::create([
            'user_id' => $user->id,
            'terms_condition_id' => $termsCondition->id,
            'is_read' => 0,
        ]);

        $credentials = ['phone' => $user->phone, 'password' => $request->password];
        if (!$token = auth()->guard('user_api')->attempt($credentials)) {
            if ($lang == 'ar') {
                return response()->json(['error' => 'رقم هاتفك أو كلمة المرور قد تكون غير صحيحة ، يرجى المحاولة مرة أخرى'], 401);
            } else {
                return response()->json(['error' => 'Your user phone or password maybe incorrect, please try agian'], 401);
            }
        }

        $this->NotificationRepository->createnotificationsetting('user', $user->id);

//        $this->sendSmsMessage($request, 'user', __('auth.Your Nathefah verification code is') . $user->otp, $user->otp);


   /*     Usernotifytype::create([
            'user_id' => $user->id,
            'notificationtype_id' => 3,
            'status' => 1,
        ]);


        Usernotifytype::create([
            'user_id' => $user->id,
            'notificationtype_id' => 5,
            'status' => 1,
        ]);*/


        $data = [];
        $data['token'] = $token;
        if ($lang == 'ar') {
            return $this->response(true, 'تم إنشاء الحساب بنجاح اذهب الى تسجيل الدخول', $data);
        } else {
            return $this->response(true, 'account created successfully go to login', $data);
        }


        /*   $token = $this->UserRepository->createUser($request);
           if (is_array($token)) {
               return $this->response(false, $token['message'], null, 403);
           }
           $data['token'] = $token;
           // discount new  user
            $discount=Discount::where('type','new-user')->first();
              if($discount!=null){
                 $userDiscount=App\Models\Discount\UserDiscount::create([
                     'user_id'=>Auth::guard('user_api')->user()->id,
                     'discount_id'=>$discount->id,
                 ]);
              }*/

        /*      $discount = Discount::all();
              if (!$discount->isEmpty()) {
                  foreach ($discount as $discount) {
                      $userDiscount = App\Models\Discount\UserDiscount::create([
                          'user_id' => Auth::guard('user_api')->user()->id,
                          'discount_id' => $discount->id,
                      ]);
                  }
              }*/

//        return $this->response(true, 'accout created successfuly go to login', $data);

    }


    public function verifyphone(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $verifyphone = $this->UserRepository->verifyphone($request);
        if ($verifyphone == false) {
            return $this->response(false, 'otp is false');
        }
        return $this->response(true, 'account verfied');
    }


    public function logout(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        if ($request->device_token != null)
            $user_id = Auth::guard('user_api')->user()->id;
        $this->NotificationRepository->delete_devicetoken($request->device_token, 'user', $user_id);
        Auth::guard('user_api')->logout();
        return response()->json([
            'status' => true,
            'message' => $lang == 'ar' ? 'تم تسجيل الخروج بنجاح' : 'logged out successfully'
        ]);
    }


    public function checkphone(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $verifyphone = $this->UserRepository->checkphone($request);
        if (is_array($verifyphone) && isset($verifyphone['message'])) {
            return $this->response(true, $verifyphone['message'], ['status' => false], 200);
        }
        $data['messageText'] = $verifyphone;
        $data['status'] = true;
        return $this->response(true, 'this number is exist', $data);
    }


    public function forgetpassword(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $validator = Validator::make($request->all(), [
            'country_code' => 'required',
            'phone' => 'required',
            'password' => 'required|min:6|max:50|confirmed',
            'password_confirmation' => 'required|max:50|min:6',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $user = User::where(['country_code' => $request->country_code, 'phone' => $request->phone])->first();
        if ($user == null) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'هذا الرقم غير موجود'], 401);
            } else {
                return response()->json(['status' => false, 'message' => 'this number not exist'], 401);
            }
        }
        if ($user->otp != $request->otp) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'كود التحقق خاطئ'], 401);
            } else {
                return response()->json(['status' => false, 'message' => 'otp code is false'], 401);
            }
        }
        $user->update([
            'password' => Hash::make($request->password),
        ]);
        $credentials = ['phone' => $user->phone, 'password' => $request->password];
        if (!$token = auth()->guard('user_api')->attempt($credentials)) {
            if ($lang == 'ar') {
                return response()->json(['error' => 'رقم هاتفك أو كلمة المرور قد تكون غير صحيحة ، يرجى المحاولة مرة أخرى'], 401);
            } else {
                return response()->json(['error' => 'Your user phone or password maybe incorrect, please try agian'], 401);
            }
        }
        $data = [];
        $data['status'] = true;
        $data['message'] = $lang == 'ar' ? 'تم تغيير كلمة المرور بنجاح' : 'password changed successfully';
        $data['data']['token'] = $token;
        return response()->json($data);
    }


    public function storefaceid(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'faceid' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $user = Auth::guard('user_api')->user();
        $user->update([
            'faceid' => $request->faceid
        ]);
        return $this->response(true, $lang == 'ar' ? 'تم تسجيل بيانات الوجه بنجاح' : 'face data saved successfully');
    }

    public function loginfaceid(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'faceid' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 403);
        }
        $user = User::where('faceid', $request->faceid)->first();
        if ($user == null) {

            if ($lang == 'ar') {
                return $this->response(false, 'هذا الوجه غير مسجل');
            } else {
                return $this->response(false, 'your data is wrong');

            }
        }
        if (!$token = auth()->guard('user_api')->tokenById($user->id)) {
            return response()->json(['message' => 'token is false'], 401);
        }
        $currentaddress = Adress::where(['user_id' => $user->id, 'curent' => 1])->first();
        $data['token'] = $token;
        $data['currentaddress'] = $currentaddress;
        return $this->response(true, $lang == 'ar' ? 'تم تسجيل الدخول بنجاح' : 'you logged in successfully', $data);

    }

    public function sendotp(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $messageText = $this->randomOtp();

        $user = User::where(['phone' => $request->phone, 'country_code' => $request->country_code])->first();
        if ($user != null) {
            if ($lang == 'ar') {
                return response()->json(['status' => false, 'message' => 'لقد تم أخذ الهاتف بالفعل .'], 406);

            } else {
                return response()->json(['status' => false, 'message' => 'The phone has already been taken .'], 406);

            }

        }


        if ($request->lang == 'ar') {
            $messageText = intval($messageText);
            $sendMessgeOtp = 'كود التحقق الخاص بك لتطبيق نظيفة هو : ' . $messageText;
        } else {
            $messageText = intval($messageText);
            $sendMessgeOtp = 'Your Nathefah verification code is : ' . $messageText;
        }

        $message = $this->sendSmsMessage($request, 'user', $sendMessgeOtp, $messageText);

        if ($message == false)
            return response()->json(['status' => false, 'message' => 'The phone has already been taken .'], 406);

        return response()->json($message);

    }

    public function checkotp(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        $validator = Validator::make($request->all(), [
            'phone' => 'required',
            'country_code' => 'required',
            'otp' => 'required'
        ]);
        if ($validator->fails()) {
            return [
                'message' => $validator->messages()->first()
            ];
        }
        $user = User::where(['phone' => $request->phone, 'country_code' => $request->country_code, 'otp' => $request->otp])->first();
        if ($user == null){

            if ($lang == 'ar') {
                return $this->response(false, 'الكود غير صحيح', null, 406);

            } else {
                return $this->response(false, 'the otp is wrong', null, 406);
            }

        }

        return $this->response(true, $lang == 'ar' ? 'الكود صحيح' : 'go to next request');
    }
}
