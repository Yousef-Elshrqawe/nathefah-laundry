<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Interfaces\OrderRepositoryInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Validator;
use App\Models\{laundryservice\Service, Support, Setting};
use App\Traits\response;


class SupportController extends Controller
{
    //
    use response;
    public function store(Request $request){
        $validator =Validator::make($request->all(), [
            'full_name'=> 'required',
            'email' => 'required',
            'country_code'=> 'required',
            'phone' => 'required',
            'full_name'=> 'required',
        ]);
        if($validator->fails()) {
            return $this->response(false,$validator->messages()->first(),null,401);
        }
        Support::create($request->all());
        $data['status']=true;
        $data['message']='your problem send successfuly';
        return response()->json($data);
    }

    public function getSupportPhone(){

        $setting=Setting::select('support_phone','country_code')->first();



        $data['country_code']= $setting->country_code;
        $data['support_phone']=$setting->support_phone;

        return $this->response(true,'you get support phone success',$data);


    }
    public function nmnm()
    {
        dd('nmnm');
    }

    public function laundryservices(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $services = DB::table('services')
            ->select('servicetranslations.name', 'services.id')
            ->join('brnachservices', 'brnachservices.service_id', '=', 'services.id')
            ->join('servicetranslations', 'servicetranslations.service_id', 'services.id')
            ->where('brnachservices.branch_id',$request->branch_id)
            ->where('servicetranslations.locale', $lang)
            ->where('brnachservices.status', 'on')
            ->get();
        return $services;
        $data['services'] = $services;
        return $this->response(true, 'get services succefully', $data);
    }
}
