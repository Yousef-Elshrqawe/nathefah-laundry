<?php

namespace App\Http\Controllers;

use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use App\Models\laundryservice\Argent;
use App\Models\Order\order;
use App\Models\Setting;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PDF;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class PDFcontroller extends Controller
{
    public function generateInvoice(Request $request , $id)
    {
        $setting=Setting::select('support_phone','country_code')->first();
        $order = Order::find($id);
        $urgents = Argent::where('order_id', $order->id)->get();
        $qrCode = QrCode::size(100)->generate(route('generateInvoice.download', $order->id));
        $branch = branch::where('id', $order->branch_id)->first();
        $laundry = Laundry::where('id', $branch->laundry_id)->first();
        $pdf = PDF::loadView('pdf.invoice', compact('order', 'urgents' , 'qrCode','setting','branch','laundry'));
        $pdf->save(public_path('invoices/invoice' . $order->id . '.pdf'));
        return $pdf->download('invoice' . $order->id . '.pdf');
    }


}
