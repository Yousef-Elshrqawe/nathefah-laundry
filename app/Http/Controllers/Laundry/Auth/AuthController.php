<?php

namespace App\Http\Controllers\Laundry\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\Traits\GeneralTrait;
use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use App\Models\Laundry\LaunbryTranslation;
use App\Interfaces\SubscriptionRepositoryInterface;
use App\Traits\fileTrait;
use App\Http\Resources\Branchinfo;
use App\Models\Wallet\laundry_wallet;
use App\Models\Admin\{Deliveryfees};
use Illuminate\Support\Facades\DB;
use Validator;
use Hash;
use App;
use App\Models\{Package, TransactionFeedback};

//LaundryPackage
use App\Traits\response;
use App\Traits\otp;


class AuthController extends Controller
{
    use fileTrait;
    use GeneralTrait;
    use response;
    use otp;

    private SubscriptionRepositoryInterface $SubscriptionRepositoryInterface;

    public function __construct(SubscriptionRepositoryInterface $SubscriptionRepository)
    {
        $this->SubscriptionRepository = $SubscriptionRepository;
    }

    public function login(Request $request)
    {
        $credentials = request(['email', 'password']);
        if (!$token = auth('laundry_api')->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
        // return response()->json(Auth::guard('laundry-api')->check());

        return response()->json($token);
        // return $this->returnData('token', $token, $msg = "");
        // return $this->respo($token);
    }

    public function registration(Request $request)
    {
        try {
            // Set locale based on header
            $lang = $request->header('lang');
            \Illuminate\Support\Facades\App::setLocale($lang);

            // Handle tax card upload
            $tax_card = null;
            $tax_number = 'nullable';
            if ($request->tax_card != null) {
                $tax_card = $this->MoveImage($request->file('tax_card'), 'uploads/laundry/tax_card');
                $tax_number = 'required';
            }

            // Validate input data
            $validator = Validator::make($request->all(), [
                'name' => 'required|unique:laundries',
                'email' => 'required|unique:laundries',
                'country_code' => 'required',
                'password' => 'required|min:6|max:50|confirmed',
                'password_confirmation' => 'required|max:50|min:6',
                'branch_number' => 'required',
                'company_register' => 'required',
                'phone' => 'required',
                'tax_number' => $tax_number . '|unique:laundries',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'message' => $validator->messages()->first()
                ], 401);
            }

            // Normalize phone number by adding a leading zero if missing
            $phone_with_zero = '0' . ltrim($request->phone, '0');
            $phone_without_zero = ltrim($request->phone, '0');

            // Check if the phone number already exists with or without a leading zero
            $existingPhoneWithZero = Laundry::where('phone', $phone_with_zero)->exists();
            $existingPhoneWithoutZero = Laundry::where('phone', $phone_without_zero)->exists();

            if ($existingPhoneWithZero || $existingPhoneWithoutZero) {
                return response()->json([
                    'message' => $lang == 'ar' ? 'رقم الهاتف موجود بالفعل' : 'Phone number already exists'
                ], 400);
            }

            // Proceed with file uploads and database operations
            $company_register = $this->MoveImage($request->file('company_register'), 'uploads/laundry/company_register');
            $logo = null;

            if ($request->laundry_logo) {
                $logo = $this->MoveImage($request->file('laundry_logo'), 'uploads/laundry/logos');
            }

            // Retrieve delivery fees
            $one_way_delivery_fees = Deliveryfees::where('type', 'one way')->first()->price;
            $door_to_door_delivery_fees = Deliveryfees::where('type', 'door to door')->first()->price;

            // Create the laundry record with the normalized phone number
            $laundry = Laundry::create([
                'name' => $request->name,
                'phone' => $phone_with_zero, // Always store with leading zero
                'country_code' => $request->country_code,
                'email' => $request->email,
                'branch' => $request->branch_number,
                'companyregister' => $company_register,
                'taxcard' => $tax_card,
                'tax_number' => $request->tax_number,
                'logo' => $logo,
                'password' => Hash::make($request->password),
                'one_way_delivery_fees' => $one_way_delivery_fees,
                'door_to_door_delivery_fees' => $door_to_door_delivery_fees
            ]);

            // Create a wallet for the laundry
            laundry_wallet::create([
                'laundry_id' => $laundry->id
            ]);

            if (!$laundry) {
                if ($lang == 'ar') {
                    return $this->response(false, 'حدث خطأ ما', null, 500);
                }
                return response()->json(['some thing rong'], 500);
            }

            // Authenticate the user
            $credentials = ['email' => $laundry->email, 'password' => $request->password];
            if (!$token = auth()->guard('laundry_api')->attempt($credentials)) {
                if ($lang == 'ar') {
                    return $this->response(false, 'قد يكون اسم المستخدم أو كلمة المرور الخاصة بالفرع غير صحيحة، يرجى المحاوله مره اخري', null, 401);
                }
                return response()->json(['error' => 'Your Branch username or password maybe incorrect, please try again'], 401);
            }

            // Prepare response data
            $data = [];
            $data['status'] = true;
            $data['message'] = $lang == 'ar' ? 'تم التسجيل بنجاح' : 'Registered successfully';
            $data['data']['laundry_id'] = $laundry->id;
            $data['data']['token'] = $token;

            return response()->json($data);
        } catch (\Exception $ex) {
            $this->logResponse($ex);
            return $this->response(false, 'some thing wrong', null, 422);
        }
    }

    // شيكلي علي  الرقم والاميل  لو موجودين ارجعلي رساله انهم موجودين

    public function verifyRegistrationDataLaundry(Request $request)
    {
        $lang = $request->header('lang');
        \Illuminate\Support\Facades\App::setLocale($lang);
        $validator = Validator::make($request->all(), [
            'name'                  => 'required|unique:laundries',
            'email'                 => 'required|unique:laundries',
            'phone'                 => 'required|unique:laundries',
            'tax_number'            => 'nullable|unique:laundries',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 401);
        }

        return $this->response(true, 'data is valid', null, 200);
    }

    public function getpranchinfo(Request $request , $laundry_id = null)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);

        if ($laundry_id == null) {
            $laundry_id = Auth::guard('laundry_api')->user()->id;
        }

        $laundry = Laundry::select('branch', 'name')->find($laundry_id);
        $branches = branch::select('address', 'id', 'open_time', 'closed_time', 'closed_time', 'phone', 'username')->where('laundry_id', $laundry_id)->get()->makehidden('translations');
        $branchcount = $branches->count();
        foreach ($branches as $branche) {
            $branche->closed_time = date('h:m a', strtotime($branche->closed_time));
            $branche->open_time = date('h:m a', strtotime($branche->open_time));
        }
        $data = [
            'status' => true,
            'message' => $lang == 'ar' ? 'تم جلب جميع الفروع بنجاح' : 'All branches fetched successfully',
        ];
        $data['data']['name'] = $laundry->name;
        $data['data']['laundry branch status'] = $laundry->branch;
        $data['data']['branchcount'] = $branchcount;
        $data['data']['branches'] = $branches;
        //return Branchinfo::collection($branches)->additional($data);
        return response()->json($data);
    }

    public function subscription(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'fort_id' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => $validator->messages()->first()
            ], 401);
        }
        // try{
        DB::beginTransaction();


        if ($request->pymenttype_id == 1) {
            if ($request->merchant_reference != null) {
                $transactionfeedback = TransactionFeedback::where('merchant_reference', $request->merchant_reference)->first();
                if ($transactionfeedback == null)
                    return $this->response(false, 'payment transaction error');
            }
        }

        $laundry = auth('laundry_api')->user();
        $laundry->status = true;
        $laundry->save();
        $this->SubscriptionRepository->subscripe($request, Auth::guard('laundry_api')->user()->id);
        DB::commit();
        return $this->response(true, 'you subscripe successfuly');
        // }catch(\Exception $ex){
        //    DB::rollback();
        //    return $this->response(false,'some thing wrong',null,422);
        //  }


    }


    //checkPhone
    public function checkPhone(Request $request)
    {
        $lang = $request->header('lang');
        \Illuminate\Support\Facades\App::setLocale($lang);
        $laundry = Laundry::where('country_code', $request->country_code)->where('phone', $request->phone)->first();
        if ($laundry == null) {
            if ($lang == 'ar') {
                return $this->response(false, 'الرقم غير موجود', null, 422);
            }
            return $this->response(false, 'phone not found', null, 422);
        }
        if ($lang == 'ar') {
            return $this->response(true, 'الرقم موجود', null, 200);
        }
        return $this->response(true, 'phone found', null, 200);
    }

    public function sendotp(Request $request)
    {
        $lang = $request->header('lang');
        \Illuminate\Support\Facades\App::setLocale($lang);
//        $messageText=1234;

        $code = $this->randomOtp();

        if ($lang == 'ar') {
            $messageText = intval($code);
            $sendMessgeOtp = 'كود التحقق الخاص بك لتطبيق نظيفة هو: ' . $code;
        } else {
            $messageText = intval($code);
            $sendMessgeOtp = 'Your Nathefah verification code is:  ' . $code;
        }
        $country_code =$request->country_code;
        $phone        =$request->phone;
       return response()->json($this->sendSmsMessage($request, 'laundry', $sendMessgeOtp , $messageText , $validation = false));
    }



    //delete laundry
    public function deleteLaundry(Request $request)
    {
        $lang = $request->header('lang');
        \Illuminate\Support\Facades\App::setLocale($lang);

        $laundry = auth('laundry_api')->user();
        $laundry->deleted = 1;
        $laundry->save();
        if ($lang == 'ar') {
            return $this->response(true, 'تم حذف المغسله بنجاح');
        }
        return $this->response(true, 'laundry deleted successfully');
    }

}
