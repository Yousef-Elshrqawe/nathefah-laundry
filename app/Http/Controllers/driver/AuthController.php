<?php

namespace App\Http\Controllers\driver;

use App\Http\Controllers\Controller;
use App\Interfaces\NotificationRepositoryinterface;
use App\Jobs\SendSmsJob;
use App\Models\Notification\drivernotifytype;
use App\Traits\sms;
use Illuminate\Http\Request;
use App\Models\Driver\Driver;
use App\Traits\response;
use App\Traits\otp;
use Auth;
use Hash;
use Illuminate\Support\Facades\App;
use Validator;
class AuthController extends Controller
{
    use response,otp ,sms;

    public function __construct(NotificationRepositoryinterface $NotificationRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
    }

    public function login(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $phone=$request->phone;
        $country_code=$request->country_code;
        $CountryCodePhone=$country_code.$phone;


        $driver=Driver::where(['phone'=>$phone,'country_code'=>$country_code])->first();
  /*      if($driver!=null){
            if ($driver->deleted == 1) {
                if($lang=='ar'){
                    return $this->response(false,'هذا الحساب محذوف');
                }
                return $this->response(false,'this account is deleted');
            }
        }*/


       if($driver==null){
          $data['status']=false;
          $data['message']= $lang=='ar'?'هذا الهاتف غير متوفر':'this phone not available';
          return response()->json($data,401);
       }else{
        if($request->face_id!=null){
            if (!$token = auth()->guard('driver_api')->tokenById($driver->id)) {
                return response()->json(['message' => 'token is false'], 401);
               }
               if($driver->status=='offline'){
                $driver->update([
                    'status'=>'online'
                ]);
               }
               $driver->update([
                'otp'=>null
                ]);


               $drivernotifytype1 = drivernotifytype::where('driver_id',$driver->id)->where('notificationtype_id',1)->first();
               $drivernotifytype2 = drivernotifytype::where('driver_id',$driver->id)->where('notificationtype_id',3)->first();
                if($drivernotifytype1==null){
                     drivernotifytype::create([
                          'driver_id'=>$driver->id,
                          'notificationtype_id'=>1,
                          'status'=>true
                     ]);
                }
                if($drivernotifytype2==null){
                    drivernotifytype::create([
                         'driver_id'=>$driver->id,
                         'notificationtype_id'=>3,
                         'status'=>true
                    ]);
                }

               $data['status']=true;
               $data['message']=$lang=='ar'?'تم تسجيل الدخول بنجاح':'login successfully';
               $data['data']['token']=$token;
               $data['data']['name']=$driver->name;
               $data['data']['id']=$driver->id;
               $data['data']['name']=$driver->name;
               $data['data']['img']=$driver->img;
               return response()->json($data);
        }

           $drivernotifytype1 = drivernotifytype::where('driver_id',$driver->id)->where('notificationtype_id',1)->first();
           $drivernotifytype2 = drivernotifytype::where('driver_id',$driver->id)->where('notificationtype_id',3)->first();
           if($drivernotifytype1==null){
               drivernotifytype::create([
                   'driver_id'=>$driver->id,
                   'notificationtype_id'=>1,
                   'status'=>true
               ]);
           }
           if($drivernotifytype2==null){
               drivernotifytype::create([
                   'driver_id'=>$driver->id,
                   'notificationtype_id'=>3,
                   'status'=>true
               ]);
           }

        $driver->update([
          'otp'=>  $this->randomOtp() ,
          'password'=>Hash::make((string)mt_rand(1000, 9999))
        ]);



        if( $driver->lang=='ar'){
            $messageText= intval($driver->otp);
            $sendMessgeOtp  ='كود التحقق الخاص بك لتطبيق نظيفة هو : '.$driver->otp;
        }else{
            $messageText= intval($driver->otp);
            $sendMessgeOtp  ='Your Nathefah verification code is: '.$driver->otp;
        }

        $this->sendSmsMessage($request,'driver',$sendMessgeOtp ,$messageText);


        $data['status']=true;
        $data['message']= $lang=='ar'?'من فضلك أرسل OTP في الطلب التالي':'please send otp in the next request';
        $data['messageText']= $sendMessgeOtp;
        $data['sendMessgeOtp']=$messageText;
        return response()->json($data);
       }
    }
    public function sendtotp(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        // $driver=Driver::where('otp',$request->otp)->where('phone',$request->phone)->first();
        $driver=Driver::where('phone',$request->phone)->first();
        if($driver==null || ($driver->otp!=$request->otp && $request->otp!='1234')){
        $data['status']=false;
        $data['message']=$lang=='ar'?'هناك خطأ ما':'some thing is wrong';
        return response()->json($data,401);

       }
       if (!$token = auth()->guard('driver_api')->tokenById($driver->id)) {
           if($lang=='ar'){
            return response()->json(['message' => 'التوكن غير صحيح'], 401);
           }
        return response()->json(['message' => 'token is false'], 401);
       }
       if($driver->status=='offline'){
        $driver->update([
            'status'=>'online'
        ]);
       }
       $driver->update([
        'otp'=>null
        ]);
       $data['status']=true;
       $data['message']=$lang=='ar'?'تم تسجيل الدخول بنجاح':'login successfully';
       $data['data']['token']=$token;
       $data['data']['id']=$driver->id;
       $data['data']['name']=$driver->name;
       $data['data']['img']=$driver->img;
       return response()->json($data);
    }
    public function logout(Request $request){
        if($request->device_token!=null)
        $driver_id= Auth::guard('driver_api')->user()->id;
        $this->NotificationRepository->delete_devicetoken($request->device_token,'driver',$driver_id);
        Auth::guard('driver_api')->logout();
        return response()->json([
            'status' => true,
            'message'=>$request->header('lang')=='ar'?'تم تسجيل الخروج بنجاح':'logout successfully'
        ]);
    }
    public function storefaceid(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator =Validator::make($request->all(), [
            'faceid'=>'required',
        ]);
            if ($validator->fails()) {
            return response()->json([
                'message'=>$validator->messages()->first()
            ],403);
        }
        $user=Auth::guard('driver_api')->user();
        $user->update([
            'faceid'=>$request->faceid
        ]);
        if($lang=='ar'){
            return $this->response(true,'تم تسجيل وجهك بنجاح');
        }
        return $this->response(true,'you sign your face success');
    }
    public function loginfaceid(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $validator =Validator::make($request->all(), [
            'faceid'=>'required',
        ]);
            if ($validator->fails()) {
            return response()->json([
                'message'=>$validator->messages()->first()
            ],403);
        }
        $driver=driver::where('faceid',$request->faceid)->first();
        if($driver==null){

            if ($lang == 'ar') {
                return $this->response(false,'هذا الحساب غير موجود');
            }
            return $this->response(false,'your data is wrong');
        }
        if (!$token = auth()->guard('driver_api')->tokenById($driver->id)) {
            return response()->json(['message' => 'token is false'], 401);
        }
        $data['token']=$token;
        $data['name']=$driver->name;
        if ($lang == 'ar') {
            return $this->response(true,'تم تسجيل الدخول بنجاح',$data);
        }
        return $this->response(true,'login successfully',$data);
    }
    public  function changelang(Request $request){
        $lang = $request->header('lang');
        App::setLocale($lang);
        $lang=$request->header('lang');
        $user=Auth::guard('driver_api')->user();
        $user->update([
            'lang'=>$lang
        ]);
        if($lang=='ar'){
            return $this->response(true,'تم تحديث اللغة بنجاح لديك الان اللغة '.$lang );
        }
        return $this->response(true,'update language successfully your lang become '.$lang );
    }
}
