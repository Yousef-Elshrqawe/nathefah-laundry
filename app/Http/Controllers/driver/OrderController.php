<?php

namespace App\Http\Controllers\driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Driver\Driver;
use App\Models\Order\{order, CancelOrder, Orderreview};
use App\Models\Order\orderdetailes;
use Illuminate\Support\Facades\DB;
use App\Models\Order\OrderDriveryStatus;
use App\Models\Laundry\branch;
use Illuminate\Pagination\LengthAwarePaginator as Paginator;
use App\Interfaces\OrderRepositoryInterface;
use App\Traits\response;
use App\Traits\queries\serviceTrait;
use App\Models\Laundry\Branchuser;
use App\Interfaces\NotificationRepositoryinterface;
use App\Models\User;
use Carbon\Carbon;
use Validator;
use Auth;
use App;

class OrderController extends Controller
{
    //
    use serviceTrait, response, App\Traits\sms , App\Traits\otp;

    public function __construct(NotificationRepositoryinterface $NotificationRepository, OrderRepositoryInterface $OrderRepository)
    {
        $this->NotificationRepository = $NotificationRepository;
        $this->OrderRepository = $OrderRepository;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    } // end of distance

    public function getneworder(Request $request)
    {
        $lat = $request->lat;
        $long = $request->long;
        $driver_id = Auth::guard('driver_api')->user()->id;
        $orders = DB::table('orders')->where('orders.driver_id', $driver_id)->where('orders.delivery_status', '=', 'no_progress')
            ->select('orders.id', 'customer_name', 'customer_phone', 'customer_location', 'order_delivery_status.order_status', 'lat', 'long', 'orders.branch_id', 'orders.delivery_status')
            ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
            ->where('order_delivery_status.confirmation', false)
            //->where('order_delivery_status.driver_id',$driver_id)
            ->latest('order_delivery_status.id')
            ->groupBy('orders.id')->groupBy('orders.customer_name')->groupBy('orders.customer_phone')->groupBy('orders.customer_location')
            ->groupBy('orders.lat')->groupBy('orders.long')
            ->groupBy('order_delivery_status.order_status')->groupBy('orders.delivery_status') //
            ->groupBy('order_delivery_status.id')
            ->groupBy('orders.branch_id')
            ->get();
        foreach ($orders as $order) {
            if ($order->order_status == 'pick_up_home') {
                $order->distance = $this->distance($order->lat, $order->long, $request->lat, $request->long, 'k');
            }
            if ($order->order_status == 'pick_up_laundry') {
                $branch = branch::select('lat', 'long')->find($order->branch_id);
                $order->distance = $this->distance($branch->lat, $branch->long, $request->lat, $request->long, 'k');
            }
        }
        /*   $data['status']=true;
           $data['message']="get new orders suceesfully";*/
        // $data['data']['orders']=$orders;
        // return response()->json($data);
        $data['data']['orders'] = $orders;
        return $this->response(true, "get new orders suceesfully", $data, 200);
    }

    public function Acceptorder(Request $request)
    {
        $order_id = $request->order_id;
        $driver = Auth::guard('driver_api')->user();
        $driver_id = $driver->id;
        $driver_name = $driver->name;
        $order = Order::where('driver_id', $driver_id)->where('id', $order_id)->first();

        OrderDriveryStatus::where('order_id', $order_id)->first()?->update(['driver_id' => $driver_id]);

        if ($order) {
            $order->update(['delivery_status' => 'inprogress']);

            // إرسال الإشعار للفرع
            $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
            $notf_s = DB::table('branchnotifytype')
                ->where('user_branch_id', $Branchusers->first()->id ?? null)
                ->where('notificationtype_id', '3')
                ->where('status', '1')
                ->first();

            if ($notf_s) {
                foreach ($Branchusers as $Branchuser) {
                    $title = 'حالة الطلب'; // قيمة افتراضية
                    $body = 'لقد وافق السائق على الطلب رقم ' . $order_id . ' الخاص بك';

                    if ($Branchuser->lang == 'en') {
                        $title = 'Order Status';
                        $body = 'Driver accepted order ' . $order_id . ' for you';
                    }

                    $phoneNumber = $Branchuser->country_code . $Branchuser->phone;
                    \App\Jobs\SendSmsJob::dispatch($phoneNumber, $body);

                    $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 3, 0, ['driver_name' => $driver_name]);
                }

                if ($Branchusers->isNotEmpty()) {
                    $this->NotificationRepository->createnotification('branch', $Branchusers->first()->branch_id,
                        ['ar' => 'حالة الطلب', 'en' => 'Order Status'],
                        ['ar' => 'لقد وافق السائق على الطلب رقم ' . $order_id . ' الخاص بك', 'en' => 'Driver accepted order ' . $order_id . ' for you']
                    );
                }
            }

            // إرسال الإشعار للمستخدم
            $user = User::find($order->user_id);
            $title = 'السائق متجه إليك'; // قيمة افتراضية
            $body = 'السائق في طريقه إليك الآن';

            if ($user->lang == 'en') {
                $title = 'Driver is on his way';
                $body = 'Driver is on his way to your location';
            }

            $phoneNumber = $user->country_code . $user->phone;
            \App\Jobs\SendSmsJob::dispatch($phoneNumber, $body);

            $usernotifytype = DB::table('usernotifytype')
                ->where('user_id', $order->user_id)
                ->where('notificationtype_id', '3')
                ->where('status', '1')
                ->first();

            if ($usernotifytype) {
                $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 0, ['order_id' => $order_id]);
                $this->NotificationRepository->createnotification('user', $order->user_id,
                    ['ar' => 'السائق متجه إليك', 'en' => 'Driver is on his way'],
                    ['ar' => 'السائق في طريقه إليك الآن', 'en' => 'Driver is on his way to your location']
                );
            }
        } else {
            return response()->json(['status' => false, 'message' => 'This order not found']);
        }

        return response()->json(['status' => true, 'message' => 'Confirm order successfully']);
    }


    public function rejectorder(Request $request)
    {
        $order_id = $request->order_id;
        $driver = Auth::guard('driver_api')->user();
        $driver_id = $driver->id;
        $driver_name = $driver->name;
        DB::beginTransaction();
        $order = order::where('driver_id', $driver_id)->where('id', $order_id)->first();
        $order->update([
            'driver_id' => null
        ]);
        OrderDriveryStatus::where('order_id', $order_id)->first()->update(['driver_id' => null]);
        CancelOrder::create([
            'order_id' => $order->id,
            'branch_id' => $order->branch_id,
            'driver_id' => $driver_id
        ]);
        $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
        foreach ($Branchusers as $Branchuser) {
            $country_code = $Branchuser->country_code;
            $phone = $Branchuser->phone;
            $poneNumber = $country_code . $phone;


            $userLang = $Branchuser->lang ?? app()->getLocale();

            if ($userLang == 'ar') {
                $title = 'حاله الطلب';
                $body = 'لقد رفض السائق الطلب رقم ' . $order_id;
            } elseif ($userLang == 'en') {
                $title = 'Order Status';
                $body = 'Driver has rejected the order ' . $order_id;
            } else {
                $title = __('Notification');
                $body = __('Order has been updated: ') . $order_id;
            }

            \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);
            $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 3, 1, ['driver_name' => $driver_name]);
        }

        $titles = ['ar' => 'حاله الطلب', 'en' => 'order status'];
        $bodys = ['ar' => 'لقد رفص السائق  الطلب رقم ' . $order_id, 'en' => 'Driver has rejected the order ' . $order_id];
        $this->NotificationRepository->createnotification('branch', $Branchuser->branch_id, $titles, $bodys);
        DB::commit();
        //$this->NotificationRepository->
        $data['status'] = true;
        $data['message'] = "reject order suceesfully";
        return response()->json($data);
    }

    //
    public function orderinfo(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order_id = $request->order_id;
        if (isset($request->code) != null) {
            $OrderDriveryStatus = OrderDriveryStatus::where(['code' => $request->code, 'confirmation' => false])->latest()->first();
            if ($OrderDriveryStatus == null) {
                return $this->response(false, 'some thing is wrong');
            }
            $order_id = $OrderDriveryStatus->order_id;
        }
        $driver_id = Auth::guard('driver_api')->user()->id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order = DB::table('order_detailes')->where('order_detailes.order_id', $order_id)
            ->join('orders', 'orders.id', '=', 'order_detailes.order_id')
            ->select('orders.id', 'orders.price_before_discount', 'orders.price_after_discount','orders.payed', 'orders.delivery_fees', 'orders.discounttype')
            ->groupBy('orders.price_before_discount')
            ->groupBy('orders.price_after_discount')
            ->groupBy('orders.payed')
            ->groupBy('orders.delivery_fees')
            ->groupBy('orders.id')
            ->first();
        if ($lang == 'ar') {
            if ($order == null) {
                return response()->json(['status' => false, 'message' => 'لم يتم العثور على هذا الطلب'], 401);
            }
        } else {
            if ($order == null) {
                return response()->json(['status' => false, 'message' => 'this order not found'], 401);
            }
        }


        // $orderargentprice=DB::table('orders')->where('orders.id',$order_id)
        // ->leftjoin('argent','orders.id','=','argent.order_id')
        // ->selectRaw('sum(argent.price) as argentprice')
        // ->groupBy('argent.order_id')
        // ->first();
        $order->price = $this->OrderRepository->orderprice($order);
        // this query get services with count of item in it
        $services = $this->serive($order_id, $driver_id, $lang);
        // this query get items with count of item in it
        $items = $this->items($order_id, $driver_id, $lang);
        // this query to get additional service
        $additionals = $this->additionals($order_id, $driver_id, $lang);
        $argents = db::table('argent')->where('order_id', $order_id)->get();
        // but additional service in the item
        foreach ($items as $key => $item) {
            $item->additonalservice = [];
            foreach ($additionals as $additional) {
                if ($item->item_id == $additional->item_id && $item->service_id == $additional->service_id) {
                    array_push($item->additonalservice, $additional);
                }
            }
            //but argent inside item
            foreach ($argents as $argent) {
                $item->argent = 0;
                if ($item->item_id == $argent->branchitem_id) {
                    $item->argent = $argent->quantity;
                }
            }
        }
        //but argent inside item
        foreach ($services as $key => $service) {
            $service->item = [];
            foreach ($items as $item) {
                if ($item->service_id == $service->service_id) {
                    array_push($service->item, $item);
                }
            }
        }
        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data']['order'] = $order;
        $data['data']['serives'] = $services;
        return response()->json($data);
    }

    public function inprogressorder(Request $request)
    {
        $order_id = $request->order_id;
        $driver_id = Auth::guard('driver_api')->user()->id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        // orders with items
        $orders = DB::table('orders')
            ->select('orders.id', 'branchitemtranslations.name',)
            ->selectRaw('sum(order_detailes.quantity) as quantity')
            ->where('orders.driver_id', $driver_id)
            ->where('delivery_status', 'inprogress')
            //->where('order_delivery_status.confirmation',false)
            //->join('order_delivery_status','order_delivery_status.order_id','=','orders.id')
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->join('branchitemtranslations', 'branchitemtranslations.branchitem_id', '=', 'order_detailes.branchitem_id')
            ->groupBy('orders.id')->groupBy('orders.customer_name')
            //->groupBy('orders.customer_phone')->groupBy('orders.customer_location')
            ->groupBy('order_detailes.quantity') //
            ->groupBy('order_detailes.order_id')
            //->groupBy('orders.branch_id')
            ->groupBy('branchitemtranslations.name')
            //->groupBy('order_delivery_status.id')
            ->where('branchitemtranslations.locale', $lang)
            ->where('order_detailes.additionalservice_id', null)
            ->get();
        // push item in array
        $items = [];
        foreach ($orders as $key => $order) {
            $items[$key]['order_id'] = $order->id;
            $items[$key]['name'] = $order->name;
            $items[$key]['quantity'] = $order->quantity;
        }
        // orders with out item
        $orders = DB::table('orders')
            ->select('orders.id', 'orders.customer_name', 'orders.customer_phone', 'orders.country_code', 'orders.customer_location', 'order_delivery_status.order_status', 'order_delivery_status.navigation', 'orders.lat', 'orders.long', 'orders.branch_id', 'orders.pymenttype_id', 'order_status', 'orders.branch_id', 'orders.payed'
                , 'orders.discounttype', 'orders.price_before_discount', 'orders.price_after_discount', 'delivery_fees', 'orders.created_at'
            )
            ->selectRaw('sum(order_detailes.quantity) as quantity')
            //->selectRaw('sum(order_detailes.price) as price')
            ->where('orders.driver_id', $driver_id)
            ->where('delivery_status', 'inprogress')
            ->leftjoin('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
            ->where('order_delivery_status.driver_id', $driver_id)->latest('order_delivery_status.id')
            ->where('order_delivery_status.confirmation', false)
            ->where('order_detailes.additionalservice_id', null)
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->groupBy('orders.id')->groupBy('orders.customer_name')->groupBy('orders.customer_phone')->groupBy('orders.customer_location')
            ->groupBy('orders.lat')->groupBy('orders.long')
            ->groupBy('order_delivery_status.order_status')
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.order_id')
            ->groupBy('orders.payed')
            ->groupBy('orders.branch_id')
            ->groupBy('orders.country_code')
            ->groupBy('orders.pymenttype_id')
            ->groupBy('order_delivery_status.id')
            ->groupBy('orders.discounttype')
            ->groupBy('orders.price_before_discount')
            ->groupBy('orders.price_after_discount')
            ->groupBy('orders.delivery_fees')
            ->groupBy('order_delivery_status.navigation')
            ->groupBy('orders.created_at')
            ->orderBy('orders.created_at', 'desc')
            ->get();
        // $argentprices=DB::table('orders')
        // ->join('argent','orders.id','=','argent.order_id')
        // ->select('orders.id')
        // ->selectRaw('sum(price) as argentprice')
        // ->groupBy('orders.id')
        // ->get();
        foreach ($orders as $key => $order) {
            $order->price = $this->OrderRepository->orderprice($order);
            if ($order->order_status == 'pick_up_home') {
                $order->distance = $this->distance($order->lat, $order->long, $request->lat, $request->long, 'k');
            }
            if ($order->order_status == 'pick_up_laundry') {
                $branch = branch::select('lat', 'long')->find($order->branch_id);
                $order->distance = $this->distance($branch->lat, $branch->long, $request->lat, $request->long, 'k');
            }
            $key = 0;
            $order->item = [];
            foreach ($items as $item) {
                if ($item['order_id'] == $order->id) {
                    $order->item[$key]['quantity'] = $item['quantity'];
                    $order->item[$key]['name'] = $item['name'];
                    $key++;
                }
            }
            //    foreach($argentprices as $argent){
            //       if($argent->id==$order->id){
            //         $order->price+=$argent->argentprice;
            //       }
            //    }
            if ($order->payed == 1) {
                $order->payed = true;
            } else {
                $order->payed = false;
            }
            if ($order->order_status == 'pick_up_home' || $order->order_status == 'drop_of_home') {
                $order->distinationlat = $order->lat;
                $order->distinationlong = $order->long;
            }//
            if ($order->order_status == 'pick_up_laundry' || $order->order_status == 'drop_of_laundry') {
                $branch = branch::find($order->branch_id);
                $order->distinationlat = floatval($branch->lat);
                $order->distinationlong = floatval($branch->long);
            }//
        }
        $data['status'] = true;
        $data['message'] = "get in progress orders suceesfully";
        $data['data']['orders'] = $orders;
        return response()->json($data);
    }

    // in this function all cycle of delivery order we use only [pick_up_laundry/pick_up_home]
    public function confirmorder(Request $request)
    { //confirm order
        $order_id = $request->order_id;
        $confirm_type = $request->confirm_type;
        $driver = Auth::guard('driver_api')->user();
        $driver_id = $driver->id;
        $driver_name = $driver->name;
        $orderstatus = OrderDriveryStatus::where('order_id', $order_id)->latest('id')->first();
        $order = order::where('driver_id', $driver_id)->where('id', $order_id)->first();
        if ($order == null) {
            $data['status'] = false;
            $data['message'] = 'this order not avilable';
            return response()->json($data);
        }
        // pick_up_home--->drop_of_laundry--->pick_up_laundry--->drop_of_home
        //  pick_up_laundy status
        if ($confirm_type == 'pick_up_laundry') {
            if ($orderstatus->order_status == 'pick_up_laundry') {
                $orderstatus->update([
                    'confirmation' => true,
                ]);
                OrderDriveryStatus::create([
                    'order_id' => $order_id,
                    'driver_id' => $driver_id,
                    'order_status' => 'drop_of_home',
                    'code' => rand(999, 9999)
                ]);
                $order->update([
                    'delivery_status' => 'inprogress',
                    'progress' => 'indelivery'
                ]);
                $user = User::find($order->user_id);
                $title = 'استلام الطلب ';
                $body = 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username;
                if ($user->lang == 'en') {
                    $title = 'order  recived';
                    $body = 'your order ' . '(' . $order->id . ')' . ' is received to laundry ' . $order->branch->username;
                }

                $country_code = $user->country_code;
                $phone = $user->phone;
                $phonen = $country_code . $phone;

                $this->sendMessage($user->country_code . $user->phone, app()->getLocale() == 'ar' ? 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username : 'Your order ' . '(' . $order->id . ')' . ' has been sent to the laundry ' . $order->branch->username);
//                $this->sendSms($phonen, $body);
                \App\Jobs\SendSmsJob::dispatch($phonen, $body);


                $titles = ['ar' => 'استلام الطلب', 'en' => 'order received'];
                $bodys = ['ar' => 'تم ارسال طلبك ' . '(' . $order->id . ')' . ' الي مغسلة ' . $order->branch->username, 'en' => 'your order ' . '(' . $order->id . ')' . ' is received to laundry ' . $order->branch->username];
                $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '1')->where('status', '1')->first();
                if ($usernotifytype) {
                    $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 2, ['order_id' => $order_id]);
                    $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
                }

                // send notification to laundry to remove qr code from page of laundry application and go to home
                $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
                foreach ($Branchusers as $Branchuser) {
                    $country_code = $Branchuser->country_code;
                    $phone = $Branchuser->phone;
                    $poneNumber = $country_code . $phone;
                    if ($Branchuser->lang == 'ar') {
                        $title = 'استلام طلب';
                        $body = 'استلم السائق الطلب رقم ' . $order_id;

//                        $this->sendSms($poneNumber,$body);
                        \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);

                    }
                    $notf_s = DB::table('branchnotifytype')->where('user_branch_id', $Branchusers->first()->id)
                        ->where('notificationtype_id', '3')
                        ->where('status', '1')->first();
                    if ($notf_s) {
                        if ($Branchuser->lang == 'en') {
                            $title = 'Receive Order';
                            $body = 'Driver receive order ' . $order_id;
                            $this->sendMessage($user->country_code . $user->phone, __('auth.The driver received your order from the laundry'));
//                        $this->sendSms($poneNumber,$body);
                            \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);

                        }
                        $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 3, 5, ['order_id' => $order_id]);
                    }
                }


            }
            $data['status'] = true;
            $data['message'] = 'confirm pick up from laundry success';
        }
        // drop of home status  not use in busniss this for test only
        if ($confirm_type == 'drop_of_home') {
            if ($orderstatus->order_status == 'drop_of_home') {
                $orderstatus->update([
                    'confirmation' => true
                ]);
                $order = order::find($order_id)->update([
                    'progress' => 'completed',
                    'delivery_status' => 'completed',
                    'payed' => true
                ]);
                // start notification
                $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
                foreach ($Branchusers as $Branchuser) {
                    $country_code = $Branchuser->country_code;
                    $phone = $Branchuser->phone;
                    $poneNumber = $country_code . $phone;
                    if ($Branchuser->lang == 'ar') {
                        $title = 'حاله الطلب';
                        $body = 'لقد وصل السائق  الطلب';

//                        $this->sendSms($poneNumber,$body);
                        \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);

                    }
                    if ($Branchuser->lang == 'en') {
                        $title = 'order status';
                        $body = 'driver arrived order';

//                        $this->sendSms($poneNumber,$body);
                        \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);

                    }
                    $titles = ['ar' => 'حاله الطلب', 'en' => 'order status'];
                    $bodys = ['ar' => 'لقد وصل السائق الطلب رقم ' . $order_id, 'en' => 'driver arrived order ' . $order_id];

                    $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 3, 2, ['driver_name' => $driver_name]);
                    $this->NotificationRepository->createnotification('branch', $Branchuser->branch_id, $titles, $bodys);
                }
            }
            $data['status'] = true;
            $data['message'] = 'drop of the order to home successfuly';
        }
        //pick_up_home status
        if ($confirm_type == 'pick_up_home') {

            if ($orderstatus->order_status == 'pick_up_home') {
                $orderstatus->update([
                    'confirmation' => true
                ]);
                OrderDriveryStatus::create([
                    'order_id' => $order_id,
                    'driver_id' => $driver_id,
                    'order_status' => 'drop_of_laundry',
                    'code' => rand(999, 9999)
                ]);

                $order->update([
                    'delivery_status' => 'inprogress',
                    'progress' => 'indelivery',
                    'picked_date' => now()->format('Y-m-d H:i:s'),
                    'start_tracking' => false
                ]);

                $title = 'حالة الطلب';
                $body = '';

                $Branchusers = Branchuser::where('branch_id', $order->branch_id)->get();
                foreach ($Branchusers as $Branchuser) {
                    $country_code = $Branchuser->country_code;
                    $phone = $Branchuser->phone;
                    $poneNumber = $country_code . $phone;
                    if ($Branchuser->lang == 'ar') {
                        $title = 'حاله الطلب';
                        $body = 'لقد استلم السائق  الطلب';

//                        $this->sendSms($poneNumber, $body);
                        \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);

                    }
                    if ($Branchuser->lang == 'en') {
                        $title = 'Order Status';
                        $body = 'Driver has accepted the order';

//                        $this->sendSms($poneNumber, $body);
                        \App\Jobs\SendSmsJob::dispatch($poneNumber, $body);

                    }
                    $titles = ['ar' => 'حاله الطلب', 'en' => 'Order Status'];
                    $bodys = ['ar' => ' لقد استلم السائق الطلب رقم ' . $order_id, 'en' => 'Driver has accepted the order ' . $order_id];
                    $this->NotificationRepository->sendnotification('branch', $Branchuser->id, $title, $body, 3, 3, ['driver_name' => $driver_name]);
                    $this->NotificationRepository->createnotification('branch', $Branchuser->branch_id, $titles, $bodys);
                }
                // send notification to user to remove qr code from page of user application and go to home
//                $this->NotificationRepository->sendnotification('user', $order->user_id, 'Recive Order', 'Recive order', 3, 5, ['order_id' => $order_id]);
                $user = User::find($order->user_id);
                if ($user->lang == 'ar') {
                    $title = 'استلام الطلب';
                    $body = 'تم استلام طلبك  رقم ' . $order_id . ' من المنزل';
                }
                if ($user->lang == 'en') {
                    $title = 'Order received';
                    $body = 'your order ' . $order_id . ' is received from home';
                }
                $usernotifytype = DB::table('usernotifytype')->where('user_id', $order->user_id)->where('notificationtype_id', '3')->where('status', '1')->first();
                if ($usernotifytype) {
                    $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 5, ['order_id' => $order_id]);
                }
            }
            $data['status'] = true;
            $data['message'] = 'pick up order from home successfully';
        }
        // drop of laundry status  not use in busniss this for test only
        if ($confirm_type == 'drop_of_laundry') {
            if ($orderstatus->order_status == 'drop_of_laundry') {
                $orderstatus->update([
                    'confirmation' => true
                ]);
                OrderDriveryStatus::create([
                    'order_id' => $order_id,
                    'driver_id' => $driver_id,
                    'order_status' => 'pick_up_laundry'
                ]);
                $order->update([
                    'delivery_status' => 'inprogress',
                    'progress' => 'inprogress',
                ]);
            }
            $data['status'] = true;
            $data['message'] = 'drop of the order to home successfuly';
        }
        return response()->json($data);
    }

    public function getcode(Request $request)
    {
        $order_id = $request->order_id;
        $status = $request->status;
        $code = $this->OrderRepository->getcode($status, $order_id);
        if ($code == false)
            return $this->response(false, 'you have no accsess for this order');
        $data['order_code'] = $code;
        return $this->response(true, 'get order code succsessfuly', $data);
    }

    public function latestorder(Request $request)
    {
        $driver_id = Auth::guard('driver_api')->user()->id;
        $latestorders = DB::table('orders')
            ->select('orders.id', 'orders.customer_name', 'orders.customer_phone', 'orders.customer_location', 'delivery_status', 'orders.created_at', 'orders.updated_at')
            ->selectRaw('sum(order_detailes.quantity) as quantity')
            ->where('orders.driver_id', $driver_id)
            ->where('orders.delivery_status', '!=', null)
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->groupBy('orders.id')->groupBy('orders.customer_name')->groupBy('orders.customer_phone')->groupBy('orders.customer_location')
            ->groupBy('orders.delivery_status')->groupBy('orders.created_at')
            ->groupBy('order_detailes.order_id')
            ->latest()->take(25)->get();
        foreach ($latestorders as $latestorder) {
            $latestorder->created_at = date('Y-m-d', strtotime($latestorder->created_at));
            $latestorder->time = Carbon::parse( $latestorder->updated_at)->format('h:i A');

        }
        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data']['orders'] = $latestorders;
        // $data['data']['completedorder']=$completedorder;
        return response()->json($data);
    }

    public function latestorderinfo(Request $request)
    {
        $order_id = $request->order_id;
        if (isset($request->code) != null) {
            $OrderDriveryStatus = OrderDriveryStatus::where(['code' => $request->code, 'confirmation' => false])->latest()->first();
            if ($OrderDriveryStatus == null) {
                return $this->response(false, 'some thing is wrong');
            }
            $order_id = $OrderDriveryStatus->order_id;
        }
        $driver_id = Auth::guard('driver_api')->user()->id;
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order = DB::table('orders')
            ->select('orders.delivery_status',
                'orders.customer_location',
                'branch_id',
                'pymenttype_id',
                'user_id',
                'progress',
                'delivery_fees',
                'orders.id',
                'orders.payed',
                'orders.updated_at',
                'orders.discounttype',
                'orders.price_before_discount',
                'orders.price_after_discount',
                'order_delivery_status.navigation')
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->join('order_delivery_status', 'order_delivery_status.order_id', '=', 'orders.id')
            //->where('order_delivery_status.driver_id',$driver_id)
            ->latest('order_delivery_status.id')
            //->where('order_delivery_status.confirmation',false)
            ->where('orders.id', $order_id)
            ->where('order_detailes.order_id', $order_id)
            ->selectRaw('orders.created_at')
            ->selectRaw('order_delivery_status.order_status')
            ->selectRaw('sum(order_detailes.price) as price')
            ->groupBy('orders.id')
            ->groupBy('orders.branch_id')
            ->groupBy('orders.user_id')
            ->groupBy('orders.pymenttype_id')
            ->groupBy('orders.delivery_fees')
            ->groupBy('orders.progress')
            ->groupBy('orders.payed')
            ->groupBy('orders.customer_location')
            ->groupBy('orders.created_at')
            ->groupBy('orders.updated_at')
            ->groupBy('orders.delivery_status')
            ->groupBy('order_delivery_status.order_id')
            ->groupBy('order_delivery_status.id')
            ->groupBy('order_delivery_status.order_status')
            ->groupBy('order_delivery_status.navigation')
            ->groupBy('orders.discounttype')
            ->groupBy('orders.price_before_discount')
            ->groupBy('orders.price_after_discount')
            ->orderBy('order_delivery_status.created_at', 'desc')
            ->first();
        if ($order == null)
            return $this->response(false, 'you have no access to this order', null, 401);
        if ($order->payed == 1) {
            $order->payed = true;
        } else {
            $order->payed = false;
        }
        if ($order->order_status == 'pick_up_home') {
            $branch = branch::find($order->branch_id);
            $order->distinationlat = $branch->lat;
            $order->distinationlong = $branch->long;
        }//
        if ($order->order_status == 'pick_up_laundry') {
            $User = User::find($order->user_id);
            $order->distinationlat = $User->lat;
            $order->distinationlong = $User->long;
        }
        // if($order->order_status=='drop_of_home'){
        //     $order->payed=true;
        // }
        if ($order->progress != 'completed') {
            $order->rate = null;
        } else {
            $rate = Orderreview::where('order_id', $order_id)->first();
            if ($rate != null)
                $order->rate = $rate->rate;
        }
        $order->delivery_fees =  $order->delivery_fees;
        $order->created_at = date('Y-m-d', strtotime($order->created_at));
//        $order->time = date('h:m a', strtotime($order->created_at));
        // الساعه تكون  ساعه  سعوديه
        $order->time = Carbon::parse( $order->updated_at)->format('h:i A');
        // $orderargentprice=DB::table('orders')->where('orders.id',$order_id)
        // ->leftjoin('argent','orders.id','=','argent.order_id')
        // ->selectRaw('sum(argent.price) as argentprice')
        //  ->groupBy('argent.order_id')
        //  ->first();
        $order->price = $this->OrderRepository->orderprice($order);
        // this query get services with count of item in it
        $services = $this->serive($order_id, $driver_id, $lang);
        // this query get items with count of item in it
        $items = $this->items($order_id, $driver_id, $lang);
        // this query to get additional service
        $additionals = $this->additionals($order_id, $driver_id, $lang);
        $argents = db::table('argent')->where('order_id', $order_id)->get();
        // but additional service in the item
        foreach ($items as $key => $item) {
            $item->additonalservice = [];
            foreach ($additionals as $additional) {
                if ($item->item_id == $additional->item_id && $item->service_id == $additional->service_id) {
                    array_push($item->additonalservice, $additional);
                }
            }
            //but argent inside item
            foreach ($argents as $argent) {
                $item->argent = 0;
                if ($item->item_id == $argent->branchitem_id) {
                    $item->argent = $argent->quantity;
                }
            }
        }
        //but argent inside item
        foreach ($services as $key => $service) {
            $service->item = [];
            foreach ($items as $item) {
                if ($item->service_id == $service->service_id) {
                    array_push($service->item, $item);
                }
            }
        }
        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data']['order'] = $order;
        $data['data']['serives'] = $services;
        return response()->json($data);
    }


    public function allorder(Request $request)
    {
        $driver_id = Auth::guard('driver_api')->user()->id;
        $allorders = DB::table('orders')
            ->select('orders.id', 'orders.customer_name', 'orders.customer_phone', 'orders.customer_location', 'delivery_status', 'orders.created_at', 'orders.updated_at')
            ->selectRaw('sum(order_detailes.quantity) as quantity')
            ->where('orders.driver_id', $driver_id)
            ->where('orders.delivery_status', '!=', null)
            ->join('order_detailes', 'order_detailes.order_id', '=', 'orders.id')
            ->groupBy('orders.id')->groupBy('orders.customer_name')->groupBy('orders.customer_phone')->groupBy('orders.customer_location')
            ->groupBy('orders.delivery_status')->groupBy('orders.created_at')
            ->groupBy('order_detailes.order_id')
            ->paginate(3);
        foreach ($allorders as $allorder) {
            $allorder->created_at = date('Y-m-d', strtotime($allorder->created_at));
            $allorder->time = Carbon::parse( $allorder->updated_at)->format('h:i A');
        }
        $data['status'] = true;
        $data['message'] = "get new orders suceesfully";
        $data['data'] = $allorders;
        $allorders->status = true;
        $allorders->message = 'fvdv';
        return response()->json($allorders);
    }

    public function checkdropof(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $order_id = $request->order_id;
        $confirm_type = $request->confirm_type;
        $orderstatus = OrderDriveryStatus::where('order_status', $confirm_type)
            ->where('confirmation', true)->where('order_id', $order_id)->first();
        if ($orderstatus != null)
            return $this->response(true, 'order ' . $confirm_type . ' successfuly');
        return $this->response(false, $lang == 'ar' ? 'لم يتم تاكيد الطلب' : 'order not drop of', null, 405);
    }


    public function alertuser(Request $request)
    {
        $lang = $request->header('lang');
        App::setLocale($lang);
        $driver_id = Auth::guard('driver_api')->user()->id;

        // التحقق من تتبع السائق لطلب واحد فقط
        $order = Order::where(['driver_id' => $driver_id, 'delivery_status' => 'inprogress', 'start_tracking' => true])->first();

        if (!$order) {
            $order_id = $request->order_id;
            $order = Order::find($order_id);
            if (!$order) {
                return $this->response(false, 'Order not found', null, 404);
            }

            $order->start_tracking = true;
            $order->save();

            $user = User::find($order->user_id);
            $order_status = OrderDriveryStatus::where('order_id', $order_id)->latest()->first();

            if ($order_status) {
                $order_status->update(['navigation' => true]);
            }

            $title = 'حالة الطلب'; // قيمة افتراضية
            $body = 'السائق في الطريق لموقعك';

            if ($user->lang == 'en') {
                $title = 'Order Status';
                $body = 'Driver has accepted the order';
            }

            $phonen = $user->country_code . $user->phone;
            \App\Jobs\SendSmsJob::dispatch($phonen, $body);

            if ($request->confirm_type == 'drop_of_home' || $request->confirm_type == 'pick_up_home') {
                $titles = ['ar' => 'السائق في الطريق', 'en' => 'Driver is on his way'];
                $bodys = ['ar' => 'السائق في الطريق لموقعك', 'en' => 'Driver is on his way to your location'];

                $usernotifytype = DB::table('usernotifytype')
                    ->where('user_id', $order->user_id)
                    ->where('notificationtype_id', '3')
                    ->where('status', '1')
                    ->first();

                if ($usernotifytype) {
                    $this->NotificationRepository->sendnotification('user', $order->user_id, $title, $body, 3, 2, ['order_id' => $order_id]);
                    $this->NotificationRepository->createnotification('user', $order->user_id, $titles, $bodys);
                }
            }

            return $this->response(true, 'User has been notified');
        } else {
            return $this->response(false, $lang == 'ar' ? 'يجب إنهاء الطلب الحالي أولًا' : 'You already have an assigned order: ' . $order->id, null, 406);
        }
    }

}
