<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Laundry\branch;
use App\Traits\response;
use Illuminate\Http\Request;
use Auth;
use Validator;
use Carbon\Carbon;


class BranchScheduleController extends Controller
{
    use response;


    public function __construct()
    {
        $lang = request()->header('lang');
        if ($lang) {
            app()->setLocale($lang);
        }
    }

    /**
     * عرض مواعيد الفرع
     */
    public function index()
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);

        if (!$branch) {
            return $this->response(false, __('response.Branch not found'), null, 404);
        }

        $schedules = $branch->schedules()->get();
        return $this->response(true, __('response.Schedules retrieved successfully'), $schedules);
    }


    public function store(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);

        if (!$branch) {
            return $this->response(false,  __('response.Branch not found'), null, 404);
        }

        // التحقق من صحة البيانات المدخلة
        $validator = Validator::make($request->all(), [
            'day_id' => 'required|exists:days,id',
            'start_time' => 'required|date_format:h:i A', // دعم AM/PM
            'end_time' => 'required|date_format:h:i A|after:start_time',
        ], [
            'day_id.required'     => __('validation.The day field is required.'),
            'day_id.exists'       => __('validation.The selected day does not exist.'),
            'start_time.required' => __('validation.Start time is required.'),
            'start_time.date_format' => __('validation.Start time format is invalid. Please use a format like 02:30 PM.'),
            'end_time.required'   => __('validation.End time is required.'),
            'end_time.date_format'=> __('validation.End time format is incorrect. It should look like 03:30 PM.'),
            'end_time.after'      => __('validation.The time of completion should be after the start time.'),
        ]);
        //massage
        $validator->setAttributeNames([
            'day_id' => __('validation.attributes.day_id'),
            'start_time' => __('validation.attributes.start_time'),
            'end_time' => __('validation.attributes.end_time'),
        ]);

        if ($validator->fails()) {
            return $this->response(false, $validator->messages()->first(), null, 400);
        }

        // تحويل الأوقات من AM/PM إلى 24 ساعة
        $start_time_24 = Carbon::createFromFormat('h:i A', $request->start_time)->format('H:i');
        $end_time_24 = Carbon::createFromFormat('h:i A', $request->end_time)->format('H:i');

        // التحقق من تعارض الوقت
        $existingSchedule = $branch->schedules()
            ->where('day_id', $request->day_id)
            ->where(function ($query) use ($start_time_24, $end_time_24) {
                $query->whereBetween('start_time', [$start_time_24, $end_time_24])
                    ->orWhereBetween('end_time', [$start_time_24, $end_time_24])
                    ->orWhere(function ($query) use ($start_time_24, $end_time_24) {
                        $query->where('start_time', '<=', $start_time_24)
                            ->where('end_time', '>=', $end_time_24);
                    });
            })
            ->exists();

        if ($existingSchedule) {
            return $this->response(false,  __('response.This time slot is already taken for this day.'), null, 400);
        }

        // إضافة الموعد
        $branch->schedules()->create([
            'day_id' => $request->day_id,
            'start_time' => $start_time_24,
            'end_time' => $end_time_24,
        ]);

        return $this->response(true,  __('response.Schedule added successfully'));
    }

    /**
     * تحديث موعد موجود
     */
    public function update(Request $request, $id)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);

        if (!$branch) {
            return $this->response(false, __('response.Branch not found'), null, 404);
        }

        $schedule = $branch->schedules()->find($id);

        if (!$schedule) {
            return $this->response(false,  __('response.Schedule not found'), null, 404);
        }

        // التحقق من صحة البيانات المدخلة
        $validator = Validator::make($request->all(), [
            'day_id' => 'required|exists:days,id',
            'start_time' => 'required|date_format:h:i A', // دعم AM/PM
            'end_time' => 'required|date_format:h:i A|after:start_time',
        ], [
            'day_id.required'     =>         __('validation.The day field is required.'),
            'day_id.exists'       =>         __('validation.The selected day does not exist.'),
            'start_time.required' =>         __('validation.Start time is required.'),
            'start_time.date_format' =>      __('validation.Start time format is invalid. Please use a format like 02:30 PM.'),
            'end_time.required'   =>         __('validation.End time is required.'),
            'end_time.date_format'=>         __('validation.End time format is incorrect. It should look like 03:30 PM.'),
            'end_time.after'      =>         __('validation.The time of completion should be after the start time.'),
        ]);
        $validator->setAttributeNames([
            'day_id' =>              __('validation.attributes.day_id'),
            'start_time' =>          __('validation.attributes.start_time'),
            'end_time' =>            __('validation.attributes.end_time'),
        ]);
        if ($validator->fails()) {
            return $this->response(false, $validator->messages()->first(), null, 400);
        }

        // تحويل الأوقات من AM/PM إلى 24 ساعة
        $start_time_24 = Carbon::createFromFormat('h:i A', $request->start_time)->format('H:i');
        $end_time_24 = Carbon::createFromFormat('h:i A', $request->end_time)->format('H:i');

        // التحقق من وجود تعارض مع مواعيد أخرى في نفس اليوم
        $existingSchedule =  $branch->schedules()->where('day_id', $request->day_id)
            ->where('id', '!=', $schedule->id) // استبعاد الموعد الحالي
            ->where(function ($query) use ($start_time_24, $end_time_24) {
                $query->whereBetween('start_time', [$start_time_24, $end_time_24])
                    ->orWhereBetween('end_time', [$start_time_24, $end_time_24])
                    ->orWhere(function ($query) use ($start_time_24, $end_time_24) {
                        $query->where('start_time', '<=', $start_time_24)
                            ->where('end_time', '>=', $end_time_24);
                    });
            })
            ->exists();

        if ($existingSchedule) {
            return $this->response(false, __('response.This time slot is already taken for this day.'), null, 400);
        }

        // تحديث الموعد
        $schedule->update([
            'day_id' => $request->day_id,
            'start_time' => $start_time_24,
            'end_time' => $end_time_24,
        ]);

        return $this->response(true, __('response.Schedule updated successfully'));
    }

    /**
     * حذف موعد
     */
    public function destroy($id)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);

        if (!$branch) {
            return $this->response(false, __('response.Branch not found'), null, 404);
        }

        $schedule = $branch->schedules()->find($id);

        if (!$schedule) {
            return $this->response(false, __('response.Schedule not found'), null, 404);
        }

        // حذف الموعد
        $schedule->delete();

        return $this->response(true, __('response.Schedule deleted successfully'));
    }

    //active
    public function active($id)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);

        if (!$branch) {
            return $this->response(false, __('response.Branch not found'), null, 404);
        }

        $schedule = $branch->schedules()->find($id);

        if (!$schedule) {
            return $this->response(false, __('response.Schedule not found'), null, 404);
        }

        // لو المعاد مفعل اعمل له تعطيل والعكس
        $schedule->update([
            'is_active' => !$schedule->is_active,
        ]);

        if ($schedule->is_active) {
            return $this->response(true, __('response.Schedule deactivated successfully'));
        }
        return $this->response(true, __('response.Schedule activated successfully'));
    }
}
