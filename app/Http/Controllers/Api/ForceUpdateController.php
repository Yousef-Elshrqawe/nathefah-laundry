<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\ForceUpdate;
use App\Models\TermsCondition;
use App\Models\UserTerms;
use App\Traits\response;
use Illuminate\Http\Request;

class ForceUpdateController extends Controller
{

    use response;

    public function __construct()
    {
        $lang = request()->header('lang');
        if ($lang) {
            app()->setLocale($lang);
        }
    }

    //checkVersion
    public function checkVersion(Request $request)
    {
        $platform = $request->platform;
        $appVersion = $request->app_version;
        $appName = $request->app_name;

        $forceUpdate = ForceUpdate::where('platform', $platform)
            ->where('app_name', $appName)
            ->orderBy('id', 'desc')
            ->first();
        if (!$forceUpdate) {
            return $this->response(false, __('response.Force update not found'), null, 400);
        }
        $needsUpdate = version_compare($appVersion, $forceUpdate->version, '<');
        if (!$needsUpdate) {
            return $this->response(false, __('response.Force update not found'), null, 400);
        }



        return $this->response(true, __('response.Force update retrieved successfully'), $forceUpdate, 200);
    }

    //termsCondition
    public function termsCondition()
    {
        $termsCondition = TermsCondition::select('terms_conditions_' . app()->getLocale() . ' as terms_conditions', 'privacy_policy_' . app()->getLocale() . ' as privacy_policy')->first();

        // لو  التوكن  اتبعت  واليوزر  مسجل
        if (auth()->check()) {
            $userTerms = UserTerms::where('user_id', auth()->id())->where('terms_condition_id' , $termsCondition->id)->first();
            if (!$userTerms) {
                return $this->response(false, __('response.User terms not found'), null, 400);
            }
            $userTerms->update([
                'is_read' => 1
            ]);
        }


        return $this->response(true, __('response.Terms and conditions retrieved successfully'), $termsCondition, 200);
    }

    // userTerms
    public function userTerms()
    {
        $termsCondition = TermsCondition::first();
        $userTerms = UserTerms::where('user_id', auth()->id())->where('terms_condition_id' , $termsCondition->id)->first();
        if (!$userTerms) {
            return $this->response(false, __('response.User terms not found'), null, 400);
        }

        return $this->response(true, __('response.User terms retrieved successfully'), $userTerms, 200);
    }

    // userTerms is_read
    public function userTermsIsRead()
    {
        $termsCondition = TermsCondition::first();
        $userTerms = UserTerms::where('user_id', auth()->id())->where('terms_condition_id' , $termsCondition->id)->first();
        if (!$userTerms) {
            return $this->response(false, __('response.User terms not found'), null, 400);
        }

        $userTerms->update([
            'is_read' => 1
        ]);

        return $this->response(true, __('response.User terms retrieved successfully'), $userTerms, 200);
    }

}
