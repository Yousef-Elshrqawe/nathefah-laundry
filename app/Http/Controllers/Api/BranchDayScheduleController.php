<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Day;
use App\Models\Laundry\branch;
use App\Traits\response;
use Illuminate\Http\Request;
use Auth;

class BranchDayScheduleController extends Controller
{
    use response;

    public function __construct()
    {
        $lang = request()->header('lang');
        if ($lang) {
            app()->setLocale($lang);
        }
    }

    public function index()
    {
        $rows = Day::all();
        return $this->response(true, __('response.Days retrieved successfully'), $rows);
    }

    //الموعيد بنائن  علي  id  اليوم  و  id  الفرع
    public function daySchedule(Request $request ,$id)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);
        if (!$branch) {
            return $this->response(false, __('response.Branch not found'), null, 404);
        }
        $day = Day::find($id);
        if (!$day) {
            return $this->response(false, __('response.Day not found'), null, 404);
        }
        $schedules = $day->schedules()->where('scheduleable_id', $branch_id)->where('scheduleable_type', 'App\Models\Laundry\branch')->get();
        return $this->response(true, __('response.Schedules retrieved successfully'), $schedules);
    }

    // محتاج كل  الايام  والمواعيد  الخاصة بالفرع
    public function branchSchedule(Request $request)
    {
        $branch_id = Auth::guard('branchuser-api')->user()->branch_id;
        $branch = Branch::find($branch_id);
        if (!$branch) {
            return $this->response(false, __('response.Branch not found'), null, 404);
        }
        $days = Day::all();
        $schedules = [];
        foreach ($days as $day) {
            $schedules[$day->name] = $day->schedules()->where('scheduleable_id', $branch_id)->where('scheduleable_type', 'App\Models\Laundry\branch')->get();
        }
        return $this->response(true, __('response.Schedules retrieved successfully'), $schedules);
    }

}
