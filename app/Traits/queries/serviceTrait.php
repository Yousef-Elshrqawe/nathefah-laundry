<?php
namespace App\Traits\queries;
use Illuminate\Support\Facades\DB;
trait serviceTrait
{
     function serive($order_id,$driver_id,$lang){
     $services= DB::table('order_detailes')->where('order_detailes.order_id',$order_id)
    ->join('servicetranslations','servicetranslations.service_id','=','order_detailes.service_id')
    ->join('orders','orders.id','=','order_detailes.order_id')
    ->join('branchitems','branchitems.id','=','order_detailes.branchitem_id')
    ->join('branchitemtranslations','branchitemtranslations.branchitem_id','=','branchitems.id')
    ->selectRaw('sum(quantity) as quantity')
    ->selectRaw('servicetranslations.name as service')
    ->selectRaw('servicetranslations.service_id as service_id')
    ->where('order_detailes.order_id',$order_id)
    ->where('servicetranslations.locale',$lang)
    ->where('branchitemtranslations.locale',$lang)
    ->where('order_detailes.additionalservice_id','=',null)
    ->groupBy('servicetranslations.service_id')
    ->groupBy('servicetranslations.name')
    //->where('orders.driver_id',$driver_id)
    ->where('order_detailes.order_id',$order_id)
    ->get();
    return $services;
    }
    public function items($order_id,$driver_id,$lang){
        $items= DB::table('order_detailes')->where('order_detailes.order_id',$order_id)
        ->join('servicetranslations','servicetranslations.service_id','=','order_detailes.service_id')
        ->join('orders','orders.id','=','order_detailes.order_id')
        ->join('branchitems','branchitems.id','=','order_detailes.branchitem_id')
        ->join('branchitemtranslations','branchitemtranslations.branchitem_id','=','branchitems.id')
        ->selectRaw('branchitemtranslations.name')
        ->selectRaw('sum(quantity) as quantity')
        ->selectRaw('branchitemtranslations.branchitem_id as item_id')
        ->selectRaw('servicetranslations.service_id')
        ->selectRaw('order_detailes.order_id as order_id')
        ->where('order_detailes.order_id',$order_id)
        ->where('servicetranslations.locale',$lang)
        ->where('branchitemtranslations.locale',$lang)
        ->where('order_detailes.additionalservice_id','=',null)
        ->groupBy('servicetranslations.service_id')
        ->groupBy('servicetranslations.name')
        ->groupBy('branchitemtranslations.name')
        ->groupBy('branchitemtranslations.branchitem_id')
        ->groupBy('order_detailes.order_id')
        // ->where('orders.driver_id',$driver_id)
        ->where('order_detailes.order_id',$order_id)
        ->get();
        return $items;
    }
   public function additionals($order_id,$driver_id,$lang){
    $additionals=DB::table('order_detailes')->where('order_detailes.order_id',$order_id)
    ->join('orders','orders.id','=','order_detailes.order_id')
    ->join('additionalservicetranslations','additionalservicetranslations.additionalservice_id','=','order_detailes.additionalservice_id')
    ->join('branchitemtranslations','branchitemtranslations.branchitem_id','=','order_detailes.branchitem_id')
    ->selectRaw('order_detailes.service_id')
    ->selectRaw('sum(quantity) as quantity')
    ->selectRaw('branchitemtranslations.name as item')
    ->selectRaw('branchitemtranslations.branchitem_id as item_id')
    ->selectRaw('additionalservicetranslations.name')
    // ->where('orders.driver_id',$driver_id)->where('order_detailes.order_id',$order_id)
    ->where('order_detailes.additionalservice_id','!=',null)
    ->groupBy('additionalservicetranslations.additionalservice_id')
    ->groupBy('additionalservicetranslations.name')
    ->groupBy('branchitemtranslations.name')
    ->groupBy('branchitemtranslations.branchitem_id')
    ->groupBy('order_detailes.service_id')
    ->where('additionalservicetranslations.locale',$lang)
    ->where('branchitemtranslations.locale',$lang)
    ->get();
    return $additionals;
   }
        // orderservice this function get services inside orders
        public function orderserives($orders,$lang){
            $services=DB::table('order_detailes')
            ->select('servicetranslations.name','order_id','order_detailes.service_id')
            ->join('servicetranslations','servicetranslations.service_id','=','order_detailes.service_id')
            ->selectRaw('sum(quantity) as quantity')
            ->wherein('order_detailes.order_id',$this->orders_id)
            ->where('order_detailes.additionalservice_id','=',null)
            ->where('servicetranslations.locale',$lang)
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.service_id')
            ->groupBy('servicetranslations.service_id')
            ->groupBy('servicetranslations.name')
            ->get();
            $additionalservices=DB::table('order_detailes')
            ->select('order_id','order_detailes.service_id','order_detailes.additionalservice_id','additionalservicetranslations.name')
            ->join('additionalservicetranslations','additionalservicetranslations.additionalservice_id','=','order_detailes.additionalservice_id')
            ->selectRaw('sum(quantity) as quantity')
            ->wherein('order_detailes.order_id',$this->orders_id)
            ->where('order_detailes.additionalservice_id','!=',null)
            ->where('additionalservicetranslations.locale',$lang)
            ->groupBy('additionalservicetranslations.name')
            ->groupBy('order_detailes.order_id')
            ->groupBy('order_detailes.service_id')
            ->groupBy('order_detailes.additionalservice_id')
            ->get();
            $argents=db::table('argent')->wherein('order_id',$this->orders_id)->get();
            foreach($services as $service){
                foreach($argents as $argent){
                    $service->argent=0;
                    if($service->service_id==$argent->service_id&&$service->order_id == $argent->order_id){
                        $service->argent=$argent->quantity;
                    }
                }
            }
            foreach($this->items as $key=>$order){
                foreach($order as $item){
                        $item->additonalservice=[];
                        foreach($additionalservices as $additional){
                            if($item->service_id==$additional->service_id && $item->order_id==$additional->order_id){
                                array_push($item->additonalservice,$additional);
                            }
                        }
                }
            }
            foreach($services as $service){
                $service->items=[];
                foreach($this->items as $key=>$order){
                    foreach($order as $item){
                    if($item->service_id==$service->service_id && $item->order_id==$service->order_id){
                        array_push($service->items,$item);
                    }
                    }
                }
            }
            // but services inside order
            foreach($orders as $order){
                $order->services=[];
                    foreach($services as $service){
                        if($service->order_id  == $order->order_id){
                            array_push($order->services,$service);
                        }
                    }
                }
            return $orders;
        }
}
