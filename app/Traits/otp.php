<?php

namespace App\Traits;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

use Validator;

trait otp
{
    public function sendMessage($phone,$messageText){

        $response = Http::withHeaders([
            'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjMyYWM5Yzg5ZGU5Y2U1MTJmMWI0YjM4YTFmODg1OTNlNjU1NWY0MTMzMzBjOWJkYWJiN2E0ZjM1MDM4ZTY0YjY1MTkzMGU1M2Y3ZTVkYzNlYTRhNzQ2NjYxMjMxMDIwM2I4YzM5OGU4YjM4OWVmZGUyYmVmNjNlNDE4MmJmMjgwYWRmNTE5YzY2ZDQxZmMzYTNjODhhNTJjYWY2ZWFjOTYiLCJpYXQiOjE3MTUwOTQxNDEsImV4cCI6MzI5Mjk3NDE0MX0.W5ajfZSba0raTD3EXEX2qzRFNuoTTkc-UuxFBPGvOfo', // Add your token or headers here
            'accept' => 'application/json',
            'content-type' => 'application/json',
        ])->post('https://apis.deewan.sa/sms/v1/messages', [

            'senderName'     => 'Nathefah',
            'messageType'    =>'text',
            'messageText'    =>$messageText,
            'recipients'     =>$phone
        ]);
        $response =json_decode($response);
        return [
            'status'         =>true,
            'message'        =>'send message successfully',
            'codeNumber'     => $messageText,
            'smsresponse'    =>$response
        ];
    }

    public function sendSmsMessage($request,$type,$messageText,$codeNumber = null,$validation=true){

        $country_code =$request->country_code;
        $phone        =$request->phone;

        $phone=$request->country_code.$phone;

        if($type=='laundry'){
            $table='laundries';
        }elseif($type=='user'){
            $table='users';
        }elseif($type=='branch'){
            $table='branchs';
        } elseif($type=='drivers'){
            $table='drivers';
        }else{
            $table='branchUser';
        }

        /*     if($validation==true){
                 $validator =Validator::make($request->all(), [
                     'phone'=>'required|exists:'.$table.',phone',
                   ]);
                   if ($validator->fails()) {
                      $data['status'] =false;
                         $data['message'] =$validator->errors()->first();
                         return $data;
                   }
             }*/


        $response = Http::withHeaders([
            'Authorization' => 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ0b2tlbiI6IjMyYWM5Yzg5ZGU5Y2U1MTJmMWI0YjM4YTFmODg1OTNlNjU1NWY0MTMzMzBjOWJkYWJiN2E0ZjM1MDM4ZTY0YjY1MTkzMGU1M2Y3ZTVkYzNlYTRhNzQ2NjYxMjMxMDIwM2I4YzM5OGU4YjM4OWVmZGUyYmVmNjNlNDE4MmJmMjgwYWRmNTE5YzY2ZDQxZmMzYTNjODhhNTJjYWY2ZWFjOTYiLCJpYXQiOjE3MTUwOTQxNDEsImV4cCI6MzI5Mjk3NDE0MX0.W5ajfZSba0raTD3EXEX2qzRFNuoTTkc-UuxFBPGvOfo', // Add your token or headers here
            'accept' => 'application/json',
            'content-type' => 'application/json',
        ])->post('https://apis.deewan.sa/sms/v1/messages', [

            'senderName'     => 'Nathefah',
            'messageType'    =>'text',
            'messageText'    =>$messageText,
            'recipients'     =>$phone
        ]);
        $data['messageText']=$codeNumber;
        $response =json_decode($response);
        return [
            'status'         =>true,
            'message'        =>'send message successfully',
            'data'           =>$data,
            'messageText'    => $codeNumber,
            'codeNumber'     => $messageText,
            'smsresponse'    =>$response
        ];

    }

    public function randomOtp(){
        return rand(1000,9999);
//        return '1234';
    }
} // end of fileTrait
