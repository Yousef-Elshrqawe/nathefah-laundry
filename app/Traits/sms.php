<?php

namespace App\Traits;

use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;


trait sms
{


    public function sendSms($phone, $messageText, $validation = true)
    {
        require_once('vendor/autoload.php');

        $client = new \GuzzleHttp\Client();

        $response = $client->request('POST', 'https://apis.deewan.sa/sms/v1/messages', [
            'body' => '{
            "senderName":"Nathefah",
            "messageType":"text",
            "messageText":"'.$messageText.'",
            "recipients":"'.$phone.'"
            }',



            'headers' => [
                'Authorization' => 'Bearer ' . env('SMS_TOKEN'), // Add your token or headers here
                'accept' => 'application/json',
                'content-type' => 'application/json',
            ],
        ]);

        echo $response->getBody();

    }

} // end of fileTrait
