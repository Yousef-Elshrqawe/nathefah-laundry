<?php

namespace App\Traits;

use App\Models\Day;
use App\Models\Laundry\branch;
use Carbon\Carbon;
use Illuminate\Support\Facades\App;

trait FilterDateBranchTrait
{

    public function filterDateBranch($model)
    {
        $query = $model;

        $today = Carbon::now()->format('l');
        $day = Day::where('name', $today)->first();
        $time = Carbon::now()->format('H:i:s');

        $branchAll = branch::whereHas('schedules', function ($query) use ($day, $time) {
            $query->where('day_id', $day->id)
                ->where('is_active', 1)
                ->where('start_time', '<=', $time)
                ->where('end_time', '>=', $time);
        })->get();

    }

} // end of fileTrait
