<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendEailV1 extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $email;
    public function __construct($email)
    {
        //
        $this->email=$email;
    }
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $subject=$this->email['email_title'];
        return $this->markdown('emails.sendEmailV1')
            ->subject($subject)
            ->with([
                'email'=>$this->email
            ]);
//        return $this->view('emails.sendEmailV1',compact('email',));
    }
}
