<?php

namespace App\Mail;

use App\Models\Laundry\branch;
use App\Models\Laundry\Laundry;
use App\Models\Setting;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    public $order;
    public $urgents;

    public function __construct($order, $urgents)
    {
        //
        $this->order = $order;
        $this->urgents = $urgents;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $setting=Setting::select('support_phone','country_code')->first();
        $url = route('generateInvoice.download', $this->order->id);
//        $qrCodeBase64 = QrCode::size(100)->color(32 , 188 , 227)
//            ->style('square')
//            ->generate($url);
        $qrCode = QrCode::format('png')->size(100)
            ->color(32, 188, 227)
            ->style('square')
            ->generate($url);
        $qrCodeBase64 = base64_encode($qrCode);

        //save qr code in public
        $path = public_path('qr_codes/' . $this->order->id . '.png');
        file_put_contents($path, $qrCode);

        $order = $this->order;
        $urgents = $this->urgents;

        //laundry
        $branch = branch::where('id', $order->branch_id)->first();
        $laundry = Laundry::where('id', $branch->laundry_id)->first();

        return $this->view('Mails.invoice', compact('order', 'urgents' , 'setting','laundry'))->with('qrCodeBase64', $qrCodeBase64);
    }
}
