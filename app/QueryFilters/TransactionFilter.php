<?php

namespace App\QueryFilters;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

class TransactionFilter
{
    protected $request;

    public function __construct($request)
    {
        $this->request = $request;
    }

    public function apply(Builder $query): Builder
    {
        $this->filterByDate($query);
        $this->filterByPending($query);

        return $query;
    }

    protected function filterByDate(Builder $query): void
    {
        if ($this->request->has('date_filter')) {
            $dateFilter = $this->request->date_filter;
            $now = now();  // احصل على التاريخ والوقت الحالي

            switch ($dateFilter) {
                case 'today':
                    // تصفية اليوم فقط (دون النظر إلى الوقت)
                    $query->whereDate('updated_at', $now->toDateString());
                    break;

                case 'this_week':
                    // تصفية هذا الأسبوع (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->startOfWeek()->toDateString(), // بداية الأسبوع (التاريخ فقط)
                        $now->endOfWeek()->toDateString()    // نهاية الأسبوع (التاريخ فقط)
                    ]);
                    break;

                case 'this_month':
                    // تصفية هذا الشهر (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->startOfMonth()->toDateString(), // بداية الشهر (التاريخ فقط)
                        $now->endOfMonth()->toDateString()    // نهاية الشهر (التاريخ فقط)
                    ]);
                    break;

                case 'last_month':
                    // تصفية الشهر الماضي (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->subMonthNoOverflow()->startOfMonth()->toDateString(), // بداية الشهر الماضي
                        $now->subMonthNoOverflow()->endOfMonth()->toDateString()    // نهاية الشهر الماضي
                    ]);
                    break;

                case 'last_3_months':
                    // تصفية آخر 3 أشهر (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->subMonths(3)->startOfMonth()->toDateString(), // بداية آخر 3 أشهر
                        $now->endOfMonth()->toDateString()                  // نهاية الشهر الحالي
                    ]);
                    break;

                case 'last_6_months':
                    // تصفية آخر 6 أشهر (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->subMonths(6)->startOfMonth()->toDateString(), // بداية آخر 6 أشهر
                        $now->endOfMonth()->toDateString()                  // نهاية الشهر الحالي
                    ]);
                    break;


                case 'this_year':
                    // تصفية هذه السنة (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->startOfYear()->toDateString(), // بداية السنة
                        $now->endOfYear()->toDateString()    // نهاية السنة
                    ]);
                    break;
                case 'last_year':
                    // تصفية السنة الماضية (دون النظر إلى الوقت)
                    $query->whereBetween('updated_at', [
                        $now->subYear()->startOfYear()->toDateString(), // بداية السنة الماضية
                        $now->subYear()->endOfYear()->toDateString()    // نهاية السنة الماضية
                    ]);
                    break;

                default:
                    // تصفية إذا كان التاريخ المدخل صالحًا (دون النظر إلى الوقت)
                    if ($this->isValidDate($dateFilter)) {
                        $query->whereDate('updated_at', $dateFilter);
                    }
                    break;
            }
        }
    }


    protected function filterByPending(Builder $query): void
    {
        if ($this->request->has('pending')) {
            $pending = $this->request->pending;

            if (in_array($pending, [0, 1])) {
                $query->where('pending', $pending);
            }
        }
    }

    private function isValidDate($date): bool
    {
        return Carbon::createFromFormat('Y-m-d', $date) !== false;
    }
}
