<?php

namespace Database\Seeders;

use App\Models\Laundry\Branchuser;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BranchAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       $user = Branchuser::create([
                'username'     => "Amr Kahla",
                'email'        => "amrkahla6@gmail.com",
                'country_code' => "+20",
                'phone'        => "01154400681",
                'branch_id'    => 1,
                'password'     => 123456, // Default passwords
            ]);

        $role = Role::create([
            'guard_name' => 'branch',
            'name'       => 'Admin'
        ]);

        $permissions = Permission::pluck('id','id')->all();
        $role->syncPermissions($permissions);
        $user->assignRole([$role->id]);
    }
}
