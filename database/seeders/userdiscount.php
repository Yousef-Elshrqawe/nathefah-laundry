<?php

namespace Database\Seeders;
use App\Models\Discount\Discount;
use Illuminate\Database\Seeder;

class userdiscount extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        for($i=0;$i<100;$i++){
            for($j = 0; $j <= 1000; $j++){
            $data[]=[
            'code'=>'fdvd',
            'percent'=>25,
            'amount'=>0,
            'start_date'=>'2023-02-11 15:39:30',
            'enddate'=>'2023-02-20 15:39:30',
            'created_by'=>'Admin',
            'min_order_price'=>'150'
            ];
        }
        }
        $chuncks=array_chunk($data,5000);
        foreach($chuncks as $discount){
            Discount::insert($data);
        }
    }
}
