<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subscription\{Period,Package,PeriodTranslations as periodtrans};

class periodtranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $periods=Period::get();
        foreach($periods as $period){
            periodtrans::create([
              'period_id'=>$period->id,
              'name'=>$period->name,
              'locale'=>'ar'
            ]);
        }
    }
}
