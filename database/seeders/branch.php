<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class branch extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('branchs')->insert([[
            'id'=>1,
            'username'=>'branchusername',
            'country_code'=>'011',
            'phone'=>'2342385212',
            'status'=>'open',
            'lat'=>'30.419452',
            'long'=>'31.729253',
            'laundry_id'=>1,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
        ],
         [
            'id'=>2,
            'username'=>'secondbranch',
            'country_code'=>'011',
            'phone'=>'0125526224',
            'status'=>'open',
            'lat'=>'29.845572',
            'long'=>'31.521154',
            'laundry_id'=>1,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ],
         [
            'id'=>3,
            'username'=>'sadoons',
            'country_code'=>'011',
            'phone'=>'0125526554',
            'status'=>'open',
            'lat'=>'30.776634',
            'long'=>'31.001959',
            'laundry_id'=>2,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ], [
            'id'=>4,
            'username'=>'nazifa3',
            'country_code'=>'011',
            'phone'=>'012552655',
            'status'=>'open',
            'lat'=>'30.563142',
            'long'=>'31.187909',
            'laundry_id'=>3,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ], [
            'id'=>5,
            'username'=>'nazafa',
            'country_code'=>'011',
            'phone'=>'0125528',
            'status'=>'open',
            'lat'=>'30.464656',
            'long'=>'31.268116',
            'laundry_id'=>4,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ],[
            'id'=>6,
            'username'=>'branch4',
            'country_code'=>'011',
            'phone'=>'123456',
            'status'=>'open',
            'lat'=>'30.116374',
            'long'=>'31.201792',
            'laundry_id'=>5,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ],[
            'id'=>7,
            'username'=>'branch25',
            'country_code'=>'011',
            'phone'=>'1234562223',
            'status'=>'open',
            'lat'=>'30.016374',
            'long'=>'31.201792',
            'laundry_id'=>5,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ],[
            'id'=>8,
            'username'=>'nanazifaa',
            'country_code'=>'011',
            'phone'=>'012055236665',
            'status'=>'open',
            'lat'=>'30.016374',
            'long'=>'31.201792',
            'laundry_id'=>6,
            'open_time'=>'09:41:05',
            'closed_time'=>'10:41:05',
            'address'=>'el ahram street',
            'password' => Hash::make(123456),
         ]]);
    }
}
