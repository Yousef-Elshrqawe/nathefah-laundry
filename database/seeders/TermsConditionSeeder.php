<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TermsConditionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\TermsCondition::create([
            'terms_conditions_ar' => 'الشروط والأحكام',
            'terms_conditions_en' => 'Terms and Conditions',
            'privacy_policy_ar' => 'سياسة الخصوصية',
            'privacy_policy_en' => 'Privacy Policy',
        ]);
    }
}
