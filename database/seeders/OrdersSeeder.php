<?php
namespace Database\Seeders;
use App\Models\Order\order;
use App\Models\User;
use DeliveryType;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class OrdersSeeder extends Seeder
{
    public function run()
    {
        DB::statement("SET foreign_key_checks=0");
        order::truncate();
        DB::statement("SET foreign_key_checks=1");

        order::create([
            'id' => 1,
            'payed' => 1,
            'progress' => 1,
            'delivery_status' => 1,
            'driver_id' =>  \App\Models\Driver\Driver::first()->id,
            'checked' => 1,
            'customer_name' => 'John Doe',
            'customer_phone' => '123456789',
            'customer_location' => 'Cairo',
            'lat' => 30.0444,
            'long' => 31.2357,
            'branch_id' =>  166,
            'delivery_type_id' => '1',
            'payment_method_id' => '1',
            'user_id' => User::first()->id,
            'day' => '2021-07-01',
            'from' => '10:00',
            'to' => '12:00',
            'order_by' => '2021-07-01 10:00:00',
            'order_time' => '2021-07-01 10:00:00',
            'rate' => 5,
            'urgent' => 1,
            'picked_date' => '2021-07-01',
            'drop_date' => '2021-07-02',
            'price_after_discount' => 100,
            'price_before_discount' => 150,
            'refrencenumber' => '123456',
            'country_code' => '+20',
            'order_delivery_status' => '1',
        ]);
    }
}
