<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Notification\Usernotifytype;

class userwalletnotification extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $users=User::get();
        foreach($users as $user){
            Usernotifytype::create([
                'user_id'=>$user->id,
                'notificationtype_id'=>10
            ]);
        }
    }
}
