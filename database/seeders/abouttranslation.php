<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class abouttranslation extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('abouttranslations')->insert([[
            'id' => 1,
            'about_id'=>1,
            'content'=>'nazifaa is best application for washing and cleaning services',
            'locale'=>'en',
        ],
        [
            'id' => 2,
            'about_id'=>1,
            'content'=>'نظيفه تطبيق رائع من اجمل التطبيقات الذكيه ',
            'locale'=>'ar',
        ]]);
    }
}
