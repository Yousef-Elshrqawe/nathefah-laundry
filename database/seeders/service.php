<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class service extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('services')->insert([[
            'id' => 1,
            'img'=>'washing.png',
        ],[
            'id' => 2,
            'img'=>'washing.png',
        ],[
            'id' => 3,
            'img'=>'iron.png',
        ],[
            'id'=>4,
            'img'=>'iron.png',
        ]]);
    }
}
