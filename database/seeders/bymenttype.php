<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class bymenttype extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pymenttype')->insert([[
            'id' => 1,
            'name'=>'wallet'
        ],[
            'id' => 2,
            'name'=>'cash'
        ],[
            'id' => 3,
            'name'=>'visa'
        ]]);
    }
}
