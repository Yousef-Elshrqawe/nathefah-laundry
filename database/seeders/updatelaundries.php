<?php

namespace Database\Seeders;


use App\Models\Laundry\Laundry;
use Illuminate\Database\Seeder;

class updatelaundries extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       $laundries=Laundry::get();
       foreach($laundries as $laundry){
        // $laundry->update([
        //     'door_to_door_delivery_fees'=>7,
        //     'one_way_delivery_fees'=>3.5,
        //  ]);
        $laundry->update([
            'taxamount'=>0,
         ]);
       }

    }
}
