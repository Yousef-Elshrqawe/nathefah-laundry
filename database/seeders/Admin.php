<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class Admin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
               "id"=>1,
               "name"=>'mostafa saadoun',
               "email"=>'sadoon@gmail',
               "username"=>'mostafa sadoon',
               "phone"=>'01014324321',
               "country_code"=>'011',
               'password'=>bcrypt('123456')
            ]
      ]);

    }
}
