<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Laundry\Branchuser;
use App\Models\Notification\branchnotifytype;
class userbranchnotification extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
       $branchusers=Branchuser::select('id')->get();
       foreach($branchusers as $branchuser){
        for($i=1;$i<7;$i++){
            $data[]=[
                'user_branch_id'=>$branchuser->id,
                'notificationtype_id'=>$i
              ];
           }
       }
       branchnotifytype::insert($data);
    }
}
