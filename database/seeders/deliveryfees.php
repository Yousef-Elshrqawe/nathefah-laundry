<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class deliveryfees extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('delivery_fees')->insert([
            [
            'type'=>'one way',
            'price'=>3.5,
            ],
            [
            'type'=>'door to door',
            'price'=>7,
            ],
    ]);
    }
}
