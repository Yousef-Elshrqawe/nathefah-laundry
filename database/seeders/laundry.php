<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class laundry extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('laundries')->insert([[
             'id'=>1,
             'email'=>'laundry@laundry.com',
             'phone'=>'0552342385',
             'name'=>'nazifaa',
             'country_code'=>'011',
             'status'=>'true',
             'branch'=>'one',
             'companyregister'=>'2023453',
             'taxcard'=>'2556230',
             'logo'=>'vfvdvfd.jpg',
             'password' => Hash::make(123456),
        ],
        [
            'id'=>2,
            'email'=>'laundry2@laundry.com',
            'phone'=>'0552342386',
            'name'=>'nazaaf',
            'country_code'=>'011',
            'status'=>'true',
            'branch'=>'one',
            'companyregister'=>'2023453',
            'taxcard'=>'2556230',
            'logo'=>'vfvdvfd.jpg',
            'password' => Hash::make(123456),
        ],
        [
            'id'=>3,
            'email'=>'laundry3@laundry.com',
            'phone'=>'0552342387',
            'name'=>'washing&clean',
            'country_code'=>'011',
            'status'=>'true',
            'branch'=>'one',
            'companyregister'=>'2023453',
            'taxcard'=>'2556230',
            'logo'=>'vfvdvfd.jpg',
            'password' => Hash::make(123456),
        ],
        [
            'id'=>4,
            'email'=>'laundry4@laundry.com',
            'phone'=>'0552342388',
            'name'=>'nazifaa6',
            'country_code'=>'011',
            'status'=>'true',
            'branch'=>'one',
            'companyregister'=>'2023453',
            'taxcard'=>'2556230',
            'logo'=>'vfvdvfd.jpg',
            'password' => Hash::make(123456),
        ],
        [
            'id'=>5,
            'email'=>'laundry5@laundry.com',
            'phone'=>'0552342389',
            'name'=>'nazifaa7',
            'country_code'=>'011',
            'status'=>'true',
            'branch'=>'one',
            'companyregister'=>'2023453',
            'taxcard'=>'2556230',
            'logo'=>'vfvdvfd.jpg',
            'password' => Hash::make(123456),
        ],
        [
            'id'=>6,
            'email'=>'laundry6@laundry.com',
            'phone'=>'0552342380',
            'name'=>'nazifaa8',
            'country_code'=>'011',
            'status'=>'true',
            'branch'=>'one',
            'companyregister'=>'2023453',
            'taxcard'=>'2556230',
            'logo'=>'vfvdvfd.jpg',
            'password' => Hash::make(123456),
        ],
        [
            'id'=>7,
            'email'=>'laundry7@laundry.com',
            'phone'=>'0552342322',
            'name'=>'nazifaa9',
            'country_code'=>'011',
            'status'=>'true',
            'branch'=>'one',
            'companyregister'=>'2023453',
            'taxcard'=>'2556230',
            'logo'=>'vfvdvfd.jpg',
            'password' => Hash::make(123456),
       ]]);
    }
}
