<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Subscription\{Period,Package,PackageTranslations as packagetrans};

class packagetranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $packages=Package::get();
        foreach($packages as $package){
            packagetrans::create([
              'name'=>$package->name,
              'package_id'=>$package->id,
              'description'=>$package->description,
              'locale'=>'ar'
            ]);
        }

    }
}
