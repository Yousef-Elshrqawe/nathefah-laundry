<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ForceUpdateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\ForceUpdate::create([
            'platform' => 'android',
            'version' => '27.0.0',
            'app_name' => 'nathefa',
            'is_force_update' => 1,
        ]);

        \App\Models\ForceUpdate::create([
            'platform' => 'ios',
            'version' => '27.0.0',
            'app_name' => 'nathefa',
            'is_force_update' => 1,
        ]);


        \App\Models\ForceUpdate::create([
            'platform' => 'android',
            'version' => '27.0.0',
            'app_name' => 'laundry',
            'is_force_update' => 1,
        ]);

        \App\Models\ForceUpdate::create([
            'platform' => 'ios',
            'version' => '27.0.0',
            'app_name' => 'laundry',
            'is_force_update' => 1,
        ]);

        \App\Models\ForceUpdate::create([
            'platform' => 'android',
            'version' => '27.0.0',
            'app_name' => 'delivery',
            'is_force_update' => 1,
        ]);

        \App\Models\ForceUpdate::create([
            'platform' => 'ios',
            'version' => '27.0.0',
            'app_name' => 'delivery',
            'is_force_update' => 1,
        ]);

    }
}
