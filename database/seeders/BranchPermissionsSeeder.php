<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class BranchPermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'branch-dashboard',
            'branch-order-list',
            'branch-order-create',
            'branch-order-edit',
            'branch-order-delete',
            'branch-service-list',
            'branch-service-create',
            'branch-service-edit',
            'branch-service-delete',
            'branch-driver-list',
            'branch-driver-create',
            'branch-driver-edit',
            'branch-driver-delete',
            'branch-notification-list',
            'branch-setting-list',
            'branch-basic-info-list',
            'branch-basic-info-create',
            'branch-basic-info-edit',
            'branch-basic-info-delete',
            'branch-notifications-list',
            'branch-notifications-edit',
            'branch-role-list',
            'branch-role-create',
            'branch-role-edit',
            'branch-role-delete',
            'branch-laundry-list',
            'branch-laundry-create',
            'branch-laundry-edit',
            'branch-laundry-delete',
        ];

        foreach ($permissions as $permission) {

            Permission::create([
                'guard_name' => 'branch',
                'name'       => $permission
            ]);
        }
    }
}
