<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class payment_method_trnasltions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('payment_method_translations')->insert([
            [

               "payment_method_id"=>1,
               "locale"=>"ar",
               "name"=>"فيزا",
            ],[

                "payment_method_id"=>1,
                "locale"=>"en",
                "name"=>"Visa",
            ],[

                "payment_method_id"=>2,
                "locale"=>"ar",
                "name"=>"ماستر",
            ],
            [

               "payment_method_id"=>2,
               "locale"=>"en",
               "name"=>"Mastercard",
             ]
             ,[

                "payment_method_id"=>3,
                "locale"=>"ar",
                "name"=>"ابل باي",
            ],
            [

               "payment_method_id"=>3,
               "locale"=>"en",
               "name"=>"Apple Pay",
             ]

             ,[

                "payment_method_id"=>4,
                "locale"=>"ar",
                "name"=>"اس تي سي باي",
            ],


             [

               "payment_method_id"=>4,
               "locale"=>"en",
               "name"=>"Stcpay",
             ]

             ,[

                "payment_method_id"=>5,
                "locale"=>"ar",
                "name"=>"مدي",
            ],

             [

               "payment_method_id"=>5,
               "locale"=>"en",
               "name"=>"Mada",
             ]
            ]);
    }
}
