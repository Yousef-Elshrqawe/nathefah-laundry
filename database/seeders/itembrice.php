<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\laundryservice\{Service,Argent,Serviceitemprice,branchAdditionalservice,Additionalservice};
use App\Models\Laundry\{branchservice,Branchitem,BranchitemTranslation};

class itembrice extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $serviceitemprices=Serviceitemprice::get();
        foreach($serviceitemprices as $service){
            $branchitem=Branchitem::select('id','item_id')->find($service->branchitem_id);
            $service->update([
               'item_id'=>$branchitem->item_id
            ]);
        }
    }
}
