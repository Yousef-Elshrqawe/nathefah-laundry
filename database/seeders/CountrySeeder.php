<?php

namespace Database\Seeders;

use App\Models\Country;
use Illuminate\Database\Seeder;


class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        foreach (countries() as $key => $country) {

            $c = country($key);
            if (!$c || !$c->getNationalNumberLengths() || count($c->getNationalNumberLengths()) == 0) {
                continue;
            }

            // remove elly ma tetsamash keep palastine
            if ($c->getCallingCode() == 972) {
                continue;
            }
            $data [] = [
                'name' => json_encode([
                    'en' => $c->getTranslation('eng')['common'],
                    'ar' => $c->getTranslation('ara')['common'],
                ]),
                'code' => $c->getCallingCode(),
                'number_length' => $c->getNationalNumberLength() + 1,
                'emoji' => $c->getEmoji(),
            ];
        }
        Country::insert($data);
    }
}
