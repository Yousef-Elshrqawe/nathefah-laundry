<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BranchUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('branchusers')->insert([
            [
                "email"=>'sadoon@gmail',
                "branch_id" => 1,
                "type" => "Admin",
                "username"=>'mostafa sadoon',
                "phone"=>'01014324321',
                'password'=>bcrypt('123456')
            ]
        ]);
    }
}
