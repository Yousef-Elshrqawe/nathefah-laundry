<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class notificationtypetranslations extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('notificationtypetranslations')->insert([[
            'id' => 1,
            'notificationtype_id'=>1,
            'name'=>'New offers',
            'locale'=>'en',
        ],
        [
            'id' => 2,
            'notificationtype_id'=>1,
            'name'=>'طلبات جديده',
            'locale'=>'ar',
        ],[
            'id' => 3,
            'notificationtype_id'=>2,
            'name'=>'Rate Orders',
            'locale'=>'en',
        ],
        [
            'id' => 4,
            'notificationtype_id'=>2,
            'name'=>'تقييم الطلبات',
            'locale'=>'ar',
        ],[
            'id' => 5,
            'notificationtype_id'=>3,
            'name'=>'order status',
            'locale'=>'en',
        ],
        [
            'id' => 6,
            'notificationtype_id'=>3,
            'name'=>'حاله الطلب',
            'locale'=>'ar',
        ],[
            'id' => 7,
            'notificationtype_id'=>4,
            'name'=>'order ready',
            'locale'=>'en',
        ],
        [
            'id' => 8,
            'notificationtype_id'=>4,
            'name'=>'جاهزيه الطلب',
            'locale'=>'ar',
        ],[
            'id' => 9,
            'notificationtype_id'=>5,
            'name'=>'new offer',
            'locale'=>'en',
        ],
        [
            'id' => 10,
            'notificationtype_id'=>5,
            'name'=>'عرض جديد',
            'locale'=>'ar',
        ]


        ,[
            'id' => 11,
            'notificationtype_id'=>8,
            'name'=>'order accepted',
            'locale'=>'en',
        ],
        [
            'id' => 12,
            'notificationtype_id'=>8,
            'name'=>'تم قبول الطلب',
            'locale'=>'ar',
        ]


        ,[
            'id' => 13,
            'notificationtype_id'=>9,
            'name'=>'order refused',
            'locale'=>'en',
        ],
        [
            'id' => 14,
            'notificationtype_id'=>9,
            'name'=>'تم رفض الطلب',
            'locale'=>'ar',
        ]


        ,[
            'id' => 14,
            'notificationtype_id'=>10,
            'name'=>'wallet',
            'locale'=>'en',
        ],
        [
            'id' => 15,
            'notificationtype_id'=>10,
            'name'=>'المحفظه',
            'locale'=>'ar',
        ]


      ]);
    }
}
