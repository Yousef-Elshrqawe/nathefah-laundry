<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class payment_method extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('payment_methods')->insert([
            [
               "id"=>1,
               'payment_key'=>'VISA',
               "img"=>'visa.png'
            ],[
              "id"=>2,
              'payment_key'=>'MASTERCARD',
              "img"=>'MasterCard.png'
            ],[
                "id"=>3,
                'payment_key'=>'APPLE_PAY',
                "img"=>'Apple_Pay.png'
            ],[
                "id"=>4,
                'payment_key'=>'STCPAY',
                "img"=>'Stc_pay.png'
            ],[
                "id"=>5,
                'payment_key'=>'MADA',
                "img"=>'Mada_Logo.png'

            ]
      ]);
    }
}
