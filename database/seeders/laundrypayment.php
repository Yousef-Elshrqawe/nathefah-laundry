<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Laundry\{branch,BranchPayment};



class laundrypayment extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $branchs=branch::get();
        foreach($branchs as $branch){
             for($i=1; $i<=5; $i++){
                BranchPayment::create([
                  'branch_id'=>$branch->id,
                  'payment_method_id'=>$i
                ]);
             }
        }

    }
}
