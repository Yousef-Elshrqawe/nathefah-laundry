<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('days', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('number')->nullable();
            $table->boolean('is_active')->default(true);
            $table->timestamps();
        });

        DB::table('days')->insert([
            ['name' => 'Saturday', 'number' => '1'],
            ['name' => 'Sunday', 'number' => '2'],
            ['name' => 'Monday', 'number' => '3'],
            ['name' => 'Tuesday', 'number' => '4'],
            ['name' => 'Wednesday', 'number' => '5'],
            ['name' => 'Thursday', 'number' => '6'],
            ['name' => 'Friday', 'number' => '7'],
        ]);
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('days');
    }

}

