<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Discount extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('discounts', function (Blueprint $table) {
            $table->id();
            $table->text('code');
            $table->double('amount');
            $table->double('percent');
            $table->datetime('start_date');
            $table->datetime('enddate');
            $table->double('min_order_price');
            $table->enum('created_by',['branch','Admin','laundry']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
