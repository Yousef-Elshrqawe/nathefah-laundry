<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Systemtransactions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('system_transactions', function (Blueprint $table) {

            $table->bigIncrements('id');


            $table->unsignedBigInteger('branch_id')->nullable();
            $table->foreign('branch_id')->references('id')->on('branchs')->onDelete('cascade');
            $table->unsignedBigInteger('laundry_id')->nullable();
            $table->foreign('laundry_id')->references('id')->on('laundries')->onDelete('cascade');
            $table->unsignedBigInteger('order_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');
            $table->double('amount');
            $table->enum('transaction',['enternance','externance'])->nullable();
            $table->enum('type',['balance','cache'])->nullable();
            $table->double('system_balance')->default(0);
            $table->double('branch_balance')->default(0);
            $table->enum('transaction_type',['Subscription','Withdraw','Order'])->nullable();
            $table->longtext('transaction_number')->nullbale();
            $table->enum("payment_type",['wallet','visa'])->nullable();


            $table->boolean('pending')->default(true);


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
