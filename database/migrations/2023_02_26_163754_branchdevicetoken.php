<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Branchdevicetoken extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('branchdevicestoken', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->longtext('device_token');
            $table->unsignedBigInteger('branch_user_id');
            $table->foreign('branch_user_id')->references('id')->on('branchusers')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
