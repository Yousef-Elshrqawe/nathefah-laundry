<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddCardHolderNameToUserpaymentcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userpaymentcards', function (Blueprint $table) {

            // card_holder_name
            $table->string('card_holder_name')->nullable()->after('is_favorite');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userpaymentcards', function (Blueprint $table) {
            $table->dropColumn('card_holder_name');
        });
    }
}
