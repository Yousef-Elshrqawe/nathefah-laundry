<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usernotificationtranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('usernotificationtranslations', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('body');
            $table->string('locale')->index();
            $table->unique(['usernotification_id', 'locale']);
            $table->unsignedBigInteger('usernotification_id');
            $table->foreign('usernotification_id')->references('id')->on('usernotification')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
