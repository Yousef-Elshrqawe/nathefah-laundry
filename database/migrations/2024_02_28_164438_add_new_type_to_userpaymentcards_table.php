<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddNewTypeToUserpaymentcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userpaymentcards', function (Blueprint $table) {
            // type visa , master , mada
            $table->enum('type',['visa','master','mada'])->default('visa')->after('card_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userpaymentcards', function (Blueprint $table) {
            //
        });
    }
}
