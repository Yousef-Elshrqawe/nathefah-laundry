<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_terms', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('terms_condition_id');
            $table->boolean('is_read')->default(0);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('terms_condition_id')->references('id')->on('terms_conditions')->onDelete('cascade');
            $table->timestamps();
        });

        $users = \App\Models\User::all();
        $terms = \App\Models\TermsCondition::first();
        foreach ($users as $user) {
            \App\Models\UserTerms::create([
                'user_id' => $user->id,
                'terms_condition_id' => $terms->id,
                'is_read' => 0
            ]);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_terms');
    }
}
