<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DriverReview extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
       Schema::create('driver_reviews', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->unsignedBigInteger('driver_id');
           $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
           $table->double('rate')->nullable();
           $table->timestamps();
       });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
