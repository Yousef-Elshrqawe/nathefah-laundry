<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ArgentEditLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('argent_edit_logs', function (Blueprint $table) {
            $table->unsignedBigInteger('order_id');
            $table->foreign('order_id')->references('id')->on('orders')->onDelete('cascade');


            $table->unsignedBigInteger('order_edit_id')->nullable();
            $table->foreign('order_edit_id')->references('id')->on('order_edit_logs')->onDelete('cascade'); 

            $table->unsignedBigInteger('branchitem_id');
            $table->foreign('branchitem_id')->references('id')->on('branchitems')->onDelete('cascade');
            $table->unsignedBigInteger('service_id')->nullable();
            $table->foreign('service_id')->references('id')->on('services')->onDelete('cascade');

            $table->double('price');
            $table->integer('quantity')->nullable();



            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
