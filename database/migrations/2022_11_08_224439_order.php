<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Order extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->boolean('payed')->default(false);
            $table->double('taxes')->default(0);
            $table->enum('progress',['no_progress','indelivery','inprogress','finished','completed'])->default('no_progress');
            $table->enum('delivery_status',['inprogress','completed','no_progress'])->default('no_progress');
            $table->unsignedBigInteger('driver_id')->nullable();
            $table->foreign('driver_id')->references('id')->on('drivers')->onDelete('cascade');
            $table->boolean('checked')->default(false);
            $table->string('customer_name');
            $table->string('customer_phone');
            $table->string('customer_location')->nullable();
            $table->double('lat')->nullable();
            $table->double('long')->nullable();
            $table->unsignedBigInteger('branch_id');
            $table->foreign('branch_id')->references('id')->on('branchs')->onDelete('cascade');
            $table->unsignedBigInteger('delivery_type_id')->nullable();
            $table->foreign('delivery_type_id')->references('id')->on('delivery_types')->onDelete('cascade');
            $table->unsignedBigInteger('payment_method_id')->nullable();
            $table->foreign('payment_method_id')->references('id')->on('payment_methods')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->string('country_code')->nullable();
            $table->date('day')->nullable();
            $table->time('from')->nullable();
            $table->time('to')->nullable();
            $table->boolean('start_tracking')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
