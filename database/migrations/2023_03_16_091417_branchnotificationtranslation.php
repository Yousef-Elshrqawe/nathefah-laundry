<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Branchnotificationtranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('branchnotificationtranslations', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('body');
            $table->string('locale')->index();
            $table->unique(['branch_notify_id', 'locale']);
            $table->unsignedBigInteger('branch_notify_id');
            $table->foreign('branch_notify_id')->references('id')->on('branchnotification')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
