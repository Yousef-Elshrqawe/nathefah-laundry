<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddExpectedTimeColumnToBrnachservicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('brnachservices', function (Blueprint $table) {
            // الوقت المتوقع للخدمة
            $table->string('expected_time')->default('10')->after('service_id')->comment('الوقت المتوقع للخدمة بالدقائق');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('brnachservices', function (Blueprint $table) {
            $table->dropColumn('expected_time');
        });
    }
}
