<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIsFavoriteToUserpaymentcardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('userpaymentcards', function (Blueprint $table) {

            // Add new column is_favorite
            $table->boolean('is_favorite')->default(0)->after('card_name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('userpaymentcards', function (Blueprint $table) {

            // Drop column is_favorite
            $table->dropColumn('is_favorite');
        });
    }
}
