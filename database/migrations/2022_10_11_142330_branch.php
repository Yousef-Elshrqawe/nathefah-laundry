<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class branch extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('branchs', function (Blueprint $table) {
            $table->id();
            $table->boolean('delivery_status')->default(true);
            $table->boolean('cash')->default(true);
            $table->boolean('visa')->default(true);
            $table->string('country_code');
            $table->string('phone')->unique();
            $table->boolean('argent')->default(true);
            $table->string('username')->unique();
            $table->string('lat');
            $table->string('long');
            $table->string('password');
            $table->enum('status',['open','closed']);
            $table->time('open_time')->nullable();
            $table->time('closed_time')->nullable();
            $table->string('address');
            $table->string('otp')->nullable();
            $table->unsignedBigInteger('laundry_id');
            $table->foreign('laundry_id')->references('id')->on('laundries')->onDelete('cascade');
            $table->double('pending_balance')->default(0);
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
