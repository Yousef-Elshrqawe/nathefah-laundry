<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Drivernotificationtranslation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('drivernotificationtranslations', function (Blueprint $table) {
            $table->id();
            $table->text('title');
            $table->text('body');
            $table->string('locale')->index();
            $table->unique(['driver_notify_id', 'locale']);
            $table->unsignedBigInteger('driver_notify_id');
            $table->foreign('driver_notify_id')->references('id')->on('drivernotification')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
