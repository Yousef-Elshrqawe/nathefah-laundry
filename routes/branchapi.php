<?php

use  Illuminate\Http\Request;
use  Illuminate\Support\Facades\Route;
use  App\Http\Controllers\branch\Auth\AuthController;
use  App\Http\Controllers\branch\ServiceController;
use  App\Http\Controllers\branch\closeingdaycontroller;
use  App\Http\Controllers\branch\OrderController;
use  App\Http\Controllers\branch\driverController;
use  App\Http\Controllers\branch\NotificationController;
use  App\Http\Controllers\branch\HomeController;
use  App\Http\Controllers\branch\SettingController;
use  App\Http\Controllers\branch\indeliveryorderController;
use  App\Http\Controllers\branch\ProfileController;
use  App\Http\Controllers\branch\SalesController;
use  App\Http\Controllers\branch\aboutcontroller;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::controller(AuthController::class)->group(function () {
        Route::post('branch/login','login');
        // forget password
        Route::post('checkphone','checkphone');
        Route::post('forgetpassword','forgetpassword');
        Route::post('signin/byfaceid','loginfaceid');
        Route::post('send/otp/','sendotp');   //
        Route::post('check/otp/','checkotp');

});
Route::controller(SettingController::class)->group(function () {

});
Route::controller(aboutcontroller::class)->group(function(){
    Route::get('about','index');
});
    Route::group(['middleware' => 'lundryApiAuth'],function(){

        Route::controller(AuthController::class)->group(function () {
        Route::post('branch/register','registration');
        });
    });

        Route::controller(ServiceController::class)->group(function () {
        Route::post('branch/services/set/itemprice','setitemprice');
        Route::post('branch/aditionalservices/set/itemprice','setaditionalserviceprice');
        });
    Route::controller(closeingdaycontroller::class)->group(function () {
        Route::get('branch/closingday','getcloseingday');
    });
    Route::controller(ServiceController::class)->group(function () {
        Route::get('branch/services','getservices');
        Route::get('branch/aditionalservices{branch_id?}','getaditionalservices');
        Route::get('get/branch/category/item{category_id?}','getcategoryitem');
    });
Route::group(['middleware' => 'brancusers'],function(){
    Route::controller(AuthController::class)->group(function () {
        Route::post('branch/logout','logout');
        Route::post('save/faceid','storefaceid');
    });
    Route::controller(ServiceController::class)->group(function () {
       Route::get('get/branch/additionalservice/category/item{category_id?}','additionalserviceitem');
       Route::get('get/branch/services','branchservices');
       Route::get('branch/update/service{service_id?}','updateservicestatus');
       Route::get('branch/update/additionalservice{branchitem_id?&additionalservice_id}','updateadditionalservicestatus');
       Route::get('get/branch/edit/services','edit');
       Route::get('branch/update/argent','updateargent');
       Route::get('get/branch/edit/category/service{service_id?&category_id?}','getcategory');
       Route::get('get/branch/edit/category/additional{additionalservice_id?&category_id?}','getaditionalservicecategory');
       Route::POST('branch/update/item/price','updateprice');
    });


    Route::controller(OrderController::class)->group(function () {
        Route::get('order/services','getservice');
        Route::get('order/item/detailes{item_id?}','itemdetailes');
        Route::get('order/info{order_id?}','orderinfo');
        Route::get('order/unassigned','unasignedorder');
        Route::post('order/submit','submitorder');
        Route::get('order/cancel{order_id?}','cancelorder');
        Route::get('branch/open-time','getopentime');
        Route::post('order/check','checkorder');
        Route::get('order/completedorder','completedorder');
        Route::get('order/inprogressorder','inprogressorder');
        Route::get('order/indeliveryorder','indeliveryorder');
        Route::get('order/moreorder','moreorder');
        Route::get('order/ordersummary','ordersummary');
        Route::post('order/search','serachorder');
        Route::get('order/recive{order_id?&confirm_type?}','reciveorder');
        //we use this in sacn
        Route::get('recive/order/info/{order_id?}','reciveorderinfo');
        Route::get('new/orders','getneworder');
        Route::get('new/order/info/{order_id?}','neworderinfo');
        Route::get('accept/order/{order_id?}','acceptorder');
        Route::get('reject/order/{order_id?}','rejectorder');
        Route::Post('reject/reason','rejectreason');
        Route::get('finish/order/{order_id?}','finishorder');
        // filter order
        Route::Post('order/filter/','filterorder');
        Route::get('order/delivery/status/{order_id?}','orderdeliverystatus');
        Route::get('get/order/code{order_id?}','getcode');

    });
    Route::controller(NotificationController::class)->group(function () {
        Route::get('getnotification/setting','getnotificationsetting');
        Route::get('update/notification/{notification_id?}','updatenotification');
        Route::Post('update/firebase/token','updatetoken');
        Route::get('/getnotification','getnotification');
    });
    Route::controller(driverController::class)->group(function () {
        Route::get('avilable/driver','avilabledriver');
        Route::get('offline/driver/driver','offlinedriver');
        Route::get('alldriver','alldriver');
        Route::get('assign/order/{order_id?&driver_id?}','assignorder');
        Route::POST('assign/orders','assignorders');
        Route::POST('change/deriver','changedriver');
        Route::POST('filter/driver','filterdriver');
        Route::post('search/driver','search');
    });
    Route::controller(HomeController::class)->group(function () {
        Route::get('balance','getbalance');
        Route::get('branch/info','branchinfo');
        Route::get('branch/reviews','brenchreview');
        Route::get('latest/order/review','latestreview');  //
        Route::get('balance/transaction','balancetransaction');
    });
    Route::controller(SettingController::class)->group(function () { //
        Route::get('updatestatus','updatestatus');
        Route::get('/dayes','avilabledayes');
        Route::POST('update/delivery/status','update_delivery_status');
        Route::POST('update/payment/method','update_payment_type');
        Route::get('branch/setting','getsetting');

        Route::get('get/payment/method','getpaymentmethod');

        Route::POST('update/payment/methods','update_payment_method');
    });
    Route::controller(indeliveryorderController::class)->group(function () {
        Route::get('delivery/fordriver','driverorder');
        Route::get('delivery/forcustomer','customerorder');
        Route::post('search/delivery/forcustomer','forcustomersearch');
        Route::post('search/delivery/fordriver','searchdriverorder');
    });
    Route::controller(ProfileController::class)->group(function () {
        Route::get('edit/profile','edit');
        Route::POST('update/profile','update');
        Route::POST('edit/phone','editphone');
        Route::POST('update/phone','updatephone');
        Route::POST('update/password','updatepassword');
        Route::get('changelang','changelang');
        Route::post('check/branch/otp','checkotp');
        //deleteBranch
        Route::post('delete/branch','deleteBranch');
    });
    Route::controller(SalesController::class)->group(function(){
        Route::get('get/sales','getsales');
    });


    //branche/schedules
    Route::get('branche/schedules/index' ,[\App\Http\Controllers\Api\BranchScheduleController::class,'index']);
    //store
    Route::post('branche/schedules/store' ,[\App\Http\Controllers\Api\BranchScheduleController::class,'store']);
    //update
    Route::post('branche/schedules/update/{id}' ,[\App\Http\Controllers\Api\BranchScheduleController::class,'update']);
    //destroy
    Route::post('branche/schedules/destroy/{id}' ,[\App\Http\Controllers\Api\BranchScheduleController::class,'destroy']);
    //active
    Route::post('branche/schedules/active/{id}' ,[\App\Http\Controllers\Api\BranchScheduleController::class,'active']);


    //days
    Route::get('days/index' ,[\App\Http\Controllers\Api\BranchDayScheduleController::class,'index']);
    //daySchedule
    Route::get('days/{id}/daySchedule' ,[\App\Http\Controllers\Api\BranchDayScheduleController::class,'daySchedule']);
    //branchSchedule
    Route::get('days/branch-schedule' ,[\App\Http\Controllers\Api\BranchDayScheduleController::class,'branchSchedule']);
});
