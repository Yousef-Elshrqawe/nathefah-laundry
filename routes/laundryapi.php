<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Laundry\Auth\AuthController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\PeriodController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::controller(AuthController::class)->group(function () {
    Route::post('laundry/login', 'login');
    Route::post('laundry/register', 'registration')->middleware('log');
    Route::post('laundry/verify-registration-data-laundry', 'verifyRegistrationDataLaundry');
    Route::post('send/otp/', 'sendotp')->middleware('log');
    //checkPhone
    Route::get('check/phone/', 'checkPhone')->middleware('log');
    //deleteLaundry
    Route::post('delete/laundry', 'deleteLaundry');

});

//getpranchinfo

Route::controller(AuthController::class)->group(function () {
    Route::get('laundry/branchsinfo/{laundry_id?}', 'getpranchinfo');
});


Route::group(['middleware' => 'lundryApiAuth'], function () {
    Route::controller(AuthController::class)->group(function () {
//        Route::get('laundry/branchsinfo','getpranchinfo');
        Route::post('subscript', 'subscription');
    });
});


Route::controller(PeriodController::class)->group(function () {
    Route::get('get/periods', 'getPeriods');
});
Route::controller(PackageController::class)->group(function () {
    Route::get('packages/periods/{period_id}/hide-package/{package_id?}', 'getPackage');
});
