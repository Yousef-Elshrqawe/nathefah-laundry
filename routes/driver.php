<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\driver\AuthController;
use App\Http\Controllers\driver\driverController;
use App\Http\Controllers\driver\OrderController;
use App\Http\Controllers\driver\NotificationController;
use App\Http\Controllers\driver\aboutcontroller;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});


Route::controller(aboutcontroller::class)->group(function(){
    Route::get('about','index');
});



Route::controller(AuthController::class)->group(function () {
    Route::post('login','login');
    Route::post('sendtotp','sendtotp');  //
    Route::post('signin/byfaceid','loginfaceid');
});

Route::group(['middleware' => 'driverApiAuth'],function(){
    Route::controller(AuthController::class)->group(function () {
        Route::post('logout','logout');
        Route::post('save/faceid','storefaceid');
        Route::get('changelang','changelang');

    });
    Route::controller(driverController::class)->group(function () {
        Route::get('update/status','updatestatus');
        Route::get('get/driverinfo','driverinfo');
        Route::post('update/info','updateinfo');
        Route::post('update/phone','updatephone');
        //deleteaccount
        Route::post('delete/driver','deleteaccount');
    });

    Route::controller(OrderController::class)->group(function () {
        Route::get('new/order/{lat?&long?}','getneworder');
        Route::get('acceptorder/order{order_id?}','Acceptorder');
        Route::get('reject/order{order_id?}','rejectorder');
        Route::get('order/info{order_id?}','orderinfo');
        Route::get('order/inprogress/{lat?&long?}','inprogressorder');
        Route::get('order/confirm/pickup/{order_id?&confirm_type}','confirmorder');
        Route::get('latest/order','latestorder');
        Route::get('latest/latestorderinfo/{order_id?}','latestorderinfo');
        Route::get('allorder','allorder');
        Route::get('get/order/code{order_id?}','getcode');
        // we use this for mobile developer in fire base
        Route::get('check/confirm/drop_of{order_id?&confirm_type}','checkdropof');

        Route::post('driver/notify/user/{order_id?&confirm_type}','alertuser');
    });

    Route::controller(NotificationController::class)->group(function () {
        Route::get('getnotification','getnotification');
        Route::get('update/notification/{notification_id?}','updatenotification');
        Route::post('updatetoken','updatetoken');
    });

});
