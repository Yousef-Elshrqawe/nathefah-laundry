<?php

use App\Http\Controllers\branch\DateController;
use App\Http\Controllers\PDFcontroller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('branch.Auth.login');
});


//view invoice
Route::get('/invoice/view', function () {
    return view('Mails.invoice');
});


Route::get('/generate/invoice/{id}', [PDFcontroller::class, 'generateInvoice'])->name('generateInvoice.download');


//Route::post('web/mail/send/order/{id}', [PDFcontroller::class, 'sendOrder'])->name('sendOrder.download');

//dates
