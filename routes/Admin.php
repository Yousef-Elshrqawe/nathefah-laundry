<?php

use App\Http\Controllers\Admin\AdditionalServiceController;
use App\Http\Controllers\Admin\CategoriesController;
use App\Http\Controllers\Admin\PackageController;
use App\Http\Controllers\Admin\PeriodController;
use App\Http\Controllers\Admin\PointController;
use App\Http\Controllers\branch\DateController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\{AdminController,
    ApplicationRatesController,
    BranchsController,
    HomeController,
    BalanceController,
    DriverController,
    BranchController,
    OrderController,
    SendNotificationsController,
    SettingController,
    AuthController,
    LaundryController,
    ServiceController,
    DiscountController,
    ItemController,
    DeliveryfeesController,
    TermsConditionController,
    UpdateAppsController,
    UserController,
    WalletController,
    SupportController
};


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('Super-admin')->group(function () {  //Admin.logout
    Route::controller(AuthController::class)->group(function () {
        Route::get('login', 'loginview')->name('Admin.login');
        Route::Post('login', 'login')->name('Admin.login.submit');
    });
    Route::middleware(['AdminAuth'])->group(function () {
        Route::controller(HomeController::class)->group(function () {
            Route::get('home', 'index')->name('Admin.deashboard');
        });

        Route::controller(AuthController::class)->group(function () {
            Route::get('logout', 'logout')->name('Admin.logout');
        });

        Route::controller(AdminController::class)->group(function () {
            Route::get('admins', 'index')->name('Admin.admins');
            Route::post('admins/store', 'store')->name('admin.admins.store');
            Route::get('admins/edit/{id}', 'edit')->name('admin.admins.edit');
            Route::post('admins/update/{id}', 'update')->name('admin.admins.update');
            //admin.admins.delete
            Route::get('admins/delete/{id}', 'destroy')->name('admin.admins.delete');
        });

        Route::controller(UserController::class)->group(function () {
            Route::get('users', 'index')->name('admin.users');
            Route::post('users/store', 'store')->name('admin.users.store');
            Route::get('users/edit/{id}', 'edit')->name('admin.users.edit');
            Route::post('users/update/{id}', 'update')->name('admin.users.update');
            //admin.users.delete
            Route::get('users/delete/{id}', 'destroy')->name('admin.users.delete');
        });

        Route::controller(DriverController::class)->group(function () {
            Route::get('drivers', 'index')->name('admin.drivers');
            Route::post('drivers/store', 'store')->name('admin.drivers.store');
            Route::get('drivers/edit/{id}', 'edit')->name('admin.drivers.edit');
            Route::post('drivers/update/{id}', 'update')->name('admin.drivers.update');
            Route::get('drivers/delete/{id}', 'destroy')->name('admin.drivers.delete');
            //getBranchesAjax
            Route::get('getBranchesAjax/{id}', 'getBranchesAjax')->name('admin.getBranchesAjax');
        });

        Route::controller(BranchsController::class)->group(function () {
            Route::get('branches', 'index')->name('admin.branches.index');
            //create
            Route::get('branches/create', 'create')->name('admin.branches.create');
            Route::post('branches/store', 'store')->name('admin.branches.store');
            Route::get('branches/edit/{id}', 'edit')->name('admin.branches.edit');
            Route::post('branches/update/{id}', 'update')->name('admin.branches.update');
            Route::get('branches/delete/{id}', 'destroy')->name('admin.branches.delete');

            Route::get('/dates/{branch_id?}', [DateController::class, 'index'])->name('admin.dates.index');
            //branch.dates.show
            Route::get('/dates/show/{id}/{branch_id?}', [DateController::class, 'show'])->name('admin.branch.dates.show');
            //branch.edit.schedule
            Route::get('/dates/edit/{id}/{branch_id?}', [DateController::class, 'edit'])->name('admin.branch.edit.schedule');
            //branch.update.schedule
            Route::post('/dates/update/{id}/{branch_id?}', [DateController::class, 'update'])->name('admin.branch.update.schedule');
            //branch.delete.schedule
            Route::get('/dates/delete/{id}/{branch_id?}', [DateController::class, 'delete'])->name('admin.branch.delete.schedule');
            //branch.store.schedule
            Route::post('/dates/store/{branch_id?}', [DateController::class, 'store'])->name('admin.branch.store.schedule');
        });

        Route::controller(BranchsController::class)->group(function () {
            //payment
            Route::get('branch/payment/{branch_id}', 'payment')->name('admin.branch.payment');
            Route::post('branch/payment/store', 'paymentstore')->name('admin.branch.payment.store');
            Route::get('branch/payment/edit/{id}', 'paymentedit')->name('admin.branch.payment.edit');


        });


        //Update Apps
        Route::controller(UpdateAppsController::class)->group(function () {
            Route::get('update/apps', 'index')->name('admin.update.apps');
            Route::post('update/apps/store', 'store')->name('admin.update.apps.store');
            Route::get('update/apps/edit/{id}', 'edit')->name('admin.update.apps.edit');
            Route::post('update/apps/update/{id}', 'update')->name('admin.update.apps.update');
            Route::get('update/apps/delete/{id}', 'destroy')->name('admin.update.apps.delete');
        });

        //terms_conditions
        Route::controller(TermsConditionController::class)->group(function () {
            Route::get('terms_conditions', 'index')->name('admin.terms_conditions');
            Route::post('terms_conditions/store', 'store')->name('admin.terms_conditions.store');
            Route::get('terms_conditions/edit/{id}', 'edit')->name('admin.terms_conditions.edit');
            Route::post('terms_conditions/update/{id}', 'update')->name('admin.terms_conditions.update');
            Route::get('terms_conditions/delete/{id}', 'destroy')->name('admin.terms_conditions.delete');
        });

        Route::controller(LaundryController::class)->group(function () {
            Route::get('pending/laundries', 'pandinglaundry')->name('Admin.pending.laundry');
            Route::get('pending/laundry/{id}', 'shownewlaundry')->name('Admin.pending.laundry.show');
            Route::get('pending/laundry/Activation/{id}', 'Activation')->name('Admin.pending.laundry.Activation');

            Route::get('laundries/{date}', 'index')->name('Admin.laundry.index');
            Route::post('laundries/store', 'store')->name('Admin.laundry.store');
            Route::get('laundry/laundry-details/{laundry_id}', 'laundryDetails')->name('Admin.laundry.details');
            Route::put('laundry/laundry-details/update/{laundry_id}', 'laundryDetailsUpdate')->name('Admin.laundry.details.update');

            Route::get('active/laundry/{id}', 'acticealundry')->name('Admin.Active.Luandry');
            Route::get('disaple/laundry/{id}', 'disapleslaundry')->name('Admin.disaple.Luandry');

            //createBranch
            Route::get('create/branch', 'createBranch')->name('Admin.create.branch');

        });

        Route::controller(DiscountController::class)->group(function () {
            Route::get('discount/{type}', 'index')->name('Admin.discount');
            Route::post('discount/store', 'store')->name('Admin.discount.store');
            Route::get('discount/edit/{id}', 'edit')->name('Admin.discount.edit');
            Route::get('discount/delete/{id}', 'delete')->name('Admin.discount.delete');
            Route::post('discount/update/', 'update')->name('Admin.discount.update');

            // discount for  new user
            Route::get('new/users/discount/{type?}', 'newuserdiscount')->name('Admin.new.users.discount.users');
            Route::get('new/users/discount/edit/{id}', 'newuserdiscountEdit')->name('Admin.new.users.discount.edit');
            Route::post('new/users/discount/update/', 'newuserdiscountUpdate')->name('Admin.new.users.discount.update');

            // show
            Route::get('discount/assign/{id}', 'assign')->name('Admin.discount.assign');
            Route::get('discount/assignDiscount/{id}', 'assginalluser')->name('Admin.assign.discount');

            Route::get('assigned/user/delete/{id}', 'deleteassigneduser')->name('assigned.delete');
            // assign specify user
            Route::get('search/user/{name}', 'search')->name('name.discount.user');
            Route::post('assgined/user/', 'assginuser')->name('assigned.user');

        });

        Route::controller(SendNotificationsController::class)->group(function () {
            Route::get('send-notifications', 'index')->name('Admin.send-notifications');
            //create
            Route::get('send-notifications/create/{type}', 'create')->name('Admin.send-notifications.create');
            //store
            Route::post('send-notifications/store', 'store')->name('Admin.send-notifications.store');
        });
        Route::controller(DriverController::class)->group(function () {
            Route::get('driver', 'index')->name('Admin.driver');
        });
        Route::controller(BranchController::class)->group(function () {
            Route::get('branchs', 'index')->name('Admin.branchs');
        });
        Route::controller(OrderController::class)->group(function () {
            Route::get('orders/{type}/{date}/{reviewdate}', 'index')->name('admin.orders');
            //create order
            Route::get('create/order', 'create')->name('admin.admin.create.order');
            Route::get('store/order', 'store')->name('admin.admin.store.order');

            Route::get('completed/orders', 'completed')->name('admin.completed.orders');
            //filterOrders
            Route::get('filter/orders', 'filterOrders')->name('admin.filter.orders');
            Route::get('/getBransh/{id?}', 'getBransh')->name('admin.getBransh');

            //export invoices orders to excel
            Route::get('export/invoices/{ids}', 'exportInvoices')->name('admin.export.invoices');

            Route::post('submit', 'submit')->name('admin.submit.order');
            Route::get('latest/orders', 'latestorder')->name('admin.latest.orders');
            Route::get('all/order', 'viewall')->name('admin.allorder');
            Route::get('order/view/{id}', 'view')->name('admin.order.view');
            Route::get('confirm/order/{order_id}', 'confirmorder')->name('admin.confirm.order');
            Route::get('finish/order/{order_id}', 'finishorder')->name('admin.finish.order');
            Route::get('order/accept/order/{order_id}', 'acceptorder')->name('admin.accept.order');
            Route::get('assign/order/view/{order_id}', 'assignorderview')->name('admin.assignorderview');
            Route::get('assign/driver/{driver_id}/{order_id}', 'assgindriver')->name('admin.assign.driver');
            Route::get('get/qr_code/{order_id}', 'generate_qr_code')->name('admin.generate_qr_code');
            Route::get('receive/order/{order_id?}', 'reciveorder')->name('admin.receive.order');
            Route::get('admin/branch/laundry', 'getBranch')->name('admin.get.laundry.branches');
        });
        Route::controller(SettingController::class)->group(function () {
            Route::get('setting', 'index')->name('Admin.setting');
            Route::post('update', 'update')->name('Admin.setting.update');
            Route::post('add/user', 'storeuser')->name('Admin.setting.adduser');
        });


        // Categories
        Route::controller(CategoriesController::class)->group(function () {
            Route::get('categories', 'index')->name('Admin.categories');
            Route::post('categories/store', 'store')->name('admin.categories.store');
            Route::put('categories/update', 'update')->name('Admin.categories.update');
            Route::delete('categories/delete/{id}', 'destroy')->name('Admin.categories.destroy');
        });

        //Services
        Route::controller(ServiceController::class)->group(function () {
            Route::get('services', 'index')->name('Admin.services');
            Route::post('services/store', 'store')->name('admin.services.store');
            Route::get('services/edit/{id}', 'edit')->name('admin.services.edit');
            Route::post('services/update/{id}', 'update')->name('admin.services.update');
            Route::post('services/category', 'assignCategoryToService')->name('admin.services.category');
            Route::get('services/category', 'getServiceCategory')->name('admin.get.services.category');
        });

        // Additional Services
        Route::controller(AdditionalServiceController::class)->group(function () {
            Route::get('additional/services', 'index')->name('Admin.additional-services');
            Route::post('additional/services/store', 'store')->name('admin.additional-services.store');
            Route::get('additional/services/edit/{id}', 'edit')->name('admin.additional-services.edit');
            Route::post('additional/services/update/{id}', 'update')->name('admin.additional-services.update');
            Route::post('additional/services/category', 'assignCategoryToService')->name('admin.additional-services.category');
            Route::get('additional/services/category', 'getServiceCategory')->name('admin.get.additional-services.category');
        });


        // get all items
        Route::controller(ItemController::class)->group(function () {
            Route::get('items', 'index')->name('Admin.items');
            Route::post('items/store', 'store')->name('admin.items.store');
            Route::get('items/edit/{id}', 'edit')->name('admin.items.edit');
            Route::post('items/update/{id}', 'update')->name('admin.items.update');
        });

        // delivery fees controller
        Route::controller(DeliveryfeesController::class)->group(function () {
            Route::get('deliveryfees', 'index')->name('Admin.delivery_fees');   //
            Route::post('deliveryfees/update', 'update')->name('Admin.deliveryfees.update');
        });


        // Points Routes
        Route::controller(PointController::class)->group(function () {
            Route::get('points', 'index')->name('Admin.points');
            Route::put('points/update', 'update')->name('Admin.points.update');
        });


        // Periods Routes
        Route::controller(PeriodController::class)->group(function () {
            Route::get('periods', 'index')->name('Admin.periods');
            Route::post('periods/store', 'store')->name('Admin.periods.store');
            Route::put('periods/update', 'update')->name('Admin.periods.update');
            Route::delete('periods/delete/{period_id}', 'destroy')->name('Admin.periods.delete');
        });


        // Package Routes
        Route::controller(PackageController::class)->group(function () {
            Route::get('packages', 'index')->name('Admin.packages');
            Route::post('packages/store', 'store')->name('Admin.packages.store');
            Route::put('packages/update', 'update')->name('Admin.packages.update');
            Route::delete('packages/delete/{package_id}', 'destroy')->name('Admin.packages.delete');
            Route::get('packages/periods/{period_id}', 'getPackage')->name('Admin.packages.period');
            Route::get('ajax/packages/periods/{period_id}', 'getPackageAjax')->name('Admin.ajax.packages.period');
        });

        // user walletes

        Route::controller(WalletController::class)->group(function () {
            Route::get('user/wallet', 'index')->name('Admin.user.wallet');
            Route::post('add/free/wallet', 'addfreewallet')->name('Admin.user.addfreewallet');  //search
            Route::post('search/user/wallet', 'search')->name('Admin.user.wallet.search');
        });


        Route::controller(BalanceController::class)->group(function () {
            Route::get('balance/{type}/{date}', 'index')->name('Admin.balance.index');
            Route::post('balance/search', 'search')->name('Admin.transaction.search');
            Route::get('/transaction/view/{id}', 'view')->name('Admin.transaction.view');
        });

        //
        Route::controller(SupportController::class)->group(function () {

            Route::get('support', 'index')->name('Admin.support.index');
            Route::post('update/support/phone', 'update')->name('Admin.Support.updatephone');
        });

        Route::controller(ApplicationRatesController::class)->group(function () {
            Route::get('edit/application-rate', 'edit')->name('admin.application-rate.edit');
            Route::post('update/application-rate', 'update')->name('admin.application-rate.update');
        });


    }); // end of middleware

}); // end of prefix
