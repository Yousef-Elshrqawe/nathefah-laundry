<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\Auth\AuthController;
use App\Http\Controllers\User\AdressController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\LaundryController;
use App\Http\Controllers\User\NotificationController;
use App\Http\Controllers\User\SupportController;
use App\Http\Controllers\User\FilterController;
use App\Http\Controllers\User\DiscountController;
use App\Http\Controllers\User\WalletController;
use App\Http\Controllers\User\aboutcontroller;
use App\Http\Controllers\User\PaymentController;
use App\Http\Controllers\User\TermsandCondittionController;
use App\Http\Controllers\User\PaymentPayfortController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// View and Download Invoice
Route::get('/download/invoice/{order_id}',     [OrderController::class, 'downloadInvoice']);
Route::post('calculate/destination',           [OrderController::class, 'calculatDestination']);

// Discount Routes
Route::post('creatediscount',                  [DiscountController::class, 'creatediscount']);

// About Routes
Route::get('about',                            [aboutcontroller::class, 'index']);

// Terms and Conditions Routes
Route::get('TermsCondition',                   [TermsandCondittionController::class, 'termsindex']);
Route::get('privacypolicy',                    [TermsandCondittionController::class, 'policyindex']);

// Payment Routes
Route::post('get/sdktoken',                    [PaymentController::class, 'sdktoken']);
Route::post('save/paymeny/card',               [PaymentController::class, 'savepaymentcard']);
Route::get('get/paymeny/card',                 [PaymentController::class, 'getusercurds']);
Route::post('get/generateotp/',                [PaymentController::class, 'generateotd']);
Route::post('transaction/feedback/',           [PaymentController::class, 'transactionFeedback']);
Route::post('transaction/success',             [PaymentController::class, 'successtransaction']);

// Payment Payfort Routes
Route::post('test/get/sdktoken',               [PaymentPayfortController::class, 'sdktoken']);
Route::post('test/get/generateotp/',           [PaymentPayfortController::class, 'generateotd']);
Route::post('test/transaction/feedback/',      [PaymentPayfortController::class, 'transactionFeedback']);
Route::post('test/transaction/success',        [PaymentPayfortController::class, 'successtransaction']);

// Auth Routes
Route::post('/signin',                         [AuthController::class, 'signin']);
Route::post('register',                        [AuthController::class, 'register']);
Route::post('verifyphone',                     [AuthController::class, 'verifyphone']);
Route::post('checkphone',                      [AuthController::class, 'checkphone']);
Route::post('forgetpassword',                  [AuthController::class, 'forgetpassword']);
Route::post('signin/byfaceid',                 [AuthController::class, 'loginfaceid']);
Route::post('send/otp/',                       [AuthController::class, 'sendotp']);
Route::post('check/otp/',                      [AuthController::class, 'checkotp']);

// Filter Routes
Route::get('additionalservice',                [FilterController::class, 'getadditionalservice']);
Route::post('filter/laundry',                  [FilterController::class, 'filterlaundry']);

// Home Routes
Route::get('slider',                           [HomeController::class, 'getslider']);
Route::get('top/laundry/{lat?&lon?}',          [HomeController::class, 'toplaundries']);
Route::get('check/range/{lat?&lon?}',          [HomeController::class, 'checkrange']);
Route::post('home/search/laundry',             [HomeController::class, 'search']);

// Laundry Routes
Route::get('laundryinfo/{branch_id?&lat?&long?}', [LaundryController::class, 'laundryinfo']);
Route::get('get/laundries/{lat?&lon?}',           [LaundryController::class, 'getlaundries']);
Route::post('search/laundries',                   [LaundryController::class, 'search']);
Route::get('branch/reviews/{branch_id?}',         [LaundryController::class, 'laundryreviews']);

// Support Routes
Route::post('send/support',                    [SupportController::class, 'store']);
Route::get('get/support/phone',                [SupportController::class, 'getSupportPhone']);
Route::get('nmnm',                             [SupportController::class, 'nmnm']);

// Order Routes
Route::get('services',                                             [OrderController::class, 'getservices']);
Route::post('select/laundry/',                                     [OrderController::class, 'selectlaundry']);
Route::post('choose/laundry',                                      [OrderController::class, 'chooselaundry']);
Route::get('category/items/{service_id?&category_id?&branch_id?}', [OrderController::class, 'getcategoryitems']);
Route::get('items/detailes{item_id?}',                             [OrderController::class, 'itemdetailes']);
Route::get('laundry/service{branch_id?}',                          [OrderController::class, 'laundryservices']);

// Authenticated User Routes
Route::middleware('auth:sanctum')->group(function() {
    Route::post('logout',                      [AuthController::class, 'logout']);
    Route::post('store/faceid',                [AuthController::class, 'storefaceid']);

    Route::post('newadress',                   [AdressController::class, 'createadress']);
    Route::post('updateaddress',               [AdressController::class, 'updateaddress']);
    Route::get('delete/adress/{adress_id?}',   [AdressController::class, 'deleteadress']);

    Route::get('/edit/profile',                [ProfileController::class, 'edit']);
    Route::get('/edit/profile/phone',          [ProfileController::class, 'editphone']);
    Route::post('update/profile',              [ProfileController::class, 'update']);
    Route::post('profile/updatepassword',      [ProfileController::class, 'updatepassword']);
    Route::post('profile/updatephone',         [ProfileController::class, 'updatephone']);
    Route::post('verified/phone',              [ProfileController::class, 'verifyphone']);
    Route::get('user/adress',                  [ProfileController::class, 'getaddresses']);
    Route::get('user/changelang',              [ProfileController::class, 'changelang']);
    Route::post('mail/send/order',             [ProfileController::class, 'sendmailOrder']);

    Route::post('submitorder',                              [OrderController::class, 'submitorder']);
    Route::get('order/summary/{order_id?}',                 [OrderController::class, 'ordersummary']);
    Route::get('branch/open-time/{branch_id?}',             [OrderController::class, 'opentime']);
    Route::get('cancel/order/{order_id?}',                  [OrderController::class, 'cancelorder']);
    Route::get('after/checkout/{branch_id?&order_id?}',     [OrderController::class, 'branchinfo']);
    Route::post('checkout',                                 [OrderController::class, 'checkout']);
    Route::get('recive/order/info/{order_id?}',             [OrderController::class, 'reciveorderinfo']);
    Route::get('order/delivery/status/{order_id?}',         [OrderController::class, 'orderdeliverystatus']);
    Route::get('order/pick_up{order_id?&confirm_type?}',    [OrderController::class, 'reciveorder']);
    Route::get('active/order',                              [OrderController::class, 'activeorder']);
    Route::get('previous/order',                            [OrderController::class, 'previousorder']);
    Route::post('rate/order',                               [OrderController::class, 'rateorder']);
    Route::post('rate/delivery',                            [OrderController::class, 'deliveryrate']);
    Route::post('review/order',                             [OrderController::class, 'orderreview']);
    Route::get('get/order/code{order_id?}',                 [OrderController::class, 'getcode']);
    Route::get('ordercancelation/{order_id?}',              [OrderController::class, 'ordercancelation']);
    Route::get('edit/order/{order_id?}',                    [OrderController::class, 'edit']);
    Route::post('update/order',                             [OrderController::class, 'update']);
    Route::get('edit/before/checkout/{order_id?}',          [OrderController::class, 'edit_order_summary']);
    Route::post('edit/checkout',                            [OrderController::class, 'editcheckout']);
    Route::get('cancel/edit/order/{order_id?}',             [OrderController::class, 'canceleditorder']);

    Route::get('setting/get/notification',     [NotificationController::class, 'getnotificationsetting']);
    Route::post('update/notification',         [NotificationController::class, 'updatenotificationsetting']);
    Route::post('updatetoken',                 [NotificationController::class, 'updatetoken']);
    Route::get('/getnotification',             [NotificationController::class, 'getnotification']);

    Route::get('get/user/discount',            [DiscountController::class, 'getdiscount']);
    Route::post('use/discount',                [DiscountController::class, 'usediscount']);

    Route::get('get/wallet',                   [WalletController::class, 'getpallence']);
    Route::post('charge/wallet',               [WalletController::class, 'charge']);
    Route::post('points/wallet',               [WalletController::class, 'convertPointsToWallet']);
    Route::get('get/points',                   [WalletController::class, 'getpoints']);
});

?>
