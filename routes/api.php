<?php

use App\Http\Controllers\Api\ForceUpdateController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\User\Auth\AuthController;
use App\Http\Controllers\User\AdressController;
use App\Http\Controllers\User\OrderController;
use App\Http\Controllers\User\ProfileController;
use App\Http\Controllers\User\HomeController;
use App\Http\Controllers\User\LaundryController;
use App\Http\Controllers\User\NotificationController;
use App\Http\Controllers\User\SupportController;
use App\Http\Controllers\User\FilterController;
use App\Http\Controllers\User\DiscountController;
use App\Http\Controllers\User\WalletController;
use App\Http\Controllers\User\aboutcontroller;
use App\Http\Controllers\User\PaymentController;  //
use App\Http\Controllers\User\TermsandCondittionController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('force/update',[ForceUpdateController::class,'checkVersion']);
Route::get('terms/condition',[ForceUpdateController::class,'termsCondition']);
Route::get('user/terms/condition',[ForceUpdateController::class,'userTerms'])->middleware('userApiAuth');
Route::get('user/is-read/terms/condition',[ForceUpdateController::class,'userTermsIsRead'])->middleware('userApiAuth');
Route::post('send/notification', [ProfileController::class, 'sendnotification'] );

//view invoice

//generateInvoice
//downloadInvoice
Route::get('/download/invoice/{order_id}', [OrderController::class, 'downloadInvoice']);
Route::post('calculate/destination',[OrderController::class,'calculatDestination']);

Route::controller(DiscountController::class)->group(function () {  //
    Route::POST('creatediscount','creatediscount');
 });
 Route::controller(aboutcontroller::class)->group(function(){
    Route::get('about','index');
});

Route::controller(TermsandCondittionController::class)->group(function (){
   Route::get('TermsCondition','termsindex');
   Route::get('privacypolicy','policyindex');
});

Route::controller(PaymentController::class)->group(function () {  //
    Route::Post('get/sdktoken','sdktoken');
    Route::Post('save/paymeny/card','savepaymentcard');
    Route::get('get/paymeny/card','getusercurds');

    Route::get('get/payment/branch/method/{id}','getbranchpaymentmethod');


    Route::get('delete/payment/card/{id}','deleteCard')->middleware('userApiAuth');
    //addFavoriteCard
    Route::post('add/favorite/card','addFavoriteCard')->middleware('userApiAuth');

    // this for payment stcp
    Route::Post('get/generateotp/','generateotd');
    Route::post('transaction/feedback/','transactionFeedback');

     Route::post('transaction/success','successtransaction');
});

Route::controller(\App\Http\Controllers\User\PaymentPayfortController::class)->group(function () {
    Route::Post('test/get/sdktoken','sdktoken');
    Route::Post('test/get/generateotp/','generateotd');
    Route::post('test/transaction/feedback/','transactionFeedback');
    Route::post('test/transaction/success','successtransaction');

});
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::controller(AuthController::class)->group(function () {
    Route::POST('/signin','signin');
    Route::POST('redister','register');
    Route::POST('verifyphone','verifyphone');
    Route::POST('checkphone','checkphone');
    Route::POST('forgetpassword','forgetpassword');
    Route::POST('signin/byfaceid','loginfaceid');  //
    Route::post('send/otp/','sendotp');
    Route::post('check/otp/','checkotp');
});
Route::controller(FilterController::class)->group(function () {  // test
   Route::get('additionalservice','getadditionalservice');
   Route::POST('filter/laundry','filterlaundry');
});
Route::controller(HomeController::class)->group(function () {
   Route::get('slider','getslider');
   Route::get('top/laundry/{lat?&lon?}','toplaundries');
   Route::get('check/range/{lat?&lon?}','checkrange');
   Route::post('home/search/laundry','search');
});
Route::controller(LaundryController::class)->group(function () {
    Route::get('laundryinfo/{branch_id?&lat?&long?}','laundryinfo');
    Route::get('get/laundries/{lat?&lon?}','getlaundries');
    Route::post('search/laundries','search');
    Route::get('branch/reviews/{branch_id?}','laundryreviews');
 });
 Route::controller(SupportController::class)->group(function () {
    Route::post('send/support','store');
    Route::get('get/support/phone','getSupportPhone');
     Route::get('nmnm' , 'nmnm');
//     Route::get('laundry/service{branch_id?}','laundryservices');
 });
Route::controller(OrderController::class)->group(function () {
  //order  signin/byfaceid
  Route::get('services','getservices');
  Route::Post('select/laundry/','selectlaundry');
  Route::POST('choose/laundry','chooselaundry');
  Route::get('category/items/{service_id?&category_id?&branch_id?}','getcategoryitems');
  Route::get('items/detailes{item_id?}','itemdetailes');
//  Route::get('laundry/services/{id?}','laundryService');
  Route::get('laundry/service{branch_id?}','laundryservices');

});
Route::group(['middleware' => 'userApiAuth'],function(){
    Route::controller(AuthController::class)->group(function () {
        Route::POST('logout','logout');
        Route::POST('store/faceid','storefaceid');
    });
    Route::controller(AdressController::class)->group(function () {
        Route::POST('newadress','createadress');
        Route::POST('updateaddress','updateaddress');
        Route::get('delete/adress/{adress_id?}','deleteadress');
    });
    Route::controller(ProfileController::class)->group(function () { //
        Route::get('/edit/profile','edit');
        Route::get('/edit/profile/phone','editphone');
        Route::post('update/profile','update');
        Route::post('profile/updatepassword','updatepassword');
        Route::post('profile/updatephone','updatephone');
        Route::post('verified/phone','verifyphone');
        Route::get('user/adress','getaddresses');
        Route::get('user/changelang','changelang');
        //delete user
        Route::post('delete/user','deleteaccount');
//        Route::post('send/mail/order','sendmailOrder');


    });



    Route::post('mail/send/order', [ProfileController::class, 'sendmailOrder'] );

//    Route::post('send/mail', [OrderController::class, 'sendmail'] );



    Route::controller(OrderController::class)->group(function () {
        //sendmail
//        Route::post('send/mail/order','sendmailOrder');
        //calculatDestination

        Route::POST('submitorder','submitorder');
        // this api after check order
        Route::get('order/summary/{order_id?}','ordersummary');
        Route::get('branch/open-time/{branch_id?}','opentime');
        Route::get('cancel/order/{order_id?}','cancelorder');
        Route::get('after/checkout/{branch_id?&order_id?}','branchinfo');
        Route::POST('checkout','checkout');
        //this requist after scan qr code
        Route::get('recive/order/info/{order_id?}','reciveorderinfo');
        Route::get('order/delivery/status/{order_id?}','orderdeliverystatus');
        Route::get('order/pick_up{order_id?&confirm_type?}','reciveorder');
        Route::get('active/order','activeorder');
        Route::get('previous/order','previousorder');
        Route::Post('rate/order','rateorder');
        Route::Post('rate/delivery','deliveryrate');
        Route::Post('review/order','orderreview');
        Route::get('get/order/code{order_id?}','getcode');
        Route::get('ordercancelation/{order_id?}','ordercancelation');
        // edit order cycle
        // Route::get('branch/serivices/{branch_id?}','branchservices');
        // Route::get('branch/category/items/{order_id?&category_id,&service_id}','categoryitems');
        // Route::get('edit/branchitem/{branchitem_id?&order_id?}','getitemdetailes');
        Route::get('edit/order/{order_id?}','edit');
        Route::post('update/order','update');
        Route::get('edit/before/checkout/{order_id?}','edit_order_summary');  //canceleditorder
        Route::post('edit/checkout','editcheckout');
        Route::get('cancel/edit/order/{order_id?}','canceleditorder');
    });
    Route::controller(NotificationController::class)->group(function () {
        Route::get('setting/get/notification','getnotificationsetting');
        Route::Post('update/notification','updatenotificationsetting');
        Route::post('updatetoken','updatetoken');
        Route::get('/getnotification','getnotification');
    });
    //
    Route::controller(DiscountController::class)->group(function () {  //
        Route::get('get/user/discount','getdiscount');
        Route::Post('use/discount','usediscount');
     });
     Route::controller(WalletController::class)->group(function () {  //
        Route::get('get/wallet','getpallence');
        Route::Post('charge/wallet','charge');
        Route::post('points/wallet','convertPointsToWallet');
        Route::get('get/points','getpoints');
     });




});
