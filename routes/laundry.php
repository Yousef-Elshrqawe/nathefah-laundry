<?php

use App\Http\Controllers\Admin\laundry\OrderController;
use App\Http\Controllers\Admin\laundry\ServiceController;
use App\Http\Controllers\Admin\laundry\BranchController;
use App\Http\Controllers\Admin\laundry\dashboardController;
use App\Http\Controllers\Admin\laundry\DriverController;
use App\Http\Controllers\Admin\laundry\SettingController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\laundry\AuthController;
use App\Http\Controllers\Admin\laundry\DeliveryfeesController;
use App\Http\Controllers\Admin\laundry\TaxesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('laundry-admin')->group(function () {
    Route::controller(AuthController::class)->group(function () {
        Route::get('login','loginview')->name('laundry.login');
        Route::Post('login','login')->name('laundry.login.submit');
    });

    Route::middleware(['laundryAuth'])->group(function () {

        Route::controller(AuthController::class)->group(function () {
            Route::get('logout','logout')->name('laundry.logout');
        });
        Route::controller(dashboardController::class)->group(function () {
            Route::get('/home/{week}','home')->name('laundry.home');
        });

        ######################## START orders ############################
        Route::controller(OrderController::class)->group(function () {
            Route::get('orders/{type}/{date}/{reviewdate}','index')->name('laundry.orders');
            Route::post('submit','submit')->name('laundry.submit.order');
            Route::get('latest/orders','latestOrder')->name('laundry.latest.orders');
            Route::get('all/order','viewAll')->name('laundry.allorder');
            Route::get('order/view/{id}','view')->name('laundry.order.view');
            Route::post('confirm/order','confirmorder')->name('laundry.confirm.order');
            Route::get('finish/order/{order_id}','finishOrder')->name('laundry.finish.order');
            Route::get('order/accept/order/{order_id}','acceptOrder')->name('laundry.accept.order');
            Route::get('assign/order/view/{order_id}','assignOrderReview')->name('laundry.assignorderview');
            Route::get('assign/driver/{driver_id}/{order_id}','assignDriver')->name('laundry.assign.driver');
            Route::get('get/qr_code/{order_id}','generate_qr_code')->name('laundry.generate_qr_code');
            Route::get('recive/order/{order_id?}','receiveOrder')->name('laundry.recive.order');
            Route::get('deliver/order/{id}','deliverOrder')->name('laundry.deliver.order');

            //Route::post('submit/order','stote')->name('branch.order.submit');
        });
        ########################    End orders ############################

        ######################## START BRANCHES ############################
        Route::controller(BranchController::class)->group(function () {
            Route::get('laundry/branches','index')->name('laundry.branches');
            Route::post('laundry/branches/store','store')->name('laundry.branches.store');
            Route::get('laundry/branches/edit','edit')->name('laundry.branches.edit');
            Route::put('laundry/branches/update','update')->name('laundry.branches.update');
            Route::delete('laundry/branches/delete/{branch_id}','destroy')->name('laundry.branches.destroy');
            Route::get('laundry/branches/reports','reports')->name('laundry.branches.reports');

            Route::put('laundry/branches/branch-details/update/{branch_id}','branchDetails')->name('laundry.branch-details-update');
            Route::get('laundry/branches/branch-details/{branch_id}','branchDetails')->name('laundry.branch-details');
        });
        ########################  END BRANCHES  ############################

        ######################## START DRIVERS ############################
        Route::controller(DriverController::class)->group(function () {
            Route::get('laundry/drivers','index')->name('laundry.drivers');
            Route::post('laundry/store/drivers','store')->name('laundry.store.drivers');
            //selectSearch
            Route::get('ajax-autocomplete-search','selectSearch')->name('laundry.drivers.selectSearch');
            Route::get('laundry/branch/drivers','getDriversInBranch')->name('laundry.branch.drivers');
            Route::get('laundry/branch/top-drivers','getTopDriversInBranch')->name('laundry.branch.top-drivers');
            Route::get('laundry/driver/details/{driver_id}','driverDetails')->name('laundry.driver.details');
        });
        ########################  END DRIVERS  ############################

        ######################## START SETTING ############################
        Route::controller(SettingController::class)->group(function () {
            Route::get('setting','index')->name('laundry.setting');
            //Basic Info
            Route::post('laundry/setting/basic-info/update/{laundry_id}','updateInfo')->name('laundry.setting.basic-info.update');
//            Route::delete('setting/basic-info/rest/{branch_id}','restInfo')->name('branch.setting.basic-info.rest');
        });
        ########################  END SETTING  ############################

        ######################## START Services ############################
        Route::controller(ServiceController::class)->group(function(){
            Route::get('services','index')->name('laundry.services');
            Route::get('service/edit','edit')->name('laundry.service.edit');
            Route::post('service/update','update')->name('laundry.service.update');
            Route::get('get/branch-item/{id}','getItem');
            Route::post('edit-item','editItem')->name('laundry.edit.item');
            Route::post('add/item','addItem')->name('laundry.add.item');
            Route::put('service/change-status','changeStatus')->name('laundry.service.change.status');
            Route::put('service/item/change-status','changeItemStatus')->name('laundry.service.item.change.status');
            Route::put('service/item/argent-price','changeArgentPrice')->name('laundry.service.item.argent.price');
        });
        ######################## End Services ############################

       ######################### start delivery fees #####################
       Route::controller(DeliveryfeesController::class)->group(function(){
        Route::get('deliveryfees','index')->name('laundry.delivery_fees');   //
        Route::post('deliveryfees/update','update')->name('laundry.deliveryfees.update');
       });
       ########################  end deliveryfees #########################

       #######################   start taxes      #########################
       Route::controller(TaxesController::class)->group(function(){
        Route::get('taxes','index')->name('laundry.taxes');   //
        Route::post('update/taxes','update')->name('laundry.update.taxes');
        Route::get('/taxes/status','updatestatus')->name('laundry.status.taxes');
       });


       ######################   end taxes        ###########################



       });
});
