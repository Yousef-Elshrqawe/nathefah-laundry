<?php

use App\Http\Controllers\Admin\branch\DriverController;
use App\Http\Controllers\Admin\branch\RoleController;
use App\Http\Controllers\Admin\branch\SettingController;
use App\Http\Controllers\branch\DateController;
use App\Http\Controllers\branch\ExpectedTimeController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\branch\AuthController;
use App\Http\Controllers\Admin\branch\dashboardcontroller;
use App\Http\Controllers\Admin\branch\OrderController;
use App\Http\Controllers\Admin\branch\NotificationController;
use App\Http\Controllers\Admin\branch\ServiceController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::prefix('branch-admin')->group(function () {
    Route::post('confirm/order', [OrderController::class, 'confirmorder'])->name('branch.confirm.order');
    Route::post('submit', [OrderController::class, 'submit'])->name('branch.submit.order');
    Route::controller(OrderController::class)->group(function () {
        Route::get('orders/{type}/{date}/{reviewdate}', 'index')->name('branch.orders');
        Route::get('latest/orders', 'latestorder')->name('branch.latest.orders');
        Route::get('all/order', 'viewall')->name('branch.allorder');
        Route::get('order/view/{id}', 'view')->name('branch.order.view');
        Route::get('finish/order/{order_id}', 'finishorder')->name('branch.finish.order');
        Route::get('order/accept/order/{order_id}', 'acceptorder')->name('branch.accept.order');
        Route::get('assign/order/view/{order_id}', 'assignorderview')->name('branch.assignorderview');
        Route::get('assign/driver/{driver_id}/{order_id}', 'assgindriver')->name('branch.assign.driver');
        Route::get('get/qr_code/{order_id}', 'generate_qr_code')->name('branch.generate_qr_code');
        Route::get('recive/order/{order_id?}', 'reciveorder')->name('branch.recive.order');
        Route::get('deliver/order/{id}', 'deliverOrder')->name('branch.deliver.order');

        Route::get('reject/order/{order_id?}', 'rejectorder')->name('branch.reject.order');
        //Route::post('submit/order','stote')->name('branch.order.submit');
    });

    Route::controller(AuthController::class)->group(function () {
        Route::get('login', 'loginview')->name('branch.login');
        Route::Post('login', 'login')->name('branch.login.submit');
    });
    //
    Route::middleware(['brancAuth'])->group(function () {
        Route::controller(AuthController::class)->group(function () {
            Route::get('logout', 'logout')->name('branch.logout');
        });
        Route::controller(dashboardController::class)->group(function () {
            Route::get('home/{week}', 'home')->name('branch.home');
            // Route::controller(dashboardcontroller::class)->group(function () {
            //     Route::get('home','home')->name('branch.home');
            // });

            ######################## START orders ############################

            ########################    End orders ############################

            ######################## START Services ############################
            Route::controller(ServiceController::class)->group(function () {
                Route::get('services', 'index')->name('branch.services');
                Route::get('service/edit', 'edit')->name('branch.service.edit');
                Route::post('service/update', 'update')->name('branch.service.update');
                Route::get('get/branchitem/{id}', 'getitem');
                Route::post('edititem', 'edititem')->name('branch.edit.item');
                Route::post('add/item', 'additem')->name('branch.add.item');
                Route::put('service/change-status', 'changeStatus')->name('service.change.status');
                Route::put('service/item/change-status', 'changeItemStatus')->name('service.item.change.status');
                Route::put('service/item/argent-price', 'changeArgentPrice')->name('service.item.argent.price');
                Route::put('service/change-status/argent-price', 'changeArgentPriceStatus')->name('service.item.argent.price-change-status');
            });
            ######################## End Services ############################
            Route::controller(ExpectedTimeController::class)->group(function () {
                Route::get('expected-time', 'index')->name('branch.expected.time');
                Route::get('expected-time/edit/{id}', 'edit')->name('branch.edit.expected.time');
                Route::post('expected-time/update/{id}', 'update')->name('branch.update.expected.time');
            });



            ######################## START Notofocation ############################
            Route::controller(NotificationController::class)->group(function () {
                Route::get('get/notification', 'index')->name('branch.get.notification');
            });
            ########################    End Notofocation ############################
            ######################## START DRIVERS ############################
            Route::controller(DriverController::class)->group(function () {
                Route::get('drivers/{driverlist}/{driverreport}', 'index')->name('branch.drivers');
                Route::post('add/driver', 'store')->name('branch.store.driver');
                Route::get('driver/details/{driver_id}', 'driverDetails')->name('branch.driver.details');
                Route::put('driver/edit', 'update')->name('branch.edit.driver');
            });
            ########################  END DRIVERS  ############################


            ######################## START SETTING ############################
            Route::controller(SettingController::class)->group(function () {
                Route::get('setting', 'index')->name('branch.setting');
                //Basic Info
                Route::post('setting/basic-info/update/{branch_id}', 'updateInfo')->name('branch.setting.basic-info.update');
                Route::delete('setting/basic-info/rest/{branch_id}', 'restInfo')->name('branch.setting.basic-info.rest');
                //Permissions
                Route::post('setting/permissions/add-user', 'storeUserInBranch')->name('branch.setting.permissions-store.user');
                Route::get('setting/permissions/edit-user', 'editUserInBranch')->name('branch.setting.permissions-edit.user');
                Route::put('setting/permissions/update-user', 'updateUserInBranch')->name('branch.setting.permissions-update.user');
                Route::delete('setting/permissions/delete/{user_id}', 'deleteUserInBranch')->name('branch.setting.permissions-delete.user');
                //Notifications
                Route::post('setting/notification/add-delete', 'addDeleteNotification')->name('branch.setting.notification.add.delete');
                Route::delete('setting/notification/rest', 'restNotifications')->name('branch.setting.notification.rest');
                //Branch Details
                Route::get('setting/branch-details', 'branchDetails')->name('setting.branch.details');

            });
            ########################  END SETTING  ############################
            ######################## START SETTING ############################
            Route::controller(RoleController::class)->group(function () {
                Route::post('role/store', 'store')->name('branch.setting.new.role');
                Route::get('role/edit', 'edit')->name('branch.role.edit');
                Route::put('role/update', 'update')->name('branch.role.update');
                Route::delete('role/delete/{role_id}', 'destroy')->name('branch.role.delete');
            });
            ########################  END SETTING  ############################
            Route::get('dates/{branch_id?}', [DateController::class, 'index'])->name('dates.index');
            //branch.dates.show
            Route::get('/dates/show/{id}/{branch_id?}', [DateController::class, 'show'])->name('branch.dates.show');
            //branch.edit.schedule
            Route::get('/dates/edit/{id}/{branch_id?}', [DateController::class, 'edit'])->name('branch.edit.schedule');
            //branch.update.schedule
            Route::post('/dates/update/{id}/{branch_id?}', [DateController::class, 'update'])->name('branch.update.schedule');
            //branch.delete.schedule
            Route::get('/dates/delete/{id}/{branch_id?}', [DateController::class, 'delete'])->name('branch.delete.schedule');
            //branch.store.schedule
            Route::post('/dates/store/{branch_id?}', [DateController::class, 'store'])->name('branch.store.schedule');


            //transactions
            Route::get('transactions', [OrderController::class, 'transactions'])->name('branch.transactions');


        });
        // end middleware
    });
    // end perfix
});
