<?php

 /*
    |--------------------------------------------------------------------------
    | response Language Lines
    |--------------------------------------------------------------------------
    */

return [

    'wrong'                 =>   'some thing error we will fix it as soon as possible',
    'invoice_header'        =>   'elctronic invoice from nazifaa',
    'Branch not found'   =>   'Branch not found',
    'Schedules retrieved successfully' => 'Schedules retrieved successfully',
    'This time slot is already taken for this day.' => 'This time slot is already taken for this day.',
    'Schedule added successfully' => 'Schedule added successfully',
    'Schedule not found' => 'Schedule not found',
    'Schedule deactivated successfully'  => 'Schedule deactivated successfully',
    'Schedule activated successfully'    => 'Schedule activated successfully',
    'Force update not found'             => 'Force update not found',
    'Force update retrieved successfully' => 'Force update retrieved successfully',
    'Terms and conditions retrieved successfully' => 'Terms and conditions retrieved successfully',
    'User  not found' => 'User  not found',
    'User terms retrieved successfully' => 'User terms retrieved successfully',

    'this discount not available' => 'this discount not available',
    'you already used this discount' => 'you already used this discount',
    'min_order_price' => 'The order amount must be greater than :min_order SAR to use the code.',

];
