<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'password' => 'The provided password is incorrect.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'Your Nathefah verification code is' => 'Your Nathefah verification code is :',
    'Your request has been received successfully' => 'your order is received from laundry ',
    'Your request has been canceled successfully' => 'Order cancellation : Your order has been cancelled.',
    'Your request has been modified successfully' => 'Your request has been modified successfully',
    'Your order has been canceled and the amount has been refunded to you  order number :' => 'Your order has been cancelled and the amount will be refunded to your account within a couple days.  order number :',
    'your order is accepted' => 'your order is accepted',
    'your order is rejected' => 'your order is rejected',
    'please go to laundry to receive your order' => 'please go to laundry to receive your order',
    'Driver is on his way to you location' => 'The driver is on the way to you ',
    'your order is received from laundry' => 'your order is received from laundry',
    'The driver received your order from the laundry' => 'The driver has received your order from the laundry',

];
