<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Order placed' => 'Order placed 🛒',
    'indelivery' => 'In delivery 🚚',
    'inprogress' => 'In progress ⏳',
    'finished' => 'Ready for delivery 📦',
    'completed' => 'Completed ✔',
    'no_progress' => 'No progress 🚫',
];
