<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Your password has been reset!',
    'sent' => 'We have emailed your password reset link!',
    'throttled' => 'Please wait before retrying.',
    'token' => 'This password reset token is invalid.',
    'user' => "We can't find a user with that email address.",
    'The specified password does not match the old password' => 'The specified password does not match the old password',
    'branch password updated successfully' => 'branch password updated successfully',
    'please send otp in the next request with phone number' => 'please send otp in the next request with phone number',
    'this number is already exist' => 'this number is already exist',
    'phone updated successfully' => 'phone updated successfully',


];
