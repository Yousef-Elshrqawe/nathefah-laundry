<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Order placed' => 'تم  الطلب 🛒',
    'indelivery' => 'في التوصيل 🚚',
    'inprogress' => 'قيد التنفيذ ⏳',
    'finished' => 'تم الانتهاء 📦',
    'completed' => 'تم الانتهاء ✔',
    'no_progress' => ' لا يوجد تقدم🚫',
];
