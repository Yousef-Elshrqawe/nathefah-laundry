<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'بيانات الاعتماد هذه غير متطابقة مع البيانات المسجلة لدينا.',
    'throttle' => 'عدد كبير جدا من محاولات الدخول. يرجى المحاولة مرة أخرى بعد :seconds ثانية.',
    'Your Nathefah verification code is' => '  رمز التحقق الخاص بك هو :',
    'Your request has been received successfully' => 'تم استلام طلبك من المغسلة',
    'Your request has been canceled successfully' => 'لقد تم الغاء طلبك',
    'Your request has been modified successfully' => 'تم تعديل طلبك بنجاح',
    'Your order has been canceled and the amount has been refunded to you  order number :' => ' تم إلغاء طلبك وسيتم رد المبلغ إلى حسابك خلال بضعة أيام. رقم الطلب: ',
    'your order is accepted' => 'تم قبول طلبك',
    'your order is rejected' => 'تم رفض طلبك',
    'please go to laundry to receive your order' => 'يرجى الذهاب إلى المغسلة لاستلام طلبك',
    'Driver is on his way to you location' => 'السائق في الطريق إليك',
    'your order is received from laundry' => 'تم استلام طلبك من المغسلة',
    'The driver received your order from the laundry' => 'لقد استلم السائق طلبك من المغسلة',
];
