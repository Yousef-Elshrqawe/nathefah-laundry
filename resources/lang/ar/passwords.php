<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'يجب أن لا يقل طول كلمة المرور عن ستة أحرف، كما يجب أن تتطابق مع حقل التأكيد.',
    'reset'    => 'تمت إعادة تعيين كلمة المرور!',
    'sent'     => 'تم إرسال تفاصيل استعادة كلمة المرور الخاصة بك إلى بريدك الإلكتروني!',
    'token'    => 'رمز استعادة كلمة المرور الذي أدخلته غير صحيح.',
    'user'     => 'لم يتم العثور على أيّ حسابٍ بهذا العنوان الإلكتروني.',
    'The specified password does not match the old password' => 'كلمة المرور المحددة لا تتطابق مع كلمة المرور القديمة',
    'branch password updated successfully' => 'تم تحديث كلمة مرور الفرع بنجاح',
    'please send otp in the next request with phone number' => 'يرجى إرسال رمز التحقق في الطلب التالي مع رقم الهاتف',
    'this number is already exist' => 'هذا الرقم موجود بالفعل',
    'otp is wrong or phone' => 'رمز التحقق غير صحيح أو الهاتف',
    'phone updated successfully' => 'تم تحديث رقم الهاتف بنجاح',
];
