<?php

 /*
    |--------------------------------------------------------------------------
    | response Language Lines
    |--------------------------------------------------------------------------
    */

return [

    'wrong'              =>   'هناك خطاء ما جاري اصلاحه',
    'invoice_header'     =>   'فاتورتك الالكترونيه من نظيفه',
    'Branch not found'   =>   'الفرع غير موجود',
    'Schedules retrieved successfully' => 'تم استرجاع المواعيد بنجاح',
    'This time slot is already taken for this day.' => 'هذا الوقت محجوز بالفعل لهذا اليوم',
    'Schedule added successfully' => 'تمت اضافة الموعد بنجاح',
    'Schedule not found' => 'الموعد غير موجود',
    'Schedule deactivated successfully' => 'تم تعطيل الموعد بنجاح',
    'Schedule activated successfully'  => 'تم تفعيل الموعد بنجاح',
    'Days retrieved successfully'      => 'تم استرجاع الايام بنجاح',
    'Schedules retrieved successfully' => 'تم استرجاع المواعيد بنجاح',
    'Schedules retrieved successfully' => 'تم استرجاع المواعيد بنجاح',
    'Force update not found'           => 'لا يوجد تحديث',
    'Force update retrieved successfully' => 'تم استرجاع التحديث بنجاح',
    'Terms and conditions retrieved successfully' => 'تم استرجاع الشروط والاحكام بنجاح',
    'User  not found' => 'المستخدم غير موجود',
    'User terms retrieved successfully' => 'تم استرجاع شروط المستخدم بنجاح',

    'this discount not available' => 'هذا الخصم غير متاح',
    'you already used this discount' => 'لقد استخدمت هذا الخصم بالفعل',
    'min_order_price' => 'يجب أن تكون قيمة الطلب أكبر من :min_order ريال لاستخدام الكود',


];
