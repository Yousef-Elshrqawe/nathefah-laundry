<?php
$active_links = ['blogs' , 'article'];
?>
@extends('layouts.admin')
@section('content')

    <div class="app-content content">
        <div class="content-wrapper">
            <div class="content-header row">
                <div class="content-header-left col-md-6 col-12 mb-2">
                    {{-- <h3 class="content-header-title"> الافكار </h3> --}}
                    <div class="row breadcrumbs-top">
                        <div class="breadcrumb-wrapper col-12">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{route('admin.dashboard')}}">@lang('dashboard.main')</a>
                                </li>
                                <li class="breadcrumb-item active"> @lang('dashboard.article')
                                </li>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="content-body">
                <!-- DOM - jQuery events table -->
                <section id="dom">
                    <div class="row">
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <a class="heading-elements-toggle"><i
                                            class="la la-ellipsis-v font-medium-3"></i></a>
                                    <div class="heading-elements">
                                        <ul class="list-inline mb-0">
                                            <li><a data-action="collapse"><i class="ft-minus"></i></a></li>
                                            <li><a data-action="reload"><i class="ft-rotate-cw"></i></a></li>
                                            <li><a data-action="expand"><i class="ft-maximize"></i></a></li>
                                            <li><a data-action="close"><i class="ft-x"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                @include('dashboard.includes.alerts.success')
                                @include('dashboard.includes.alerts.errors')

                                <div class="card-content collapse show">
                                    <div class="card-body card-dashboard">
                                        <table style="width:100%" class="table text-center display nowrap table-striped table-bordered scroll-horizontal">
                                            <form action="#" method="get">
                                                <div class="row">
                                                    <div class="col-md-2">
                                                        <input type="text" name="search" class="form-control" value="{{ request()->search }}"
                                                               placeholder="@lang('user.search')">
                                                    </div>
                                                    <div class="col-md-4 mb-4">
                                                        <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i>@lang('user.search')</button>
                                                        <a href='{{route('admin.article.create')}}' type="button" class="btn btn-success mr-1">@lang('dashboard.add_article')</a>
                                                    </div>
                                                </div>
                                            </form>
                                            <thead >

                                            <tr>

                                                <th style="width:2%">#</th>
                                                <th >@lang('dashboard.title') </th>

                                                <th style="width:40%">@lang('dashboard.image') </th>
                                                <th> @lang('dashboard.operations')</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            {{-- @if(count($blogs) != 0) --}}
                                                @foreach($post as $key => $item)
                                                    <tr>

                                                        <td>{{$key +1}}</td>
                                                        <td>{{$item ->title}}</td>
                                                        <td><img class="img-thumbnail" width="30%" height="30%" src="{{$item->image_path}}"></td>
                                                        <td>
                                                            <div class="btn-group" role="group"
                                                                 aria-label="Basic example">
                                                                <a href="{{route('admin.article.edit', $item->id)}}"
                                                                   class="btn btn-outline-primary btn-min-width box-shadow-3 mr-1 mb-1">@lang('dashboard.edit')</a>

                                                                   <form action="{{route('admin.article.destroy',$item->id)}}" method="post" enctype="multipart/form-data">
                                                                    @csrf
                                                                    @method('DELETE')
                                                                    <input type="hidden" name="id" value="{{$item->id}}"/>
                                                                    <button type="submit" class="btn btn-outline-danger btn-min-width box-shadow-3 mr-1 mb-1">@lang('dashboard.delete')</button>

                                                                </form>

                                                            <input class="form-check-input dispaly_blog" type="checkbox" id="flexSwitchCheckDefault" @if($item->displaying==1)  checked  @endif>

                                                            </div>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            {{-- @else --}}
                                            {{-- <div>@lang('dashboard.no_records')</div> --}}
                                            {{-- @endif --}}
                                            </tbody>
                                        </table>
                                        <div class="justify-content-center d-flex">
                                            {!! $post->appends(Request::except('page'))->render() !!}
                                        </div>
                                        <div class="justify-content-center d-flex">
{{--                                            {{ $departs->links() }}--}}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

@stop
@section('script')
<script>
var SITEURL = "{{ url('/ar/admin') }}";
$('.dispaly_blog').click(function(){

    $.ajax({
        url:SITEURL+"/display/"+id,
        type:"GET", //send it through get method
        success: function (response) {
            console.log('update')
        },
        error: function(response) {
            console.log('fdfd');
        }
        });

});

</script>

@endsection
