@extends('branch_temp')
@section('content')
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="page-topbar">
                        <div class="overview-box">
                            <div class="row align-items-center flex-column-reverse flex-xl-row">
                                <div class="col-xl-4 col-md-12">
                                    <div class="top-bar-title">
                                        <h5>Welcome  {{Session::get('username')}} </h5>
                                        <p class="cl-gray">Here is what happened in the branches today</p>
                                    </div>
                                </div>


                                <div class="col-xl-4 col-md-12">
                                    <div class="notify-box">
                                        {{--  <div class="notify-bell">
                                              <p><a href="{{route('branch.get.notification')}}"><i class="fal fa-bell"></i>Notifications</a></p>
                                          </div>--}}
                                        <div>
                                            <p>{{now()->format('l  m-d')}}</p>
                                        </div>
                                        <div>
                                            <a href="{{route('branch.logout')}}">log out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-8 col-md-12">
                    <div class="whitebox-panel">
                        <div class="backbtn-title ">
                            <h5 class="mb-0"><a href="#"></a> Notifications</h5>
                        </div>
                        @foreach ($notifications as $notification)
                            <div class="delivery-box">
                                <div class="delivry-content">
                                    @if ($notification->translations->first()!=null)
                                    <h6 class="active">{{$notification->translations->first()->title}}</h6>
                                    @endif
                                    @if ($notification->translations->first()!=null)
                                    <p class="cl-gray">{{$notification->translations->first()->body}}</p>                                    @endif
                                </div>
                                <div class="delivry-content text-right">
                                    <span class="cl-gray">{{date($notification->created_at)}}</span>
                                    {{-- <a href="#" class="cl-light">Mark as read</a> --}}
                                </div>
                            </div>
                        @endforeach
                        <div class="justify-content-center d-flex">
                            {!! $notifications->appends(Request::except('page'))->render() !!}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
@section('scripts')
    <!-- <script>
        $(window).scroll(function(){
          var sticky = $('.page-topbar'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixedhead');
          else sticky.removeClass('fixedhead');
        });
    </script> -->


    <script>
        $(document).ready(function(){

            $("#last-btn").click(function(){
                $("#progressbar").addClass("hide");
            });

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var current = 1;
            var steps = $("fieldset").length;

            setProgressBar(current);

            $(".next").click(function(){

                //current_fs = $(this).parent();
                // next_fs = $(this).parent().next();
                current_fs = $(this).closest("fieldset");
                next_fs = $(this).closest("fieldset").next();
                //alert(next_fs);
                //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the next <fieldset></fieldset>
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({'opacity': opacity});
                    },
                    duration: 500
                });
                setProgressBar(++current);
            });

            $(".previous").click(function(){

                current_fs = $(this).closest("fieldset");
                previous_fs = $(this).closest("fieldset").prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({opacity: 0}, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({'opacity': opacity});
                    },
                    duration: 500
                });
                setProgressBar(--current);
            });

            function setProgressBar(curStep){
                var percent = parseFloat(100 / steps) * curStep;
                percent = percent.toFixed();
                $(".progress-bar")
                    .css("width",percent+"%")
            }

            $(".submit").click(function(){
                return false;
            })

        });
    </script>

    <script>
        $(document).ready(function() {
            $('.minus').click(function () {
                var $input = $(this).parent().find('input');
                var count = parseInt($input.val()) - 1;
                count = count < 0 ? 1 : count;
                $input.val(count);
                $input.change();
                return false;
            });
            $('.plus').click(function () {
                var $input = $(this).parent().find('input');
                $input.val(parseInt($input.val()) + 1);
                $input.change();
                return false;
            });
        });
    </script>

@endsection
