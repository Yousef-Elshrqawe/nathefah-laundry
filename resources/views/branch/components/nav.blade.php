<div class="row">
    <div class="col-xl-12 mb-4">
        <div class="page-topbar">
            <div class="overview-box">
                <div class="row align-items-center flex-column-reverse flex-xl-row">
                    <div class="col-xl-4 col-md-12">
                        <div class="top-bar-title">
                            <h5>Welcome  {{Session::get('username')}} </h5>
                            <p class="cl-gray">Here is what happened in the branches today</p>
                        </div>
                    </div>

                    @php
                        $cur_route = Route::current()->getName();

                        $currentUrl = url()->current();
                        $urlParts   = explode('/',$currentUrl);

                        if($cur_route == "branch.home")
                            {
                                $url = route('branch.home',$urlParts[5]);
                                $nameUrl = "Branch name";
                            }elseif($cur_route == "branch.orders"){
                                $url = route('branch.orders',[$urlParts[5],$urlParts[6],$urlParts[7]]);
                                $nameUrl = "Customer name";
                            }elseif ($cur_route == "branch.services"){
                                    $url = route('branch.services');
                                    $nameUrl = "Service name";
                            }elseif ($cur_route == "branch.drivers"){
                                    $url = route('branch.drivers',[$urlParts[5],$urlParts[6]]);
                                    $nameUrl = "Driver name";
                            }elseif ($cur_route == "branch.get.notification"){
                                    $url = route('branch.get.notification');
                                    $nameUrl = "Notification";
                            }elseif ($cur_route == 'branch.setting'){
                                    $url = route('branch.setting');
                                    $nameUrl = "Setting";
                            }else{
                                $url = "";
                                $nameUrl = "";
                            }

                    @endphp
                    <div class="col-xl-4 col-md-12">
                        <div class="search-box">
                             {{-- note url shoudn't contain public --}}
                            <form action="{{$url}}">
                                <input type="text" name="search" id="" class="form-control" placeholder="{{$nameUrl}}">
                                <i class="fal fa-search" style="cursor: pointer" onclick='this.parentNode.submit(); return false;'></i>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-12">
                        <div class="notify-box">
                          {{--  <div class="notify-bell">
                                <p><a href="{{route('branch.get.notification')}}"><i class="fal fa-bell"></i>Notifications</a></p>
                            </div>--}}
                            <div>
                                <p>{{now()->format('l  m-d')}}</p>
                            </div>
                            <div>
                                <a href="{{route('branch.logout')}}">log out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
