<div class="tabbing-area">
    <form method="post" action="{{route('branch.confirm.order')}}">
        @csrf
    <hr class="spacer30px"/>
    <input type="hidden" name="order_id" value="{{$order->id}}">
    <input type="hidden" name="branch_id" value="{{$order->branch_id}}">
    <input type="hidden" name="delivery_fees" id="delivery_fees">
<div class="row">
     <div class="col-xl-4 col-md-4">
         <div class="success-txt">
             <span>Order number : {{$order->id}}</span>
             <span>Customer phone number {{$order->customer_phone}}</span>
             <span>Service Branch : {{$order->branch->username}}</span>
         </div>
     </div>
     <div class="col-xl-4 col-md-4">
         <div class="success-txt">
             <span>Customer name : {{$order->customer_name}}</span>
             <span>number of Items : {{$itemcount}} </span>
         </div>
     </div>







  {{--   <div class="col-xl-4 col-md-4">
         <div class="success-txt">
             <span>Customer Location : {{$order->customer_location}} </span>
             <span id="delivery_fees_value">Delivery fees:  </span>
             <span id="total_fees">Total Fees : {{$order->price_before_discount}} SR </span>
         </div>
     </div>--}}
 </div>
 </div>

 <h6>delivery type</h6>
 <div class="form-group">
     <select class="form-select" name="delivery_type" id="delivery_type"  onclick="deliverytype('delivery',{{$distance}},{{$order->price_before_discount}})">
         <option value="self_delivery" selected>Self delivery </option>
         {{-- <option value="bydelivery">By  delivery </option>
         <option value="on_way_delivery">One Way Delivery </option> --}}
     </select>
 </div>


    <div class="hide" id="one_way_delivery_type" hidden >
       <label> One Way Delivery </label>
         <div class="form-group">
             <select class="form-select" name="way_delivery" id="delivery_type"  onclick="deliverytype('delivery',{{$distance}},{{$order->price_before_discount}})">
                 <option value="home_drop_of" selected>home drop of</option>
                 <option value="self_drop_of">self drop of</option>
             </select>

         </div>
    </div>


<div id="delivery_date_type">
    <h6>Delivery Date</h6>
    <div>
        <label class="form-check-label" for="Instant">
            Instant
        </label>
        <input class="form-check-input" type="radio" name="delivery_date" id="Instant" checked
               onclick="datetype('Instant')">

        <label class="form-check-label" for="Scheduled">
            Scheduled
        </label>
        <input class="form-check-input" type="radio" name="delivery_date" id="Scheduled"
               onclick="datetype('Scheduled')">
    </div>
</div>

<!-- إخفاء هذا القسم افتراضياً عبر style -->
<div id="delivery_date" style="display: none;">
    <div class="row">
        <div class="col-3">
            <label>day</label>
            <input type="date" class="date" id="day" name="day">
        </div>
        <div class="col-3">
            <label>from</label>
            <input type="time" class="date" id="from" name="from">
        </div>
        <div class="col-3">
            <label>to</label>
            <input type="time" class="date" id="to" name="to">
        </div>
    </div>
</div>

<div class="btn-question mt-3">
    <button type="submit" class="btn-style-one">Confirm Order</button>
</div>

</form>
<script>
    function datetype(type) {
        // الحصول على العناصر
        const deliveryDateDiv = document.getElementById("delivery_date");
        const dayInput = document.getElementById("day");
        const fromInput = document.getElementById("from");
        const toInput = document.getElementById("to");

        if (type === 'Scheduled') {
            // إظهار الحقول
            deliveryDateDiv.style.display = "block";

            // جعلها مطلوبة
            dayInput.required = true;
            fromInput.required = true;
            toInput.required = true;

        } else if (type === 'Instant') {
            // إخفاء الحقول
            deliveryDateDiv.style.display = "none";

            // تفريغ القيم
            dayInput.value = "";
            fromInput.value = "";
            toInput.value = "";

            // إزالة required
            dayInput.required = false;
            fromInput.required = false;
            toInput.required = false;
        }
    }
</script>
