<!-- ==============CREATE NEW Additional service MODAL===================== -->
<div class="modal fade" id="Add_Additional_service" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form id="msform" action="{{route('branch.add.item')}}" method="post">
                    @csrf
                    <input type="hidden" name="type" value="additionalservice">
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Add Item </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="tabbing-area">
                        <ul class="nav nav-tabs">
                            @foreach ($additionalservices as $key=>$additionalservice)
                                <li class="nav-item">
                                    <a class="nav-link @if($key==0) active @endif " data-bs-toggle="tab" href="#tabadditional{{$key+1}}">{{$additionalservice->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach ($additionalservices as $key=>$additionalservice)
                             <div class="tab-pane @if($key==0) active @endif" id="tabadditional{{$key+1}}">
                                <div class="row">
                                    <div class="col-md-12 subnav-tab">
                                        <ul class="nav nav-tabs">
                                            @foreach($additionalservice->categories as $categorykey=>$category)
                                                <li class="nav-item">
                                                    <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tabadditionalcategory{{$key+1}}-{{$categorykey+1}}">{{$category->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>

                                <div class="tab-content">
                                    @foreach($additionalservice->categories as $categorykey=>$category)
                                        <div class="tab-pane @if($categorykey==0) active @endif" id="tabadditionalcategory{{$key+1}}-{{$categorykey+1}}">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="services-panel">
                                                    @foreach ($category->items as $item)
                                                        @foreach ($category->branchitems as $branchitem)
                                                        @if ($branchitem->item_id==$item->id)
                                                        <div class="services-panel-box">
                                                            <label>{{$item->name}}</label>
                                                            @php $pricevalue=''  ; @endphp

                                                            @foreach ($branchitem->branchitemprice as $itemprice)
                                                                @if ($itemprice->item_id==$item->id&& $itemprice->category_id==$category->id&&$itemprice->additionalservice_id==$additionalservice->id)
                                                                   @php $pricevalue=$itemprice->price  @endphp

                                                                @endif
                                                            @endforeach

                                                            <input name="count[]" type="number"class="form-control" value="{{$pricevalue}}">
                                                            <input type="hidden" name="item_id[]" value="{{$item->id}}">
                                                            <input type="hidden" name="category_id[]" value="{{$category->id}}">
                                                            <input type="hidden" name="service_id[]" value="{{$additionalservice->id}}">
                                                        </div>
                                                        @endif
                                                        @endforeach
                                                    @endforeach




                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>

                             </div>

                            @endforeach
                        </div>
                         {{-- end of main service --}}
                    </div>



                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Add Item</button >
                        <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
