@extends('branch_temp')
@section('content')


<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="page-topbar">
                        <div class="overview-box">
                            <div class="row align-items-center flex-column-reverse flex-xl-row">
                                <div class="col-xl-4 col-md-12">
                                    <div class="top-bar-title">
                                        <h5>Welcome  {{Session::get('username')}} </h5>
                                        <p class="cl-gray">Here is what happened in the branches today</p>
                                    </div>
                                </div>


                                <div class="col-xl-4 col-md-12">
                                    <div class="notify-box">
                                        {{--  <div class="notify-bell">
                                              <p><a href="{{route('branch.get.notification')}}"><i class="fal fa-bell"></i>Notifications</a></p>
                                          </div>--}}
                                        <div>
                                            <p>{{now()->format('l  m-d')}}</p>
                                        </div>
                                        <div>
                                            <a href="{{route('branch.logout')}}">log out</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <h4 class="title-inner mb-2">
                                Services
                            </h4>
                        </div>
                        <div class="col-xl-12 mb-4">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">Services Reports</h5>
                            </div>
                            <div class="table-box-select">
                                {{-- <div class="d-select-box">
                                    <select class="form-select" aria-label="Default select example">
                                        <option selected="">Period last month</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div> --}}
                                {{-- <div class="d-select-box">
                                    <select class="form-select" aria-label="Default select example">
                                        <option selected="">Branch Riyadh</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div> --}}
                            </div>
                        </div>
                    </div>
                </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Delivered Orders</p>
                                    </div>
                                </div>
                                <h4 class="cl-light">{{$reports->allOrders}}</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Completed Orders</p>
                                    </div>
                                </div>
                                <h4 class="cl-light">{{$reports->completedOrders}}</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">additional services</p>
                                    </div>
                                </div>

                                <h4 class="cl-light">{{$additionalservicescount}}</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">services</p>
                                    </div>
                                    {{-- <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div> --}}
                                </div>

                                <h4 class="cl-light">{{$branchservices}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">Services </h5>
                            </div>
                            <div class="table-box-select">
                                <a href="{{route('branch.service.edit')}}" class="btn-style-two border">Edit Items</a>
                                <a href="#" class="btn-style-two backbtn ms-0" data-bs-toggle="modal" data-bs-target="#Add_item">
                                    <i class="fas fa-plus">
                                    </i> Add item
                                </a>
                                <a href="#" class="btn-style-two backbtn ms-0" data-bs-toggle="modal" data-bs-target="#itemsStatus">
                                    <i class="fas fa-toggle-on">
                                    </i>Service Status
                                </a>
                                <a href="#" class="btn-style-two backbtn ms-0" data-bs-toggle="modal" data-bs-target="#argentprice">
                                    <i class="fas fa-money-bill"></i>
                                    Urgent Price
                                </a>

                                <a href="#" class="backbtn ms-0" onclick="changeArgentPriceStatus({{$branch->id}})" title="Open|Close argent status">
                                       <i class="{{$branch->argent == 1 ? "fas fa-toggle-on" : "fas fa-toggle-off"}}" id="argentStatus"></i>
                                    Urgent Price
                                </a>

                            </div>
                        </div>
                        <div class="branch-admin-service-tabing">
                <div class="row">
                    <div class="col-md-12">
                        <div class="line-title"><h6>Main Services</h6></div>
                        @include('components.services.mainservice',['services'=>$services])

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 mt-3">
                        <div class="line-title"><h6>Additional Services </h6></div>
                        @include('components.services.additionalservice',['additionalservices'=>$additionalservices])
                    </div>
                </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

@include('branch.services.create_item')



<!-- ==============CREATE NEW Additional service MODAL===================== -->




{{-- start edit item modal --}}
<div class="modal fade" id="edititem" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Edit item</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <form action="{{route('branch.edit.item')}}" method="post">
            <input type="hidden" name="id" id="branch_id">
            @csrf
            <div class="modal-body">
                @foreach (config('translatable.locales') as $locale)
                <label>{{ __('dashboard.'.$locale.'.name') }}</label>
                <input class="form-control" id="name_{{$locale}}" name="{{$locale}}[name]" value="" type="text">
                  @if ($errors->has($locale.'.name'))
                    <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                  @endif
            @endforeach
            </div>
            <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
            </div>
        </form>
      </div>
    </div>
</div>

@include('branch.services.add_additional_service')


@include('components.services.item_status')
@include('branch.services.argent_price')
@include('branch.services.edit-argent-price')

{{-- end edit item modal --}}
@endsection
@section('scripts')
{{-- script of edit item --}}
<script>
    var SITEURL = "{{ url('/branch-admin') }}";
    var languages={!!json_encode(config('translatable.locales'))!!}
    console.log(languages);
    $('.item_name').click(function(){
            var id=$(this).attr("data-id");
            $.ajax({
            url:SITEURL+"/get/branchitem/"+id,
            type:"GET", //send it through get method
            success: function (response) {
                $('#branch_id').val(response.id);
                for (let x in response.branchitemtranslation) {
                    for(let y in languages){
                        if(languages[y]==response.branchitemtranslation[x].locale){
                            $('#name_'+languages[y]).val(response.branchitemtranslation[x].name);
                        }
                    }
                }
            },
            error: function(response) {
                console.log('fdfd');
            }
        });
    });
</script>
{{-- script of error edit modal --}}
@if (Session::get('errors')!=null)
    <script>
        $(document).ready(function() {
            $('#edititem').modal('show');
        });
    </script>
@endif
    @include('branch.services.scripts')
@endsection
