<script>
    function changeStatus(id)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: "PUT",
            url: "{{route('service.change.status')}}",
            data:{
                id:id,
            },
            beforeSend: function(){

            },
            success: function(response) {
                //change publish value in td
                $(`#statusInput-${id}`).val(response.data.status);

                if(response.data.status == "on"){
                    $(`#status-${id}`).removeClass("fas fa-toggle-off").addClass('fas fa-toggle-on');
                }else{
                    $(`#status-${id}`).removeClass("fas fa-toggle-on").addClass('fas fa-toggle-off');
                }

            },complete: function(){
                swal({
                    title:'Success!',
                    text:"تم تغير الحاله ",
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },error: function(error){

            }

        });

    }


    function changeItemStatus(id)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: "PUT",
            url: "{{route('service.item.change.status')}}",
            data:{
                id:id,
            },
            beforeSend: function(){

            },
            success: function(response) {
                //change publish value in td
                $(`#itemInputStatus-${id}`).val(response.data.status);

                if(response.data.status == "on"){
                    $(`#itemStatus-${id}`).removeClass("fas fa-toggle-off").addClass('fas fa-toggle-on');
                }else{
                    $(`#itemStatus-${id}`).removeClass("fas fa-toggle-on").addClass('fas fa-toggle-off');
                }

            },complete: function(){
                swal({
                    title:'Success!',
                    text:"تم تغير الحاله ",
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },error: function(error){

            }

        });
    }


    $('#editItemArgentPrice').on('show.bs.modal', function(event) {
        let button = $(event.relatedTarget)
        let id = button.data('id')
        console.log(id)
        let argentPrice = button.data('argentprice')

        let modal = $(this)
        modal.find('.modal-body #PriceId').val(id);
        modal.find('.modal-body #editArgentPrice').val(argentPrice);

    });


    function changeArgentPriceStatus(id)
    {
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },

            type: "PUT",
            url: "{{route('service.item.argent.price-change-status')}}",
            data:{
                id:id,
            },
            beforeSend: function(){

            },
            success: function(response) {

                if(response.data.argent == 1){
                    $("#argentStatus").removeClass("fas fa-toggle-off").addClass('fas fa-toggle-on');
                }else{
                    $("#argentStatus").removeClass("fas fa-toggle-on").addClass('fas fa-toggle-off');
                }

            },complete: function(){
                swal({
                    title:'Success!',
                    text:"تم تغير الحاله ",
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },error: function(error){

            }

        });
    }

</script>
