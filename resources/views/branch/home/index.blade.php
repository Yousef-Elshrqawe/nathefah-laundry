@extends('branch_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            @include('branch.components.nav')
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <h4 class="title-inner mb-2">
                                Overview
                            </h4>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Your Balance</p>
                                    </div>
                                    {{-- <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                                <h4 class="cl-light">{{$Balance}} SR</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Today Balance</p>
                                    </div>
                                    {{-- <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                                <h4 class="cl-light">{{$Balancetoday}} SR</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Today's Orders</p>
                                    </div>
                                    {{-- <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                                <h4 class="cl-light">{{$todayorders}}</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">in Progress Orders</p>
                                    </div>
                                    {{-- <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div> --}}
                                </div>
                                <h4 class="cl-light">{{$inprogress}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 mb-3">
                            <div class="overview-box">
                                <div class="graph-box">
                                    <div class="row">
                                        <div class="col-xl-5">
                                            <h5 class="left-border">Branches Finance</h5>
                                        </div>
                                        <div class="col-xl-7">
                                            <div class="gr-drop-select">
                                                <div class="d-export-txt">
                                                    <div class="ex-img">
                                                        <img src="images/receipt-item.png" alt="">
                                                    </div>
                                                    <div>
                                                        <p class="cl-gray">Export</p>
                                                    </div>
                                                </div>
                                                <div class="d-select-box">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Period Last 30 Days</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="dropdown btn-dropdown">
                                                    <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                      <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                      <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <img src="images/graph1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 mb-3">
                            <div class="overview-box">
                                <div class="graph-box">
                                    <div class="row">
                                        <div class="col-xl-5 col-md-5">
                                            <h5 class="left-border">Orders Summary</h5>
                                        </div>
                                        <div class="col-xl-7 col-md-7">
                                            <div class="gr-drop-select">
                                                <div class="d-export-txt">
                                                    <div class="ex-img">
                                                        <img src="images/receipt-item.png" alt="">
                                                    </div>
                                                    <div>
                                                        <p class="cl-gray">Export</p>
                                                    </div>
                                                </div>
                                                <div class="d-select-box">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Branch : Riyadh</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="dropdown btn-dropdown">
                                                    <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                      <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                      <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <img src="images/graph1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">Latest Orders</h5>
                            </div>
                            <div class="table-box-select">
                                <div class="d-select-box">
                                    {{-- <form id="latest_order_form" method="get" action="{{route('branch.latest.orders')}}">
                                     @csrf --}}
                                    <select id="select_last_order" name="days" class="form-select" aria-label="Default select example">
                                        <option value="1825" selected>  Period last Week</option>
                                        <option value="1825" @if($week=='1825') selected  @endif>All</option>
                                        <option value="7" @if($week=='7') selected  @endif>One</option>
                                        <option value="14" @if($week=='14') selected  @endif>Two</option>
                                        <option value="21" @if($week=='21') selected  @endif>Three</option>
                                    </select>
                                   {{-- </form> --}}
                                </div>
                                <div class="d-selct-view">
                                    <a href="{{route('branch.allorder')}}" class="cl-light">View All</a>
                                </div>
                            </div>
                        </div>
                        <div class="table_responsive_maas v2">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th><span class="th-head-icon">Order number</span></th>
                                        <th><span class="th-head-icon">Customer name </span></th>
                                        <th><span class="th-head-icon">Delivery Type </span></th>
                                        <th><span class="th-head-icon">Arrival Date </span></th>
                                        <th><span class="th-head-icon">Order Status </span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach ($orders as $order)
                                    <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->customer_name}}</td>
                                            <td>
                                               @if ($order->deliverytype!=null)
                                               {{$order->deliverytype->name}}
                                               @endif
                                            </td>
                                            <td>{{$order->drop_date}}</td>
                                            <td>
                                                @if($order->progress!=null)
                                                  {{$order->progress}}
                                                @else
                                                     in accepted process
                                                @endif
                                            </td>
                                            <td>
                                                <a href="#" class="cl-light view_button" data-id="{{$order->id}}" data-bs-toggle="modal" data-bs-target="#order-details-modal">View</a>
                                            </td>
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>







                     <!--=================ORDER DETAILS MODAL================= -->
                    <div class="modal fade" id="order-details-modal" tabindex="-1" aria-labelledby="order-details-modal" aria-hidden="true">
                        <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5>Order Details</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>

                            <div class="modal-body" id="order_body">

                            </div>
                        </div>
                        </div>
                    </div>






                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
var url={!!json_encode(url('/'))!!}
$('#select_last_order').change(function(){
 var week=$(this).find(":selected").val();
 window.location.href=url+'/branch-admin/home/'+week;
});
</script>



{{-- show order --}}
<script>
    var SITEURL = "{{ url('/branch-admin') }}";
    $('.view_button').click(function(){
        //alert('vdfzx');
        var id=$(this).attr("data-id");
        //alert(id);
        $.ajax({
            url:SITEURL+"/order/view/"+id,
            type:"GET", //send it through get method
            success: function (response) {
                $('#order_body').html(response);
            },
            error: function(response) {
                console.log('fdfd');
            }
        });
    });
</script>



@endsection
