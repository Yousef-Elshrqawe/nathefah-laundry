<!-- ==============CREATE NEW DRIVER MODAL===================== -->
<div class="modal fade" id="new-order-modal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="msform" action="{{route('branch.store.driver')}}" method="post">
                    @csrf
                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>

                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Add Driver </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Driver Name</label>
                                <input type="text" name="name" class="form-control" id="" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed" required>
                                @error('name')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="" aria-describedby="emailHelp" placeholder="Enter Driver Email" required>

                                @error('email')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">country code</label>
                                <input type="text" value="+966"  disabled name="country_code" class="form-control" id="" aria-describedby="emailHelp" placeholder="5xxxxxxxx">

                                @error('country_code')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Phone Number</label>
                                <input type="text" name="phone" class="form-control" id="" aria-describedby="emailHelp" placeholder="5xxxxxxxx" required>
                                @error('phone')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>
                        {{-- <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Driving license copy</label>
                                <div class="file file--upload">
                                    <label for="input-file">
                                        <i class="fal fa-camera-alt"></i>Upload Tax Number
                                    </label>
                                    <input id="input-file"  type="file"  accept="image/*"   />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">One TIme Password</label>
                                <input type="email" class="form-control" id="" aria-describedby="emailHelp" placeholder="Enter the Password">
                            </div>
                        </div> --}}
                    </div>

                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Add Driver</button >
                        <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                    </div>




                </form>
            </div>
        </div>
    </div>
</div>
