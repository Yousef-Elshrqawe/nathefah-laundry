@extends('branch_temp')
@section('content')



        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">


                    @include('branch.components.nav')


                    <div class="row">

							<div class="col-lg-8 col-md-12">
								<div class="whitebox-panel">

									<div class="backbtn-title ">
                                        <h5>
                                            <a href="{{route('branch.drivers', ['driverlist' => 'All' , 'driverreport' => 'All'])}}">
                                                <i class="fas fa-chevron-left"></i>
                                            </a> Driver Details
                                        </h5>
                                    </div>

									<div class="driver-profile-top">
										<div class="driver-profile-top1">
											<div class="driver-profile-top2">
                                                <img src="{{asset('images/profile.jpg')}}" alt="">
                                            </div>

                                            <input type="hidden" id="driverId"  value="{{$driver->id}}">

											<div class="driver-profile-top3">
                                                <h4 >
                                                    <span id="driverName"> {{$driver->name}}</span>
                                                    <span>( {{$driver->successDriverCount}} Succesfull Delivery )</span>
                                                </h4>

												<span class="avl-version">
                                                    <i class="fas fa-circle"></i>
                                                    Available </span>
                                            </div>
										</div>
										<div class="driver-profile-top2">
										<div class="dropdown btn-dropdown">

														<a href="#" class="editbtn" onclick="openEditModal()"><i class="far fa-edit"></i></a>


													</div>
										</div>
									</div>
								{{--	<div class="driver-profile-topbtn">
										<button type="button" class="btn-style-two">Assign ali</button>
									    <button type="button" class="btn-style-two backbtn"> Deliver an Order</button>
									</div>--}}



									<div class="line-title "><h6>Basic info</h6></div>

									<div class="row">
										<div class="col-lg-12">
											<div class="drivers-info">
												<div class="drivers-info-box1">
                                                    <i class="far fa-location"></i> <strong>Email</strong> <span class="dot1"><i class="fas fa-circle"></i></span>
                                                    <span id="driverEmail">{{$driver->email}}</span>
                                                </div>
												<div class="drivers-info-box1">
                                                    <i class="far fa-phone-alt"></i> <strong>Main</strong> <span class="dot1"><i class="fas fa-circle"></i></span>
                                                    <span id="driverCountryCode">{{$driver->country_code}}</span>-<span id="driverPhone">{{$driver->phone}}</span>
                                                </div>
											</div>
										</div>
									</div>

									<div class="row">
                                            <div class="col-md-12">
                                                <h5 class="left-border">Latest Orders</h5>
                                                <div class="table_responsive_maas pt-2">
                                                    <table class="table" width="100%">
                                                        <thead>
                                                            <tr>
                                                                <th><span class="th-head-icon">Order number</span></th>
                                                                <th><span class="th-head-icon">Customer name </span></th>
                                                                <th><span class="th-head-icon">Order Status </span></th>
                                                                <th>&nbsp;</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        @foreach($driver->orders as $item)
                                                            <tr>
                                                                <td>{{$item->id}}</td>
                                                                <td>{{$item->customer_name}}</td>
                                                                <td>{{$item->delivery_status}} </td>
                                                                <td>
                                                                    <a href="#" class="cl-light" data-bs-toggle="modal"
                                                                       data-id="{{$item->id}}" data-created="{{$item->created_at}}"
                                                                       data-status="{{$item->status}}"
                                                                       data-bs-target="#order-details-modal">View Order</a>
                                                                </td>
                                                            </tr>
                                                        @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>


                                            </div>
                                        </div>

								</div>
							</div>
							<div class="col-lg-4 col-md-12">
							{{--	<div class="whitebox-panel">
									<div class="whitebox-panel-header">
										<h5 class="left-border">Driver Reports</h5>

									</div>
									<div class="whitebox-panel-tabbing">
										<ul class="nav nav-tabs nav-fill">
										  <li class="nav-item">
											<a class="nav-link active" data-bs-toggle="tab" href="#tab1">Today</a>
										  </li>
										  <li class="nav-item">
											<a class="nav-link" data-bs-toggle="tab" href="#tab2">Week</a>
										  </li>
										  <li class="nav-item">
											<a class="nav-link" data-bs-toggle="tab" href="#tab3">Month</a>
										  </li>
										</ul>

									<div class="tab-content">
										<div class="tab-pane active" id="tab1">
											<div class="row">
												<div class="col-md-12">

													<div class="whitebox-panel-graphmain">

														<div class="whitebox-panel-graph-body">
															<div class="whitebox-panel-graph-body1">
															<span class="font-style1">Completed Orders</span>
															<span class="font-style3 pb-3">{{$driver->completedOrderToday}}</span>
															<span class="font-style4" style="color:#20E33F;"> {{$complete}}.00 </span>
															<span class="font-style2">Previous Day</span>
															</div>
													    	<div class="whitebox-panel-graph-body2"><img src="{{asset('images/graph-small.jpg')}}" alt=""/></div>
														</div>
														<div class="whitebox-panel-graph-body">
															<div class="whitebox-panel-graph-body1">
															<span class="font-style1">All Orders</span>
															<span class="font-style3 pb-3">{{$driver->allOrderToday}}</span>
															<span class="font-style4" style="color:#E32020;"> {{$all}}  </span>
															<span class="font-style2">Previous Day</span>
															</div>
													    	<div class="whitebox-panel-graph-body2"><img src="{{asset('images/graph-small.jpg')}}" alt=""/></div>
														</div>
														<div class="whitebox-panel-graph-body">
															<div class="whitebox-panel-graph-body1">
															<span class="font-style1">Delayed Orders</span>
															<span class="font-style3 pb-3">0</span>
															<span class="font-style4" style="color:#868686;">+0</span>
															<span class="font-style2">Previous Day</span>
															</div>
													    	<div class="whitebox-panel-graph-body2"><img src="{{asset('images/graph-small.jpg')}}" alt=""/></div>
														</div>


													</div>


												</div>
											</div>
										</div>
										<div class="tab-pane" id="tab2">
											<div class="row">
												<div class="col-md-12">
													2
												</div>
											</div>
										</div>
										<div class="tab-pane" id="tab3">
											<div class="row">
												<div class="col-md-12">
													3
												</div>
											</div>
										</div>
									</div>
								</div>	</div>--}}

								<div class="whitebox-panel">
									<div class="whitebox-panel-header">
										<h5 class="left-border">Latest Orders</h5>
										<a href="#" class="viewall">View All </a>
									</div>
									<div class="whitebox-panel-body">
										<div class="table_responsive_maas v2 pt-0">
                                    <table class="table m-0" width="100%">
                                        <thead>
                                            <tr>
                                                <th><span class="th-head-icon">Order #</span></th>
                                                <th><span class="th-head-icon">Arrival Date</span></th>
                                                <th><span class="th-head-icon">Order Status</span></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($latestOrder as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>{{date("F jS, Y", strtotime($item->created_at))}}</td>
                                                <td>{{$item->delivery_status}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
									</div>
								</div>



					</div>
				</div>
			</div>
		</div>
	</div>
</section>

        @include('branch.drivers.driver-order-details')
        @include('branch.drivers.edit')
    <!-- <script>
        $(window).scroll(function(){
          var sticky = $('.page-topbar'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixedhead');
          else sticky.removeClass('fixedhead');
        });
    </script> -->
    <script src="js/menu.js"></script>
    <script type="text/javascript">
        $("#cssmenu").menumaker({
            title: "",
            format: "multitoggle"
        });
    </script>

@endsection
@section('scripts')
    @include('branch.drivers.driver-details-script')
@endsection
