@extends('branch_temp')
@section('content')
    <section>
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    @include('branch.components.nav')

                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="inner-title-box">
                                <div>
                                    <h4 class="title-inner">Drivers</h4>
                                </div>
                                <div class="btn-flex">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-order-modal"><i class="fas fa-plus"></i> Add new Driver</a>
                                </div>
                            </div>
                        </div>



                        <div class="col-lg-12">
                            <div class="driverbox-main">
                                @isset( $drivers)
                                    @foreach( $drivers as $driver )
                                        <div class="driverbox">
                                            <div class="driverbox1">
                                                <img src=" {{ $driver['img'] }} " alt=""/>
                                                <h6 style="cursor: pointer"> <a href="{{route('branch.driver.details',$driver->id)}}" target="_blank">{{ $driver['name'] }}</a> </h6>
                                                @if( $driver['status'] == 'online' )
                                                    <span class="avl-version"><i class="fas fa-circle"></i> Online </span>
                                                @else
                                                    <span class=""><i class="fad fa-circle"></i> Offline </span>
                                                @endif
                                                <span class="order-count">
                                                    <strong>
                                                        {{ $driver->driverOrdersInProgress()  }}
                                                    </strong> Orders</span>
                                                    <span class="">  <strong>   <a class="btn btn-primary" href="{{route('branch.assign.driver',[$driver->id,$order_id])}}">assign</a></strong></span>
                                            </div>
                                            <div class="driverbox2">
                                                <div class="driverbox2-sub1">Driver Rate
                                                    <strong>
                                                        {{ $driver->driverRates_count() }}
                                                    </strong>
                                                </div>
                                                <div class="driverbox2-sub1">Success Deliverys
                                                    <strong>
                                                        {{ $driver->driverOrdersCompleted() }}
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endisset
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>



    @include('branch.drivers.create')

@stop
@section('scripts')

    @include('branch.drivers.driver-scripts')

@endsection
