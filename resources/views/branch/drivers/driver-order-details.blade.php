<div class="modal fade" id="order-details-modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5>Order Details</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="or-details-box">
                    <ul class="col-line-3">
                        <li><i class="fal fa-user-alt"></i>Order ID :<span class="cl-gray" id="orderID"></span></li>
                        <li><i class="fal fa-clock"></i>Delivery Date :<span class="cl-gray" id="orderCreatedAt"> </span></li>
                        <li><i class="fal fa-user-alt"></i>Driver :<span class="cl-gray">None</span></li>
                        <li><i class="fal fa-clock"></i>Arrival Date :<span class="cl-gray">12 Dec.2022 , 13:20 </span></li>
                        <li><i class="fal fa-file"></i>Order Status :<a href="#" class="status-btn">In Progress</a></li>
                        <li><i class="fal fa-usd-square"></i>Total Order :<span class="cl-gray">100 SR</span></li>
                        <li class="text-right"><a href="#" class="edit-btn"><i class="fal fa-edit"></i>Edit Order</a> </li>
                        <li><i class="fal fa-truck"></i>Delivery Type :<span class="cl-gray">Sefl Delivery</span></li>
                        <li><i class="fal fa-wallet"></i>Payment Method :<span class="cl-gray">Cash</span></li>
                    </ul>
                </div>
                <div class="customer-data">
                    <div class="line-title"><h6 class="cl-gray">Customer Info</h6></div>
                    <div class="">
                        <div class="row">
                            <div class="col-xl-4 col-md-6">
                                <div class="or-details-box">
                                    <ul>
                                        <li><i class="fal fa-user-alt"></i>Customer name :<span class="cl-gray">Ahmed Mohamed</span></li>
                                        <li><i class="fal fa-map-marker-alt"></i>Location :<span class="cl-gray">Street name , district name</span></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6">
                                <div class="or-details-box">
                                    <ul>
                                        <li><i class="fas fa-phone-alt"></i>Phone Number :<span class="cl-gray">5631231</span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="item-info-order">
                    <div class="line-title"><h6 class="cl-gray">Items Info</h6></div>
                    <div class="table_responsive_maas v2">
                        <table class="table" width="100%">
                            <thead>
                            <tr>
                                <th>Item</th>
                                <th>Service</th>
                                <th>Fee </th>
                                <th>Additionals </th>
                                <th>Fee</th>
                                <th>Quantity</th>
                                <th>Urgent</th>
                                <th>Total Fees</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>Shirt</td>
                                <td>Washing</td>
                                <td>90.80</td>
                                <td>Smoother</td>
                                <td>90.80</td>
                                <td>3</td>
                                <td>3</td>
                                <td>14.00</td>
                            </tr>
                            <tr>
                                <td>Shirt</td>
                                <td>Washing</td>
                                <td>90.80</td>
                                <td>Smoother</td>
                                <td>90.80</td>
                                <td>3</td>
                                <td>3</td>
                                <td>14.00</td>
                            </tr>
                            <tr>
                                <td>Shirt</td>
                                <td>Washing</td>
                                <td>90.80</td>
                                <td>Smoother</td>
                                <td>90.80</td>
                                <td>3</td>
                                <td>3</td>
                                <td>14.00</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
