@extends('branch_temp')
@section('content')
    <section>
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    @include('branch.components.nav')

                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="inner-title-box">
                                <div>
                                    <h4 class="title-inner">Drivers</h4>
                                </div>
                                <div class="btn-flex">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-order-modal"><i class="fas fa-plus"></i> Add new Driver</a>
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-12 mb-4">
                            <div class="overview-box">
                                <div class="table-box">
                                    <div>
                                        <h5 class="left-border">Drivers List</h5>
                                    </div>
                                    <div class="table-box-select">
                                        <div class="d-select-box">
                                            <form>
                                            <select id="driver_list" class="form-select" aria-label="Default select example">
                                                <option   @if($driverlist==1) selected  @endif value="1">Period last month</option>
                                                <option value="All" @if($driverlist=='All') selected  @endif >All</option>

                                                <option value="1" @if($driverlist==1) selected  @endif >One</option>
                                                <option value="2" @if($driverlist==2) selected  @endif >Two</option>
                                                <option value="3" @if($driverlist==3) selected  @endif >Three</option>

                                            </select>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="driverbox-main">
                                @isset( $drivers)
                                    @foreach( $drivers as $driver )
                                        <div class="driverbox">
                                            <div class="driverbox1">
                                                <img src=" {{ $driver['img'] != null ? asset('storage/'.$driver['img']) : asset('images/profile.jpg') }}" alt="">

                                                <h6 style="cursor: pointer"> <a href="{{route('branch.driver.details',$driver->id)}}" target="_blank">{{ $driver['name'] }}</a> </h6>
                                                @if( $driver['status'] == 'online' )
                                                    <span class="avl-version"><i class="fas fa-circle"></i> Online </span>
                                                @else
                                                    <span class=""><i class="fad fa-circle"></i> Offline </span>
                                                @endif
                                                <span class="order-count">
                                                    <strong>
                                                        {{ $driver->driverOrdersInProgress()  }}
                                                    </strong> Orders</span>
                                            </div>
                                            <div class="driverbox2">
                                                <div class="driverbox2-sub1">Driver Rate
                                                    <strong>
                                                        {{ $driver->rate }}
                                                    </strong>
                                                </div>
                                                <div class="driverbox2-sub1">Success Deliverys
                                                    <strong>
                                                        {{ $driver->driverOrdersCompleted() }}
                                                    </strong>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                @endisset
                            </div>
                        </div>




                        <div class="col-xl-12 mb-4">
                            <div class="overview-box">
                                <div class="table-box">
                                    <div>
                                        <h5 class="left-border">Drivers Reports</h5>
                                    </div>
                                    <div class="table-box-select">
                                        <div class="d-select-box">
                                        <form>
                                            <select id="driver_report" class="form-select" aria-label="Default select example">
                                                    <option @if($driverreport==1) selected  @endif selected value="1">Period last month</option>
                                                    <option value="All" @if($driverreport=='All') selected  @endif>All</option>

                                                    <option value="1" @if($driverreport==1) selected  @endif>One</option>
                                                    <option value="2" @if($driverreport==2) selected  @endif>Two</option>
                                                    <option value="3" @if($driverreport==3) selected  @endif>Three</option>


                                            </select>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">canceled Orders</p>
                                            </div>

                                        </div>
                                        <h4 class="cl-light">{{$canceledorders}}</h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Success Deliverys</p>
                                            </div>

                                        </div>
                                        <h4 class="cl-light">{{$Deliveryscount}}</h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">in delivery Orders</p>
                                            </div>

                                        </div>

                                        <h4 class="cl-light">{{$indeliverycount}}</h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Top Driver </p>
                                            </div>

                                        </div>

                                        <h4 class="cl-light">{{ $topdriver ? $topdriver->name : ""}}</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



@include('branch.drivers.create')

@stop
@section('scripts')

    @include('branch.drivers.driver-scripts')

@endsection
