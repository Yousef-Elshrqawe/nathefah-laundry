<!-- <script>
    $(window).scroll(function(){
        var sticky = $('.page-topbar'),
            scroll = $(window).scrollTop();

        if (scroll >= 100) sticky.addClass('fixedhead');
        else sticky.removeClass('fixedhead');
    });
</script> -->
<script>
    $(document).ready(function(){

        $("#last-btn").click(function(){
            $("#progressbar").addClass("hide");
        });

        var current_fs, next_fs, previous_fs; //fieldsets
        var opacity;
        var current = 1;
        var steps = $("fieldset").length;

        setProgressBar(current);

        $(".next").click(function(){

            //current_fs = $(this).parent();
            // next_fs = $(this).parent().next();
            current_fs = $(this).closest("fieldset");
            next_fs = $(this).closest("fieldset").next();
            //alert(next_fs);
            //Add Class Active
            $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the next <fieldset></fieldset>
            next_fs.show();
            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    next_fs.css({'opacity': opacity});
                },
                duration: 500
            });
            setProgressBar(++current);
        });

        $(".previous").click(function(){

            current_fs = $(this).closest("fieldset");
            previous_fs = $(this).closest("fieldset").prev();

            //Remove class active
            $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

            //show the previous fieldset
            previous_fs.show();

            //hide the current fieldset with style
            current_fs.animate({opacity: 0}, {
                step: function(now) {
                    // for making fielset appear animation
                    opacity = 1 - now;

                    current_fs.css({
                        'display': 'none',
                        'position': 'relative'
                    });
                    previous_fs.css({'opacity': opacity});
                },
                duration: 500
            });
            setProgressBar(--current);
        });

        function setProgressBar(curStep){
            var percent = parseFloat(100 / steps) * curStep;
            percent = percent.toFixed();
            $(".progress-bar")
                .css("width",percent+"%")
        }

        $(".submit").click(function(){
            return false;
        })

    });
</script>
<script>
    $(document).ready(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 0 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>
<script>
    var driver_list=$('#driver_list').find(":selected").val();
    var driverreport=$('#driver_report').find(":selected").val();
    var url={!!json_encode(url('/'))!!}
    $('#driver_report').change(function(){
        var driverreport=$(this).find(":selected").val();
        window.location.href=url+'/branch-admin/drivers/'+driver_list+'/'+driverreport;
    });
    $('#driver_list').change(function(){
        var driver_list=$(this).find(":selected").val();
        window.location.href=url+'/branch-admin/drivers/'+driver_list+'/'+driverreport;
    });
</script>
@if (Session::get('errors')!=null)
    <script>
        $(document).ready(function() {
            $('#new-order-modal').modal('show');
        });
    </script>
@endif
