<!-- ==============EDIT  DRIVER MODAL===================== -->
<div class="modal fade" id="editDriverModal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form action="{{route('branch.edit.driver')}}" method="POST">
                    @csrf
                    @method("PUT")

                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>

                    <input type="hidden" name="id" id="driverIdInput">

                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Edit Driver </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Driver Name</label>
                                <input type="text" name="name" class="form-control" id="driverNameInput" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed" required>
                                @error('name')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="driverEmailInput" aria-describedby="emailHelp" placeholder="Enter Driver Email" required>
                                @error('email')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">country code</label>
                                <input type="text" name="country_code" class="form-control" disabled value="+966" id="driverCountryCodeInput" aria-describedby="emailHelp" placeholder="5xxxxxxxx">
                                @error('country_code')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Phone Number</label>
                                <input type="text" name="phone" class="form-control" id="driverPhoneInput" aria-describedby="emailHelp" placeholder="5xxxxxxxx" required>
                                @error('phone')
                                <span class="text-danger">{{$message}}</span>
                                @enderror
                            </div>
                        </div>
                    </div>

                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Edit Driver</button >
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>


