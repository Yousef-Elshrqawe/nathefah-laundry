<div class="row">
    <div class="col-xl-4 col-md-4">
        <div class="or-details-box">
            <ul>
                <li><i class="fal fa-user-alt"></i>Order ID :<span class="cl-gray" id="order_id">{{$order->id}}</span></li>
                <li><i class="fal fa-clock"></i>Delivery Date :<span class="cl-gray" id="delivery_date">14 Dec.2022 </span></li>
                <li><i class="fal fa-user-alt"></i>Driver :<span class="cl-gray"  id="driver"> @if($order->driver!=null) {{$order->driver->name}} @endif</span></li>
            </ul>
        </div>
    </div>
    <div class="col-xl-4 col-md-4">
        <div class="or-details-box">
            <ul >
                <li><i class="fal fa-clock"></i>Arrival Date :<span class="cl-gray">{{$order->drop_date}} </span></li>
                <li><i class="fal fa-file"></i>Order Status :<a href="#" class="status-btn"> @if($order->progress!=null)
                    {{$order->progress}}
                  @else
                       in accepted process
                  @endif</a></li>


                  <li>
                    <i class="fal fa-usd-square"></i>Taxes :<span class="cl-gray">
                        {{$order->taxes}}
                    SR</span>
                  </li>




            </ul>
        </div>
    </div>
    <div class="col-xl-4 col-md-4">
        <div class="or-details-box">
            <ul>
                {{-- @if ($order->confirmation=='pending')
                  <li class="text-right d-mob-edit"><a href="{{route('branch.finish.order',$order->id)}}" class="edit-btn"><i class="fal fa-edit"></i>Accept Order</a> </li>
                  <a href="{{route('branch.finish.order',$order->id)}}" class="edit-btn mob"><i class="fal fa-edit"></i>Accept Order</a>
                  @endif --}}

                {{-- <li class="text-right d-mob-edit"><a href="#" class="edit-btn"><i class="fal fa-edit"></i>Edit Order</a> </li> --}}
                {{-- <a href="#" class="edit-btn mob"><i class="fal fa-edit"></i>Edit Order</a> --}}

                <li><i class="fal fa-truck"></i>Delivery Type :<span class="cl-gray">{{$order->deliverytype->name}}</span></li>
                <li><i class="fal fa-wallet"></i>Payment Method :<span class="cl-gray">
                   @if ($order->paymenttype!=null)
                       {{$order->paymenttype->name}}
                   @else
                        cash
                   @endif
                </span></li>


                <li>
                    <i class="fal fa-usd-square"></i>delivery fees :<span class="cl-gray">
                        {{$order->delivery_fees}}
                    SR</span>
                </li>

                    <li>
                        <i class="fal fa-usd-square"></i>Discount :<span class="cl-gray">
                            @if ($order->discounttype == null)
                                0
                            @elseif($order->discounttype == 'branch')
                                {{ round($order->price_before_discount - $order->price_after_discount, 1) }}
                            @elseif($order->discounttype == 'Admin')
                                0
                            @else
                                0
                            @endif
                        SR</span>
                    </li>

                    <li>
                        <i class="fal fa-usd-square"></i>Total Order :
                        <span class="cl-gray">
        @if ($order->discounttype == null)
                                {{ round($order->price_before_discount, 1) }}
                            @elseif($order->discounttype == 'branch')
                                {{ round($order->price_after_discount, 1) }}
                            @elseif($order->discounttype == 'Admin')
                                {{ round($order->price_before_discount, 1) }}
                            @else
                                {{ round($order->price_before_discount, 1) }}
                            @endif
    SR</span>
                    </li>

            </ul>
        </div>
    </div>
</div>

<div class="customer-data">
    <div class="line-title"><h6 class="cl-gray">Customer Info</h6></div>
    <div class="row">
        <div class="col-xl-4 col-md-6">
            <div class="or-details-box">
                <ul>
                    <li><i class="fal fa-user-alt"></i>Customer name :<span class="cl-gray" id="customer_name">{{$order->customer_name}}</span></li>
                    <li><i class="fal fa-map-marker-alt"></i>Location :<span class="cl-gray" id="customer_localtion">{{$order->customer_location}}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-xl-4 col-md-6">
            <div class="or-details-box">
                <ul>
                    <li><i class="fas fa-phone-alt"></i>Phone Number :<span class="cl-gray"id="customer_phone">{{$order->customer_phone}}</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="item-info-order">
    <div class="line-title"><h6 class="cl-gray">Items Info</h6></div>
    <div class="table_responsive_maas v2">
        <table class="table" width="100%">
            <thead>
            <tr>
                <th>Item</th>
                <th>Service</th>
                <th>Fee</th>
                <th>Additionals</th>
                <th>Fee</th>
                <th>Quantity</th>
                <th>Urgent</th>
                <th>Fee</th>
                <th>Total Fees</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($order->orderdetailes as $detail)
                @php
                    $itemFee       = $detail->additionalservice ? 0 : $detail->price; // سعر القطعة بدون خدمة إضافية
                    $additionalFee = $detail->additionalservice ? $detail->price : 0; // سعر الخدمة الإضافية لكل قطعة
                    $urgentFee     = 0;

                    foreach ($urgents as $urgent) {
                        if ($urgent->service_id == $detail->service_id && $urgent->branchitem_id == $detail->branchitem_id) {
                            $urgentFee = $urgent->price; // سعر الـ Urgent لكل قطعة
                            break;
                        }
                    }

                    // حساب الرسوم بالشكل الصحيح (بدون ضرب خاطئ في العدد)
                    $totalFees = ($itemFee + $additionalFee + $urgentFee);
                @endphp

                <tr>
                    <!-- 🏷️ اسم العنصر -->
                    <td class="fw-bold text-dark">
                         {{ $detail->Branchitem->name }}
                    </td>

                    <!-- 🛠️ اسم الخدمة -->
                    <td class="text-secondary">
                         {{ ucfirst($detail->service->name) }}
                    </td>

                    <!-- 💰 رسوم العنصر الأساسي لكل قطعة -->
                    <td class="text-end">
        <span class="badge bg-light text-dark px-2 py-1 shadow-sm">
            {{ number_format($itemFee / $detail->quantity, 2) }} SR
        </span>
                    </td>


                    <td class="text-center">
                        {!! $detail->additionalservice
                            ? '<span class="text-success fw-bold"><i class="fas fa-plus-circle"></i> ' . $detail->additionalservice->name . '</span>'
                            : '<span class="text-muted"><i class="fas fa-minus-circle"></i> لا يوجد</span>' !!}
                    </td>


                    <td class="text-end">
                        {!! $additionalFee
                            ? '<span class="badge bg-warning text-dark px-2 py-1 shadow-sm">' . number_format($additionalFee, 2) . ' SR</span>'
                            : '<span class="text-muted"><i class="fas fa-ban"></i> —</span>' !!}
                    </td>


                    <td class="text-center fw-bold text-dark">
        <span class="badge bg-info text-white px-3 py-1 shadow-sm">
            {{ $detail->quantity }}
        </span>
                    </td>


                    <td class="text-center">
                        {!! $urgentFee
                            ? '<span class="badge bg-danger text-white px-3 py-1 shadow-sm"><i class="fas fa-bolt"></i> مستعجل</span>'
                            : '<span class="badge bg-secondary text-white px-3 py-1 shadow-sm"><i class="fas fa-clock"></i> عادي</span>' !!}
                    </td>


                    <td class="text-end">
                        {!! $urgentFee
                            ? '<span class="badge bg-danger text-white px-2 py-1 shadow-sm">' . number_format($urgentFee, 2) . ' SR</span>'
                            : '<span class="text-muted"><i class="fas fa-ban"></i> —</span>' !!}
                    </td>


                    <td class="text-end fw-bold text-primary">
        <span class="badge bg-primary text-white px-3 py-2 shadow-sm">
            {{ number_format($totalFees, 2) }} SR
        </span>
                    </td>
                </tr>


            @endforeach

            </tbody>

        </table>
    </div>
</div>
