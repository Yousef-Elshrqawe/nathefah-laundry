<!-- ==============CREATE NEW USER MODAL===================== -->
<div class="modal fade" id="edit-user-branch-modal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form action="{{route('branch.setting.permissions-update.user')}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Edit User </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>

                    <input type="hidden" id="editUserId" name="id">

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">User Name</label>
                                <input type="text" name="username" class="form-control" id="editUsername" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed">
                                @error('name')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Email</label>
                                <input type="email" name="email" class="form-control" id="editUserEmail" aria-describedby="emailHelp" placeholder="Enter Driver Email">

                                @error('email')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">country code</label>
                                <input type="text" name="country_code" class="form-control" id="editUserCountryCode" aria-describedby="emailHelp" placeholder="5xxxxxxxx">

                                @error('country_code')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Phone Number</label>
                                <input type="text" name="phone" class="form-control" id="editUserPhone" aria-describedby="emailHelp" placeholder="5xxxxxxxx">
                                @error('phone')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Role</label>
                                <select name="role" id="editUserRole" class="form-control">
                                    <option>Choose Role</option>
                                    @foreach($roles as $item)
                                        <option value="{{$item->id . "-" . $item->name}}">{{$item->name}}</option>
                                    @endforeach
                                </select>

                                @error('country_code')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>

                    </div>

                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Add User</button >
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
