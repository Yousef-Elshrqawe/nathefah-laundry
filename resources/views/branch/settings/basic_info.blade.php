<div class="tab-pane active" id="leftnav1">
    <div class="row">
        <div class="col-md-12">

            <h5 class="pb-4 left-border">Brand info </h5>

            {{-- <div class="row">
                <div class="col-lg-12">
                    <div class="settings-profile">
                        <a href="{{route('setting.branch.details')}}"><img src="{{asset('images/company-logo.jpg')}}" alt=""/></a>
                        <a href="" class="btn-style-two"><i class="fas fa-plus"></i> Upload new Picture</a>
                        <a href="#" class="btn-style-two backbtn">Remove Picture</a>
                    </div>
                </div>
            </div> --}}


            <div class="line-title lightcolor"><h6>Basic info</h6></div>

            <div class="row">
                <div class="col-lg-11">
                    <div class="row pt-2">

                        {{--branch id--}}
                        <input type="hidden" id="branchID" value="{{$branchInfo->id}}">

                        <div class="col-md-6">
                            <div class="form-box mb-4">
                                <label for="branchUserId" class="form-label">Brand info</label>
                                <input type="email" class="form-control" id="branchUserId" value="{{$branchInfo->username}}" placeholder="Fresh & Clean ">
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box mb-4">
                                <label for="branchStatusId" class="form-label">status</label>
                                <select class="form-select" id="branchStatusId" aria-label="Default select example">
                                    <option selected disabled>Choose Status</option>
                                    <option value="open" {{$branchInfo->status == "open" ? "selected" : "" }}>Open</option>
                                    <option value="closed" {{$branchInfo->status == "closed" ? "selected" : "" }}>Closed</option>
                                </select>
                            </div>
                        </div>


                    </div>
                    <div class="row">


                        <div class="col-md-6">
                            <div class="form-box mb-4">
                                <label for="" class="form-label">Branch Location</label>
                                <div class="settings-grab-location">
                                    <input type="email" class="form-control" value="{{$branchInfo->address}}" id="branchAddressId" placeholder="Shop No. 700, Gali Kundewalan ">
                                    <a href="#" class="icon"><i class="fal fa-map"></i></a>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="form-box mb-4">
                                <label for="branchCountryCodeId" class="form-label">Branch Country Code</label>
                                <input type="text" value="{{$branchInfo->country_code}}" class="form-control" id="branchCountryCodeId" placeholder="56 866 0899">
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="form-box mb-4">
                                <label for="branchPhoneId" class="form-label">Branch Number</label>
                                <input type="text" value="{{$branchInfo->phone}}" class="form-control" id="branchPhoneId" placeholder="56 866 0899">
                            </div>
                        </div>

                    </div>
                    <div class="row">
                     {{--   <div class="col-md-6">
                            <div class="form-box mb-4">
                                <label for="" class="form-label">Working Hours</label>

                                <div class="width-calc-less">
                                    <div class="selectbox-mid-secboxes">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="selectbox-mid-secboxes1">
--}}{{--                                                    <i class="fal fa-clock"></i>--}}{{--
                                                    <input type="time" value="{{$branchInfo->open_time}}" class="form-control" id="branchOpenTimeId" placeholder="56 866 0899">
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="selectbox-mid-secboxes1">
--}}{{--                                                    <i class="fal fa-clock"></i>--}}{{--
                                                    <input type="time" value="{{$branchInfo->closed_time}}" class="form-control" id="branchCloseTimeId" placeholder="56 866 0899">

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>--}}
                        <div class="col-md-6">
                            <div class="form-box mb-4">
                                {{-- <label for="" class="form-label">Holiday</label> --}}
                                <div class="selectbox-mid-secboxes">

                                    {{-- <div class="row">
                                        <div class="col-md-6"><div class="selectbox-mid-secboxes1">
                                                <i class="far fa-suitcase-rolling"></i>
                                                <select class="form-select" aria-label="Default select example">
                                                    <option selected="">holidays  : Select</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div></div>
                                    </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mt-2 mb-4">
                            <div class="form-box">
                                <button type="button" class="btn-style-two" onclick="updateBranchInfo()">
                                    <i class="far fa-check"></i>
                                    Accept Changes
                                </button>
                                <button type="button" class="btn-style-two backbtn" onclick="restBranchInfo()">Reset</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            {{-- <div class="line-title lightcolor mb-2"><h6>Subscribtion info</h6></div>


            <div class="row settings-subsc">
                <div class="col-lg-6">
                    <div class="settings-subscribe-info">
                        <div class="d-flex align-items-center justify-content-between">
                            <span>Riyadh Branch</span>
                            <span class="ends">Ends in 16/9/2022</span>
                        </div>
                        <p>your Subscription will be auto renewed after the it ends</p>
                        <div class="d-flex align-items-center justify-content-between">
                            <span><a href="#" class="btn-style-two">Renew Subscription</a></span>
                            <span><a href="#" class="btn-style-two backbtn2">Cancel Subscription</a></span>
                        </div>
                    </div>
                </div>

            </div> --}}



        </div>
    </div>
</div>
