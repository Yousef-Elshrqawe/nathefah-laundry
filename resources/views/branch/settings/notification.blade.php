<div class="tab-pane " id="leftnav2">
    <div class="row">
        <div class="col-md-12">

            <h5 class="pb-4 left-border">Notifications Settings</h5>
            <h6 class="pb-3">Select Notification you want to receive</h6>
            <div class="row">
                    <div class="col-lg-12">
                        <div class="checkbox-form-main">
                            <div class="checkbox-loop chkboxmain">
                                New Orders
                                <input id="Option1" {{$notifications ? $notifications[0]->status == 1 ? "checked" : "" : ""}} name="option1"  type="checkbox">
                                <label class="checkbox" for="Option1"></label>
                            </div>
                            <div class="checkbox-loop chkboxmain">
                                Delayed Orders
                                <input id="Option2" {{$notifications[1]->status == 1 ? "checked" : ""}}  name="option2" type="checkbox">
                                <label class="checkbox" for="Option2"></label>
                            </div>
                            <div class="checkbox-loop chkboxmain">
                                Bad Reviews
                                <input id="Option3"  {{$notifications[2]->status == 1 ? "checked" : ""}}  name="option3" type="checkbox">
                                <label class="checkbox" for="Option3"></label>
                            </div>
                            <div class="checkbox-loop chkboxmain">
                                Unaccepted Orders
                                <input id="Option4" {{$notifications[3]->status == 1 ? "checked" : ""}}  name="option4" type="checkbox">
                                <label class="checkbox" for="Option4"></label>
                            </div>
                            <div class="checkbox-loop chkboxmain">
                                All Reviews
                                <input id="Option5" {{$notifications[4]->status == 1 ? "checked" : ""}}   name="option5" type="checkbox">
                                <label class="checkbox" for="Option5"></label>
                            </div>

                        </div>
                    </div>
            </div>


            <div class="row">
                <div class="col-md-12 mt-2 mt-5">
                    <div class="form-box">
                        <button type="button" class="btn-style-two" onclick="addDeleteNotification()">
                            <i class="far fa-check"></i>
                            Accept Changes
                        </button>
                        <button type="button" class="btn-style-two backbtn" id="resetNotification" onclick="resetNotifications()">Reset</button>
                    </div>
                </div>
            </div>






        </div>
    </div>
</div>
