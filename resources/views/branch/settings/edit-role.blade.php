<!-- ==============CREATE NEW USER MODAL===================== -->
<div class="modal fade" id="edit-role" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form action="{{route('branch.role.update')}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Edit Role </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <input type="hidden" name="id" id="editRoleId">
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Role Name</label>
                                <input type="text" name="name" class="form-control" id="editRole" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed">
                                @error('name')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-box" id="editPermissions">
                            </div>
                        </div>



                    </div>

                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Update Role</button >
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
