<div class="tab-pane " id="leftnav3">
    <div class="row">
        <div class="col-md-12">

            <h5 class="pb-4 left-border">User & Permissions</h5>
            <h6 class="pb-2">Current User</h6>

            <div class="row settings-permission-main">
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <div class="settings-permission-info">

                        <div class="settings-permission-infotop">
                            <div class="settings-permission-infotop1"><img src="{{asset('images/profile.jpg')}}" alt=""/></div>
                            <div class="settings-permission-infotop2">
                                <h6>{{$currentUser->username}}</h6>
                                {{$currentUser->type}}
                            </div>
                        </div>
                        <div class="d-flex align-items-center justify-content-between">
                            <span><a href="#" class="btn-style-two border" onclick="editUserBranch({{$currentUser->id}})">Edit permissions</a></span>
{{--                            <span><a href="#" class="btn-style-two backbtn2">Delete</a></span>--}}
                        </div>

                    </div>
                </div>
            </div>

            <h6 class="pb-2">All Users</h6>
            <div class="row settings-permission-main">

                @foreach($branchInfo->users as $item)
                    @if($item->id != auth('branch')->id())
                         <div class="col-xl-4 col-lg-6 col-md-6" id="userInfo-{{$item->id}}">
                            <div class="settings-permission-info">

                                <div class="settings-permission-infotop">
                                    <div class="settings-permission-infotop1"><img src="{{asset('images/profile.jpg')}}" alt=""/></div>
                                    <div class="settings-permission-infotop2">
                                        <h6>{{$item->username}}</h6>
                                        {{$item->type}}
                                    </div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>
                                        <a class="btn-style-two border" onclick="editUserBranch({{$item->id}})">Edit permissions</a>
                                    </span>
                                    <span>
                                        <button class="btn-style-two backbtn2" onclick="deleteUserBranch({{$item->id}})">Delete</button>
                                    </span>
                                </div>

                            </div>
                         </div>
                    @endif
                @endforeach
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <a onclick="addNewUserBranch()" style="cursor: pointer">
                        <div class="add-branch"><i class="fal fa-plus"></i> Add Branch User</div>
                    </a>
                </div>
            </div>




        </div>
    </div>
</div>
