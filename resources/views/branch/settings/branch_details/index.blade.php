@extends('branch_temp')
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                @include('branch.components.nav')
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="branch-inner">
                            <div class="branch-laundry">
                                <img src="images/laundry.png" alt="">
                            </div>
                            <div class="laundry-small-logo">
                                <img src="images/company-logo.jpg" alt="">
                            </div>
                            <div class="whitebox-panel">

                                <div class="driver-profile-top mb-0">
                                    <div class="backbtn-title ">
                                        <h5 class="mb-0">
                                            <a href="#">
                                                <i class="fas fa-chevron-left"></i>
                                            </a> {{$branchInfo->username}}
                                        </h5>
                                        <span class="avl-version">{{$successOrders}} Successful Orders</span>
                                    </div>
                                    <div class="driver-profile-top2">
                                        <a  onclick="openEditBranch()" class="editbtn">
                                            <i class="far fa-edit"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="line-title ">
                                    <h6>Basic info</h6>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="drivers-info">
                                            <div class="drivers-info-box1">
                                                <i class="far fa-location"></i>
                                                <strong>Riyadh</strong>
                                                <span class="dot1">
                                              <i class="fas fa-circle"></i>
                                             </span> {{$branchInfo->address}}
                                            </div>
                                            <div class="drivers-info-box1">
                                                <i class="far fa-phone-alt"></i>
                                                <strong>Working Hours</strong>
                                                <span class="dot1">
                                              <i class="fas fa-circle"></i>
                                             </span> {{$branchInfo->open_time}} to {{$branchInfo->closed_time}}
                                            </div>
                                            <div class="drivers-info-box1">
                                                <i class="far fa-phone-alt"></i>
                                                <strong>Main</strong>
                                                <span class="dot1">
                                                 <i class="fas fa-circle"></i>
                                               </span> {{$branchInfo->country_code . $branchInfo->phone}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="line-title"><h6>Main Services</h6></div>
                                        <div class="tabbing-area">
                                            <ul class="nav nav-tabs">
                                                @foreach ($services as $key=>$service)
                                                    @if (in_array($service->id,$services_id))
                                                        <li class="nav-item">
                                                            <a class="nav-link @if($key==0) active @endif " data-bs-toggle="tab" href="#tab{{$key+1}}">{{$service->name}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            <div class="tab-content">
                                                @foreach ($services as $key=>$service)
                                                    @if (in_array($service->id,$services_id))
                                                        <div class="tab-pane @if($key==0) active @endif" id="tab{{$key+1}}">
                                                            <div class="row">
                                                                <div class="col-md-12 subnav-tab">
                                                                    <ul class="nav nav-tabs">
                                                                        @foreach($service->categories as $categorykey=>$category)
                                                                            <li class="nav-item">
                                                                                <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tab{{$key+1}}-{{$categorykey+1}}">{{$category->name}}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="tab-content">
                                                                @foreach($service->categories as $categorykey=>$category)
                                                                    <div class="tab-pane @if($categorykey==0) active @endif" id="tab{{$key+1}}-{{$categorykey+1}}">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="services-panel">
                                                                                    @foreach ($category->branchitems as $item)
                                                                                        @foreach ($item->branchitemprice as $price)
                                                                                            @if($price->category_id==$category->id && $price->service_id==$service->id)
                                                                                                <div class="services-panel-box">
                                                                                                    <label>{{$item->name}}
                                                                                                        <span>
                                                                                                            <a href="#" class="item_name"  data-id="{{$item->id}}" data-bs-toggle="modal" data-bs-target="#edititem">

                                                                                                            </a>
                                                                                                        </span>
                                                                                                    </label>
                                                                                                    <span class="input-writeup">{{$price->price}} SR</span>
                                                                                                </div>
                                                                                            @endif
                                                                                        @endforeach
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>

                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>
                                            {{-- end of main service --}}
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <div class="line-title"><h6>Additional Services </h6></div>
                                        <div class="tabbing-area">
                                            <ul class="nav nav-tabs">
                                                @foreach ($additionalservices as $key=>$additionalservice)
                                                    @if (in_array($additionalservice->id,$additionalservices_id))
                                                        <li class="nav-item">
                                                            <a class="nav-link @if($key==0) active @endif" data-bs-toggle="tab" href="#tab1{{$key+1}}">{{$additionalservice->name}}</a>
                                                        </li>
                                                    @endif
                                                @endforeach
                                            </ul>
                                            <div class="tab-content">
                                                @foreach ($additionalservices as $key=>$additionalservice)
                                                    @if (in_array($additionalservice->id,$additionalservices_id))
                                                        <div class="tab-pane @if($key==0) active @endif" id="tab1{{$key+1}}">
                                                            <div class="row">
                                                                <div class="col-md-12 subnav-tab">
                                                                    <ul class="nav nav-tabs">
                                                                        @foreach ($additionalservice->categories as $categorykey=>$category)
                                                                            <li class="nav-item">
                                                                                <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tab11-{{$categorykey+1}}">{{$category->name}}</a>
                                                                            </li>
                                                                        @endforeach
                                                                    </ul>
                                                                    <div class="tab-content">
                                                                        @foreach ($additionalservice->categories as $categorykey=>$category)
                                                                            <div class="tab-pane @if($categorykey==0) active  @endif" id="tab11-{{$categorykey+1}}">
                                                                                <div class="row">
                                                                                    <div class="col-md-12">
                                                                                        <div class="services-panel">
                                                                                            @foreach ($category->branchitems as $item)
                                                                                                @foreach ($item->branchitemprice as $itemprice)
                                                                                                    @if ($itemprice->category_id==$category->id&&$itemprice->additionalservice_id==$additionalservice->id)
                                                                                                        <div class="services-panel-box">
                                                                                                            <label>{{$item->name}} <span><a href="#" class="item_name"  data-id="{{$item->id}}" data-bs-toggle="modal" data-bs-target="#edititem"></a></span></label>
                                                                                                            <span class="input-writeup">{{$itemprice->price}} SR</span>
                                                                                                            @foreach($item->itemadditionalservice as $additional)
                                                                                                                @if($additional->additionalservice_id == $additionalservice->id)
                                                                                                                    <span class="input-writeup m-auto">
                                                                                                                        <i  id="itemStatus-{{$additional->id}}" title="Change status"
                                                                                                                           class="{{$additional->status == "on" ? "fas fa-toggle-on" : "fas fa-toggle-off"}}">
                                                                                                                        </i>
                                                                                                                         <input type="hidden" value="{{$additional->status}}" id="itemInputStatus-{{$additional->id}}">
                                                                                                                       </span>
                                                                                                                @endif
                                                                                                            @endforeach

                                                                                                        </div>
                                                                                                    @endif
                                                                                                @endforeach
                                                                                            @endforeach
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <br>
                                <div class="row">
                                    <div class="col-xl-12">
                                        <div class="table-box">
                                            <div>
                                                <h5 class="left-border">Reports</h5>
                                            </div>
                                            <div class="table-box-select">
                                                <div class="d-select-box">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Sort By : Latest</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        @foreach ($reports as $report)
                                            <div class="review-sec">
                                                <div class="review-profile-details">
                                                    <img src="images/avatar.png" alt="">
                                                    <div class="review-content">
                                                        <div class="report-name">
                                                            <ul>
                                                                <li>{{$report->user->name}}</li>
                                                                <li class="border-round">{{$report->created_at}}</li>
                                                            </ul>
                                                        </div>
                                                        <h6>{{$report->desc}}</h6>
                                                        <div class="selling-title-box">
                                                            <ul>
                                                                <li><i class="fas fa-star"></i></li>
                                                                <li><a href="#">{{$report->rate}}</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="review-point">
                                                    <a href="#" data-id="{{$report->order_id}}" class="blue-btn-link view_button" data-bs-toggle="modal" data-bs-target="#order-details-modal">View </a>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="justify-content-center d-flex">
                                            {!! $reports->appends(Request::except('page'))->render() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="whitebox-panel">
                            <div class="whitebox-panel-header">
                                <h5 class="left-border">Driver Reports</h5>
                            </div>
                            <div class="whitebox-panel-tabbing">
                                <ul class="nav nav-tabs nav-fill">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-bs-toggle="tab" href="#tab1-day">Today</a>
                                    </li>
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" data-bs-toggle="tab" href="#tab2-week">Week</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" data-bs-toggle="tab" href="#tab3-month">Month</a>--}}
{{--                                    </li>--}}
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab1-day">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="whitebox-panel-graphmain">
                                                    <div class="whitebox-panel-graph-body">
                                                        <div class="whitebox-panel-graph-body1">
                                                            <span class="font-style1">Completed Orders</span>
                                                            <span class="font-style3 pb-3">{{$todaySuccessOrders}}</span>
                                                            <span class="font-style4" style="color:#20E33F;">{{$todaySuccessOrders - $yesterdaySuccessOrders}} </span>
                                                            <span class="font-style2">Previous Day</span>
                                                        </div>
                                                        <div class="whitebox-panel-graph-body2">
                                                            <img src="images/graph-small.jpg" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="whitebox-panel-graph-body">
                                                        <div class="whitebox-panel-graph-body1">
                                                            <span class="font-style1">All Orders</span>
                                                            <span class="font-style3 pb-3">{{$todayOrders}}</span>
                                                            <span class="font-style4" style="color:#E32020;">{{$todayOrders - $yesterdayOrders}}</span>
                                                            <span class="font-style2">Previous Day</span>
                                                        </div>
                                                        <div class="whitebox-panel-graph-body2">
                                                            <img src="images/graph-small.jpg" alt="" />
                                                        </div>
                                                    </div>
                                                    <div class="whitebox-panel-graph-body">
                                                        <div class="whitebox-panel-graph-body1">
                                                            <span class="font-style1">Delayed Orders</span>
                                                            <span class="font-style3 pb-3">0</span>
                                                            <span class="font-style4" style="color:#868686;">+0</span>
                                                            <span class="font-style2">Previous Day</span>
                                                        </div>
                                                        <div class="whitebox-panel-graph-body2">
                                                            <img src="images/graph-small.jpg" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab2-week">
                                        <div class="row">
                                            <div class="col-md-12"> 2 </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab3-month">
                                        <div class="row">
                                            <div class="col-md-12"> 3 </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                      @include('branch.settings.branch_details.latest_orders')
                        <div class="whitebox-panel">
                            <div class="whitebox-panel-header">
                                <h5 class="left-border">Ads Manager</h5>
                                <a href="#" class="viewall">Open Ads Manager </a>
                            </div>
                            <p class="cl-gray">Reach for more Customers through ads</p>
                            <a href="#" class="btn-style-one"><i class="far fa-plus"></i>New Ad</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<!-- <script>
    $(window).scroll(function(){
      var sticky = $('.page-topbar'),
          scroll = $(window).scrollTop();

      if (scroll >= 100) sticky.addClass('fixedhead');
      else sticky.removeClass('fixedhead');
    });
</script> -->
<script src="js/menu.js"></script>
<script type="text/javascript">
    $("#cssmenu").menumaker({
        title: "",
        format: "multitoggle"
    });
</script>

    @include('branch.settings.branch_details.edit_branch_info')

@endsection
@section('scripts')
    @include('branch.settings.branch_details.script')
@endsection
