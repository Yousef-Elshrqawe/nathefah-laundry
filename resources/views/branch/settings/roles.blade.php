<div class="tab-pane " id="roleNav2">
    <div class="row">
        <div class="col-md-12">

            <h5 class="pb-4 left-border">Roles</h5>

            <h6 class="pb-2">All Roles</h6>
            <div class="row settings-permission-main">

                @foreach($roles as $item)
                        <div class="col-xl-4 col-lg-6 col-md-6" id="roleDiv-{{$item->id}}">
                            <div class="settings-permission-info">

                                <div class="settings-permission-infotop">
{{--                                    <div class="settings-permission-infotop1"><img src="{{asset('images/profile.jpg')}}" alt=""/></div>--}}
                                    <div class="settings-permission-infotop2">
                                        <h6>{{$item->name}}</h6>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center justify-content-between">
                                    <span>
                                        <a href="#" class="btn-style-two border" onclick="editRole({{$item->id}})">Edit role</a>
                                    </span>
                                    <span>
                                        <button class="btn-style-two backbtn2" onclick="deleteRole({{$item->id}})">Delete</button>
                                    </span>
                                </div>

                            </div>
                        </div>
                @endforeach
                <div class="col-xl-4 col-lg-6 col-md-6">
                    <a onclick="addNewRole()" style="cursor: pointer">
                        <div class="add-branch"><i class="fal fa-plus"></i> Add New Role</div>
                    </a>
                </div>
            </div>




        </div>
    </div>
</div>
