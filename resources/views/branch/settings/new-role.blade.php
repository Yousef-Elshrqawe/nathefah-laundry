<!-- ==============CREATE NEW USER MODAL===================== -->
<div class="modal fade" id="create-new-role" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form action="{{route('branch.setting.new.role')}}" method="post">
                    @csrf

                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Add Role </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">

                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="" class="form-label">Role Name</label>
                                <input type="text" name="name" class="form-control" id="" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed">
                                @error('name')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>



                        <div class="col-md-6">
                            <div class="form-box">

                                @foreach($permission as $value)
                                    <div class="checkbox-loop chkboxmain">
                                        {{$value->name}}
                                        <input id="permissions-{{ $value->id}}"  value="{{ $value->id }}"   name="permissions[]" type="checkbox">
                                        <label class="checkbox" for="permissions-{{ $value->id}}"></label>
                                    </div>
                                    @error('permissions')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                @endforeach


                            </div>
                        </div>



                    </div>

                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Add Role</button >
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
