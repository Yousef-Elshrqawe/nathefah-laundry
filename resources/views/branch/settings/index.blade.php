@extends('branch_temp')
@section('content')
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="page-topbar">
                                <div class="overview-box">
                                    <div class="row align-items-center flex-column-reverse flex-xl-row">
                                        <div class="col-xl-4 col-md-12">
                                            <div class="top-bar-title">
                                                <h5>Welcome  {{Session::get('username')}} </h5>
                                                <p class="cl-gray">Here is what happened in the branches today</p>
                                            </div>
                                        </div>


                                        <div class="col-xl-4 col-md-12">
                                            <div class="notify-box">
                                                {{--  <div class="notify-bell">
                                                      <p><a href="{{route('branch.get.notification')}}"><i class="fal fa-bell"></i>Notifications</a></p>
                                                  </div>--}}
                                                <div>
                                                    <p>{{now()->format('l  m-d')}}</p>
                                                </div>
                                                <div>
                                                    <a href="{{route('branch.logout')}}">log out</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <h4 class="title-inner">Settings</h4>

							<div class="whitebox-shadow">
								<div class="row">
									<div class="col-xl-3 col-lg-3 col-md-12 lefttab-nav">
										<ul class="nav nav-tabs">
											<li class="nav-item">
												<a class="nav-link active" data-bs-toggle="tab" href="#leftnav1">Basic info</a>
											</li>
											<li class="nav-item">
												<a class="nav-link " data-bs-toggle="tab" href="#leftnav2">Notifications</a>
											</li>
                                            <li class="nav-item">
                                                <a class="nav-link " data-bs-toggle="tab" href="#roleNav2">Roles</a>
                                            </li>
											<li class="nav-item">
												<a class="nav-link" data-bs-toggle="tab" href="#leftnav3">Users & Permissions</a>
											</li>
										</ul>
									</div>

									<div class="col-xl-9 col-lg-9 col-md-12">
										<div class="tab-content setting-body">

										    @include('branch.settings.basic_info')
											@include('branch.settings.notification')
											@include('branch.settings.user_permissions')
                                            @include('branch.settings.create-new-user')
                                            @include('branch.settings.edit-user')
                                            @include('branch.settings.roles')
                                            @include('branch.settings.new-role')
                                            @include('branch.settings.edit-role')


										</div>
									</div>
								</div>
							</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- <script>
        $(window).scroll(function(){
          var sticky = $('.page-topbar'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixedhead');
          else sticky.removeClass('fixedhead');
        });
    </script> -->
    <script src="js/menu.js"></script>
    <script type="text/javascript">
        $("#cssmenu").menumaker({
            title: "",
            format: "multitoggle"
        });
    </script>

@endsection
@section('scripts')
    @include('branch.settings.scripts')
@endsection
