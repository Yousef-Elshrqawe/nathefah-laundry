<script>


    /**
     * Update branch Info
     */

    function updateBranchInfo()
    {
        let username = $("#branchUserId").val();
        let address  = $("#branchAddressId").val();
        let phone    = $("#branchPhoneId").val();
        let status   = $("#branchStatusId").val();
        let open     = $("#branchOpenTimeId").val();
        let closed   = $("#branchCloseTimeId").val();
        let code     = $("#branchCountryCodeId").val();

        let branchId = $("#branchID").val();

        let url = '{{route("branch.setting.basic-info.update", ":id")}}';
        url = url.replace(':id', branchId);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: url,
            data:{
                username:username,
                address:address,
                phone:phone,
                status:status,
                open_time:open,
                closed_time:closed,
                country_code:code
            },
            beforeSend: function(){
            },
            success: function(response) {

                swal({
                    title:'Success!',
                    text:response.message,
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);



            },complete: function(){
                // setTimeout(function () {
                //     location.reload();
                // }, 1000);
            },error: function(error){

            }

        });
    }


    /**
     * Reset branch info
     */
    function restBranchInfo()
    {
        $("#branchUserId").val("");
        $("#branchEmailId").val("");
        $("#branchAddressId").val("");
        $("#branchPhoneId").val("");
        $("#branchStatusId").val("");
        $("#branchOpenTimeId").val("");
        $("#branchCloseTimeId").val("");

        let branchId = $("#branchID").val();

        let url = '{{route("branch.setting.basic-info.rest", ":id")}}';
        url = url.replace(':id', branchId);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "DELETE",
            url: url,
            beforeSend: function(){
            },
            success: function(response) {
                swal({
                    title:'Success!',
                    text:response.message,
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },complete: function(){
                // setTimeout(function () {
                //     location.reload();
                // }, 1000);
            },error: function(error){

            }

        });
    }


    function addNewUserBranch()
    {
        $('#create-new-user-branch-modal').modal('show');

    }

    function editUserBranch(id)
    {
        $('#edit-user-branch-modal').modal('show');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{route('branch.setting.permissions-edit.user')}}",
            data:{
                id:id
            },
            beforeSend: function(){
            },
            success: function(response) {
                $("#editUserId").val(id)
                $("#editUsername").val(response.data.username)
                $("#editUserEmail").val(response.data.email)
                $("#editUserCountryCode").val(response.data.country_code)
                $("#editUserPhone").val(response.data.phone)
                $("#editUserRole").val(response.data.roles[0].id + "-" + response.data.roles[0].name)

            },complete: function(){

            },error: function(error){

            }

        });

    }

    function addNewRole()
    {
        $('#create-new-role').modal('show');
    }

    function editRole(id)
    {
        $('#edit-role').modal('show');

        $("#editPermissions").empty();
        $("#editRoleId").val(id);
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{route('branch.role.edit')}}",
            data:{
                id:id
            },
            beforeSend: function(){
            },
            success: function(response) {
                let role = response.data.role;
                $("#editRole").val(role.name)

                let countPermissions = response.data.permissions.length;
                let Permissions      = response.data.permissions;

                for (let i=0; i<countPermissions; i++){
                    let isChecked = "";

                    let exist =   response.data.role.permissions.find(task => task.id === Permissions[i].id);

                    if (exist){
                        isChecked = "checked";
                    }

                    let role = `<div class="checkbox-loop chkboxmain">
                    ${Permissions[i].name}
                <input id="editPermissions-${Permissions[i].id}"  value="${Permissions[i].id}"   name="permissions[]" type="checkbox" ${isChecked}>
                                        <label class="checkbox" for="editPermissions-${Permissions[i].id}"></label>
                                    </div>`;
                    $("#editPermissions").append(role)
                }

            },complete: function(){

            },error: function(error){

            }

        });
    }


    /**
     * Delete user in branch
     * @param user_id
     */
    function deleteUserBranch(user_id)
    {
        let url = '{{route("branch.setting.permissions-delete.user", ":id")}}';
        url = url.replace(':id', user_id);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "DELETE",
            url: url,
            beforeSend: function(){
            },
            success: function(response) {

                //Delete div
                $(`#userInfo-${user_id}`).remove();

                swal({
                    title:'Success!',
                    text:response.message,
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },complete: function(){
            },error: function(error){

            }

        });
    }

    /**
     * Add or delete notification
     */
    function addDeleteNotification()
    {
        let option1 = $("[name='option1']:checked").val() == "on" ? 1 : 0;
        let option2 = $("[name='option2']:checked").val() == "on" ? 1 : 0;
        let option3 = $("[name='option3']:checked").val() == "on" ? 1 : 0;
        let option4 = $("[name='option4']:checked").val() == "on" ? 1 : 0;
        let option5 = $("[name='option5']:checked").val() == "on" ? 1 : 0;

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "POST",
            url: '{{route("branch.setting.notification.add.delete")}}',
            data:{
                option1 : option1,
                option2 : option2,
                option3 : option3,
                option4 : option4,
                option5 : option5,
            },
            beforeSend: function(){
            },
            success: function(response) {
                swal({
                    title:'Success!',
                    text:response.message,
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },complete: function(){
                // setTimeout(function () {
                //     location.reload();
                // }, 1000);
            },error: function(error){

            }

        });
    }

    /**
     * Rest notification
     */
    function resetNotifications()
    {

        // $("#resetNotification").change(function () {
            $('input:checkbox').attr('checked',false);
        // });

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "DELETE",
            url: '{{route("branch.setting.notification.rest")}}',
            beforeSend: function(){
            },
            success: function(response) {
                swal({
                    title:'Success!',
                    text:response.message,
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },complete: function(){
                // setTimeout(function () {
                //     location.reload();
                // }, 1000);
            },error: function(error){

            }

        });
    }


    /**
     * Delete user in branch
     * @param user_id
     */
    function deleteRole(role_id)
    {
        let url = '{{route("branch.role.delete", ":id")}}';
        url = url.replace(':id', role_id);

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "DELETE",
            url: url,
            beforeSend: function(){
            },
            success: function(response) {

                //Delete div
                $(`#roleDiv-${role_id}`).remove();

                swal({
                    title:'Success!',
                    text:response.message,
                    timer:5000,
                    type:'success'
                }).then((value) => {
                    //location.reload();
                }).catch(swal.noop);
            },complete: function(){
            },error: function(error){

            }

        });
    }

</script>
