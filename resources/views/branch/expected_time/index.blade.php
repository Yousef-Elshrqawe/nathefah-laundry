<?php
$active_links = ['days', ''];
?>

    @extends('branch_temp')

@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('branch.components.nav')



                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <p class="left-border">Expected time of services</p>

                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">Service Name</span></th>
                                        <th><span class="th-head-icon">Expected time of services</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($rows)

                                        @foreach($rows as $index => $row)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$row->service->name}}</td>
                                                <td class="text-info">{{$row->expected_time}}</td>
                                                <td>
                                                    <a href="{{route('branch.edit.expected.time', $row->id)}}"
                                                       class="btn-style-one btn btn-sm btn-info">
                                                        <i class="fas fa-edit fa-fw"></i> edit time
                                                    </a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                {{--<div class="justify-content-center d-flex">
                                    {!! $items->appends(Request::except('page'))->render() !!}
                                </div>--}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ============== CREATE NEW items MODAL===================== -->
    {{--
        <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">

                    <div class="modal-body">
                        <form id="msform" action="{{route('admin.items.store')}}" method="post">
                            @csrf
                            <ul id="progressbar">
                                <li class="active" id="account"></li>
                                <li id="personal"></li>
                                <li id="miscla"></li>
                                <li id="confirm"></li>
                            </ul>

                            <fieldset id="firsttab">
                                <div class="modal-header p-0 mb-5">
                                    <h5 class="left-border" id="head">@lang('admin.add_item')</h5>
                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            @foreach (config('translatable.locales') as $locale)
                                                <label for="" class="form-label">{{ __('admin.'.$locale.'.name') }}</label>
                                                --}}
    {{--  <label>{{ __('admin.'.$locale.'.name') }}</label>  --}}{{--

                                                --}}
    {{--  <input class="form-control" name="{{$locale}}[name]" value="{{$service->translateOrNew($locale)->name, true}}" type="text">  --}}{{--

                                                <input type="text" name="{{$locale}}[name]" class="form-control"
                                                       value="{{ old($locale . '.name') }}" aria-describedby="emailHelp"
                                                       placeholder="{{ __('admin.'.$locale.'.name') }}" required>
                                                @if ($errors->has($locale.'.name'))
                                                    <p class="alert-danger">{{ $errors->first($locale.'.name')}}</p>
                                                    --}}
    {{--  @error('{{$locale}}[name]')
                                                        <p class="alert-danger">{{ $message }}</p>
                                                    @enderror  --}}{{--

                                                @endif
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        --}}
    {{--  <div class="form-box">
                                            <label for="" class="form-label">@lang('admin.serial')</label>
                                            <input type="text" name="serial" class="form-control"  aria-describedby="emailHelp" placeholder="@lang('admin.serial')" required>
                                            @error('serial')
                                              <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>  --}}{{--


                                        <div class="form-box">
                                            <label> @lang('admin.category')</label>
                                            <select name="category_id" class="form-control">
                                                <option disabled selected>@lang('admin.select_category')</option>
                                                @if($categories && $categories -> count() > 0)
                                                    @foreach($categories as $category)
                                                        <option value="{{$category->id }}"> {{$category->name}}
                                                        </option>
                                                        @endforeach
                                                        @endif
                                                        </optgroup>
                                            </select>
                                            @error('category_id')
                                            <span class="text-danger"> {{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="btn-question mt-4">
                                    <button type="submit" name="next" class="next btn-style-one" value=""><i
                                            class="fas fa-plus"></i>@lang('admin.add_item')</button>
                                    <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                                </div>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    --}}

@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif

@endsection
