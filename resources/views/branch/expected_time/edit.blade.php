<?php
$active_links = ['days', ''];

?>
@extends('branch_temp')

@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <h5 class="left-border">Edit expected time of services</h5>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <div class="card-body">
                                    <form class="form" action="{{route('branch.update.expected.time' , ['id'=> $row -> id])}}" method="post" >
                                        @csrf

                                        <div class="form-body">

                                            <div class="row">
                                                <div class="col-md-6">
                                                    <label>Service Name</label>
                                                    <input type="text" class="form-control" name="start_time" value="{{$row->service->name}}"  disabled>
                                                </div>
                                                <div class="col-md-6">
                                                    <label>expected time</label>
                                                    <input type="number" class="form-control" name="expected_time" value="{{$row -> expected_time}}" required>
                                                    @if ($errors->has('expected_time'))
                                                        <p class="text-danger">{{ $errors->first('expected_time')}}</p>
                                                    @endif
                                                </div>
                                            </div>


                                        </div>

<br>
                                        <div class="form-actions">
                                              <button type="button" class="btn btn-warning mr-1"
                                                onclick="history.back();">
                                                <i class="ft-x"></i> back
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Update
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('scripts')

@endsection
