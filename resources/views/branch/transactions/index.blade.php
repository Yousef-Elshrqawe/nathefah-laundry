<?php
$active_links = ['days', ''];
?>

    @extends('branch_temp')

@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome  {{Session::get('username')}} </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>


                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            {{--  <div class="notify-bell">
                                                  <p><a href="{{route('branch.get.notification')}}"><i class="fal fa-bell"></i>Notifications</a></p>
                                              </div>--}}
                                            <div>
                                                <p>{{now()->format('l  m-d')}}</p>
                                            </div>
                                            <div>
                                                <a href="{{route('branch.logout')}}">log out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="row">

                            <div class="col-xl-6 col-md-6 mb-6">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Your percentage of completed orders</p>
                                        </div>
                                        {{-- <div class="dropdown btn-dropdown">
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                              <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                              <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <h4 class="cl-light"> {{ $rows->where('pending', 1)->sum('amount')  - $rows->where('pending', 1)->sum('application_percentage') }} SR</h4>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-6 mb-6">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Application percentage of completed applications</p>
                                        </div>
                                        {{-- <div class="dropdown btn-dropdown">
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                              <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                              <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <h4 class="cl-light"> {{ $rows->where('pending', 1)->sum('application_percentage') }} SR</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                    <h5 class="left-border">Transactions</h5>


                                <div class="table-box-select">
                                    <div class="d-select-box">
                                        <select id="select_last_order" name="date_filter" class="form-select" aria-label="Default select example">
                                            <option value="today" {{request()->get('date_filter') == 'today' ? 'selected' : ''}}>Today</option>
                                            <option value="this_week" {{request()->get('date_filter') == 'this_week' ? 'selected' : ''}}>This Week</option>
                                            <option value="this_month" {{request()->get('date_filter') == 'this_month' ? 'selected' : ''}}>This Month</option>
                                            <option value="last_month" {{request()->get('date_filter') == 'last_month' ? 'selected' : ''}}>Last Month</option>
                                            <option value="last_3_months" {{request()->get('date_filter') == 'last_3_months' ? 'selected' : ''}}>Last 3 Months</option>
                                            <option value="last_6_months" {{request()->get('date_filter') == 'last_6_months' ? 'selected' : ''}}>Last 6 Months</option>
                                            <option value="last_year" {{request()->get('date_filter') == 'last_year' ? 'selected' : ''}}>Last Year</option>
                                            <option value="this_year" {{request()->get('date_filter') == 'this_year' ? 'selected' : ''}}>This Year</option>
                                        </select>
                                    </div>
                                    <div class="d-selct-view">
                                        <a href="{{route('branch.transactions')}}" class="cl-light">View All</a>
                                    </div>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">amount</span></th>
                                        <th><span class="th-head-icon">type</span></th>
                                        <th><span class="th-head-icon">transaction type</span></th>
                                        <th><span class="th-head-icon">transaction number</span></th>
                                        <th><span class="th-head-icon">payment type</span></th>
                                        <th><span class="th-head-icon">status</span></th>
                                        <th><span class="th-head-icon">application percentage</span></th>
                                        <th><span class="th-head-icon">created at </span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($rows)

                                        @foreach($rows as $index => $row)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$row->amount ?? '---'}}</td>
                                                <td class="text-info">{{$row->type ?? '---'}}</td>
                                                <td class="text-info">{{$row->transaction_type ?? '---'}}</td>
                                                <td class="text-info">{{$row->transaction_number ?? '---'}}</td>
                                                <td class="text-info">{{$row->payment_type ?? '---'}}</td>
                                                <td class="text-info">{{$row->pending  == 0 ? 'Pending' : 'Completed'}}</td>
                                                <td class="text-info">{{$row->application_percentage ?? '---'}}</td>
                                                <td class="text-info">
                                                    {{ \Carbon\Carbon::parse($row->created_at)->format('Y-m-d H:i:s') ?? '---' }}
                                                </td>

                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $rows->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif
    <script>
        document.getElementById('select_last_order').addEventListener('change', function () {

            const selectedValue = this.value;


            const url = "{{ route('branch.transactions') }}?date_filter=" + selectedValue;


            window.location.href = url;
        });
    </script>
@endsection
