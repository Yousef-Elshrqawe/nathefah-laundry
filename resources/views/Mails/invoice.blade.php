<html lang="en"><head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style type="text/css">
        html, body{
            margin: 0.5rem 0;
            padding: 0;
        }

        *{
            box-sizing: border-box;
        }

        hr {
            display: block;
            height: 1px;
            border: 0;
            border-top: 1px solid rgba(0, 0, 0, 0.2);
            margin: 1em 0;
            padding: 0;
        }

        .container {
            width: 100%;
            padding-right: var(--bs-gutter-x, 0.75rem);
            padding-left: var(--bs-gutter-x, 0.75rem);
            margin-right: auto;
            margin-left: auto;
        }

        .invoice  .header img {
            max-width: 250px;
        }
        .invoice .header p {
            font-size: 1.5rem;
        }

        .invoice .clint-info {
            display: flex;
            justify-content: space-between;
        }

        .invoice .clint-info img {
            width: 250px;
            object-fit: contain;
        }

        .invoice  .order-items {
            background-color: rgba(32, 188, 227, 0.1);
            border-bottom-width: 2px;
            border-bottom-style: dashed;
            border-color: black;
            border-image: repeating-linear-gradient(to right,
            rgba(32, 188, 227, 1) 0,
            rgba(32, 188, 227, 1) 10px,
            transparent 10px,
            transparent 20px) 1;
            padding: unset;
            padding-top: 1rem;
            border-top-left-radius: 20px;
            border-top-right-radius: 20px;
            margin-top: 3rem;
        }

        .invoice .order-items h3 {
            color: rgba(32, 188, 227, 1);
            font-size: 1.7rem;
        }

        .invoice .quntity-price {
            display: grid;
            grid-template-columns: 2fr 1fr;
            gap: 30%;
        }


        .invoice  .quntity-price p {
            color: rgba(0, 0, 0, 0.5);
            font-size: 1.2rem;
        }

        .invoice  table {
            width: 100%;
            border-collapse: collapse;
            padding: 3rem;
            border-bottom-left-radius: 20px;
            border-bottom-right-radius: 20px;
        }

        .invoice  th,
        .invoice td {
            border-top: 1px solid rgba(0, 0, 0, 0.15);
            padding: 0.5rem 3rem;
            text-align: left;
            font-size: 2rem;

        }

        .invoice th {
            font-size: 2rem;
            color: rgba(32, 188, 227, 1);
            border-top: none;
            padding: 2rem 3rem;
        }

        .invoice tbody tr:last-child {
            background-color: rgba(32, 188, 227, 0.1);
            border-top-width: 2px;
            border-top-style: dashed;
            border-color: rgba(32, 188, 227, 1);
            border-image: repeating-linear-gradient(to right,
            rgba(32, 188, 227, 1) 0,
            rgba(32, 188, 227, 1) 10px,
            transparent 10px,
            transparent 20px) 1;
        }

        .invoice tbody tr td:last-child {
            text-align: end;
        }

        .invoice tbody tr:last-child td {
            color: rgba(32, 188, 227, 1);
        }

        .invoice .inovice-btn-con {
            display: flex;
            justify-content: center;
        }

        .invoice .inovice-btn {
            margin: auto;
            margin-top: 3rem;
            background-color: rgba(32, 188, 227, 1);
            color: white;
            border-radius: 20px;
            padding: 2rem 0;
            border: none;
            width: 50%;
            font-size: 1.5rem;
            text-align: center;
            text-decoration: none;
        }

        .invoice .order-info {
            padding: 0rem ;
        }

        .invoice .order-items tbody tr:last-child {
            background-color: unset;
            border: none;
        }

        .invoice .order-items tbody tr td {
            border: unset;
            padding: 0;
        }

        .invoice  .order-items tbody tr td p {
            color: rgba(0, 0, 0, 0.5);
        }

        .invoice .order-items tbody tr:last-child td {
            color: unset;
        }

        .invoice .order-items tbody tr h3 {
            color: unset;
        }
        .invoice .order-items th {
            padding: 1.5rem 0rem;
        }
        .invoice .order-info th,.invoice .order-info td{
            padding: 1rem 3rem;
        }


        @media (max-width: 992px) {
            .invoice .header p{
                font-size: 1rem;
            }

            .invoice .clint-info img {
                width: 135px !important;
            }

            .invoice th{
                font-size: 1.5rem;
            }

            .invoice .order-items tbody tr td{
                font-size: 1.2rem;
            }

            .invoice .order-items tbody tr h3{
                font-size: 1.2rem;
                padding: unset;
                margin: 0;
            }

            .invoice .order-items tbody tr td p{
                font-size: 1.2rem;
                margin-top: 0.3rem;
                margin-bottom: 1rem;
            }

            .invoice .inovice-btn{
                width: 100%;
                padding: 1rem;
                font-size: 1rem;
                color: white;
            }

            .order-info th{
                font-size: 1.5rem;
            }

            .order-info td{
                font-size: 1rem;
            }

            .invoice .order-info th, .invoice .order-info td{
                padding: 1rem;
            }
        }
        @media (max-width:320px) {
            .invoice .header img{
                width: 220px;
            }
        }

        @media (min-width:767px) {
            .invoice .clint-info{
                padding: 0rem 3.75rem;
            }
            .invoice .header{
                padding: 0rem 3.75rem;
            }

        }
        .order-items tr td,.order-items tr th {
            padding: 1rem 3rem  !important;
        }
    </style>
</head>


<body>
<div class="invoice">

    <div class="container header">
        <img src="https://nathefa.shiftcodes.net/Logo.png" alt="" srcset="">
        <h4 style="color: rgba(32, 188, 227, 1)">{{$laundry->name}}</h4>
        @if($laundry->tax_number)
            <h3>CRN: {{$laundry->tax_number}}</h3>
        @endif
        <hr style="color: gray"/>
        <p>{{ date('d-m-Y', strtotime($order->created_at))  }}</p>
    </div>
    <div class="container clint-info">
        <div>
            <h4>Name: {{$order->customer_name}}</h4>
            <h4>Phone: {{$order->customer_phone}}</h4>
            <h4>Address: {{$order->customer_location}}</h4>
        </div>
        <img src="https://app.nathefah.com/qr_codes/{{$order->id}}.png" alt="">
    </div>
    <div class="container">
        <div class=" order-items">
            <table>
                <thead>
                <tr>
                    <th colspan="2">Items</th>
                </tr>
                </thead>
                <tbody>
                {{--          <tr>
                              <td>
                                  <h3>Washing</h3>
                                  <p>1 pants | 3 jackets | 2 shirts</p>
                              </td>
                              <td>75 SAR</td>
                          </tr>--}}

                @foreach ($order->orderdetailes as $detail)

                    @if ($detail->additionalservice_id==null)

                        @php  $serviceprice=$detail->price @endphp
                        @foreach ($order->orderdetailes as  $additional)
                            @if ($additional->additionalservice_id!=null && $additional->service_id==$detail->service_id && $additional->branchitem_id==$detail->branchitem_id)
                                @php $serviceprice+=$additional->price     @endphp
                            @endif
                        @endforeach
                        @foreach ($urgents as $urgent)
                            @if ($urgent->service_id==$detail->service_id && $urgent->branchitem_id==$detail->branchitem_id)
                                @php $serviceprice+=$urgent->price     @endphp
                            @endif
                        @endforeach

                        <tr>
                            <td>
                                <h3>{{$detail->service->name}}</h3>
                                <p>{{$detail->quantity}} {{$detail->Branchitem->name}}</p>
                            </td>
                            <td>{{$serviceprice}}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

        </div>
        <div class=" order-info">
            <table>
                <thead>
                <tr>
                    <th colspan="2">Order Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Order ID:</td>
                    <td>{{$order->id}}</td>
                </tr>
                <tr>
                    <td>Order Payment type:</td>
                    <td>  @if ($order->pymenttype_id ==1)
                            wallet
                        @elseif ($order->pymenttype_id ==3)
                            visa
                        @else
                            cash
                        @endif</td>
                </tr>
                <tr>
                    <td>Delivery type:</td>
                    <td>{{$order->deliverytype->name ?? 'No delivery'}}</td>
                </tr>
                <tr>
                    <td>Delivery fees:</td>
                    <td>{{$order->delivery_fees}}</td>
                </tr>
                <tr>
                    <td>Tax:</td>
                    <td>{{$order->taxes}}</td>
                </tr>
                <tr>
                    <td>Discount:</td>
                    <td>
                        @if ($order->discounttype==null)
                            0
                        @else
                            {{$order->price_before_discount-$order->price_after_discount}}
                        @endif
                    </td>
                </tr>
                <tr>
                    <td>Total order</td>
                    <td>
                        @if ($order->discounttype==null)
                            {{$order->price_before_discount}}
                        @else
                            {{$order->price_after_discount}}
                        @endif
                        SAR
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container inovice-btn-con">
        <a href="tel:{{$setting->country_code}}{{$setting->support_phone}}" class="inovice-btn" style="color: white">
            Call Us: {{$setting->country_code}}{{$setting->support_phone}}
        </a>
    </div>

</div>
</body>
</html>
