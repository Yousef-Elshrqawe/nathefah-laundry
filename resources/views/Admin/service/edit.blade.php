<?php
$active_links = ['services' , ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.service_edit')</h5>
                                    <div class="search-box">

                                    </div>


                                {{--  <div class="table-box-select">
                                    <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Sort By : Latest</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Subscription  : All</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>  --}}
                            </div>




                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <div class="card-body">
                                    <form class="form" action="{{route('admin.services.update' , ['id'=> $service -> id])}}" method="post"
                                        enctype="multipart/form-data">
                                        @csrf

                                        <div class="form-body">


                                            {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                            <div class="row">


                                                @foreach (config('translatable.locales') as $locale)
                                                    <label>{{ __('admin.'.$locale.'.name') }}</label>
                                                    <input class="form-control" name="{{$locale}}[name]" value="{{$service->translateOrNew($locale)->name, true}}" type="text" required>
                                                    @if ($errors->has($locale.'.name'))
                                                    <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                                                    @endif
                                                @endforeach
                                                {{--  @foreach (array_keys(config('laravellocalization.supportedLocales')) as
                                                $locale)
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{ __('admin/forms.'.$locale.'.name') }}</label>
                                                        <input type="text" name="{{ $locale }}[name]"
                                                            class="form-control" value="{{ $service->translate($locale)->name }}">
                                                        @error($locale.'.name')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                @endforeach  --}}
                                                    <div class="col-md-6">
                                                        <label>expected time</label>
                                                        <input type="number" class="form-control" name="expected_time"  value="{{$service -> expected_time}}" required>
                                                        @if ($errors->has('expected_time'))
                                                            <p class="text-danger">{{ $errors->first('expected_time')}}</p>
                                                        @endif
                                                    </div>

                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="col-sm-8">
                                                            <div class="form-group">
                                                                <label for="image"> {{ __('admin.image') }} </label>
                                                                <input  type="file"  accept="image/*"   id="image" class="form-control" name="image">
                                                                @error("image")
                                                                <span class="text-danger">{{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-sm-4">

                                                            <img src="{{$service->img}}" class="img-thumbnail" style="margin-top: 2.3rem !important;width: 50px;">
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

<br>
                                        <div class="form-actions">
                                            {{--  <button type="button" class="btn btn-warning mr-1"
                                                onclick="history.back();">
                                                <i class="ft-x"></i> تراجع
                                            </button>  --}}
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Update
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('scripts')

@endsection
