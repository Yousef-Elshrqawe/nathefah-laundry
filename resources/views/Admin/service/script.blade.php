<script>
    function categories(id)
    {
        $('#categories-modal').modal('show');
        $("#service_id").val(id)
        document.getElementById("serviceCategoryForm").reset();

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{route('admin.get.services.category')}}",
            data:{
                id:id
            },
            beforeSend: function(){
            },
            success: function(response) {
                const val = document.querySelector('input').value;
                for (let i =0; i < response.data.length; i++)
                {
                    $(`#${response.data[i].category_id}`).prop( "checked", true );
                }

            },complete: function(){

            },error: function(error){

            }

        });
    }

</script>
