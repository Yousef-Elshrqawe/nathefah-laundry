<?php
$active_links = ['services' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.services')</h5>
                                </div>

                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-service-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.add_services') </a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">#</th>
                                            <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                            <th><span class="th-head-icon">expected time (min)</span></th>
                                            <th><span class="th-head-icon">@lang('admin.image')</span></th>
                                            <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($services as $index => $service)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$service->name}}</td>
                                                <td class="text-info">{{$service->expected_time}}</td>
                                                <td>
                                                    <img src="{{$service->img}}" class="img-thumbnail" style="width: 50px;">
                                                </td>
                                                <td>
                                                    <a href="{{route('admin.services.edit', ['id'=> $service -> id])}}"
                                                       class="cl-light">@lang('admin.edit')
                                                    </a>

                                                    <a  class="cl-green m-3" style="cursor: pointer" onclick="categories({{$service->id}})">Categories</a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $services->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('Admin.service.create')
    @include('Admin.service.category')


@endsection


@section('scripts')

@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-service-modal').modal('show');
        });
</script>
@endif
    @include('Admin.service.script')
@endsection
