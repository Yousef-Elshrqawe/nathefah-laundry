<!-- ==============CREATE NEW Discount MODAL===================== -->
<div class="modal fade" id="categories-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form id="serviceCategoryForm" action="{{route('admin.services.category')}}" method="POST">
                @csrf

                <input type="hidden" name="service_id" id="service_id">

                <div class="modal-body" id="modalData">
                        <div class="modal-header p-0 mb-5">
                            <h5 class="left-border" id="head">Categories</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="row">
                            <div class="form-box">
                                @foreach($categories as $category)
                                    <div class="checkbox-loop chkboxmain">
                                        {{$category->name}}
                                        <input id="{{$category->id}}" value="{{$category->id}}"  name="category_ids[]" type="checkbox">
                                        <label class="checkbox" for="{{$category->id}}"></label>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="btn-question mt-4">
                            <button  type="submit" class="btn-style-one">
                                <i class="fas fa-plus"></i>
                                Add
                            </button >
                        </div>
                </div>
            </form>
        </div>
    </div>
</div>



