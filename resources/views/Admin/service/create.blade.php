<!-- ==============CREATE NEW Discount MODAL===================== -->
<div class="modal fade" id="new-service-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="" action="{{route('admin.services.store')}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <fieldset id="firsttab">
                        <div class="modal-header p-0 mb-5">
                            <h5 class="left-border" id="head">@lang('admin.add_services')</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-box">
                                    @foreach (config('translatable.locales') as $locale)
                                        <label for="" class="form-label">{{ __('admin.'.$locale.'.name') }}</label>
                                        <input type="text" name="{{$locale}}[name]" class="form-control" value="{{ old($locale . '.name') }}" aria-describedby="emailHelp" placeholder="{{ __('admin.'.$locale.'.name') }}" required>
                                        @if ($errors->has($locale.'.name'))
                                            <p class="alert-danger">{{ $errors->first($locale.'.name')}}</p>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                            <div class="col-md-6">
                                <label>expected time</label>
                                <input type="number" class="form-control" name="expected_time"  required>
                                @if ($errors->has('expected_time'))
                                    <p class="text-danger">{{ $errors->first('expected_time')}}</p>
                                @endif
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="" class="form-label">@lang('admin.image')</label>
                                    <input  type="file"  accept="image/*"   name="image" class="form-control"  aria-describedby="emailHelp" placeholder="@lang('admin.image')" required>
                                    @error('image')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>


                        </div>
                        <div class="btn-question mt-4">
                            <button  type="submit" name="next" class="next btn-style-one" value="" ><i class="fas fa-plus"></i>@lang('admin.add_services')</button >
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
