<?php
$active_links = ['drivers' , ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">





                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <div class="card-body">
                                    <form class="form" action="{{route('admin.drivers.update' ,  ['id'=> $row -> id])}}" method="post">
                                        @csrf
                                        <ul id="progressbar">
                                            <li class="active" id="account"></li>
                                            <li id="personal"></li>
                                            <li id="miscla"></li>
                                            <li id="confirm"></li>
                                        </ul>

                                        <fieldset id="firsttab">
                                            <div class="modal-header p-0 mb-5">
                                                <h5 class="left-border" id="head">@lang('admin.drivers_edit')</h5>

                                            </div>
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">{{ __('admin.name') }}</label>
                                                        <input type="text" name="name" class="form-control"
                                                               value="{{ $row->name }}" aria-describedby="emailHelp"
                                                               placeholder="{{ __('admin.name') }}" required>
                                                        @if ($errors->has('name'))
                                                            <p class="alert-danger">{{ $errors->first('name')}}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.country_code')</label>
                                                        <input type="text" name="country_code" class="form-control"
                                                               value="+966"
                                                               disabled aria-describedby="emailHelp"
                                                               placeholder="@lang('admin.country_code')" required>
                                                        @if ($errors->has('country_code'))
                                                            <p class="alert-danger">{{ $errors->first('country_code')}}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.phone')</label>
                                                        <input type="text" name="phone" class="form-control"
                                                               value="{{ $row->phone }}" aria-describedby="emailHelp"
                                                               placeholder="5xxxxxxxx" required>
                                                        @if ($errors->has('phone'))
                                                            <p class="alert-danger">{{ $errors->first('phone')}}</p>
                                                        @endif
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.email')</label>
                                                        <input type="email" name="email" class="form-control"
                                                               value="{{ $row->email }}" aria-describedby="emailHelp"
                                                               placeholder="@lang('admin.email')" required>
                                                        @if ($errors->has('email'))
                                                            <p class="alert-danger">{{ $errors->first('email')}}</p>
                                                        @endif
                                                    </div>
                                                </div>



                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.laundry')</label>
                                                        <select name="laundry_id" class="form-control" required  id="laundry_id">
                                                            <option value="{{$row->branch->laundry->id}}" selected>{{$row->branch->laundry->name}}</option>
                                                        @foreach($laundries as $laundry)
                                                            @if($laundry->id == $row->branch->laundry->id)
                                                                @continue
                                                            @endif
                                                                <option value="{{$laundry->id}}">{{$laundry->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('laundry_id'))
                                                            <p class="alert-danger">{{ $errors->first('laundry_id')}}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.branch')</label>
                                                        <select name="branch_id" class="form-control" required  id="branch_id">
                                                            <option value="{{$row->branch_id}}" selected>{{$row->branch->username}}</option>
                                                        </select>
                                                        @if ($errors->has('branch_id'))
                                                            <p class="alert-danger">{{ $errors->first('branch_id')}}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                     {{--           <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.password')</label>
                                                        <input type="password" name="password" class="form-control"
                                                               value="{{ old('password') }}" aria-describedby="emailHelp"
                                                               placeholder="@lang('admin.password')" >
                                                        @if ($errors->has('password'))
                                                            <p class="alert-danger">{{ $errors->first('password')}}</p>
                                                        @endif
                                                    </div>
                                                </div>


                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label for="" class="form-label">@lang('admin.password_confirmation')</label>
                                                        <input type="password" name="password_confirmation" class="form-control"
                                                               value="{{ old('password_confirmation') }}" aria-describedby="emailHelp"
                                                               placeholder="@lang('admin.password_confirmation')" >
                                                        @if ($errors->has('password_confirmation'))
                                                            <p class="alert-danger">{{ $errors->first('password_confirmation')}}</p>
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>--}}


                                            <div class="btn-question mt-4">
                                                <button type="submit" name="next" class="next btn-style-one" value=""><i
                                                        class="fas fa-plus"></i>@lang('admin.drivers_edit')</button>
                                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                                            </div>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('scripts')
            <script>
                $(document).ready(function () {
                    $('#laundry_id').on('change', function () {
                        var laundry_id = $(this).val();
                        console.log(laundry_id);
                        if (laundry_id) {
                            $.ajax({
                                url: "{{ URL::to('Super-admin/getBranchesAjax') }}/" + laundry_id,
                                type: 'GET',
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                    $('#branch_id').empty();
                                    $.each(data, function (index, subcategory) {
                                        $('#branch_id').append('<option value="' + subcategory.id + '">' + subcategory.username + '</option>');
                                    });
                                }
                            });
                        } else {
                            $('#branch_id').empty();
                        }
                    });
                });
            </script>
@endsection
