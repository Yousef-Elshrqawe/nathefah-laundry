<?php
$active_links = ['drivers', ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.drivers')</h5>
                                    <div class="search-box">
                                    </div>
                                </div>

                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal"
                                       data-bs-target="#new-item-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.add_drivers') </a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</th>
                                        <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.email')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.phone')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.status')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.orders_count')</span></th>
                                        <th><span class="th-head-icon">Branches</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($rows)

                                        @foreach($rows as $index => $row)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$row->name}}</td>
                                                <td class="text-info">{{$row->email}}</td>
                                                <td class="text-info">{{$row->country_code}} {{$row->phone}}</td>
                                                <td class="text-info">{{$row->status}}</td>
                                                <td class="text-info">{{$row->orders->count()}}</td>
                                                <td class="text-info">{{$row->branch->username}}</td>
                                                <td>
                                                    <a href="{{route('admin.drivers.edit', ['id'=> $row -> id])}}"
                                                       class="cl-light">@lang('admin.edit')</a>


                                                        <a href="{{route('admin.drivers.delete', ['id'=> $row -> id])}}" class="cl-danger"
                                                        style="color: red;" onclick="return confirm('Are you sure you want to delete this Drivers?');"
                                                        >@lang('admin.delete')</a>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $rows->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ============== CREATE NEW items MODAL===================== -->
    <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form id="msform" action="{{route('admin.drivers.store')}}" method="post">
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="account"></li>
                            <li id="personal"></li>
                            <li id="miscla"></li>
                            <li id="confirm"></li>
                        </ul>

                        <fieldset id="firsttab">
                            <div class="modal-header p-0 mb-5">
                                <h5 class="left-border" id="head">@lang('admin.add_drivers')</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">{{ __('admin.name') }}</label>
                                        <input type="text" name="name" class="form-control"
                                               value="{{ old('name') }}" aria-describedby="emailHelp"
                                               placeholder="{{ __('admin.name') }}" required>
                                        @if ($errors->has('name'))
                                            <p class="alert-danger">{{ $errors->first('name')}}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.country_code')</label>
                                        <input type="text" name="country_code" class="form-control"
                                               value="+966"
                                               disabled aria-describedby="emailHelp"
                                               placeholder="@lang('admin.country_code')" >
                                        @if ($errors->has('country_code'))
                                            <p class="alert-danger">{{ $errors->first('country_code')}}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.phone')</label>
                                        <input type="number" name="phone" class="form-control"
                                               value="{{ old('phone') }}" aria-describedby="emailHelp"
                                               placeholder="5xxxxxxxx" required>
                                        @if ($errors->has('phone'))
                                            <p class="alert-danger">{{ $errors->first('phone')}}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.email')</label>
                                        <input type="email" name="email" class="form-control"
                                               value="{{ old('email') }}" aria-describedby="emailHelp"
                                               placeholder="@lang('admin.email')" required>
                                        @if ($errors->has('email'))
                                            <p class="alert-danger">{{ $errors->first('email')}}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.laundry')</label>
                                        <select name="laundry_id" class="form-control" required  id="laundry_id">
                                            <option selected value="">Choose Laundry</option>
                                            @foreach($laundries as $laundry)
                                                <option value="{{$laundry->id}}">{{$laundry->name}}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('laundry_id'))
                                            <p class="alert-danger">{{ $errors->first('laundry_id')}}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.branch')</label>
                                        <select name="branch_id" class="form-control" required  id="branch_id">
                                            <option value="">@lang('admin.select_branch')</option>
                                        {{--    @foreach($branches as $branch)
                                                <option value="{{$branch->id}}">{{$branch->username}}</option>
                                            @endforeach--}}
                                        </select>
                                        @if ($errors->has('branch_id'))
                                            <p class="alert-danger">{{ $errors->first('branch_id')}}</p>
                                        @endif
                                    </div>
                                </div>







                               {{-- <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.password')</label>
                                        <input type="password" name="password" class="form-control"
                                               value="{{ old('password') }}" aria-describedby="emailHelp"
                                               placeholder="@lang('admin.password')" required>
                                        @if ($errors->has('password'))
                                            <p class="alert-danger">{{ $errors->first('password')}}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.password_confirmation')</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                               value="{{ old('password_confirmation') }}" aria-describedby="emailHelp"
                                               placeholder="@lang('admin.password_confirmation')" required>
                                        @if ($errors->has('password_confirmation'))
                                            <p class="alert-danger">{{ $errors->first('password_confirmation')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>--}}
                            </div>

                            <div class="btn-question mt-4">
                                <button type="submit" name="next" class="next btn-style-one" value=""><i
                                        class="fas fa-plus"></i>@lang('admin.add_users')</button>
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $(document).ready(function () {
            $('#laundry_id').on('change', function () {
                var laundry_id = $(this).val();
                console.log(laundry_id);
                if (laundry_id) {
                    $.ajax({
                        url: "{{ URL::to('Super-admin/getBranchesAjax') }}/" + laundry_id,
                        type: 'GET',
                        dataType: 'json',
                        success: function (data) {
                            console.log(data);
                            $('#branch_id').empty();
                            // إضافة الخيار الافتراضي
                            $('#branch_id').append('<option value="" selected>Choose branch</option>');

                            $.each(data, function (index, subcategory) {
                                $('#branch_id').append('<option value="' + subcategory.id + '">' + subcategory.username + '</option>');
                            });
                        }
                    });
                } else {
                    $('#branch_id').empty();
                }
            });
        });
    </script>




@endsection
