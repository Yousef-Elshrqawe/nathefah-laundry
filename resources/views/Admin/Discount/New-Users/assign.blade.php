<?php
$active_links = ['discount' , ''];
?>

@extends('Admin_temp')
@section('style')

<link rel="stylesheet" type="text/css" href="{{asset('dashboard/assets/select2/dist/css/select2.min.css')}}"/>
<link rel="stylesheet" href="{{asset('dashboard/css/filter_multi_select.css')}}" />
<script src="{{asset('dashboard/js/filter-multi-select-bundle.min.js')}}"></script>

@endsection
@section('content')

<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-topbar">
                        <div class="overview-box">
                            <div class="row align-items-center flex-column-reverse flex-xl-row">
                                <div class="col-xl-4 col-md-12">
                                    <div class="top-bar-title">
                                        <h5>Welcome Ahmed ! </h5>
                                        <p class="cl-gray">Here is what happened in the branches today</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="search-box">
                                        <input type="text" name="" id="" class="form-control" placeholder="Search on Order, Driver, Branch">
                                        <i class="fal fa-search"></i>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="notify-box">
                                        <div class="notify-bell">
                                            <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                        </div>
                                        <div>
                                            <p>Sun 24 July 4:35 PM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-12 mb-4 mt-3">
                <div class="overview-box">
                    <div class="table-box">
                        <div class="tab-flex">
                            <div>
                                <h5 class="left-border">Discounts</h5>
                            </div>

                        </div>
                        <div class="table-box-select">
                            {{-- <div class="d-select-box">
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Sort by date</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="d-select-box">
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Branch Riyadh</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div> --}}
                            @if($discount->assigned==false)
                                <div class="d-selct-view">
                                    <a href="{{route('Admin.assign.discount',$discount->id)}}" class="btn-style-one"><i class="fas fa-plus"></i> Assign All user</a>
                                </div>
                            @endif
                            <form action="{{route('assigned.user')}}" method="post">
                                <input type="hidden" value="" id="user_id" name="user_id">
                                <input type="hidden" value="{{$discount->id}}" name="discount_id">
                                @csrf
                                <div class="d-flex">
                                    <input type="text" id="name" name="user" value="" class="form-control" placeholder="assigned user">
                                    <input type="submit" class="btn btn-success m-2" value="assgined">

                                </div>
                                <ul id="ul_user">

                                </ul>
                            </form>
                            {{-- <a href="{{route('assigned.user')}}" class="btn-style-one"><i class="fas fa-plus"></i> Assign user</a> --}}

                        </div>
                    </div>



                    <div class="tab-content">
                        <div class="tab-pane active" id="tabdel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table_responsive_maas">
                                        @include('Admin.Discount.tablecomponant.user',['discountusers'=>$discountusers])

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


        </div>
    </div>
</div>
<!-- ==============Start User MODAL===================== -->
{{-- <div class="modal fade" id="user-modal" tabindex="-1" aria-labelledby="branch-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="msform" action="{{route('Admin.assign.discount')}}" method="post">
                    <input type="hidden" name="id" value="{{$discount->id}}">
                    @csrf
                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>

                    <fieldset id="firsttab">
	                	<div class="modal-header p-0 mb-5">
                           <h5 class="left-border" id="head">Assign Discount</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="row">
                            <label>users</label>
                            <select name="users[]"  multiple name="users" id="users" class="filter-multi-select">
                                @foreach ($users as $user)
                                     <option value="{{$user->id}}">{{$user->name}}</option>
                                @endforeach
                            </select>
                            </div>
                             <div class="form-group">
                               <label>number of usage</label>
                               <input class="form-control" name="usage" type="number" value="1">
                             </div>

                            <div class="btn-question mt-4">
                                <button  type="submit" name="next" class="next btn-style-one" value="" ><i class="fas fa-plus"></i> Add Discount</button >
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div> --}}
<!-- =============End User MODAL===================== -->
@endsection
@section('scripts')
<script src="{{asset('dashboard/js/filter-multi-select-bundle.min.js')}}"></script>

<script src="{{asset('dashboard/assets/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('dashboard/assets/select2/dist/js/select2.min.js')}}"></script>
<script>
  //*************//  dashboard
  // For select 2
  //*************//
function enable() {
$('input:disabled, select:disabled').each(function () {
   $(this).removeAttr('disabled');
});
}

  $(".select2").select2();


</script>

<script>
    var SITEURL = "{{ url('/Super-admin') }}";
       $('#name').on("keyup",function(){
        var name=$('#name').val();
               $.ajax({
               url:SITEURL+"/search/user/"+name,
               type:"GET", //send it through get method
               success: function (response) {
                var result='';
                for (const item of response) {
                result+='<li class="list-group-item"><a class="usermember" data-name="'+item.name+'" data-id="'+item.id+'">'+item.name+'</a></li>';
                }
                    $('#ul_user').html(result);
                    $('.usermember').click(function(){
                       $('#name').val($(this).attr('data-name'));
                       $('#user_id').val($(this).attr('data-id'));
                       $('#ul_user').html('');
                     });
               },
               error: function(response) {
                   console.log('fdfd');
               }
             });
       });



</script>


@endsection
