<?php
$active_links = ['discount' , 'edit-discount'];
?>

@extends('Admin_temp')
@section('content')


<!-- ==============Edit NEW Discount MODAL===================== -->
<div class="main-content">

    <div class="page-content">
    <div class="modal-dialog">
        @include('Admin.components.nav')
        <div class="modal-content">

            <div class="modal-body">
                <form id="msform" action="{{route('Admin.new.users.discount.update')}}" method="post">
                    @csrf
                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>

                    <fieldset id="firsttab">
	                	<div class="modal-header p-0 mb-5">
                           <h5 class="left-border" id="head">Edit Discount </h5>
                           <input type="hidden" name="id" id="id" value="{{$discount->id}}">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discound code</label>
                                        <input type="text" name="code" class="form-control" id="code" aria-describedby="emailHelp" value="{{$discount->code}}"  placeholder="Enter Discount Code">
                                        @error('code')
                                           <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discount percent</label>
                                        <input type="number" name="percent" class="form-control" id="percent" aria-describedby="emailHelp" value="{{$discount->percent}}" placeholder="Discount Percent">
                                        @error('percent')
                                          <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discount Amount</label>
                                        <input type="number" name="amount" class="form-control" id="amount" aria-describedby="emailHelp" value="{{$discount->amount}}" placeholder="Discount Amount">
                                        @error('amount')
                                           <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Order Min price</label>
                                        <input type="number" name="min_order_price" class="form-control" id="min_order_price" aria-describedby="emailHelp" value="{{$discount->min_order_price}}" placeholder="Enter Order Min Price">
                                        @error('min_order_price')
                                         <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Start Date</label>
                                        <input type="datetime-local" name="start_date" class="form-control" id="start_date" aria-describedby="emailHelp" value="{{$discount->start_date}}" placeholder="Enter Order Min Price">
                                        @error('start_date')
                                        <p class="alert-danger">{{ $message }}</p>
                                       @enderror

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">End Date</label>
                                        <input type="datetime-local" name="enddate" class="form-control" id="enddate" aria-describedby="emailHelp" value="{{$discount->enddate}}" placeholder="Enter Order Min Price">
                                        @error('enddate')
                                        <p class="alert-danger">{{ $message }}</p>
                                       @enderror

                                    </div>
                                </div>


                            </div>
                            <div class="btn-question mt-4">
                                <button  type="submit" name="next" class="next btn-style-one" value="" id="add_discount"><i class="fas fa-plus"></i> Edit Discount</button >
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>


@endsection


    @section('scripts')
        <script src="js/menu.js"></script>
        <script type="text/javascript">
            $("#cssmenu").menumaker({
                title: "",
                format: "multitoggle"
            });
        </script>

        <script>
            $(document).ready(function(){

                $("#last-btn").click(function(){
                    $("#progressbar").addClass("hide");
                });

                var current_fs, next_fs, previous_fs; //fieldsets
                var opacity;
                var current = 1;
                var steps = $("fieldset").length;

                setProgressBar(current);

                $(".next").click(function(){

                    //current_fs = $(this).parent();
                    // next_fs = $(this).parent().next();
                    current_fs = $(this).closest("fieldset");
                    next_fs = $(this).closest("fieldset").next();
                    //alert(next_fs);
                    //Add Class Active
                    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                    //show the next <fieldset></fieldset>
                    next_fs.show();
                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function(now) {
                            // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            next_fs.css({'opacity': opacity});
                        },
                        duration: 500
                    });
                    setProgressBar(++current);
                });

                $(".previous").click(function(){

                    current_fs = $(this).closest("fieldset");
                    previous_fs = $(this).closest("fieldset").prev();

                    //Remove class active
                    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                    //show the previous fieldset
                    previous_fs.show();

                    //hide the current fieldset with style
                    current_fs.animate({opacity: 0}, {
                        step: function(now) {
                            // for making fielset appear animation
                            opacity = 1 - now;

                            current_fs.css({
                                'display': 'none',
                                'position': 'relative'
                            });
                            previous_fs.css({'opacity': opacity});
                        },
                        duration: 500
                    });
                    setProgressBar(--current);
                });

                function setProgressBar(curStep){
                    var percent = parseFloat(100 / steps) * curStep;
                    percent = percent.toFixed();
                    $(".progress-bar")
                        .css("width",percent+"%")
                }

                $(".submit").click(function(){
                    return false;
                })

            });
        </script>

        <script>
            $(document).ready(function() {
                $('.minus').click(function () {
                    var $input = $(this).parent().find('input');
                    var count = parseInt($input.val()) - 1;
                    count = count < 0 ? 1 : count;
                    $input.val(count);
                    $input.change();
                    return false;
                });
                $('.plus').click(function () {
                    var $input = $(this).parent().find('input');
                    $input.val(parseInt($input.val()) + 1);
                    $input.change();
                    return false;
                });
            });
        </script>
        @if (Session::get('errors')!=null)
            <script>
                $(document).ready(function() {
                    $('#new-order-modal').modal('show');
                });
            </script>
        @endif
        <script>



            $('#discount_amount').click(function(){
                $('#discount_percent').val(0);
            });
            $('#discount_percent').click(function(){
                $('#discount_amount').val(0);
            });

            $('.edit_button').click(function(){
                var id=$(this).attr("data-id");
                $.ajax({
                    url:SITEURL+"/discount/edit/"+id,
                    type:"GET", //send it through get method
                    success: function (response) {
                        $('#discount_id').val(response.id);
                        $('#code').val(response.code);
                        $('#amount').val(response.amount);
                        $('#percent').val(response.percent);
                        $('#min_order_price').val(response.min_order_price);
                        $('#start_date').val(response.start_date);
                        $('#enddate').val(response.enddate);
                        $("#edit-order-modal").modal('toggle');
                    },
                    error: function(response) {
                        console.log('fdfd');
                    }
                });
            });
        </script>

@endsection
