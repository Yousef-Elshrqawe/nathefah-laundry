<?php
$active_links = ['discount' , ''];
?>

@extends('Admin_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            @include('Admin.components.nav')



            <div class="col-xl-12 mb-4 mt-3">
                <div class="overview-box">
                    <div class="table-box">
                        <div class="tab-flex">
                            <div>
                                <h5 class="left-border">Discounts</h5>
                            </div>
                        </div>
                        <div class="table-box-select">
                            {{-- <div class="d-select-box">
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Sort by date</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="d-select-box">
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Branch Riyadh</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div> --}}
                            @if ($discount == null)
                            <div class="d-selct-view">
                                <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-order-modal"><i class="fas fa-plus"></i> Add Discount new user</a>
                            </div>
                                @else
                                <div class="d-selct-view">
                                    <a href="{{route('Admin.new.users.discount.edit',$discount->id)}}" class="btn-style-one edit_button" data-id="{{$discount->id}}"><i class="fas fa-plus"></i> Edit Discount new user</a>
                                </div>
                            @endif
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabdel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table_responsive_maas">
                                        <table class="table" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><span class="th-head-icon">Code</span></th>
                                                    <th><span class="th-head-icon">percent </span></th>
                                                    <th><span class="th-head-icon">amount</span> </th>
                                                    <th><span class="th-head-icon">min price</span></th>
                                                    <th><span class="th-head-icon">Start Date </span></th>
                                                    <th><span class="th-head-icon">End Date </span></th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>

                                                <tr>
                                                    @if ($discount != null)
                                                    <th><span class="th-head-icon">{{$discount->code}}</th>
                                                    <th><span class="th-head-icon">{{$discount->percent}}</th>
                                                    <th><span class="th-head-icon">{{$discount->amount}}</th>
                                                    <th><span class="th-head-icon">{{$discount->min_order_price}}</th>
                                                    <th><span class="th-head-icon">{{$discount->start_date}}</th>
                                                    <th><span class="th-head-icon">{{$discount->enddate}}</th>
                                                    <th>
                                                        <a href="{{route('Admin.new.users.discount.edit',$discount->id)}}" class="btn btn-success btn-sm"><i class="fas fa-edit"></i></a>
                                                    </th>
                                                    @else
                                                    <th colspan="7" class="text-center">No Discount</th>
                                                    @endif
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<div class="justify-content-center d-flex">
{{--    {!! $discounts->appends(Request::except('page'))->render() !!}--}}
</div>

<!-- ==============CREATE NEW Discount MODAL===================== -->
<div class="modal fade" id="new-order-modal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="msform" action="{{route('Admin.discount.store')}}" method="post">
                    @csrf
                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>

                    <fieldset id="firsttab">
	                	<div class="modal-header p-0 mb-5">
                           <h5 class="left-border" id="head">Add Discount </h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discound code</label>
                                        <input type="text" name="code" class="form-control"  aria-describedby="emailHelp" placeholder="ex . Vhm523">
                                        @error('code')
                                           <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discount percent</label>
                                        <input type="number" name="percent" class="form-control" id="discount_percent"  aria-describedby="emailHelp" placeholder="Discount Percent">
                                        @error('percent')
                                          <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discount Amount</label>
                                        <input type="number" name="amount" class="form-control" id="discount_amount" aria-describedby="emailHelp" placeholder="Discount Amount">
                                        @error('amount')
                                           <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <input type="text" name="type" value="new-user" hidden>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Order Min price</label>
                                        <input type="number" name="min_order_price" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Order Min Price">
                                        @error('min_order_price')
                                         <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Start Date</label>
                                        <input type="datetime-local" name="start_date" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Order Min Price">
                                        @error('start_date')
                                        <p class="alert-danger">{{ $message }}</p>
                                       @enderror

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">End Date</label>
                                        <input type="datetime-local" name="enddate" class="form-control"  aria-describedby="emailHelp" placeholder="Enter Order Min Price">
                                        @error('enddate')
                                        <p class="alert-danger">{{ $message }}</p>
                                       @enderror

                                    </div>
                                </div>


                            </div>
                            <div class="btn-question mt-4">
                                <button  type="submit" name="next" class="next btn-style-one" value="" ><i class="fas fa-plus"></i> Add Discount</button >
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>



@endsection

@section('scripts')
<script src="js/menu.js"></script>
<script type="text/javascript">
    $("#cssmenu").menumaker({
        title: "",
        format: "multitoggle"
    });
</script>

<script>
$(document).ready(function(){

$("#last-btn").click(function(){
    $("#progressbar").addClass("hide");
});

var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $("fieldset").length;

setProgressBar(current);

$(".next").click(function(){

     //current_fs = $(this).parent();
    // next_fs = $(this).parent().next();
    current_fs = $(this).closest("fieldset");
    next_fs = $(this).closest("fieldset").next();
    //alert(next_fs);
    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the next <fieldset></fieldset>
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(++current);
});

$(".previous").click(function(){

    current_fs = $(this).closest("fieldset");
    previous_fs = $(this).closest("fieldset").prev();

    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(--current);
});

function setProgressBar(curStep){
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
}

$(".submit").click(function(){
    return false;
})

});
</script>

<script>
        $(document).ready(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 0 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>
@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-order-modal').modal('show');
        });
</script>
@endif
<script>



   $('#discount_amount').click(function(){
     $('#discount_percent').val(0);
   });
   $('#discount_percent').click(function(){
     $('#discount_amount').val(0);
   });

  $('.edit_button').click(function(){
                var id=$(this).attr("data-id");
                $.ajax({
                url:SITEURL+"/discount/edit/"+id,
                type:"GET", //send it through get method
                success: function (response) {
                    $('#discount_id').val(response.id);
                    $('#code').val(response.code);
                    $('#amount').val(response.amount);
                    $('#percent').val(response.percent);
                    $('#min_order_price').val(response.min_order_price);
                    $('#start_date').val(response.start_date);
                    $('#enddate').val(response.enddate);
                    $("#edit-order-modal").modal('toggle');
                },
                error: function(response) {
                    console.log('fdfd');
                }
              });
        });
</script>

@endsection

