<?php
$active_links = ['discount' , ''];
?>

@extends('Admin_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">

            @include('Admin.components.nav')



            <div class="col-xl-12 mb-4 mt-3">
                <div class="overview-box">
                    <div class="table-box">
                        <div class="tab-flex">
                            <div>
                                <h5 class="left-border">Discounts</h5>
                            </div>
                            <div class="tab-box">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link  "  href="{{route('Admin.discount','avilable')}}">current discount </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link "   href="{{route('Admin.discount','previous')}}">previous discount</a>
                                    </li>

                                </ul>
                            </div>
                        </div>
                        <div class="table-box-select">
                            {{-- <div class="d-select-box">
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Sort by date</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div>
                            <div class="d-select-box">
                                <select class="form-select" aria-label="Default select example">
                                    <option selected>Branch Riyadh</option>
                                    <option value="1">One</option>
                                    <option value="2">Two</option>
                                    <option value="3">Three</option>
                                </select>
                            </div> --}}
                            <div class="d-selct-view">
                                <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-order-modal"><i class="fas fa-plus"></i> Add new Discount</a>
                                <a href="{{route('Admin.new.users.discount.users')}}" class="btn-style-one"  ><i class="fas fa-eye"></i> Discount New Users</a>
                            </div>
                        </div>
                    </div>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tabdel">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="table_responsive_maas">
                                        <table class="table" width="100%">
                                            <thead>
                                                <tr>
                                                    <th><span class="th-head-icon">Code</span></th>
                                                    <th><span class="th-head-icon">percent </span></th>
                                                    <th><span class="th-head-icon">amount</span> </th>
                                                    <th><span class="th-head-icon">min price</span></th>
                                                    <th><span class="th-head-icon">Start Date </span></th>
                                                    <th><span class="th-head-icon">End Date </span></th>
                                                    <th>&nbsp;</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($discounts as $discount)
                                                <tr>
                                                    <th><span class="th-head-icon">{{$discount->code}}</th>
                                                    <th><span class="th-head-icon">{{$discount->percent}}</th>
                                                    <th><span class="th-head-icon">{{$discount->amount}}</th>
                                                    <th><span class="th-head-icon">{{$discount->min_order_price}}</th>
                                                    <th><span class="th-head-icon">{{$discount->start_date}}</th>
                                                    <th><span class="th-head-icon">{{$discount->enddate}}</th>
                                                    <th>
                                                        @if($type=='avilable')
                                                        <a  class="btn btn-primary" href="{{route('Admin.discount.assign',$discount->id)}}">Assign</a>
{{--                                                        <a  class="btn btn-success edit_button" data-id="{{$discount->id}}">Edit</a>--}}
                                                            <button  class="btn edit_button btn-success" data-bs-toggle="modal" data-bs-target="#edit-discount-modal"
                                                                     data-id="{{$discount->id}}" >
                                                                @lang('admin.edit')
                                                            </button>
                                                        <a  class="btn btn-danger" href="{{route('Admin.discount.delete',$discount->id)}}">Delete</a>
                                                        @endif
                                                    </th>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>


<div class="justify-content-center d-flex">
    {!! $discounts->appends(Request::except('page'))->render() !!}
</div>

<!-- ==============CREATE NEW Discount MODAL===================== -->
<!-- نموذج HTML -->
<div class="modal fade" id="new-order-modal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form id="msform" action="{{route('Admin.discount.store')}}" method="post">
                    @csrf
                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>
                    <fieldset id="firsttab">
                        <div class="modal-header p-0 mb-5">
                            <h5 class="left-border" id="head">Add Discount</h5>
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label class="form-label">Discount code</label>
                                    <input type="text" name="code" class="form-control" placeholder="ex . Vhm523" id="discound_code_required" required>
                                    @error('code')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label class="form-label">Discount percent</label>
                                    <input type="number" name="percent" class="form-control" id="discount_percent_required" placeholder="Discount Percent" required>
                                    @error('percent')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label class="form-label">Discount Amount</label>
                                    <input type="number" name="amount" class="form-control" id="discount_amount_required" placeholder="Discount Amount" required>
                                    @error('amount')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <input type="text" name="type" value="all" hidden>
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label class="form-label">Order Min price</label>
                                    <input type="number" name="min_order_price" class="form-control" id="min_order_price_required" placeholder="Enter Order Min Price" required>
                                    @error('min_order_price')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label class="form-label">Start Date</label>
                                    <input type="datetime-local" name="start_date" class="form-control" id="start_date_required" placeholder="Enter Start Date" required>
                                    @error('start_date')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label class="form-label">End Date</label>
                                    <input type="datetime-local" name="enddate" class="form-control" id="enddate_required" placeholder="Enter End Date" required>
                                    @error('enddate')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="btn-question mt-4">
                            <button type="submit" name="next" id="submit_required" class="next btn-style-one" disabled style="background-color: red;">
                                <i class="fas fa-plus"></i> Add Discount
                            </button>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- سكريبت جافاسكريبت -->
<script>
    document.addEventListener("DOMContentLoaded", function(){
        const form = document.getElementById("msform");
        const requiredInputs = form.querySelectorAll("input[required]");
        const submitButton = document.getElementById("submit_required");

        // الحصول على حقول الخصم بالنسبة والمبلغ
        const discountPercentInput = document.getElementById("discount_percent_required");
        const discountAmountInput = document.getElementById("discount_amount_required");

        // دالة لفحص الحقول المطلوبة وتحديث حالة الزر
        function checkInputs() {
            let allFilled = true;
            requiredInputs.forEach(function(input){
                if(input.value.trim() === ""){
                    allFilled = false;
                }
            });
            if(allFilled) {
                submitButton.disabled = false;
                submitButton.style.backgroundColor = "green";
            } else {
                submitButton.disabled = true;
                submitButton.style.backgroundColor = "red";
            }
        }

        // التحقق من قيمة حقل "Discount percent"
        discountPercentInput.addEventListener("input", function() {
            const percentValue = parseFloat(discountPercentInput.value);
            if(!isNaN(percentValue) && percentValue > 0) {
                discountAmountInput.value = 0;
            }
            checkInputs();
        });

        // التحقق من قيمة حقل "Discount Amount"
        discountAmountInput.addEventListener("input", function() {
            const amountValue = parseFloat(discountAmountInput.value);
            if(!isNaN(amountValue) && amountValue > 0) {
                discountPercentInput.value = 0;
            }
            checkInputs();
        });

        // إضافة حدث input لجميع الحقول المطلوبة لتحديث الحالة بشكل مستمر
        requiredInputs.forEach(function(input){
            input.addEventListener("input", checkInputs);
        });

        // فحص الحقول عند تحميل الصفحة
        checkInputs();
    });
</script>


<!-- ==============Edit NEW Discount MODAL===================== -->

<div class="modal fade" id="edit-discount-modal" tabindex="-1" aria-labelledby="edit-discount-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="msform" action="{{route('Admin.discount.update')}}" method="post">
                    @csrf
                    <ul id="progressbar">
                        <li class="active" id="account"></li>
                        <li id="personal"></li>
                        <li id="miscla"></li>
                        <li id="confirm"></li>
                    </ul>

                    <fieldset id="firsttab">
	                	<div class="modal-header p-0 mb-5">
                           <h5 class="left-border" id="head">Edit Discount </h5>
                           <input type="hidden" name="id" id="discount_id" value="">
                            <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discound code</label>
                                        <input type="text" name="code" class="form-control" id="code" aria-describedby="emailHelp" placeholder="ex . Vhm523" required>
                                        @error('code')
                                           <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>

                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discount percent</label>
                                        <input type="number" name="percent" class="form-control" id="percent" aria-describedby="emailHelp" placeholder="Discount Percent" required>
                                        @error('percent')
                                          <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Discount Amount</label>
                                        <input type="number" name="amount" class="form-control" id="amount" aria-describedby="emailHelp" placeholder="Discount Amount" required>
                                        @error('amount')
                                           <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Order Min price</label>
                                        <input type="number" name="min_order_price" class="form-control" id="min_order_price" aria-describedby="emailHelp" placeholder="Enter Order Min Price" required>
                                        @error('min_order_price')
                                         <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">Start Date</label>
                                        <input type="datetime-local" name="start_date" class="form-control" id="start_date" aria-describedby="emailHelp" placeholder="Enter Order Min Price" required>
                                        @error('start_date')
                                        <p class="alert-danger">{{ $message }}</p>
                                       @enderror

                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">End Date</label>
                                        <input type="datetime-local" name="enddate" class="form-control" id="enddate" aria-describedby="emailHelp" placeholder="Enter Order Min Price" required>
                                        @error('enddate')
                                        <p class="alert-danger">{{ $message }}</p>
                                       @enderror

                                    </div>
                                </div>


                            </div>
                            <div class="btn-question mt-4">
                                <button  type="submit" name="next" class="next btn-style-one" value="" id="add_discount"><i class="fas fa-plus"></i> Edit Discount</button >
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="js/menu.js"></script>
<script type="text/javascript">
    $("#cssmenu").menumaker({
        title: "",
        format: "multitoggle"
    });
</script>

<script>
$(document).ready(function(){

$("#last-btn").click(function(){
    $("#progressbar").addClass("hide");
});

var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $("fieldset").length;

setProgressBar(current);

$(".next").click(function(){

     //current_fs = $(this).parent();
    // next_fs = $(this).parent().next();
    current_fs = $(this).closest("fieldset");
    next_fs = $(this).closest("fieldset").next();
    //alert(next_fs);
    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the next <fieldset></fieldset>
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(++current);
});

$(".previous").click(function(){

    current_fs = $(this).closest("fieldset");
    previous_fs = $(this).closest("fieldset").prev();

    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();

    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(--current);
});

function setProgressBar(curStep){
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
}

$(".submit").click(function(){
    return false;
})

});
</script>

<script>
        $(document).ready(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 0 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>
@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-order-modal').modal('show');
        });
</script>
@endif
<script>



   $('#discount_amount').click(function(){
     $('#discount_percent').val(0);
   });
   $('#discount_percent').click(function(){
     $('#discount_amount').val(0);
   });

  $('.edit_button').click(function(){
                var id=$(this).attr("data-id");
                $.ajax({
                               url: "{{route('Admin.discount.edit' , '')}}"+"/"+id,
                type:"GET", //send it through get method
                success: function (response) {
                    console.log(response);
                    $('#discount_id').val(response.id);
                    $('#code').val(response.code);
                    $('#amount').val(response.amount);
                    $('#percent').val(response.percent);
                    $('#min_order_price').val(response.min_order_price);
                    $('#start_date').val(response.start_date);
                    $('#enddate').val(response.enddate);
                    $("#edit-discount-modal").modal('toggle');
                },
                error: function(response) {
                    console.log('fdfd');
                }
              });
        });
</script>

@endsection

