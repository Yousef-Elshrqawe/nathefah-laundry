<table class="table" width="100%">
    <thead>
        <tr>
            <th><span class="th-head-icon">id</th>
            <th><span class="th-head-icon">name</th>
            <th><span class="th-head-icon">phone</th>
            <th><span class="th-head-icon">Discount Code</th>
            <th><span class="th-head-icon">usage count</th>
            <th>&nbsp;procedures</th>
        </tr>
    </thead>
    <tbody>
          @foreach ($discountusers as $discountuser)
          <tr>
            <td>{{$discountuser->user->id}}</td>
            <td>{{$discountuser->user->name}}</td>
            <td>{{$discountuser->user->phone}}</td>
            <td>{{$discount->code}}</td>
            <td>{{$discountuser->count}}</td>
            <td><a  class="btn btn-danger" href="{{route('assigned.delete',$discountuser->id)}}">Delete</a></td>
        </tr>
          @endforeach
    </tbody>
</table>
<div class="justify-content-center d-flex">
    {!! $discountusers->appends(Request::except('page'))->render() !!}
</div>
