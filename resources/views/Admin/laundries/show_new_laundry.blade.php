<?php
$active_links = ['view laundry' , ''];
?>
@extends('Admin_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-topbar">
                        <div class="overview-box">
                            <div class="row align-items-center flex-column-reverse flex-xl-row">
                                <div class="col-xl-4 col-md-12">
                                    <div class="top-bar-title">
                                        <h5>Welcome Ahmed ! </h5>
                                        <p class="cl-gray">Here is what happened in the branches today</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="search-box">
                                        <input type="text" name="" id="" class="form-control" placeholder="Search on Order, Driver, Branch">
                                        <i class="fal fa-search"></i>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="notify-box">
                                        <div class="notify-bell">
                                            <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                        </div>
                                        <div>
                                            <p>Sun 24 July 4:35 PM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            {{-- start show laundry --}}
             <div class="row">
                 <h2 class="text-center">{{$Laundry->name}}</h2>
                 <div class="col-5">
                    <h3> company register</h3>
                   <img class="img-fluid rounded " src="{{$Laundry->companyregister}}">
                 </div>
                 <div class="col-5">
                    <h3> company tax card</h3>
                    <img class="img-fluid rounded " src="{{$Laundry->taxcard}}">
                </div>
             </div>

             <div class="row">
                <div class="table_responsive_maas v2">
                    <table class="table" width="100%">
                        <thead>
                            <tr>
                                <th><span class="th-head-icon">Branch ID</th>
                                <th><span class="th-head-icon">Branch name</th>

                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Laundry->branchs as $branch)
                                <tr>
                                    <td>{{$branch->id}}</td>
                                    <td>{{$branch->username}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <a class="btn btn-success" href="{{route('Admin.pending.laundry.Activation',$Laundry->id)}}">active laundry</a>
            {{-- end show laundry --}}
        </div>
    </div>
</div>
@endsection
