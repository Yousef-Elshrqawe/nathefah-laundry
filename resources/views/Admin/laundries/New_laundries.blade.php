<?php
$active_links = ['new laundries' , ''];
?>
@extends('Admin_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="page-topbar">
                        <div class="overview-box">
                            <div class="row align-items-center flex-column-reverse flex-xl-row">
                                <div class="col-xl-4 col-md-12">
                                    <div class="top-bar-title">
                                        <h5>Welcome Ahmed ! </h5>
                                        <p class="cl-gray">Here is what happened in the branches today</p>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="search-box">
                                        <input type="text" name="" id="" class="form-control" placeholder="Search on Order, Driver, Branch">
                                        <i class="fal fa-search"></i>
                                    </div>
                                </div>
                                <div class="col-xl-4 col-md-12">
                                    <div class="notify-box">
                                        <div class="notify-bell">
                                            <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                        </div>
                                        <div>
                                            <p>Sun 24 July 4:35 PM</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="table_responsive_maas v2">
                    <table class="table" width="100%">
                        <thead>
                            <tr>
                                <th><span class="th-head-icon">Laundry ID</span></th>
                                <th><span class="th-head-icon">Laundry name</span></th>
                                <th><span class="th-head-icon">Number Of Branches</span></th>
                                {{-- <th><span class="th-head-icon">Total Orders</th> --}}
                                <th><span class="th-head-icon">Subscription Type</span></th>
                                {{-- <th><span class="th-head-icon">Subscription Status</th> --}}
                                {{-- <th><span class="th-head-icon">Rate</th> --}}
                                <th>&nbsp;</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($Laundries as $laundry)
                                <tr>
                                    <td>{{$laundry->id}}</td>
                                    <td>{{$laundry->name}}</td>
                                    <td>{{$laundry->branchs->count()}}</td>
                                    {{-- <td>0</td> --}}
                                    <td>Yearly</td>
                                    {{-- <td>1 months</td> --}}
                                    {{-- <td class="cl-green"></td> --}}
                                    <td>
                                        <a href="{{route('Admin.pending.laundry.show',$laundry->id)}}" class="cl-light">View</a> |
                                        <a href="{{route('Admin.Active.Luandry',$laundry->id)}}" class="cl-light">active</a>

                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="justify-content-center d-flex">
    {!! $Laundries->appends(Request::except('page'))->render() !!}
</div>
@endsection
