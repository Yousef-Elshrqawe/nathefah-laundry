<div class="modal fade" id="add-branch-modal" tabindex="-1" aria-labelledby="add-branch-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="left-border">Add Laundry / Branch </h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="subnav-tab">
                    <ul class="nav nav-tabs mb-4">
                        <li class="nav-item">
                            <a class="nav-link active" data-bs-toggle="tab" href="#tab1-1">New Laundry</a>
                        </li>

                    </ul>


                    <div class="tab-content">
                        <div class="tab-pane active" id="tab1-1">
                            <form action="{{route('Admin.laundry.store')}}" method="POST" enctype='multipart/form-data'
                                  id="msform">
                                @csrf
                                <div class="row align-items-end">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Laundry Name</label>
                                            <input type="text" class="form-control" id="laundryNameId" name="name"
                                                   aria-describedby="emailHelp" placeholder="Enter The Laundry name"
                                                   value="{{ old('name') }}"
                                                   required>
                                            @error('name')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Email</label>
                                            <input type="email" class="form-control" id="laundryEmailId" name="email"
                                                   value="{{ old('email') }}"
                                                   placeholder="Enter you Email" required>
                                            @error('email')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Country Code</label>
                                            <input type="text" class="form-control" name="country_code"
                                                   id="laundryCountryCodeId" placeholder="5xxxxxxxx "
                                                   value="+966" disabled>
                                            @error('country_code')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror

                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Phone Number</label>
                                            <input type="text" class="form-control" name="phone" id="laundryPhoned"
                                                    value="{{ old('phone') }}"
                                                   placeholder="5xxxxxxxx" required>
                                            @error('phone')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Branch</label>
                                            <select class="form-control" required id="laundryBranchId" name="branch_number">
                                                <option selected value="">Choose Branch</option>
                                                <option value="one" {{ old('branch_number') == 'one' ? 'selected' : '' }}>
                                                    One</option>
                                                <option value="many" {{ old('branch_number') == 'many' ? 'selected' : '' }}>
                                                    Many</option>
                                            </select>
                                            @error('branch_number')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Laundry Logo</label>
                                            <input type="file"  accept="image/*" required class="form-control" name="laundry_logo"
                                            "
                                                   id="laundryLogoId" placeholder="Enter The Laundry name">
                                            @error('laundry_logo')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">tax card</label>
                                            <input  type="file"  accept="image/*"   class="form-control" name="tax_card"
                                            "
                                                   value="{{ old('tax_card') }}"
                                                   id="laundryTaxNumberId" placeholder="Enter The Laundry name">
                                            @error('laundry_logo')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">tax number</label>
                                            <input type="text" class="form-control" name="tax_number" id="tax_number"
                                                    value="{{ old('tax_number') }}"
                                                   placeholder="Enter the tax number">
                                            @error('tax_number')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Registration Number</label>
                                            <input  type="file"  accept="image/*"   required class="form-control" id="laundryRegistrationNumberId"
                                                   "
                                                   value="{{ old('company_register') }}"
                                                   name="company_register" placeholder="Enter Transaction ID">
                                            @error('company_register')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Password</label>
                                            <input type="password" class="form-control" id="laundryPasswordId"
                                                   value="{{ old('password') }}"
                                                   name="password" placeholder="Enter Password">
                                            @error('password')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Period</label>
                                            <select name="period" required id="laundryPeriodId" class="form-control">
                                                <option selected disabled>Choose Period</option>
                                                @foreach($periods as $item)
                                                    <option value="{{$item->id}}" >
                                                        {{$item->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('period')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>


                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Package</label>
                                            <select name="package" required id="laundryPackageId" class="form-control">
                                                <option selected value="">Choose Package</option>
                                            </select>
                                            @error('package')
                                            <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>
                                    </div>


                                </div>

                                <div class="btn-question mt-4">
                                    <button type="submit" class="btn-style-one"><i
                                            class="fal fa-chevron-square-right pr-2"></i>Save
                                    </button>
                                </div>
                            </form>

                        </div>
                        <div class="tab-pane " id="tab1-2">

                            <!-- fieldsets file:///E:/Suryakanta/VTC-Revamp/HTML/agent-register.html-->
                            <fieldset id="firsttab2">
                                <div class="row align-items-end">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Laundry Name or ID</label>
                                            <input type="email" class="form-control" id="" aria-describedby="emailHelp"
                                                   placeholder="Enter The Laundry name / ID">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Branch Name</label>
                                            <input type="email" class="form-control" id="" aria-describedby="emailHelp"
                                                   placeholder="Enter the Branch Name">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Branch Number</label>
                                            <input type="text" class="form-control" id=""
                                                   placeholder="Enter the Branch Number">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">City</label>
                                            <input type="text" class="form-control" id="" placeholder="Enter City">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            <label for="" class="form-label">Street</label>
                                            <input type="text" class="form-control" id="" placeholder="Street">
                                        </div>
                                    </div>

                                </div>

                                <div class="btn-question mt-4">
                                    <button type="button" name="next" class="next btn-style-one"><i
                                            class="fal fa-chevron-square-right pr-2"></i>Next
                                    </button>
                                    <input type="button" name="previous" class="previous btn-style-two backbtn"
                                           value="Reset"/>
                                </div>
                            </fieldset>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
