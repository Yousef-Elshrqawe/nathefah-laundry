<?php
$active_links = ['laundries', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')

                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="row">
                            <div class="col-xl-12 mb-4">
                                <div class="inner-title-box">
                                    <div>
                                        <h4 class="title-inner">Laundries</h4>
                                    </div>
                                    <div class="btn-flex">
                                        <a href="#" class="btn-style-one" data-bs-toggle="modal"
                                            data-bs-target="#add-branch-modal"><i class="fas fa-plus"></i>Add Laundry</a>
                                    </div>
                                    <div class="d-selct-view">
                                        <a href="{{ route('Admin.create.branch') }}"
                                           class="btn-style-one">
                                            <i class="fas fa-plus"></i>@lang('admin.add_branch') </a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <div class="row">
                                    {{-- <div class="col-xl-4 col-md-6 mb-3">
                                        <div class="overview-box">
                                            <div class="laundry-request-flex">
                                                <div class="request-text">
                                                    <p>disabled Laundry</p>
                                                </div>
                                                <div class="request-counter">
                                                    <span>{{$disbledlaundriess}}</span>
                                                    <a href="{{route('Admin.pending.laundry')}}">
                                                        <i class="far fa-chevron-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    {{-- <div class="col-xl-4 col-md-6 mb-3">
                                        <div class="overview-box">
                                            <div class="laundry-request-flex">
                                                <div class="request-text">
                                                    <p>New Branch Requests</p>
                                                </div>
                                                <div class="request-counter">
                                                    <span>3</span>
                                                    <i class="far fa-chevron-right"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Joined Laundries</p>
                                        </div>
                                        {{-- <div class="dropdown btn-dropdown">
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <h4 class="cl-light">{{ $LaundriesCount }}</h4>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">active Laundries</p>
                                        </div>
                                        {{-- <div class="dropdown btn-dropdown">
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div> --}}
                                    </div>
                                    <h4 class="cl-light">{{ $activelaundries }}</h4>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Top Laundry</p>
                                        </div>
                                        {{-- <div class="dropdown btn-dropdown">
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div> --}}
                                    </div>

                                    <h4 class="cl-light">{{ $toplaundry->name }}</h4>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Today Balance</p>
                                        </div>
                                        {{-- <div class="dropdown btn-dropdown">
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div> --}}
                                    </div>

                                    <h4 class="cl-light">{{ $todaybalance }} SR</h4>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">Laundries</h5>
                                    <div class="search-box">

                                        <form action="{{ route('Admin.laundry.index', ['date' => request('date', 'All')]) }}" method="GET">
                                            <input type="text" name="search" id="laundrySearch" class="form-control"
                                                   placeholder="Find a Laundry" value="{{ request('search') }}">
                                            <i class="fal fa-search" onclick='this.parentNode.submit(); return false;'></i>

                                            <!-- حقل مخفي للاحتفاظ بتاريخ الفلترة عند البحث -->
                                            <input type="hidden" name="date" value="{{ request('date', 'All') }}">
                                        </form>


                                    </div>
                                </div>
                                <div class="table-box-select">
                                    <div class="d-select-box">
                                        <select class="form-select" id="select_date" aria-label="Default select example">
                                            <option selected>Sort By month: Latest</option>
                                            <option value="All" @if ($date == 'All') selected @endif>All
                                            </option>
                                            <option value="30" @if ($date == '30') selected @endif>One
                                            </option>
                                            <option value="60" @if ($date == '60') selected @endif>Two
                                            </option>
                                            <option value="90" @if ($date == '90') selected @endif>Three
                                            </option>
                                        </select>
                                    </div>
                                    {{-- <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Subscription  : All</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div> --}}
                                </div>
                            </div>
                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">#</span></th>
                                            <th><span class="th-head-icon">Laundry name</span></th>
                                            <th><span class="th-head-icon">Number Of Branches</span></th>
                                            <th><span class="th-head-icon">Total Orders</span></th>
                                            <th><span class="th-head-icon">Total Orders Price</span></th>
                                            <th><span class="th-head-icon">Subscription Type</span></th>
                                            <th><span class="th-head-icon">Subscription Status</span></th>
                                            <th><span class="th-head-icon">Rate</span></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($laundries as $key => $laundry)
                                            <tr>
                                                <td>{{ $key + 1 }}</td>
                                                <td>{{ $laundry->name }}</td>
                                                <td>{{ count($laundry->branchs) }}</td>
                                                <td>{{ $laundry->branchs->sum('allOrders') }}</td>
                                                <td>{{ $laundry->branchs->sum('balance') }}</td>
                                                <td>{{ $laundry->package ? $laundry->package->package->period->duration_unit : '' }}
                                                </td>
                                                <td>{{ $laundry->package ? $laundry->package->package->period->duration_value : '' }}
                                                    {{ $laundry->package ? $laundry->package->package->period->duration_unit : '' }}
                                                </td>
                                                <td class="cl-green">{{ $laundry->rate }}</td>
                                                <td><a href="{{ route('Admin.laundry.details', $laundry->id) }}"
                                                        class="cl-light">View</a> |
                                                    @if ($laundry->status == 1)
                                                        <a href="{{ route('Admin.disaple.Luandry', $laundry->id) }}"
                                                            class="cl-light">disapled</a>
                                                    @elseif ($laundry->status == 0)
                                                        <a href="{{ route('Admin.Active.Luandry', $laundry->id) }}"
                                                            class="cl-light">active</a>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                {{ $laundries->withQueryString()->links() }}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('Admin.laundries.create')


{{--    <!-- ============== CREATE NEW items MODAL===================== -->
    <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form id="msform" action="{{ route('admin.branches.store') }}" method="post">
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="account"></li>
                            <li id="personal"></li>
                            <li id="miscla"></li>
                            <li id="confirm"></li>
                        </ul>

                        <fieldset id="firsttab">
                            <div class="modal-header p-0 mb-5">
                                <h5 class="left-border" id="head">@lang('admin.add_branch')</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="row">

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">{{ __('admin.username') }}</label>
                                        <input type="text" name="username" class="form-control"
                                            value="{{ old('username') }}" aria-describedby="emailHelp"
                                            placeholder="{{ __('admin.username') }}" required>
                                        @if ($errors->has('username'))
                                            <p class="alert-danger">{{ $errors->first('username') }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.country_code')</label>
                                        <input type="text" name="country_code" class="form-control" value="+966"
                                            disabled aria-describedby="emailHelp" placeholder="@lang('admin.country_code')"
                                            required>
                                        @if ($errors->has('country_code'))
                                            <p class="alert-danger">{{ $errors->first('country_code') }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.phone')</label>
                                        <input type="number" name="phone" class="form-control"
                                            value="{{ old('phone') }}" aria-describedby="emailHelp"
                                            placeholder="5xxxxxxxx" required>
                                        @if ($errors->has('phone'))
                                            <p class="alert-danger">{{ $errors->first('phone') }}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.argent')</label>
                                        <select name="argent" class="form-control" required>
                                            <option value="1" {{ old('argent') == 1 ? 'selected' : '' }}>
                                                @lang('admin.yes')</option>
                                            <option value="0" {{ old('argent') == 0 ? 'selected' : '' }}>
                                                @lang('admin.no')</option>
                                        </select>
                                        @if ($errors->has('argent'))
                                            <p class="alert-danger">{{ $errors->first('argent') }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.laundry')</label>
                                        <select name="laundry_id" class="form-control" required>
                                            @foreach ($laundriesAll as $laundry)
                                                <option value="{{ $laundry->id }}" {{ old('laundry_id') == $laundry->id ? 'selected' : '' }}>
                                                    {{ $laundry->name }}</option>
                                            @endforeach
                                        </select>
                                        @if ($errors->has('laundry_id'))
                                            <p class="alert-danger">{{ $errors->first('laundry_id') }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.status')</label>
                                        <select name="status" class="form-control" required>
                                            <option value="open" {{ old('status') == 'open' ? 'selected' : '' }}>
                                                @lang('admin.open')</option>
                                            <option value="closed" {{ old('status') == 'closed' ? 'selected' : '' }}>
                                                @lang('admin.closed')</option>
                                        </select>
                                        @if ($errors->has('status'))
                                            <p class="alert-danger">{{ $errors->first('status') }}</p>
                                        @endif
                                    </div>
                                </div>


                                --}}{{-- <div class="col-md-6">
                                     <div class="form-box">
                                         <label for="" class="form-label">@lang('admin.open_time')</label>
                                         <input type="time" name="open_time" class="form-control"
                                                value="{{ old('open_time') }}" aria-describedby="emailHelp"
                                                placeholder="@lang('admin.open_time')" required>
                                         @if ($errors->has('open_time'))
                                             <p class="alert-danger">{{ $errors->first('open_time')}}</p>
                                         @endif
                                     </div>
                                 </div>

                                 <div class="col-md-6">
                                     <div class="form-box">
                                         <label for="" class="form-label">@lang('admin.closed_time')</label>
                                         <input type="time" name="closed_time" class="form-control"
                                                value="{{ old('closed_time') }}" aria-describedby="emailHelp"
                                                placeholder="@lang('admin.closed_time')" required>
                                         @if ($errors->has('closed_time'))
                                             <p class="alert-danger">{{ $errors->first('closed_time')}}</p>
                                         @endif
                                     </div>
                                 </div> --}}{{--



                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.password')</label>
                                        <input type="password" name="password" class="form-control"
                                            value="{{ old('password') }}" aria-describedby="emailHelp"
                                            placeholder="@lang('admin.password')" required>
                                        @if ($errors->has('password'))
                                            <p class="alert-danger">{{ $errors->first('password') }}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.password_confirmation')</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                            value="{{ old('password_confirmation') }}" aria-describedby="emailHelp"
                                            placeholder="@lang('admin.password_confirmation')" required>
                                        @if ($errors->has('password_confirmation'))
                                            <p class="alert-danger">{{ $errors->first('password_confirmation') }}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="createLatitudeId" class="form-label">Latitude</label>
                                        <input type="text" name="lat"  class="form-control"
                                            id="createLatitudeId" aria-describedby="emailHelp"
                                            placeholder="Enter the Latitude">

                                        @error('lat')
                                            <p class="alert-danger">{{ $message }}</p>
                                        @enderror

                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="createLongitudeId" class="form-label">Longitude</label>
                                        <input type="text" name="long"  class="form-control"
                                            id="createLongitudeId" aria-describedby="emailHelp"
                                            placeholder="Enter the Longitude">
                                        @error('long')
                                            <p class="alert-danger">{{ $message }}</p>
                                        @enderror

                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-box">
                                        <label for="createUserCountryCodeId" class="form-label">location</label>
                                        <div id="map" style="height: 500px; width: 100%"></div>

                                    </div>
                                </div>
                            </div>


                            <div class="btn-question mt-4">
                                <button type="submit" name="next" class="next btn-style-one" value=""><i
                                        class="fas fa-plus"></i>@lang('admin.add_branch')</button>
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>


                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>--}}
@endsection

@section('scripts')
    <script type="text/javascript">
        $("#cssmenu").menumaker({
            title: "",
            format: "multitoggle"
        });
    </script>
    <script>
        $(document).ready(function() {

            $("#last-btn").click(function() {
                $("#progressbar").addClass("hide");
            });

            var current_fs, next_fs, previous_fs; //fieldsets
            var opacity;
            var current = 1;
            var steps = $("fieldset").length;

            setProgressBar(current);

            $(".next").click(function() {

                //current_fs = $(this).parent();
                // next_fs = $(this).parent().next();
                current_fs = $(this).closest("fieldset");
                next_fs = $(this).closest("fieldset").next();
                //alert(next_fs);
                //Add Class Active
                $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the next <fieldset></fieldset>
                next_fs.show();
                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        next_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 500
                });
                setProgressBar(++current);
            });

            $(".previous").click(function() {

                current_fs = $(this).closest("fieldset");
                previous_fs = $(this).closest("fieldset").prev();

                //Remove class active
                $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                //show the previous fieldset
                previous_fs.show();

                //hide the current fieldset with style
                current_fs.animate({
                    opacity: 0
                }, {
                    step: function(now) {
                        // for making fielset appear animation
                        opacity = 1 - now;

                        current_fs.css({
                            'display': 'none',
                            'position': 'relative'
                        });
                        previous_fs.css({
                            'opacity': opacity
                        });
                    },
                    duration: 500
                });
                setProgressBar(--current);
            });

            function setProgressBar(curStep) {
                var percent = parseFloat(100 / steps) * curStep;
                percent = percent.toFixed();
                $(".progress-bar")
                    .css("width", percent + "%")
            }

            $(".submit").click(function() {
                return false;
            })

        });

        /*
                function getPackage()
                {
                    let period_id = $("#laundryPeriodId").val();
                    let url = '{{ route('Admin.packages.period', ':id') }}';
                    url = url.replace(':id', period_id);

                    $("#laundryPackageId").empty();

                    $.ajax({
                        type: "GET",
                        url: url,
                        beforeSend: function(){

                        },
                        success: function(response) {
                            let packages       = response.data.pakages;
                            let PackagesCount  = packages.length;
                            for (let i =0; i< PackagesCount; i++){
                                $("#laundryPackageId").append($('<option>', {
                                    value: packages[i].id,
                                    text:  packages[i].name
                                }));
                            }
                        },complete: function(){
                        },error: function(error){
                        }
                    });

                }*/
    </script>
    <script>
        $(document).ready(function() {
            $('#laundryPeriodId').on('change', function() {
                var laundry_id = $(this).val();
                $('#laundryPackageId').empty();
                console.log(laundry_id);
                if (laundry_id) {
                    $.ajax({
                        url: "{{ URL::to('Super-admin/ajax/packages/periods') }}/" + laundry_id,
                        type: 'GET',
                        dataType: 'json',
                        success: function(data) {
                            console.log(data);
                            $('#branch_id').empty();
                            $.each(data, function(index, subcategory) {
                                $('#laundryPackageId').append('<option value="' +
                                    subcategory.id + '">' + subcategory.name +
                                    '</option>');
                            });
                        }
                    });
                } else {
                    $('#laundryPackageId').empty();
                }
            });
        });
    </script>
    {{-- stert sort script --}}
    <script>
        var url = "{{ url('/Super-admin') }}";
        //var balancetype=$('#select_type').find(":selected").val();
        //var date=$('#select_date').find(":selected").val();
        // $('#select_type').change(function(){
        //     var balancetype=$(this).find(":selected").val();
        //     window.location.href=url+'/balance/'+balancetype+'/'+date;
        // });

        $('#select_date').change(function() {
            var date = $(this).find(":selected").val();
            window.location.href = url + '/laundries/' + date;
        });
    </script>
    {{-- end sort script --}}



    @if (Session::get('errors') != null)
        <script>
            $(document).ready(function() {
                $('#add-branch-modal').modal('show');
            });
        </script>
    @endif

{{--    @if (Session::get('errors') != null)--}}
{{--        <script>--}}
{{--            $(document).ready(function() {--}}
{{--                $('#new-item-modal').modal('show');--}}
{{--            });--}}
{{--        </script>--}}
{{--    @endif--}}


    <script>
        $("#pac-input").focusin(function() {
            $(this).val('');
        });

        $('#createLatitudeId').val('{{ old('lat') }}');
        $('#createLongitudeId').val('{{ old('long') }}');


        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initAutocomplete() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: 24.740691,
                    lng: 46.6528521
                },
                zoom: 13,
                mapTypeId: 'roadmap'
            });

            // move pin and current location
            infoWindow = new google.maps.InfoWindow;
            geocoder = new google.maps.Geocoder();
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude,
                    };
                    map.setCenter(pos);
                    var marker = new google.maps.Marker({
                        position: new google.maps.LatLng(pos),
                        map: map,
                        title: 'موقعك الحالي'
                    });

                    //اضافه  الموقع  الحالي  الي  الحقل
                    $('#createLatitudeId').val(pos.lat);
                    $('#createLongitudeId').val(pos.lng);
                    markers.push(marker);
                    marker.addListener('click', function() {
                        geocodeLatLng(geocoder, map, infoWindow, marker);
                    });
                    // to get current position address on load
                    google.maps.event.trigger(marker, 'click');
                }, function() {
                    handleLocationError(true, infoWindow, map.getCenter());
                });
            } else {
                // Browser doesn't support Geolocation
                console.log('dsdsdsdsddsd');
                handleLocationError(false, infoWindow, map.getCenter());
            }

            var geocoder = new google.maps.Geocoder();
            google.maps.event.addListener(map, 'click', function(event) {
                SelectedLatLng = event.latLng;
                geocoder.geocode({
                    'latLng': event.latLng
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            deleteMarkers();
                            addMarkerRunTime(event.latLng);
                            SelectedLocation = results[0].formatted_address;
                            console.log(results[0].formatted_address);
                            splitLatLng(String(event.latLng));
                            $("#pac-input").val(results[0].formatted_address);

                        }
                    }
                });
            });

            function geocodeLatLng(geocoder, map, infowindow, markerCurrent) {
                var latlng = {
                    lat: markerCurrent.position.lat(),
                    lng: markerCurrent.position.lng()
                };
                /* $('#branch-latLng').val("("+markerCurrent.position.lat() +","+markerCurrent.position.lng()+")");*/
                $('#createLatitudeId').val(markerCurrent.position.lat());
                $('#createLongitudeId').val(markerCurrent.position.lng());

                geocoder.geocode({
                    'location': latlng
                }, function(results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            map.setZoom(8);
                            var marker = new google.maps.Marker({
                                position: latlng,
                                map: map
                            });
                            markers.push(marker);
                            infowindow.setContent(results[0].formatted_address);
                            SelectedLocation = results[0].formatted_address;
                            $("#pac-input").val(results[0].formatted_address);


                            infowindow.open(map, marker);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
                SelectedLatLng = (markerCurrent.position.lat(), markerCurrent.position.lng());
            }

            function addMarkerRunTime(location) {
                var marker = new google.maps.Marker({
                    position: location,
                    map: map
                });
                markers.push(marker);
            }

            function setMapOnAll(map) {
                for (var i = 0; i < markers.length; i++) {
                    markers[i].setMap(map);
                }
            }

            function clearMarkers() {
                setMapOnAll(null);
            }

            function deleteMarkers() {
                clearMarkers();
                markers = [];
            }

            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            $("#pac-input").val("أبحث هنا ");
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function() {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.
            searchBox.addListener('places_changed', function() {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function(marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function(place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(100, 100),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));


                    $('#createLatitudeId').val(place.geometry.location.lat());
                    $('#createLongitudeId').val(place.geometry.location.lng());

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }
                });
                map.fitBounds(bounds);
            });
        }

        function handleLocationError(browserHasGeolocation, infoWindow, pos) {
            infoWindow.setPosition(pos);
            infoWindow.setContent(browserHasGeolocation ?
                'Error: The Geolocation service failed.' :
                'Error: Your browser doesn\'t support geolocation.');
            infoWindow.open(map);
        }

        function splitLatLng(latLng) {
            var newString = latLng.substring(0, latLng.length - 1);
            var newString2 = newString.substring(1);
            var trainindIdArray = newString2.split(',');
            var lat = trainindIdArray[0];
            var Lng = trainindIdArray[1];

            $("#createLatitudeId").val(lat);
            $("#createLongitudeId").val(Lng);

            console.log(lat);
            console.log(Lng);
        }
    </script>




    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBljsOSEkY07FkBuA6p0DmtOcE64VW-rfE&libraries=places&callback=initAutocomplete&language={{ app()->getLocale() }}
         async defer"></script>
@endsection
