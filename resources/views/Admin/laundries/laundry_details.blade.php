<?php
    $active_links = ['laundries' , ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">
        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')

                <h4 class="title-inner mt-0"><a href="{{route('Admin.laundry.index' , 'All')}}"><i class="fas fa-chevron-left"></i>  &nbsp; Laundry Profile</a></h4>
                <div class="row">
                    <div class="col-lg-8 col-md-12">
                        <div class="branch-inner">
                            <div class="branch-laundry">
                                <img src="images/laundry.png" alt="">
                            </div>
                            <div class="laundry-small-logo">
                                <img src="images/company-logo.jpg" alt="">
                            </div>
                            <div class="whitebox-panel">
                                <div class="driver-profile-top">
                                    <div class="driver-profile-top1">
                                        <div class="driver-profile-top3">
                                            <h4 class="m-0">{{ $laundry->name }}
                                                <span class="avl-version-star ps-3">
                @for ($i = 0; $i < $fullStars; $i++)
                                                        <i class="fas fa-star text-warning"></i> {{-- ⭐ نجمة كاملة --}}
                                                    @endfor

                                                    @if ($halfStar)
                                                        <i class="fas fa-star-half-alt text-warning"></i> {{-- ⭐ نصف نجمة --}}
                                                    @endif

                                                    @for ($i = 0; $i < $emptyStars; $i++)
                                                        <i class="fal fa-star text-secondary"></i> {{-- ☆ نجمة فارغة --}}
                                                    @endfor
            </span>

                                                <span class="ps-2 cl-gray"> ({{ $orderReviewCount }} Review{{ $orderReviewCount > 1 ? 's' : '' }}) </span>
                                            </h4>
                                            <span class="cl-gray">{{ $completedOrders }} Successful Orders</span>
                                        </div>
                                    </div>

                                    <div class="driver-profile-top2 me-0">
                                        <div class="dropdown btn-dropdown">
                                            {{-- <a href="#" class="editbtn"><i class="far fa-edit"></i></a>
                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton11" data-bs-toggle="dropdown" aria-expanded="false"> --}}
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton11">
                                                <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row addressbar">
                                    @foreach($branches as $branch)
                                    <div class="col-lg-6 col-md-6">
                                        <div class="add-branch-whitebox">
                                            <div class="addbranch-header">
                                                <div class="addbranch-logo"><img src="{{asset('images/small-logo.png')}}" alt=""/></div>
                                                <div class="addbranch-content">
                                                    <p>{{$branch->useranme}} </p>
                                                    <h6>({{$branch->username}})</h6>
                                                </div>
                                                {{-- <span class="dlt"><a href="#"><i class="fas fa-trash"></i></a></span> --}}
                                            </div>
                                            <ul>
                                                <li><i class="far fa-location"></i> Riyadh <span class="dot1"><i class="fas fa-circle"></i></span> {{$branch->address}}</li>
                                                <li><i class="far fa-phone-alt"></i> Main <span class="dot1"><i class="fas fa-circle"></i></span> {{$branch->country_code}}{{$branch->phone}}</li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach
                                    {{-- <div class="col-lg-6 col-md-6">
                                        <a href="#" class="add-branch" onclick="openNewBranchModal()">
                                            <i class="fal fa-plus" onclick="openNewBranchModal()"></i>
                                            Add New Branch
                                        </a>
                                    </div> --}}
                                </div>
                                <div class="line-title ">
                                    <h6>Basic info</h6>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="drivers-info">

                                            <!-- ✅ Tax Number -->
                                            <div class="drivers-info-box1 text-center">
                                                <i class="fal fa-sticky-note"></i>
                                                <strong>Tax Number</strong>
                                                <span class="dot1"><i class="fas fa-circle"></i></span>
                                                <span class="{{ $laundry->tax_number ? 'cl-green' : 'cl-red'}}" style="font-weight: 500;">
                    <em class="fal fa-check px-2"></em>
                    {{ $laundry->tax_number ? 'Verified' : 'Not Verified' }}
                </span>

                                                @if($laundry->tax_number)
                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#taxNumberModal" class="cl-light ps-2" style="font-weight: 500;">View</a>
                                                @endif
                                            </div>

                                            <!-- ✅ Registration Number -->
                                            <div class="drivers-info-box1 text-center">
                                                <i class="fal fa-sticky-note"></i>
                                                <strong>Registration Number</strong>
                                                <span class="dot1"><i class="fas fa-circle"></i></span>
                                                <span class="{{$laundry->companyregister ? 'cl-green' : 'cl-red' }}" style="font-weight: 500;">
                    <em class="fal fa-check px-2"></em>
                    {{ $laundry->companyregister ? 'Verified' : 'Not Verified' }}
                </span>

                                                @if($laundry->companyregister)
                                                    <a href="#" data-bs-toggle="modal" data-bs-target="#registrationModal" class="cl-light ps-2" style="font-weight: 500;">View</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <!-- ✅ Bootstrap Modals to View Images -->
                                @if($laundry->taxcard)
                                    <div class="modal fade" id="taxNumberModal" tabindex="-1" aria-labelledby="taxNumberModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="taxNumberModalLabel">Tax Number Document</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body text-center" >
                                                    <img src="{{ $laundry->taxcard }}" class="img-fluid rounded" alt="Tax Number Document">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($laundry->companyregister)
                                    <div class="modal fade" id="registrationModal" tabindex="-1" aria-labelledby="registrationModalLabel" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="registrationModalLabel">Registration Document</h5>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                                </div>
                                                <div class="modal-body text-center" >
                                                    <img src="{{ $laundry->companyregister }}" class="img-fluid rounded" alt="Registration Document">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif


                                <form action="{{route('Admin.laundry.details.update',$laundry->id)}}" method="POST">
                                    @csrf
                                    @method('PUT')
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="pr-50 mb-3">
                                                <label for="" class="form-label">Laundry Name</label>
                                                <input type="text" class="form-control" id="laundryName" value="{{$laundry->name}}" name="name" required aria-describedby="emailHelp" placeholder="Fresh & Clean">
                                                @error('name')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pr-50 mb-3">
                                                <label for="" class="form-label">Email</label>
                                                <input type="email" class="form-control" id="laundryEmail" required name="email"  value="{{$laundry->email}}"  aria-describedby="emailHelp" placeholder="Fresh&Clean@mail.com">
                                                @error('email')
                                                    <span class="text-danger">{{ $message }}</span>
                                                @enderror

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="pr-50 mb-3">
                                                <label for="" class="form-label">Phone Number</label>
                                                <input type="text" class="form-control" id="laundryPhone" required name="phone" value="{{$laundry->phone}}"  aria-describedby="emailHelp" placeholder="+9764213576345">
                                                @error('phone')
                                                <span class="text-danger">{{ $message }}</span>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pt-4">
                                            <div class="form-box">
                                                <button type="submit" class="btn-style-two border"><i class="far fa-check"></i> Save</button>
                                                {{--                                            <button type="button" class="btn-style-two backbtn2"> Reset All</button>--}}
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
{{--                        <div class="whitebox-panel">--}}
{{--                            <div class="whitebox-panel-header">--}}
{{--                                <h5 class="left-border">Laundry Reports</h5>--}}
{{--                            </div>--}}
{{--                            <div class="whitebox-panel-tabbing">--}}
{{--                                <ul class="nav nav-tabs nav-fill">--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link active" data-bs-toggle="tab" href="#tab1-day">Today</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" data-bs-toggle="tab" href="#tab2-week">Week</a>--}}
{{--                                    </li>--}}
{{--                                    <li class="nav-item">--}}
{{--                                        <a class="nav-link" data-bs-toggle="tab" href="#tab3-month">Month</a>--}}
{{--                                    </li>--}}
{{--                                </ul>--}}
{{--                                <div class="tab-content">--}}
{{--                                    <div class="tab-pane active" id="tab1-day">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-12">--}}
{{--                                                <div class="whitebox-panel-graphmain">--}}
{{--                                                    <div class="whitebox-panel-graph-body">--}}
{{--                                                        <div class="whitebox-panel-graph-body1">--}}
{{--                                                            <span class="font-style1">Branch income</span>--}}
{{--                                                            <span class="font-style3 pb-3">291.00 SR</span>--}}
{{--                                                            <span class="font-style4" style="color:#20E33F;">+32.00 </span>--}}
{{--                                                            <span class="font-style2">Previous Day</span>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="whitebox-panel-graph-body2">--}}
{{--                                                            <img src="images/graph-small.jpg" alt="" />--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="whitebox-panel-graph-body">--}}
{{--                                                        <div class="whitebox-panel-graph-body1">--}}
{{--                                                            <span class="font-style1">Canceled Orders</span>--}}
{{--                                                            <span class="font-style3 pb-3">3</span>--}}
{{--                                                            <span class="font-style4" style="color:#E32020;">+3 </span>--}}
{{--                                                            <span class="font-style2">Previous Day</span>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="whitebox-panel-graph-body2">--}}
{{--                                                            <img src="images/graph-small.jpg" alt="" />--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                    <div class="whitebox-panel-graph-body">--}}
{{--                                                        <div class="whitebox-panel-graph-body1">--}}
{{--                                                            <span class="font-style1">Delayed Orders</span>--}}
{{--                                                            <span class="font-style3 pb-3">0</span>--}}
{{--                                                            <span class="font-style4" style="color:#868686;">+0</span>--}}
{{--                                                            <span class="font-style2">Previous Day</span>--}}
{{--                                                        </div>--}}
{{--                                                        <div class="whitebox-panel-graph-body2">--}}
{{--                                                            <img src="images/graph-small.jpg" alt="" />--}}
{{--                                                        </div>--}}
{{--                                                    </div>--}}
{{--                                                </div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="tab-pane" id="tab2-week">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-12"> 2 </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                    <div class="tab-pane" id="tab3-month">--}}
{{--                                        <div class="row">--}}
{{--                                            <div class="col-md-12"> 3 </div>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
                        <div class="whitebox-panel">
                            <div class="whitebox-panel-header">
                                <h5 class="left-border">Latest Orders</h5>

                                <a href="{{route('admin.allorder','branch_id='.$branch_id)}}" class="viewall" type="button" >View All </a>
                            </div>
                            <div class="whitebox-panel-body">
                                <div class="table_responsive_maas v2 pt-0">
                                    <table class="table m-0" width="100%">
                                        <thead>
                                        <tr>
                                            <th>
                                                <span class="th-head-icon">Order #</span>
                                            </th>
                                            <th>
                                                <span class="th-head-icon">Branch</span>
                                            </th>
                                            <th>
                                                <span class="th-head-icon">Order Status</span>
                                            </th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($latestOrders as $order)
                                                <tr>
                                                    <td>{{$order->id}}</td>
                                                    <td>{{$order->branch->username}}</td>
                                                    <td>{{$order->delivery_status}}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

    @include('Admin.laundries.add_new_branch')
@endsection
@section('scripts')
    <script>
        function openNewBranchModal()
        {
            $("#add-new-branch").show();
        }
    </script>

<script>
    $("#pac-input").focusin(function() {
        $(this).val('');
    });

    $('#createLatitudeId').val('');
    $('#createLongitudeId').val('');


    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 24.740691,
                lng: 46.6528521
            },
            zoom: 13,
            mapTypeId: 'roadmap'
        });

        // move pin and current location
        infoWindow = new google.maps.InfoWindow;
        geocoder = new google.maps.Geocoder();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                map.setCenter(pos);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pos),
                    map: map,
                    title: 'موقعك الحالي'
                });

                //اضافه  الموقع  الحالي  الي  الحقل
                $('#createLatitudeId').val(pos.lat);
                $('#createLongitudeId').val(pos.lng);
                markers.push(marker);
                marker.addListener('click', function() {
                    geocodeLatLng(geocoder, map, infoWindow, marker);
                });
                // to get current position address on load
                google.maps.event.trigger(marker, 'click');
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            console.log('dsdsdsdsddsd');
            handleLocationError(false, infoWindow, map.getCenter());
        }

        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(map, 'click', function(event) {
            SelectedLatLng = event.latLng;
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        deleteMarkers();
                        addMarkerRunTime(event.latLng);
                        SelectedLocation = results[0].formatted_address;
                        console.log(results[0].formatted_address);
                        splitLatLng(String(event.latLng));
                        $("#pac-input").val(results[0].formatted_address);

                    }
                }
            });
        });

        function geocodeLatLng(geocoder, map, infowindow, markerCurrent) {
            var latlng = {
                lat: markerCurrent.position.lat(),
                lng: markerCurrent.position.lng()
            };
            /* $('#branch-latLng').val("("+markerCurrent.position.lat() +","+markerCurrent.position.lng()+")");*/
            $('#createLatitudeId').val(markerCurrent.position.lat());
            $('#createLongitudeId').val(markerCurrent.position.lng());

            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        map.setZoom(8);
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map
                        });
                        markers.push(marker);
                        infowindow.setContent(results[0].formatted_address);
                        SelectedLocation = results[0].formatted_address;
                        $("#pac-input").val(results[0].formatted_address);


                        infowindow.open(map, marker);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
            SelectedLatLng = (markerCurrent.position.lat(), markerCurrent.position.lng());
        }

        function addMarkerRunTime(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
        }

        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        function clearMarkers() {
            setMapOnAll(null);
        }

        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        $("#pac-input").val("أبحث هنا ");
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(100, 100),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));


                $('#createLatitudeId').val(place.geometry.location.lat());
                $('#createLongitudeId').val(place.geometry.location.lng());

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }

    function splitLatLng(latLng) {
        var newString = latLng.substring(0, latLng.length - 1);
        var newString2 = newString.substring(1);
        var trainindIdArray = newString2.split(',');
        var lat = trainindIdArray[0];
        var Lng = trainindIdArray[1];

        $("#createLatitudeId").val(lat);
        $("#createLongitudeId").val(Lng);

        console.log(lat);
        console.log(Lng);
    }
</script>




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBljsOSEkY07FkBuA6p0DmtOcE64VW-rfE&libraries=places&callback=initAutocomplete&language={{ app()->getLocale() }}
     async defer"></script>

@endsection
