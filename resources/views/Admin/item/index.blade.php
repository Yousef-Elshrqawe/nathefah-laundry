<?php
$active_links = ['items' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.items')</h5>
                                    <div class="search-box">
                                        {{--  <input type="text" name="item_search_input" id="" class="form-control" placeholder="Find a Laundry">
                                        <i class="fal fa-search"></i>  --}}

                                    </div>
                                    {{--  <div class="form-group">
                                        <input class="form-control" type="text"
                                            name="city_search_input"
                                            placeholder="{{__('admin/forms.search')}}">
                                    </div>  --}}
                                </div>
                                {{--  <div class="table-box-select">
                                    <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Sort By : Latest</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Subscription  : All</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>  --}}
                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-item-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.add_item') </a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">#</th>
                                            <th><span class="th-head-icon">@lang('admin.name')</th>
                                            <th><span class="th-head-icon">@lang('admin.category_name')</th>
                                            {{--  <th><span class="th-head-icon">@lang('admin.serial')</th>  --}}
                                            <th><span class="th-head-icon">@lang('admin.operations')</th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @isset($items)

                                        @foreach($items as $index => $item)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$item->name}}</td>
                                                <td class="text-info">{{$item->category->name ?? ''}}  </td>
                                                {{--  <td class="text-info">{{$item->serial ?? ''}}  </td>  --}}
                                                <td><a href="{{route('admin.items.edit', ['id'=> $item -> id])}}" class="cl-light">@lang('admin.edit')</a></td>
                                                {{--  <a href="{{route('admin.sides.edit', ['id'=> $side -> id])}}"
                                                    class="btn btn-info box-shadow-3 mr-1 "><i class="ft-edit"></i></a>  --}}
                                                {{--  <td><a href=""
                                                    class="btn btn-info box-shadow-3 mr-1 ">@lang('admin.edit')</i></a></td>  --}}
                                            </tr>
                                        @endforeach
                                        @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $items->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ============== CREATE NEW items MODAL===================== -->
    <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form id="msform" action="{{route('admin.items.store')}}" method="post" >
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="account"></li>
                            <li id="personal"></li>
                            <li id="miscla"></li>
                            <li id="confirm"></li>
                        </ul>

                        <fieldset id="firsttab">
                            <div class="modal-header p-0 mb-5">
                               <h5 class="left-border" id="head">@lang('admin.add_item')</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-box">
                                            @foreach (config('translatable.locales') as $locale)
                                                <label for="" class="form-label">{{ __('admin.'.$locale.'.name') }}</label>
                                                {{--  <label>{{ __('admin.'.$locale.'.name') }}</label>  --}}
                                                {{--  <input class="form-control" name="{{$locale}}[name]" value="{{$service->translateOrNew($locale)->name, true}}" type="text">  --}}
                                                <input type="text" name="{{$locale}}[name]" class="form-control" value="{{ old($locale . '.name') }}" aria-describedby="emailHelp" placeholder="{{ __('admin.'.$locale.'.name') }}" required>
                                                @if ($errors->has($locale.'.name'))
                                                    <p class="alert-danger">{{ $errors->first($locale.'.name')}}</p>
                                                {{--  @error('{{$locale}}[name]')
                                                    <p class="alert-danger">{{ $message }}</p>
                                                @enderror  --}}
                                                @endif
                                            @endforeach
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        {{--  <div class="form-box">
                                            <label for="" class="form-label">@lang('admin.serial')</label>
                                            <input type="text" name="serial" class="form-control"  aria-describedby="emailHelp" placeholder="@lang('admin.serial')" required>
                                            @error('serial')
                                              <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>  --}}

                                        <div class="form-box">
                                            <label> @lang('admin.category')</label>
                                            <select name="category_id" class="form-control">
                                                <option disabled selected >@lang('admin.select_category')</option>
                                                    @if($categories && $categories -> count() > 0)
                                                    @foreach($categories as $category)
                                                    <option value="{{$category->id }}" > {{$category->name}}
                                                    </option>
                                                    @endforeach
                                                    @endif
                                                </optgroup>
                                            </select>
                                            @error('category_id')
                                            <span class="text-danger"> {{$message}}</span>
                                            @enderror
                                        </div>
                                    </div>

                                </div>

                                <div class="btn-question mt-4">
                                    <button  type="submit" name="next" class="next btn-style-one" value="" ><i class="fas fa-plus"></i>@lang('admin.add_item')</button >
                                    <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                                </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>



@endsection


@section('scripts')

@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-item-modal').modal('show');
        });
</script>
@endif

@endsection
