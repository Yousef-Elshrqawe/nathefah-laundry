<?php
$active_links = ['items' , ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.item_edit')</h5>
                                    <div class="search-box">

                                    </div>


                                {{--  <div class="table-box-select">
                                    <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Sort By : Latest</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                    <div class="d-select-box">
                                        <select class="form-select" aria-label="Default select example">
                                            <option selected>Subscription  : All</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>  --}}
                            </div>




                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <div class="card-body">
                                    <form class="form" action="{{route('admin.items.update' , ['id'=> $item -> id])}}" method="post" >
                                        @csrf

                                        <div class="form-body">

                                            {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                            <div class="row">


                                                @foreach (config('translatable.locales') as $locale)
                                                    <label>{{ __('admin.'.$locale.'.name') }}</label>
                                                    <input class="form-control" name="{{$locale}}[name]" value="{{$item->translateOrNew($locale)->name, true}}" type="text" required>
                                                    @if ($errors->has($locale.'.name'))
                                                    <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                                                    @endif
                                                @endforeach
                                                {{--  @foreach (array_keys(config('laravellocalization.supportedLocales')) as
                                                $locale)
                                                <div class="col-md-6">
                                                    <div class="form-group">
                                                        <label>{{ __('admin/forms.'.$locale.'.name') }}</label>
                                                        <input type="text" name="{{ $locale }}[name]"
                                                            class="form-control" value="{{ $item->translate($locale)->name }}">
                                                        @error($locale.'.name')
                                                        <span class="text-danger">{{$message}}</span>
                                                        @enderror
                                                    </div>
                                                </div>
                                                @endforeach  --}}

                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="form-group">
                                                            <label> @lang('admin.category')</label>
                                                            <select name="category_id" class="select2 form-control">
                                                                <option disabled selected >@lang('admin.select_category')</option>
                                                                    @if($categories && $categories -> count() > 0)
                                                                    @foreach($categories as $category)
                                                                    <option @if ($category->id == $item->category_id)
                                                                        selected
                                                                    @endif value="{{$category->id }}">
                                                                        {{$category->name}}
                                                                        </option>
                                                                    @endforeach
                                                                    @endif
                                                                </optgroup>
                                                            </select>
                                                            @error('category_id')
                                                            <span class="text-danger"> {{$message}}</span>
                                                            @enderror
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>


                                        </div>

<br>
                                        <div class="form-actions">
                                            {{--  <button type="button" class="btn btn-warning mr-1"
                                                onclick="history.back();">
                                                <i class="ft-x"></i> تراجع
                                            </button>  --}}
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Update
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('scripts')

@endsection
