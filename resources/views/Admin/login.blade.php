<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>NATHEFAH Admin</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('dashboard/images/favicon.ico')}}">
<link href="{{asset('dashboard/css/bootstrap.min.css')}}"rel="stylesheet">
<link href="{{asset('dashboard/css/fontawesome-all.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="{{asset('dashboard/css/style.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('dashboard/css/menu.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('dashboard/css/animate.min.css')}}" type="text/css" media="screen">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
</head>
<body class="bg-cl-light">

    <section class="main-wrapper login-body">
        <div class="login-wrapper">
            <div class="container">
                <div class="loginbox">
                    <div class="logo">
                        <img src="images/laundry-logo.png" alt="">
                    </div>
                    <div class="form-sign">
                        <h4>Login Account</h4>
                        <form action="{{route('Admin.login.submit')}}" method="post">
                            @csrf
                            <div class="form-box-two">
                                <div class="mb-3">
                                <label for="" class="form-label">user name</label>
                                <input type="text" name="username" class="form-control" id="" aria-describedby="emailHelp" placeholder="user name">
                                </div>
                                <div class="mb-3">
                                    <label for="" class="form-label">Password</label>
                                    <input type="password" name="password" class="form-control" id="" placeholder="Enter your password">
                                </div>
                                <div class="mb-3 d-inline-block mt-3">
                                    <button type="submit" class="btn-style-one">log in</button><br>
                                    @if(Session::get('error')!=null)
                                    <div class="alert alert-danger">
                                        <ul>
                                            <li>{{ Session::get('error') }}</li>
                                        </ul>
                                    </div>
                                    @endif
                                    @if ($errors->any())
                                        <div class="alert alert-danger">
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li>{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    @endif
                                    {{-- <a href="#" class="color-gray mt-3 d-inline-block">have account ?</a><br>
                                    <a href="#" class="color-main">Sign in</a> --}}
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


</body>
</html>
