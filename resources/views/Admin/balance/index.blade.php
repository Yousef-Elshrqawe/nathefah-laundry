<?php
$active_links = ['balance' , ''];
?>
@extends('Admin_temp')
@section('content')
<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">
            @include('Admin.components.nav')

            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="inner-title-box">
                                <div>
                                    <h4 class="title-inner">Balance</h4>
                                </div>
                         {{--       <div class="btn-flex">
                                    <a href="#" class="btn-style-one"  data-bs-toggle="modal" data-bs-target="#transfer-money-modal"><i class="fas fa-plus"></i>Transfer Money</a>
                                </div>--}}
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Total Orders</p>
                                    </div>

                                </div>
                                @if(isset($total_order))
                                    <h4 class="cl-light">{{$total_order}} </h4>
                                @else
                                    <h4 class="cl-light">0</h4>
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Total Orders value</p>
                                    </div>

                                </div>
                                @if(isset($total_price))
                                <h4 class="cl-light">{{$total_price}} SR</h4>
                                @else
                                <h4 class="cl-light">0 SR</h4>
                                @endif
                            </div>
                        </div>
                        <div class="col-xl-4 col-md-6 mb-4">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Total Orders Revenue</p>
                                    </div>

                                </div>
                                @if(isset($app_percentage))
                                <h4 class="cl-light">{{$app_percentage}} SR</h4>
                                @else
                                <h4 class="cl-light">0 SR</h4>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div class="tran-flex">
                                <h5 class="left-border">Transactions</h5>
                                <div class="search-box">
                            {{--        <form id="search_form" action="{{route('Admin.transaction.search')}}" method="POST">
                                        @csrf
                                        <input type="hidden" name="type" value="{{$type}}">
                                        <input type="hidden" name="date" value="{{$date}}">
                                        <input type="text" name="search_key"  class="form-control" placeholder="Find a Transaction" value="{{old('search_key')}}">
                                        <i class="fal fa-search" style="cursor: pointer" onclick='this.parentNode.submit();'></i>
                                    </form>--}}

                                </div>
                            </div>
                            <div class="table-box-select">
                                <div class="d-select-box">
                                    <select class="form-select" id="select_date" aria-label="Default select example">
                                        <option value="All" @if($date=='All') selected  @endif>All</option>
                                        <option value="30" @if($date=='30') selected  @endif>One    moth</option>
                                        <option value="60" @if($date=='60') selected  @endif>Two    month</option>
                                        <option value="90" @if($date=='90') selected  @endif>Three  month</option>
                                    </select>
                                </div>
                                <div class="d-select-box">
                                    <select class="form-select" id="select_type" varia-label="Default select example">
                                        <option selected>Type : All</option>
                                        <option value="All" @if($type=='All') selected  @endif>All</option>
                                        <option value="order" @if($type=='order') selected  @endif>order</option>
                                        <option value="subscription" @if($type=='subscription') selected  @endif>subscription</option>
                                        <option value="Withdraw" @if($type=='Withdraw') selected  @endif>Withdraw</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="table_responsive_maas v2">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th><span class="th-head-icon">Amount</th>
                                        <th><span class="th-head-icon">Transaction Type</th>
                                        <th><span class="th-head-icon">Laundry name</th>
                                        <th><span class="th-head-icon">Customer name</th>
                                        <th><span class="th-head-icon">Transaction Type</th>
                                        <th><span class="th-head-icon">Transaction Date</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($transactions as $transaction)
                                    <tr>
                                        <td class="@if($transaction->transaction=='enternance')  cl-green @else cl-red-dark  @endif">{{round($transaction->amount,1)}} SR</td>
                                        <td>{{$transaction->transaction_type}}</td>
                                        @if ($transaction->branch!=null)
                                          <td>{{$transaction->branch->laundry->name}}</td>
                                        @else
                                        <td>
                                            @if ($transaction->transaction_type=='Subscription')
                                                {{$transaction->laundry->name}}
                                            @endif
                                        </td>
                                        @endif
                                        <td>
                                        @if ($transaction->order!=null)

                                            {{$transaction->order->customer_name ?? '---'}}

                                        @endif
                                       </td>

                                        <td>{{$transaction->payment_type ?? '---'}}</td>

                                        <td>{{ date('Y:M:d   h:i A', strtotime($transaction->created_at)) }}</td>
                                        <td><a href="#" class="cl-light view_transaction" data-id="{{$transaction->id}}" data-bs-toggle="modal" data-bs-target="#transfer-money-view">View</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="justify-content-center d-flex">
                                {!! $transactions->appends(Request::except('page'))->render() !!}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
            <!-- start transaction model -->
            <div class="modal fade" id="transfer-money-view" tabindex="-1" aria-labelledby="transfer-money-view" aria-hidden="true">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <div class="modal-view-title">
                            <h5 class="left-border">Transaction Details</h5>
                            <div class="margint-5">
                                <a href="#" class="btn-green">Dipost</a>
                            </div>
                        </div>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="tran-details">
                            <div class="tran-item">
                                <div class="tran-first-item">
                                    <div> <i class="far fa-usd-square"></i>Amount :    </div>
                                </div>
                                <div class="tran-two-item">
                                    <span class="cl-green font-18" id="amount"> SR</span>
                                </div>
                            </div>
                            <div class="tran-item">
                                <div class="tran-first-item">
                                    <div> <i class="far fa-user"></i>From   :        </div>
                                </div>
                                <div class="tran-two-item">

                                    <span class="cl-gray" id="from"></span>
                                </div>
                            </div>
                            <div class="tran-item">
                                <div class="tran-first-item">
                                    <div> <i class="far fa-file-invoice-dollar"></i>Transaction Type : </div>
                                </div>

                                <div class="tran-two-item">
                                    <span class="cl-gray" id="transaction_type"></span>
                                </div>
                            </div>






                            <div class="tran-item">
                                <div class="tran-first-item">
                                    <div> <i class="far fa-file"></i>Transaction Status :</div>
                                </div>
                                <div class="tran-two-item">
                                    <a href="#" class="btn-green">Success</a>
                                </div>
                            </div>
                            <div class="tran-item">
                                <div class="tran-first-item">
                                    <div> <i class="far fa-file-invoice-dollar" ></i>fort ID : </div>
                                </div>
                                <div class="tran-two-item">
                                    <span id="transaction_id"></span><span class="cl-65878F"></span>
                                </div>
                            </div>
                            <div class="tran-item">
                                <div class="tran-first-item">
                                    <div> <i class="far fa-file-invoice-dollar" ></i>Payment Type :</div>
                                </div>
                                <div class="tran-two-item">
                                    <span class="cl-gray" id="payment_type"> </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            {{-- end transaction model --}}
        </div>
    </div>
</div>

@endsection
@section('scripts')
{{-- stert sort script --}}
<script>
    var url = "{{ url('/Super-admin') }}";
    var balancetype=$('#select_type').find(":selected").val();
    var date=$('#select_date').find(":selected").val();
    var type={!!json_encode($type)!!}
    $('#select_type').change(function(){
        var balancetype=$(this).find(":selected").val();
        window.location.href=url+'/balance/'+balancetype+'/'+date;
    });

    $('#select_date').change(function(){
        var date=$(this).find(":selected").val();
        var balancetype=$('#select_type').find(":selected").val();
        window.location.href=url+'/balance/'+balancetype+'/'+date;
    });
</script>
{{-- end sort script --}}

{{-- start view transaction   --}}
<script>
$('.view_transaction').click(function(){
        var id=$(this).attr("data-id");
        $.ajax({
        url:url+"/transaction/view/"+id,
        type:"GET", //send it through get method
        success: function (response) {
           console.log(response);
           $('#payment_type').html(response.payment_type);
           $('#transaction_type').html(response.transaction_type);
           $('#amount').html(response.amount);
           $('#transaction_id').html(response.transaction_number);
           if(response.transaction_type=='subscription'){
               $('#from').html(response.laundry.name);
           }else{
            $('#from').html(response.order.customer_name);
           }

        },
        error: function(response) {
            console.log('fdfd');
        }
        });
});
</script>
{{-- end view transaction  --}}
@endsection
