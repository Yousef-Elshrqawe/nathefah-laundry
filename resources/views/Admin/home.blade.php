<?php
$active_links = ['home' , ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')

                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="row">
                            <div class="col-xl-12">
                                <h4 class="title-inner">
                                    Overview
                                </h4>
                            </div>
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Total Orders</p>
                                        </div>

                                    </div>
                                    <h4 class="cl-light">{{$total_order}} </h4>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Total Orders value</p>
                                        </div>

                                    </div>
                                    <h4 class="cl-light">{{$total_price}} SR</h4>
                                </div>
                            </div>
                            <div class="col-xl-4 col-md-6 mb-4">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Total Orders Revenue</p>
                                        </div>

                                    </div>

                                    <h4 class="cl-light">{{$app_percentage}} SR</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12 mb-4">
                        <div class="row">
                            <div class="col-xl-6 col-md-12 mb-3">
                                <div class="overview-box">
                                    <div class="graph-box">
                                        <div class="row">
                                            <div class="col-xl-5">
                                                <h5 class="left-border">Top Laundries</h5>
                                            </div>
                                            {{-- <div class="col-xl-7">
                                                <div class="gr-drop-select">
                                                    <div class="d-export-txt">
                                                        <div class="ex-img">
                                                            <img src="images/receipt-item.png" alt="">
                                                        </div>
                                                        <div>
                                                            <p class="cl-65878F">Export</p>
                                                        </div>
                                                    </div>
                                                    <div class="d-select-box">
                                                        <select class="form-select" aria-label="Default select example">
                                                            <option selected>Period Last 30 Days</option>
                                                            <option value="1">One</option>
                                                            <option value="2">Two</option>
                                                            <option value="3">Three</option>
                                                        </select>
                                                    </div>
                                                    <div class="dropdown btn-dropdown">
                                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        </button>
                                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                            <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                            <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div> --}}
                                        </div>
                                    </div>
                                    <div class="table_responsive_maas v2">
                                        <table class="table" width="100%">
                                            <thead>
                                            <tr>
                                                <th><span class="th-head-icon">Laundry Name</span></th>
                                                <th><span class="th-head-icon">Total Orders</span></th>
                                                <th><span class="th-head-icon">Orders Rating</span></th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($topLaundries as $laundry)
                                              <tr>
                                                <td>{{$laundry->name}}</td>
                                                <td>{{$laundry->branchs->sum('allOrders')}}</td>
                                                <td>{{$laundry->rate ? number_format($laundry->rate,1) : 0}}</td>
                                                <td><a href="{{route('Admin.laundry.details',$laundry->id)}}" class="cl-light">Visit Profile</a></td>
                                            </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12 mb-3">
                                <div class="overview-box">
                                    <div class="graph-box">
                                        <div class="row">
                                            <div class="col-xl-5">
                                                <h5 class="left-border">Bad Review Laundries</h5>
                                            </div>
                                            <div class="col-xl-7">
                                                {{-- <div class="gr-drop-select">
                                                    <div class="d-export-txt">
                                                        <div class="ex-img">
                                                            <img src="images/receipt-item.png" alt="">
                                                        </div>
                                                        <div>
                                                            <p class="cl-65878F">Export</p>
                                                        </div>
                                                    </div>
                                                    <div class="d-select-box">
                                                        <select class="form-select" aria-label="Default select example">
                                                            <option selected>Period Last 30 Days</option>
                                                            <option value="1">One</option>
                                                            <option value="2">Two</option>
                                                            <option value="3">Three</option>
                                                        </select>
                                                    </div>
                                                    <div class="dropdown btn-dropdown">
                                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                        </button>
                                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                            <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                            <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                        </ul>
                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="table_responsive_maas v2">
                                        <table class="table" width="100%">
                                            <thead>
                                            <tr>
                                                <th><span class="th-head-icon">Laundry Name</span></th>
                                                <th><span class="th-head-icon">Total Orders</span></th>
                                                <th><span class="th-head-icon">Orders Rating</span></th>
                                                <th>&nbsp;</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($badLaundries as $laundry)
                                                  <tr>
                                                    <td>{{$laundry->name}}</td>
                                                    <td>{{$laundry->branchs->sum('allOrders')}}</td>
                                                      <td>{{$laundry->rate ? number_format($laundry->rate,1) : 0}}</td>
                                                      <td><a href="{{route('Admin.laundry.details',$laundry->id)}}" class="cl-light">Visit Profile</a></td>
                                                  </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


@endsection
