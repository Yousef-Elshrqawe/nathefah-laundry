<!-- ============== CREATE NEW CATEGORY MODAL===================== -->
<div class="modal fade" id="edit-period-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="" action="{{route('Admin.periods.update')}}" method="post" >
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="periodId" name="id">

                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border" id="head">@lang('admin.period')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">

                        @foreach (config('translatable.locales') as $locale)
                            <label>{{ __('dashboard.'.$locale.'.name') }}</label>
                            <input class="form-control" name="{{$locale}}[name]" id="{{$locale}}_name" value="" type="text" required>
                            @if ($errors->has($locale.'.name'))
                            <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                            @endif
                        @endforeach

                    </div>

                    <div class="form-group">
                        <label>@lang('admin.duration_value')</label>
                        <input  class="form-control" type="number" name="duration_value" min="1" id="editPackageDurationValue" placeholder="Type Package Description Value" required>
                    </div>

                    <div class="form-group">
                        <label>@lang('admin.duration_unit')</label>
                        <select class="form-control" name="duration_unit" id="editPackageDurationUnit" required>
                            <option selected value="">Select @lang('admin.duration_unit')</option>
                            <option value="day">Day</option>
                            <option value="week">Week</option>
                            <option value="month">Month</option>
                            <option value="year">Year</option>
                        </select>
                    </div>



                    <div class="btn-question mt-4">
                        <button  type="submit" class="btn-style-one">
                            <i class="fas fa-plus"></i>
                            @lang('admin.edit')
                        </button >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
