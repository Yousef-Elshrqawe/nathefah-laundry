<!-- ============== CREATE NEW CATEGORY MODAL===================== -->
<div class="modal fade" id="new-period-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="" action="{{route('Admin.periods.store')}}" method="post" >
                    @csrf
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border" id="head">@lang('admin.period')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">



                       @foreach (config('translatable.locales') as $locale)
                            <div>
                                <label>{{ __('dashboard.'.$locale.'.name') }}</label>
                                <input class="form-control" name="{{$locale}}[name]" type="text" required>
                            </div>
                            @if ($errors->has($locale.'.name'))
                            <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                            @endif
                        @endforeach




                        {{-- <div class="form-group">
                            <label>@lang('admin.name')</label>
                            <input  class="form-control" type="text" name="name" id="packageName" placeholder="Type Period Name" required>
                        </div> --}}
                    </div>


                    <div class="form-group">
                        <label>@lang('admin.duration_value')</label>
                        <input  class="form-control" type="number" name="duration_value" min="1" id="packageDescriptionValue" placeholder="Type Package Description Value" required>
                    </div>


                    <div class="form-group">
                        <label>@lang('admin.duration_unit')</label>
                        <select class="form-control" name="duration_unit" id="packageDurationUnit" required>
                            <option selected value="">Select @lang('admin.duration_unit')</option>
                            <option value="day">Day</option>
                            <option value="week">Week</option>
                            <option value="month">Month</option>
                            <option value="year">Year</option>
                        </select>
                    </div>


                    <div class="btn-question mt-4">
                        <button  type="submit" class="btn-style-one">
                            <i class="fas fa-plus"></i>
                            @lang('admin.add')
                        </button >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
