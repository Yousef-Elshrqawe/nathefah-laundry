<?php
$active_links = ['periods' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                @include('Admin.components.nav')


                <div class="col-xl-12 mb-4 mt-3">
                    <div class="overview-box">
                        <div class="table-box">
                            <div class="tab-flex">
                                <div>
                                </div>
                                <div class="tab-box">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                        </li>
                                        <li class="nav-item">
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="table-box-select">

                                <div class="d-select-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-period-modal"><i class="fas fa-plus"></i> Add new period</a>
                                </div>
                            </div>
                        </div>
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.periods')</h5>
                                </div>
                                <div class="d-selct-view">
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">{{__('dashboard.en.name')}}</span></th>
                                        <th><span class="th-head-icon">{{__('dashboard.ar.name')}}</span></th>

                                        <th><span class="th-head-icon">@lang('admin.subscriptions')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operation')</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($periods as $key=>$period)
                                        <tr>
                                            <td class="text-info">{{$key+1}}</td>

                                            <td class="text-info">{{$period->translateOrNew('en')->name}}</td>
                                            <td class="text-info">{{$period->translateOrNew('ar')->name}}</td>

                                            <td class="text-info"> {{$period->duration_value}} {{$period->duration_unit}}</td>
                                            <td style="display: -webkit-inline-box;">
                                                <button  class="btn btn-success m-1" data-bs-toggle="modal" data-bs-target="#edit-period-modal"
                                                         data-id="{{$period->id}}" data-name_ar="{{$period->translateOrNew('ar')->name}}"
                                                         data-name_en="{{$period->translateOrNew('en')->name}}"
                                                         data-duration_value="{{$period->duration_value}}" data-duration_unit="{{$period->duration_unit}}">
                                                    @lang('admin.edit')
                                                </button>

                                                <form action="{{route('Admin.periods.delete',$period->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"  class="btn btn-danger m-1">
                                                        @lang('admin.delete')
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




    @include('Admin.periods.create')
    @include('Admin.periods.edit')

@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $('#edit-period-modal').on('show.bs.modal', function(event) {
            let button          = $(event.relatedTarget)
            let id              = button.data('id')
            let name_ar         = button.data('name_ar')
            let name_en         = button.data('name_en')
            let durationValue   = button.data('duration_value')
            let durationUnit    = button.data('duration_unit')
            let modal = $(this)
            modal.find('.modal-body #periodId').val(id);
            modal.find('.modal-body #editPeriodName').val(name);
            modal.find('.modal-body #editPackageDurationValue').val(durationValue);
            modal.find('.modal-body #editPackageDurationUnit').val(durationUnit);


            modal.find('.modal-body #ar_name').val(name_ar);
            modal.find('.modal-body #en_name').val(name_en);

        })
    </script>

@endsection
