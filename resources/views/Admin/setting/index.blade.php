

<?php
$active_links = ['setting' , ''];
?>

@extends('Admin_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            @include('Admin.components.nav')

            <div class="row">
                <div class="col-xl-12 mb-4">
                    <h4 class="title-inner">Settings</h4>

                    <div class="whitebox-shadow">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-12 lefttab-nav">
                                <ul class="nav nav-tabs">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-bs-toggle="tab" href="#leftnav1">Basic info</a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" @if(Session('add_user')) active  @endif data-bs-toggle="tab" href="#leftnav2">Password</a>
                                    </li>

                                    {{-- <li class="nav-item">
                                        <a class="nav-link" data-bs-toggle="tab" href="#leftnav3">Users & Permissions</a>
                                    </li> --}}
                                </ul>
                            </div>
                            <div class="col-xl-9 col-lg-9 col-md-12">
                                <div class="tab-content setting-body">
                                    <div class="tab-pane active" id="leftnav1">
                                            <div class="row">
                                                <div class="col-md-12">

                                                <h5 class="pb-4 left-border">Brand info </h5>




                                                <div class="line-title lightcolor"><h6>Basic info</h6></div>
                                                    <div class="row"><div class="col-lg-11">
                                                     <form action="{{route('Admin.setting.update')}}" method="post">
                                                        @csrf
                                                        <div class="row pt-2">
                                                            <div class="col-md-6">
                                                                <div class="form-box mb-4">
                                                                    <label for="" class="form-label">name</label>
                                                                    <input type="text" name="name" class="form-control" id="" value="{{$user->name}}" placeholder="Fresh &amp; Clean " required>
                                                                </div>

                                                                @error('name')
                                                                <p class="alert-danger">{{ $message }}</p>
                                                                 @enderror
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-box mb-4">
                                                                    <label for="" class="form-label">Email</label>
                                                                    <input type="email" name="email" class="form-control" id="" value="{{$user->email}}" placeholder="Fresh&amp;Clean@mail.com" required>
                                                                </div>

                                                                @error('email')
                                                                <p class="alert-danger">{{ $message }}</p>
                                                                 @enderror
                                                            </div>


                                                            <div class="col-md-6">
                                                                <div class="form-box mb-4">
                                                                    <label for="" class="form-label">user name</label>
                                                                    <input type="text" name="username" class="form-control" id="" value="{{$user->username}}" placeholder="Fresh&amp;Clean@mail.com" required>
                                                                </div>

                                                                @error('username')
                                                                   <p class="alert-danger">{{ $message }}</p>
                                                                @enderror
                                                            </div>



                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-12 mt-2 mb-4">
                                                                <div class="form-box">
                                                                    <button type="submit" class="btn-style-two"><i class="far fa-check"></i> Accept Changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </form>
                                                    </div>
                                                </div>









                                                </div>
                                            </div>
                                        </div>




                                        <div class="tab-pane " id="leftnav2">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <form action="{{route('Admin.setting.update')}}" method="post">
                                                        @csrf
                                                        <div class="row pt-2">
                                                            <div class="col-md-6">
                                                                <div class="form-box mb-4">
                                                                    <label for="" class="form-label">new password</label>
                                                                    <input type="password" name="password" class="form-control" id="">
                                                                </div>
                                                                @error('password')
                                                                  <p class="alert-danger">{{ $message }}</p>
                                                                @enderror
                                                            </div>

                                                            <div class="col-md-6">
                                                                <div class="form-box mb-4">
                                                                    <label for="" class="form-label">confirm password</label>
                                                                    <input type="password" name="password_confirmation" class="form-control" id="">
                                                                </div>

                                                                @error('password_confirmation')
                                                                  <p class="alert-danger">{{ $message }}</p>
                                                                @enderror

                                                            </div>





                                                        </div>


                                                        <div class="row">
                                                            <div class="col-md-12 mt-2 mb-4">
                                                                <div class="form-box">
                                                                    <button type="submit" class="btn-style-two"><i class="far fa-check"></i> Accept Changes</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                      </form>
                                                </div>
                                            </div>
                                        </div>




                                        <div class="tab-pane " id="leftnav3">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="d-flex justify-content-end">
                                                        <button type="submit" class="btn-style-two" data-bs-toggle="modal" data-bs-target="#add-user-modal"> Add user</button>
                                                    </div>
                                                    <h5 class="pb-4 left-border">Users</h5>
                                                    <h6 class="pb-2">Current Uses</h6>

                                                    <div class="row settings-permission-main">
                                                      <div class="col-xl-4 col-lg-6 col-md-6">
                                                            <div class="settings-permission-info">

                                                              <div class="settings-permission-infotop">
                                                                  <div class="settings-permission-infotop1"><img src="images/profile.jpg" alt=""/></div>
                                                                  <div class="settings-permission-infotop2">
                                                                  <h6>{{Auth::guard('Admin')->user()->name}}</h6>
                                                                  {{-- Manager --}}
                                                                  </div>
                                                              </div>
                                                                <div class="d-flex align-items-center justify-content-between">
                                                                    <span><a href="#" class="btn-style-two border">Edit permissions</a></span>
                                                                    <span><a href="#" class="btn-style-two backbtn2">Delete</a></span>
                                                                </div>

                                                        </div>
                                                      </div>

                                                    </div>

                                                    <h6 class="pb-2">All Users</h6>
                                                    <div class="row settings-permission-main">

                                                     @foreach ($users as $user)
                                                        <div class="col-xl-4 col-lg-6 col-md-6">
                                                                <div class="settings-permission-info">

                                                                <div class="settings-permission-infotop">
                                                                    <div class="settings-permission-infotop1"><img src="images/profile.jpg" alt=""/></div>
                                                                    <div class="settings-permission-infotop2">
                                                                    <h6>Ahmed Esam</h6>
                                                                    Manager
                                                                    </div>
                                                                </div>
                                                                    <div class="d-flex align-items-center justify-content-between">
                                                                        <span><a href="#" class="btn-style-two border">Edit permissions</a></span>
                                                                        <span><a href="#" class="btn-style-two backbtn2">Delete</a></span>
                                                                    </div>
                                                        </div>
                                                        </div>
                                                      @endforeach
                                                      </div>


                                                    </div>




                                                </div>
                                            </div>
                                        </div>




                                        <div class="modal fade" id="add-user-modal" tabindex="-1" aria-labelledby="add-user-modal" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">

                                                    <div class="modal-body">
                                                        <form action="{{route('Admin.setting.adduser')}}" method="post">
                                                            @csrf
                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-box mb-4">
                                                                        <label for="" class="form-label">name</label>
                                                                        <input type="text" name="name" class="form-control" placeholder="name" id="" required>
                                                                    </div>

                                                                    @error('name')
                                                                      <p class="alert-danger">{{ $message }}</p>
                                                                    @enderror

                                                                </div>


                                                                <div class="col-md-6">
                                                                    <div class="form-box mb-4">
                                                                        <label for="" class="form-label">email</label>
                                                                        <input type="email" name="email" class="form-control" placeholder="email" id="" required>
                                                                    </div>

                                                                    @error('email')
                                                                      <p class="alert-danger">{{ $message }}</p>
                                                                    @enderror

                                                                </div>





                                                                <div class="col-md-6">
                                                                    <div class="form-box mb-4">
                                                                        <label for="" class="form-label">user name</label>
                                                                        <input type="text" name="username" class="form-control" placeholder="username" id="" required>
                                                                    </div>

                                                                    @error('username')
                                                                      <p class="alert-danger">{{ $message }}</p>
                                                                    @enderror

                                                                </div>

                                                                <div class="col-md-6">
                                                                    <div class="form-box mb-4">
                                                                        <label for="" class="form-label">password</label>
                                                                        <input type="password" name="password" class="form-control" placeholder="password" id="" required>
                                                                    </div>

                                                                    @error('password')
                                                                      <p class="alert-danger">{{ $message }}</p>
                                                                    @enderror

                                                                </div>


                                                                <div class="col-md-6">
                                                                    <div class="form-box mb-4">
                                                                        <label for="" class="form-label">confirm password</label>
                                                                        <input type="password" name="password_confirmation" placeholder="confirm password" class="form-control" id="">
                                                                    </div>

                                                                    @error('password_confirmation')
                                                                      <p class="alert-danger">{{ $message }}</p>
                                                                    @enderror

                                                                </div>
                                                            </div>





                                                            <div class="row">
                                                                <div class="col-md-12 mt-2 mb-4">
                                                                    <div class="form-box">
                                                                        <button type="submit" class="btn-style-two"><i class="far fa-check"></i> Add</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>



                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#add-user-modal').modal('show');
        });
</script>
@endif
@endsection
