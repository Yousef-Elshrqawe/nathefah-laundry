<!-- ============== CREATE NEW CATEGORY MODAL===================== -->
<div class="modal fade" id="edit-package-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="" action="{{route('Admin.packages.update')}}" method="post" >
                    @csrf
                    @method('PUT')
                    <input type="hidden" id="packageId" name="id">

                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border" id="head">@lang('admin.package')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">


                        @foreach (config('translatable.locales') as $locale)
                            <div>
                                <label>{{ __('dashboard.'.$locale.'.name') }}</label>
                                <input class="form-control" name="{{$locale}}[name]" id="name_{{$locale}}" type="text" required>
                            </div>
                            @if ($errors->has($locale.'.name'))
                            <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                            @endif
                        @endforeach


                        @foreach (config('translatable.locales') as $locale)
                            <div>
                                <label>{{ __('dashboard.'.$locale.'.description') }}</label>
                                <input class="form-control" name="{{$locale}}[description]" id="description_{{$locale}}" type="text" required>
                            </div>
                            @if ($errors->has($locale.'.description'))
                            <p class="text-danger">{{ $errors->first($locale.'.description')}}</p>
                            @endif
                        @endforeach



                        {{-- <div class="form-group">
                            <label>@lang('admin.name')</label>
                            <input  class="form-control" type="text" name="name" id="editPackageName" placeholder="Type Package Name" required>
                        </div>

                        <div class="form-group">
                            <label>@lang('admin.description')</label>
                            <input  class="form-control" type="text" name="description" id="editPackageDescription" placeholder="Type Package Description" required>
                        </div> --}}

                        <div class="form-group">
                            <label>@lang('admin.min_branch')</label>
                            <input  class="form-control" type="number" min="1" name="min_branch" id="editPackageMinNumberOfBranches" placeholder="Type Minimum Number Of Branches" required>
                        </div>

                        <div class="form-group">
                            <label>@lang('admin.max_branch')</label>
                            <input  class="form-control" type="number" min="1" name="max_branch" id="editPackageMaxNumberOfBranches" placeholder="Type Maximum Number Of Branches" required>
                        </div>

                        <div class="form-group">
                            <label>@lang('admin.period')</label>
                            <select class="form-control" name="period_id" id="editPackagePeriod" required>
                                <option selected value="">Select @lang('admin.period')</option>
                                @foreach($periods as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>


                        <div class="form-group">
                            <label>@lang('admin.price')</label>
                            <input  class="form-control" type="text" name="price" id="editPackagePrice" placeholder="Type Package Price" required>
                        </div>

                    </div>

                    <div class="btn-question mt-4">
                        <button  type="submit" class="btn-style-one">
                            <i class="fas fa-plus"></i>
                            @lang('admin.edit')
                        </button >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
