<?php
$active_links = ['packages' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="col-xl-12 mb-4 mt-3">
                    <div class="overview-box">
                        <div class="table-box">
                            <div class="tab-flex">
                                <div>
                                </div>
                                <div class="tab-box">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                        </li>
                                        <li class="nav-item">
                                        </li>

                                    </ul>
                                </div>
                            </div>
                            <div class="table-box-select">

                                <div class="d-select-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-package-modal"><i class="fas fa-plus"></i> Add new Package</a>
                                </div>
                            </div>
                        </div>
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.packages')</h5>
                                </div>
                                <div class="d-selct-view">
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.description')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.min_branches')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.max_branches')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.period')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.price')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operation')</span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($packages as $key=>$package)
                                        <tr>
                                            <td class="text-info">{{$key+1}}</td>
                                            <td class="text-info">{{$package->translateOrNew('en')->name}}</td>
                                            <td class="text-info">{{$package->translateOrNew('en')->description}}</td>
                                            <td class="text-info">{{$package->min_branch}}</td>
                                            <td class="text-info">{{$package->max_branch}}</td>
                                            <td class="text-info">{{$package->period->name}}</td>
                                            <td class="text-info">{{$package->price}}</td>
                                            <td style="display: -webkit-inline-box;">
                                                <button  class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-package-modal"
                                                         data-id="{{$package->id}}"

                                                         data-name_ar="{{$package->translateOrNew('ar')->name}}"
                                                         data-name_en="{{$package->translateOrNew('en')->name}}"


                                                         data-description_ar="{{$package->translateOrNew('ar')->description}}"
                                                         data-description_en="{{$package->translateOrNew('en')->description}}"                                                         data-description="{{$package->description}}" data-period="{{$package->period_id}}"


                                                         data-min_branch="{{$package->min_branch}}"    data-max_branch="{{$package->max_branch}}"
                                                         data-price="{{$package->price}}">
                                                    @lang('admin.edit')
                                                </button>

                                                <form action="{{route('Admin.packages.delete',$package->id)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button type="submit"  class="btn btn-danger">
                                                        @lang('admin.delete')
                                                    </button>
                                                </form>

                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




    @include('Admin.packages.create')
    @include('Admin.packages.edit')

@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $('#edit-package-modal').on('show.bs.modal', function(event) {
            let button          = $(event.relatedTarget)
            let id              = button.data('id')
            let name_ar         = button.data('name_ar')
            let name_en         = button.data('name_en')

            let description_ar  = button.data('description_ar')
            let description_en  = button.data('description_en')

            let minBranch       = button.data('min_branch')
            let maxBranch       = button.data('max_branch')
            let period          = button.data('period')
            let price           = button.data('price')
            let modal = $(this)
            modal.find('.modal-body #packageId').val(id);

            modal.find('.modal-body #name_ar').val(name_ar);
            modal.find('.modal-body #name_en').val(name_en);

            modal.find('.modal-body #description_ar').val(description_ar);
            modal.find('.modal-body #description_en').val(description_en);


          //  modal.find('.modal-body #editPackageDescription').val(description);
            modal.find('.modal-body #editPackageMinNumberOfBranches').val(minBranch);
            modal.find('.modal-body #editPackageMaxNumberOfBranches').val(maxBranch);
            modal.find('.modal-body #editPackagePeriod').val(period);
            modal.find('.modal-body #editPackagePrice').val(price);
        })
    </script>

@endsection
