<?php
$active_links = ['additional' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">Additional Services</h5>
                                </div>

                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-service-modal">
                                        <i class="fas fa-plus"></i>Add Additional Service</a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon"></span>#</th>
                                            <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                            <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($services as $index => $service)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$service->name}}</td>
                                                <td>
                                                    <a href="{{route('admin.additional-services.edit', ['id'=> $service -> id])}}"
                                                       class="cl-light">@lang('admin.edit')
                                                    </a>

                                                    <a  class="cl-green m-3" style="cursor: pointer" onclick="categories({{$service->id}})">Categories</a>

                                                </td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $services->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('Admin.additional_services.create')
    @include('Admin.additional_services.category')


@endsection


@section('scripts')

@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-service-modal').modal('show');
        });
</script>
@endif
    @include('Admin.additional_services.script')
@endsection
