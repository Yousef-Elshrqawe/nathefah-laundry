<?php
$active_links = ['additional' , ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">Edit Additional Service</h5>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <div class="card-body">
                                    <form class="form" action="{{route('admin.additional-services.update' , ['id'=> $service -> id])}}" method="post">
                                        @csrf
                                        <div class="form-body">
                                            <div class="row">
                                                @foreach (config('translatable.locales') as $locale)
                                                    <label>{{ __('admin.'.$locale.'.name') }}</label>
                                                    <input class="form-control" name="{{$locale}}[name]" value="{{$service->translateOrNew($locale)->name, true}}" type="text" required>
                                                    @if ($errors->has($locale.'.name'))
                                                    <p class="text-danger">{{ $errors->first($locale.'.name')}}</p>
                                                    @endif
                                                @endforeach

                                            </div>
                                        </div>
                                        <br>

                                        <div class="form-actions">

                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i>
                                                Update
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('scripts')

@endsection
