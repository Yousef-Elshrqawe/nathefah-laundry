<?php
$active_links = ['notifications' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">
                    <div class="col-xl-12">

                        <div class="card-body">
                            <form class="form" action="{{route('Admin.send-notifications.store')}}" method="POST">
                                @csrf

                                <div class="form-body">

                                    {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                    <div class="row">

                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="form-group">
                                                    <label> @lang('admin.'.$type)</label>
                                                    <select name="user_id" class="select2 form-control" required>
                                                        <option value="" selected >@lang('admin.select_'.$type)</option>
                                                        <option value="all">@lang('admin.select_all')</option>
                                                        @foreach($rows as $user)
                                                            @if($type == 'branch')
                                                                <option value="{{$user->id }}">
                                                                    {{$user->username}}
                                                                </option>
                                                            @else
                                                                <option value="{{$user->id }}">
                                                                    {{$user->name}}
                                                                </option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    @error('user_id')
                                                    <span class="text-danger"> {{$message}}</span>
                                                    @enderror
                                                </div>
                                            </div>
                                        </div>


                                            <label>{{ __('admin.ar.title') }}</label>
                                            <input class="form-control" name="title_ar" value="" type="text" required>
                                            @if ($errors->has('ar.title'))
                                                <p class="text-danger">{{ $errors->first('ar.title')}}</p>
                                            @endif

                                            <label>{{ __('admin.en.title') }}</label>
                                            <input class="form-control" name="title_en" value="" type="text" required>
                                            @if ($errors->has('en.title'))
                                                <p class="text-danger">{{ $errors->first('en.title')}}</p>
                                            @endif



                                            <label>{{ __('admin.ar.body') }}</label>
                                            <input class="form-control" name="body_ar" value="" type="text" required>
                                            @if ($errors->has('ar.body'))
                                                <p class="text-danger">{{ $errors->first('ar.body')}}</p>
                                            @endif

                                            <label>{{ __('admin.en.body') }}</label>
                                            <input class="form-control" name="body_en" value="" type="text" required>
                                            @if ($errors->has('en.body'))
                                                <p class="text-danger">{{ $errors->first('en.body')}}</p>
                                            @endif


                                        <input class="form-control" name="type" value="{{$type}}" type="hidden" required>






                                    </div>


                                </div>

                                <br>
                                <div class="form-actions">
                                     {{-- <button type="button" class="btn btn-warning mr-1"
                                        onclick="history.back();">
                                        <i class="ft-x"></i> تراجع
                                    </button> --}}
                                    <button type="submit" class="btn btn-primary">
                                        <i class="la la-check-square-o"></i> Send
                                    </button>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>




@endsection


@section('scripts')

@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-item-modal').modal('show');
        });
</script>
@endif

@endsection
