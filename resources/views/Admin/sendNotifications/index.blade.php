<?php
$active_links = ['notifications' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.users')</h5>
                                </div>
                                <div class="d-selct-view">
                                    <a href="{{route('Admin.send-notifications.create' , 'user')}}" class="btn-style-one" >
                                        <i class="fas fa-plus"></i>@lang('admin.send_notification') </a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <br>
                <br>
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.laundrys')</h5>
                                </div>
                                <div class="d-selct-view">
                                    <a href="{{route('Admin.send-notifications.create' , 'branch')}}" class="btn-style-one" >
                                        <i class="fas fa-plus"></i>@lang('admin.send_notification') </a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <br>
                <br>
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.drivers')</h5>
                                </div>
                                <div class="d-selct-view">
                                    <a href="{{route('Admin.send-notifications.create' , 'driver')}}" class="btn-style-one" >
                                    <i class="fas fa-plus"></i>@lang('admin.send_notification') </a>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
    </div>




@endsection


@section('scripts')

@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#new-item-modal').modal('show');
        });
</script>
@endif

@endsection
