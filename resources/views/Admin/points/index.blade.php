<?php
$active_links = ['points' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.points')</h5>
                                </div>
                                <div class="d-selct-view">
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">@lang('admin.amount')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.price')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="text-info">{{$points->amount}}</td>
                                            <td class="text-info">{{$points->price}}</td>
                                            <td style="display: -webkit-inline-box;">
                                                <button  class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-points-modal"
                                                         data-id="{{$points->id}}" data-amount="{{$points->amount}}"
                                                         data-price="{{$points->price}}">
                                                    @lang('admin.edit')
                                                </button>
                                            </td>
                                        </tr>

                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {{--                                    {!! $categories->appends(Request::except('page'))->render() !!}--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




    @include('Admin.points.edit')


@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $('#edit-points-modal').on('show.bs.modal', function(event) {
            let button = $(event.relatedTarget)
            let id     = button.data('id')
            let amount = button.data('amount')
            let price  = button.data('price')

            let modal = $(this)
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #amount').val(amount);
            modal.find('.modal-body #price').val(price);
        })
    </script>

@endsection
