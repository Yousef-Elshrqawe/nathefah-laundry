<?php
$active_links = ['update_apps', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <p class="left-border">update app platform : {{$row->platform}}</p>
                            </div>
                            <div class="table-box">
                                <p class="left-border">app name : {{$row->app_name}}</p>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">

                                    <div class="card-body">
                                        <form class="form"
                                              action="{{route('admin.update.apps.update' , ['id'=> $row -> id])}}"
                                              method="post">
                                            @csrf

                                            <div class="form-body">

                                                {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">version</label>
                                                            <input type="text" id="numberInput" pattern="^\d+(\.\d+)*$" name="version" class="form-control"
                                                                   value="{{ $row->version }}"
                                                                   placeholder="27.0.0" required>
                                                            @if ($errors->has('version'))
                                                                <p class="alert-danger">{{ $errors->first('version')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-4">
                                                        <div class="row">
                                                            <div class="form-group">
                                                                <label>is force update</label>
                                                                <select name="is_force_update"
                                                                        class="select2 form-control">
                                                                    <option disabled selected>select force update
                                                                    </option>
                                                                    <option value="1"
                                                                            @if($row->is_force_update == 1) selected @endif>
                                                                        force update
                                                                    </option>
                                                                    <option value="0"
                                                                            @if($row->is_force_update == 0) selected @endif>
                                                                        optional update
                                                                    </option>
                                                                </select>
                                                                @error('is_force_update')
                                                                <span class="text-danger"> {{$message}}</span>
                                                                @enderror
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>


                                            </div>

                                            <br>
                                            <div class="form-actions">
                                                {{--  <button type="button" class="btn btn-warning mr-1"
                                                    onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>  --}}
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Update
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')
            <script>
                document.getElementById('numberInput').addEventListener('input', function (e) {
                    const value = e.target.value;
                    // السماح بالأرقام والنقاط فقط
                    if (!/^\d*(\.\d*)*$/.test(value)) {
                        e.target.value = value.slice(0, -1); // إزالة آخر حرف مدخل إذا كان غير صالح
                    }
                });
            </script>
@endsection
