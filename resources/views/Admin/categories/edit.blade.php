<!-- ============== CREATE NEW CATEGORY MODAL===================== -->
<div class="modal fade" id="edit-category-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="" action="{{route('Admin.categories.update')}}" method="post" >
                    @csrf
                    @method('PUT')
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border" id="head">@lang('admin.edit_category')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <input type="hidden" name="id" id="category_id">
                        <div class="form-box">
                            @foreach (config('translatable.locales') as $locale)
                                <label for="" class="form-label">{{ __('admin.'.$locale.'.name') }}</label>
                                <input type="text" name="{{$locale}}[name]" id="{{$locale}}Id" class="form-control" value="{{ old($locale . '.name') }}" aria-describedby="emailHelp" placeholder="{{ __('admin.'.$locale.'.name') }}" required>
                                @if ($errors->has($locale.'.name'))
                                    <p class="alert-danger">{{ $errors->first($locale.'.name')}}</p>
                                @endif
                            @endforeach
                        </div>
                    </div>

                    <div class="btn-question mt-4">
                        <button  type="submit" class="btn-style-one">
                            <i class="fas fa-plus"></i>
                            @lang('admin.edit_category')
                        </button >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
