<?php
$active_links = ['categories' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">


                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">categories</h5>
                                </div>
                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-category-modal">
                                        <i class="fas fa-plus"></i>Add Categories
                                    </a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($categories)

                                        @foreach($categories as $index => $item)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$item->name}}</td>
                                                <td style="display: -webkit-inline-box;">

                                                    <button  class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-category-modal"
                                                             data-id="{{$item->id}}" data-name_ar="{{$item->categorytranslate[1]->name}}"
                                                             data-name_en="{{$item->categorytranslate[0]->name}}">
                                                        @lang('admin.edit')
                                                    </button>
                                                    <form action="{{route('Admin.categories.destroy', ['id'=> $item -> id])}}" method="POST">
                                                        @csrf
                                                        @method("DELETE")
                                                        <button class="btn btn-danger">@lang('admin.delete')</button>
                                                    </form>


                                                </td>

                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
{{--                                    {!! $categories->appends(Request::except('page'))->render() !!}--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    @include('Admin.categories.create')
    @include('Admin.categories.edit')


@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $('#edit-category-modal').on('show.bs.modal', function(event) {
            let button = $(event.relatedTarget)
            let id     = button.data('id')
            let nameAr = button.data('name_ar')
            let nameEn = button.data('name_en')

            let modal = $(this)
            modal.find('.modal-body #category_id').val(id);
            modal.find('.modal-body #arId').val(nameAr);
            modal.find('.modal-body #enId').val(nameEn);
        })
    </script>

@endsection
