<?php
$active_links = ['branches', ''];
?>

@extends('Admin_temp')

@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">Edit Time</h5>
                                    <div class="search-box">

                                    </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xl-12">

                                <div class="card-body">
                                    <form class="form" action="{{route('admin.branch.update.schedule' , ['id'=> $row -> id , 'branch_id' => $branch -> id])}}" method="post" >
                                        @csrf

                                        <div class="form-body">

                                            <div class="row">
                                                <input type="hidden" name="day_id" value="{{$row -> day_id}}">
                                                <div class="col-md-6">
                                                    <label>From</label>
                                                    <input type="time" class="form-control" name="start_time" value="{{$row -> start_time}}" required>
                                                    @if ($errors->has('start_time'))
                                                        <p class="text-danger">{{ $errors->first('start_time')}}</p>
                                                    @endif
                                                </div>
                                                <div class="col-md-6">
                                                    <label>To</label>
                                                    <input type="time" class="form-control" name="end_time" value="{{$row -> end_time}}" required>
                                                    @if ($errors->has('end_time'))
                                                        <p class="text-danger">{{ $errors->first('end_time')}}</p>
                                                    @endif
                                                </div>
                                                <div class="col-md-12">
                                                    <label>Status</label>
                                                    <select class="form-select" name="is_active" required>
                                                        <option value="1" @if($row -> is_active == 1) selected @endif>Active</option>
                                                        <option value="0" @if($row -> is_active == 0) selected @endif>Inactive</option>
                                                    </select>
                                                    @if ($errors->has('is_active'))
                                                        <p class="text-danger">{{ $errors->first('is_active')}}</p>
                                                    @endif
                                                </div>
                                            </div>


                                        </div>

<br>
                                        <div class="form-actions">
                                              <button type="button" class="btn btn-warning mr-1"
                                                onclick="history.back();">
                                                <i class="ft-x"></i> back
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                <i class="la la-check-square-o"></i> Update
                                            </button>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>





@endsection

@section('scripts')

@endsection
