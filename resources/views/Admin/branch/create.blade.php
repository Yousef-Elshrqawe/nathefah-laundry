<?php
$active_links = ['laundries', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')

                <div class="row">
                    <!-- ============== CREATE NEW CATEGORY MODAL===================== -->

                        <div class="modal-dialog">
                            <div class="modal-content">

                                <div class="modal-body">
                                    <form id="msform" action="{{ route('admin.branches.store') }}" method="post"  >
                                        @csrf
                                        <ul id="progressbar">
                                            <li class="active" id="account"></li>
                                            <li id="personal"></li>
                                            <li id="miscla"></li>
                                            <li id="confirm"></li>
                                        </ul>

                                        <fieldset id="firsttab">
                                            <div class="modal-header p-0 mb-5">
                                                <h5 class="left-border" id="head">@lang('admin.add_branch')</h5>
                                            </div>
                                            <div class="row">

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">{{ __('admin.username') }}</label>
                                                        <input type="text" name="username" class="form-control"
                                                               value="{{ old('username') }}"
                                                               placeholder="{{ __('admin.username') }}"
                                                               required  >
                                                        @if ($errors->has('username'))
                                                            <p class="alert-danger">{{ $errors->first('username') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.country_code')</label>
                                                        <input type="text" name="country_code" class="form-control"
                                                               value="+966"
                                                               disabled placeholder="@lang('admin.country_code')"
                                                                >
                                                        @if ($errors->has('country_code'))
                                                            <p class="alert-danger">{{ $errors->first('country_code') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.phone')</label>
                                                        <input type="number" name="phone" class="form-control"
                                                               value="{{ old('phone') }}"
                                                               placeholder="5xxxxxxxx" required  >
                                                        @if ($errors->has('phone'))
                                                            <p class="alert-danger">{{ $errors->first('phone') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.argent')</label>
                                                        <select name="argent" class="form-control" required  >
                                                            <option selected value="">Choose Argent</option>
                                                            <option value="1" {{ old('argent') == 1 ? 'selected' : '' }}>@lang('admin.yes')</option>
                                                            <option value="0" {{ old('argent') == 0 ? 'selected' : '' }}>@lang('admin.no')</option>
                                                        </select>
                                                        @if ($errors->has('argent'))
                                                            <p class="alert-danger">{{ $errors->first('argent') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.laundry')</label>
                                                        <select name="laundry_id" class="form-control" required  >
                                                            <option selected value="">Choose Laundry</option>
                                                            @foreach ($laundriesAll as $laundry)
                                                                <option value="{{ $laundry->id }}" {{ old('laundry_id') == $laundry->id ? 'selected' : '' }}>{{ $laundry->name }}</option>
                                                            @endforeach
                                                        </select>
                                                        @if ($errors->has('laundry_id'))
                                                            <p class="alert-danger">{{ $errors->first('laundry_id') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.status')</label>
                                                        <select name="status" class="form-control" required  >
                                                            <option selected value="">Choose Status</option>
                                                            <option value="open" {{ old('status') == 'open' ? 'selected' : '' }}>
                                                                @lang('admin.open')</option>
                                                            <option value="closed" {{ old('status') == 'closed' ? 'selected' : '' }}>
                                                                @lang('admin.closed')</option>
                                                        </select>
                                                        @if ($errors->has('status'))
                                                            <p class="alert-danger">{{ $errors->first('status') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.password')</label>
                                                        <input type="password" name="password" class="form-control"
                                                               value="{{ old('password') }}"
                                                               placeholder="@lang('admin.password')" required autocomplete="new-password">
                                                        @if ($errors->has('password'))
                                                            <p class="alert-danger">{{ $errors->first('password') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">@lang('admin.password_confirmation')</label>
                                                        <input type="password" name="password_confirmation" class="form-control"
                                                               value="{{ old('password_confirmation') }}"
                                                               placeholder="@lang('admin.password_confirmation')" required autocomplete="new-password">
                                                        @if ($errors->has('password_confirmation'))
                                                            <p class="alert-danger">{{ $errors->first('password_confirmation') }}</p>
                                                        @endif
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">Latitude</label>
                                                        <input type="text" name="lat" class="form-control"
                                                               id="createLatitudeId"
                                                               value="{{ old('lat') }}"
                                                               placeholder="Enter the Latitude" required  >
                                                        @error('lat')
                                                        <p class="alert-danger">{{ $message }}</p>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="form-box">
                                                        <label class="form-label">Longitude</label>
                                                        <input type="text" name="long" class="form-control"
                                                               id="createLongitudeId"
                                                               value="{{ old('long') }}"
                                                               placeholder="Enter the Longitude" required  >
                                                        @error('long')
                                                        <p class="alert-danger">{{ $message }}</p>
                                                        @enderror
                                                    </div>
                                                </div>

                                                <div class="col-md-12">
                                                    <div class="form-box">
                                                        <label class="form-label">Location</label>
                                                        <div id="map" style="height: 500px; width: 100%"></div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="btn-question mt-4">
                                                <button type="submit" name="next" class="next btn-style-one" value="">
                                                    <i class="fas fa-plus"></i>@lang('admin.add_branch')
                                                </button>
                                            </div>
                                        </fieldset>
                                    </form>

                                </div>
                            </div>
                        </div>


                </div>
            </div>
        </div>

@endsection

@section('scripts')
            <script>
                $("#pac-input").focusin(function() {
                    $(this).val('');
                });

                $('#createLatitudeId').val('{{ old('lat') }}');
                $('#createLongitudeId').val('{{ old('long') }}');


                // This example adds a search box to a map, using the Google Place Autocomplete
                // feature. People can enter geographical searches. The search box will return a
                // pick list containing a mix of places and predicted search terms.

                // This example requires the Places library. Include the libraries=places
                // parameter when you first load the API. For example:
                // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

                function initAutocomplete() {
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: {
                            lat: 24.740691,
                            lng: 46.6528521
                        },
                        zoom: 13,
                        mapTypeId: 'roadmap'
                    });

                    // move pin and current location
                    infoWindow = new google.maps.InfoWindow;
                    geocoder = new google.maps.Geocoder();
                    if (navigator.geolocation) {
                        navigator.geolocation.getCurrentPosition(function(position) {
                            var pos = {
                                lat: position.coords.latitude,
                                lng: position.coords.longitude,
                            };
                            map.setCenter(pos);
                            var marker = new google.maps.Marker({
                                position: new google.maps.LatLng(pos),
                                map: map,
                                title: 'موقعك الحالي'
                            });

                            //اضافه  الموقع  الحالي  الي  الحقل
                            $('#createLatitudeId').val(pos.lat);
                            $('#createLongitudeId').val(pos.lng);
                            markers.push(marker);
                            marker.addListener('click', function() {
                                geocodeLatLng(geocoder, map, infoWindow, marker);
                            });
                            // to get current position address on load
                            google.maps.event.trigger(marker, 'click');
                        }, function() {
                            handleLocationError(true, infoWindow, map.getCenter());
                        });
                    } else {
                        // Browser doesn't support Geolocation
                        console.log('dsdsdsdsddsd');
                        handleLocationError(false, infoWindow, map.getCenter());
                    }

                    var geocoder = new google.maps.Geocoder();
                    google.maps.event.addListener(map, 'click', function(event) {
                        SelectedLatLng = event.latLng;
                        geocoder.geocode({
                            'latLng': event.latLng
                        }, function(results, status) {
                            if (status == google.maps.GeocoderStatus.OK) {
                                if (results[0]) {
                                    deleteMarkers();
                                    addMarkerRunTime(event.latLng);
                                    SelectedLocation = results[0].formatted_address;
                                    console.log(results[0].formatted_address);
                                    splitLatLng(String(event.latLng));
                                    $("#pac-input").val(results[0].formatted_address);

                                }
                            }
                        });
                    });

                    function geocodeLatLng(geocoder, map, infowindow, markerCurrent) {
                        var latlng = {
                            lat: markerCurrent.position.lat(),
                            lng: markerCurrent.position.lng()
                        };
                        /* $('#branch-latLng').val("("+markerCurrent.position.lat() +","+markerCurrent.position.lng()+")");*/
                        $('#createLatitudeId').val(markerCurrent.position.lat());
                        $('#createLongitudeId').val(markerCurrent.position.lng());

                        geocoder.geocode({
                            'location': latlng
                        }, function(results, status) {
                            if (status === 'OK') {
                                if (results[0]) {
                                    map.setZoom(8);
                                    var marker = new google.maps.Marker({
                                        position: latlng,
                                        map: map
                                    });
                                    markers.push(marker);
                                    infowindow.setContent(results[0].formatted_address);
                                    SelectedLocation = results[0].formatted_address;
                                    $("#pac-input").val(results[0].formatted_address);


                                    infowindow.open(map, marker);
                                } else {
                                    window.alert('No results found');
                                }
                            } else {
                                window.alert('Geocoder failed due to: ' + status);
                            }
                        });
                        SelectedLatLng = (markerCurrent.position.lat(), markerCurrent.position.lng());
                    }

                    function addMarkerRunTime(location) {
                        var marker = new google.maps.Marker({
                            position: location,
                            map: map
                        });
                        markers.push(marker);
                    }

                    function setMapOnAll(map) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);
                        }
                    }

                    function clearMarkers() {
                        setMapOnAll(null);
                    }

                    function deleteMarkers() {
                        clearMarkers();
                        markers = [];
                    }

                    // Create the search box and link it to the UI element.
                    var input = document.getElementById('pac-input');
                    $("#pac-input").val("أبحث هنا ");
                    var searchBox = new google.maps.places.SearchBox(input);
                    map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

                    // Bias the SearchBox results towards current map's viewport.
                    map.addListener('bounds_changed', function() {
                        searchBox.setBounds(map.getBounds());
                    });

                    var markers = [];
                    // Listen for the event fired when the user selects a prediction and retrieve
                    // more details for that place.
                    searchBox.addListener('places_changed', function() {
                        var places = searchBox.getPlaces();

                        if (places.length == 0) {
                            return;
                        }

                        // Clear out the old markers.
                        markers.forEach(function(marker) {
                            marker.setMap(null);
                        });
                        markers = [];

                        // For each place, get the icon, name and location.
                        var bounds = new google.maps.LatLngBounds();
                        places.forEach(function(place) {
                            if (!place.geometry) {
                                console.log("Returned place contains no geometry");
                                return;
                            }
                            var icon = {
                                url: place.icon,
                                size: new google.maps.Size(100, 100),
                                origin: new google.maps.Point(0, 0),
                                anchor: new google.maps.Point(17, 34),
                                scaledSize: new google.maps.Size(25, 25)
                            };

                            // Create a marker for each place.
                            markers.push(new google.maps.Marker({
                                map: map,
                                icon: icon,
                                title: place.name,
                                position: place.geometry.location
                            }));


                            $('#createLatitudeId').val(place.geometry.location.lat());
                            $('#createLongitudeId').val(place.geometry.location.lng());

                            if (place.geometry.viewport) {
                                // Only geocodes have viewport.
                                bounds.union(place.geometry.viewport);
                            } else {
                                bounds.extend(place.geometry.location);
                            }
                        });
                        map.fitBounds(bounds);
                    });
                }

                function handleLocationError(browserHasGeolocation, infoWindow, pos) {
                    infoWindow.setPosition(pos);
                    infoWindow.setContent(browserHasGeolocation ?
                        'Error: The Geolocation service failed.' :
                        'Error: Your browser doesn\'t support geolocation.');
                    infoWindow.open(map);
                }

                function splitLatLng(latLng) {
                    var newString = latLng.substring(0, latLng.length - 1);
                    var newString2 = newString.substring(1);
                    var trainindIdArray = newString2.split(',');
                    var lat = trainindIdArray[0];
                    var Lng = trainindIdArray[1];

                    $("#createLatitudeId").val(lat);
                    $("#createLongitudeId").val(Lng);

                    console.log(lat);
                    console.log(Lng);
                }
            </script>




            <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBljsOSEkY07FkBuA6p0DmtOcE64VW-rfE&libraries=places&callback=initAutocomplete&language={{ app()->getLocale() }}
         async defer"></script>
@endsection
