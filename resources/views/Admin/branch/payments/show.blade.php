<?php
$active_links = ['branches', ''];
?>

@extends('Admin_temp')

@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')



                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">{{$day->name}} appointments</h5>
                                    <div class="search-box">


                                    </div>
                                </div>

                                <div class="d-selct-view">
                                    <a href="{{route('admin.dates.index' , ['branch_id' => $branch->id])}}" class="btn-style-one">
                                        <i class="fas fa-arrow-left"></i>
                                        Back to the days
                                    </a>

                                    <a href="#" class="btn-style-one" data-bs-toggle="modal"
                                       data-bs-target="#new-item-modal">
                                        <i class="fas fa-plus"></i> Add new time
                                    </a>
                                </div>

                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">from</span></th>
                                        <th><span class="th-head-icon">to</span></th>
                                        <th><span class="th-head-icon">status</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($schedules)

                                        @foreach($schedules as $index => $row)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{ Carbon\Carbon::parse($row->start_time)->format('h:i A') }}</td>
                                                <td class="text-info">{{ Carbon\Carbon::parse($row->end_time)->format('h:i A') }}</td>
                                                <td class="text-info">
                                                    @if($row->is_active == 1)
                                                        <span class="badge bg-success">Active</span>
                                                    @else
                                                        <span class="badge bg-danger">Inactive</span>
                                                    @endif
                                                </td>
                                                <td>
                                                 {{--   <a href="{{route('admin.branch.edit.schedule', ['id' => $row->id, 'branch_id' => $branch->id])}}"
                                                       class="btn btn-sm btn-info">
                                                        <i class="fas fa-edit fa-fw"></i>
                                                    </a>--}}

                                                    <a href="{{route('branch.delete.schedule', ['id' => $row->id, 'branch_id' => $branch->id])}}"
                                                         onclick="return confirm('Are you sure you want to delete this schedule?');"
                                                       class="btn btn-sm btn-danger">
                                                        <i class="fas fa-trash fa-fw"></i>
                                                    </a>


                                                </td>
                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                {{--<div class="justify-content-center d-flex">
                                    {!! $items->appends(Request::except('page'))->render() !!}
                                </div>--}}
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ============== CREATE NEW items MODAL===================== -->
    <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form id="msform" action="{{route('admin.branch.store.schedule' , ['branch_id' => $branch->id])}}" method="post" >
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="account"></li>
                            <li id="personal"></li>
                            <li id="miscla"></li>
                            <li id="confirm"></li>
                        </ul>

                        <fieldset id="firsttab">
                            <div class="modal-header p-0 mb-5">
                                <h5 class="left-border" id="head">Add new time</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <input type="hidden" name="day_id" value="{{$day->id}}">
                            <div class="row">
                                <div class="col-md-6">
                                    <label>From</label>
                                    <input type="time" class="form-control" name="start_time" required>
                                    @if ($errors->has('start_time'))
                                        <p class="text-danger">{{ $errors->first('start_time')}}</p>
                                    @endif
                                </div>
                                <div class="col-md-6">
                                    <label>To</label>
                                    <input type="time" class="form-control" name="end_time" required>
                                    @if ($errors->has('end_time'))
                                        <p class="text-danger">{{ $errors->first('end_time')}}</p>
                                    @endif
                                </div>
                                <div class="col-md-12" hidden>
                                    <label>Status</label>
                                    <select class="form-select" name="is_active" required>
                                        <option value="1" selected>Active</option>
                                        <option value="0">Inactive</option>
                                    </select>
                                    @if ($errors->has('is_active'))
                                        <p class="text-danger">{{ $errors->first('is_active')}}</p>
                                    @endif
                                </div>
                            </div>

                            <div class="btn-question mt-4">
                                <button  type="submit" name="next" class="next btn-style-one" value="" ><i class="fas fa-plus"></i>Add</button >
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif

@endsection
