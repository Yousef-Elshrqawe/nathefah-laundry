<?php
$active_links = ['branches', ''];
?>

@extends('Admin_temp')

@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">

                                <h5 class="left-border">Brunch payments</h5>
                                <h5 class="left-border">{{ $branch->username }}</h5>
                                <h5 class="left-border">{{ $branch->balance }}</h5>


                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal"
                                       data-bs-target="#new-item-modal">
                                        <i class="fas fa-plus"></i> Add payment </a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">paid</span></th>
                                        <th><span class="th-head-icon">remaining</span></th>
                                        <th><span class="th-head-icon">Branch balance before payment</span></th>
                                        <th><span class="th-head-icon">Branch balance after payment</span></th>
                                        <th><span class="th-head-icon">date</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($rows)

                                        @foreach($rows as $index => $row)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$row->paid}}</td>
                                                <td class="text-danger">{{$row->remaining}}</td>
                                                <td class="text-success">{{$row->branch_balance}}</td>
                                                <td class="text-success">{{$row->current_balance}}</td>
                                                <td>{{$row->created_at->format('Y-m-d')}}</td>
                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $rows->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ============== CREATE NEW items MODAL===================== -->

    <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form id="msform" action="{{route('admin.branch.payment.store')}}" method="post">
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="account"></li>
                            <li id="personal"></li>
                            <li id="miscla"></li>
                            <li id="confirm"></li>
                        </ul>

                        <fieldset id="firsttab">
                            <div class="modal-header p-0 mb-5">
                                <h5 class="left-border" id="head">Add payment</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="row">

                                <input type="hidden" name="branch_id" value="{{$branch->id}}">
                                <div class="col-md-6">

                                    <div class="form-box">
                                        <label for="" class="form-label">paid</label>
                                        <input type="number" name="paid" class="form-control"
                                               aria-describedby="emailHelp" placeholder="paid"
                                               required>
                                        <p class="alert-warning">It is not possible to pay an amount more than the current balance</p>

                                        @error('paid')
                                        <p class="alert-danger">{{ $message }}</p>
                                        @enderror
                                    </div>
                                </div>
                                <div class="col-md-6">

                                    <div class="form-box">
                                        <label for="" class="form-label">branch balance</label>
                                        <input type="number"  class="form-control" disabled
                                               value="{{$branch->balance}}"
                                               aria-describedby="emailHelp" placeholder="paid">
                                    </div>
                                </div>

                            </div>

                            <div class="btn-question mt-4">
                                <button type="submit" name="next" class="next btn-style-one" value=""><i
                                        class="fas fa-plus"></i> Add payment </button>
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif

@endsection
