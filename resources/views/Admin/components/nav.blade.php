<div class="row">
    <div class="col-xl-12 mb-4">
        <div class="page-topbar">
            <div class="overview-box">
                <div class="row align-items-center flex-column-reverse flex-xl-row">
                    <div class="col-xl-4 col-md-12">
                        <div class="top-bar-title">
                            <h5>Welcome {{Auth::guard('Admin')->user()->name}}! </h5>
                            {{-- <p class="cl-gray">Here is what happened in the branches today</p> --}}
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-12">
                        @if ($active_links[0] == 'wallet')
                            <div class="search-box">
                                <form action="{{route('Admin.user.wallet.search')}}" method="post">
                                    @csrf
                                    <input type="text" name="search" id="" class="form-control" placeholder="search user by name or phone number">
                                    <i class="fal fa-search" style="cursor: pointer" onclick='this.parentNode.submit(); return false;'></i>
                                </form>
                            </div>
                        @endif
                    </div>

                    <div class="col-xl-4 col-md-12">
                        <div class="notify-box">
                            <div class="notify-bell">
                                {{-- <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p> --}}
                            </div>
                            <div>
                                {{-- <p>Sun 24 July 4:35 PM</p> --}}
                                <p>{{now()->format('l  m-d')}}</p>

                            </div>

                            <div>
                                <a href="{{route('Admin.logout')}}">log out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
