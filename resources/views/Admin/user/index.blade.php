<?php
$active_links = ['users', ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.users')</h5>
                                    <div class="search-box">
                                    </div>
                                </div>

                                <div class="d-selct-view">
                                    <a href="#" class="btn-style-one" data-bs-toggle="modal"
                                       data-bs-target="#new-item-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.add_users') </a>
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</th>
                                        <th><span class="th-head-icon">@lang('admin.name')</th>
                                        <th><span class="th-head-icon">@lang('admin.email')</th>
                                        <th><span class="th-head-icon">@lang('admin.phone')</th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($rows)

                                        @foreach($rows as $index => $row)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$row->name}}</td>
                                                <td class="text-info">{{$row->email}}</td>
                                                <td class="text-info">{{$row->country_code}} {{$row->phone}}</td>
                                                <td>
                                                    <a href="{{route('admin.users.edit', ['id'=> $row -> id])}}"
                                                       class="cl-light">@lang('admin.edit')</a>


                                                        <a href="{{route('admin.users.delete', ['id'=> $row -> id])}}" class="cl-danger"
                                                        style="color: red;" onclick="return confirm('Are you sure you want to delete this User?' )"
                                                        >@lang('admin.delete')</a>

                                                </td>

                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $rows->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- ============== CREATE NEW items MODAL===================== -->
    <div class="modal fade" id="new-item-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">

                <div class="modal-body">
                    <form id="msform" action="{{route('admin.users.store')}}" method="post">
                        @csrf
                        <ul id="progressbar">
                            <li class="active" id="account"></li>
                            <li id="personal"></li>
                            <li id="miscla"></li>
                            <li id="confirm"></li>
                        </ul>

                        <fieldset id="firsttab">
                            <div class="modal-header p-0 mb-5">
                                <h5 class="left-border" id="head">@lang('admin.add_users')</h5>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                        aria-label="Close"></button>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">{{ __('admin.name') }}</label>
                                        <input type="text" name="name" class="form-control"
                                               value="{{ old('name') }}" aria-describedby="emailHelp"
                                               placeholder="{{ __('admin.name') }}" required>
                                        @if ($errors->has('name'))
                                            <p class="alert-danger">{{ $errors->first('name')}}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.country_code')</label>
                                        <input type="text" name="country_code" class="form-control" aria-describedby="emailHelp"
                                               value="+966"
                                               disabled
                                               placeholder="@lang('admin.country_code')" required>
                                        @if ($errors->has('country_code'))
                                            <p class="alert-danger">{{ $errors->first('country_code')}}</p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.phone')</label>
                                        <input type="number" name="phone" class="form-control"
                                               value="{{ old('phone') }}" aria-describedby="emailHelp"
                                               placeholder="5xxxxxxxx" required>

                                        @if ($errors->has('phone'))
                                            <p class="alert-danger">{{ $errors->first('phone')}}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.email')</label>
                                        <input type="email" name="email" class="form-control"
                                               value="{{ old('email') }}" aria-describedby="emailHelp"
                                               placeholder="@lang('admin.email')" required>
                                        @if ($errors->has('email'))
                                            <p class="alert-danger">{{ $errors->first('email')}}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.password')</label>
                                        <input type="password" name="password" class="form-control"
                                               value="{{ old('password') }}" aria-describedby="emailHelp"
                                               placeholder="@lang('admin.password')" required>
                                        @if ($errors->has('password'))
                                            <p class="alert-danger">{{ $errors->first('password')}}</p>
                                        @endif
                                    </div>
                                </div>


                                <div class="col-md-6">
                                    <div class="form-box">
                                        <label for="" class="form-label">@lang('admin.password_confirmation')</label>
                                        <input type="password" name="password_confirmation" class="form-control"
                                               value="{{ old('password_confirmation') }}" aria-describedby="emailHelp"
                                               placeholder="@lang('admin.password_confirmation')" required>
                                        @if ($errors->has('password_confirmation'))
                                            <p class="alert-danger">{{ $errors->first('password_confirmation')}}</p>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="btn-question mt-4">
                                <button type="submit" name="next" class="next btn-style-one" value=""><i
                                        class="fas fa-plus"></i>@lang('admin.add_users')</button>
                                <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif

@endsection
