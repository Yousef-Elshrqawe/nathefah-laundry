<?php
$active_links = ['application_rate', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.application_rate')</h5>
                                    <div class="search-box">

                                    </div>


                                    {{--  <div class="table-box-select">
                                        <div class="d-select-box">
                                            <select class="form-select" aria-label="Default select example">
                                                <option selected>Sort By : Latest</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                        <div class="d-select-box">
                                            <select class="form-select" aria-label="Default select example">
                                                <option selected>Subscription  : All</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                    </div>  --}}
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-xl-12">

                                    <div class="card-body">
                                        <form class="form" action="{{route('admin.application-rate.update' )}}"
                                              method="post">
                                            @csrf

                                            <div class="form-body">

                                                {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                                <div class="row">

                                                    <label>Application rate of laundry (%)</label>
                                                    <input class="form-control" name="rate" value="{{$row->rate}}"
                                                           type="text" required>
                                                    @if ($errors->has('rate'))
                                                        <p class="text-danger">{{ $errors->first('rate')}}</p>
                                                    @endif


                                                </div>


                                            </div>

                                            <br>
                                            <div class="form-actions">
                                                {{--  <button type="button" class="btn btn-warning mr-1"
                                                    onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>  --}}
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Update
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection
