<?php
$active_links = ['wallet', ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')
                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.walletes')</h5>
                                </div>
                                <div class="d-selct-view">
                                    {{-- <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-category-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.delivery_fees')
                                    </a> --}}
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">@lang('admin.id')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.phone')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.wallet')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.free_wallet')</span></th>

                                        <th><span class="th-head-icon">@lang('admin.total')</span></th>


                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>

                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach ($users as $key=>$user)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td class="text-info">{{$user->id}}</td>
                                            <td class="text-info">{{$user->name}}</td>
                                            <td class="text-info">{{$user->phone}}</td>

                                            @if (isset($user->userwallet?->amount))
                                                <td>{{round($user->userwallet?->amount,1)}}</td>
                                            @else
                                                <td>0</td>
                                            @endif
                                            @if (isset($user->userfreewallet?->first()->amount))
                                                <td>{{round($user->userfreewallet?->first()->amount,1)}} </td>
                                            @else
                                                <td>0</td>
                                            @endif

                                            <td>
                                            @if (isset($user->totalwallet))
                                                {{round($user->totalwallet,1)}}
                                            @else
                                                <td>0</td>
                                                @endif


                                                </td>

                                                <td style="display: -webkit-inline-box;">


                                                    <div class="form-check form-switch">
                                                    </div>
                                                    <a href="#" class="add_free_wallet" data-id="{{$user->id}}"
                                                       data-bs-toggle="modal" data-bs-target="#add-freewallet-modal">
                                                        <i class="fas fa-plus"></i>
                                                    </a>


                                                </td>

                                        </tr>
                                    @endforeach


                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
                                    {!! $users->appends(Request::except('page'))->render() !!}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="add-freewallet-modal" tabindex="-1" aria-labelledby="edit-tax-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="msform" action="{{route('Admin.user.addfreewallet')}}" method="post">
                        @csrf
                        <input type="hidden" id="userid" name="userid" value="">
                        <div class="row">
                            <div class="col-5">
                                <div class="form-group">
                                    <label>amount</label>
                                    <input class="form-control" type="number" value="" name="amount" required placeholder="amount" min="0" >
                                    @error('amount')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>

                            </div>
                            <div class="col-4">
                                <div class="form-group">
                                    <label>end date</label>
                                    <input class="form-control" type="date" value="" name="end_date" required placeholder="end date">
                                    @error('end_date')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class=" mt-4">
                            <button type="submit" class="next btn-style-one" value=""><i class="fas fa-plus"></i>Add
                                free wallet
                            </button>
                            <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection



@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function () {
                $('#add-freewallet-modal').modal('show');
            });
        </script>
    @endif


    <script>
        var SITEURL = "{{ url('/laundry-admin') }}";
        $('.add_free_wallet').click(function () {
            var id = $(this).data("id");
            $('#userid').val(id);
        });
    </script>

@endsection
