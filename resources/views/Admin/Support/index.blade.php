
<?php
$active_links = ['setting' , ''];
?>

@extends('Admin_temp')
@section('content')
<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            @include('Admin.components.nav')

            <div class="row">
                <div class="col-xl-12 mb-4">
                    <h4 class="title-inner">Support</h4>
                </div>


                <div class="form-group col-3">
                     <label>phone</label>
                     <div class="d-flex justify-content-center">
                        <input  class="form-control" type="text" name="number" value="{{$setting->country_code}}{{$setting->support_phone}}">




                        <a class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-phone-modal">edit</a>
                    </div>

                </div>



                    {{-- display supports --}}

                    <div class="row">

                        @foreach ($Supports as $support)
                        <div class="col-md-3 mt-2">
                            <div class="card" style="width: 18rem; height: 250px; overflow: auto;" >
                                <div class="card-body">
                                  <h5 class="card-title">{{$support->full_name}}</h5>
                                  <h6 class="card-subtitle mb-2 text-muted">{{$support->email}}</h6>
                                   <span class="text-primary">{{$support->country_code}} {{$support->phone}}  </span>

                                  <p class="card-text" style="">{{$support->problem}}</p>

                                </div>
                              </div>
                        </div>

                        @endforeach


                    </div>

                    <div class="justify-content-center d-flex">
                        {!! $Supports->appends(Request::except('page'))->render() !!}
                    </div>
                 {{-- display supports --}}


                 <div class="modal fade" id="edit-phone-modal" tabindex="-1" aria-labelledby="edit-phone-modal" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-body">
                                <form id="msform" action="{{route('Admin.Support.updatephone')}}" method="post">
                                    @csrf

                                   <div class="row">
                                       <div class="col-5">

                                        <div class="form-group">
                                            <label>country code</label>
                                            <input class="form-control" type="text" value="{{$setting->country_code}}" name="country_code">
                                            @error('amount')
                                                <p class="alert-danger">{{ $message }}</p>
                                            @enderror
                                        </div>

                                            <div class="form-group">
                                                <label>phone</label>
                                                <input class="form-control" type="text" value="{{$setting->support_phone}}" name="phone">
                                                @error('amount')
                                                    <p class="alert-danger">{{ $message }}</p>
                                                @enderror
                                            </div>

                                       </div>

                                   </div>
                                    <div class=" mt-4">
                                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i>edit</button >
                                        <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>



            </div>
        </div>
    </div>
</div>


@endsection

@section('scripts')
@if (Session::get('errors')!=null)
<script>
        $(document).ready(function() {
            $('#add-user-modal').modal('show');
        });
</script>
@endif
@endsection

