<!-- ============== CREATE NEW CATEGORY MODAL===================== -->
<div class="modal fade" id="edit-delivery_fees-modal" tabindex="-1" aria-labelledby="new-service-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form id="" action="{{route('Admin.deliveryfees.update')}}" method="post" >
                    @csrf
                    @method('post')
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border" id="head">@lang('admin.delivery_fees')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label id="type"></label>
                            <input  class="form-control" type="text" name="price" id="price">
                        </div>

                    </div>

                    <div class="btn-question mt-4">
                        <button  type="submit" class="btn-style-one">
                            <i class="fas fa-plus"></i>
                            @lang('admin.edit')
                        </button >
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
