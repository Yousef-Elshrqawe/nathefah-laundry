<?php
$active_links = ['deliveryfees' , ''];
?>

@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.delivery_fees')</h5>
                                </div>
                                <div class="d-selct-view">
                                    {{-- <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-category-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.delivery_fees')
                                    </a> --}}
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.price')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($delivery_fees)

                                        @foreach($delivery_fees as $index => $delivery_fee)
                                            <tr>
                                                <td>{{$index + 1}}</td>
                                                <td class="text-info">{{$delivery_fee->type}}</td>
                                                <td class="text-info">{{$delivery_fee->price}}</td>
                                                <td style="display: -webkit-inline-box;">

                                                    <button  class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-delivery_fees-modal"
                                                             data-id="{{$delivery_fee->id}}" data-price="{{$delivery_fee->price}}"
                                                             data-type="{{$delivery_fee->type}}">
                                                        @lang('admin.edit')
                                                    </button>



                                                </td>

                                            </tr>
                                        @endforeach
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
{{--                                    {!! $categories->appends(Request::except('page'))->render() !!}--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




@include('Admin.deliveryfees.edit_delivery_fees')


@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $('#edit-delivery_fees-modal').on('show.bs.modal', function(event) {
            let button = $(event.relatedTarget)
            let id     = button.data('id')
            let type = button.data('type')
            let price = button.data('price')

            let modal = $(this)
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #type').html(type);
            modal.find('.modal-body #price').val(price);
        })
    </script>

@endsection
