<?php
$active_links = ['orders', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            {{--                                            <p class="cl-gray">Here is what happened in the branches today</p>--}}
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">

                            </div>
                            <div class="row">
                                <div class="col-xl-12">

                                    <div class="row" style="margin-top: 20px;">
                                        <div class="col-xl-12">
                                            <a href="#" class="btn btn-primary"  data-bs-toggle="modal" data-bs-target="#new-order-modal"
                                               style="background-color: #FFB64D; border-color: #FFB64D; color: white; width: 100%;"
                                               data-toggle="modal" data-target="#new-item-modal">
                                                <i class="fal fa-plus"></i> Create New Order ( {{$branch->username}} )
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @include('components.orders.create',[$services,$porttype ,$isadmin , $branchId ,$crate_admin])

        </div>

        @endsection

        @section('scripts')
            <script src="js/menu.js"></script>
            <script type="text/javascript">
                $("#cssmenu").menumaker({
                    title: "",
                    format: "multitoggle"
                });
            </script>
            <script>
                $(document).ready(function(){
                    $("#last-btn").click(function(){
                        $("#progressbar").addClass("hide");
                    });
                    var current_fs, next_fs, previous_fs; //fieldsets
                    var opacity;
                    var current = 1;
                    var steps = $("fieldset").length;
                    setProgressBar(current);
                    $(".next").click(function(){
                        //current_fs = $(this).parent();
                        // next_fs = $(this).parent().next();
                        current_fs = $(this).closest("fieldset");
                        next_fs = $(this).closest("fieldset").next();
                        //alert(next_fs);
                        //Add Class Active
                        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
                        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

                        //show the next <fieldset></fieldset>
                        next_fs.show();
                        //hide the current fieldset with style
                        current_fs.animate({opacity: 0}, {
                            step: function(now) {
                                // for making fielset appear animation
                                opacity = 1 - now;

                                current_fs.css({
                                    'display': 'none',
                                    'position': 'relative'
                                });
                                next_fs.css({'opacity': opacity});
                            },
                            duration: 500
                        });
                        setProgressBar(++current);
                    });
                    $(".previous").click(function(){
                        current_fs = $(this).closest("fieldset");
                        previous_fs = $(this).closest("fieldset").prev();
                        //Remove class active
                        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
                        //show the previous fieldset
                        previous_fs.show();
                        //hide the current fieldset with style
                        current_fs.animate({opacity: 0}, {
                            step: function(now) {
                                // for making fielset appear animation
                                opacity = 1 - now;

                                current_fs.css({
                                    'display': 'none',
                                    'position': 'relative'
                                });
                                previous_fs.css({'opacity': opacity});
                            },
                            duration: 500
                        });
                        setProgressBar(--current);
                    });
                    function setProgressBar(curStep){
                        var percent = parseFloat(100 / steps) * curStep;
                        percent = percent.toFixed();
                        $(".progress-bar")
                            .css("width",percent+"%")
                    }
                    $(".submit").click(function(){
                        return false;
                    })
                });
            </script>
            <script>
                $(document).ready(function() {
                    $('.minus').click(function () {
                        var $input = $(this).parent().find('input');
                        var count = parseInt($input.val()) - 1;
                        count = count < 0 ? 1 : count;
                        $input.val(count);
                        $input.change();
                        return false;
                    });
                    $('.plus').click(function () {
                        var $input = $(this).parent().find('input');
                        $input.val(parseInt($input.val()) + 1);
                        $input.change();
                        return false;
                    });
                });
            </script>
            {{-- show order --}}
            <script>
                var SITEURL = "{{ url('/branch-admin') }}";
                $('.view_button').click(function(){
                    //alert('vdfzx');
                    var id=$(this).attr("data-id");
                    //alert(id);
                    $.ajax({
                        url:SITEURL+"/order/view/"+id,
                        type:"GET", //send it through get method
                        success: function (response) {
                            $('#order_body').html(response);
                        },
                        error: function(response) {
                            console.log('fdfd');
                        }
                    });
                });
            </script>


            {{-- change option value  --}}
            <script>
                var reportdate=$('#reportdate').find(":selected").val();
                var selectdate=$('#selectdate').find(":selected").val();
                $('#selectdate').change(function(){
                    var selectdate=$(this).find(":selected").val();
                    window.location.href=url+'/branch-admin/orders/'+type+'/'+selectdate+'/'+reportdate;
                });
                $('#reportdate').change(function(){
                    var reportdate=$(this).find(":selected").val();
                    window.location.href=url+'/branch-admin/orders/'+type+'/'+selectdate+'/'+reportdate;
                })
            </script>
            {{-- Qr code --}}
            <script>
                $('.scan_button').click(function(){
                    var id=$(this).attr("data-id");
                    $.ajax({
                        url:url+"/branch-admin/get/qr_code/"+id,
                        type:"GET", //send it through get method
                        success: function (response) {
                            $('#qr_code').html(response);
                        },
                        error: function(response) {
                            console.log('fdfd');
                        }
                    });
                });
            </script>
    {{-- google maps --}}
    @include('components.Scripts.map')
    {{-- create order --}}
    @include('components.Scripts.createorder',[$porttype])
    {{-- End create order --}}
@endsection
