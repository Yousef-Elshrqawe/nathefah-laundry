<?php
$active_links = ['orders' , ''];
?>
@extends('Admin_temp')
@section('content')

<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            @include('branch.components.nav')
            <div class="row">
                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">All Orders</h5>
                            </div>
                            <div class="table-box-select">
                                <div class="d-select-box">
                                    <form action="{{route('admin.allorder')}}" method="GET">
                                    <select class="form-select" name="branch_id" id="branchesSelect" onchange="this.form.submit();" aria-label="Default select example">
                                        <option selected>{{$branchname}}</option>

                                        {{-- <option selected>Choose Branch</option> --}}
                                    </select>
                                    </form>
                                </div>


                                    <div class="d-select-box">
                                        <select class="form-select" id="laundrySelect" onchange="getBranches()" aria-label="Default select example">
                                            <option selected>{{$laundryname}}</option>

                                            @foreach($laundries as $laundry)
                                                <option value="{{$laundry->id}}">{{$laundry->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    {{-- <form id="latest_order_form" method="get" action="{{route('branch.latest.orders')}}">
                                     @csrf
                                    <select id="select_last_order" name="days" class="form-select" aria-label="Default select example">
                                        <option value="7" selected>Period last Week</option>
                                        <option value="7">One</option>
                                        <option value="14">Two</option>
                                        <option value="21">Three</option>
                                    </select>
                                   </form> --}}
                                {{-- <div class="d-selct-view">
                                    <a href="#" class="cl-light">View All</a>
                                </div> --}}
                            </div>
                        </div>
                        <div class="table_responsive_maas v2">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th><span class="th-head-icon">Order number</th>
                                        <th><span class="th-head-icon">Customer name </th>
                                        <th><span class="th-head-icon">Delivery Type </th>
                                        <th><span class="th-head-icon">Arrival Date </th>
                                        <th><span class="th-head-icon">Order Status </th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach ($orders as $order)
                                    <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->customer_name}}</td>
                                            <td>  @if($order->deliverytype!=null)
                                                {{$order->deliverytype->name}}
                                                @endif</td>
                                            <td>{{$order->drop_date}}</td>
                                            <td>
                                                {{-- this meen that laundry accept order from customer andd assign order for driver and driver accept order from laundry --}}
                                                @if($order->progress!=null)
                                                   @if ($order->progress=='finished'&&$order->driver_id!=null )
                                                     {{$order->progress}}/waiting driver
                                                    @elseif($order->progress=='finished'&&$order->driver_id==null )
                                                     {{$order->progress}}/assign driver
                                                        @elseif($order->progress=='indelivery'&&$order->OrderDriveryStatus->first()->order_status=='drop_of_home')
                                                        {{$order->progress}}/drop of home
                                                        @elseif($order->progress=='indelivery'&&$order->OrderDriveryStatus->first()->order_status=='drop_of_laundry')
                                                        {{$order->progress}}/drop of laundry
                                                    @elseif($order->progress=='completed')
                                                    {{$order->progress}}
                                                   @endif
                                                @else
                                                {{--in this case $order->progress== null and delivery_status==inprogress that mean that driver accept order from laundry and go to recive order from customer  --}}
                                                   @if ($order->delivery_status=='inprogress')
                                                          pick up  home
                                                    @else
                                                    {{-- in this case $order->progress== null and delivery_status==null --}}
                                                        @if ($order->confirmation=='pending')
                                                           new
                                                        @endif
                                                        @if ($order->confirmation=='accepted'&& $order->driver_id==null)
                                                          un assigned
                                                        @endif
                                                        @if ($order->confirmation=='accepted'&& $order->driver_id!=null)
                                                          waiting for driver accept
                                                        @endif
                                                   @endif
                                                @endif
                                            </td>
                                            <td><a href="#"  data-id="{{$order->id}}" data-bs-toggle="modal" data-bs-target="#order-details-modal"   class="cl-light view_button">View Order</a></td>
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            <div class="justify-content-center d-flex">
                                {!! $orders->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <!--=================ORDER DETAILS MODAL================= -->
<div class="modal fade" id="order-details-modal" tabindex="-1" aria-labelledby="order-details-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h5>Order Details</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body" id="order_body">

        </div>
      </div>
    </div>
</div>

</div>
@endsection
@section('scripts')
<script>
$('#select_last_order').change(function(){
$('#latest_order_form').submit();
});
</script>


{{-- show order --}}
<script>
    var SITEURL = "{{ url('/Super-admin') }}";
    $('.view_button').click(function(){
        //alert('vdfzx');
        var id=$(this).attr("data-id");
        //alert(id);
        $.ajax({
            url:SITEURL+"/order/view/"+id,
            type:"GET", //send it through get method
            success: function (response) {
                $('#order_body').html(response);
            },
            error: function(response) {
            }
        });
    });
    </script>


@include('Admin.orders.scripts')

@endsection
