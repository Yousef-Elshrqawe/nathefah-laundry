<?php
$active_links = ['orders', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')


                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">Select Branch</h5>
                                    <div class="search-box">

                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-xl-12">

                                    <div class="card-body">
                                        <form class="form" action="{{route('admin.admin.store.order' )}}"
                                              method="get">


                                            <div class="form-body">

                                                {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                                <div class="row">

                                                    <div class="col-md-6">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">@lang('admin.laundry')</label>
                                                            <select name="laundry_id" class="form-control" required  id="laundry_id">
                                                                <option selected value="">Choose Laundry</option>
                                                                @foreach($laundries as $laundry)
                                                                    <option value="{{$laundry->id}}">{{$laundry->name}}</option>
                                                                @endforeach
                                                            </select>
                                                            @if ($errors->has('laundry_id'))
                                                                <p class="alert-danger">{{ $errors->first('laundry_id')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-6">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">@lang('admin.branch')</label>
                                                            <select name="branch_id" class="form-control" required  id="branch_id">
                                                                <option selected value="">@lang('admin.select_branch')</option>
                                                                {{--    @foreach($branches as $branch)
                                                                        <option value="{{$branch->id}}">{{$branch->username}}</option>
                                                                    @endforeach--}}
                                                            </select>
                                                            @if ($errors->has('branch_id'))
                                                                <p class="alert-danger">{{ $errors->first('branch_id')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>


                                                </div>


                                            </div>

                                            <br>
                                            <div class="form-actions">
                                                {{--  <button type="button" class="btn btn-warning mr-1"
                                                    onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>  --}}
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> next
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

        @section('scripts')

            @if (Session::get('errors')!=null)
                <script>
                    $(document).ready(function () {
                        $('#new-item-modal').modal('show');
                    });
                </script>
            @endif


            <script>
                $(document).ready(function () {
                    $('#laundry_id').on('change', function () {
                        var laundry_id = $(this).val();
                        console.log(laundry_id);
                        if (laundry_id) {
                            $.ajax({
                                url: "{{ URL::to('Super-admin/getBranchesAjax') }}/" + laundry_id,
                                type: 'GET',
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                    $('#branch_id').empty();
                                    $('#branch_id').append('<option selected value="">Choose branch</option>');
                                    $.each(data, function (index, subcategory) {
                                        $('#branch_id').append('<option value="' + subcategory.id + '">' + subcategory.username + '</option>');
                                    });
                                }
                            });
                        } else {
                            $('#branch_id').empty();
                        }
                    });
                });
            </script>



@endsection
