<?php
$active_links = ['orders' , ''];
?>
@extends('Admin_temp')
@section('content')

<div class="main-content">
    <div class="page-content pb-3">
        <div class="container-fluid">
            @include('Admin.components.nav')

            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="inner-title-box">
                        <div>
                            <h4 class="title-inner"> Orders & Reports</h4>
                        </div>
                        <div class="btn-flex">
                            <a href="{{route('admin.admin.create.order')}}" class="btn-style-two border"><i class="fas fa-plus"></i> Create Order</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 mb-4">
                    <div class="overview-box">
                        <div class="table-box">
                            <div class="tab-flex">
                                <div>
                                    <h5 class="left-border">Orders</h5>
                                </div>
                                <div class="tab-box">
                                    @php
                                        $branchid = isset($_GET['branch_id']) ? 'branch_id='.$_GET['branch_id'] : "";
                                        $landuryid = isset($_GET['laundry_id']) ? 'laundry_id='.$_GET['laundry_id'] : "";
                                    @endphp
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link  @if($type=='indelivery') active @endif"  href="{{route('admin.orders',['indelivery',$orderdate,$datereview,$branchid,$landuryid])}}">In Delivery ( {{$indelivery}} )</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='inprogress') active @endif"   href="{{route('admin.orders',['inprogress',$orderdate,$datereview,$branchid,$landuryid])}}">in Progress ( {{$inprogresscount}} )</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='completed') active @endif"  href="{{route('admin.orders',['completed',$orderdate,$datereview,$branchid,$landuryid])}}">Completed ( {{$completed}} )</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='unassigned') active @endif"      href="{{route('admin.orders',['unassigned',$orderdate,$datereview,$branchid,$landuryid])}}">unassigned ( {{$unassigned}} )</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='new') active @endif"      href="{{route('admin.orders',['new',$orderdate,$datereview,$branchid,$landuryid])}}">new ( {{$neworder}} )</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='one_way_delivery') active @endif"      href="{{route('admin.orders',['one_way_delivery',$orderdate,$datereview,$branchid,$landuryid])}}">one way delivery  ( {{$onewayorders}} )</a>
                                        </li>


                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='all') active @endif"      href="{{route('admin.orders',['all',$orderdate,$datereview,$branchid,$landuryid])}}">All ( {{$all}} )</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>

                        @php
                            $cur_route = Route::current()->getName();

                            $currentUrl = url()->current();
                            $urlParts   = explode('/',$currentUrl);
                        @endphp

                            <form action="{{route('admin.orders',[$urlParts[5],$urlParts[6],$urlParts[7]])}}" method="GET">

                                <div class="table-box-select">
                                    <div class="d-select-box">
                                        <select id="selectdate" class="form-select" aria-label="Default select example">
                                            <option selected    @if($orderdate=='all') selected @endif value="all">Sort by date</option>
                                            <option selected    @if($orderdate=='all') selected @endif value="all">All</option>
                                            <option value="7"   @if($orderdate==7) selected @endif>One week</option>
                                            <option value="14"  @if($orderdate==14) selected @endif>Two week</option>
                                            <option value="21"  @if($orderdate==21) selected @endif>Three week</option>
                                        </select>
                                    </div>

                                    <div class="d-select-box">
                                        <select class="form-select" name="branch_id" id="branchesSelect" onchange="this.form.submit();" aria-label="Default select example">
                                            <option {{ request('branch_id') == '' ? 'selected' : '' }} value="">All</option>
                                             {{-- <option selected>{{$branchname}}</option> --}}
                                              @foreach ($branchs as $branch)
                                              <option value="{{$branch->id}}" @if(request('branch_id') == $branch->id) selected @endif >{{$branch->username}}</option>
                                              @endforeach
                                            {{-- <option selected>Choose Branch</option> --}}
                                        </select>
                                    </div>

                                    <div class="d-select-box">
                                        <select class="form-select" name="laundry_id" id="laundrySelect" onchange="getBranches()" aria-label="Default select example">
                                            <option {{ request('laundry_id') == '' ? 'selected' : ''}} value="">All</option>

                                            @foreach($laundries as $laundry)
                                                <option {{ request('laundry_id') == $laundry->id ? 'selected' : ''}} value="{{$laundry->id}}">{{$laundry->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                    <div class="d-select-box">
                                        <select class="form-select" name="user_id" id="user_id" onchange="this.form.submit();" aria-label="Default select example">
                                            <option {{ request('user_id') == '' ? 'selected' : ''}} value="">All</option>
                                            @foreach($users as $user)
                                                <option {{ request('user_id') == $user->id ? 'selected' : ''}} value="{{$user->id}}">{{$user->name}} -- {{$user->phone}}</option>
                                            @endforeach
                                        </select>
                                    </div>


                                </div>

                            </form>

{{--                                <div class="d-selct-view">--}}
{{--                                    <a href="#" class="cl-light" type="button"  data-bs-toggle="modal" data-bs-target="#viewall-modal">View All</a>--}}
{{--                                </div>--}}

                        </div>
                        @include('components.orders.ordertable',['orders'=>$orders])
                    </div>
                </div>
                <div class="justify-content-center d-flex">
                    {!! $orders->appends(Request::except('page'))->render() !!}
                </div>
                {{-- Start Reports --}}
                <div class="col-lg-12">
                    <div class="row">

                    <div class="col-lg-8 col-md-12">
                        <div class="whitebox-panel">
                            <div class="row">
                  <div class="col-xl-12">
                    <div class="table-box">
                      <div>
                          <h5 class="left-border">Reports</h5>
                      </div>
                      <div class="table-box-select">
                          <div class="d-select-box">
                              <select id="reportdate" class="form-select" aria-label="Default select example">
                                  <option selected value="all" @if($datereview=='all') selected @endif>Sort By : Latest All</option>
                                  <option selected value="all" @if($datereview=='all') selected @endif>All</option>
                                  <option value="7"  @if($datereview=='7') selected @endif>One Week</option>
                                  <option value="14" @if($datereview=='14') selected @endif>Two Week</option>
                                  <option value="21" @if($datereview=='21') selected @endif>Three Week</option>
                              </select>
                          </div>
                      </div>
                    </div>
                    @foreach ($reports as $report)
                        <div class="review-sec">
                            <div class="review-profile-details">
                                <img src="images/avatar.png" alt="">
                                <div class="review-content">
                                <div class="report-name">
                                    <ul>
                                    <li>{{$report->user->name}}</li>
                                    <li class="border-round">{{$report->created_at}}</li>
                                    </ul>
                                </div>
                                <h6>{{$report->desc}}</h6>
                                <div class="selling-title-box">
                                    <ul>
                                        <li><i class="fas fa-star"></i></li>
                                        <li><a href="#">{{$report->rate}}</a></li>
                                    </ul>
                                </div>
                                </div>
                            </div>
                            <div class="review-point">
                            <a href="#" data-id="{{$report->order_id}}" class="blue-btn-link view_button" data-bs-toggle="modal" data-bs-target="#order-details-modal">View </a>
                            </div>
                        </div>
                    @endforeach
                    <div class="justify-content-center d-flex">
                        {!! $reports->appends(Request::except('page'))->render() !!}
                    </div>
                </div>
                </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-12">
                        <div class="whitebox-panel">
                          <div class="whitebox-panel-header">
                            <h5 class="left-border">Latest Orders</h5>





                            <a href="{{route('admin.allorder','branch_id='.is_array($branchid)?'':$branchid)}}" class="viewall" type="button" >View All </a>
                          </div>
                          <div class="whitebox-panel-body">
                            <div class="table_responsive_maas v2 pt-0">
                                @include('components.orders.latestordertable',['latestorders'=>$latestorders])
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>




                <!--=================ORDER DETAILS MODAL================= -->
                <div class="modal fade" id="order-details-modal" tabindex="-1" aria-labelledby="order-details-modal" aria-hidden="true">
                    <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5>Order Details</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                        </div>

                        <div class="modal-body" id="order_body">

                        </div>
                    </div>
                    </div>
                </div>


                </div>
            </div>
        </div>
    </div>



@endsection
@section('scripts')
<script src="js/menu.js"></script>
<script type="text/javascript">
    $("#cssmenu").menumaker({
        title: "",
        format: "multitoggle"
    });
</script>
<script>
$(document).ready(function(){
$("#last-btn").click(function(){
    $("#progressbar").addClass("hide");
});
var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $("fieldset").length;
setProgressBar(current);
$(".next").click(function(){
     //current_fs = $(this).parent();
    // next_fs = $(this).parent().next();
    current_fs = $(this).closest("fieldset");
    next_fs = $(this).closest("fieldset").next();
    //alert(next_fs);
    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the next <fieldset></fieldset>
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(++current);
});
$(".previous").click(function(){
    current_fs = $(this).closest("fieldset");
    previous_fs = $(this).closest("fieldset").prev();
    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(--current);
});
function setProgressBar(curStep){
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
}
$(".submit").click(function(){
    return false;
})
});
</script>
<script>
        $(document).ready(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 0 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>
{{-- show order --}}
<script>
var SITEURL = "{{ url('/Super-admin') }}";
$('.view_button').click(function(){
    //alert('vdfzx');
    var id=$(this).attr("data-id");
    //alert(id);
    $.ajax({
        url:SITEURL+"/order/view/"+id,
        type:"GET", //send it through get method
        success: function (response) {
            $('#order_body').html(response);
        },
        error: function(response) {
        }
    });
});
</script>
{{-- create order --}}
<script>
var url={!!json_encode(url('/'))!!};
var submitorderurl={!!json_encode(route('admin.submit.order'))!!};
var type={!!json_encode($type)!!};
$('#submit').click(function(){
    var input = document.getElementsByName('itempriceid[]');
    var countinput = document.getElementsByName('count[]');
    var customer_name = $('#customer_name').val();
    var customer_phone = $('#customer_phone').val();
    var location = $('#location').val();
    const itempriceid=[];
    const count=[];
    for (var i = 0; i < input.length; i++) {
        itempriceid.push(input[i].value);
    }
    for (var i = 0; i < countinput.length; i++) {
        count.push(countinput[i].value);
    }
     $.ajax({
          type:'POST',
          url:submitorderurl,
          async: true,
          data: {
                   _token: "{{ csrf_token() }}",
                    itempriceid:itempriceid,
                     count:count,
                     customer_name:customer_name,
                     customer_phone:customer_phone,
                     location:location
              },
          success:function(response) {
               $('#order_detailes').html(response);
          },
          error:function(response){
          },
        });

});
</script>
{{-- change option value  --}}
<script>
     var reportdate=$('#reportdate').find(":selected").val();
     var selectdate=$('#selectdate').find(":selected").val();
     var branch_id= {!!request('branch_id')!!}
     var landary_id= {{request('laundry_id')}}
    $('#selectdate').change(function(){
        var selectdate=$(this).find(":selected").val();
        window.location.href=url+'/Super-admin/orders/'+type+'/'+selectdate+'/'+reportdate+'?branch_id='+branch_id +'&laundry_id='+landary_id;
    });
    $('#reportdate').change(function(){
        var reportdate=$(this).find(":selected").val();
        window.location.href=url+'/Super-admin/orders/'+type+'/'+selectdate+'/'+reportdate+'?branch_id='+branch_id+'&laundry_id='+landary_id;
    })
</script>
{{-- Qr code --}}
<script>
$('.scan_button').click(function(){
    var id=$(this).attr("data-id");
    $.ajax({
        url:url+"/Super-admin/get/qr_code/"+id,
        type:"GET", //send it through get method
        success: function (response) {
            $('#qr_code').html(response);
        },
        error: function(response) {
            console.log('fdfd');
        }
        });
    });
</script>
<script>
	$('.read').click(function () {
		console.log($('.read').text());
		if ($(this).text() == "More Details ") {
			$(this).html('Less Details <i class="fas fa-chevron-up"></i>');
			$(this).next($('.extradetails')).addClass('active');


		} else {
			$(this).html('More Details <i class="fas fa-chevron-right"></i>');
			$(this).next($('.extradetails')).removeClass('active');
		}
	});


</script>
@include('Admin.orders.scripts')
@endsection
