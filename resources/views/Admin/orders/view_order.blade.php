<div class="row">
    <div class="col-xl-4 col-md-4">
        <div class="or-details-box">
            <ul>
                <li><i class="fal fa-user-alt"></i>Order ID :<span class="cl-gray" id="order_id">{{$order->id}}</span></li>
                <li><i class="fal fa-clock"></i>Delivery Date :<span class="cl-gray" id="delivery_date">14 Dec.2022 </span></li>
                <li><i class="fal fa-user-alt"></i>Driver :<span class="cl-gray"  id="driver"> @if($order->driver!=null) {{$order->driver->name}} @endif</span></li>
            </ul>
        </div>
    </div>
    <div class="col-xl-4 col-md-4">
        <div class="or-details-box">
            <ul >
                <li><i class="fal fa-clock"></i>Arrival Date :<span class="cl-gray">{{$order->drop_date}} </span></li>
                <li><i class="fal fa-file"></i>Order Status :<a href="#" class="status-btn"> @if($order->progress!=null)
                    {{$order->progress}}
                  @else
                       in accepted process
                  @endif</a></li>
                <li><i class="fal fa-usd-square"></i>Total Order :<span class="cl-gray">
                      @if ($order->discounttype==null)
                       {{$order->price_before_discount}}
                      @elseif($order->discounttype=='branch')
                      {{$order->price_after_discount}}
                      @elseif($order->discounttype=='Admin')
                      {{$order->price_before_discount}}
                      @else
                      {{$price_before_discount}}
                      @endif


                    SR</span></li>
            </ul>
        </div>
    </div>
    <div class="col-xl-4 col-md-4">
        <div class="or-details-box">
            <ul>
                @if ($order->confirmation=='pending')
                  <li class="text-right d-mob-edit"><a href="{{route('branch.finish.order',$order->id)}}" class="edit-btn"><i class="fal fa-edit"></i>Accept Order</a> </li>
                  <a href="{{route('branch.finish.order',$order->id)}}" class="edit-btn mob"><i class="fal fa-edit"></i>Accept Order</a>
                  @endif
                
                <li class="text-right d-mob-edit"><a href="#" class="edit-btn"><i class="fal fa-edit"></i>Edit Order</a> </li>
                <a href="#" class="edit-btn mob"><i class="fal fa-edit"></i>Edit Order</a>

                <li><i class="fal fa-truck"></i>Delivery Type :<span class="cl-gray">{{$order->deliverytype->name}}</span></li>
                <li><i class="fal fa-wallet"></i>Payment Method :<span class="cl-gray">
                   @if ($order->paymenttype!=null)
                       {{$order->paymenttype->name}}
                   @else
                        cash
                   @endif
                </span></li>
            </ul>
        </div>
    </div>
</div>

<div class="customer-data">
    <div class="line-title"><h6 class="cl-gray">Customer Info</h6></div>
    <div class="row">
        <div class="col-xl-4 col-md-6">
            <div class="or-details-box">
                <ul>
                    <li><i class="fal fa-user-alt"></i>Customer name :<span class="cl-gray" id="customer_name">{{$order->customer_name}}</span></li>
                    <li><i class="fal fa-map-marker-alt"></i>Location :<span class="cl-gray" id="customer_localtion">{{$order->customer_location}}</span></li>
                </ul>
            </div>
        </div>
        <div class="col-xl-4 col-md-6">
            <div class="or-details-box">
                <ul>
                    <li><i class="fas fa-phone-alt"></i>Phone Number :<span class="cl-gray"id="customer_phone">{{$order->customer_phone}}</span></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="item-info-order">
    <div class="line-title"><h6 class="cl-gray">Items Info</h6></div>
    <div class="table_responsive_maas v2">
        <table class="table" width="100%">
            <thead>
                <tr>
                    <th>Item</th>
                    <th>Service</th>
                    <th>Fee </th>
                    <th>Additionals </th>
                    <th>Fee</th>
                    <th>Quantity</th>
                    <th>Urgent</th>
                    <th>Fee</th>
                    <th>Total Fees</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($order->orderdetailes as $detail)
                    <tr>
                        <td>{{$detail->Branchitem->name}}</td>
                        <td>{{$detail->service->name}}</td>
                        <td>
                            @if($detail->additionalservice==null)
                                {{$detail->price}}
                            @endif
                        </td>
                        <td>
                            @if($detail->additionalservice!=null)
                                {{$detail->additionalservice->name}}
                            @endif
                        </td>
                        <td>
                            @if($detail->additionalservice!=null)
                                {{$detail->price}}
                            @endif
                        </td>
                        <td>{{$detail->quantity}}</td>
                        <td>
                            @php$urgentprice=0 @endphp
                            @if($detail->additionalservice==null)
                                @foreach($urgents as $urgent)
                                    @if($urgent->service_id==$detail->service_id&&$urgent->branchitem_id==$detail->branchitem_id)
                                        {{$urgent->quantity}}

                                        @php $urgentprice=$urgent->price @endphp
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        <td>

                            @if($detail->additionalservice==null)
                                @foreach($urgents as $urgent)
                                    @if($urgent->service_id==$detail->service_id&&$urgent->branchitem_id==$detail->branchitem_id)
                                        {{$urgent->price}}
                                    @endif
                                @endforeach
                            @endif
                        </td>
                        <td>
                            @if($detail->additionalservice==null)
                            {{$detail->price+$urgentprice}}
                             @else
                             {{$detail->price}}
                            @endif

                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
