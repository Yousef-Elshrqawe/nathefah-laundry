<?php
$active_links = ['orders_completed', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                @include('Admin.components.nav')

                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="row">
                            <div class="col-xl-6 col-md-12 mb-4">
                                <div class="inner-title-box">
                                    <div>
                                        <h4 class="title-inner">Orders Completed</h4>
                                        <p class="title-inner">Total Orders Completed: {{ $rows->count() }}</p>

                                        @php
                                            $total_payment_cash_after_discount = $rows
                                                ->where('pymenttype_id', 2)
                                                ->where('price_after_discount', '!=', null)
                                                ->sum('price_after_discount');
                                            $total_payment_cash_before_discount = $rows
                                                ->where('pymenttype_id', 2)
                                                ->where('price_after_discount', null)
                                                ->sum('price_before_discount');
                                            $total_payment_cash =
                                                $total_payment_cash_after_discount +
                                                $total_payment_cash_before_discount;
                                            $total_payment_wallet_after_discount = $rows
                                                ->where('pymenttype_id', 1)
                                                ->where('price_after_discount', '!=', null)
                                                ->sum('price_after_discount');
                                            $total_payment_wallet_before_discount = $rows
                                                ->where('pymenttype_id', 1)
                                                ->where('price_after_discount', null)
                                                ->sum('price_before_discount');
                                            $total_payment_wallet =
                                                $total_payment_wallet_after_discount +
                                                $total_payment_wallet_before_discount;
                                            // $total_payment_online = $rows->where('pymenttype_id', 3)->where('price_after_discount', '!=', null)->sum('price_after_discount');
                                            // $total_payment_online_before_discount = $rows->where('pymenttype_id', 3)->where('price_after_discount', null)->sum('price_before_discount');
                                            // $total_payment_online = $total_payment_online + $total_payment_online_before_discount;
                                            $total_payment_online = 0;
                                        @endphp
                                        <p class="title-inner">Total Cash Payment: {{ $total_payment_cash }}</p>
                                        <p class="title-inner">Total Wallet Payment: {{ $total_payment_wallet }}</p>
                                        {{-- <p class="title-inner">online Payment: {{ $total_payment_online }}</p> --}}
                                        @foreach ($payment_methods as $payment_method)
                                            @php
                                                $total_payment_method_after = $rows
                                                    ->where('payment_method_id', $payment_method->id)
                                                    ->where('price_after_discount', '!=', null)
                                                    ->sum('price_after_discount');
                                                $total_payment_method_before_discount = $rows
                                                    ->where('payment_method_id', $payment_method->id)
                                                    ->where('price_after_discount', null)
                                                    ->sum('price_before_discount');
                                                $total_payment_method =
                                                    $total_payment_method_after + $total_payment_method_before_discount;
                                                $total_payment_online += $total_payment_method;
                                            @endphp
                                            <p class="title-inner">Total {{ $payment_method->translations[0]->name }}
                                                Payment: {{ $total_payment_method }}</p>
                                        @endforeach


                                        {{-- @php

                                        @endphp
                                        <p class="title-inner">Nathefah Commission: </p> --}}
                                    </div>

                                </div>
                            </div>
                            <div class="col-xl-6 col-md-12 mb-4">
                                <div class="inner-title-box">
                                    <div>

                                        @php
                                            $price =
                                                $total_payment_cash + $total_payment_wallet + $total_payment_online;
                                            $adminCommission =
                                                (DB::table('application_rates')->first()->rate * $price) / 100;
                                        @endphp
                                        <p class="title-inner">Admin Commission: {{ round($adminCommission, 2) }}</p>
                                        <p class="title-inner">Laundry dues: {{ round($price - $adminCommission, 2) }}
                                        </p>


                                        <p class="title-inner">Total Price: {{ $price }}</p>

                                        {{-- @php

                                        @endphp
                                        <p class="title-inner">Nathefah Commission: </p> --}}
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <form action="{{ route('admin.filter.orders') }}" method="get">
                                    <div class="table-box-select">
                                        <div class="d-select-box">
                                            <select class="form-select" id="laundry" name="laundry"
                                                aria-label="Default select example">
                                                <option {{ request('laundry') == 'All' ? 'selected' : '' }} value="All">
                                                    All</option>
                                                @foreach ($laundries as $laundry)
                                                    <option {{ request('laundry') == $laundry->id ? 'selected' : '' }}
                                                        value="{{ $laundry->id }}">{{ $laundry->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="d-select-box">
                                            <select class="form-select" id="branch" name="branch"
                                                aria-label="Default select example">
                                                <option {{ request('branch') == 'All' ? 'selected' : '' }} value="All">All</option>
                                                <option value=""></option>
                                            </select>
                                        </div>

                                        <div class="d-select-box">
                                            <select class="form-select" id="pymenttype_id" name="pymenttype_id"
                                                aria-label="Default select example">
                                                <option {{ request('pymenttype_id') == 'All' ? 'selected' : '' }}
                                                    value="All">All</option>
                                                <option {{ request('pymenttype_id') == '1' ? 'selected' : '' }}
                                                    value="1">Wallet</option>
                                                <option {{ request('pymenttype_id') == '2' ? 'selected' : '' }}
                                                    value="2">Cash</option>
                                                <option {{ request('pymenttype_id') == '3' ? 'selected' : '' }}
                                                    value="3">online payment</option>
                                            </select>
                                        </div>
                                        <div class="d-select-box" id="payment_method">
                                            <select class="form-select" id="select_date" name="payment_method"
                                                aria-label="Default select example">
                                                <option value="All">All</option>
                                                @foreach ($payment_methods as $payment_method)
                                                    <option
                                                        {{ request('payment_method') == $payment_method->id ? 'selected' : '' }}
                                                        value="{{ $payment_method->id }}">
                                                        {{ $payment_method->translations[0]->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="d-select-box">
                                            <select class="form-select" id="select_date" name="date"
                                                aria-label="Default select example">
                                                <option {{ request('date') == 'All' ? 'selected' : '' }} value="All">All
                                                </option>
                                                <option {{ request('date') == '1' ? 'selected' : '' }} value="1">One
                                                    day</option>
                                                <option {{ request('date') == '7' ? 'selected' : '' }} value="7">One
                                                    week</option>
                                                <option {{ request('date') == '30' ? 'selected' : '' }} value="30">One
                                                    moth</option>
                                                <option {{ request('date') == '60' ? 'selected' : '' }} value="60">Two
                                                    month</option>
                                                <option {{ request('date') == '90' ? 'selected' : '' }} value="90">
                                                    Three month</option>
                                            </select>
                                        </div>

                                        <button type="submit" class="btn-style-one">Filter</button>

                                    </div>
                                </form>
                                <br>
                            </div>
                            <div class="row justify-content-end">
                                <div class="col-auto btn-right">
                                    <a href="{{ route('admin.export.invoices', $rows->pluck('id')) }}"
                                        class="btn-style-one">Export</a>
                                </div>
                            </div>
                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">Order number</span></th>
                                            <th><span class="th-head-icon">Customer name </span></th>
                                            <th><span class="th-head-icon">Customer Phone </span></th>
                                            <th><span class="th-head-icon">Branch name </span></th>
                                            <th><span class="th-head-icon">Payment type</span></th>
                                            <th><span class="th-head-icon">Payment method</span></th>
                                            <th><span class="th-head-icon">Delivery fees </span></th>
                                            <th><span class="th-head-icon">Taxes </span></th>
                                            <th><span class="th-head-icon">Discount value </span></th>
                                            <th><span class="th-head-icon">Price </span></th>
                                            <th><span class="th-head-icon">Created at </span></th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($rows as $row)
                                            <tr>
                                                <td>{{ $row->id }}</td>
                                                <td>{{ $row->customer_name }}</td>
                                                <td>{{ $row->customer_phone }}</td>
                                                <td>{{ $row->branch->username }}</td>
                                                @php
                                                    if ($row->pymenttype_id == 1) {
                                                        $payment_type = 'Wallet';
                                                    } elseif ($row->pymenttype_id == 2) {
                                                        $payment_type = 'Cash';
                                                    } else {
                                                        $payment_type = 'Visa';
                                                    }

                                                    if ($row->price_after_discount != null) {
                                                        $price = $row->price_after_discount;
                                                    } else {
                                                        $price = $row->price_before_discount;
                                                    }
                                                @endphp
                                                <td>{{ $payment_type }}</td>
                                                @if ($row->pymenttype_id == 3)
                                                    <td>{{ $row->paymenttype ? $row->paymenttype->translations[0]->name : '--' }}
                                                    </td>
                                                @else
                                                    <td> -- </td>
                                                @endif
                                                <td>{{ $row->delivery_fees }}</td>
                                                <td>{{ $row->taxes }}</td>
                                                <td>{{ $row->discount_value }}</td>
                                                <td>{{ $price }}</td>
                                                <td>{{ $row->created_at }}</td>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>

                                <div class="justify-content-center d-flex">
                                    {!! $rows->appends(Request::except('page'))->render() !!}
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
                <!-- start transaction model -->
                <div class="modal fade" id="transfer-money-view" tabindex="-1" aria-labelledby="transfer-money-view"
                    aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <div class="modal-view-title">
                                    <h5 class="left-border">Transaction Details</h5>
                                    <div class="margint-5">
                                        <a href="#" class="btn-green">Dipost</a>
                                    </div>
                                </div>
                                <button type="button" class="btn-close" data-bs-dismiss="modal"
                                    aria-label="Close"></button>
                            </div>
                            <div class="modal-body">
                                <div class="tran-details">
                                    <div class="tran-item">
                                        <div class="tran-first-item">
                                            <div><i class="far fa-usd-square"></i>Amount :</div>
                                        </div>
                                        <div class="tran-two-item">
                                            <span class="cl-green font-18" id="amount"> SR</span>
                                        </div>
                                    </div>
                                    <div class="tran-item">
                                        <div class="tran-first-item">
                                            <div><i class="far fa-user"></i>From :</div>
                                        </div>
                                        <div class="tran-two-item">

                                            <span class="cl-gray" id="from"></span>
                                        </div>
                                    </div>
                                    <div class="tran-item">
                                        <div class="tran-first-item">
                                            <div><i class="far fa-file-invoice-dollar"></i>Transaction Type :</div>
                                        </div>

                                        <div class="tran-two-item">
                                            <span class="cl-gray" id="transaction_type"></span>
                                        </div>
                                    </div>


                                    <div class="tran-item">
                                        <div class="tran-first-item">
                                            <div><i class="far fa-file"></i>Transaction Status :</div>
                                        </div>
                                        <div class="tran-two-item">
                                            <a href="#" class="btn-green">Success</a>
                                        </div>
                                    </div>
                                    <div class="tran-item">
                                        <div class="tran-first-item">
                                            <div><i class="far fa-file-invoice-dollar"></i>fort ID :</div>
                                        </div>
                                        <div class="tran-two-item">
                                            <span id="transaction_id"></span><span class="cl-65878F"></span>
                                        </div>
                                    </div>
                                    <div class="tran-item">
                                        <div class="tran-first-item">
                                            <div><i class="far fa-file-invoice-dollar"></i>Payment Type :</div>
                                        </div>
                                        <div class="tran-two-item">
                                            <span class="cl-gray" id="payment_type"> </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- end transaction model --}}
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.7.0/dist/jquery.min.js"></script>

    <script>
        $(document).ready(function() {
            if ($('#pymenttype_id').val() != 3) {
                $('#payment_method').hide(); // Correct way to hide an element using jQuery
            }
            $('#pymenttype_id').on('change', function() {
                var pymenttype_id = $(this).val();
                console.log(pymenttype_id);

                if (pymenttype_id == 3) {
                    $('#payment_method').show(); // Show the element when condition is met
                } else {
                    $('#payment_method').hide(); // Hide otherwise
                }
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            var laundry = $('#laundry').val();
            var req = @json(request()->all());
            console.log(laundry);
            console.log(req);
            if (laundry) {
                $.ajax({
                    url: '/Super-admin/getBransh/' + req['laundry'], // Removed unnecessary concatenation
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $('#branch').empty();
                        $('#branch').append('<option disabled>Select Branch</option>');

                        // Append "All" option


                        // Append dynamic options
                        $.each(data, function(index, subcategory) {
                            $('#branch').append(
                                `<option value="${subcategory.id}" ${req['branch'] == subcategory.id ? "selected" : ""}>${subcategory.username}</option>`
                            );
                        });
                    }
                });
            }

        });
        // getSubCategory
        $('#laundry').on('change', function() {
            var laundry = $(this).val();
            console.log(laundry);
            if (laundry) {
                $.ajax({
                    url: '/Super-admin/getBransh/' + laundry + '',
                    type: 'GET',
                    dataType: 'json',
                    success: function(data) {
                        console.log(data);
                        $('#branch').empty();
                        $('#branch').append('<option disabled >Select Branch</option>');
                        $.each(data, function(index, subcategory) {
                            $('#branch').append('<option value="' + subcategory.id + '">' +
                                subcategory.username + '</option>');

                        });
                    }
                });
            } else {
                $('#branch').empty();
            }
        });
    </script>
@endsection
