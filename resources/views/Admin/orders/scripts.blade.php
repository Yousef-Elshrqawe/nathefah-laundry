<script>
    function getBranches() {

        let laundry_id = $('#laundrySelect').val();

        $.ajax({
            type: "GET",
            url: "{{route('admin.get.laundry.branches')}}",
            data: {
                laundry_id: laundry_id
            },
            beforeSend: function(){
            },
            success: function(response) {

                //Empty Select Option
                $('#branchesSelect').empty();

                //Choose first Option
                $('#branchesSelect').append($(
                    '<option>', {
                        value: "",
                        text: 'Choose Branch',
                        selected:true,
                        disabled:true
                    }))


                $('#branchesSelect').append($(
                    '<option>', {
                        value: "",
                        text: 'All Branches',
                    }))



                for (let i =0; i< response.data.length; i++){
                    $('#branchesSelect').append($('<option>', {
                        value: response.data[i].id,
                        text: response.data[i].username
                    }));
                }
            },complete: function(){
            },error: function(error){
            }

        });
    }


</script>
