<?php
$active_links = ['update_apps', ''];
?>
@extends('Admin_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome Ahmed ! </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                    </div>
                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            <div class="notify-bell">
                                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                            </div>
                                            <div>
                                                <p>Sun 24 July 4:35 PM</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <p class="left-border">update Terms & Conditions</p>
                            </div>
                            <div class="row">
                                <div class="col-xl-12">

                                    <div class="card-body">
                                        <form class="form"
                                              action="{{route('admin.terms_conditions.update' , ['id'=> $row -> id])}}"
                                              method="post">
                                            @csrf

                                            <div class="form-body">

                                                {{--  <h4 class="form-section"><i class="ft-home"></i> بيانات الجهة </h4>  --}}

                                                <div class="row">

                                                    <div class="col-md-12">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">terms conditions arabic</label>
                                                            <textarea name="terms_conditions_ar" id="EditorJS" cols="30" rows="10"
                                                                      class="form-control">{!! $row -> terms_conditions_ar !!}</textarea>
                                                            @if ($errors->has('terms_conditions_ar'))
                                                                <p class="alert-danger">{{ $errors->first('terms_conditions_ar')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">terms conditions english</label>
                                                            <textarea name="terms_conditions_en" id="EditorJS" cols="30" rows="10"
                                                                      class="form-control">{!! $row -> terms_conditions_en !!}</textarea>
                                                            @if ($errors->has('terms_conditions_en'))
                                                                <p class="alert-danger">{{ $errors->first('terms_conditions_en')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">privacy policy arabic</label>
                                                            <textarea name="privacy_policy_ar" id="EditorJS" cols="30" rows="10"
                                                                      class="form-control">{!! $row -> privacy_policy_ar !!}</textarea>
                                                            @if ($errors->has('privacy_policy_ar'))
                                                                <p class="alert-danger">{{ $errors->first('privacy_policy_ar')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="col-md-12">
                                                        <div class="form-box">
                                                            <label for="" class="form-label">privacy policy english</label>
                                                            <textarea name="privacy_policy_en" id="EditorJS" cols="30" rows="10"
                                                                      class="form-control">{!! $row -> privacy_policy_en !!}</textarea>
                                                            @if ($errors->has('privacy_policy_en'))
                                                                <p class="alert-danger">{{ $errors->first('privacy_policy_en')}}</p>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>


                                            </div>

                                            <br>
                                            <div class="form-actions">
                                                {{--  <button type="button" class="btn btn-warning mr-1"
                                                    onclick="history.back();">
                                                    <i class="ft-x"></i> تراجع
                                                </button>  --}}
                                                <button type="submit" class="btn btn-primary">
                                                    <i class="la la-check-square-o"></i> Update
                                                </button>
                                            </div>
                                        </form>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')

@endsection
