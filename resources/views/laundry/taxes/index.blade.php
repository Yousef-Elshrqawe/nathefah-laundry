<?php
$active_links = ['taxes' , ''];
?>

@extends('laundry_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome  {{Auth::guard('laundry')->user()->name}} </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            {{--  <div class="notify-bell">
                                                  <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                              </div>--}}
                                            <div>
                                                {{-- <p>Sun 24 July 4:35 PM</p> --}}
                                                <p>{{now()->format('l  m-d')}}</p>
                                            </div>
                                            <div>
                                                <a href="{{route('laundry.logout')}}">log out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.taxes')</h5>
                                </div>
                                <div class="d-selct-view">
                                    {{-- <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-category-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.delivery_fees')
                                    </a> --}}
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>

                                        <th><span class="th-head-icon">@lang('admin.amount')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>

                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td> 1</td>
                                            <td class="text-info">{{$taxes->taxamount}}</td>




                                            <td style="display: -webkit-inline-box;">




                                                <div class="form-check form-switch">
                                                    <input class="form-check-input" type="checkbox" id="flexSwitchCheckDefault"style="display: inline-block;" @if($taxes->taxes==true) checked  @endif>
                                                  </div>
                                                  <a href="#" class="item_name"  data-id="" data-bs-toggle="modal" data-bs-target="#edit-tax-modal">
                                                    <i class="fas fa-edit"></i>
                                                </a>


                                            </td>

                                        </tr>
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
{{--                                    {!! $categories->appends(Request::except('page'))->render() !!}--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="modal fade" id="edit-tax-modal" tabindex="-1" aria-labelledby="edit-tax-modal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <form id="msform" action="{{route('laundry.update.taxes')}}" method="post">
                        @csrf

                        <div class="form-group">
                            <label>tax amount</label>
                            <input class="form-control" type="number" value="{{$taxes->taxamount}}" name="taxamount">
                        </div>

                        <div class=" mt-4">
                            <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> save</button >
                            <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>




@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#edit-tax-modal').modal('show');
            });
        </script>
    @endif


    <script>
      var SITEURL = "{{ url('/laundry-admin') }}";
      $('#flexSwitchCheckDefault').click(function(){
        $.ajax({
                url:SITEURL+"/taxes/status",
                type:"GET", //send it through get method
                success: function (response) {
                  if(response=='true'){
                    $('#flexSwitchCheckDefault').attr('checked');
                  }
                  if(response=='fasle'){

                  }
                },
                error: function(response) {
                    console.log('fdfd');
                }
              });
      });
    </script>

@endsection
