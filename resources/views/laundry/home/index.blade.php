@extends('laundry_temp')
@section('content')


<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">
            @include('laundry.components.nav')
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <h4 class="title-inner mb-2">
                                Overview
                            </h4>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Your Balance</p>
                                    </div>
                                </div>
                                <h4 class="cl-light">{{$balance}} SR</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Today Balance</p>
                                    </div>
                                </div>
                                <h4 class="cl-light">{{$balanceToday}} SR</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Today's Orders</p>
                                    </div>
                                </div>

                                <h4 class="cl-light">{{$todayOrders}} </h4>

                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">in Progress Orders</p>
                                    </div>
                                </div>

                                <h4 class="cl-light">{{$inProgress}}</h4>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 mb-3">
                            <div class="overview-box">
                                <div class="graph-box">
                                    <div class="row">
                                        <div class="col-xl-5">
                                            <h5 class="left-border">Branches Finance</h5>
                                        </div>
                                        <div class="col-xl-7">
                                            <div class="gr-drop-select">
                                                <div class="d-export-txt">
                                                    <div class="ex-img">
                                                        <img src="images/receipt-item.png" alt="">
                                                    </div>
                                                    <div>
                                                        <p class="cl-65878F">Export</p>
                                                    </div>
                                                </div>
                                                <div class="d-select-box">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Period Last 30 Days</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="dropdown btn-dropdown">
                                                    <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                      <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                      <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <img src="images/graph1.jpg" alt="">
                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 mb-3">
                            <div class="overview-box">
                                <div class="graph-box">
                                    <div class="row">
                                        <div class="col-xl-5 col-md-5">
                                            <h5 class="left-border">Orders Summary</h5>
                                        </div>
                                        <div class="col-xl-7 col-md-7">
                                            <div class="gr-drop-select">
                                                <div class="d-export-txt">
                                                    <div class="ex-img">
                                                        <img src="images/receipt-item.png" alt="">
                                                    </div>
                                                    <div>
                                                        <p class="cl-65878F">Export</p>
                                                    </div>
                                                </div>
                                                <div class="d-select-box">
                                                    <select class="form-select" aria-label="Default select example">
                                                        <option selected>Period Last 30 Days</option>
                                                        <option value="1">One</option>
                                                        <option value="2">Two</option>
                                                        <option value="3">Three</option>
                                                    </select>
                                                </div>
                                                <div class="dropdown btn-dropdown">
                                                    <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                    </button>
                                                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                      <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                      <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <img src="images/graph1.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div> --}}
                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">Latest Orders</h5>
                            </div>
                            <div class="table-box-select">
                                <div class="d-select-box">
                                    <select id="select_last_order" name="days" class="form-select" aria-label="Default select example">
                                        <option value="7" selected>Period last Week</option>
                                        <option value="All">All</option>
                                        <option value="7">One</option>
                                        <option value="14">Two</option>
                                        <option value="21">Three</option>
                                    </select>
                                </div>
                                <div class="d-select-box">
                                    <form action="{{route('laundry.home',[$week])}}" method="GET">
                                        <div class="d-select-box">
                                            <select class="form-select" name="branch_id" aria-label="Default select example" onchange="this.form.submit();">
                                                {{-- <option selected disabled>Branch Riyadh</option> --}}
                                                @foreach($branches as $branch)
                                                 <option value="{{$branch->id}}"  @if($branch->id == $branch_id) selected @endif>{{$branch->username}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </form>
                                </div>
                                @if(isset($orders))
                                <div class="d-selct-view">
                                    <a href="{{route('laundry.allorder')}}" class="cl-light">View All</a>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="table_responsive_maas v2">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th><span class="th-head-icon">Order number</span></th>
                                        <th><span class="th-head-icon">Customer name </span></th>
                                        <th><span class="th-head-icon">Delivery time </span></th>
                                        <th><span class="th-head-icon">Arrival Date </span></th>
                                        <th><span class="th-head-icon">Order Status </span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($orders))
                                    @foreach ($orders as $order)
                                        <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->customer_name}}</td>
                                            <td>
                                                @if ($order->deliverytype!=null)
                                                    {{$order->deliverytype->name}}
                                                @endif
                                            </td>
                                            <td>{{$order->drop_date}}</td>
                                            <td>
                                                @if($order->progress!=null)
                                                    {{$order->progress}}
                                                @else
                                                    in accepted process
                                                @endif
                                            </td>
                                            <td>    <a href="#" class="cl-light view_button" data-id="{{$order->id}}" data-bs-toggle="modal" data-bs-target="#order-details-modal">View</a> </td>
                                        </tr>
                                    @endforeach
                                    @endif

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>




    <!--=================ORDER DETAILS MODAL================= -->
<div class="modal fade" id="order-details-modal" tabindex="-1" aria-labelledby="order-details-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h5>Order Details</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body" id="order_body">

        </div>
      </div>
    </div>
</div>



</div>


@endsection
@section('scripts')
    <script>

        let url={!!json_encode(url('/'))!!}

        $('#select_last_order').change(function(){

            let week = $(this).find(":selected").val();

            window.location.href=url+'/laundry-admin/home/'+week;
        });

    </script>

    {{-- show order --}}
<script>
    var SITEURL = "{{ url('/laundry-admin') }}";
    $('.view_button').click(function(){
        //alert('vdfzx');
        var id=$(this).attr("data-id");
        //alert(id);
        $.ajax({
            url:SITEURL+"/order/view/"+id,
            type:"GET", //send it through get method
            success: function (response) {
                $('#order_body').html(response);
            },
            error: function(response) {
                console.log('fdfd');
            }
        });
    });
    </script>
@endsection
