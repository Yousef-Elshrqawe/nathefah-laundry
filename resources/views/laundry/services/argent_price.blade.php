<div class="modal fade" id="argentprice" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tabdel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table_responsive_maas">
                                    <table class="table" width="100%">
                                        <thead>
                                        <tr>
                                            <th><span class="th-head-icon">#</span></th>
                                            <th><span class="th-head-icon">Item </span></th>
                                            <th><span class="th-head-icon">Price </span></th>
                                            <th><span class="th-head-icon">Edit </span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($argentPrice as $key=>$price)

                                            <tr>
                                                <td>{{$key+1}}</td>
                                                <td>{{$price->name}}</td>
                                                <td>{{$price->argentprice}}</td>
                                                <td>
                                                    <a href="#" class="item_name"  data-id="{{$price->id}}" data-argentprice="{{$price->argentprice}}" data-bs-toggle="modal" data-bs-target="#editItemArgentPrice">
                                                        <i class="fas fa-edit"></i>
                                                    </a>
                                                </td>
                                            </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
