@extends('laundry_temp')
@section('content')


<div class="main-content">

    <div class="page-content">
        <div class="container-fluid">
            @include('laundry.components.nav')
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-12">
                            <h4 class="title-inner mb-2">
                                Services
                            </h4>
                        </div>
                        <div class="col-xl-12 mb-4">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">Services Reports</h5>
                            </div>
                            <div class="table-box-select">
                                <div class="d-select-box">
                                    <select class="form-select" aria-label="Default select example">
                                        <option selected="">Period last month</option>
                                        <option value="1">One</option>
                                        <option value="2">Two</option>
                                        <option value="3">Three</option>
                                    </select>
                                </div>
                                <form action="{{route('laundry.service.edit')}}" method="GET">
                                    <div class="d-select-box">
                                        <select class="form-select" name="branch_id" aria-label="Default select example" onchange="this.form.submit();">
                                            @foreach($branches as $branch)
                                            <option value="{{$branch->id}}"  @if($branch->id == $branch_id) selected @endif>{{$branch->username}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Delivered Orders</p>
                                    </div>
                                    <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h4 class="cl-light">27</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Completed Orders</p>
                                    </div>
                                    <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <h4 class="cl-light">24</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Delayed Orders</p>
                                    </div>
                                    <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <h4 class="cl-light">15</h4>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6 mb-3">
                            <div class="overview-box p-v2">
                                <div class="overlay-txt">
                                    <div>
                                        <p class="cl-gray">Canceled Orders</p>
                                    </div>
                                    <div class="dropdown btn-dropdown">
                                        <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                          <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                          <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                        </ul>
                                    </div>
                                </div>

                                <h4 class="cl-light">40</h4>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">Services </h5>
                            </div>
                            <div class="table-box-select">
                                {{-- <button form="form-updateservices" class="btn-style-two border">save</button> --}}
                                <a href="#" class="btn-style-two backbtn ms-0"><i class="fas fa-plus"></i> Add new item</a>
                            </div>
                        </div>
                        <div class="branch-admin-service-tabing">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="line-title"><h6>Main Services</h6></div>
                                    <form id="form-updateservices" method="post" action="{{route('laundry.service.update')}}">
                                    @csrf
                                    <input type="hidden" name="servicetype" value="main">
                                    <div class="tabbing-area">
                                        <ul class="nav nav-tabs">
                                            @foreach ($services as $key=>$service)
                                                <li class="nav-item">
                                                    <a class="nav-link @if($key==0) active @endif " data-bs-toggle="tab" href="#tab{{$key+1}}">{{$service->name}}</a>
                                                </li>
                                            @endforeach
                                            <li><a href="#" class="add-main-servicebtn"><i class="fas fa-plus"></i> Add new Main Service</a></li>
                                        </ul>
                                        <div class="tab-content">
                                            @foreach ($services as $key=>$service)
                                             <div class="tab-pane @if($key==0) active @endif" id="tab{{$key+1}}">
                                                <div class="row">
                                                    <div class="col-md-12 subnav-tab">
                                                        <ul class="nav nav-tabs">
                                                            @foreach($service->categories as $categorykey=>$category)
                                                                <li class="nav-item">
                                                                    <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tab{{$key+1}}-{{$categorykey+1}}">{{$category->name}}</a>
                                                                </li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </div>

                                                <div class="tab-content">
                                                    @foreach($service->categories as $categorykey=>$category)
                                                        <div class="tab-pane @if($categorykey==0) active @endif" id="tab{{$key+1}}-{{$categorykey+1}}">
                                                            <div class="row">
                                                                <div class="col-md-12">
                                                                    <div class="services-panel">
                                                                        @foreach ($category->branchitems as $item)
                                                                        @foreach ($item->branchitemprice as $price)
                                                                           @if($price->category_id==$category->id && $price->service_id==$service->id)
                                                                            <div class="services-panel-box">
                                                                                <label>{{$item->name}}</label>
                                                                                    <input name="count[]" type="number"class="form-control"  value="{{$price->price}}">
                                                                                    <input type="hidden" name="itempriceid[]" value="{{$price->id}}">
                                                                            </div>
                                                                              {{-- <span class="input-writeup">{{$price->price}} SR</span> --}}
                                                                        @endif
                                                                        @endforeach
                                                                        @endforeach
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>

                                             </div>

                                            @endforeach
                                        </div>
                                         {{-- end of main service --}}
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 pt-4">
                                            <div class="form-box">
                                                 <button type="submit" class="btn-style-two"><i class="far fa-check"></i> Save</button>
                                                {{-- <button type="button" class="btn-style-two backbtn"> Reset</button> --}}
                                            </div>
                                        </div>
                                    </div>
                                   </form>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 mt-3">
                                    <div class="line-title"><h6>Additional Services </h6></div>
                                    <form id="form-updateservices" method="post" action="{{route('branch.service.update')}}">
                                        @csrf
                                        <input type="hidden" name="servicetype" value="additional">
                                        <div class="tabbing-area">
                                            <ul class="nav nav-tabs">
                                                @foreach ($additionalservices as $key=>$additionalservice)
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($key==0) active @endif" data-bs-toggle="tab" href="#tab1{{$key+1}}">{{$additionalservice->name}}</a>
                                                    </li>
                                                @endforeach
                                                <li><a href="#" class="add-main-servicebtn"><i class="fas fa-plus"></i> Add new Main Service</a></li>
                                            </ul>
                                            <div class="tab-content">
                                                @foreach ($additionalservices as $key=>$additionalservice)
                                                <div class="tab-pane @if($key==0) active @endif" id="tab1{{$key+1}}">
                                                    <div class="row">
                                                        <div class="col-md-12 subnav-tab">
                                                            <ul class="nav nav-tabs">
                                                                @foreach ($additionalservice->categories as $categorykey=>$category)
                                                                <li class="nav-item">
                                                                    <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tab11-{{$categorykey+1}}">{{$category->name}}</a>
                                                                </li>
                                                                @endforeach
                                                            </ul>
                                                                <div class="tab-content">
                                                                    @foreach ($additionalservice->categories as $categorykey=>$category)
                                                                    <div class="tab-pane @if($categorykey==0) active  @endif" id="tab11-{{$categorykey+1}}">
                                                                        <div class="row">
                                                                            <div class="col-md-12">
                                                                                <div class="services-panel">
                                                                                    @foreach ($category->branchitems as $item)
                                                                                    @foreach ($item->branchitemprice as $itemprice)
                                                                                        @if ($itemprice->category_id==$category->id&&$itemprice->additionalservice_id==$additionalservice->id)
                                                                                            <div class="services-panel-box">
                                                                                                <label>{{$item->name}}</label>
                                                                                                <input name="count[]" type="number"class="form-control"  value="{{$itemprice->price}}">
                                                                                                <input type="hidden" name="itempriceid[]" value="{{$itemprice->id}}">

                                                                                            </div>
                                                                                        @endif
                                                                                    @endforeach
                                                                                    @endforeach
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    </div>
                                                                    @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                @endforeach
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12 pt-4">
                                                <div class="form-box">
                                                     <button type="submit" class="btn-style-two"><i class="far fa-check"></i> Save</button>
                                                    {{-- <button type="button" class="btn-style-two backbtn"> Reset</button> --}}
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


@endsection
@section('scripts')

@endsection
