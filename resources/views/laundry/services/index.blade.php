@extends('laundry_temp')
@section('content')


    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome  {{Auth::guard('laundry')->user()->name}} </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>




                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            {{--  <div class="notify-bell">
                                                  <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                              </div>--}}
                                            <div>
                                                {{-- <p>Sun 24 July 4:35 PM</p> --}}
                                                <p>{{now()->format('l  m-d')}}</p>
                                            </div>
                                            <div>
                                                <a href="{{route('laundry.logout')}}">log out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="row">
                            <div class="col-xl-12">
                                <h4 class="title-inner mb-2">
                                    Services
                                </h4>
                            </div>
                            <div class="col-xl-12 mb-4">
                                <div class="overview-box">
                                    <div class="table-box">
                                        <div>
                                            <h5 class="left-border">Services Reports</h5>
                                        </div>
                                        <div class="table-box-select">
                                            <div class="d-select-box">
                                                <select class="form-select" aria-label="Default select example">
                                                    <option selected="">Period last month</option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                            <form action="{{route('laundry.services')}}" method="GET">
                                                <div class="d-select-box">
                                                    <select class="form-select" name="branch_id" aria-label="Default select example" onchange="this.form.submit();">
                                                        @foreach($branches as $branch)
                                                            <option value="{{$branch->id}}" @if($branch->id == request()->get('branch_id')) selected @endif>{{$branch->username}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </form>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Delivered Orders</p>
                                        </div>
                                    </div>
                                    <h4 class="cl-light">{{$reports->allOrders}}</h4>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Completed Orders</p>
                                        </div>
                                    </div>
                                    <h4 class="cl-light">{{$reports->completedOrders}}</h4>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Delayed Orders</p>
                                        </div>
                                    </div>

                                    <h4 class="cl-light">{{$reports->inProgressOrders}}</h4>
                                </div>
                            </div>
                            <div class="col-xl-3 col-md-6 mb-3">
                                <div class="overview-box p-v2">
                                    <div class="overlay-txt">
                                        <div>
                                            <p class="cl-gray">Canceled Orders</p>
                                        </div>
                                    </div>

                                    <h4 class="cl-light">{{$cancelOrders}}</h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div>
                                    <h5 class="left-border">Services </h5>
                                </div>
                                <div class="table-box-select">
                                    <a href="{{route('laundry.service.edit')}}" class="btn-style-two border">Edit Items</a>
                                    <a href="#" class="btn-style-two backbtn ms-0" data-bs-toggle="modal" data-bs-target="#Add_item">
                                        <i class="fas fa-plus">
                                        </i> Add item</a>
                                    <a href="#" class="btn-style-two backbtn ms-0" data-bs-toggle="modal" data-bs-target="#itemsStatus">
                                        <i class="fas fa-toggle-on">
                                        </i>Service Status</a>
                                    <a href="#" class="btn-style-two backbtn ms-0" data-bs-toggle="modal" data-bs-target="#argentprice">
                                        <i class="fas fa-money-bill">
                                        </i>Urgent Price</a>
                                </div>
                            </div>
                            <div class="branch-admin-service-tabing">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="line-title"><h6>Main Services</h6></div>
                                        @include('components.services.mainservice',['services'=>$services])

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 mt-3">
                                        <div class="line-title"><h6>Additional Services </h6></div>
                                        @include('components.services.additionalservice',['additionalservices'=>$additionalservices])

                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- ==============CREATE NEW Item MODAL===================== -->
    @include('laundry.services.create_item')
    <!-- ==============CREATE NEW Additional service MODAL===================== -->
    @include('laundry.services.add_additional_service')



    {{-- start edit item modal --}}
    @include('laundry.services.edit_item')

    @include('components.services.item_status')
    @include('laundry.services.argent_price')
    {{-- start edit item modal --}}
    <div class="modal fade" id="editItemArgentPrice" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Argent Price</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{route('laundry.service.item.argent.price')}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-body">
                        <input type="hidden" name="id" id="PriceId">
                        <label>Price</label>
                        <input class="form-control" id="editArgentPrice" name="argentprice" type="text">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    {{-- end edit item modal --}}
@endsection
@section('scripts')
    {{-- script of edit item --}}
    <script>
        var SITEURL = "{{ url('/branch-admin') }}";
        var languages={!!json_encode(config('translatable.locales'))!!}
        console.log(languages);
        $('.item_name').click(function(){
            var id=$(this).attr("data-id");
            $.ajax({
                url:SITEURL+"/get/branchitem/"+id,
                type:"GET", //send it through get method
                success: function (response) {
                    $('#branch_id').val(response.id);
                    for (let x in response.branchitemtranslation) {
                        for(let y in languages){
                            if(languages[y]==response.branchitemtranslation[x].locale){
                                $('#name_'+languages[y]).val(response.branchitemtranslation[x].name);
                            }
                        }
                    }
                },
                error: function(response) {
                    console.log('fdfd');
                }
            });
        });
    </script>
    {{-- script of error edit modal --}}
    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#edititem').modal('show');
            });
        </script>
    @endif
    @include('laundry.services.scripts')
@endsection
