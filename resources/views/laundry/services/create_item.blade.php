<div class="modal fade" id="Add_item" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <form id="msform" action="{{route('laundry.add.item')}}" method="post">
                    @csrf
                    <input type="hidden" name="type" value="service">
                    <input type="hidden" name="branch_id" value="{{isset($_GET['branch_id']) ? $_GET['branch_id'] : ""}}">
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Add Item </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="tabbing-area">
                        <ul class="nav nav-tabs">
                            @foreach ($services as $key=>$service)
                                <li class="nav-item">
                                    <a class="nav-link @if($key==0) active @endif " data-bs-toggle="tab" href="#tabs{{$key+1}}">{{$service->name}}</a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="tab-content">
                            @foreach ($services as $key=>$service)
                                <div class="tab-pane @if($key==0) active @endif" id="tabs{{$key+1}}">
                                    <div class="row">
                                        <div class="col-md-12 subnav-tab">
                                            <ul class="nav nav-tabs">
                                                @foreach($service->categories as $categorykey=>$category)
                                                    <li class="nav-item">
                                                        <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tabc{{$key+1}}-{{$categorykey+1}}">{{$category->name}}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="tab-content">
                                        @foreach($service->categories as $categorykey=>$category)
                                            <div class="tab-pane @if($categorykey==0) active @endif" id="tabc{{$key+1}}-{{$categorykey+1}}">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="services-panel">


                                                            @foreach ($category->items as $item)
                                                                <div class="services-panel-box">
                                                                    <label>{{$item->name}}</label>
                                                                    @php $pricevalue=''  ; @endphp
                                                                    @foreach ($category->branchitems as $branchitem)
                                                                        @foreach ($branchitem->branchitemprice as $itemprice)
                                                                            @if ($itemprice->item_id==$item->id&& $itemprice->category_id==$category->id&&$itemprice->service_id==$service->id)
                                                                                @php $pricevalue=$itemprice->price    @endphp

                                                                            @endif
                                                                        @endforeach
                                                                    @endforeach
                                                                    <input name="count[]" type="number"class="form-control" value="{{$pricevalue}}">
                                                                    <input type="hidden" name="item_id[]" value="{{$item->id}}">
                                                                    <input type="hidden" name="category_id[]" value="{{$category->id}}">
                                                                    <input type="hidden" name="service_id[]" value="{{$service->id}}">
                                                                </div>
                                                            @endforeach





                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>

                                </div>

                            @endforeach
                        </div>
                        {{-- end of main service --}}
                    </div>



                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Add Item</button >
                        <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
