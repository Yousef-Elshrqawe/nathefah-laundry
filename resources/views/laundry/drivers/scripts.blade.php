<script>


    function addNewDriver()
    {
        $('#new-driver-modal').modal('show');
    }

    function getDrivers()
    {
        let branchId = $("#branchId").val();
        let month    = $("#selectedMonth").val();
        if (!branchId) {
            alert("Choose branch!");
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{route('laundry.branch.drivers')}}",
            data:{
                branchId:branchId,
                month:month
            },
            beforeSend: function(){
            },
            success: function(response) {
                $("#driversData").empty();
                let countData = response.data.length;
                let data      = response.data;
                console.log(response.data);

                for(let i =0; i < countData; i++) {
                    let image = "{{asset('images/profile.jpg')}}";

                    let route = '{{route("laundry.driver.details", ":id")}}';
                     route = route.replace(':id', data[i].id);

                    let appendData =  `<div class="driverbox" >
                        <div class="driverbox1">
                            <img src="${image}" alt=""/>
                            <h6 style="cursor: pointer"> <a href="${route}" target="_blank">${data[i].name}</a> </h6>
                            <span class="avl-version"><i class="fas fa-circle"></i> Available </span>
                            <span class="order-count"><strong>${data[i].orderscount}</strong> Orders</span>
                        </div>
                        <div class="driverbox2">
                            <div class="driverbox2-sub1">Driver Rate <strong>${data[i].rate ? data[i].rate.rate : 0}</strong></div>
                            <div class="driverbox2-sub1">Success Deliverys<strong>${data[i].delivery_status.length}</strong></div>
                        </div>
                    </div>`;
                    $('#driversData').append(appendData);
                }

            },complete: function(){

            },error: function(error){

            }

        });
    }


    function getTopDrivers()
    {
        let branchId = $("#topDriverBranchId").val();
        let month    = $("#selectedMonthTopDriver").val();
        if (!branchId) {
            alert("Choose branch!");
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{route('laundry.branch.top-drivers')}}",
            data:{
                branchId:branchId,
                month:month
            },
            beforeSend: function(){
            },
            success: function(response) {
                $("#deliverdOrder").html(response.data.deliverdOrder)
                $("#successDriverCount").html(response.data.successDriverCount)
                $("#canceledorders").html(response.data.canceledorders)
                $("#toDriverName").html(response.data.topDriver.name)

            },complete: function(){

            },error: function(error){

            }

        });
    }
</script>
