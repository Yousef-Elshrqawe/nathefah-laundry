<script>
    function openEditModal()
    {
        $('#editDriverModal').modal('show');

        let driverID          = $("#driverId").val();
        let driverName        = $("#driverName").text();
        let driverEmail       = $("#driverEmail").text();
        let driverCountryCode = $("#driverCountryCode").text();
        let driverPhone       = $("#driverPhone").text();

        $("#driverIdInput").val(driverID);
        $("#driverNameInput").val(driverName);
        $("#driverEmailInput").val(driverEmail);
        $("#driverCountryCodeInput").val(driverCountryCode);
        $("#driverPhoneInput").val(driverPhone);
    }



    $('#order-details-modal').on('show.bs.modal', function(event) {
        let button = $(event.relatedTarget)
        let id      = button.data('id')
        let created = button.data('created')
        console.log(created)
        let modal = $(this)
        modal.find('.modal-body #orderID').html(id)
        modal.find('.modal-body #orderCreatedAt').html(created)
    });
</script>
