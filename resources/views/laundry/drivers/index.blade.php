@extends('laundry_temp')
@section('content')


        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    @include('laundry.components.nav')


                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="inner-title-box">
                                <div>
                                    <h4 class="title-inner">Drivers</h4>
                                </div>
                                <div class="btn-flex">
                                    {{-- <a href="#" class="btn-style-one">Deliver an Order</a> --}}
                                    <a href="#" class="btn-style-two border" onclick="addNewDriver()"><i class="fas fa-plus"></i> Add new Driver</a>
                                </div>
                            </div>
                        </div>

                            <div class="col-xl-12 mb-4">
                                <div class="overview-box">
                                    <div class="table-box">
                                        <div>
                                            <h5 class="left-border">Drivers List</h5>
                                        </div>
                                        <div class="table-box-select">
                                            <div class="d-select-box">
                                                <select class="form-select" onchange="getDrivers()" id="selectedMonth" aria-label="Default select example">
                                                    <option selected disabled value="">Period last month</option>
                                                    <option value="{{date('Y-m-d', strtotime("-1 month"))}}">One</option>
                                                    <option value="{{date('Y-m-d', strtotime("-2 month"))}}">Two</option>
                                                    <option value="{{date('Y-m-d', strtotime("-3 month"))}}">Three</option>
                                                </select>
                                            </div>
                                            <div class="d-select-box">
                                                <select class="form-select" onchange="getDrivers()" id="branchId" name="branch_id" aria-label="Default select example">
                                                    @foreach($branches as $item)
                                                        <option value="{{$item->id}}">{{$item->username}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>




						<div class="col-lg-12">

							<div class="driverbox-main" id="driversData">

                                @foreach($drivers as $item)
                                    <div class="driverbox" >
                                        <div class="driverbox1">
                                            <img src="{{asset('images/profile.jpg')}}" alt=""/>
                                            <h6 style="cursor: pointer"> <a href="{{route('laundry.driver.details',$item->id)}}" target="_blank">{{$item->name}}</a> </h6>
                                            <span class="avl-version"><i class="fas fa-circle"></i> Available </span>
                                            <span class="order-count"><strong>{{$item->driverOrdersInProgress()}}</strong> Orders</span>
                                        </div>
                                        <div class="driverbox2">
                                            <div class="driverbox2-sub1">Driver Rate <strong>{{ $item->rate ? $item->rate : 0 }}</strong></div>
                                            <div class="driverbox2-sub1">Success Deliverys<strong>{{ $item->driverOrdersCompleted() }}</strong></div>
                                        </div>
                                    </div>
                                @endforeach
							</div>
						</div>




                        <div class="col-xl-12 mb-4">
                            <div class="overview-box">
                                <div class="table-box">
                                    <div>
                                        <h5 class="left-border">Drivers Reports</h5>
                                    </div>
                                    <div class="table-box-select">
                                        <div class="d-select-box">
                                            <select class="form-select" onchange="getTopDrivers()" id="selectedMonthTopDriver" aria-label="Default select example">
                                                <option selected>Period last month</option>
                                                <option value="{{date('Y-m-d', strtotime("-1 month"))}}">One</option>
                                                <option value="{{date('Y-m-d', strtotime("-2 month"))}}">Two</option>
                                                <option value="{{date('Y-m-d', strtotime("-3 month"))}}">Three</option>
                                            </select>
                                        </div>
                                        <div class="d-select-box">
                                            <select class="form-select" onchange="getTopDrivers()"  id="topDriverBranchId" aria-label="Default select example">
                                                @foreach($branches as $item)
                                                    <option value="{{$item->id}}">{{$item->username}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12" id="topDriverReports">
                            <div class="row">
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Delivered Orders</p>
                                            </div>
                                            <div class="dropdown btn-dropdown">
                                                {{-- <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                </button> --}}

                                            </div>
                                        </div>
                                        <h4 class="cl-light" id="deliverdOrder">{{$deliverdOrder!=null ? $deliverdOrder : ''}} </h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Success Deliverys</p>
                                            </div>
                                            <div class="dropdown btn-dropdown">
                                                {{-- <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                </button> --}}

                                            </div>
                                        </div>
                                        <h4 class="cl-light" id="successDriverCount">{{$successDriverCount}}</h4>
                                    </div>
                                </div>


                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">canceled Orders</p>
                                            </div>
                                            <div class="dropdown btn-dropdown">
                                                {{-- <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                </button> --}}

                                            </div>
                                        </div>
                                        <h4 class="cl-light" id="canceledorders">{{$canceledorders}}</h4>
                                    </div>
                                </div>



                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Top Driver </p>
                                            </div>
                                            <div class="dropdown btn-dropdown">
                                                {{-- <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                </button>
                                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                  <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                  <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                </ul> --}}
                                            </div>
                                        </div>

                                        <h4 class="cl-light" id="toDriverName">{{$topDriver!=null ? $topDriver->name  : ''}} </h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('laundry.drivers.create')
    <!-- <script>
        $(window).scroll(function(){
          var sticky = $('.page-topbar'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixedhead');
          else sticky.removeClass('fixedhead');
        });
    </script> -->
    <script src="js/menu.js"></script>
    <script type="text/javascript">
        $("#cssmenu").menumaker({
            title: "",
            format: "multitoggle"
        });
    </script>

@endsection
@section('scripts')
    @include('laundry.drivers.scripts')

@endsection
