@extends('laundry_temp')
@section('content')
        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">


                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="page-topbar">
                                <div class="overview-box">
                                    <div class="row align-items-center flex-column-reverse flex-xl-row">
                                        <div class="col-xl-4 col-md-12">
                                            <div class="top-bar-title">
                                                <h5>Welcome  {{Auth::guard('laundry')->user()->name}} </h5>
                                                <p class="cl-gray">Here is what happened in the branches today</p>
                                            </div>
                                        </div>

                                        <div class="col-xl-4 col-md-12">
                                            <div class="notify-box">
                                                {{--  <div class="notify-bell">
                                                      <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                                  </div>--}}
                                                <div>
                                                    {{-- <p>Sun 24 July 4:35 PM</p> --}}
                                                    <p>{{now()->format('l  m-d')}}</p>
                                                </div>
                                                <div>
                                                    <a href="{{route('laundry.logout')}}">log out</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <h4 class="title-inner">Settings</h4>
                            <input type="hidden" id="laundryID" value="{{$laundryInfo->id}}">
							<div class="whitebox-shadow">
								<div class="row">
									<div class="col-xl-3 col-lg-3 col-md-12 lefttab-nav">
										<ul class="nav nav-tabs">
											<li class="nav-item">
												<a class="nav-link active" data-bs-toggle="tab" href="#leftnav1">Basic info</a>
											</li>
{{--											<li class="nav-item">--}}
{{--												<a class="nav-link " data-bs-toggle="tab" href="#leftnav2">Notifications</a>--}}
{{--											</li>--}}
{{--											<li class="nav-item">--}}
{{--												<a class="nav-link" data-bs-toggle="tab" href="#leftnav3">Users & Permissions</a>--}}
{{--											</li>--}}
										</ul>
									</div>
									<div class="col-xl-9 col-lg-9 col-md-12">
										<div class="tab-content setting-body">
                                            @include('laundry.settings.basic_info',[$branches])
										</div>
									</div>
								</div>
							</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- <script>
        $(window).scroll(function(){
          var sticky = $('.page-topbar'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixedhead');
          else sticky.removeClass('fixedhead');
        });
    </script> -->
    <script src="js/menu.js"></script>
    <script type="text/javascript">
        $("#cssmenu").menumaker({
            title: "",
            format: "multitoggle"
        });
    </script>

@endsection
@section('scripts')
    @include('laundry.settings.scripts')
@endsection
