<div class="tab-pane active" id="leftnav1">
    <div class="row">
        <div class="col-md-12">

            <h5 class="pb-4 left-border">Brand info </h5>



            <form action="{{route('laundry.setting.basic-info.update',$laundryInfo->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
            <div class="row">
                <div class="col-lg-12">
                    <div class="settings-profile">
                        <img src="{{$laundryInfo->logo}}"  id="blah" alt=""/>
                        <input  type="file"    name="logo"  accept="image/*"  id="imgInp" style="display:none"/>
                        <a href="#" class="btn-style-two" id="uploadNewImage"><i class="far fa-plus"></i> Upload new Picture</a>
                       {{-- <a href="#" class="btn-style-two backbtn">Remove Picture</a>--}}
                    </div>
                </div>
            </div>


            <div class="line-title lightcolor"><h6>Basic info</h6></div>

                <div class="row">
                    <div class="col-lg-11">
                        <div class="row pt-2">
                            <div class="col-md-6">
                                <div class=" mb-4">
                                    <label for="" class="form-label">Laundry Name</label>
                                    <input type="text" name="name" class="form-control" value="{{$laundryInfo->name}}" id="laundryNameID" placeholder="Fresh & Clean ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class=" mb-4">
                                    <label for="" class="form-label">Brand Email</label>
                                    <input type="email" name="email" class="form-control" value="{{$laundryInfo->email}}" id="laundryEmailID" placeholder="Fresh&Clean@mail.com">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <div class=" mb-4">
                                    <label for="" class="form-label">Country Code</label>
                                    <input type="text" name="country_code" class="form-control" value="{{$laundryInfo->country_code}}" id="laundryCountryCodeID" placeholder="Shop No. 700, Gali Kundewalan ">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class=" mb-4">
                                    <label for="" class="form-label">Support Number</label>
                                    <input type="text" name="phone" class="form-control" value="{{$laundryInfo->phone}}" id="laundryPhoneID" placeholder="56 866 0899">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class=" mb-4">
                                    <label for="" class="form-label">tax number </label>
                                    <input type="text" name="tax_number" class="form-control" value="{{$laundryInfo->tax_number}}" id="laundryPhoneID">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12 mt-2 mb-4">
                                <div class="">
                                    <button type="submit" class="btn-style-two"><i class="far fa-check"></i> Accept Changes</button>
{{--                                    <button type="button" class="btn-style-two backbtn">Reset</button>--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </form>
            <div class="line-title lightcolor mb-2"><h6>Subscribtion info</h6></div>


            <div class="row settings-subsc">

                @foreach ($branches as $branch)
                <div class="col-lg-6">
                    <div class="settings-subscribe-info">
                        <div class="d-flex align-items-center justify-content-between">
                            <span>{{$branch->username}} Branch</span>
                            <span class="ends">Ends in {{$laundrypackage->end_date ?? 'N/A'}}</span>
                        </div>
                        <p>your Subscription will be auto renewed after the it ends</p>
                        {{-- <div class="d-flex align-items-center justify-content-between">
                            <span><a href="#" class="btn-style-two">Renew Subscription</a></span>
                            <span><a href="#" class="btn-style-two backbtn2">Cancel Subscription</a></span>
                        </div> --}}
                    </div>
                </div>
                @endforeach
                {{-- <div class="col-lg-6">
                    <div class="settings-subscribe-info">
                        <div class="d-flex align-items-center justify-content-between">
                            <span>Riyadh Branch</span>
                            <span class="ends">Ends in 16/9/2022</span>
                        </div>
                        <p>your Subscription will be auto renewed after the it ends</p>
                        <div class="d-flex align-items-center justify-content-between">
                            <span><a href="#" class="btn-style-two">Renew Subscription</a></span>
                            <span><a href="#" class="btn-style-two backbtn2">Cancel Subscription</a></span>
                        </div>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
</div>
