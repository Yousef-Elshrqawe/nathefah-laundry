<!-- ==============CREATE NEW USER MODAL===================== -->
<div class="modal fade" id="edit-branch-info-modal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-body">
                <form action="{{route('laundry.branch-details-update',$branchId)}}" method="post">
                    @csrf
                    @method('PUT')
                    <div class="modal-header p-0 mb-5">
                        <h5 class="left-border">Edit User </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchUserId" class="form-label">Brand info</label>
                                <input type="text" class="form-control" name="username" id="branchUserId" value="{{$branchInfo->username}}" placeholder="Fresh & Clean ">
                                @error('username')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchStatusId" class="form-label">Status</label>
                                <select class="form-select" name="status" id="branchStatusId" aria-label="Default select example">
                                    <option selected disabled>Choose Status</option>
                                    <option value="open" {{$branchInfo->status == "open" ? "selected" : "" }}>Open</option>
                                    <option value="closed" {{$branchInfo->status == "closed" ? "selected" : "" }}>Closed</option>
                                </select>

                                @error('status')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchAddressId" class="form-label">Branch Location</label>
                                <input type="text" class="form-control" name="address" value="{{$branchInfo->address}}" id="branchAddressId" placeholder="Shop No. 700, Gali Kundewalan ">

                                @error('address')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchCountryCodeId" class="form-label">Branch Country Code</label>
                                <input type="text" value="{{$branchInfo->country_code}}" name="country_code" class="form-control" id="branchCountryCodeId" placeholder="56 866 0899">
                                @error('country_code')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchPhoneId" class="form-label">Branch Number</label>
                                <input type="text" name="phone" value="{{$branchInfo->phone}}" class="form-control" id="branchPhoneId" placeholder="56 866 0899">
                                @error('phone')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchOpenTimeId" class="form-label">Branch Number</label>
                                <input type="time" value="{{$branchInfo->open_time}}" name="open_time" class="form-control" id="branchOpenTimeId" placeholder="56 866 0899">
                                @error('phone')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="form-box">
                                <label for="branchCloseTimeId" class="form-label">Branch Number</label>
                                <input type="time" value="{{$branchInfo->closed_time}}" name="closed_time" class="form-control" id="branchCloseTimeId" placeholder="56 866 0899">
                                @error('phone')
                                <p class="alert-danger">{{ $message }}</p>
                                @enderror

                            </div>
                        </div>

                    </div>

                    <div class=" mt-4">
                        <button  type="submit"  class="next btn-style-one" value=""><i class="fas fa-plus"></i> Update Branch</button >
                    </div>


                </form>
            </div>
        </div>
    </div>
</div>
