@extends('laundry_temp')
@section('style')
<link rel="stylesheet" type="text/css" href="{{asset('dashboard/assets/select2/dist/cssselect2.min.css')}}"/>
<link rel="stylesheet" type="text/css" href="{{asset('dashboard/css/filter_multi_select.css')}}"/>
@endsection
@section('content')

        <div class="main-content">

            <div class="page-content">
                <div class="container-fluid">

                    @include('laundry.components.nav')

                    <div class="row">
                        <div class="col-xl-12 mb-4">
                            <div class="inner-title-box">
                                <div>
                                    <h4 class="title-inner"> Branches</h4>
                                </div>
                                <div class="btn-flex">
                                    <a class="btn-style-two border" onclick="createBranch()"><i class="fas fa-plus"></i> Add new Branch</a>
                                </div>

                            </div>
                        </div>
                        <div class="col-lg-12">
							<div class="driverbox-main">
                                @foreach($branches as $item)
								    <div class="driverbox">
									<div class="addbranch-header">
									  	<div class="addbranch-logo"><img src="{{asset('images/small-logo.png')}}" alt=""></div>
										<div class="addbranch-content">
											<p>Fresh &amp; Clean Laundry </p>
										  	<h6>
                                                <a href="{{route('laundry.branch-details',$item->id)}}"> ({{$item->username }}) </a>
                                            </h6>
										</div>
										<span class="dlt"><div class="dropdown btn-dropdown">
                                                <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                </button>
                                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                  <li>
                                                        <a class="dropdown-item" onclick="editBranch({{$item->id}})"><i class="fas fa-edit"></i>Edit</a>
                                                  </li>

                                                    <form action="{{route('laundry.branches.destroy',$item->id)}}" method="POST">
                                                        @csrf
                                                        @method('DELETE')
                                                           <li>
                                                               <button type="submit" class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</button>
                                                           </li>
                                                    </form>

                                                </ul>
                                            </div></span>
									</div>
									<div class="driverbox2">
										<div class="driverbox2-sub1">Branch Balance <strong>{{ $item->balance ?  $item->balance : 0}} SAR</strong></div>
										<div class="driverbox2-sub1">Total Orders<strong>{{ $item->totalOrders ?  $item->totalOrders : 0}}</strong></div>
									</div>
							    </div>
                                @endforeach
							</div>
						</div>

                        <div class="col-xl-12 mb-4">
                            <div class="overview-box">
                                <div class="table-box">
                                    <div>
                                        <h5 class="left-border">Branch Reports</h5>
                                    </div>
                                    <div class="table-box-select">
                                        <div class="d-select-box">
                                            <select class="form-select" id="selectedMonthReports" onclick="branchReport()" aria-label="Default select example">
                                                <option selected>Period last month</option>
                                                <option value="All">All</option>
                                                <option value="1">One</option>
                                                <option value="2">Two</option>
                                                <option value="3">Three</option>
                                            </select>
                                        </div>
                                        <div class="d-select-box">
                                            <select class="form-select" id="selectedBranchId" aria-label="Default select example" onchange="branchReport()">
                                                @foreach($branches as $item)
                                                    <option value="{{$item->id}}">{{$item->username}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="row">
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Delivered Orders</p>
                                            </div>
                                        </div>
                                        <h4 class="cl-light" id="allOrdersReports">{{$reports ? $reports->AllOrders : 0}} </h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Completed Orders</p>
                                            </div>
                                        </div>
                                        <h4 class="cl-light" id="completedOrdersReports">{{$reports ? $reports->completedOrders : 0}} </h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Delayed Orders</p>
                                            </div>
                                        </div>

                                        <h4 class="cl-light" id="inProgressOrdersReports">{{$reports ? $reports->inProgressOrders : 0}}</h4>
                                    </div>
                                </div>
                                <div class="col-xl-3 col-md-6 mb-3">
                                    <div class="overview-box p-v2">
                                        <div class="overlay-txt">
                                            <div>
                                                <p class="cl-gray">Canceled Orders</p>
                                            </div>
                                            <div class="dropdown btn-dropdown">
                                                <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                </button>
                                                <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                  <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                  <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                </ul>
                                            </div>
                                        </div>

                                        <h4 class="cl-light">40</h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="col-xl-12 mb-4">
                            <div class="row">
                                <div class="col-xl-6 col-md-12 mb-3">
                                    <div class="overview-box">
                                        <div class="graph-box">
                                            <div class="row">
                                                <div class="col-xl-5">
                                                    <h5 class="left-border">Branches Finance</h5>
                                                </div>
                                                <div class="col-xl-7">
                                                    <div class="gr-drop-select">
                                                        <div class="d-export-txt">
                                                            <div class="ex-img">
                                                                <img src="images/receipt-item.png" alt="">
                                                            </div>
                                                            <div>
                                                                <p class="cl-gray">Export</p>
                                                            </div>
                                                        </div>
                                                        <div class="d-select-box">
                                                            <select class="form-select" aria-label="Default select example">
                                                                <option selected>Period Last 30 Days</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                        <div class="dropdown btn-dropdown">
                                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                            </button>
                                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                              <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                              <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="images/graph1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="col-xl-6 col-md-12 mb-3">
                                    <div class="overview-box">
                                        <div class="graph-box">
                                            <div class="row">
                                                <div class="col-xl-5 col-md-5">
                                                    <h5 class="left-border">Monthly income</h5>
                                                </div>
                                                <div class="col-xl-7 col-md-7">
                                                    <div class="gr-drop-select">
                                                        <div class="d-export-txt">
                                                            <div class="ex-img">
                                                                <img src="images/receipt-item.png" alt="">
                                                            </div>
                                                            <div>
                                                                <p class="cl-gray">Export</p>
                                                            </div>
                                                        </div>
                                                        <div class="d-select-box">
                                                            <select class="form-select" aria-label="Default select example">
                                                                <option selected>Period Last 30 Days</option>
                                                                <option value="1">One</option>
                                                                <option value="2">Two</option>
                                                                <option value="3">Three</option>
                                                            </select>
                                                        </div>
                                                        <div class="dropdown btn-dropdown">
                                                            <button class="btn-dot dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                                            </button>
                                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                                              <li><a class="dropdown-item" href="#"><i class="fal fa-edit"></i> Edit</a></li>
                                                              <li><a class="dropdown-item" href="#"><i class="far fa-trash-alt"></i> Delete</a></li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <img src="images/graph1.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </section>





    @include('laundry.branches.create')
    @include('laundry.branches.edit')

@endsection

@section('scripts')

<script src="{{asset('dashboard/js/filter-multi-select-bundle.min.js')}}"></script>
<script src="{{asset('dashboard/assets/select2/dist/js/select2.full.min.js')}}"></script>
<script src="{{asset('dashboard/assets/select2/dist/js/select2.min.js')}}"></script>

    <!-- <script>
        $(window).scroll(function(){
          var sticky = $('.page-topbar'),
              scroll = $(window).scrollTop();

          if (scroll >= 100) sticky.addClass('fixedhead');
          else sticky.removeClass('fixedhead');
        });
    </script> -->
    <script src="js/menu.js"></script>
    <script type="text/javascript">
        $("#cssmenu").menumaker({
            title: "",
            format: "multitoggle"
        });
    </script>

<script>
    $(document).ready(function(){

    $("#last-btn").click(function(){
        $("#progressbar").addClass("hide");
    });

    var current_fs, next_fs, previous_fs; //fieldsets
    var opacity;
    var current = 1;
    var steps = $("fieldset").length;

    setProgressBar(current);

    $(".next").click(function(){

         //current_fs = $(this).parent();
        // next_fs = $(this).parent().next();
        current_fs = $(this).closest("fieldset");
        next_fs = $(this).closest("fieldset").next();
        //alert(next_fs);
        //Add Class Active
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the next <fieldset></fieldset>
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                next_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(++current);
    });

    $(".previous").click(function(){

        current_fs = $(this).closest("fieldset");
        previous_fs = $(this).closest("fieldset").prev();

        //Remove class active
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //show the previous fieldset
        previous_fs.show();

        //hide the current fieldset with style
        current_fs.animate({opacity: 0}, {
            step: function(now) {
                // for making fielset appear animation
                opacity = 1 - now;

                current_fs.css({
                    'display': 'none',
                    'position': 'relative'
                });
                previous_fs.css({'opacity': opacity});
            },
            duration: 500
        });
        setProgressBar(--current);
    });

    function setProgressBar(curStep){
        var percent = parseFloat(100 / steps) * curStep;
        percent = percent.toFixed();
        $(".progress-bar")
          .css("width",percent+"%")
    }

    $(".submit").click(function(){
        return false;
    })

    });
    </script>

    <script>
        	$(document).ready(function() {
			$('.minus').click(function () {
				var $input = $(this).parent().find('input');
				var count = parseInt($input.val()) - 1;
				count = count < 0 ? 1 : count;
				$input.val(count);
				$input.change();
				return false;
			});
			$('.plus').click(function () {
				var $input = $(this).parent().find('input');
				$input.val(parseInt($input.val()) + 1);
				$input.change();
				return false;
			});
		});
    </script>

    @include('laundry.branches.scripts')
@endsection
