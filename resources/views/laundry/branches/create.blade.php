<form role="form" action="{{ route('laundry.branches.store') }}" method="post" class="registration-form">
    <!-- Modal -->
    <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    {{-- <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button> --}}
                    <h4 class="modal-title" id="myModalLabel">Add New Branch</h4>
                </div>
                <div class="modal-body">

                    <fieldset style="display: block;">
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createBranchNameId" class="form-label">Branch Name</label>
                                    <input type="text"  name="branch_name" class="form-control required-fielddd"
                                           required
                                        id="createBranchNameId" aria-describedby="emailHelp"
                                        placeholder="ex . Mohamed Ahmed">
                                    @error('branch_name')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createAddressId" class="form-label">Branch Address</label>
                                    <input type="text" required name="address" class="form-control required-fielddd" id="createAddressId"
                                        aria-describedby="emailHelp" placeholder="ex . Cairo Egypt">
                                    @error('address')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createCountryCodeId" class="form-label">Country Code</label>
                                    <input type="text" disabled value="+966"  name="branch_country_code" class="form-control required-fielddd"
                                        id="createCountryCodeId" aria-describedby="emailHelp"
                                        placeholder="Enter Driver Country Code">

                                    @error('branch_country_code')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createPhoneId" class="form-label">Phone Number</label>
                                    <input type="number" required name="branch_phone" class="form-control required-fielddd" id="createPhoneId customer_phone1"
                                        aria-describedby="emailHelp" placeholder="5xxxxxxxx">
                                    <span id="customer_name_error1" style="color:red;"></span>

                                    @error('branch_phone')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createLatitudeId" class="form-label">Latitude</label>
                                    <input type="text" name="lat" required  class="form-control required-fielddd" id="createLatitudeId"
                                        aria-describedby="emailHelp" placeholder="Enter the Latitude">

                                    @error('lat')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createLongitudeId" class="form-label">Longitude</label>
                                    <input type="text" name="long" required  class="form-control required-fielddd" id="createLongitudeId"
                                        aria-describedby="emailHelp" placeholder="Enter the Longitude">
                                    @error('long')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>







                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createBranchStatusId" class="form-label">Branch Status</label>
                                    <select class="form-select" name="status" id="createBranchStatusId"
                                            required
                                        aria-label="Default select example">
                                        <option selected> Choose Branch Status</option>
                                        <option value="open">Open</option>
                                        <option value="closed">Closed</option>
                                    </select>
                                    @error('status')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createBranchPasswordId" class="form-label">Branch Password</label>
                                    <input type="password" required name="branch_password" class="form-control required-fielddd"
                                        id="createBranchPasswordId" aria-describedby="emailHelp"
                                        placeholder="ex . Mohamed Ahmed">
                                    @error('branch_password')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-box">
                                    <label for="createUserCountryCodeId" class="form-label">location</label>
                                    <div id="map" style="height: 500px; width: 100%"></div>

                                </div>
                            </div>


                        </div>
                    </fieldset>

                </div>
                <div class="modal-footer">
                    <button type="button" id="prev_putt" disabled class="btn btn-default btn-prev">Prev</button>
                    <button type="button" id="next_putt"  class="btn btn-default btn-next">Next</button>
                </div>
            </div>
        </div>
    </div>
    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal2">
        u can hide me
    </button>
    <!-- Modal -->
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add Admin Branch</h4>
                </div>
                <div class="modal-body">
                    <fieldset>
                        <div class="row">

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createUserNameId" class="form-label">User Name</label>
                                    <input type="text" required name="username" class="form-control" id="createUserNameId"
                                        aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed">
                                    @error('username')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>



                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createUserPasswordId" class="form-label">User Password</label>
                                    <input type="password" required name="password" class="form-control"
                                        id="createUserPasswordId" aria-describedby="emailHelp"
                                        placeholder="ex . Mohamed Ahmed">
                                    @error('password')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createBranchPhoneId" class="form-label">Phone</label>
                                    <input type="number" required name="user_phone" class="form-control"
                                        id="createBranchPhoneId customer_phone2" aria-describedby="emailHelp"
                                        placeholder="ex . Mohamed Ahmed">
                                    <span id="customer_name_error2" style="color:red;"></span>

                                    @error('user_phone')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="createUserCountryCodeId" class="form-label">Country Code</label>
                                    <input type="text" disabled value="+966" name="user_country_code" class="form-control"
                                        id="createUserCountryCodeId" aria-describedby="emailHelp"
                                        placeholder="Enter Driver Country Code">

                                    @error('user_country_code')
                                        <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>



                            {{--                        <div class="col-md-6"> --}}
                            {{--                            <div class="form-box"> --}}
                            {{--                                <label for="createUserPhoneId" class="form-label">Phone Number</label> --}}
                            {{--                                <input type="text" name="user_phone" class="form-control" id="createUserPhoneId" aria-describedby="emailHelp" placeholder="Enter the Phone Number"> --}}

                            {{--                                @error('user_phone') --}}
                            {{--                                <p class="alert-danger">{{ $message }}</p> --}}
                            {{--                                @enderror --}}

                            {{--                            </div> --}}
                            {{--                        </div> --}}


                        </div>
                    </fieldset>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-prev">Prev</button>
                    <button type="submit" class="btn btn-default">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>
<style>
    .form-box input:invalid, .form-box select:invalid {
        border: 2px solid red;
    }
    .btn:disabled {
        background-color: #ccc;
        cursor: not-allowed;
    }
</style>

<!-- JavaScript -->
<script>
    document.addEventListener("DOMContentLoaded", function () {
        const nextButton = document.getElementById("next_putt");
        const inputs = document.querySelectorAll(".required-fielddd");

        function checkInputs() {
            let allFilled = true;
            inputs.forEach(input => {
                if (input.value.trim() === "" || input.value === "Choose Branch Status") {
                    allFilled = false;
                }
            });
            nextButton.disabled = !allFilled;
        }



        inputs.forEach(input => {
            input.addEventListener("input", checkInputs);
            input.addEventListener("change", checkInputs);
        });

        checkInputs();
    });
</script>
