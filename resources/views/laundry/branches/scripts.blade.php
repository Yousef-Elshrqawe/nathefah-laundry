<script>
    function createBranch() {
        $('#myModal1').modal('show');
    }

    $("div[id^='myModal']").each(function() {

        var currentModal = $(this);

        //click next
        currentModal.find('.btn-next').click(function() {
            currentModal.modal('hide');
            currentModal.closest("div[id^='myModal']").nextAll("div[id^='myModal']").first().modal(
                'show');
        });

        //click prev
        currentModal.find('.btn-prev').click(function() {
            currentModal.modal('hide');
            currentModal.closest("div[id^='myModal']").prevAll("div[id^='myModal']").first().modal(
                'show');
        });

    });

    $("div[id^='editBranch']").each(function() {

        var currentModal = $(this);

        //click next
        currentModal.find('.btn-next').click(function() {
            currentModal.modal('hide');
            currentModal.closest("div[id^='editBranch']").nextAll("div[id^='editBranch']").first()
                .modal('show');
        });

        //click prev
        currentModal.find('.btn-prev').click(function() {
            currentModal.modal('hide');
            currentModal.closest("div[id^='editBranch']").prevAll("div[id^='editBranch']").first()
                .modal('show');
        });

    });

    function editBranch(branchId) {
        $('#editBranch').modal('show');

        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{ route('laundry.branches.edit') }}",
            data: {
                branchId: branchId,
            },
            beforeSend: function() {},
            success: function(response) {
                console.log(response.data)
                $("#editBranchId").val(branchId)
                $("#editBranchNameId").val(response.data.username)
                $("#editAddressId").val(response.data.address)
                $("#editCountryCodeId").val(response.data.country_code)
                $("#editPhoneId").val(response.data.phone)
                $("#editLatitudeId").val(response.data.lat)
                $("#editLongitudeId").val(response.data.long)
                $("#editOpenTimeId").val(response.data.open_time)
                $("#editClosedTimeId").val(response.data.closed_time)
                $("#editBranchStatusId").val(response.data.status)

                var latitude = $('#editLatitudeId').val();
                var longitude = $('#editLongitudeId').val();
                console.log(latitude + " " + longitude)
                setTimeout(function () {
                    var center = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude));

                    map = new google.maps.Map(document.getElementById("editMap"), {
                        zoom: 15,
                        center: center,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    });

                    if (typeof marker !== 'undefined' && marker) {
                        // إذا كان الماركر مُعرّفًا
                        marker.setMap(null); // إزالته من الخريطة
                    }


                    marker = new google.maps.Marker({
                        position: center,
                        map: map,
                        draggable: true
                    });

                    // تحديث الإحداثيات عند سحب الماركر
                    google.maps.event.addListener(marker, 'dragend', function(event) {
                        $('#editLatitudeId').val(event.latLng.lat());
                        $('#editLongitudeId').val(event.latLng.lng());
                    });

                }, 500); // تأخير بسيط لضمان فتح المودال بالكامل قبل تحميل الخريطة


            },
            complete: function() {

            },
            error: function(error) {

            }

        });

    }


    function branchReport() {
        let branchId = $("#selectedBranchId").val();
        let month = $("#selectedMonthReports").val();
        if (!branchId) {
            alert("Choose branch!");
        }
        $.ajax({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            type: "GET",
            url: "{{ route('laundry.branches.reports') }}",
            data: {
                branchId: branchId,
                month: month
            },
            beforeSend: function() {},
            success: function(response) {
                console.log(response)
                $("#allOrdersReports").html(response.data.AllOrders)
                $("#completedOrdersReports").html(response.data.completedOrders)
                $("#inProgressOrdersReports").html(response.data.inProgressOrders)

            },
            complete: function() {

            },
            error: function(error) {

            }

        });
    }
</script>
@if (Session::get('errors') != null)
    <script>
        $(document).ready(function() {
            $('#myModal1').modal('show');
        });
    </script>
@endif

<script>
    $("#pac-input").focusin(function() {
        $(this).val('');
    });

    $('#createLatitudeId').val('');
    $('#createLongitudeId').val('');


    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.

    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

    function initAutocomplete() {
        var map = new google.maps.Map(document.getElementById('map'), {
            center: {
                lat: 24.740691,
                lng: 46.6528521
            },
            zoom: 13,
            mapTypeId: 'roadmap'
        });

        // move pin and current location
        infoWindow = new google.maps.InfoWindow;
        geocoder = new google.maps.Geocoder();
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude,
                };
                map.setCenter(pos);
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pos),
                    map: map,
                    title: 'موقعك الحالي'
                });

                //اضافه  الموقع  الحالي  الي  الحقل
                $('#createLatitudeId').val(pos.lat);
                $('#createLongitudeId').val(pos.lng);
                markers.push(marker);
                marker.addListener('click', function() {
                    geocodeLatLng(geocoder, map, infoWindow, marker);
                });
                // to get current position address on load
                google.maps.event.trigger(marker, 'click');
            }, function() {
                handleLocationError(true, infoWindow, map.getCenter());
            });
        } else {
            // Browser doesn't support Geolocation
            console.log('dsdsdsdsddsd');
            handleLocationError(false, infoWindow, map.getCenter());
        }

        var geocoder = new google.maps.Geocoder();
        google.maps.event.addListener(map, 'click', function(event) {
            SelectedLatLng = event.latLng;
            geocoder.geocode({
                'latLng': event.latLng
            }, function(results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                        deleteMarkers();
                        addMarkerRunTime(event.latLng);
                        SelectedLocation = results[0].formatted_address;
                        console.log(results[0].formatted_address);
                        splitLatLng(String(event.latLng));
                        $("#pac-input").val(results[0].formatted_address);

                    }
                }
            });
        });

        function geocodeLatLng(geocoder, map, infowindow, markerCurrent) {
            var latlng = {
                lat: markerCurrent.position.lat(),
                lng: markerCurrent.position.lng()
            };
            /* $('#branch-latLng').val("("+markerCurrent.position.lat() +","+markerCurrent.position.lng()+")");*/
            $('#createLatitudeId').val(markerCurrent.position.lat());
            $('#createLongitudeId').val(markerCurrent.position.lng());

            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                if (status === 'OK') {
                    if (results[0]) {
                        map.setZoom(8);
                        var marker = new google.maps.Marker({
                            position: latlng,
                            map: map
                        });
                        markers.push(marker);
                        infowindow.setContent(results[0].formatted_address);
                        SelectedLocation = results[0].formatted_address;
                        $("#pac-input").val(results[0].formatted_address);


                        infowindow.open(map, marker);
                    } else {
                        window.alert('No results found');
                    }
                } else {
                    window.alert('Geocoder failed due to: ' + status);
                }
            });
            SelectedLatLng = (markerCurrent.position.lat(), markerCurrent.position.lng());
        }

        function addMarkerRunTime(location) {
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            markers.push(marker);
        }

        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        function clearMarkers() {
            setMapOnAll(null);
        }

        function deleteMarkers() {
            clearMarkers();
            markers = [];
        }

        // Create the search box and link it to the UI element.
        var input = document.getElementById('pac-input');
        $("#pac-input").val("أبحث هنا ");
        var searchBox = new google.maps.places.SearchBox(input);
        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(input);

        // Bias the SearchBox results towards current map's viewport.
        map.addListener('bounds_changed', function() {
            searchBox.setBounds(map.getBounds());
        });

        var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener('places_changed', function() {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            // Clear out the old markers.
            markers.forEach(function(marker) {
                marker.setMap(null);
            });
            markers = [];

            // For each place, get the icon, name and location.
            var bounds = new google.maps.LatLngBounds();
            places.forEach(function(place) {
                if (!place.geometry) {
                    console.log("Returned place contains no geometry");
                    return;
                }
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(100, 100),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                // Create a marker for each place.
                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));


                $('#createLatitudeId').val(place.geometry.location.lat());
                $('#createLongitudeId').val(place.geometry.location.lng());

                if (place.geometry.viewport) {
                    // Only geocodes have viewport.
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    }

    function handleLocationError(browserHasGeolocation, infoWindow, pos) {
        infoWindow.setPosition(pos);
        infoWindow.setContent(browserHasGeolocation ?
            'Error: The Geolocation service failed.' :
            'Error: Your browser doesn\'t support geolocation.');
        infoWindow.open(map);
    }

    function splitLatLng(latLng) {
        var newString = latLng.substring(0, latLng.length - 1);
        var newString2 = newString.substring(1);
        var trainindIdArray = newString2.split(',');
        var lat = trainindIdArray[0];
        var Lng = trainindIdArray[1];

        $("#createLatitudeId").val(lat);
        $("#createLongitudeId").val(Lng);

        console.log(lat);
        console.log(Lng);
    }
</script>




<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBljsOSEkY07FkBuA6p0DmtOcE64VW-rfE&libraries=places&callback=initAutocomplete&language={{ app()->getLocale() }}
     async defer"></script>
