

<form role="form" action="{{route('laundry.branches.update')}}" method="post" class="registration-form">
    @csrf
    @method('PUT')
    <!-- Modal -->
    <div class="modal fade" id="editBranch" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add New Branch</h4>
                </div>
                <div class="modal-body">

                    <fieldset style="display: block;">
                        <div class="row">
                            <input type="hidden" id="editBranchId" name="id">
                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editBranchNameId" class="form-label">Branch Name</label>
                                    <input type="text" name="branch_name" class="form-control" id="editBranchNameId" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed">
                                    @error('branch_name')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editAddressId" class="form-label">Branch Address</label>
                                    <input type="text" name="address" class="form-control" id="editAddressId" aria-describedby="emailHelp" placeholder="ex . Cairo Egypt">
                                    @error('address')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editCountryCodeId" class="form-label">Country Code</label>
                                    <input type="text" name="branch_country_code" class="form-control" id="editCountryCodeId" aria-describedby="emailHelp" placeholder="Enter Driver Country Code">

                                    @error('branch_country_code')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editPhoneId" class="form-label">Phone Number</label>
                                    <input type="text" name="branch_phone" class="form-control" id="editPhoneId" aria-describedby="emailHelp" placeholder="5xxxxxxxx">

                                    @error('branch_phone')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editLatitudeId" class="form-label">Latitude</label>
                                    <input type="text" name="lat" class="form-control"  id="editLatitudeId" aria-describedby="emailHelp" placeholder="Enter the Latitude">

                                    @error('lat')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editLongitudeId" class="form-label">Longitude</label>
                                    <input type="text" name="long" class="form-control" id="editLongitudeId" aria-describedby="emailHelp" placeholder="Enter the Longitude">
                                    @error('long')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editOpenTimeId" class="form-label">Open Time</label>
                                    <input type="time" name="open_time" class="form-control" id="editOpenTimeId" aria-describedby="emailHelp" placeholder="Enter the Branch Open Time">

                                    @error('open_time')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editClosedTimeId" class="form-label">Closed Time</label>
                                    <input type="time" name="closed_time" class="form-control" id="editClosedTimeId" aria-describedby="emailHelp" placeholder="Enter the Longitude">
                                    @error('closed_time')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editBranchStatusId" class="form-label">Branch Status</label>
                                    <select class="form-select" name="status" id="editBranchStatusId" aria-label="Default select example">
                                        <option value="open">Open</option>
                                        <option value="closed">Closed</option>
                                    </select>
                                    @error('status')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-box">
                                    <label for="editBranchPasswordId" class="form-label">Branch Password</label>
                                    <input type="password" name="branch_password" class="form-control" id="editBranchPasswordId" aria-describedby="emailHelp" placeholder="ex . Mohamed Ahmed">
                                    @error('branch_password')
                                    <p class="alert-danger">{{ $message }}</p>
                                    @enderror

                                </div>
                            </div>


                            <div class="col-md-12">
                                <div class="form-box">
                                    <label for="editUserCountryCodeId" class="form-label">location</label>
                                    <div id="editMap" style="height: 500px; width: 100%"></div>

                                </div>
                            </div>



                        </div>
                    </fieldset>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-prev">close</button>
                    <button type="submit" class="btn btn-default">Save</button>
                </div>
            </div>
        </div>
    </div>
</form>

