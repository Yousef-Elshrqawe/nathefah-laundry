<div class="row">
    <div class="col-xl-12 mb-4">
        <div class="page-topbar">
            <div class="overview-box">
                <div class="row align-items-center flex-column-reverse flex-xl-row">
                    <div class="col-xl-4 col-md-12">
                        <div class="top-bar-title">
                            <h5>Welcome  {{Auth::guard('laundry')->user()->name}} </h5>
                            <p class="cl-gray">Here is what happened in the branches today</p>
                        </div>
                    </div>

                    @php
                        $cur_route = Route::current()->getName();

                        $currentUrl = url()->current();
                        $urlParts   = explode('/',$currentUrl);

                        if($cur_route == "laundry.home")
                            {
                                $url = route('laundry.home',$urlParts[5]);
                                $nameUrl = "customer name";
                            }elseif($cur_route == "laundry.orders"){
                                $url = route('laundry.orders',[$urlParts[5],$urlParts[6],$urlParts[7]]);
                                $nameUrl = "customer name";
                            }elseif ($cur_route == "laundry.services"){
                                    $url = route('laundry.services');
                                    $nameUrl = "service name";
                            }elseif ($cur_route == "laundry.drivers"){
                                    $url = route('laundry.drivers');
                                    $nameUrl = "driver name";
                            }elseif ($cur_route == 'laundry.setting'){
                                    $url = route('laundry.setting');
                                    $nameUrl = "setting";
                            }elseif($cur_route == 'laundry.branches'){
                                     $url = route('laundry.branches');
                                        $nameUrl = "branch name";
                            }else{
                                $url = "";
                                $nameUrl = "";
                            }

                    @endphp

                    <div class="col-xl-4 col-md-12">
                        <div class="search-box">
                            <form action="{{$url}}">
                                <input type="text" name="search" id="" class="form-control" placeholder="{{$nameUrl}}">
                                <i class="fal fa-search" style="cursor: pointer" onclick='this.parentNode.submit(); return false;'></i>
                            </form>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-12">
                        <div class="notify-box">
                          {{--  <div class="notify-bell">
                                <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                            </div>--}}
                            <div>
                                {{-- <p>Sun 24 July 4:35 PM</p> --}}
                                <p>{{now()->format('l  m-d')}}</p>
                            </div>
                            <div>
                                <a href="{{route('laundry.logout')}}">log out</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
