<?php
$active_links = ['deliveryfees' , ''];
?>

@extends('laundry_temp')
@section('content')
    <div class="main-content">

        <div class="page-content">
            <div class="container-fluid">

                <div class="row">
                    <div class="col-xl-12 mb-4">
                        <div class="page-topbar">
                            <div class="overview-box">
                                <div class="row align-items-center flex-column-reverse flex-xl-row">
                                    <div class="col-xl-4 col-md-12">
                                        <div class="top-bar-title">
                                            <h5>Welcome  {{Auth::guard('laundry')->user()->name}} </h5>
                                            <p class="cl-gray">Here is what happened in the branches today</p>
                                        </div>
                                    </div>

                                    <div class="col-xl-4 col-md-12">
                                        <div class="notify-box">
                                            {{--  <div class="notify-bell">
                                                  <p><a href="#"><i class="fal fa-bell"></i>Notifications</a></p>
                                              </div>--}}
                                            <div>
                                                {{-- <p>Sun 24 July 4:35 PM</p> --}}
                                                <p>{{now()->format('l  m-d')}}</p>
                                            </div>
                                            <div>
                                                <a href="{{route('laundry.logout')}}">log out</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">

                    <div class="col-xl-12">
                        <div class="overview-box">
                            <div class="table-box">
                                <div class="tran-flex">
                                    <h5 class="left-border">@lang('admin.delivery_fees')</h5>
                                </div>
                                <div class="d-selct-view">
                                    {{-- <a href="#" class="btn-style-one" data-bs-toggle="modal" data-bs-target="#new-category-modal">
                                        <i class="fas fa-plus"></i>@lang('admin.delivery_fees')
                                    </a> --}}
                                </div>
                            </div>


                            <div class="table_responsive_maas v2">
                                <table class="table" width="100%">
                                    <thead>
                                    <tr>
                                        <th><span class="th-head-icon">#</span></th>
                                        <th><span class="th-head-icon">@lang('admin.name')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.price')</span></th>
                                        <th><span class="th-head-icon">@lang('admin.operations')</span></th>
                                        <th>&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @isset($delivery_fees)

                                            <tr>
                                                <td> 1</td>
                                                <td class="text-info">one way delivery</td>
                                                <td class="text-info">{{$delivery_fees->one_way_delivery_fees}}</td>
                                                <td style="display: -webkit-inline-box;">

                                                    <button  class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-delivery_fees-modal"
                                                          data-id="1"    data-price="{{$delivery_fees->one_way_delivery_fees}}"
                                                             data-type="one way delivery">
                                                        @lang('admin.edit')
                                                    </button>



                                                </td>

                                            </tr>
                                            <tr>
                                                <td> 2</td>
                                                <td class="text-info">door to door delivery</td>
                                                <td class="text-info">{{$delivery_fees->door_to_door_delivery_fees}}</td>
                                                <td style="display: -webkit-inline-box;">

                                                    <button  class="btn btn-success" data-bs-toggle="modal" data-bs-target="#edit-delivery_fees-modal"
                                                    data-id="2"    data-price="{{$delivery_fees->door_to_door_delivery_fees}}"
                                                             data-type="door to door delivery">
                                                        @lang('admin.edit')
                                                    </button>



                                                </td>

                                            </tr>
                                    @endisset
                                    </tbody>
                                </table>
                                <div class="justify-content-center d-flex">
{{--                                    {!! $categories->appends(Request::except('page'))->render() !!}--}}
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>




@include('laundry.deliveryfees.edit_delivery_fees')


@endsection


@section('scripts')

    @if (Session::get('errors')!=null)
        <script>
            $(document).ready(function() {
                $('#new-item-modal').modal('show');
            });
        </script>
    @endif


    <script>
        $('#edit-delivery_fees-modal').on('show.bs.modal', function(event) {
            let button = $(event.relatedTarget)
            let id     = button.data('id')
            let type = button.data('type')
            let price = button.data('price')

            let modal = $(this)
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #type').html(type);
            modal.find('.modal-body #price').val(price);
        })
    </script>

@endsection
