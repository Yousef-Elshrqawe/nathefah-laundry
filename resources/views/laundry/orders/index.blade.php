@extends('laundry_temp')
@section('style')
@include('components.styles.map')
@endsection

@section('content')
<div class="main-content">
    <div class="page-content pb-3">
        <div class="container-fluid">
            @include('laundry.components.nav')
            <div class="row">
                <div class="col-xl-12 mb-4">
                    <div class="inner-title-box">
                        <div>
                            <h4 class="title-inner"> Orders & Reports</h4>
                        </div>
                        <div class="btn-flex">
                            {{-- <a href="#" class="btn-style-one lightblue">Deliver an Order</a> --}}
                             {{-- <a href="#" class="btn-style-one lightblue ">Receive an Order</a> --}}
                            <a href="#" class="btn-style-two border" data-bs-toggle="modal" data-bs-target="#new-order-modal"><i class="fas fa-plus"></i> Create Order</a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-12 mb-4">
                    <div class="row">
                        <div class="col-xl-6 col-md-12 mb-4">
                            <div class="inner-title-box">
                                <div>
                                    <h4 class="title-inner">Orders Completed</h4>
                                    <p class="title-inner">Total Orders Completed: {{ $orders->count() }}</p>

                                    @php
                                        $total_payment_cash_after_discount = $orders
                                            ->where('pymenttype_id', 2)
                                            ->where('price_after_discount', '!=', null)
                                            ->sum('price_after_discount');
                                        $total_payment_cash_before_discount = $orders
                                            ->where('pymenttype_id', 2)
                                            ->where('price_after_discount', null)
                                            ->sum('price_before_discount');
                                        $total_payment_cash =
                                            $total_payment_cash_after_discount +
                                            $total_payment_cash_before_discount;
                                        $total_payment_wallet_after_discount = $orders
                                            ->where('pymenttype_id', 1)
                                            ->where('price_after_discount', '!=', null)
                                            ->sum('price_after_discount');
                                        $total_payment_wallet_before_discount = $orders
                                            ->where('pymenttype_id', 1)
                                            ->where('price_after_discount', null)
                                            ->sum('price_before_discount');
                                        $total_payment_wallet =
                                            $total_payment_wallet_after_discount +
                                            $total_payment_wallet_before_discount;
                                        // $total_payment_online = $orders->where('pymenttype_id', 3)->where('price_after_discount', '!=', null)->sum('price_after_discount');
                                        // $total_payment_online_before_discount = $orders->where('pymenttype_id', 3)->where('price_after_discount', null)->sum('price_before_discount');
                                        // $total_payment_online = $total_payment_online + $total_payment_online_before_discount;
                                        $total_payment_online = 0;
                                    @endphp
                                    <p class="title-inner">Total Cash Payment: {{ $total_payment_cash }}</p>
                                    <p class="title-inner">Total Wallet Payment: {{ $total_payment_wallet }}</p>
                                    {{-- <p class="title-inner">online Payment: {{ $total_payment_online }}</p> --}}
                                    @foreach ($payment_methods as $payment_method)
                                        @php
                                            $total_payment_method_after = $orders
                                                ->where('payment_method_id', $payment_method->id)
                                                ->where('price_after_discount', '!=', null)
                                                ->sum('price_after_discount');
                                            $total_payment_method_before_discount = $orders
                                                ->where('payment_method_id', $payment_method->id)
                                                ->where('price_after_discount', null)
                                                ->sum('price_before_discount');
                                            $total_payment_method =
                                                $total_payment_method_after + $total_payment_method_before_discount;
                                            $total_payment_online += $total_payment_method;
                                        @endphp
                                        <p class="title-inner">Total {{ $payment_method->translations[0]->name }}
                                            Payment: {{ $total_payment_method }}</p>
                                    @endforeach


                                    {{-- @php

                                    @endphp
                                    <p class="title-inner">Nathefah Commission: </p> --}}
                                </div>

                            </div>
                        </div>
                        <div class="col-xl-6 col-md-12 mb-4">
                            <div class="inner-title-box">
                                <div>

                                    @php
                                        $price =
                                            $total_payment_cash + $total_payment_wallet + $total_payment_online;
                                        $adminCommission =
                                            (DB::table('application_rates')->first()->rate * $price) / 100;
                                    @endphp
                                    <p class="title-inner">Admin Commission: {{ round($adminCommission, 2) }}</p>
                                    <p class="title-inner">Laundry dues: {{ round($price - $adminCommission, 2) }}
                                    </p>


                                    <p class="title-inner">Total Price: {{ $price }}</p>

                                    {{-- @php

                                    @endphp
                                    <p class="title-inner">Nathefah Commission: </p> --}}
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-xl-12 mb-4">
                    <div class="overview-box">
                        <div class="table-box">
                            <div class="tab-flex">
                                <div>
                                    <h5 class="left-border">Orders</h5>
                                </div>
                                <div class="tab-box">
                                    <ul class="nav nav-tabs">
                                        <li class="nav-item">
                                            <a class="nav-link  @if($type=='indelivery') active @endif"  href="{{route('laundry.orders',['indelivery',$orderdate,$datereview])}}">In Delivery ( {{$indelivery}} )</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='inprogress') active @endif"   href="{{route('laundry.orders',['inprogress',$orderdate,$datereview])}}">in Progress ( {{$inprogresscount}} )</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='completed') active @endif"  href="{{route('laundry.orders',['completed',$orderdate,$datereview])}}">Completed ( {{$completed}} )</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='unassigned') active @endif"      href="{{route('laundry.orders',['unassigned',$orderdate,$datereview])}}">unassigned ( {{$unassigned}} )</a>
                                        </li>

                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='new') active @endif"      href="{{route('laundry.orders',['new',$orderdate,$datereview])}}">new ( {{$neworder}} )</a>
                                        </li>


                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='one_way_delivery') active @endif"      href="{{route('laundry.orders',['one_way_delivery',$orderdate,$datereview])}}">one way delivery  ( {{$onewayorders}} )</a>
                                        </li>


                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='self_delivery') active @endif"      href="{{route('laundry.orders',['self_delivery',$orderdate,$datereview])}}">Self Delivery  ( {{$self_delivery_orders}} )</a>
                                        </li>



                                        <li class="nav-item">
                                            <a class="nav-link @if($type=='all') active @endif"      href="{{route('laundry.orders',['all',$orderdate,$datereview])}}">All ( {{$all}} )</a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="table-box-select">
                                <div class="d-select-box">
                                    <select id="selectdate" class="form-select" aria-label="Default select example">
                                        <option selected    @if($orderdate=='all') selected @endif value="all">Sort by date</option>
                                        <option selected    @if($orderdate=='all') selected @endif value="all">All</option>
                                        <option value="7"   @if($orderdate==7) selected @endif>One week</option>
                                        <option value="14"  @if($orderdate==14) selected @endif>Two week</option>
                                        <option value="21"  @if($orderdate==21) selected @endif>Three week</option>
                                    </select>
                                </div>

                                <form action="{{route('laundry.orders',[$type,'all','all'])}}" method="GET">
                                    <div class="d-select-box">
                                        <select class="form-select" name="branch_id" aria-label="Default select example" onchange="this.form.submit();">
                                            {{-- <option selected disabled>Branch Riyadh</option> --}}
                                            @foreach($branches as $branch)
                                                <option value="{{$branch->id}}"  @if($branch->id == $branch_id) selected @endif>{{$branch->username}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>

                                <div class="d-selct-view">
                                    <a href="{{route('laundry.allorder')}}" class="cl-light" type="button">View All</a>
                                </div>
                            </div>
                        </div>
                        @include('components.orders.ordertable',[$orders,$porttype])
                    </div>
                </div>
                <div class="justify-content-center d-flex">
                    {!! $orders->appends(Request::except('page'))->render() !!}
                </div>
                {{-- Start Reports --}}
                @include('components.orders.reports',['reports'=>$reports])

                    <div class="col-lg-4 col-md-12">
                        <div class="whitebox-panel">
                          <div class="whitebox-panel-header">
                            <h5 class="left-border">Latest Orders</h5>
                            <a href="{{route('laundry.allorder')}}" class="viewall" type="button" >View All </a>
                          </div>
                          <div class="whitebox-panel-body">
                            <div class="table_responsive_maas v2 pt-0">
                                @include('components.orders.latestordertable',['latestorders'=>$latestorders])
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                </div>


                </div>
            </div>
        </div>
    </div>

<!--=====================Modal orders===========-->
<div class="modal fade" id="viewall-modal" tabindex="-1" aria-labelledby="viewall-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <div class="table-box">
                <div class="tab-flex">
                    <div>
                        <h5 class="left-border">Orders</h5>
                    </div>
                    <div class="tab-box">
                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-bs-toggle="tab" href="#tabdelmodal">In Delivery ( 12 )</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#tabpromodal">in Progress ( 12 )</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#tabcommodal">Completed ( 12 )</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-bs-toggle="tab" href="#taballmodal">All ( 12 )</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-box-select">
                    <div class="d-select-box">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Sort by date</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                    <div class="d-select-box">
                        <select class="form-select" aria-label="Default select example">
                            <option selected>Branch Riyadh</option>
                            <option value="1">One</option>
                            <option value="2">Two</option>
                            <option value="3">Three</option>
                        </select>
                    </div>
                </div>
            </div>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <div class="tab-content">
                <div class="tab-pane active" id="tabdelmodal">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="table_responsive_maas">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">Order number</th>
                                            <th><span class="th-head-icon">Customer name </th>
                                            <th><span class="th-head-icon">Delivery time </th>
                                            <th><span class="th-head-icon">Arrival Date </th>
                                            <th><span class="th-head-icon">Order Status </th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>426</td>
                                            <td>Bessie Cooper</td>
                                            <td>12 hours ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>In Progress</td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>740</td>
                                            <td>Cody Fisher</td>
                                            <td>22 hours ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>In Progress</td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>492</td>
                                            <td>Annette Black</td>
                                            <td>2 minutes ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>In Progress</td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>177</td>
                                            <td>Darrell Steward</td>
                                            <td>2 hours ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>In Progress</td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>540</td>
                                            <td>Cameron Williamson</td>
                                            <td>3 days ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td class="cl-red">Delayed </td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>798</td>
                                            <td>Marvin McKinney</td>
                                            <td>10 minutes ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>In Progress</td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>429</td>
                                            <td>Arlene McCoy</td>
                                            <td>3 days ago</td>
                                            <td>Dec 7, 2019 23:26</td>
                                            <td>In Progress</td>
                                            <td><a href="#" class="cl-light" data-bs-toggle="modal" data-bs-target="#order-details-modal">View Order</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="tabpromodal">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="table_responsive_maas">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">Order number 2</th>
                                            <th><span class="th-head-icon">Customer name </th>
                                            <th><span class="th-head-icon">Delivery time </th>
                                            <th><span class="th-head-icon">Arrival Date </th>
                                            <th><span class="th-head-icon">Order Status </th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>426</td>
                                            <td>Bessie Cooper</td>
                                            <td>12 hours ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>740</td>
                                            <td>Cody Fisher</td>
                                            <td>22 hours ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>492</td>
                                            <td>Annette Black</td>
                                            <td>2 minutes ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>177</td>
                                            <td>Darrell Steward</td>
                                            <td>2 hours ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>540</td>
                                            <td>Cameron Williamson</td>
                                            <td>3 days ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>798</td>
                                            <td>Marvin McKinney</td>
                                            <td>10 minutes ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>429</td>
                                            <td>Arlene McCoy</td>
                                            <td>3 days ago</td>
                                            <td>Dec 7, 2019 23:26</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="tabcommodal">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="table_responsive_maas">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">Order number 3</th>
                                            <th><span class="th-head-icon">Customer name </th>
                                            <th><span class="th-head-icon">Delivery time </th>
                                            <th><span class="th-head-icon">Arrival Date </th>
                                            <th><span class="th-head-icon">Order Status </th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>426</td>
                                            <td>Bessie Cooper</td>
                                            <td>12 hours ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>740</td>
                                            <td>Cody Fisher</td>
                                            <td>22 hours ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>492</td>
                                            <td>Annette Black</td>
                                            <td>2 minutes ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>177</td>
                                            <td>Darrell Steward</td>
                                            <td>2 hours ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>540</td>
                                            <td>Cameron Williamson</td>
                                            <td>3 days ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>798</td>
                                            <td>Marvin McKinney</td>
                                            <td>10 minutes ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>429</td>
                                            <td>Arlene McCoy</td>
                                            <td>3 days ago</td>
                                            <td>Dec 7, 2019 23:26</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="tab-pane " id="taballmodal">
                    <div class="row">
                        <div class="col-md-12">

                            <div class="table_responsive_maas">
                                <table class="table" width="100%">
                                    <thead>
                                        <tr>
                                            <th><span class="th-head-icon">Order number 4</th>
                                            <th><span class="th-head-icon">Customer name </th>
                                            <th><span class="th-head-icon">Delivery time </th>
                                            <th><span class="th-head-icon">Arrival Date </th>
                                            <th><span class="th-head-icon">Order Status </th>
                                            <th>&nbsp;</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>426</td>
                                            <td>Bessie Cooper</td>
                                            <td>12 hours ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>740</td>
                                            <td>Cody Fisher</td>
                                            <td>22 hours ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>492</td>
                                            <td>Annette Black</td>
                                            <td>2 minutes ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>177</td>
                                            <td>Darrell Steward</td>
                                            <td>2 hours ago</td>
                                            <td>Feb 2, 2019 19:28</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>540</td>
                                            <td>Cameron Williamson</td>
                                            <td>3 days ago</td>
                                            <td>Dec 4, 2019 21:42</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>798</td>
                                            <td>Marvin McKinney</td>
                                            <td>10 minutes ago</td>
                                            <td>Dec 30, 2019 05:18</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                        <tr>
                                            <td>429</td>
                                            <td>Arlene McCoy</td>
                                            <td>3 days ago</td>
                                            <td>Dec 7, 2019 23:26</td>
                                            <td>Order Status</td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
</div>
<!--=================ORDER DETAILS MODAL================= -->
<div class="modal fade" id="order-details-modal" tabindex="-1" aria-labelledby="order-details-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h5>Order Details</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>

        <div class="modal-body" id="order_body">

        </div>
      </div>
    </div>
</div>


<!--=================ORDER Scan MODAL================= -->
<div class="modal fade" id="order-scan-modal" tabindex="-1" aria-labelledby="order-scan-modal" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
            <h5>Scan Order</h5>
            <div id="qr_code">

            </div>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body" id="order_body">

        </div>
      </div>
    </div>
</div>
<!-- ==============ENd scan ORDER MODAL===================== -->
<!-- ==============CREATE NEW ORDER MODAL===================== -->
@include('components.orders.create',[$services,$porttype])

</div>
@endsection
@section('scripts')

{{-- google maps --}}
@include('components.Scripts.map')
{{-- create order --}}
@include('components.Scripts.createorder')
{{-- End create order --}}

<script src="js/menu.js"></script>

<script>
$(document).ready(function(){
$("#last-btn").click(function(){
    $("#progressbar").addClass("hide");
});
var current_fs, next_fs, previous_fs; //fieldsets
var opacity;
var current = 1;
var steps = $("fieldset").length;
setProgressBar(current);
$(".next").click(function(){
     //current_fs = $(this).parent();
    // next_fs = $(this).parent().next();
    current_fs = $(this).closest("fieldset");
    next_fs = $(this).closest("fieldset").next();
    //alert(next_fs);
    //Add Class Active
    $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the next <fieldset></fieldset>
    next_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            next_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(++current);
});
$(".previous").click(function(){
    current_fs = $(this).closest("fieldset");
    previous_fs = $(this).closest("fieldset").prev();
    //Remove class active
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");
    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({opacity: 0}, {
        step: function(now) {
            // for making fielset appear animation
            opacity = 1 - now;

            current_fs.css({
                'display': 'none',
                'position': 'relative'
            });
            previous_fs.css({'opacity': opacity});
        },
        duration: 500
    });
    setProgressBar(--current);
});
function setProgressBar(curStep){
    var percent = parseFloat(100 / steps) * curStep;
    percent = percent.toFixed();
    $(".progress-bar")
      .css("width",percent+"%")
}
$(".submit").click(function(){
    return false;
})
});
</script>
<script>
        $(document).ready(function() {
        $('.minus').click(function () {
            var $input = $(this).parent().find('input');
            var count = parseInt($input.val()) - 1;
            count = count < 0 ? 1 : count;
            $input.val(count);
            $input.change();
            return false;
        });
        $('.plus').click(function () {
            var $input = $(this).parent().find('input');
            $input.val(parseInt($input.val()) + 1);
            $input.change();
            return false;
        });
    });
</script>
{{-- show order --}}
<script>
var SITEURL = "{{ url('/laundry-admin') }}";
$('.view_button').click(function(){
    //alert('vdfzx');
    var id=$(this).attr("data-id");
    //alert(id);
    $.ajax({
        url:SITEURL+"/order/view/"+id,
        type:"GET", //send it through get method
        success: function (response) {
            $('#order_body').html(response);
        },
        error: function(response) {
            console.log('fdfd');
        }
    });
});
</script>

{{-- change option value  --}}
<script>
     var reportdate=$('#reportdate').find(":selected").val();
     var selectdate=$('#selectdate').find(":selected").val();

     let branchId = location.search;


    $('#selectdate').change(function(){
        var selectdate=$(this).find(":selected").val();
        window.location.href=url+'/laundry-admin/orders/'+type+'/'+selectdate+'/'+ reportdate + branchId;
    });
    $('#reportdate').change(function(){
        var reportdate=$(this).find(":selected").val();
        window.location.href=url+'/laundry-admin/orders/'+type+'/'+selectdate+'/'+ reportdate + branchId;
    })
</script>
{{-- Qr code --}}
<script>
$('.scan_button').click(function(){
    var id=$(this).attr("data-id");
    $.ajax({
        url:url+"/laundry-admin/get/qr_code/"+id,
        type:"GET", //send it through get method
        success: function (response) {
            $('#qr_code').html(response);
        },
        error: function(response) {
            console.log('fdfd');
        }
        });
    });
</script>




@endsection
