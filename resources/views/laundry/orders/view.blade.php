@extends('laundry_temp')
@section('content')

<div class="main-content">
    <div class="page-content">
        <div class="container-fluid">
            @include('laundry.components.nav')
            <div class="row">
                <div class="col-xl-12">
                    <div class="overview-box">
                        <div class="table-box">
                            <div>
                                <h5 class="left-border">All Orders</h5>
                            </div>



                            <div class="d-select-box">
                                <form action="{{route('laundry.allorder')}}" method="GET">
                                    <div class="d-select-box">
                                        <select class="form-select" name="branch_id" aria-label="Default select example" onchange="this.form.submit();">
                                            {{-- <option selected disabled>Branch Riyadh</option> --}}
                                            @foreach($branches as $branch)
                                                <option value="{{$branch->id}}"  @if($branch->id == $branch_id) selected @endif>{{$branch->username}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </form>
                            </div>

                        </div>
                        <div class="table_responsive_maas v2">
                            <table class="table" width="100%">
                                <thead>
                                    <tr>
                                        <th><span class="th-head-icon">Order number</th>
                                        <th><span class="th-head-icon">Customer name </th>
                                        <th><span class="th-head-icon">Delivery Type </th>
                                        <th><span class="th-head-icon">Arrival Date </th>
                                        <th><span class="th-head-icon">Order Status </th>
                                        <th>&nbsp;</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach ($orders as $order)
                                    <tr>
                                            <td>{{$order->id}}</td>
                                            <td>{{$order->customer_name}}</td>
                                            <td>  @if($order->deliverytype!=null)
                                                {{$order->deliverytype->name}}
                                                @endif</td>
                                            <td>{{$order->drop_date}}</td>
                                            <td>
                                                @if($order->progress!=null)
                                                  {{$order->progress}}
                                                @else
                                                     in accepted process
                                                @endif
                                            </td>
                                            <td><a href="#" class="cl-light">View Order</a></td>
                                    </tr>
                                   @endforeach
                                </tbody>
                            </table>
                            <div class="justify-content-center d-flex">
                                {!! $orders->appends(Request::except('page'))->render() !!}
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
$('#select_last_order').change(function(){
$('#latest_order_form').submit();
});
</script>
@endsection
