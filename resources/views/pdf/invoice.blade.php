
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<style>
    /* Default container styling */
    .container {
        width: 100%;
        padding-right: var(--bs-gutter-x, 0.75rem);
        /* Adjusts horizontal padding */
        padding-left: var(--bs-gutter-x, 0.75rem);
        /* Adjusts horizontal padding */
        margin-right: auto;
        /* Centers the container */
        margin-left: auto;
        /* Centers the container */
    }

    /* Media queries for responsive fixed widths */
    @media (min-width: 576px) {
        .container {
            max-width: 540px;
        }
    }

    @media (min-width: 768px) {
        .container {
            max-width: 720px;
        }
    }

    @media (min-width: 992px) {
        .container {
            max-width: 960px;
        }
    }

    @media (min-width: 1200px) {
        .container {
            max-width: 1140px;
        }
    }

    @media (min-width: 1400px) {
        .container {
            max-width: 1320px;
        }
    }

    #Inovice  .header img {
        width: 250px;
    }
    #Inovice .header p {
        font-size: 1.5rem;
    }

    #Inovice .clint-info {
        display: grid;
        grid-template-columns: 2fr 1fr;
        gap: 30%;
        align-items: center;
    }

    #Inovice  .clint-info h4 {
        font-size: 2rem;
    }

    #Inovice .clint-info img {
        width: 100%;
    }

    #Inovice  .order-items {
        background-color: rgba(32, 188, 227, 0.1);
        border-bottom-width: 2px;
        border-bottom-style: dashed;
        border-color: black;
        border-image: repeating-linear-gradient(to right,
        rgba(32, 188, 227, 1) 0,
        rgba(32, 188, 227, 1) 10px,
        transparent 10px,
        transparent 20px) 1;
        padding: 3rem;
        padding-top: 1rem;
        border-top-left-radius: 20px;
        border-top-right-radius: 20px;
        margin-top: 3rem;
    }

    #Inovice .order-items h3 {
        color: rgba(32, 188, 227, 1);
        font-size: 1.7rem;
    }

    #Inovice .order-items h4 {
        font-size: 2rem;
        margin: 0;
    }

    #Inovice .quntity-price {
        display: grid;
        grid-template-columns: 2fr 1fr;
        gap: 30%;
    }

    #Inovice .quntity-price h4 {
        text-align: center;
        font-size: 2rem;
    }

    #Inovice  .quntity-price p {
        color: rgba(0, 0, 0, 0.5);
        font-size: 1.2rem;
    }

    #Inovice  table {
        width: 100%;
        border-collapse: collapse;
        padding: 3rem;
        border-bottom-left-radius: 20px;
        border-bottom-right-radius: 20px;
    }

    #Inovice  th,
    #Inovice td {
        border-top: 1px solid rgba(0, 0, 0, 0.15);
        padding: 0.5rem 3rem;
        text-align: left;
        font-size: 2rem;

    }

    #Inovice th {
        font-size: 2rem;
        color: rgba(32, 188, 227, 1);
        border-top: none;
        padding: 2rem 3rem;
    }

    #Inovice tbody tr:last-child {
        background-color: rgba(32, 188, 227, 0.1);
        border-top-width: 2px;
        border-top-style: dashed;
        border-color: rgba(32, 188, 227, 1);
        border-image: repeating-linear-gradient(to right,
        rgba(32, 188, 227, 1) 0,
        rgba(32, 188, 227, 1) 10px,
        transparent 10px,
        transparent 20px) 1;
    }

    #Inovice tbody tr td:last-child {
        text-align: end;
    }

    #Inovice tbody tr:last-child td {
        color: rgba(32, 188, 227, 1);
    }

    #Inovice .inovice-btn-con {
        display: flex;
        justify-content: center;
    }

    #Inovice .inovice-btn {
        margin: auto;
        margin-top: 3rem;
        background-color: rgba(32, 188, 227, 1);
        color: white;
        border-radius: 20px;
        padding: 2rem 0;
        border: none;
        width: 50%;
        font-size: 1.5rem;
    }

    #Inovice .order-info {
        padding: 0rem ;
    }

    #Inovice .order-items tbody tr:last-child {
        background-color: unset;
        border: none;
    }

    #Inovice .order-items tbody tr td {
        border: unset;
        padding: 0;
    }

    #Inovice  .order-items tbody tr td p {
        color: rgba(0, 0, 0, 0.5);
    }

    #Inovice .order-items tbody tr:last-child td {
        color: unset;
    }

    #Inovice .order-items tbody tr h3 {
        color: unset;
    }
    #Inovice .order-items th {
        padding: 1.5rem 0rem;
    }
    #Inovice .order-info th,#Inovice .order-info td{
        padding: 1rem 3rem;
    }


    .invoice .new-section{
        border-bottom: 1px black solid;
        margin-bottom: 2rem;
        padding-bottom: 1rem;
    }
    .invoice .new-section h2{
        color: rgba(32, 188, 227, 1);
        font-size: 2rem;
    }

    .invoice .new-section h3{
        font-size: 2rem;
    }

    @media (max-width:767px) {
        .invoice .new-section h2{
            font-size: 1.5rem;
        }
        .invoice .new-section h3{
            font-size: 1.5rem;
        }
    }


    @media (max-width:456px) {
        .invoice .new-section h2{
            font-size: 1.2rem;
        }
        .invoice .new-section h3{
            font-size: 1.2rem;
        }
    }

    @media (max-width:767px) {
        #Inovice .clint-info h4{
            font-size: 1.5rem;
        }
        #Inovice .clint-info {
            gap: 7%;
        }
        #Inovice .inovice-btn{
            width: 100%;
        }
    }
    @media (max-width:456px) {
        #Inovice .clint-info h4{
            font-size: 1.2rem;
        }
        #Inovice .order-items{
            padding: 1rem;
            padding-top: 1rem;
        }
        #Inovice .order-info th,#Inovice .order-info td {
            padding: 1rem 1rem;
        }
    }
</style>

<body>
<main id="Inovice">

    <div class="container header">
        <img src="https://nathefa.shiftcodes.net/Logo.png" alt="" srcset="">
        <p>{{ date('d-m-Y', strtotime($order->created_at))  }}</p>
    </div>

    <div class="container new-section">
        <h2>{{$laundry->name}}</h2>
        @if($laundry->tax_number)
            <h3>CRN: {{$laundry->tax_number}}</h3>
        @endif
    </div>

    <div class="container clint-info">
        <div>
            <h4>Name: {{$order->customer_name}}</h4>
            <h4>Phone: {{$order->customer_phone}}</h4>
            <h4>Address: {{$order->customer_location}}</h4>
        </div>


{{--        <img src="./qr-code 1.png" alt="">--}}
     {{--   <img src="https://app.nathefah.com/qr_codes/{{$order->id}}.png" alt="" srcset=""
             style="width: 15% ; height: max-content; margin-top: 3rem;">--}}
{{--       {!! $qrCode !!}--}}
    </div>
    <div class="container">
        <div class=" order-items">
            <table>
                <thead>
                <tr>
                    <th colspan="2">Items</th>
                </tr>
                </thead>
                <tbody>
                @foreach ($order->orderdetailes as $detail)

                    @if ($detail->additionalservice_id==null)

                        @php  $serviceprice=$detail->price @endphp
                        @foreach ($order->orderdetailes as  $additional)
                            @if ($additional->additionalservice_id!=null && $additional->service_id==$detail->service_id && $additional->branchitem_id==$detail->branchitem_id)
                                @php $serviceprice+=$additional->price     @endphp
                            @endif
                        @endforeach
                        @foreach ($urgents as $urgent)
                            @if ($urgent->service_id==$detail->service_id && $urgent->branchitem_id==$detail->branchitem_id)
                                @php $serviceprice+=$urgent->price     @endphp
                            @endif
                        @endforeach

                        <tr>
                            <td>
                                <h3>{{$detail->service->name}}</h3>
                                <p>{{$detail->quantity}} {{$detail->Branchitem->name}}</p>
                            </td>
                            <td>{{$serviceprice}}</td>
                        </tr>
                    @endif
                @endforeach
                </tbody>
            </table>

        </div>
        <div class=" order-info">
            <table>
                <thead>
                <tr>
                    <th colspan="2">Order Information</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>Order ID:</td>
                    <td>{{$order->id}}</td>
                </tr>
                <tr>
                    <td>Order Payment type:</td>
                    <td>  @if ($order->pymenttype_id ==1)
                            wallet
                        @elseif ($order->pymenttype_id ==3)
                            visa
                        @else
                            cash
                        @endif</td>
                </tr>
                <tr>
                    <td>Delivery type:</td>
                    <td>{{$order->deliverytype->name ?? 'No delivery'}}</td>
                </tr>
                <tr>
                    <td>Delivery fees:</td>
                    <td>{{$order->delivery_fees}}</td>
                </tr>
                <tr>
                    <td>Tax:</td>
                    <td>{{$order->taxes}}</td>
                </tr>
                <tr>
                    <td>Discount:</td>
                    <td>  @if ($order->discounttype==null)
                            0
                        @else
                            {{$order->price_before_discount-$order->price_after_discount}}
                        @endif</td>
                </tr>
                <tr>
                    <td>Total order</td>
                    <td>  @if ($order->discounttype==null)
                            {{$order->price_before_discount}}
                        @else
                            {{$order->price_after_discount}}
                        @endif
                         SAR</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="container inovice-btn-con">
        <a href="tel:{{$setting->country_code}}{{$setting->support_phone}}" class="inovice-btn" style="color: white">
            Call Us: {{$setting->country_code}}{{$setting->support_phone}}
        </a>
    </div>

</main>

</body>

</html>
