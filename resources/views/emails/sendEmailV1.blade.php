@component('mail::message')
    # {{ $email['email_title'] }}

    {{ $email['email_body'] }}

Thanks,<br>
{{ config('app.name') }}
@endcomponent
