<div class="left-side-menu">
    <div class="logo">
        <a href="#"><img src="{{asset('dashboard/images/laundry-logo.png')}}" alt=""></a>
    </div>
    <div class="desktop-menu">
        <span class="menu-cl">Menu</span>
        <ul>
            <li>
                <a href="{{route('laundry.home',[7])}}" class="{{request()->is('laundry-admin/home*') ? 'active':''}}">
                    <img src="{{asset('images/home.png')}}" alt="">
                    <span> Dashboard </span>
                </a>
            </li>

            <li>
                <a href="{{route('laundry.orders',['inprogress','all','all'])}}"
                   class="{{request()->is('laundry-admin/orders/*')? 'active':''}}">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Orders & Reports </span>
                </a>
            </li>

            <li><a href="{{route('laundry.branches')}}"
                   class="{{request()->is('laundry-admin/laundry/branches') ? 'active':''}}">
                    <img src="{{asset('images/shop.png')}}" alt="">
                    <span> Branches </span> </a>
            </li>

            <li>
                <a href="{{route('laundry.services')}}"
                   class="{{request()->is('laundry-admin/services') ? 'active':''}}">
                    <img src="{{asset('images/shop.png')}}" alt="">
                    <span> Services </span>
                </a>
            </li>

            <li>
                <a href="{{route('laundry.drivers')}}"
                   class="{{request()->is('laundry-admin/laundry/drivers') ? 'active':''}}">
                    <img src="{{asset('images/truck-fast.png')}}" alt="">
                    <span> Drivers </span> </a>
            </li>
            <li>
                <a href="{{route('laundry.setting')}}" class="{{request()->is('laundry-admin/setting') ? 'active':''}}">
                    <img src="{{asset('images/setting-2.png')}}" alt="">
                    <span> Settings </span>
                </a>
            </li>

            <li>
                <a href="{{route('laundry.delivery_fees')}}"
                   class="{{request()->is('laundry-admin/deliveryfees') ? 'active':''}}">
                    <img src="{{asset('images/setting-2.png')}}" alt="">
                    <span> delivery fees </span>
                </a>
            </li>
            @if(auth()->guard('laundry')->user()->tax_number != null)
                <li>
                    <a href="{{route('laundry.taxes')}}" class="{{request()->is('laundry-admin/taxes') ? 'active':''}}">
                        <img src="{{asset('images/setting-2.png')}}" alt="">
                        <span> Taxes </span>
                    </a>
                </li>
            @endif


        </ul>
    </div>
    <div class="mob-menu">
        <nav id="cssmenu" class="head_btm_menu">
            <ul>
                <li><a href=""><img src="images/home.png" alt=""><span> Dashboard </span> </a></li>
                <li><a href=""><img src="images/receipt-item.png" alt=""><span> Orders & Reports </span> </a></li>
                <li><a href=""><img src="images/shop.png" alt=""><span> Branches </span> </a></li>
                <li><a href="{{route('Admin.driver')}}"><img src="images/truck-fast.png" alt=""><span> Drivers </span>
                    </a></li>
                <li>
                    <a href="{{route('laundry.setting')}}"
                       class="{{request()->is('laundry-admin/setting/*')? 'active':''}}">
                        <img src="{{asset('images/setting-2.png')}}" alt="">
                        <span> Settings </span>
                    </a>
                </li>
                <li><a href="{{route('laundry.delivery_fees')}}"
                       class="{{request()->is('laundry-admin/deliveryfees')? 'active':''}}"><img
                            src="images/setting-2.png" alt=""><span> Delivery Fees </span> </a></li>

                @if(auth()->guard('laundry')->user()->tax_number != null)
                    <li><a href="{{route('laundry.taxes')}}" class="active"><img src="images/setting-2.png"
                                                                                 alt=""><span> Taxes </span> </a></li>
                @endif


            </ul>
        </nav>
    </div>
</div>
