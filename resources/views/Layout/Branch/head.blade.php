<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>NATHEFAH Branch</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('dashboard/images/favicon.ico')}}">
<link href="{{asset('dashboard/css/bootstrap.min.css')}}"rel="stylesheet">
<link href="{{asset('dashboard/css/fontawesome-all.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="{{asset('dashboard/css/style.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('dashboard/css/menu.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('dashboard/css/animate.min.css')}}" type="text/css" media="screen">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

<script src="{{asset('dist/sweetalert.min.js')}}"></script>
<script src="{{asset('jquery/jquery.min.js')}}"></script>
<script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
{{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css" integrity="sha512-1ycn6IcaQQ40/MKBW2W4Rhis/DbILU74C1vSrLJxCq57o941Ym01SwNsOMqvEBFlcgUa6xLiPY/NS5R+E6ztJQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /> --}}
<style>
    .hide{
        display: none;
    }

</style>
<style type="text/css">
    #map {
      height: 400px;
    }
</style>
@yield('style')
</head>
<body class="bg-cl-light">
