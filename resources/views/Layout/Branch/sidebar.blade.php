<div class="left-side-menu">


    <div class="logo">
        <a href="{{route('branch.home',7)}}"><img src="{{asset('dashboard/images/laundry-logo.png')}}" alt=""></a>
    </div>


    {{--For desktop--}}
    <div class="desktop-menu">
        <span class="menu-cl">Menu</span>
        <ul>

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-dashboard'))

                <li>
                    <a href="{{route('branch.home',[7])}}" class="{{request()->is('branch-admin/home*')? 'active':''}}">
                        <img src="{{asset('images/home.png')}}" alt="">
                        <span> Dashboard </span>
                    </a>
                </li>
            @endif

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-order-list'))
                <li>
                    <a href="{{route('branch.orders',['inprogress','all','all'])}}"
                       class="{{request()->is('branch-admin/orders/*')? 'active':''}}"
                    ><img src="{{asset('images/receipt-item.png')}}" alt=""><span> Orders & Reports </span> </a>
                </li>
            @endif

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-service-list'))
                <li>
                    <a href="{{route('branch.services')}}"
                       class="{{request()->is('branch-admin/services*')? 'active':''}}">
                        <img src="{{asset('images/shop.png')}}" alt="">
                        <span> Services </span>
                    </a>
                </li>
            @endif

                @if(auth('branch')->check() && auth('branch')->user()->can('branch-service-list'))
                    <li>
                        <a href="{{route('branch.expected.time')}}"
                           class="{{request()->is('branch-admin/expected-time*')? 'active':''}}">
                            <img src="{{asset('images/back-in-time.png')}}" alt="">
                            <span> Expected time of services </span>
                        </a>
                    </li>
                @endif
            {{--
            <li><a href="{{route('Admin.branchs')}}"
               class="{{request()->is('branch-adminm/*')? 'active':''}}"
               ><img src="images/shop.png" alt=""><span> Branches </span> </a></li>
            --}}

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-driver-list'))
                <li>
                    <a href="{{route('branch.drivers',[1,1])}}"
                       class="{{request()->is('branch-admin/drivers*')? 'active':''}}">
                        <img src="{{asset('images/truck-fast.png')}}" alt="">
                        <span> Drivers </span>
                    </a>
                </li>
            @endif

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-notification-list'))
                <li>
                    <a href="{{route('branch.get.notification')}}"
                       class="{{request()->is('branch-admin/get/notification*')? 'active':''}}">
                        <img src="{{asset('images/setting-2.png')}}" alt="">
                        <span> Notification </span>
                    </a>
                </li>
            @endif

                @if(auth('branch')->check() && auth('branch')->user()->can('branch-setting-list'))
                    <li>
                        <a href="{{route('branch.transactions')}}"
                           class="{{request()->is('branch-admin/transactions*')? 'active':''}}">
                            <img src="{{asset('images/transaction.png')}}" alt="">
                            <span> Transactions </span> </a>
                    </li>
                @endif

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-setting-list'))
                <li>
                    <a href="{{route('dates.index' , ['branch_id' => null])}}"
                       class="{{request()->is('branch-admin/dates*')? 'active':''}}">
                        <img src="{{asset('images/deadline.png')}}" alt="">
                        <span> Dates </span> </a>
                </li>
            @endif

            @if(auth('branch')->check() && auth('branch')->user()->can('branch-setting-list'))
                <li>
                    <a href="{{route('branch.setting')}}"
                       class="{{request()->is('branch-admin/setting*')? 'active':''}}">
                        <img src="{{asset('images/setting-2.png')}}" alt="">
                        <span> Settings </span> </a>
                </li>
            @endif


        </ul>

    </div>


    {{--For Mobile--}}

    <div class="mob-menu">
        <nav id="cssmenu" class="head_btm_menu">
            <ul>
                <li>
                    <a href="{{route('branch.home',[7])}}">
                        <img src="{{asset('images/home.png')}}" alt="">
                        <span> Dashboard </span> </a>
                </li>

                <li>
                    <a href="{{route('branch.orders',['inprogress','all','all'])}}">
                        <img src="images/receipt-item.png" alt="">
                        <span> Orders & Reports </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('branch.services')}}">
                        <img src="{{asset('images/shop.png')}}" alt="">
                        <span> Services </span>
                    </a>
                </li>
                {{-- <li><a href="{{route('Admin.branchs')}}"><img src="images/shop.png" alt=""><span> Branches </span> </a></li> --}}
                <li>
                    <a href="{{route('branch.drivers',[1,1])}}">
                        <img src="{{asset('images/truck-fast.png')}}" alt="">
                        <span> Drivers </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('branch.get.notification')}}" class="active">
                        <img src="{{asset('images/setting-2.png')}}" alt="">
                        <span> notification </span>
                    </a>
                </li>

                <li>
                    <a href="{{route('branch.setting')}}" class="{{request()->is('setting/*')? 'active':''}}">
                        <img src="{{asset('images/setting-2.png')}}" alt="">
                        <span> Settings </span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
