<div class="left-side-menu"style="overflow:auto">
    <div class="logo">
        <a href="#"><img src="{{asset('dashboard/images/laundry-logo.png')}}" alt=""></a>
    </div>
    <div class="desktop-menu" style="overscroll:auto">
        <span class="menu-cl">Menu</span>
        <ul style="overscroll:auto">

            <li>
                <a href="{{route('Admin.deashboard')}}" class="<?php if($active_links[0] == 'home')  echo 'active'; ?>">
                    <img src="{{asset('images/home.png')}}" alt="">
                    <span> Dashboard </span>
                </a>
            </li>


            <li>
                <a href="{{route('Admin.admins')}}"
                   class="<?php if($active_links[0] == 'admin')  echo 'active'; ?>"
                ><img src="{{asset('images/user.png')}}" alt=""><span> Admins </span> </a>
            </li>


            <li>
                <a href="{{route('admin.users')}}"
                   class="<?php if($active_links[0] == 'users')  echo 'active'; ?>"
                ><img src="{{asset('images/uuser.png')}}" alt=""><span> Users </span> </a>
            </li>

            <li>
                <a href="{{route('admin.drivers')}}"
                   class="<?php if($active_links[0] == 'drivers')  echo 'active'; ?>"
                ><img src="{{asset('images/driver.png')}}" alt=""><span> Drivers </span> </a>
            </li>

            <li>
                <a href="{{route('admin.branches.index')}}"
                   class="<?php if($active_links[0] == 'branches')  echo 'active'; ?>"
                ><img src="{{asset('images/branch.png')}}" alt=""><span> Branches </span> </a>
            </li>

            <li>
                <a href="{{route('Admin.balance.index',['All','All'])}}" class="<?php if($active_links[0] == 'balance')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Balance </span>
                </a>
            </li>

            <li>
                <a href="{{route('admin.orders',['inprogress','all','all'])}}"
                   class="<?php if($active_links[0] == 'orders')  echo 'active'; ?>"
                ><img src="{{asset('images/receipt-item.png')}}" alt=""><span> Orders & Reports </span> </a>
            </li>

            <li>
                <a href="{{route('admin.completed.orders')}}"

                   class="<?php if($active_links[0] == 'orders_completed')  echo 'active'; ?>"
                ><img src="{{asset('images/receipt-item.png')}}" alt=""><span> Orders Completed </span> </a>
            </li>

            <li>
                <a href="{{route('Admin.laundry.index',['All'])}}" class="<?php if($active_links[0] == 'laundries')  echo 'active'; ?>">
                    <img src="{{asset('images/shop.png')}}" alt="">
                    <span> Laundries </span>
                </a>
            </li>

            {{-- <li>
                <a href="#">
                    <img src="{{asset('images/shop.png')}}" alt="">
                    <span> new Laundries </span>
                </a>


            <li>
                <a href="{{route('Admin.branchs')}}">
                    <img src="{{asset('images/shop.png')}}" alt="">
                    <span> Branches </span>
                </a>
            </li> --}}


            <li>
                <a href="{{route('Admin.categories')}}" class="<?php if($active_links[0] == 'categories')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> @lang('admin.categories') </span>
                </a>
            </li>

            {{--  <li><a href="{{route('Admin.setting')}}" class="active"><img src="images/setting-2.png" alt=""><span> Settings </span> </a></li>  --}}
            <li>
                <a href="{{route('Admin.services')}}" class="<?php if($active_links[0] == 'services')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> @lang('admin.services') </span>
                </a>
            </li>


            <li>
                <a href="{{route('Admin.additional-services')}}" class="<?php if($active_links[0] == 'additional')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Additional Services </span>
                </a>
            </li>



            <li>
                <a href="{{route('Admin.items')}}" class="<?php if($active_links[0] == 'items')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> @lang('admin.items') </span>
                </a>
            </li>

            <li>
                <a href="{{route('Admin.discount','avilable')}}" class="<?php if($active_links[0] == 'discount')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Discount </span>
                </a>
            </li>


            <li>
                <a href="{{route('Admin.send-notifications')}}" class="<?php if($active_links[0] == 'notifications')  echo 'active'; ?>">
                    <img src="{{asset('images/icons-notifications.png')}}" alt="">
                    <span> @lang('admin.send_notifications') </span>
                </a>
            </li>

            {{-- <li><a href="{{route('Admin.driver')}}"><img src="images/truck-fast.png" alt=""><span> Drivers </span> </a></li> --}}
            <li>
                <a href="{{route('Admin.setting')}}" class="<?php if($active_links[0] == 'setting')  echo 'active'; ?>">
                    <img src="{{asset('images/setting-2.png')}}" alt="">
                    <span> Settings </span>
                </a>
            </li>


        {{--    <li>
                <a href="{{route('Admin.delivery_fees')}}" class="<?php if($active_links[0] == 'delivery_fees')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Delivery fees </span>
                </a>
            </li>--}}

            <li>
                <a href="{{route('admin.update.apps')}}" class="<?php if($active_links[0] == 'update_apps')  echo 'active'; ?>">
                    <img src="{{asset('images/update.png')}}" alt="">
                    <span> Update applications </span>
                </a>
            </li>

            <li>
                <a href="{{route('admin.terms_conditions')}}" class="<?php if($active_links[0] == 'terms_conditions')  echo 'active'; ?>">
                    <img src="{{asset('images/terms-and-conditions.png')}}" alt="">
                    <span> Terms Condition </span>
                </a>
            </li>

            <li>
                <a href="{{route('Admin.points')}}" class="<?php if($active_links[0] == 'points')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Points </span>
                </a>
            </li>


            <li>
                <a href="{{route('Admin.periods')}}" class="<?php if($active_links[0] == 'periods')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Periods </span>
                </a>
            </li>



            <li>
                <a href="{{route('Admin.packages')}}" class="<?php if($active_links[0] == 'packages')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Packages </span>
                </a>
            </li>


            <li>
                <a href="{{route('Admin.user.wallet')}}" class="<?php if($active_links[0] == 'wallet')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> Wallet </span>
                </a>
            </li>


            <li>
                <a href="{{route('admin.application-rate.edit')}}" class="<?php if($active_links[0] == 'application_rate')  echo 'active'; ?>">
                    <img src="{{asset('images/interest-rate.png')}}" alt="">
                    <span> @lang('admin.application_rate') </span>
                </a>
            </li>


            <li>
                <a href="{{route('Admin.support.index')}}" class="<?php if($active_links[0] == 'wallet')  echo 'active'; ?>">
                    <img src="{{asset('images/receipt-item.png')}}" alt="">
                    <span> support </span>
                </a>
            </li>



        </ul>
    </div>
    <div class="mob-menu">
        <nav id="cssmenu" class="head_btm_menu">
            <ul style="overscroll:auto">
                <li><a href="{{route('Admin.deashboard')}}"><img src="images/home.png" alt=""><span> Dashboard </span> </a></li>
                <a href="{{route('admin.orders',['inprogress','all','all'])}}">
                    <img src="images/receipt-item.png" alt="">
                    <span> Orders & Reports </span>
                </a>
                <li><a href="{{route('Admin.balance.index',['All','All'])}}"><img src="images/shop.png" alt=""><span> balance </span> </a></li>
                <li><a href="{{route('Admin.laundry.index',['All'])}}"><img src="images/shop.png" alt=""><span> Laundries </span> </a></li>
                <li><a href="#"><img src="images/shop.png" alt=""><span> new Laundries </span> </a>
                <li><a href="{{route('Admin.branchs')}}"><img src="images/shop.png" alt=""><span> Branches </span> </a></li>
                {{-- <li><a href="{{route('Admin.driver')}}"><img src="images/truck-fast.png" alt=""><span> Drivers </span> </a></li> --}}
                <li><a href="{{route('Admin.services')}}" class="<?php if($active_links[0] == 'services')  echo 'active'; ?>" ><img src="images/receipt-item.png" alt=""><span> @lang('admin.services') </span> </a></li>
                <li><a href="{{route('Admin.items')}}" class="<?php if($active_links[0] == 'items')  echo 'active'; ?>" ><img src="images/receipt-item.png" alt=""><span> @lang('admin.items') </span> </a></li>
                <li><a href="{{route('Admin.discount','avilable')}}" class="<?php if($active_links[0] == 'discount')  echo 'active'; ?>"><img src="images/receipt-item.png" alt=""><span> Discount </span> </a></li>
                <li><a href="{{route('Admin.setting')}}" class="active"><img src="images/setting-2.png" alt=""><span> Settings </span> </a></li>
                <li><a href="{{route('Admin.delivery_fees')}}" class="active"><img src="images/setting-2.png" alt=""><span> Delivery Fees </span> </a></li>
                <li><a href="{{route('Admin.user.wallet')}}" class="active"><img src="images/setting-2.png" alt=""><span> Wallet </span> </a></li>

            </ul>
        </nav>
    </div>
</div>
