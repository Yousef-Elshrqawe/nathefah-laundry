<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>NATHEFAH Admin</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;" />
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta name="format-detection" content="telephone=no">
<link rel="shortcut icon" type="image/x-icon" href="{{asset('dashboard/images/favicon.ico')}}">
<link href="{{asset('dashboard/css/bootstrap.min.css')}}"rel="stylesheet">
<link href="{{asset('dashboard/css/fontawesome-all.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous"/>
<link rel="stylesheet" href="{{asset('dashboard/css/style.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('dashboard/css/menu.css')}}" type="text/css" media="screen">
<link rel="stylesheet" href="{{asset('dashboard/css/animate.min.css')}}" type="text/css" media="screen">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="{{asset('dist/sweetalert.min.js')}}"></script>
    <script src="{{asset('jquery/jquery.min.js')}}"></script>
    <script src="{{asset('bootstrap/js/bootstrap.bundle.min.js')}}" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
</head>
<body class="bg-cl-light">
@yield('style')
