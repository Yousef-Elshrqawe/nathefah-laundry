<script>
    var lat;var lng;
    // This example adds a search box to a map, using the Google Place Autocomplete
    // feature. People can enter geographical searches. The search box will return a
    // pick list containing a mix of places and predicted search terms.
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">
    function initAutocomplete() {
    const map = new google.maps.Map(document.getElementById("map"), {
        center: { lat: 24.6734719, lng: 46.7512559 },
        zoom: 10,
        mapTypeId: "roadmap",
    });


    var marker=  new google.maps.Marker({
            position: { lat: 24.6734719, lng: 46.7512559 },
            map,
            draggable:true,
            title: "Hello Rajkot!",
        });

        $('.next').click(function(){
            lat= marker.getPosition().lat();
            lng= marker.getPosition().lng();

        });


        google.maps.event.addListener(map, "click", (event) => {
            lat = marker.getPosition().lat();
            lng = marker.getPosition().lng();
        });
        // Create the search box and link it to the UI element.
        const input = document.getElementById("pac-input");
        const searchBox = new google.maps.places.SearchBox(input);

        map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
        // Bias the SearchBox results towards current map's viewport.
        map.addListener("bounds_changed", () => {
            searchBox.setBounds(map.getBounds());
        });

        let markers = [];

        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
        searchBox.addListener("places_changed", () => {
            const places = searchBox.getPlaces();

            if (places.length == 0) {
            return;
            }

        // Clear out the old markers.
        markers.forEach((marker) => {
        marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        const bounds = new google.maps.LatLngBounds();

        places.forEach((place) => {
        if (!place.geometry || !place.geometry.location) {
            console.log("Returned place contains no geometry");
            return;
        }

        const icon = {
            url: place.icon,
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25),
        };

        // Create a marker for each place.
        markers.push(
            new google.maps.Marker({
            map,
            icon,
            title: place.name,
            position: place.geometry.location,
            })
        );
        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
        });
        map.fitBounds(bounds);
    });
    }

     window.initAutocomplete = initAutocomplete;
    </script>




    <script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC9TSnlu0annI67Y3BqwFv8cZg1CcYVorw&callback=initAutocomplete&libraries=places&v=weekly"
      defer
    ></script>
