<script>
    // start create order
    var porttype = {!!json_encode($porttype)!!}
    if(porttype == 'branch'){
        var submitorderurl = {!!json_encode(route('branch.submit.order'))!!};
    }else{
        var submitorderurl = {!!json_encode(route('laundry.submit.order'))!!};
    }

    var url = {!!json_encode(url('/'))!!};
    var type = {!!json_encode($type ?? 'self_delivery')!!};
    var one_way_delivery_fees = {!!json_encode($laundry->one_way_delivery_fees)!!};
    var door_to_door_delivery_fees = {!!json_encode($laundry->door_to_door_delivery_fees)!!};
    var distance = $('#distance_input').val();


    $('#submit').click(function () {
        // lat= marker.getPosition().lat();
        // lng= marker.getPosition().lng();

        var input = document.getElementsByName('itempriceid[]');
        var countinput = document.getElementsByName('count[]');
        var customer_name = $('#customer_name').val();
        var customer_phone = $('#customer_phone').val();
        var discount_type = $('#discount_type').val();
        var discount_value = $('#discount_value').val();
        var branchId = $('#branchId').val();
        var location = $('#location').val();
        var additionalcountinput = document.getElementsByName('additionalcount[]');
        var additional_idinput = document.getElementsByName('additional_ids[]');

        var argentinput = document.getElementsByName('argent[]');
        var argent_id_input = document.getElementsByName('argent_ids[]');


        const itempriceid = [];
        const count = [];
        const additionalcount = [];
        const additional_ids = [];
        const argent = [];
        const argent_ids = [];

        inputpush(input, itempriceid);
        inputpush(additionalcountinput, additionalcount);


        inputpush(countinput, count);
        inputpush(additional_idinput, additional_ids);

        inputpush(argentinput, argent);
        inputpush(argent_id_input, argent_ids);

        // in this function put value of counters in array to send it to server
        function inputpush(input, arr) {
            for (var i = 0; i < input.length; i++) {
                arr.push(input[i].value);
            }
        }

        $.ajax({
            type: 'POST',
            url: submitorderurl,
            async: true,
            data: {
                _token: "{{ csrf_token() }}",
                itempriceid: itempriceid,
                count: count,
                additionalcount: additionalcount,
                additional_ids: additional_ids,
                argent: argent,
                argent_ids: argent_ids,
                customer_name: customer_name,
                customer_phone: customer_phone,
                discount_type: discount_type,
                discount_value: discount_value,
                location: location,
                branchId: branchId,
                lat: lat,
                long: lng,
            },
            success: function (response) {
                $('#order_detailes').html(response);
            },
            error: function (response) {
            },
        });

    });


    // start check out order
    function deliverytype(type, distance, totla_fees) {

        var delivery_type = $("#delivery_type option:selected").val();
        if (delivery_type == 'bydelivery') {
            calaculate_delivery_fees('bydelivery', distance, totla_fees);
            $('#delivery_date_type').removeClass('hide');
            if ($('#Scheduled').is(':checked')) {
                $('#delivery_date').removeClass('hide');
            }
            if ($('#Instant').is(':checked')) {
                $('#delivery_date').addClass('hide');
                emptydate();
            }
        }
        if (delivery_type == 'on_way_delivery') {
            calaculate_delivery_fees('on_way_delivery', distance, totla_fees);
            $('#delivery_date_type').removeClass('hide');
            $('#one_way_delivery_type').removeClass('hide');
            if ($('#Scheduled').is(':checked')) {
                $('#delivery_date').removeClass('hide');
            }
            if ($('#Instant').is(':checked')) {
                $('#delivery_date').addClass('hide');
                emptydate();
            }
        }
        if (delivery_type == 'self_delivery') {
            calaculate_delivery_fees('self_delivery', distance, totla_fees);
            $('#delivery_date_type').addClass('hide');
            $('#one_way_delivery_type').addClass('hide');
            $('#delivery_date').addClass('hide');
        }
        //    if($('#delivery').is(':checked')){
        //     $('#delivery_date_type').removeClass('hide');
        //    }
    }


    function datetype(type) {
        if ($('#Scheduled').is(':checked')) {
            $('#delivery_date').removeClass('hide');
        }
        if ($('#Instant').is(':checked')) {
            emptydate();
        }
    }

    function emptydate() {
        $('#day').val('');
        $('#from').val('');
        $('#to').val('');
        $('#delivery_date').addClass('hide');
    }


    function calaculate_delivery_fees(type, distance, price) {

        var delivery_fes_value;
        if (type == 'on_way_delivery') {
            delivery_fes_value = distance * one_way_delivery_fees;
        }

        if (type == 'bydelivery') {
            delivery_fes_value = distance * door_to_door_delivery_fees;
        }

        if (type == 'self_delivery') {
            delivery_fes_value = 0;
        }


        $('#delivery_fees_value').html('Delivery fees:' + Math.round(delivery_fes_value));
        var total_fees = Math.round(price + delivery_fes_value);
        $('#total_fees').html('total fees' + total_fees);
        $('#delivery_fees').val(delivery_fes_value);
    }


</script>
