
<div class="modal fade" id="new-order-modal" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="left-border">Create New Order</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                @if ($porttype=='branch')
                    <form id="msform" action="{{route('branch.confirm.order')}}" method="post">
                        @else
                            <form id="msform" action="{{route('laundry.confirm.order')}}" method="post">
                                @endif
                                @csrf
                                {{-- <input type="hidden" name="delivery_fees" id="delivery_fees"> --}}
                                <ul id="progressbar">
                                    <li class="active" id="account"></li>
                                    <li id="personal"></li>
                                    <li id="miscla"></li>
                                    <li id="confirm"></li>
                                </ul>
                                <!-- fieldsets file:///E:/Suryakanta/VTC-Revamp/HTML/agent-register.html-->
                                <fieldset id="firsttab">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Customer Name</label>
                                                <input type="text" id="customer_name" name="customer_name"
                                                       class="form-control" aria-describedby="emailHelp"
                                                       placeholder="Enter  Customer name" required>
                                                <span id="customer_name_error" class="text-danger"></span>

                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Country Code</label>
                                                <input type="text"  value="+966" disabled
                                                       class="form-control"
                                                       placeholder="Enter Customer phone number" required>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Customer phone number</label>
                                                <input type="text" id="customer_phone" name="customer_phone"
                                                       class="form-control" aria-describedby="emailHelp"
                                                       placeholder="Enter Customer phone number" required>
                                                <span id="customer_phone_error" class="text-danger"></span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="isadmin" value="{{$isadmin ?? 0}}">
                                        <input type="hidden" id="branchId" name="branch_id" value="{{$branchId ?? 0}}">
                                        <input type="hidden" name="adminCreate" value="{{$crate_admin ?? 0}}">

                                        <!-- اختار  نسبه  الخصم -->
                                        <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Ratio or number</label>
                                                <select class="form-select" aria-label="Default select example"
                                                        name="discount_type" id="discount_type">
                                                    <option value="1">Ratio %</option>
                                                    <option selected value="2">Amount SR</option>
                                                </select>
                                            </div>
                                        </div>

                                        <!-- اكتب  نسبه  الخصم -->
                                        <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Discount</label>
                                                <input type="number" id="discount_value" name="discount_value"
                                                       class="form-control" aria-describedby="emailHelp"
                                                       placeholder="Enter  Discount">
                                            </div>
                                        </div>
                                        {{-- <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Customer address</label>
                                                <input type="text" id="location" name="location" class="form-control"  aria-describedby="emailHelp" placeholder="Enter  Customer address" required>
                                            </div>
                                        </div>
                                        <input id="pac-input" class="controls" type="text" placeholder="Search Box"/>
                                        <div style="width: 398px; position: absolute; left: 209px; top: -50px; display: none;"></div>

                                        <div id="map" ></div> --}}


                                        {{-- <div class="col-md-6">
                                            <div class="form-box">
                                                <label for="" class="form-label">Service Branch</label>
                                                <select class="form-select" aria-label="Default select example">
                                                    <option selected>Branch :  <strong><em>Riyadh  </em></strong></option>
                                                    <option value="1">One</option>
                                                    <option value="2">Two</option>
                                                    <option value="3">Three</option>
                                                </select>
                                            </div>
                                        </div> --}}
                                    </div>
                                    {{-- <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-box">
                                                <label for="" class="form-label">Select Services</label>
                                            </div>
                                        </div>
                                        <div class="col-lg-12">
                                            <div class="checkbox-form-main">
                                                <div class="checkbox-loop chkboxmain">
                                                    Washing
                                                    <input id="Option1" type="checkbox">
                                                    <label class="checkbox" for="Option1"></label>
                                                </div>
                                                <div class="checkbox-loop chkboxmain">
                                                    Dry Clean
                                                    <input id="Option2" type="checkbox">
                                                    <label class="checkbox" for="Option2"></label>
                                                </div>
                                                <div class="checkbox-loop chkboxmain">
                                                    Washing & Ironing
                                                    <input id="Option3" type="checkbox">
                                                    <label class="checkbox" for="Option3"></label>
                                                </div>
                                                <div class="checkbox-loop chkboxmain">
                                                    Ironing
                                                    <input id="Option4" type="checkbox">
                                                    <label class="checkbox" for="Option4"></label>
                                                </div>
                                            </div>
                                        </div>
                                    </div> --}}
                                    <div class="btn-question mt-4">
                                        <input type="button" name="next" id="create_order_button_one" class="next btn-style-one"
                                               value="Next"/>
                                        <!--<input type="button" name="previous" class="previous action-button disablebtn" value="Back"/>-->
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <div class="tabbing-area">
                                        <ul class="nav nav-tabs">
                                            @foreach ($services as $key=>$service)
                                                <li class="nav-item">
                                                    <a class="nav-link @if($key==0) active @endif"
                                                       data-bs-toggle="tab"
                                                       href="#tab{{$key+1}}">{{$service->name}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                        {{-- categories of services --}}
                                        <div class="tab-content">
                                            @foreach ($services as $servicekey=>$service)
                                                {{-- categories --}}
                                                <div class="tab-pane @if($servicekey==0)  active   @endif"
                                                     id="tab{{$servicekey+1}}">
                                                    <div class="row">
                                                        <div class="col-md-12 subnav-tab">
                                                            <ul class="nav nav-tabs">
                                                                @foreach ($service->categories as $key=>$category)
                                                                    <li class="nav-item">
                                                                        <a class="nav-link @if($key==0)  active   @endif"
                                                                           data-bs-toggle="tab"
                                                                           href="#tab{{$servicekey+1}}-{{$key+1}}">{{$category->name}}</a>
                                                                    </li>
                                                                @endforeach
                                                            </ul>
                                                            {{-- items of categories --}}
                                                            <div class="tab-content">
                                                                {{-- items --}}
                                                                @foreach ($service->categories as $key=>$category)
                                                                    <div
                                                                        class="tab-pane @if($key==0)  active   @endif"
                                                                        id="tab{{$servicekey+1}}-{{$key+1}}">
                                                                        <div class="row">
                                                                            @foreach ($category->branchitems as $branchitem)
                                                                                <div
                                                                                    class="col-xl-4 col-md-4 mb-3">
                                                                                    <div class="cloth-box">
                                                                                        <div class="cloth-data">
                                                                                            <div
                                                                                                class="cloth-img">
                                                                                                <div>
                                                                                                    <img
                                                                                                        src="images/shirt.png"
                                                                                                        alt="">
                                                                                                </div>
                                                                                                <div
                                                                                                    class="cloth-content">
                                                                                                    <h6>{{$branchitem->name}}</h6>
                                                                                                    @foreach ($branchitem->branchitemprice as $price)
                                                                                                        @if($price->category_id==$category->id && $price->service_id==$service->id)
                                                                                                            <p>{{$price->price}}
                                                                                                                SR
                                                                                                                /
                                                                                                                item</p>
                                                                                                        @endif
                                                                                                    @endforeach
                                                                                                </div>
                                                                                            </div>
                                                                                            <div
                                                                                                class="count-number">
                                                                                                <input
                                                                                                    type="text"
                                                                                                    name="count[]"
                                                                                                    value="0"/>
                                                                                                <span
                                                                                                    class="plus fas fa-chevron-up"></span>
                                                                                                <span
                                                                                                    class="minus fas fa-chevron-down"></span>
                                                                                            </div>
                                                                                        </div>
                                                                                        @foreach ($branchitem->branchitemprice as $price)
                                                                                            @if($price->category_id==$category->id && $price->service_id==$service->id)
                                                                                                {{-- {{$price->id}} --}}
                                                                                                <input
                                                                                                    type="hidden"
                                                                                                    class="itempriceid"
                                                                                                    name="itempriceid[]"
                                                                                                    value="{{$price->id}}">
                                                                                            @endif
                                                                                        @endforeach
                                                                                        {{-- <a href="#" class="more-btn">More Details <i class=" fas fa-chevron-right"></i></a> --}}
                                                                                        <!--<a href="#" class="more-btn">More Details <i class=" fas fa-chevron-right"></i></a>-->
                                                                                        <a class="more-btn read"
                                                                                           data-bs-toggle="collapse"
                                                                                           href="#multiCollapseExample{{$price->id}}"
                                                                                           role="button"
                                                                                           aria-expanded="false"
                                                                                           aria-controls="multiCollapseExample1">More
                                                                                            Details<i
                                                                                                class=" fas fa-chevron-right"></i></a>


                                                                                        {{-- start more detailes --}}
                                                                                        <div
                                                                                            class="collapse multi-collapse"
                                                                                            id="multiCollapseExample{{$price->id}}">
                                                                                            <div
                                                                                                class="extradetails">
                                                                                                {{-- start urgent price --}}


                                                                                                <div
                                                                                                    class="extradetails-box">
                                                                                                    <div
                                                                                                        class="extradetails-box-left">
                                                                                                        <p>Add
                                                                                                            Urgent</p>
                                                                                                        <p>
                                                                                                            ({{$branchitem->argentprice}}
                                                                                                            SAR
                                                                                                            /
                                                                                                            item)</p>


                                                                                                    </div>
                                                                                                    <div
                                                                                                        class="extradetails-box-right">
                                                                                                        <div
                                                                                                            class="count-number">
                                                                                                            <input
                                                                                                                type="text"
                                                                                                                name="argent[]"
                                                                                                                value="0">
                                                                                                            <input
                                                                                                                type="hidden"
                                                                                                                name="argent_ids[]"
                                                                                                                value="{{$branchitem->id}}">
                                                                                                            <span
                                                                                                                class="plus fas fa-chevron-up"></span>
                                                                                                            <span
                                                                                                                class="minus fas fa-chevron-down"></span>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                                <hr>
                                                                                                {{-- start additional service --}}
                                                                                                <h6>Add
                                                                                                    Additional
                                                                                                    service</h6>
                                                                                                @foreach ($category->additionalservice as $additional)
                                                                                                    @foreach ($branchitem->branchitemprice as $price)
                                                                                                        @if ($additional->id==$price->additionalservice_id)
                                                                                                            <div
                                                                                                                class="extradetails-box">
                                                                                                                <div
                                                                                                                    class="extradetails-box-left">
                                                                                                                    <p>{{$additional->name}}</p>
                                                                                                                    <p>
                                                                                                                        ({{$price->price}}
                                                                                                                        SAR
                                                                                                                        /
                                                                                                                        item)</p>
                                                                                                                </div>
                                                                                                                <div
                                                                                                                    class="extradetails-box-right">
                                                                                                                    <div
                                                                                                                        class="count-number">
                                                                                                                        <input
                                                                                                                            type="text"
                                                                                                                            name="additionalcount[]"
                                                                                                                            value="0"/>

                                                                                                                        <span
                                                                                                                            class="plus fas fa-chevron-up"></span>
                                                                                                                        <span
                                                                                                                            class="minus fas fa-chevron-down"></span>
                                                                                                                    </div>
                                                                                                                    <input
                                                                                                                        type="hidden"
                                                                                                                        name="additional_ids[]"
                                                                                                                        value="{{$price->id}}"/>
                                                                                                                </div>
                                                                                                            </div>
                                                                                                        @endif
                                                                                                    @endforeach

                                                                                                @endforeach


                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            @endforeach
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    <div class="btn-question mt-3">
                                        <input type="button" id="submit" name="next" class="next btn-style-one create_order_button"
                                               value="Create Order" disabled />
                                        <!--<input type="button" name="previous" class="previous action-button " value="Back"/>-->
                                         <div id="product-error" style="color:red; margin-top:5px;"></div>


                                    </div>
                                    <style>
                                        .create_order_button:disabled {
                                            background-color: red !important;
                                            cursor: not-allowed;
                                        }

                                        .create_order_button {
                                            transition: background-color 0.3s ease-in-out;
                                        }

                                        #create_order_button_one:disabled {
                                            background-color: red !important;
                                            cursor: not-allowed;
                                        }

                                        #create_order_button_one {
                                            transition: background-color 0.3s ease-in-out;
                                        }
                                    </style>
                                </fieldset>
                                <fieldset id="order_detailes">
                                </fieldset>
                            </form>
                    </form>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        function validateFields() {
            var name = $('#customer_name').val().trim();
            var phone = $('#customer_phone').val().trim();

            // التحقق من أن الاسم يحتوي على 4 أحرف على الأقل
            var nameValid = name.length >= 4;

            // التحقق من رقم الهاتف:
            // يجب أن يبدأ بـ "05" ويتبعه 8 أرقام أو يبدأ بـ "5" ويتبعه 8 أرقام
            var phoneValid = /^(05\d{8}|5\d{8})$/.test(phone);

            // عرض رسالة الخطأ لحقل الاسم
            if (!nameValid) {
                $('#customer_name_error').text('Customer name must be at least 4 characters.');
            } else {
                $('#customer_name_error').text('');
            }

            // عرض رسالة الخطأ لحقل الهاتف
            if (!phoneValid) {
                $('#customer_phone_error').text("Mobile number must start with '05' or '5' and have exactly 8 digits after it.");
            } else {
                $('#customer_phone_error').text('');
            }

            // تفعيل أو تعطيل زر "Next" بناءً على صحة المدخلات
            if (nameValid && phoneValid) {
                $('#create_order_button_one').prop('disabled', false);
                $('#create_order_button_one').css('background-color', 'rgb(58 129 18)');
            } else {
                $('#create_order_button_one').prop('disabled', true);
                $('#create_order_button_one').css('background-color', 'red');
            }
        }

        // تنفيذ التحقق عند تغيير قيمة حقلي الاسم والهاتف
        $('#customer_name, #customer_phone').on('keyup change', validateFields);

        // تحقق أولي عند تحميل الصفحة
        validateFields();
    });
</script>
<script>
    $(document).ready(function(){
        // دالة لفحص اختيار المنتجات
        function checkProductSelection(){
            var total = 0;
            // اجمع قيم جميع حقول "count[]"
            $('input[name="count[]"]').each(function(){
                var val = parseInt($(this).val()) || 0;
                total += val;
            });

            // إذا كان المجموع > 0، فعل الزر وحدّث النص
            if(total > 0){
                $('.create_order_button').prop('disabled', false).val('Create Order →');
                $('.create_order_button').css('background-color', 'rgb(58 129 18)');

                $('#product-error').text('');
            } else {
                $('.create_order_button').prop('disabled', true).val('Create Order');
                //خليلي لون البوتن احمر
                $('.create_order_button').css('background-color', 'red');
                $('#product-error').text('Please select at least one product.');
            }
        }

        // تحقق أولي عند تحميل الصفحة
        checkProductSelection();

        // تأكد من ربط الحدث على حقول "count[]" فقط
        $('input[name="count[]"]').on('keyup change', checkProductSelection);

    });
</script>



