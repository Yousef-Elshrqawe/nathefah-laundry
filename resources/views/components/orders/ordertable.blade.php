<div class="tab-content">
    <div class="tab-pane active" id="tabdel">
        <div class="row">
            <div class="col-md-12">
                <div class="table_responsive_maas">
                    <table class="table" width="100%">
                        <thead>
                        <tr>
                            <th><span class="th-head-icon">Order number</span></th>
                            <th><span class="th-head-icon">Customer name </span></th>
                            <th><span class="th-head-icon">Customer phone </span></th>
                            <th><span class="th-head-icon">fort id</span></th>
                            <th><span class="th-head-icon">Delivery type </span></th>
                            <th><span class="th-head-icon">Arrival Date </span></th>
                            <th><span class="th-head-icon">Order Status </span></th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td>{{$order->id}}</td>
                                <td>{{$order->customer_name}}</td>
                                <td>{{$order->user? $order->user->phone : $order->customer_phone}}</td>
                                <td>{{$order->fort_id == null ? 'N/A' : $order->fort_id}}</td>
                                <td>
                                    @if ($order->deliverytype!=null)
                                        {{$order->deliverytype->name}}
                                    @endif
                                </td>
                                <td>{{$order->drop_date}}</td>
                                {{-- start Order Status --}}
                                <td>
                                    @if(!empty($order->progress) && $order->progress != 'no_progress')
                                        @if($order->progress == 'finished' && $order->driver_id != null)
                                            Finished / Awaiting Driver Confirmation
                                        @elseif($order->progress == 'finished' && $order->driver_id == null && $order->delivery_type_id != 4 && $order->delivery_type_id != 2)
                                            Finished / Pending Driver Assignment
                                        @elseif($order->progress == 'finished' && $order->delivery_type_id == 2)
                                            Finished
                                        @elseif($order->progress == 'finished' && $order->driver_id == null && $order->delivery_type_id == 4)
                                            Finished / Awaiting Customer Confirmation
                                        @elseif($order->progress == 'indelivery' && optional($order->OrderDriveryStatus->first())->order_status == 'drop_of_home')
                                            In Delivery / Home Drop-Off
                                        @elseif($order->progress == 'indelivery' && optional($order->OrderDriveryStatus->first())->order_status == 'drop_of_laundry')
                                            In Delivery / Laundry Drop-Off
                                        @elseif($order->progress == 'completed')
                                            Completed
                                        @elseif($order->progress == 'inprogress')
                                            In Progress
                                        @else
                                            {{ ucfirst($order->progress) }} {{-- عرض افتراضي في حال عدم مطابقة أي شرط --}}
                                        @endif
                                    @else
                                        @if($order->delivery_status == 'inprogress')
                                            Pickup from Home
                                        @else
                                            @if($order->confirmation == 'pending')
                                                New
                                            @elseif($order->confirmation == 'accepted' && $order->driver_id == null)
                                                Unassigned
                                            @elseif($order->confirmation == 'accepted' && $order->driver_id != null)
                                                Awaiting Driver Acceptance
                                            @else
                                                Unknown Status
                                            @endif
                                        @endif
                                    @endif
                                </td>


                                {{-- End Order Status --}}
                                <td>
                                    @if($type=='indelivery')
                                        @if ($order->OrderDriveryStatus->first()!=null && $order->OrderDriveryStatus->first()->order_status=='drop_of_laundry')
                                            @if ($porttype=='branch')
                                                <a href="{{url('branch-admin/recive/order?order_id='.$order->id.'&confirm_type=drop_of_laundry')}}"
                                                   class="cl-light scan_button" data-id="{{$order->id}}"
                                                   class="cl-light">Recive</a>
                                                |
                                            @elseif($porttype=='laundry')
                                                <a href="{{url('laundry-admin/recive/order?order_id='.$order->id.'&confirm_type=drop_of_laundry')}}"
                                                   class="cl-light scan_button" data-id="{{$order->id}}"
                                                   class="cl-light">Recive</a>
                                                |
                                            @endif

                                        @endif
                                    @endif


                                    @if ($type=='inprogress')
                                        @if ($porttype=='branch')
                                            <a href="{{route('branch.finish.order',$order->id)}}" class="cl-light">finish</a>
                                            |
                                        @elseif($porttype=='laundry')
                                            <a href="{{route('laundry.finish.order',$order->id)}}" class="cl-light">finish</a>
                                            |
                                        @endif
                                    @endif
                                    @if ($type=='unassigned')
                                        @if ($porttype=='branch')
                                            <a href="{{route('branch.assignorderview',$order->id)}}" class="cl-light">assign
                                                driver</a>
                                            |
                                        @elseif($porttype=='laundry')
                                            <a href="{{route('laundry.assignorderview',$order->id)}}" class="cl-light">assign
                                                driver</a>
                                            |
                                        @endif
                                    @endif

                                    @if ($type=='all')

                                    @endif

                                    @if($type=='one_way_delivery')
                                        {{--start home drop of --}}
                                        {{-- display scan button --}}
                                        @if($order->delivery_type_id==3 && $order->order_delivery_status==1 &&$order->driver_id!=null)

                                            @if ($porttype!='Admin')
                                                <a href="#" class="cl-light scan_button" data-id="{{$order->id}}"
                                                   data-bs-toggle="modal" data-bs-target="#order-scan-modal">scan</a>
                                                |
                                            @endif
                                        @endif
                                        {{-- display assign driver button  --}}
                                        @if($order->delivery_type_id==3 && $order->order_delivery_status==1 &&$order->driver_id==null)
                                            <a href="{{route('branch.assignorderview',$order->id)}}" class="cl-light">assign
                                                driver</a>
                                            |
                                        @endif
                                        @if($order->delivery_type_id==3 && $order->order_delivery_status==0)
                                            @if ($porttype=='branch')
                                                <a href="{{url('branch-admin/recive/order?order_id='.$order->id.'&confirm_type=drop_of_laundry')}}"
                                                   class="cl-light scan_button" data-id="{{$order->id}}"
                                                   class="cl-light">Recive</a>
                                                |
                                            @elseif ($porttype=='laundry')
                                                <a href="{{url('laundry-admin/recive/order?order_id='.$order->id.'&confirm_type=drop_of_laundry')}}"
                                                   class="cl-light scan_button" data-id="{{$order->id}}"
                                                   class="cl-light">Recive</a>
                                                |
                                            @endif
                                        @endif
                                        {{--end home drop of --}}

                                        {{--start self drop of --}}


                                        @if($order->delivery_type_id==4)
                                            @if ($porttype!='Admin')
                                                <a href="#" class="cl-light scan_button" data-id="{{$order->id}}"
                                                   data-bs-toggle="modal" data-bs-target="#order-scan-modal">scan</a>
                                                |
                                            @endif
                                        @endif
                                        {{--end self drop of --}}
                                    @endif


                                    @if ($type=='self_delivery')

                                        @if($order->progress=='no_progress')
                                            @if ($porttype=='branch')
                                                <a href="{{url('branch-admin/recive/order?order_id='.$order->id.'&confirm_type=drop_of_laundry')}}"
                                                   class="cl-light scan_button" data-id="{{$order->id}}"
                                                   class="cl-light">Recive</a>
                                                |
                                            @elseif($porttype=='laundry')
                                                <a href="{{url('laundry-admin/recive/order?order_id='.$order->id.'&confirm_type=drop_of_laundry')}}"
                                                   class="cl-light scan_button" data-id="{{$order->id}}"
                                                   class="cl-light">Recive</a>
                                                |
                                            @endif

                                        @endif
                                        {{-- scan puton to generate qr code and user will sacn it by his phone to recive it --}}
                                        @if($order->progress=='finished')
                                            {{-- check port --}}
                                            @if ($porttype=='branch')
                                                {{-- here branch create order for user not register in the system --}}
                                                @if ($order->user_id==null)
                                                    <a href="{{route('branch.deliver.order',$order->id)}}"
                                                       class="cl-light">deliver</a>

                                                    {{-- branch or user create order (user already avilable in the system ) --}}
                                                @else
                                                    <a href="#" class="cl-light scan_button" data-id="{{$order->id}}"
                                                       data-bs-toggle="modal"
                                                       data-bs-target="#order-scan-modal">scan</a>
                                                @endif
                                                |
                                            @elseif($porttype=='laundry')
                                                {{-- here branch create order for user not register in the system --}}
                                                @if ($order->user_id==null)
                                                    <a href="{{route('laundry.deliver.order',$order->id)}}"
                                                       class="cl-light">deliver</a>
                                                    {{-- branch or user create order (user already avilable in the system ) --}}
                                                @else
                                                    <a href="#" class="cl-light scan_button" data-id="{{$order->id}}"
                                                       data-bs-toggle="modal"
                                                       data-bs-target="#order-scan-modal">scan</a>
                                                @endif

                                                |
                                            @endif
                                        @endif

                                    @endif



                                    @if ($order->progress=='finished' || $order->progress =='completed'  &&$order->driver_id!=null)
                                        <a href="#" class="cl-light scan_button" data-id="{{$order->id}}"
                                           data-bs-toggle="modal" data-bs-target="#order-scan-modal">scan</a>
                                        |
                                    @endif

                                    {{-- display in new & all --}}
                                    @if ($order->confirmation=='pending')
                                        @if ($porttype=='branch')
                                            <a href="{{route('branch.accept.order',$order->id)}}"
                                               class="cl-light scan_button">accept</a>
                                            |
                                            <a href="{{url('branch-admin/reject/order?order_id='.$order->id)}}"
                                               class="cl-light scan_button">reject</a>
                                            |
                                        @elseif ($porttype=='laundry')
                                            <a href="{{route('laundry.accept.order',$order->id)}}"
                                               class="cl-light scan_button">accept</a>
                                            |
                                            {{-- <a href="{{url('branch-admin/reject/order?order_id='.$order->id)}}" class="cl-light scan_button">reject</a>
                                            | --}}
                                        @endif
                                    @endif




                                        @if(!in_array($order->original_progress, ['completed', 'finished']) && in_array($order->confirmation, ['pending', 'accepted']))
                                            <a href="javascript:void(0);"
                                               onclick="confirmRejectOrder({{ $order->id }})"
                                               class="cl-light scan_button">
                                                Cancel
                                            </a>
                                        @endif



                                        <a href="#" class="cl-light view_button" data-id="{{$order->id}}"
                                       data-bs-toggle="modal" data-bs-target="#order-details-modal">View</a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- تأكد من تضمين مكتبة SweetAlert2 -->
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<script>
    function confirmRejectOrder(orderId) {
        Swal.fire({
            title: 'Are you sure?',
            text: 'Are you sure you want to cancel the order? This action cannot be undone',
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#d33',
            cancelButtonColor: '#3085d6',
            confirmButtonText: 'Yes cancel',
            cancelButtonText: 'no thanks'
        }).then((result) => {
            if (result.isConfirmed) {
                window.location.href =
                    '{{ url('branch-admin/reject/order') }}?order_id=' + orderId;
            }
        });
    }
</script>
