<div class="col-lg-12">
    <div class="row">

    <div class="col-lg-8 col-md-12">
        <div class="whitebox-panel">
            <div class="row">
  <div class="col-xl-12">
    <div class="table-box">
      <div>
          <h5 class="left-border">Reports</h5>
      </div>
      <div class="table-box-select">
          <div class="d-select-box">
              <select id="reportdate" class="form-select" aria-label="Default select example">
                  <option selected value="all" @if($datereview=='all') selected @endif>Sort By : Latest All</option>
                  <option selected value="all" @if($datereview=='all') selected @endif>All</option>
                  <option value="7"  @if($datereview=='7') selected @endif>One Week</option>
                  <option value="14" @if($datereview=='14') selected @endif>Two Week</option>
                  <option value="21" @if($datereview=='21') selected @endif>Three Week</option>
              </select>
          </div>
      </div>
    </div>
    @foreach ($reports as $report)
        <div class="review-sec">
            <div class="review-profile-details">
                <img src="images/avatar.png" alt="">
                <div class="review-content">
                <div class="report-name">
                    <ul>
                    <li>{{$report->user->name}}</li>
                    <li class="border-round">{{$report->created_at}}</li>
                    </ul>
                </div>
                <h6>{{$report->desc}}</h6>
                <div class="selling-title-box">
                    <ul>
                        <li><i class="fas fa-star"></i></li>
                        <li><a href="#">{{$report->rate}}</a></li>
                    </ul>
                </div>
                </div>
            </div>
            <div class="review-point">
            <a href="#" data-id="{{$report->order_id}}" class="blue-btn-link view_button" data-bs-toggle="modal" data-bs-target="#order-details-modal">View </a>
            </div>
        </div>
    @endforeach
    <div class="justify-content-center d-flex">
        {!! $reports->appends(Request::except('page'))->render() !!}
    </div>
</div>
</div>
        </div>
    </div>
