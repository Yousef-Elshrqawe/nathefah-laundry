<table class="table m-0" width="100%">
    <thead>
      <tr>
        <th>
          <span class="th-head-icon">Order #</span>
        </th>
        <th>
          <span class="th-head-icon">Branch</span>
        </th>
        <th>
          <span class="th-head-icon">Order Status</span>
        </th>
      </tr>
    </thead>
    <tbody>
        @foreach ($latestorders as $order)
          <tr>
            <td>{{$order->id}}</td>
            <td>{{Session::get('username')}}</td>
            <td>
                {{-- this meen that laundry accept order from customer andd assign order for driver and driver accept order from laundry --}}
                @if($order->progress!=null)
                   @if ($order->progress=='finished'&&$order->driver_id!=null )
                     {{$order->progress}}/waiting driver
                    @elseif($order->progress=='finished'&&$order->driver_id==null &&$order->delivery_type_id!=4)
                     {{$order->progress}}/assign driver
                        @elseif($order->progress=='indelivery'&&$order->OrderDriveryStatus->first()->order_status=='drop_of_home')
                        {{$order->progress}}/drop of home
                        @elseif($order->progress=='indelivery'&&$order->OrderDriveryStatus->first()->order_status=='drop_of_laundry')
                        {{$order->progress}}/drop of laundry
                    @elseif($order->progress=='completed')
                    {{$order->progress}}
                   @endif
                @else
                {{--in this case $order->progress== null and delivery_status==inprogress that mean that driver accept order from laundry and go to recive order from customer  --}}
                   @if ($order->delivery_status=='inprogress')
                          pick up  home
                    @else
                    {{-- in this case $order->progress== null and delivery_status==null --}}
                        @if ($order->confirmation=='pending')
                           new
                        @endif
                        @if ($order->confirmation=='accepted'&& $order->driver_id==null)
                          un assigned
                        @endif
                        @if ($order->confirmation=='accepted'&& $order->driver_id!=null)
                          waiting for driver accept
                        @endif
                   @endif
                @endif
            </td>
          </tr>
        @endforeach
    </tbody>
  </table>
