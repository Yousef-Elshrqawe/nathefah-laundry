<div class="visible-print text-center">
    {!! QrCode::size(350)->generate($code); !!}
    <h4>Code.</h4>
    <h6>{{$code}}</h6>
</div>
