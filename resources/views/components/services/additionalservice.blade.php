<div class="tabbing-area">
    <ul class="nav nav-tabs">
        @foreach ($additionalservices as $key=>$additionalservice)
        @if (in_array($additionalservice->id,$additionalservices_id))
            <li class="nav-item">
                <a class="nav-link @if($key==0) active @endif" data-bs-toggle="tab" href="#tab1{{$key+1}}">{{$additionalservice->name}}</a>
            </li>
        @endif
        @endforeach
        <li><a href="#" class="add-main-servicebtn" data-bs-toggle="modal" data-bs-target="#Add_Additional_service"><i class="fas fa-plus"></i> Add Additional Service</a></li>
    </ul>
    <div class="tab-content">
        @foreach ($additionalservices as $key=>$additionalservice)
        @if (in_array($additionalservice->id,$additionalservices_id))
        <div class="tab-pane @if($key==0) active @endif" id="tab1{{$key+1}}">
            <div class="row">
                <div class="col-md-12 subnav-tab">
                    <ul class="nav nav-tabs">
                        @foreach ($additionalservice->categories as $categorykey=>$category)
                        <li class="nav-item">
                            <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tab11-{{$categorykey+1}}">{{$category->name}}</a>
                        </li>
                        @endforeach
                    </ul>
                        <div class="tab-content">
                            @foreach ($additionalservice->categories as $categorykey=>$category)
                            <div class="tab-pane @if($categorykey==0) active  @endif" id="tab11-{{$categorykey+1}}">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="services-panel">
                                            @foreach ($category->branchitems as $item)
                                              @foreach ($item->branchitemprice as $itemprice)
                                                  @if ($itemprice->category_id==$category->id&&$itemprice->additionalservice_id==$additionalservice->id)
                                                    <div class="services-panel-box">
                                                        <label>{{$item->name}} <span><a href="#" class="item_name"  data-id="{{$item->id}}" data-bs-toggle="modal" data-bs-target="#edititem"><i class="fas fa-edit"></i></a></span></label>
                                                        <span class="input-writeup">{{$itemprice->price}} SR</span>
                                                        @foreach($item->itemadditionalservice as $additional)
                                                            @if($additional->additionalservice_id == $additionalservice->id)
                                                                <span class="input-writeup m-auto">
                                                                    <i style="cursor: pointer" id="itemStatus-{{$additional->id}}" title="Change status" onclick="changeItemStatus({{$additional->id}})"
                                                                       class="{{$additional->status == "on" ? "fas fa-toggle-on" : "fas fa-toggle-off"}}">
                                                                    </i>
                                                                    <input type="hidden" value="{{$additional->status}}" id="itemInputStatus-{{$additional->id}}">
                                                                </span>
                                                            @endif
                                                         @endforeach

                                                    </div>
                                                  @endif
                                              @endforeach
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
        @endif
        @endforeach
    </div>

</div>
