<div class="tabbing-area">
    <ul class="nav nav-tabs">
        @foreach ($services as $key=>$service)
          @if (in_array($service->id,$services_id))
            <li class="nav-item">
                <a class="nav-link @if($key==0) active @endif " data-bs-toggle="tab" href="#tab{{$key+1}}">{{$service->name}}</a>
            </li>
          @endif
        @endforeach
        <li><a href="#" class="add-main-servicebtn" data-bs-toggle="modal" data-bs-target="#Add_item"><i class="fas fa-plus"></i> Add new Main Service</a></li>
    </ul>
    <div class="tab-content">
        @foreach ($services as $key=>$service)
        @if (in_array($service->id,$services_id))
         <div class="tab-pane @if($key==0) active @endif" id="tab{{$key+1}}">
            <div class="row">
                <div class="col-md-12 subnav-tab">
                    <ul class="nav nav-tabs">
                        @foreach($service->categories as $categorykey=>$category)
                            <li class="nav-item">
                                <a class="nav-link @if($categorykey==0) active @endif" data-bs-toggle="tab" href="#tab{{$key+1}}-{{$categorykey+1}}">{{$category->name}}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="tab-content">
                @foreach($service->categories as $categorykey=>$category)
                    <div class="tab-pane @if($categorykey==0) active @endif" id="tab{{$key+1}}-{{$categorykey+1}}">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="services-panel">
                                    @foreach ($category->branchitems as $item)
                                        @foreach ($item->branchitemprice as $price)
                                            @if($price->category_id==$category->id && $price->service_id==$service->id)
                                            <div class="services-panel-box">
                                                <label>{{$item->name}}
                                                    <span>
                                                        <a href="#" class="item_name"  data-id="{{$item->id}}" data-bs-toggle="modal" data-bs-target="#edititem">
                                                            <i class="fas fa-edit"></i>
                                                        </a>
                                                    </span>
                                                </label>
                                                <span class="input-writeup">{{$price->price}} SR</span>
                                            </div>
                                            @endif
                                        @endforeach
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>

         </div>
        @endif
        @endforeach
    </div>
     {{-- end of main service --}}
</div>
