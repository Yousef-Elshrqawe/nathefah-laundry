<div class="modal fade" id="itemsStatus" tabindex="-1" aria-labelledby="new-order-modal" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <div class="tab-content">
                    <div class="tab-pane active" id="tabdel">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table_responsive_maas">
                                    <table class="table" width="100%">
                                        <thead>
                                        <tr>
                                            <th><span class="th-head-icon">#</span></th>
                                            <th><span class="th-head-icon">Service </span></th>
                                            <th><span class="th-head-icon">Change Status </span></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach ($services as $service)
                                            <tr>
                                                <td>{{$service->id}}</td>
                                                <td>{{$service->name}}</td>
                                                <th>
                                                    <span class="th-head-icon">
                                                        @if (!$service->branchservice->isEmpty())
                                                            @if ($service->branchservice[0]!=null)
                                                            <input type="hidden" id="statusInput-{{$service->branchservice[0]->id}}" value="{{$service->branchservice[0]->status}}">
                                                            <i style="cursor: pointer" id="status-{{$service->branchservice[0]->id}}"
                                                            onclick="changeStatus({{$service->branchservice[0]->id}})"
                                                            class="{{$service->branchservice[0]->status == "on" ? "fas fa-toggle-on" : "fas fa-toggle-off"}}"></i>
                                                            @endif
                                                        @endif
                                                    </span>
                                                </th>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
