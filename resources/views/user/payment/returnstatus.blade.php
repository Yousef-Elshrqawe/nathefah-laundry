<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Payment Status</title>

    <style>
        body {
            font-family: Arial, sans-serif;
            text-align: center;
            background-color: #f4f4f4;
            padding: 20px;
        }
        .container {
            max-width: 600px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
        }
        h1 {
            color: #e74c3c;
        }
        .goback-button {
            background-color: #3498db;
            color: #fff;
            padding: 10px 20px;
            text-decoration: none;
            border-radius: 5px;
            display: inline-block;
            margin-top: 20px;
            cursor: pointer;
        }
        .goback-button:hover {
            background-color: #2980b9;
        }
    </style>
</head>
<body>

<script>
    // Function to parse URL parameters
    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    // Get transaction status from the URL
    var transactionStatus = getParameterByName('response_message');

    // Display appropriate message based on the status
    if (transactionStatus === 'success') {
        document.write('<h1>Payment Success!</h1>');
        // Additional success message or actions
    } else if (transactionStatus === 'Transaction declined') {
        document.write('<h1>Payment Failed</h1>');
        // Additional failure message or actions
        <a href="https://sbcheckout.payfort.com/FortAPI/paymentPage" class="goback-button" onclick="goBack()">Go Back</a>
    } else {
        document.write('<h1>Invalid Transaction</h1>');
        // Handle other cases or errors

    }
</script>

<script>
    // Function to go back to the previous page
    function goBack() {
        window.history.back();
    }
</script>

</body>
</html>
