<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
   <h1>PRIVACY POLICY</h1>
   This Privacy Policy describes your privacy rights regarding our collection, use, storage, sharing and protection of your personal information. It applies to the Nathefah.com website and all related sites, applications, services and tools regardless of how you access or use them.

You accept this Privacy Policy when you sign up for, access, or use our services, content, features, technologies or functions offered on our website and all related sites, applications, and services. We may amend this policy at any time by posting a revised version on our website. The revised version will be effective at the time we post it. In addition, if the revised version includes a substantial change, we will provide you with 30 days prior notice by emailing you a notice of the change at the email address you provided during service sign up.



   <section>
     <p>•	All credit/debit cards’ details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.</p>
        <p>•	If you make a payment for our products or services on our website, the details you are asked to submit will be provided directly to our payment provider via a secured connection</p>

        <p>•	Merchant will not pass any debit/credit card details to third parties.</p>

        <p>•	Merchant takes appropriate steps to ensure data privacy and security including through various hardware and software methodologies. However, (nathefah.com) cannot guarantee the security of any information that is disclosed online.</p>


        <p>•	The merchant is not responsible for the privacy policies of websites to which it links. If you provide any information to such third parties different rules regarding the collection and use of your personal information may apply. You should contact these entities directly if you have any questions about their use of the information that they collect.</p>


        <p>•	Some of the advertisements you see on the Site are selected and delivered by third parties, such as ad networks, advertising agencies, advertisers, and audience segment providers. These third parties may collect information about you and your online activities, either on the Site or on other websites, through cookies, web beacons, and other technologies in an effort to understand your interests and deliver to you advertisements that are tailored to your interests. Please remember that we do not have access to, or control over, the information these third parties may collect. The information practices of these third parties are not covered by this privacy policy. Kindly remove any other statements that contradicts with the above statements.</p>



    </section>


    <h1> How we collect information about you :</h1>
    When you sign up for service through the Nathefah.com website, you voluntarily provide us with your contact information including your address, phone number and email address as well as your financial information. This information is collected so that we can accurately perform pick up and drop off services and so that we can communicate with you regarding your laundry services


    <p>•	• If you open an account, we may collect the following types of information:</p>
    <p>•	• Contact information, such as your name, address, phone, email and other similar information</p>
    <p>•	• Financial information, such as the full bank account numbers and/or credit card numbers that you give us when you use our Services.</p>

    <p>•	• Detailed cleaning preferences that pertain to your garments.</p>
    <section>
        You may choose to provide us with access to certain personal information stored by third parties such as social media sites (e.g., Facebook and Twitter). This is done when you voluntarily “like” our Facebook Page or when you voluntarily “follow” of Twitter Page. The information we may receive varies by site and is controlled by that site. By associating an account managed by a third party with your account and authorizing our services to have access to this information, you agree that we may collect, store and use this information in accordance with this Privacy Policy.
        We may also collect additional information from or about you in other ways, such as through contact with our customer support team, results when you respond to a survey and from interactions with providers/representatives of the services.
    </section>


    <h1>How we protect and store personal information :</h1>
    Throughout this policy, we use the term “personal information” to describe information that can be associated with a specific person and can be used to identify that person. We do not consider personal information to include information that has been made anonymous so that it does not identify a specific user.
We store and process your personal information on our private servers. We protect your information using physical, technical, and administrative security measures to reduce the risks of loss, misuse, unauthorized access, disclosure and alteration. Some of the safeguards we use are physical access controls to your data and information access authorization controls.



<h1>How we share personal information with other parties :</h1>

<section>
    <p>•	• If you open an account, we may collect the following types of information:</p>


    <p>•	• Services providers – individuals who provide laundry service to you or your business</p>

    <p>•	• Service providers who help with our business operations such as fraud prevention, accounting, marketing and technology services. These service providers only use your information in connection with the services they perform for us and not for their own benefit.</p>


    <p>•	• Credit bureaus and collection agencies to report account information, as permitted by law.</p>


    <p>•	• Companies that we plan to merge with or are acquired by. (Should such a combination occur, we will require that the new combined entity follow this privacy policy with respect to your personal information. If your personal information could be used contrary to this policy, you will receive prior notice.)</p>


    <p>•	• Law enforcement, government officials, or other third parties pursuant to a subpoena, court order, or other legal process or requirement applicable to the services; when we need to do so to comply with law or credit card rules; or when we believe, in our sole </p>

    <p>discretion, that the disclosure of personal information is necessary to prevent physical harm or financial loss, to report suspected illegal activity or to investigate violations of our Terms of Service.
        •	• Other third parties with your consent or direction to do so.
    </p>

    We will not sell or rent any of your personal information to third parties for their marketing purposes and only shares your personal information with third parties as described in this policy.

</section>

<h1>How you can access or change your personal information :</h1>
You can edit your personal and payment information at any time by submitting changes through the online client portal or mobile application. You can also close your account via those platforms. If you close your account, we will mark your account in our database as “Closed,” but may retain personal information from your account to collect any fees owed, resolve disputes, troubleshoot problems, assist with any investigations, prevent fraud, enforce our Terms of Service, or take other actions as required or permitted by law.

<h1>How you can contact us about privacy questions :</h1>

If you have questions or concerns regarding this policy, you should contact us by emailing Nathefah Care.

If you have questions or concerns regarding this policy, you should contact us by emailing Nathefah Care.

<h1>Contacts and support:</h1>
+966500506704

</body>
</html>
