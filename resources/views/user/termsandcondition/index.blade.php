<!DOCTYPE html>
<html lang="en">
<!-- End Header -->
<main id="main">
    <!-- ======= Policy Section ======= -->
    <section id="tabs" class="tabs">
        <div class="container" data-aos="fade-up">

            <div class="section-title">
                <h2>Terms & Condition</h2>
                <p style="text-align:left">
                    Thanks for using the Nathefah. The following terms and conditions (these “Terms of Use”) govern your access to, and use of, the Nathefah service, including any services, content, and functionality offered on or through the Nathefah mobile application (The “App”) and website (The “Site”).
                </p>
                <br>
                <p style="text-align:left">
                    Please read the following Terms of Use carefully before using the App or Site.
                </p>
                <br>
                <div style="text-align:left">
                    <ul>
                        <li>All credit/debit cards details and personally identifiable information will NOT be stored, sold, shared, rented or leased to any third parties.</li>
                        <li>Saudi Arabia is our country of domicile.</li>
                        <li>Website will NOT deal or provide any services or products to any of OFAC (Office of Foreign Assets Control) sanctions countries in accordance with the law of Saudi Arabia.</li>
                        <li>We accept payments online using Visa and MasterCard credit/debit card in SAR. </li>
                        <li>Refunds will be done only through the Original Mode of Payment.</li>
                        <li>Any dispute or claim arising out of or in connection with this website shall be governed and construed in accordance with the laws of Saudi Arabia. </li>
                    </ul>
                </div>
            </div>

            <div class="tab-content">
                <div class="tab-pane active show" id="tab-1">
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>YOUR ACCEPTANCE</h3><br>
                            <p>
                                You understand that by using the App or Site, you are accepting and agreeing to be bound and abide by these Terms of Use as well as our Terms of Service and Privacy Policy, found at within the Nathefah mobile application, which is incorporated herein by reference. Please do not use the App or Service if you do not agree with these Terms of Use. You acknowledge and agree that we will not be liable if for any reason all or any part of the App is unavailable at any time or for any period. We reserve the right to change these Terms of Use at any time in our sole discretion and without notice. All changes are effective immediately when we post them. Your continued use of the App after we have posted changes to these Terms of Use means that you agree to be bound and abide by the changes, so please check the Terms of Use regularly for any changes.
                                </b><br>
                        </div>
                    </div>
                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>YOUR USE OF THE APP</h3><br>
                            <p>
                                You are strictly prohibited from using the App for any unlawful purpose, or for any other purpose that is not expressly allowed by these Terms of Use. In addition, you may not interfere or attempt to interfere with the proper operation of our App, including through the use of any device, software or routine, or access or attempt to gain access to any data, files or passwords related to our site through hacking, circumvention, or any other means. We reserve the right, in our sole discretion, to deny you access to our site, or any portion of our site, without notice. If we terminate or suspend your account for any reason, you are prohibited from registering and creating a new account under your name, a fake or borrowed name, or the name of any third party, even if you may be acting on behalf of the third party. In addition to terminating or suspending your account, we reserve the right to take appropriate legal action, including without limitation pursuing civil and criminal.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>TERMS OF SERVICE</h3><br>
                            <p>
                                Please refer to our “Terms of Service” found at <a href="https://www.nathefah.com/TermsCondition.html" target="_blank">Terms and Condition</a>, which addresses specific details regarding the services provided via our App and Site. By using the App or Site, you consent to the terms listed regarding, but not limited to, scheduling, pickup, procedures, requirements, limitations, delivery, cancellations, missed pickups/deliveries, loss/damage and any associated fees. Nathefah reserves the right to refuse service to any customer who violates the terms of service.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>YOUR INFORMATION</h3><br>
                            <p>
                                Please refer to our Privacy Policy found at <a href="https://www.nathefah.com/PrivacyPolicy.html" target="_blank">Privacy Policy</a>, which addresses the information that we may collect from you when you use the App or Site. By using the App or Site, you consent to all actions taken by us with respect to your information in compliance with the Privacy Policy.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>FRAMING</h3><br>
                            <p>
                                You may not frame the App within another App, without our express, written permission.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>INTELLECTUAL PROPERTY RIGHTS IN THE APP</h3><br>
                            <p>
                                The App and Site contains various information in the form of data, text, graphics, and other materials from Nathefah and third parties (the “Content”). You acknowledge that the App/Site and various elements contained therein are protected by copyrights, trademarks, trade secrets, patents, or other proprietary rights, and that these worldwide rights are valid and protected in all forms, media, and technologies existing now and hereinafter developed. You also acknowledge that the Content is and shall remain the property of Nathefah or its respective owners. You agree to comply with all intellectual property laws and you shall not encumber any interest in, or assert any rights to, the Content. You may not modify, transmit, participate in the sale or transfer of, or create derivative works based on any Content, in whole or in part. However, you may:
                                </b><br>
                            <div style="text-align:left">
                                <ul>
                                    <li>(i) print a single copy of the Content for your own personal use, provided that you maintain any notices contained in the Content, such as all copyright notices, trademark legends, or other proprietary rights notices.</li>
                                    <li>(ii) temporarily store copies of such Content in your computer’s and/or mobile devices RAM that are incidental to your accessing and viewing of such Content.</li>
                                    <li>(iii) store files that are automatically cached by your mobile device for display enhancement purposes.</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>TRADEMARKS</h3><br>
                            <p>
                                The trademarks, service marks, and logos used and displayed on our site are registered and unregistered trademarks of ours. You must not use such trademarks without the prior written permission of Nathefah. All other names, logos, product and service names, designs and slogans on this App are the trademarks of their respective owners.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>COPYRIGHT COMPLAINTS</h3><br>
                            <p>
                                If you believe that any Content on our site violates your or a third party’s copyright, please notify us by providing the following information to us at our e-mail address at <a href="mailto:no-reply@nathefah.com">Nathefah Care.</a>
                                </b><br>

                            <div style="text-align:left">
                                <ul>
                                    <li>(1) an electronic or physical signature of the person authorized to act on behalf of the owner of the copyright interest.</li>
                                    <li>(2) a description of the copyrighted work that you claim has been infringed.</li>
                                    <li>(3) a description of where the material that you claim is infringing is located on our site.</li>
                                    <li>(4) your address, telephone number and email address.</li>
                                    <li>(5) a statement by you that you have a good faith belief that the disputed use is not authorized by the copyright owner, its agent or the law.</li>
                                    <li>(6) a statement by you, made under penalty of perjury, that the above information in your notice is accurate and that you are the copyright owner or duly authorized to act on the copyright owner’s behalf.</li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>LINKS TO THIRD PARTY APPS</h3><br>
                            <p>
                                The App and Site may contain links to third party Apps and sites. Any such links are provided for your convenience only. We do not control those Apps or sites, and we are not responsible for their contents or practices, including their privacy practices. We do not endorse the operators of those apps or sites, nor do we endorse or make any representations with respect to the contents of those apps or sites or any products and/or services that may be offered on those apps or sites.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>CORRECTIONS</h3><br>
                            <p>
                                There may be information the App or Site that contains typographical errors, inaccuracies or omissions, including descriptions, pricing, availability and various other information. We reserve the right to correct any errors, inaccuracies or omissions and to change or update the App or Site at any time, without prior notice. Any changes made take effect immediately.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>WARRANTY DISCLAIMER</h3><br>
                            <p>
                                This App/Site and the Content, materials and services on this site are provided “as is” and without warranties of any kind, whether express or implied. Neither we nor any of our affiliates assume any responsibility for the accuracy of any information contained on the App, for any interruptions or errors in accessing the App/Site, or for any viruses or other harmful components contained on the App or the server from which our site is made available. Neither we nor any of our affiliates make any warrantees or representations regarding the use of the materials in this site in terms of their completeness, correctness, accuracy, adequacy, usefulness, timeliness, reliability or otherwise. WE AND OUR AFFILIATES DO NOT MAKE, AND HEREBY DISCLAIM, ANY AND ALL WARRANTIES OF ANY KIND WITH RESPECT TO THE APP OR THE SERVICES OFFERED THROUGH THE APP/SITE, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, NON-INFRINGEMENT OR ANY WARRANTIES ARISING FROM A COURSE OF PERFORMANCE, A COURSE OF DEALING OR TRADE USAGE.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>GOVERNING LAW AND JURISDICTION</h3><br>
                            <p>
                                These Terms of Use, and all matters arising directly or indirectly from your access to or other use of the App, shall be governed by and construed in accordance with the laws of Saudi Arabia, without regard to its conflict of laws rules. You hereby submit to the exclusive jurisdiction of Saudi Arabia, and waive any jurisdictional, venue, or inconvenient forum objections to such courts. A printed version of these Terms of Use shall be admissible in judicial or administrative proceedings based on or relating to these Terms of Use to the same extent and subject to the same conditions as other business documents and records originally generated and maintained in printed form.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>SEVERABILITY</h3><br>
                            <p>
                                If any provision of these Terms of Use is held to be illegal, invalid or unenforceable, such illegality, invalidity or unenforceability shall apply only to such provision and shall not in any manner affect or render illegal, invalid or unenforceable any other provision of these Terms of Use, and that provision and these Terms of Use generally shall be reformed, construed and enforced so as to most nearly give lawful effect to the intent of the parties as expressed in these Terms of Use.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>ENTIRE AGREEMENT</h3><br>
                            <p>
                                These Terms of Use constitute the entire agreement between you and us with respect to our site, and supersede all prior or contemporaneous communications and proposals, whether electronic, oral or written, between you and us with respect to our site.
                                </b><br>
                        </div>
                    </div>

                    <br><br>
                    <div class="row">
                        <div class="col-lg-12 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
                            <h3>YOUR COMMENTS AND CONCERNS</h3><br>
                            <p>
                                Thank you for your cooperation. We hope that you enjoy your experience on our App. Questions or comments regarding the App or Site, including any reports of non-functioning links, should be submitted using our e-mail address at <a href="mailto:no-reply@nathefah.com">Nathefah Care.</a>
                                </b><br>
                        </div>
                    </div>
                </div>
            </div>

    </section><!-- End  Section -->
</main><!-- End #main -->

</body>
</html>
